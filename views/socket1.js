window.onload = function() {

    var messages = [];
    var socket = io.connect(document.getElementById('apilink'));
    var sendButton = document.getElementById('connect');
    socket.on('view_fleets_location_success', function (data) {
        console.log("view_fleets_location_success");
        console.log(data);
    });

    socket.on('check_notifications_success', function (data) {
        console.log("check_notifications_success");
        console.log(data)
    });
    socket.on('view_fleets_location_error', function (data) {
        console.log("view_fleets_location_error");
        console.log(data)
    });
    socket.on('check_notifications_error', function (data) {
        console.log("check_notifications_error");
        console.log(data);
    });

    sendButton.onclick = function() {
        console.log("connect");
        socket.emit('view_fleets_location', {access_token: '780e139515e9bf5db2a6f00527ebd133'});
        socket.emit('check_notifications', {access_token: '780e139515e9bf5db2a6f00527ebd133'});
    };

};