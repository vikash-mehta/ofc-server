var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var json2csv = require('json2csv');
var socketResponse = require('./socketResponses');
var parseCsv = require('fast-csv');
var jobs = require('./jobs');
/*
 * --------------------------
 * VIEW JOB DETAILS
 * --------------------------
 */
exports.view_job_details = function (req, res) {
    var job_id = req.body.job_id;
    var manvalues = [job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateJobHash(job_id, function (result) {
            if (result == 0) {
                responses.invalidUrlErrorResponse(res);
                return;
            } else {
                module.exports.fetch_job_details(job_id, function (response) {
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
}

exports.fetch_job_details = function (job_id, callback) {
    var sql = "SELECT jobs.`job_id`,jobs.`is_customer_rated`,jobs.`is_customer_rated` as `status`,jobs.`job_pickup_address`," +
        "jobs.`job_address`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_type`,jobs.`job_status`," +
        "fleets.`latitude` as `fleet_latitude`,fleets.`longitude` as fleet_longitude,fleets.`is_available`,fleets.`fleet_id`," +
        "fleets.`status` as `fleet_status`,fleets.`username` as `fleet_name`,fleets.`phone` as `fleet_phone`,fleets.`fleet_image`,fleets.`fleet_thumb_image`, " +
        "user.`user_id`,user.`company_image`,user.`is_company_image_view`,user.`is_driver_image_view`,user.`is_whitelabel`,user.`brand_image`,user.`fav_icon` " +
        "FROM `tb_jobs` jobs " +
        "LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id` = jobs.`fleet_id` " +
        "LEFT JOIN `tb_users` user ON user.`user_id` = jobs.`user_id` " +
        "WHERE jobs.`job_hash`= ? ";
    connection.query(sql, [job_id], function (err, customer) {
        if (err) {
            var response = {
                "message": constants.responseMessages.ERROR_IN_EXECUTION,
                "status": constants.responseFlags.ERROR_IN_EXECUTION,
                "data": {}
            };
            callback(response);
        } else {
            if (customer[0].is_company_image_view == 1) {
                customer[0].fleet_image = customer[0].company_image;
            } else if (customer[0].is_driver_image_view == 1) {
                customer[0].fleet_image = customer[0].fleet_thumb_image;
            }
            if (customer[0].job_status == constants.jobStatus.FAILED) {
                customer[0].status = constants.ratingWindowStatus.CANCEL
            } else if ((customer[0].job_status == constants.jobStatus.ENDED ) && (customer[0].is_customer_rated == constants.isCusotmerRated.NO)) {
                customer[0].status = constants.ratingWindowStatus.YES
            } else if ((customer[0].job_status == constants.jobStatus.ENDED) && (customer[0].is_customer_rated == constants.isCusotmerRated.YES)) {
                customer[0].status = constants.ratingWindowStatus.DISABLE
            } else if (customer[0].job_status == constants.jobStatus.ENDED) {
                customer[0].status = constants.ratingWindowStatus.NO
            }
            var response = {
                "message": constants.responseMessages.ACTION_COMPLETE,
                "status": constants.responseFlags.ACTION_COMPLETE,
                "data": customer
            };
            callback(response);
        }
    });
}

/*
 * --------------------------
 * VIEW TASK AND FLEET DETAILs
 * --------------------------
 */
exports.task_information = function (req, res) {

    var job_id = req.body.hash;
    var manvalues = [job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateJobHash(job_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT jobs.`job_id`," +
                    "jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_type`,jobs.`job_status`," +
                    "fleets.`latitude` as `fleet_latitude`,fleets.`longitude` as fleet_longitude,fleets.`is_available`,fleets.`fleet_id`," +
                    "fleets.`status`,fleets.`username` as `fleet_name`,fleets.`phone` as `fleet_phone`,fleets.`fleet_image` " +
                    "FROM `tb_jobs` jobs LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id` = jobs.`fleet_id` " +
                    "WHERE jobs.`job_hash`= ? ";
                connection.query(sql, [job_id], function (err, result_jobs) {
                    if (err) {
                        responses.sendError(res);
                        return;
                    } else {
                        if (result_jobs.length > 0) {
                            result_jobs[0].job_type = result_jobs[0].job_type == constants.jobType.PICKUP ? "Pick-up" : result_jobs[0].job_type == constants.jobType.DELIVERY ? "Delivery" : result_jobs[0].job_type == constants.jobType.APPOINTMENT ? "Appointment" : "";

                            if (result_jobs[0].job_status == constants.jobStatus.UNASSIGNED) {
                                result_jobs[0].job_status = 'Unassigned';
                            } else if (result_jobs[0].job_status == constants.jobStatus.UPCOMING) {
                                result_jobs[0].job_status = 'Assigned';
                            } else if (result_jobs[0].job_status == constants.jobStatus.STARTED) {
                                result_jobs[0].job_status = 'In Transit';
                            } else if (result_jobs[0].job_status == constants.jobStatus.ENDED) {
                                result_jobs[0].job_status = 'Completed';
                            } else if (result_jobs[0].job_status == constants.jobStatus.FAILED) {
                                result_jobs[0].job_status = 'Failed';
                            } else if (result_jobs[0].job_status == constants.jobStatus.ARRIVED) {
                                result_jobs[0].job_status = 'Arrived';
                            } else if (result_jobs[0].job_status == constants.jobStatus.PARTIAL) {
                                result_jobs[0].job_status = 'Partial';
                            }
                        }
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result_jobs
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                });
            }
        });
    }
};
/*
 * --------------------------
 * CUSTOMER RATING
 * --------------------------
 */
exports.customer_rating = function (req, res) {

    var job_id = req.body.job_id;
    var rating = req.body.rating;
    var customer_comment = req.body.customer_comment;
    var manvalues = [job_id, rating];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        rating = parseInt(rating);
        commonFunc.authenticateJobHash(job_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var sql = "UPDATE `tb_jobs` SET `is_customer_rated`=?, `fleet_rating`=?,`customer_comment`=? WHERE `job_hash`=? ";
                connection.query(sql, [constants.isCusotmerRated.YES, rating, customer_comment, job_id], function (err, result_update_customer_rating) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in updating customer information : ", err, result_update_customer_rating);
                        responses.sendError(res);
                        return;
                    } else {
                        var sql = "UPDATE `tb_fleets` SET `total_rating` = `total_rating` + ?, `total_rated_tasks`= `total_rated_tasks` + 1 " +
                            " WHERE `fleet_id`=? LIMIT 1 ";
                        connection.query(sql, [rating, result[0].fleet_id], function (err, update_fleet_customer_rating) {
                            if (err) {
                                responses.sendError(res);
                                return;
                            } else {
                                var text = result[0].customer_username + " commented the task #" + result[0].job_id;
                                commonFunc.setNotification(result[0].user_id, text, "Customer Rating/Comment", result[0].job_id);
                                socketResponse.sendSocketResponse(result[0].job_id);
                                responses.actionCompleteResponse(res);
                            }
                        });
                    }
                });
            }
        });
    }
};
/*
 * --------------------------
 * DELETE CUSTOMER
 * --------------------------
 */
exports.delete_customer = function (req, res) {

    var access_token = req.body.access_token;
    var customer_id = req.body.customer_id;
    var manvalues = [access_token, customer_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.authenticateCustomerIdAndUserID(customer_id, user_id, function (auth) {
                    if (auth == 0) {
                        responses.authenticationError(res);
                        return;
                    } else {
                        var sql = "DELETE FROM `tb_customers` WHERE `customer_id`=? AND `user_id`=? LIMIT 1 ";
                        connection.query(sql, [customer_id, user_id], function (err, delete_customer) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in deleting customer information : ", err, delete_customer);
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                                return;
                            }
                        });
                    }
                });
            }
        });
    }
};
/*
 * --------------------------
 * VIEW CUSTOMER PROFILE
 * --------------------------
 */
exports.view_customer_profile = function (req, res) {

    var access_token = req.body.access_token;
    var customer_id = req.body.customer_id;
    var manvalues = [access_token, customer_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    user_id = result[0].dispatcher_user_id;
                    step_in(user_id);
                } else {
                    step_in(user_id);
                }
                function step_in(user_id) {
                    commonFunc.authenticateCustomerIdAndUserID(customer_id, user_id, function (authenticateCustomerIdAndUserIDResult) {
                        if (authenticateCustomerIdAndUserIDResult == 0) {
                            responses.invalidAccessError(res);
                        } else {
                            var sql = "SELECT  fleets.`fleet_id`,fleets.`username` as `fleet_name`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`," +
                                "jobs.`pickup_delivery_relationship`," +
                                "jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`,customers.`customer_username`,customers.`customer_phone`,customers.`customer_email` " +
                                "FROM `tb_customers` customers LEFT JOIN `tb_jobs` jobs ON customers.`customer_id`= jobs.`customer_id` LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
                                "WHERE customers.`customer_id`=? AND jobs.`job_status`<>?";
                            connection.query(sql, [customer_id, constants.jobStatus.DELETED], function (err, result_jobs) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in viewing customer info : ", err, result_jobs);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var result_jobs_length = result_jobs.length;
                                    for (var i = 0; i < result_jobs_length; i++) {
                                        if (result_jobs[i].job_id) {
                                            if (result_jobs[i].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                result_jobs[i].job_pickup_datetime = moment(result_jobs[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a')
                                            } else {
                                                result_jobs[i].job_pickup_datetime = '-';
                                            }
                                            if (result_jobs[i].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                result_jobs[i].job_delivery_datetime = moment(result_jobs[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a')
                                            } else {
                                                result_jobs[i].job_delivery_datetime = '-';
                                            }
                                            if (result_jobs[i].customer_username == "dummy") {
                                                result_jobs[i].customer_username = "";
                                            }
                                            if (result_jobs[i].customer_email == "dummy") {
                                                result_jobs[i].customer_email = "";
                                            }
                                            if (result_jobs[i].customer_phone == "+910000000000") {
                                                result_jobs[i].customer_phone = "";
                                            }

                                        } else {
                                            delete result_jobs[i].job_pickup_name;
                                            delete result_jobs[i].job_pickup_phone;
                                            delete result_jobs[i].job_latitude;
                                            delete result_jobs[i].job_longitude;
                                            delete result_jobs[i].job_address;
                                            delete result_jobs[i].job_status;
                                            delete result_jobs[i].job_description;
                                            delete result_jobs[i].has_pickup;
                                            delete result_jobs[i].job_pickup_datetime;
                                            delete result_jobs[i].job_id;
                                            delete result_jobs[i].job_delivery_datetime;
                                            delete result_jobs[i].job_type;
                                            delete result_jobs[i].job_pickup_latitude;
                                            delete result_jobs[i].job_pickup_longitude;
                                            delete result_jobs[i].job_pickup_address;
                                            delete result_jobs[i].pickup_delivery_relationship;
                                            delete result_jobs[i].fleet_id;
                                            delete result_jobs[i].pickup_delivery_relationship;
                                        }
                                    }
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": result_jobs
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            });
                        }
                    });
                }
            }
        });
    }
};
/*
 * --------------------------
 * VIEW ALL CUSTOMERS
 * --------------------------
 */
exports.view_customers = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    user_id = result[0].dispatcher_user_id;
                }
                var sql = "SELECT * FROM `tb_customers` " +
                    "WHERE `user_id`=? ";
                viewAllCustomers(sql, user_id, function (viewAllCustomerResult) {
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": viewAllCustomerResult
                    };
                    res.send(JSON.stringify(response));
                    return;

                })
            }
        });
    }
};
/*
 * --------------------------
 * UPDATE PARTICULAR CUSTOMER
 * --------------------------
 */
exports.update_customer = function (req, res) {

    var access_token = req.body.access_token;
    var customer_username = req.body.customer_username;
    var customer_email = req.body.customer_email;
    var customer_phone = req.body.customer_phone;
    var customer_address = req.body.customer_address;
    var customer_latitude = req.body.customer_latitude;
    var customer_longitude = req.body.customer_longitude;
    var customer_id = req.body.customer_id;
    var manvalues = [access_token, customer_username, customer_phone, customer_longitude, customer_latitude, customer_address, customer_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "UPDATE `tb_customers` SET " +
                    " `customer_username`=?,`customer_phone`=?,`customer_email`=?,`customer_longitude`=?,`customer_latitude`=?,`customer_address`=? " +
                    " WHERE `user_id`=? AND `customer_id` =? ";
                connection.query(sql, [customer_username, customer_phone, customer_email, customer_longitude, customer_latitude, customer_address, result[0].user_id, customer_id], function (err, update_customer) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in updating customer info : ", err, update_customer);
                        responses.sendError(res);
                        return;
                    } else {
                        responses.actionCompleteResponse(res);
                        return;
                    }
                });
            }
        });
    }
};
/*
 * ----------------------------------------
 * VIEW ALL CUSTOMERS WITH PAGINATION
 * ----------------------------------------
 */
exports.view_customers_with_pagination = function (req, res) {
    var access_token = req.query.access_token;
    commonFunc.authenticateUserAccessToken(access_token, function (result) {
        if (result == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            commonFunc.checkAccountExpiry(result[0].user_id, function (chk) {
                if (chk) {
                    responses.accountExpiryErrorResponse(res);
                    return;
                } else {
                    var user_id = result[0].user_id;
                    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        user_id = result[0].dispatcher_user_id;
                        step_in(user_id);
                    } else {
                        step_in(user_id);
                    }
                    function step_in(user_id) {
                        var QueryBuilder = require('datatable');
                        var tableDefinition = {
                            sSelectSql: "" +
                            "tb_customers.customer_id," +
                            "tb_customers.customer_username," +
                            "tb_customers.customer_phone," +
                            "tb_customers.customer_email," +
                            "tb_customers.customer_address",
                            sFromSql: " `tb_customers`",
                            aSearchColumns: [
                                "tb_customers.customer_id", "tb_customers.customer_username", "tb_customers.customer_phone", "tb_customers.customer_email", "tb_customers.customer_address"
                            ],
                            sWhereAndSql: " tb_customers.`user_id` = " + user_id,
                            sCountColumnName: " tb_customers.`customer_id` ",
                            aoColumnDefs: [
                                {mData: "customer_username", bSearchable: true},
                                {mData: "customer_phone", bSearchable: true},
                                {mData: "customer_email", bSearchable: true},
                                {mData: "customer_address", bSearchable: true},
                                {mData: "", bSearchable: false}
                            ]
                        };

                        var queryBuilder = new QueryBuilder(tableDefinition);
                        var requestQuery = req.query;
                        var queries = queryBuilder.buildQuery(requestQuery);
                        if (queries.length > 2) {
                            queries = queries.splice(1);
                        }
                        queries = queries.join(" ");
                        connection.query(queries, function (err, resultAllCustomerData) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in fetching all customers : ", err, resultAllCustomerData);
                                responses.sendError(res);
                                return;
                            }
                            var resultAllCustomer = resultAllCustomerData.pop();
                            var resultAllCustomerLength = resultAllCustomer.length;
                            if (resultAllCustomerLength > 0) {
                                var arrResultRides = new Array();
                                for (var i = 0; i < resultAllCustomerLength; i++) {
                                    arrResultRides[i] = new Array();
                                    if ((resultAllCustomer[i].customer_username == "dummy") || (resultAllCustomer[i].customer_username == null)) {
                                        resultAllCustomer[i].customer_username = "";
                                    }
                                    if ((resultAllCustomer[i].customer_email == "dummy") || (resultAllCustomer[i].customer_email == null)) {
                                        resultAllCustomer[i].customer_email = "";
                                    }
                                    if ((resultAllCustomer[i].customer_phone == "+910000000000") || (resultAllCustomer[i].customer_phone == null)) {
                                        resultAllCustomer[i].customer_phone = "";
                                    }
                                    arrResultRides[i][0] = '<a ng-click=getCustomerProfile(' + resultAllCustomer[i].customer_id + ',0)>' + resultAllCustomer[i].customer_username + '</a>';
                                    arrResultRides[i][1] = resultAllCustomer[i].customer_phone;
                                    arrResultRides[i][2] = resultAllCustomer[i].customer_email;
                                    arrResultRides[i][3] = resultAllCustomer[i].customer_address;

                                    var parameters = '"' + resultAllCustomer[i].customer_id + '"' + "," + '"' + resultAllCustomer[i].customer_username + '"' + "," + '"' + resultAllCustomer[i].customer_phone + '"' + "," + '"' + resultAllCustomer[i].customer_email + '"' + "," + '"' + resultAllCustomer[i].customer_address + '"';
                                    arrResultRides[i][4] = "<a href='' class='btn btn-default' tooltip='Edit' ng-click='editCustomerDetail(" + parameters + ")'><i class='fa fa-pencil icons'></i></a>" +
                                        "<a href='' tooltip='Delete' class='btn btn-default' ng-click='deleteCustomer(" + '"' + resultAllCustomer[i].customer_id + '"' + ")'><i class='fa fa-trash icons'></i></a>";

                                }
                                var response = {
                                    "iTotalDisplayRecords": resultAllCustomerData[0][0]['COUNT(*)'],
                                    "iTotalRecords": resultAllCustomerData[0][0]['COUNT(*)'],
                                    "sEcho": 0,
                                    "aaData": arrResultRides
                                };
                                res.send(JSON.stringify(response));
                                return;
                            } else {
                                var response = {
                                    "iTotalDisplayRecords": 0,
                                    "iTotalRecords": resultAllCustomerData[0][0]['COUNT(*)'],
                                    "sEcho": requestQuery.sEcho,
                                    "aaData": []
                                };
                                res.send(JSON.stringify(response));
                                return;
                            }
                        });
                    }

                }
            });
        }
    });
};
/*
 * -----------------------------
 * EXPORT ALL CUSTOMERS INTO CSV
 * ------------------------------
 */
exports.export_customers = function (req, res) {

    var access_token = req.body.access_token;
    var type = req.body.type;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var dispatcher_id;
                var dispatcher_user_id = result[0].dispatcher_user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            dispatcher_id = user_id;
                            user_id = dispatcher_user_id;
                        }
                        var sql = "SELECT * FROM `tb_customers` WHERE `user_id`=? ";
                        if (type == "custom" || type == "today") {
                            sql += " AND DATE(`creation_datetime`)>='" + req.body.start_date + "' AND DATE(`creation_datetime`)<='" + req.body.end_date + "' ";
                        }
                        viewAllCustomers(sql, user_id, function (viewAllCustomerResult) {
                            var fields = ['Customer_ID', 'Name', 'Phone', 'Email', 'Address'];
                            exportData(viewAllCustomerResult, fields, res);
                        })
                    }
                });
            }
        });
    }
};
function exportData(dataObject, fields, res) {
    if (dataObject.length == 0) {
        responses.noDataFoundError(res);
    } else {
        json2csv({data: dataObject, fields: fields}, function (err, csv) {
            console.log(err);
            if (err) console.log(err);
            res.setHeader('Content-Type', 'application/octet-stream');
            res.setHeader('Content-Disposition', 'attachment; filename=customers.csv;');
            res.end(csv, 'binary');
        });
    }
}
function viewAllCustomers(sql, user, callback) {
    connection.query(sql, [user], function (err, customer_details) {
        if (err) {
            logging.logDatabaseQueryError("Error in viewing customer info : ", err, customer_details);
            responses.sendError(res);
            return;
        } else {
            var customer_details_length = customer_details.length;
            if (customer_details_length == 0) {
                callback({});
            } else {
                for (var i = 0; i < customer_details_length; i++) {
                    customer_details[i].Name = customer_details[i].customer_username;
                    customer_details[i].Email = customer_details[i].customer_email;
                    customer_details[i].Phone = customer_details[i].customer_phone;
                    customer_details[i].Address = customer_details[i].customer_address;
                    customer_details[i].Customer_ID = customer_details[i].customer_id;
                    if (customer_details[i].Name == "dummy" || customer_details[i].Name == null || customer_details[i].Name == "null") {
                        customer_details[i].Name = "";
                    }
                    if (customer_details[i].Email == "dummy" || customer_details[i].Email == null || customer_details[i].Email == "null") {
                        customer_details[i].Email = "";
                    }
                    if (customer_details[i].Phone == "+910000000000" || customer_details[i].Phone == null | customer_details[i].Phone == "null") {
                        customer_details[i].Phone = "";
                    }
                }
                callback(customer_details);
            }
        }
    });
}
/*
 * ----------------------------------------
 * VIEW ALL CUSTOMERS WHO RATED OR COMMENTED
 * ----------------------------------------
 */
exports.get_customer_rating = function (req, res) {

    var access_token = req.query.access_token;
    commonFunc.authenticateUserAccessToken(access_token, function (result) {
        if (result == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            commonFunc.checkAccountExpiry(result[0].user_id, function (chkExp) {
                if (chkExp) {
                    responses.accountExpiryErrorResponse(res);
                    return;
                } else {
                    var   whereSql = "tb_jobs.`job_status` NOT IN ( " + constants.jobStatus.DELETED + ") AND (tb_jobs.fleet_rating > 0 || tb_jobs.customer_comment <> '') AND tb_jobs.`user_id` = ";
                    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        commonFunc.checkDispatcherPermissions(result[0].user_id, function (disPerm) {
                            var te = '0,' + disPerm[0].teams;
                            if (disPerm && disPerm.length && disPerm[0].view_task == constants.hasPermissionStatus.NO) {
                                te = disPerm[0].teams;
                            }
                            whereSql = "tb_jobs.`job_status` NOT IN ( " + constants.jobStatus.DELETED + ") AND (tb_jobs.fleet_rating > 0 || tb_jobs.customer_comment <> '') AND tb_jobs.`team_id` IN (" + te + ") AND tb_jobs.`user_id` = ";
                            checkIn(whereSql, result[0].dispatcher_user_id);
                        })
                    } else {
                        checkIn(whereSql, result[0].user_id);
                    }

                    function checkIn (whereSql, user_id) {

                        var QueryBuilder = require('datatable');
                        var tableDefinition = {
                            sSelectSql: "" +
                            "tb_jobs.job_id," +
                            "tb_jobs.customer_username," +
                            "tb_jobs.fleet_rating," +
                            "tb_jobs.pickup_delivery_relationship," +
                            "tb_jobs.customer_comment ",
                            sFromSql: " `tb_jobs` ",
                            aSearchColumns: [
                                "tb_jobs.job_id", "tb_jobs.customer_username", "tb_jobs.fleet_rating", "tb_jobs.customer_comment"
                            ],
                            sWhereAndSql: whereSql + user_id,
                            sCountColumnName: " tb_customers.`job_id` ",
                            aoColumnDefs: [
                                {mData: "job_id", bSearchable: true},
                                {mData: "customer_username", bSearchable: true},
                                {mData: "fleet_rating", bSearchable: true},
                                {mData: "customer_comment", bSearchable: true}
                            ]
                        };
                        var queryBuilder = new QueryBuilder(tableDefinition);
                        var requestQuery = req.query;
                        var queries = queryBuilder.buildQuery(requestQuery);
                        if (queries.length > 2) {
                            queries = queries.splice(1);
                        }
                        queries = queries.join(" ");
                        connection.query(queries, function (err, resultAllCustomerData) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in fetching all customers : ", err, resultAllCustomerData);
                                responses.sendError(res);
                                return;
                            }
                            var resultAllCustomer = resultAllCustomerData.pop();
                            var resultAllCustomerLength = resultAllCustomer.length;
                            if (resultAllCustomerLength > 0) {
                                var arrResultRides = new Array();
                                for (var i = 0; i < resultAllCustomerLength; i++) {
                                    arrResultRides[i] = new Array();
                                    if ((resultAllCustomer[i].customer_username == "dummy") || (resultAllCustomer[i].customer_username == null)) {
                                        resultAllCustomer[i].customer_username = "";
                                    }
                                    arrResultRides[i][0] = '<a ng-click=viewtaskProfileDialog("' + resultAllCustomer[i].pickup_delivery_relationship + ',' + resultAllCustomer[i].job_id + '")>' + resultAllCustomer[i].job_id + '</a>';
                                    arrResultRides[i][1] = resultAllCustomer[i].customer_username;
                                    arrResultRides[i][2] = resultAllCustomer[i].fleet_rating;
                                    arrResultRides[i][3] = resultAllCustomer[i].customer_comment;
                                }
                                var response = {
                                    "iTotalDisplayRecords": resultAllCustomerData[0][0]['COUNT(*)'],
                                    "iTotalRecords": resultAllCustomerData[0][0]['COUNT(*)'],
                                    "sEcho": 0,
                                    "aaData": arrResultRides
                                };
                                res.send(JSON.stringify(response));
                                return;
                            } else {
                                var response = {
                                    "iTotalDisplayRecords": 0,
                                    "iTotalRecords": resultAllCustomerData[0][0]['COUNT(*)'],
                                    "sEcho": requestQuery.sEcho,
                                    "aaData": []
                                };
                                res.send(JSON.stringify(response));
                                return;
                            }
                        })
                    }
                }
            })
        }
    })
};

/*
 * -----------------------------
 * EXPORT CUSTOMERS WITH RATING
 * ------------------------------
 */
exports.export_customers_rating = function (req, res) {

    var access_token = req.body.access_token;
    var type = req.body.type;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var dispatcher_id;
                var dispatcher_user_id = result[0].dispatcher_user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            dispatcher_id = user_id;
                            user_id = dispatcher_user_id;
                        }
                        var sql = "SELECT `job_id` as Task_ID,`customer_username` as Customer_Name,`fleet_rating` as Rating,`pickup_delivery_relationship`,`customer_comment` as Feedback " +
                            " FROM `tb_jobs` WHERE tb_jobs.`user_id` = " + user_id + " AND (tb_jobs.fleet_rating > 0 || tb_jobs.customer_comment <> '') ";
                        if (type == "custom" || type == "today") {
                            sql += " AND DATE(`creation_datetime`)>='" + req.body.start_date + "' AND DATE(`creation_datetime`)<='" + req.body.end_date + "' ";
                        }
                        viewAllCustomers(sql, user_id, function (viewAllCustomerResult) {
                            var fields = ['Task_ID', 'Customer_Name', 'Rating', 'Feedback'];
                            exportData(viewAllCustomerResult, fields, res);
                        })
                    }
                });
            }
        });
    }
};
/*
 * -------------------------
 * IMPORT CUSTOMERS FROM CSV
 * -------------------------
 */
exports.import_customers_csv = function (req, res) {
    var access_token = req.body.access_token;
    var file = req.files.customers_csv;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        parseCSV(file.path, function (data) {
                            var dataLength = data.success.length;
                            if (dataLength > 0) {
                                if (dataLength > 200) {
                                    var response = {
                                        "message": constants.responseMessages.CSV_ROWS_ERROR,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                } else {
                                    var index = 1;
                                    if (data.success && data.success[0] && data.success[0][0]) {
                                        if (data.success[0][0] && data.success[0][0].indexOf('Customer Name') >= 0 &&
                                            data.success[0][1] && data.success[0][1].indexOf('Customer Phone') >= 0 &&
                                            data.success[0][2] && data.success[0][2].indexOf('Customer Email') >= 0 &&
                                            data.success[0][3] && data.success[0][3].indexOf('Street') >= 0 &&
                                            data.success[0][4] && data.success[0][4].indexOf('State') >= 0 &&
                                            data.success[0][5] && data.success[0][5].indexOf('Country') >= 0 &&
                                            data.success[0][6] && data.success[0][6].indexOf('Zip') >= 0) {
                                            insertCustomerData(index);
                                        } else {
                                            response = {
                                                "message": constants.responseMessages.INVALID_FORMAT,
                                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                "data": {}
                                            };
                                            res.send(JSON.stringify(response));
                                            return
                                        }
                                    } else {
                                        responses.noDataFoundError(res);
                                    }

                                    function insertCustomerData(item) {
                                        if (item > data.success.length - 1) {
                                            responses.actionCompleteResponse(res);
                                            return;
                                        }
                                        else {
                                            if (item > 0) {
                                                commonFunc.delay(item, function (i) {
                                                    (function (i) {
                                                        var address = ''
                                                        for (var j = 3; j <= 6; j++) {
                                                            if (data.success[i][j]) {
                                                                address += data.success[i][j] + ",";
                                                            }
                                                        }
                                                        address = address.substring(0, address.length - 1);
                                                        data.success[i][1] = commonFunc.formatPhoneNumber(data.success[i][1], result[0].country_phone_code.toUpperCase())
                                                        jobs.addUpdateCustomer(user_id, data.success[i][1], data.success[i][0],
                                                            data.success[i][2], address, null, null, function (addUpdateCustomerResult) {
                                                                if (addUpdateCustomerResult)
                                                                    changeIndex();
                                                            })

                                                    })(i);
                                                });
                                            } else {
                                                changeIndex();
                                            }
                                        }
                                    }
                                }
                                function changeIndex() {
                                    index++;
                                    return insertCustomerData(index);
                                }
                            }
                            else {
                                responses.noDataFoundError(res);
                                return;
                            }
                        })
                    }
                })
            }
        })
    }
}


function parseCSV(path, callback) {
    var SuccessArray = [], ErrorArray = [];
    parseCsv.fromPath(path)
        .on('data', function (data) {
            SuccessArray.push(data);
        })
        .on("error", function (data) {
            ErrorArray.push(data);
        })
        .on('end', function () {
            var ResultObject = {success: SuccessArray, error: ErrorArray};
            callback(ResultObject);
        });  // This just returns something from the inner function, which has no effect.
}