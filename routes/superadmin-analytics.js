/**
 * Created by system72 on 8/10/15.
 */


var async = require('async');
var moment = require('moment');
var commonFunc = require('./commonfunction');
var responses = require('./responses');
var logging = require('./logging');



var sql_job = "SELECT COUNT(tb_jobs.job_id) AS total_task FROM tb_jobs left join tb_users on tb_jobs.user_id=tb_users.user_id  WHERE DATE(tb_jobs.`creation_datetime`) = DATE(NOW()) and tb_users.internal_user=0";
var sql_job_completed = "SELECT COUNT(tb_jobs.job_id) as completed_task FROM tb_jobs left join tb_users on tb_jobs.user_id=tb_users.user_id WHERE DATE(tb_jobs.completed_datetime) = DATE(NOW()) and tb_users.internal_user=0";
var sql_fleet_new = "SELECT COUNT(tb_fleets.fleet_id) as new_fleet FROM tb_fleets left join tb_users on tb_fleets.user_id=tb_users.user_id  where DATE(tb_fleets.creation_datetime) = DATE(NOW()) and tb_fleets.registration_status and tb_users.internal_user=0";
var sql_fleet_active = "SELECT COUNT(tb_fleets.fleet_id) as active_fleet FROM tb_fleets left join tb_users on tb_fleets.user_id=tb_users.user_id  where DATE(tb_fleets.location_update_datetime) = DATE(NOW()) and tb_fleets.registration_status and tb_users.internal_user=0";
var sql_users = "SELECT COUNT(*) as new_user From tb_users where DATE(`creation_datetime`) = DATE(NOW()) && `internal_user` = 0 && is_dispatcher = 0";
var sql_job_count = "select u.user_id, u.username as user_name,(select count(*) from tb_jobs  j where j.user_id = u.user_id && j.job_status in (2,3) ) as job_count from tb_users u having job_count >= 50"

exports.superadmin_analytics_insertion = function (req, res) {

        async.parallel([
                function (callback) {
                    connection.query(sql_job, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                },
                function (callback) {
                    connection.query(sql_fleet_new, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                },
                function (callback) {
                    connection.query(sql_users, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                },
                function (callback) {
                    connection.query(sql_job_count, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                },
                function (callback) {
                    connection.query(sql_job_completed, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                },
                function (callback) {
                    connection.query(sql_fleet_active, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                }],
                function (err, asyncResults) {
                    console.log("Response=========",asyncResults);

                    if(asyncResults.length){

                        var data = {};
                        data = {
                            total_task : 0,
                            completed_task: 0,
                            new_fleet: 0,
                            active_fleet: 0,
                            new_user: 0,
                            user_ids: '',
                            job_count: ''
                        }

                        if(asyncResults[0] && asyncResults[0][0]){
                            if(asyncResults[0][0].hasOwnProperty('total_task')){
                                data.total_task = asyncResults[0][0]['total_task'];
                            }
                        }

                        if(asyncResults[1] && asyncResults[1][0]){
                            if(asyncResults[1][0].hasOwnProperty('new_fleet')){
                                data.new_fleet = asyncResults[1][0]['new_fleet'];
                            }
                        }

                        if(asyncResults[2] && asyncResults[2][0]){
                            if(asyncResults[2][0].hasOwnProperty('new_user')){
                                data.new_user = asyncResults[2][0]['new_user'];
                            }
                        }

                        if(asyncResults[3] && asyncResults[3][0]){
                            var user_id_array = [];
                            var user_taskcount_array = [];
                            (asyncResults[3]).forEach(function(value, key){
                                if(value.hasOwnProperty('user_id')){
                                    user_id_array.push(value.user_id);
                                }

                                if(value.hasOwnProperty('job_count')){
                                    user_taskcount_array.push(value.job_count);
                                }


                            });

                            data.user_ids = user_id_array.join();
                            data.job_count = user_taskcount_array.join();
                        }

                        if(asyncResults[4] && asyncResults[4][0]){
                            if(asyncResults[4][0].hasOwnProperty('completed_task')){
                                data.completed_task = asyncResults[4][0]['completed_task'];
                            }
                        }

                        if(asyncResults[5] && asyncResults[5][0]){
                            if(asyncResults[5][0].hasOwnProperty('active_fleet')){
                                data.active_fleet = asyncResults[5][0]['active_fleet'];
                            }
                        }
                    }

                    var insert_query = 'INSERT INTO tb_analytics (total_task,completed_task,new_fleet, active_fleet, new_user, user_id, job_count, creation_datetime) ' +
                        'VALUES (?,?,?,?,?,?,?, DATE(NOW())) ON DUPLICATE ' +
                        'KEY UPDATE  total_task = values(total_task), ' +
                        'completed_task=VALUES(completed_task), ' +
                        'new_fleet=VALUES(new_fleet), ' +
                        'active_fleet=VALUES(active_fleet), ' +
                        'new_user=VALUES(new_user), ' +
                        'user_id=VALUES(user_id), ' +
                        'job_count=VALUES(job_count)';

                    console.log(insert_query);
                    connection.query(insert_query,
                        [data.total_task, data.completed_task, data.new_fleet, data.active_fleet, data.new_user, data.user_ids, data.job_count],
                    function (err, result) {
                       if(err){
                           console.log('logging error for tb_analytics insertion---', err);
                           return;
                       }
                        console.log(result);
                    res.send(JSON.stringify(data));
                    return;
                    });
                }
        );
};

function authenticateAdminAccessToken(adminAccessToken, callback)
{
    var sql = "SELECT * FROM `tb_admin` WHERE `access_token`=? LIMIT 1";
    connection.query(sql, [adminAccessToken], function(err, result) {
        if (result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};


exports.get_chart_data = function(req,res){
    var access_token = req.body.access_token;
    var start_date = req.body.start_datetime;
    var end_date = req.body.end_datetime;
    var week_view = req.body.week_view;

    var manvalues = [access_token, start_date, end_date, week_view];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        authenticateAdminAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {

                var start_date1 = new Date(start_date);
                var end_date1 = new Date(end_date);

                var sql = "SELECT * From tb_analytics WHERE DATE(`creation_datetime`)>= ? AND DATE(`creation_datetime`) <= ?"

                connection.query(sql, [start_date1, end_date1], function (err, data) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in Superadmin Analytics : ", err, data);
                        responses.sendError(res);
                        return;
                    }

                    data = _lodash.sortByOrder(data, ['creation_datetime'], ['desc']);

                   var response = [];
                    
                    data.forEach(function(value){
                        var responseObj = {
                            date: '',
                            value:[]
                        }
                        responseObj.date = value.creation_datetime;
                        responseObj.value = [value.total_task || 0,
                            value.completed_task || 0,
                            value.new_fleet || 0,
                            value.active_fleet || 0,
                            value.new_user || 0,
                            value.user_ids || '',
                            value.job_count || ''
                        ];
                        response.push(responseObj);
                    });

                    var chart_data = {
                        task : {
                            completed_task : [],
                            date:[],
                            total_task:[]
                        },
                        fleet:{
                            new_fleet:[],
                            active_fleet:[],
                            date:[]
                        },
                        job:{
                            user_ids:[],
                            job_count:[],
                            date:[]
                        },
                        user:{
                            new_user: [],
                            date:[]
                        }

                    }

                    var row_key = {
                        'total_task' : 0 ,
                        'completed_task' : 1,
                        'new_fleet': 2,
                        'active_fleet' : 3,
                        'new_user': 4,
                        'user_ids': 5,
                        'job_count': 6 
                    }

                    var lenIdx = response.length-1;
                    var resIdx = 0;
                    
                    do{
                        console.log('daily_view----------' , week_view);
                        week_view = parseInt(week_view);
                        if(week_view){
                            var dayIdx = 0;

                            if(response[lenIdx]) {
                                var date = new Date(response[lenIdx]['date']);
                                var numericDate = +new Date(date)
                                chart_data.task.date[resIdx] = numericDate;
                                chart_data.fleet.date[resIdx] = numericDate;
                                chart_data.job.date[resIdx] = numericDate;
                                chart_data.user.date[resIdx] = numericDate;

                                chart_data.task.total_task[resIdx] = 0;
                                chart_data.task.completed_task[resIdx] = 0;

                                chart_data.fleet.new_fleet[resIdx] = 0;
                                chart_data.fleet.active_fleet[resIdx] = 0;

                                chart_data.user.new_user[resIdx] = 0;

                                chart_data.job.job_count[resIdx] = 0;
                                chart_data.job.user_ids[resIdx] = '';
                                var dayCount = 0;


                                do {
                                    console.log('dayIdx--', dayIdx, 'lenIdx--', lenIdx ,'resIdx--', resIdx, 'date----', date, '---------------------');
                                    dayCount = dayCount + 1;
                                    chart_data.task.total_task[resIdx] = chart_data.task.total_task[resIdx] + response[lenIdx]['value'][row_key['total_task']];
                                    chart_data.task.completed_task[resIdx] = chart_data.task.completed_task[resIdx] + response[lenIdx]['value'][row_key['completed_task']];

                                    chart_data.fleet.new_fleet[resIdx] = chart_data.fleet.new_fleet[resIdx] + response[lenIdx]['value'][row_key['new_fleet']];
                                    chart_data.fleet.active_fleet[resIdx] = chart_data.fleet.active_fleet[resIdx] + response[lenIdx]['value'][row_key['active_fleet']];

                                    chart_data.user.new_user[resIdx] = chart_data.user.new_user[resIdx] + response[lenIdx]['value'][row_key['new_user']];

                                    chart_data.job.user_ids[resIdx] = chart_data.job.user_ids[resIdx] + response[lenIdx]['value'][row_key['user_ids']];
                                    var dayJobCount = [];
                                    if((response[lenIdx]['value'][row_key['job_count']]).length) {
                                        dayJobCount = (response[lenIdx]['value'][row_key['job_count']]).split(',');
                                    }
                                    console.log('day job count --', dayJobCount);

                                    chart_data.job.job_count[resIdx] = parseInt(chart_data.job.job_count[resIdx]) + parseInt(dayJobCount.length);

                                    dayIdx++;
                                    lenIdx--;

                                } while (dayIdx <= 6 && lenIdx >= 0);
                                console.log('day count --', dayCount);
                                console.log('job count --', chart_data.job.job_count[resIdx]);

                                chart_data.job.job_count[resIdx] = parseInt(chart_data.job.job_count[resIdx]/dayCount);
                            }

                        }else{
                            if(response[lenIdx]) {
                                console.log('lenIdx--', lenIdx , 'resIdx--', resIdx, '---------------------');

                                var date = new Date(response[lenIdx]['date']);
                                chart_data.task.date[resIdx] = +new Date(date);
                                chart_data.fleet.date[resIdx] = +new Date(date);
                                chart_data.job.date[resIdx] = +new Date(date);
                                chart_data.user.date[resIdx] =+new Date(date);

                                chart_data.task.total_task[resIdx] = response[lenIdx]['value'][row_key['total_task']];
                                chart_data.task.completed_task[resIdx] = response[lenIdx]['value'][row_key['completed_task']];

                                chart_data.fleet.new_fleet[resIdx] = response[lenIdx]['value'][row_key['new_fleet']];
                                chart_data.fleet.active_fleet[resIdx] = response[lenIdx]['value'][row_key['active_fleet']];

                                chart_data.user.new_user[resIdx] = response[lenIdx]['value'][row_key['new_user']];

                                chart_data.job.user_ids[resIdx] = response[lenIdx]['value'][row_key['user_ids']];
                                var dayJobCount = [];
                                if((response[lenIdx]['value'][row_key['job_count']]).length) {
                                    dayJobCount = (response[lenIdx]['value'][row_key['job_count']]).split(',');
                                }
                                chart_data.job.job_count[resIdx] = dayJobCount.length;

                                lenIdx--;
                            }
                        }

                        resIdx++;

                    }while(lenIdx >= 0);

                    var seriesOptions = []
                    seriesOptions = {
                        name: 'Analytics SuperAdmin',
                        data: chart_data
                    };

                    var responseFinal = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": seriesOptions
                    };
                    res.send(JSON.stringify(responseFinal));
                });

            }
        });
    }
};



exports.superadmin_analytics_past_insertion = function (req, res) {

    var pastDataRange = req.body.data_range;
    var initial = req.body.initial;
    pastDataRange = parseFloat(pastDataRange);
    initial = parseFloat(initial);

    console.log('-------------------------', initial, pastDataRange);

    for (var i=initial; i < pastDataRange ; i++ ) {

        //var sql_job = "SELECT COUNT(*) AS total_task, DATE(DATE_SUB(NOW(),INTERVAL " +i+" DAY)) as creation_datetime  FROM tb_jobs WHERE DATE(`creation_datetime`) <= DATE(DATE_SUB(NOW(),INTERVAL "+ i +" DAY)) AND DATE(`creation_datetime`) >= DATE(DATE_SUB(NOW(),INTERVAL " +(i+1) +" DAY))";
        //var sql_job_completed = "SELECT COUNT(*) as completed_task FROM tb_jobs WHERE DATE(`completed_datetime`)  <= DATE(DATE_SUB(NOW(),INTERVAL "+ i +" DAY)) AND DATE(`completed_datetime`)  >= DATE(DATE_SUB(NOW(),INTERVAL " +(i+1) +" DAY))";
        //var sql_fleet = "SELECT COUNT(*) as new_fleet FROM tb_fleets where (DATE(`creation_datetime`) <= DATE(DATE_SUB(NOW(),INTERVAL "+ i +" DAY)) AND DATE(`creation_datetime`) >= DATE(DATE_SUB(NOW(),INTERVAL " +(i+1) +" DAY))) AND `registration_status` = 1";
        //var sql_users = "SELECT COUNT(*) as new_user From tb_users where (DATE(`creation_datetime`) <= DATE(DATE_SUB(NOW(),INTERVAL "+ i +" DAY)) AND DATE(`creation_datetime`) >= DATE(DATE_SUB(NOW(),INTERVAL " +(i+1) +" DAY))) AND `internal_user` = 0 AND is_dispatcher = 0";


        var sql_job = "SELECT COUNT(tb_jobs.job_id) AS total_task,  DATE(DATE_SUB(NOW(),INTERVAL " +i+" DAY)) as creation_datetime FROM tb_jobs left join tb_users on tb_jobs.user_id=tb_users.user_id  WHERE DATE(tb_jobs.`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL "+ i +" DAY)) and tb_users.internal_user=0";
        var sql_job_completed = "SELECT COUNT(tb_jobs.job_id) as completed_task FROM tb_jobs left join tb_users on tb_jobs.user_id=tb_users.user_id WHERE DATE(tb_jobs.completed_datetime) = DATE(DATE_SUB(NOW(),INTERVAL " + i +" DAY)) and tb_users.internal_user=0";
        var sql_fleet_new = "SELECT COUNT(tb_fleets.fleet_id) as new_fleet FROM tb_fleets left join tb_users on tb_fleets.user_id=tb_users.user_id  where DATE(tb_fleets.creation_datetime) = DATE(DATE_SUB(NOW(),INTERVAL " + i +" DAY)) and tb_fleets.registration_status and tb_users.internal_user=0";
        //var sql_fleet_active = "SELECT COUNT(tb_fleets.fleet_id) as active_fleet FROM tb_fleets left join tb_users on tb_fleets.user_id=tb_users.user_id  where DATE(tb_fleets.location_update_datetime) = DATE(DATE_SUB(NOW(),INTERVAL " +i+ " DAY)) and tb_fleets.registration_status and tb_users.internal_user=0";
        var sql_users = "SELECT COUNT(*) as new_user From tb_users where DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL " + i +" DAY)) && `internal_user` = 0 && is_dispatcher = 0";

        console.log('---------------------',sql_job_completed);
        console.log('--------------i---------------',i);

        async.parallel([
                function (callback) {
                    connection.query(sql_job, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                },
                function (callback) {
                    connection.query(sql_fleet_new, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                },
                function (callback) {
                    connection.query(sql_users, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                },
                function (callback) {
                    connection.query(sql_job_completed, function (err, result) {
                        if (result.length > 0) {
                            callback(null, result);
                        } else {
                            callback(null, []);
                        }
                    });
                }
               ],
            function (err, asyncResults) {
                console.log("Response=========", asyncResults);

                if (asyncResults.length) {

                    var data = {};
                    data = {
                        total_task: 0,
                        completed_task: 0,
                        new_fleet: 0,
                        active_fleet: 0,
                        new_user: 0,
                        user_ids: '',
                        job_count: ''
                    }

                    if (asyncResults[0] && asyncResults[0][0]) {
                        if (asyncResults[0][0].hasOwnProperty('total_task')) {
                            data.total_task = asyncResults[0][0]['total_task'];
                        }

                        if (asyncResults[0][0].hasOwnProperty('creation_datetime')) {
                            data.creation_datetime = asyncResults[0][0]['creation_datetime'];
                        }
                    }

                    if (asyncResults[1] && asyncResults[1][0]) {
                        if (asyncResults[1][0].hasOwnProperty('new_fleet')) {
                            data.new_fleet = asyncResults[1][0]['new_fleet'];
                        }
                    }

                    if (asyncResults[2] && asyncResults[2][0]) {
                        if (asyncResults[2][0].hasOwnProperty('new_user')) {
                            data.new_user = asyncResults[2][0]['new_user'];
                        }
                    }

                    //if (asyncResults[3] && asyncResults[3][0]) {
                    //    var user_id_array = [];
                    //    var user_taskcount_array = [];
                    //    (asyncResults[3]).forEach(function (value, key) {
                    //        if (value.hasOwnProperty('user_id')) {
                    //            user_id_array.push(value.user_id);
                    //        }
                    //
                    //        if (value.hasOwnProperty('job_count')) {
                    //            user_taskcount_array.push(value.job_count);
                    //        }
                    //
                    //
                    //    });
                    //
                    //    data.user_ids = user_id_array.join();
                    //    data.job_count = user_taskcount_array.join();
                    //}

                    if (asyncResults[3] && asyncResults[3][0]) {
                        if (asyncResults[3][0].hasOwnProperty('completed_task')) {
                            data.completed_task = asyncResults[3][0]['completed_task'];
                        }
                    }
                }

                var insert_query = 'INSERT INTO tb_analytics (total_task, creation_datetime, completed_task, new_fleet, active_fleet, new_user, user_id, job_count) ' +
                    'VALUES (?,?,?,?,?,?,?,?) ON DUPLICATE ' +
                    'KEY UPDATE  total_task = values(total_task), ' +
                    'completed_task=VALUES(completed_task), ' +
                    'new_fleet=VALUES(new_fleet), ' +
                    'active_fleet=VALUES(active_fleet), ' +
                    'new_user=VALUES(new_user), ' +
                    'user_id=VALUES(user_id), ' +
                    'job_count=VALUES(job_count)';

                connection.query(insert_query,
                    [data.total_task, data.creation_datetime, data.completed_task, data.new_fleet, data.active_fleet, data.new_user, data.user_ids, data.job_count],
                    function (err, result) {
                        if (err) {
                            console.log('logging error for tb_analytics insertion---', err);
                            return;
                        }
                    });
            }
        );
    }
    res.send(JSON.stringify('done'));

};


