var request = require('request');
var logging = require('./logging');
var constants = require('./constants');
var nodemailer = require("nodemailer");
var commonFunc = require('./commonfunction');
var mongo = require('./mongo');
var debugging_enabled = true;
var smtpTransport = undefined;
var client = undefined;
var plivoClient = undefined;
var moment = require('moment');
var parseFormat = require('moment-parseformat');
var phantom = require("phantom");
var async = require('async');
var md5 = require('MD5');
var autoAssign = require('./auto-assignment');
var socketResponse = require('./socketResponses');
var extras = require('./extras');
var jobs = require('./jobs');
/*
 * --------------------------------------
 * CHECK EACH ELEMENT OF ARRAY FOR BLANK
 * --------------------------------------
 */
exports.checkBlank = function (arr, req, res) {
    var arrlength = arr.length;
    for (var i = 0; i < arrlength; i++) {
        if (arr[i] === undefined) {
            arr[i] = "";
        } else {
            arr[i] = arr[i];
        }
        arr[i] = arr[i].toString().trim();
        if (arr[i] === '' || arr[i] === "" || arr[i] === undefined) {
            return 1;
            break;
        }
    }
    return 0;
};

exports.isValidLng = function (point) {
    var regex = /^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/g;
    return (regex.test(point))
}
exports.isValidLat = function (point) {
    var regex = /^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/g;
    return (regex.test(point))
}
exports.isValidPoints = function (points) {
    var regex = /^([-+]?)([\d]{1,2})(((\.)(\d+)(,)))(\s*)(([-+]?)([\d]{1,3})((\.)(\d+))?)$/g;
    return (regex.test(points))
}

exports.validateEmail = function (email) {
    if (email == '') {
        return true
    }
    var regex = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;
    if (regex.test(email)) {
        return true;
    } else {
        return false;
    }
}
Number.prototype.padLeft = function (base, chr) {
    var len = (String(base || 10).length - String(this).length) + 1;
    return len > 0 ? new Array(len).join(chr || '0') + this : this;
}
exports.formatDate = function (date) {
    var d = new Date(date),
        dformat = [d.getFullYear(),
                (d.getMonth() + 1).padLeft(),
                d.getDate().padLeft()
            ].join('-') +
            ' ' +
            [d.getHours().padLeft(),
                d.getMinutes().padLeft(),
                d.getSeconds().padLeft()].join(':');
    return dformat;
}
/*
 * ------------------
 * CHECK EXPIRY DATE
 * ------------------
 */
exports.checkAccountExpiry = function (userID, callback) {
    var sql = "SELECT `is_active`,`is_dispatcher`,`dispatcher_user_id` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
    connection.query(sql, [userID], function (err, result) {
        if (result && result.length > 0) {
            if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                var sql = "SELECT `is_active`,`is_dispatcher` FROM `tb_users` WHERE `user_id`=? LIMIT 1";
                connection.query(sql, [result[0].dispatcher_user_id], function (err, dispatcher_user_result) {
                    if (dispatcher_user_result[0].is_active == constants.userActiveStatus.INACTIVE) {
                        callback(1);
                    } else {
                        callback(0);
                    }
                });
            } else if (result[0].is_active == constants.userActiveStatus.INACTIVE) {
                callback(1);
            }
            else {
                return callback(0)
            }
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * GET ALL ACTIVE FLEET OF USERS
 * -----------------------------------------------
 */
exports.getAllActiveFleets = function (user_id, callback) {
    var sql = "SELECT * FROM `tb_fleets` WHERE `user_id`=? AND `is_active` = ? AND `is_available` = ? AND `is_deleted` = ?";
    connection.query(sql, [user_id, constants.userActiveStatus.ACTIVE, constants.availableStatus.AVAILABLE, constants.userDeleteStatus.NO], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * GET ALL  FLEET OF USERS
 * -----------------------------------------------
 */
exports.getAllFleets = function (user_id, callback) {
    var sql = "SELECT * FROM `tb_fleets` WHERE `user_id`=? AND `is_active` = ? AND `is_deleted` = ?";
    connection.query(sql, [user_id, constants.userActiveStatus.ACTIVE, constants.userDeleteStatus.NO], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback([]);
        }
    });
};
/*
 * -----------------------------------------------
 * GET ALL  NEAREST FREE FLEETS OF USER
 * -----------------------------------------------
 */
exports.getAllNearestAvailableAgents = function (data, callback) {
    var job_statues = constants.jobStatus.DELETED + ',' + constants.jobStatus.FAILED + ',' + constants.jobStatus.ENDED + ',' + constants.jobStatus.CANCEL;
    if (data.team_id == 0) {
        var sql = "SELECT fleet.*," +
            "(select count(job.job_id) from `tb_jobs` job " +
            "where job.`fleet_id` = fleet.`fleet_id` and " +
            "job.`job_time`>? and job.`job_time`<? and " +
            "job.`job_status` not in (" + job_statues + ") ) as tasks " +
            "FROM `tb_fleets` fleet WHERE fleet.`user_id`=? AND fleet.`is_active` = ? AND fleet.`is_deleted` = ? AND fleet.`is_available`=? AND fleet.`registration_status`=? ";
        connection.query(sql, [data.start_date, data.end_date, data.user_id, constants.userActiveStatus.ACTIVE, constants.userDeleteStatus.NO, constants.availableStatus.AVAILABLE, constants.userVerificationStatus.VERIFY], function (err, result) {
            if (result && result.length > 0) {
                return callback(result);
            } else {
                return callback([]);
            }
        });
    } else if (data.team_id > 0) {
        var sql = "SELECT tft.*,tm.team_id, " +
            "(select count(job.job_id) from `tb_jobs` job where job.`fleet_id` = tft.`fleet_id` and " +
            "job.`job_time`>? and job.`job_time`<? and " +
            "job.`job_status` not in (" + job_statues + ") ) as tasks " +
            "FROM `tb_fleet_teams` ftm " +
            "LEFT JOIN `tb_teams` tm ON tm.`team_id` = ftm.`team_id` " +
            "LEFT JOIN `tb_fleets` tft ON tft.`fleet_id` = ftm.`fleet_id`  " +
            "WHERE tm.`team_id` = ? AND tft.`is_active` = ? AND tft.`is_deleted`=? AND tft.`is_available`=? AND tft.`registration_status`=? ";
        connection.query(sql, [data.start_date, data.end_date, data.team_id, constants.userActiveStatus.ACTIVE, constants.userDeleteStatus.NO, constants.availableStatus.AVAILABLE, constants.userVerificationStatus.VERIFY], function (err, result) {
            if (result && result.length > 0) {
                return callback(result);
            } else {
                return callback([]);
            }
        });
    } else {
        return callback([]);
    }
};
/*
 * -----------------------------------------------
 * GET ALL USERS AND THEIR DISPATCHERS
 * -----------------------------------------------
 */
exports.getAllUserAndTheirDispatchers = function (user_id, callback) {
    var sql = "SELECT * FROM `tb_users` WHERE `user_id`=? LIMIT 1";
    connection.query(sql, [user_id], function (err, result) {
        if (result && result.length > 0) {
            if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {

                var sql = "SELECT * FROM `tb_users` WHERE `dispatcher_user_id`=? || `user_id`=?";
                connection.query(sql, [result[0].dispatcher_user_id, result[0].dispatcher_user_id], function (err, dispatcher_user_result) {
                    if (dispatcher_user_result.length > 0) {
                        return callback(dispatcher_user_result);
                    } else {
                        return callback(0);
                    }
                });
            } else {
                var sql = "SELECT * FROM `tb_users` WHERE `dispatcher_user_id`=? || `user_id`=?";
                connection.query(sql, [user_id, user_id], function (err, dispatcher_user_result) {
                    if (dispatcher_user_result.length > 0) {
                        return callback(dispatcher_user_result);
                    } else {
                        return callback(0);
                    }
                });
            }
        } else {
            return callback(0);
        }
    });

};
/*
 * --------------------------------------------------------------
 * GET ALL USERS AND THEIR DISPATCHERS WITH VIEW TASK PERMISSIONS
 * --------------------------------------------------------------
 */
exports.getAllUserAndTheirDispatchersWithPermission = function (user_id, team_id, callback) {
    var sql = "SELECT * FROM `tb_users` WHERE `user_id`=? LIMIT 1";
    connection.query(sql, [user_id], function (err, owner) {
        if (owner.length > 0) {
            if (team_id == 0) {
                var sql = "SELECT u.*,d.view_task FROM `tb_users` u left join tb_dispatcher_permissions d " +
                    "on u.user_id = d.dispatcher_id  WHERE u.`dispatcher_user_id`=? AND d.view_task = ?";
                connection.query(sql, [user_id, 1], function (err, dispatcher_having_task) {
                    if (dispatcher_having_task.length > 0) {
                        owner = owner.concat(dispatcher_having_task);
                        return callback(owner);
                    } else {
                        return callback(owner);
                    }
                });
            } else {
                var sql = "SELECT u.* FROM `tb_users` u left join tb_dispatcher_teams d " +
                    "on u.user_id = d.dispatcher_id  WHERE d.team_id = ?";
                connection.query(sql, [team_id], function (err, dispatcher_teams) {
                    if (dispatcher_teams.length > 0) {
                        owner = owner.concat(dispatcher_teams);
                        return callback(owner);
                    } else {
                        return callback(owner);
                    }
                });
            }
        } else {
            return callback([]);
        }
    });
};
/*
 * -----------------------------------------------
 * GET ALL FLEET OF USERS
 * -----------------------------------------------
 */
exports.getFleetAvalability = function (user_id, fleet_id, callback) {
    var sql = "SELECT * FROM `tb_fleets` WHERE `user_id`=? AND `fleet_id`=?";
    connection.query(sql, [user_id, fleet_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------
 * GET ALL REGISTERED FLEETS OF USER
 * -----------------------------------
 */
exports.getRegisteredFleet = function (user_id, fleet_id, callback) {
    fleet_id = fleet_id == '' ? null : fleet_id;
    var sql = "SELECT * FROM `tb_fleets` WHERE `user_id`=? AND `fleet_id` IN (" + fleet_id + ") AND `registration_status` = ?";
    connection.query(sql, [user_id, constants.userVerificationStatus.VERIFY], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEETS AND TEAMID
 * -----------------------------------------------
 */
exports.authenticateFleetIDAndTeamID = function (user_id, team_id, fleets, callback) {
    fleets = fleets.length > 0 ? fleets.toString() : 0;
    var sql = "SELECT ftm.*,ft.* FROM `tb_fleet_teams` ftm " +
        " inner join tb_fleets ft on ft.fleet_id = ftm.fleet_id " +
        " WHERE ftm.`user_id`=? AND ftm.`team_id` IN ('" + team_id + "') AND ftm.`fleet_id` IN (" + fleets + ")";
    connection.query(sql, [user_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE USER ACCESS TOKEN
 * -----------------------------------------------
 */
exports.authenticateUserAccessToken = function (userAccessToken, callback) {
    var sql = "SELECT * FROM `tb_users` WHERE `access_token`=? LIMIT 1";
    connection.query(sql, [userAccessToken], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE USER ACCESS TOKEN AND USER_ID
 * -----------------------------------------------
 */
exports.authenticateUserAccessTokenAndUserId = function (userAccessToken, user_id, callback) {
    var sql = "SELECT * FROM `tb_users` WHERE `access_token`=? and `user_id`=? LIMIT 1";
    connection.query(sql, [userAccessToken, user_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE USER EMAIL
 * -----------------------------------------------
 */
exports.authenticateUserEmail = function (email, callback) {
    var sql = "SELECT * FROM `tb_users` WHERE `email`=? LIMIT 1";
    connection.query(sql, [email], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET EMAIL
 * -----------------------------------------------
 */
exports.authenticateFleetEmail = function (email, callback) {
    var sql = "SELECT * FROM `tb_fleets` WHERE `email`=? LIMIT 1";
    connection.query(sql, [email], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE TOKAN VERIFY
 * -----------------------------------------------
 */
exports.authenticateUserToken = function (token, callback) {
    var sql = "SELECT * FROM `tb_users` WHERE `verification_token`=? LIMIT 1";
    connection.query(sql, [token], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FORGOT PASSWORD TOKEN
 * -----------------------------------------------
 */
exports.authenticateForgotToken = function (tokan, type, callback) {
    if (type == 'changeF') {
        var sql = "SELECT * FROM `tb_fleets` WHERE `verification_token`=? LIMIT 1";
        connection.query(sql, [tokan], function (err, result) {
            if (result && result.length > 0) {
                return callback(result);
            } else {
                return callback(0);
            }
        });
    }
    else if (type == 'changeU') {
        var sql = "SELECT * FROM `tb_users` WHERE `verification_token`=? LIMIT 1";
        connection.query(sql, [tokan], function (err, result) {
            if (result && result.length > 0) {
                return callback(result);
            } else {
                return callback(0);
            }
        });
    }
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET ACCESSTOKEN
 * -----------------------------------------------
 */
exports.authenticateFleetAcessToken = function (userAccessToken, callback) {
    var sql = "SELECT usr.has_routing,usr.has_invoicing_module,usr.billing_plan,usr.layout_type," +
        " ft.*, TIMESTAMPDIFF(SECOND,ft.`location_update_datetime`,NOW()) as `last_updated_location_time` " +
        " FROM `tb_fleets` ft " +
        " INNER JOIN tb_users usr ON ft.user_id=usr.user_id " +
        " WHERE ft.`access_token`=? LIMIT 1";
    connection.query(sql, [userAccessToken], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET ACCESSTOKEN AND FLEET_ID
 * -----------------------------------------------
 */
exports.authenticateFleetAccessTokenANDFleetID = function (token, id, callback) {
    var sql = "SELECT *, TIMESTAMPDIFF(SECOND,`location_update_datetime`,NOW()) as `last_updated_location_time` " +
        " FROM `tb_fleets` " +
        " WHERE `access_token`=? AND `fleet_id`=? LIMIT 1";
    connection.query(sql, [token, id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE JOB HASH
 * -----------------------------------------------
 */
exports.authenticateJobHash = function (job_hash, callback) {
    var sql = "SELECT * FROM `tb_jobs` WHERE `job_hash`=? LIMIT 1";
    connection.query(sql, [job_hash], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * GET CUSTOMER DETAILS
 * -----------------------------------------------
 */
exports.getCustomeDetails = function (customer_id, callback) {
    var sql = "SELECT * FROM `tb_customers` WHERE `customer_id`=? LIMIT 1";
    connection.query(sql, [customer_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE CUSTOMER AND USER ID
 * -----------------------------------------------
 */
exports.authenticateCustomerIdAndUserID = function (customer_id, user_id, callback) {
    var sql = "SELECT * FROM `tb_customers` WHERE `customer_id`=? AND `user_id`=? LIMIT 1";
    connection.query(sql, [customer_id, user_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET ID AND JOB ID
 * -----------------------------------------------
 */
exports.authenticateFleetIdAndJobId = function (fleet_id, job_id, callback) {
    var sql = "SELECT job.* FROM `tb_jobs` job  " +
        "WHERE job.`fleet_id`=? and job.`job_id`=? and job.`job_status`<>? LIMIT 1";
    connection.query(sql, [fleet_id, job_id, constants.jobStatus.DELETED], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE AUTO ASSIGN TASK
 * -----------------------------------------------
 */
exports.authenticateAutoAssignTask = function (fleet_id, fleet_name, job_id, job_status, callback) {
    var sql = "SELECT job.* FROM `tb_jobs` job  " +
        "WHERE job.`fleet_id` IS NULL and job.`job_id`=? and job.`job_status`<>? and job.job_status = ? and job.`auto_assignment`= ? LIMIT 1";
    connection.query(sql, [job_id, constants.jobStatus.DELETED, constants.jobStatus.UNASSIGNED, constants.autoAssign.YES], function (err, result) {
        if (result && result.length > 0) {
            if (job_status != constants.jobStatus.DECLINE) {
                var sql = "UPDATE tb_jobs SET `job_status`=?,`fleet_id`=? WHERE `pickup_delivery_relationship` = ?";
                connection.query(sql, [constants.jobStatus.ACCEPTED, fleet_id, result[0].pickup_delivery_relationship], function (err, result) {
                    var sch = "UPDATE tb_schedules SET `is_processed`=1 WHERE `job_id` = ? LIMIT 1";
                    connection.query(sch, [job_id], function (err, update_schedules) {
                        autoAssign.cancelJobSchedule(job_id);
                        autoAssign.cancelDisableAutoAssignSchedule(job_id);
                        return callback(1);
                    })
                })
            } else {
                var sql = "INSERT into `tb_declined_tasks` (`job_id`,`fleet_id`,`user_id`) VALUES (?,?,?)";
                connection.query(sql, [job_id, fleet_id, result[0].user_id], function (err, insert_declined) {
                    commonFunc.setTaskHistory(fleet_id, constants.taskHistoryType.TASK_PUSH_NOTIFY, job_id, fleet_name + " declined the task.");
                    autoAssign.cancelJobSchedule(job_id);
                    autoAssign.processAgentsOneAtATime();
                    return callback(0);
                });
            }
        } else {
            return callback(1);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET ID AND USER ID
 * -----------------------------------------------
 */
exports.authenticateFleetIdAndUserId = function (fleet_id, user_id, callback) {
    if (fleet_id) {
        var sql = "SELECT * FROM `tb_fleets` WHERE `fleet_id`=? and `user_id`=? and `is_active`=1 LIMIT 1";
        connection.query(sql, [fleet_id, user_id], function (err, result) {
            if (result && result.length > 0) {
                return callback(result);
            } else {
                return callback(0);
            }
        });
    } else {
        callback(1);
    }
};
/*
 * -----------------------------------------------
 * AUTHENTICATE DEACTIVATE FLEET ID AND USER ID
 * -----------------------------------------------
 */
exports.authenticateDeactivateFleetIdAndUserId = function (fleet_id, user_id, callback) {
    var sql = "SELECT * FROM `tb_fleets` WHERE `fleet_id`=? AND `user_id`=? ";
    connection.query(sql, [fleet_id, user_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET ID AND DISPATCHER ID
 * -----------------------------------------------
 */
exports.authenticateFleetIdAndDispatcherId = function (fleet_id, dispatcher_id, callback) {
    var sql = "SELECT * FROM `tb_fleets` WHERE `fleet_id`=? and `dispatcher_id`=? LIMIT 1";
    connection.query(sql, [fleet_id, dispatcher_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET ID AND APPOINTMENT FIELD DATA ID
 * -----------------------------------------------
 */
exports.authenticateAppointmentFieldIdANDFleetID = function (appointment_field_data_id, fleet_id, callback) {
    var sql = "SELECT jobs.`fleet_id`,aptData.* FROM `tb_jobs` jobs INNER JOIN `tb_appointment_field_data` aptData ON aptData.job_id=jobs.job_id " +
        "WHERE aptData.appointment_field_data_id=? AND jobs.fleet_id=?";
    connection.query(sql, [appointment_field_data_id, fleet_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET ID AND JOB_IMAGES
 * -----------------------------------------------
 */
exports.authenticateJobImageANDFleetID = function (job_image_id, fleet_id, callback) {
    var sql = "SELECT jobs.`fleet_id`,job_image.* FROM `tb_jobs` jobs INNER JOIN `tb_job_images` job_image ON job_image.job_id=jobs.job_id " +
        "WHERE job_image.jobs_image_id=? AND jobs.fleet_id=?";
    connection.query(sql, [job_image_id, fleet_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE DISPATCHER ID AND USER ID
 * -----------------------------------------------
 */
exports.authenticateDispatcherIdAndUserId = function (dispatcher_id, user_id, callback) {
    var sql = "SELECT * FROM `tb_users` WHERE `user_id`=? and `dispatcher_user_id`=? LIMIT 1";
    connection.query(sql, [dispatcher_id, user_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------
 * TOTAL NUMBER OF TASKS OF MONTH
 * -----------------------------------
 */
exports.numberOfTasksForCurrentMonth = function (user_id, callback) {
    var sql = "SELECT * FROM `tb_users_monthly_data` " +
        " WHERE `user_id`=? AND `month` = EXTRACT( YEAR_MONTH FROM NOW() )";
    connection.query(sql, [user_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback([]);
        }
    });
};
/*
 * -----------------------------------
 * INSERT OR UPDATE TASK TO MONTH
 * -----------------------------------
 */
exports.insertORupdateTaskForCurrentMonth = function (user_id, isUpdate) {
    var sql = "SELECT * from `tb_users_monthly_data` " +
        " WHERE " +
        " `month` = EXTRACT( YEAR_MONTH FROM NOW() ) AND  `user_id`=? ";
    connection.query(sql, [user_id], function (err, tb_users_monthly_data_result) {
        if (tb_users_monthly_data_result && tb_users_monthly_data_result.length > 0) {
            if (isUpdate) {
                var sql = "UPDATE `tb_users_monthly_data` SET `tasks_count` = `tasks_count` + 1 " +
                    " WHERE `user_id`=? and `month`=  EXTRACT( YEAR_MONTH FROM NOW())";
                connection.query(sql, [user_id], function (err, tb_users_monthly_data_result) {
                })
            }
        } else {
            var invoice_bindparms = " " + isUpdate + ",0, " + user_id + ",EXTRACT( YEAR_MONTH FROM NOW()),(select num_tasks from tb_users where user_id=" + user_id + ")," +
                "(select num_fleets from tb_users where user_id=" + user_id + ")";
            var sql = "INSERT INTO `tb_users_monthly_data` " +
                " (`tasks_count`,`fleets_count`,`user_id`,`month`,`num_tasks`,`num_fleets`) VALUES (" + invoice_bindparms + ")";
            connection.query(sql, function (err, tb_invoice_history_update_result) {
                console.log(err);
            })
        }
    });
};
/*
 * --------------------------------
 * AUTHENTICATE USER ID AND JOB ID
 * --------------------------------
 */
exports.authenticateUserIdAndJobId = function (user_id, job_id, callback) {
    var sql = "SELECT job.* FROM `tb_jobs` job " +
        "WHERE job.`user_id`=? and job.`job_id`=? and job.`job_status`<>? LIMIT 1";
    connection.query(sql, [user_id, job_id, constants.jobStatus.DELETED], function (err, result) {
        if (result && result.length > 0) {
            if (result[0].job_pickup_datetime && result[0].job_pickup_datetime != null && result[0].job_pickup_datetime != '0000-00-00 00:00:00') {
                result[0].job_pickup_datetime = result[0].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
            }
            if (result[0].job_delivery_datetime && result[0].job_delivery_datetime != null && result[0].job_delivery_datetime != '0000-00-00 00:00:00') {
                result[0].job_delivery_datetime = result[0].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
            }
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * ----------------------------------------
 * AUTHENTICATE USER ID AND CUSTOM ORDER ID
 * ----------------------------------------
 */
exports.authenticateUserIdAndOrderId = function (user_id, order_id, callback) {
    var sql = "SELECT job.* FROM `tb_jobs` job " +
        "WHERE job.`user_id`=? and job.`order_id`=?";
    connection.query(sql, [user_id, order_id], function (err, result) {
        if (result && result.length > 0) {
            var job_ids = [];
            result.forEach(function (job) {
                job_ids.push(job.job_id)
            });
            fetchTaskData(job_ids, function (tasks) {
                callback(tasks);
            })
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------
 * AUTHENTICATE USER ID AND JOB TOKEN
 * -----------------------------------
 */
exports.authenticateUserIdAndJobToken = function (user_id, job_token, callback) {
    var sql = "SELECT job.* FROM `tb_jobs` job " +
        "WHERE job.`user_id`=? AND job.`pickup_delivery_relationship`=? AND job.`job_status`<>? order by job_id";
    connection.query(sql, [user_id, job_token, constants.jobStatus.DELETED], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------
 * GET BOTH TASKS FROM SINGLE TASK ID
 * -----------------------------------
 */
exports.getBothTasksFromSingleID = function (job_id, callback) {
    commonFunc.getJobDetailsFromJobID(job_id, function (getJobDetailsFromJobIDResult) {
        if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult.length) {
            var job_result = getJobDetailsFromJobIDResult[0];
            commonFunc.authenticateUserIdAndJobToken(job_result.user_id, job_result.pickup_delivery_relationship, function (data) {
                if (data.length) {
                    var job_ids = [];
                    data.forEach(function (job) {
                        job_ids.push(job.job_id)
                    });
                    fetchTaskData(job_ids, function (tasks) {
                        callback(tasks);
                    })
                } else {
                    callback(0)
                }
            });
        } else {
            callback(0)
        }
    })
};
function fetchTaskData(jobs, callback) {
    async.map(jobs, function (jobID, callback) {
        module.exports.getJobDetailsFromJobID(jobID, function (jobs) {
            module.exports.getTaskHistory(jobs[0].job_id, {}, function (getTaskHistoryResult) {
                mongo.getOptionalFieldsForTask(jobs[0].user_id, jobs[0].job_id, {}, function (getOptionalFieldsForTaskResult) {
                    jobs[0].task_history = getTaskHistoryResult.task_history;
                    jobs[0].task_history.forEach(function (taskhistory) {
                        if (taskhistory.type == constants.taskHistoryType.SIGN_IMAGE_ADDED || taskhistory.type == constants.taskHistoryType.SIGN_IMAGE_UPDATED) {
                            jobs[0].sign_image = taskhistory.description
                        }
                    })
                    jobs[0].job_state = constants.jobStatusValue[parseInt(jobs[0].job_status)];
                    jobs[0].job_token = jobs[0].pickup_delivery_relationship;
                    jobs[0].custom_fields = getOptionalFieldsForTaskResult.fields.custom_field;
                    if (jobs[0].customer_username == "dummy") jobs[0].customer_username = "";
                    if (jobs[0].customer_email == "dummy") jobs[0].customer_email = "";
                    if (jobs[0].customer_phone == "dummy") jobs[0].customer_phone = "";
                    if (jobs[0].job_description == null) jobs[0].job_description = "";
                    jobs[0].job_state = constants.jobStatusValue[parseInt(jobs[0].job_status)];
                    if (jobs[0].started_datetime == "0000-00-00 00:00:00") jobs[0].started_datetime = "";
                    if (jobs[0].completed_datetime == '0000-00-00 00:00:00') jobs[0].completed_datetime = "";
                    if (jobs[0].arrived_datetime == "0000-00-00 00:00:00") jobs[0].arrived_datetime = "";
                    if (jobs[0].acknowledged_datetime == "0000-00-00 00:00:00") jobs[0].acknowledged_datetime = "";
                    if (jobs[0].completed_datetime) jobs[0].completed_datetime = (moment(commonFunc.convertTimeIntoLocal(jobs[0].completed_datetime, jobs[0].timezone)).format('YYYY-MM-DD HH:mm:s')).toString();
                    if (jobs[0].started_datetime) jobs[0].started_datetime = (moment(commonFunc.convertTimeIntoLocal(jobs[0].started_datetime, jobs[0].timezone)).format('YYYY-MM-DD HH:mm:s')).toString();
                    if (jobs[0].arrived_datetime) jobs[0].arrived_datetime = (moment(commonFunc.convertTimeIntoLocal(jobs[0].arrived_datetime, jobs[0].timezone)).format('YYYY-MM-DD HH:mm:s')).toString();
                    if (jobs[0].acknowledged_datetime) jobs[0].acknowledged_datetime = (moment(commonFunc.convertTimeIntoLocal(jobs[0].acknowledged_datetime, jobs[0].timezone)).format('YYYY-MM-DD HH:mm:s')).toString();
                    if ((jobs[0].started_datetime != '') && (jobs[0].completed_datetime != '')) {
                        var started_time = new Date(jobs[0].started_datetime);
                        var completed_datetime = new Date(jobs[0].completed_datetime);
                        var timeDifference = (completed_datetime.getTime() - started_time.getTime()) / 1000; // time in Seconds
                        timeDifference = Math.round(timeDifference / 60) // time in Minutes
                        jobs[0].total_time_spent_at_task_till_completion = timeDifference;
                    }
                    jobs[0].total_distance_travelled = commonFunc.metersToUnit(jobs[0].total_distance_travelled, jobs[0].distance_in);
                    jobs[0].job_time = moment(jobs[0].job_time).format('YYYY-MM-DD hh:mm:s');
                    jobs[0].tracking_link = config.get('webAppLink') + "/tracking/index.html?jobID=" + jobs[0].job_hash;
                    jobs[0].job_pickup_datetime = moment(jobs[0].job_pickup_datetime).format('MM/DD/YYYY hh:mm a');
                    jobs[0].job_delivery_datetime = moment(jobs[0].job_delivery_datetime).format('MM/DD/YYYY hh:mm a');
                    callback(null, jobs);
                });
            });
        })
    }, function (err, results) {
        var success = [];
        results.forEach(function (job) {
            success = success.concat(job);
        })
        callback(success);
    });
}

/*
 * -----------------------------------------------
 * AUTHENTICATE FLEET ID AND JOB TOKEN
 * -----------------------------------------------
 */
exports.authenticateFleetIdAndJobToken = function (user_id, fleet_id, job_token, callback) {
    var sql = "SELECT job.* FROM `tb_jobs` job " +
        "WHERE job.`user_id`=? AND job.`pickup_delivery_relationship`=? AND job.`fleet_id`=? ";
    connection.query(sql, [user_id, job_token, fleet_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE USER ID AND TEAM ID
 * -----------------------------------------------
 */
exports.authenticateUserIdAndTeamId = function (user_id, team_id, callback) {
    var sql = "SELECT * FROM `tb_teams` WHERE `user_id`=? and `team_id`=? LIMIT 1";
    connection.query(sql, [user_id, team_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
exports.authenticateDispatcherIdAndTeamId = function (disp_id, team_id, callback) {
    var sql = "SELECT * FROM `tb_dispatcher_teams` WHERE `dispatcher_id`=? and `team_id`=? LIMIT 1";
    connection.query(sql, [disp_id, team_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * AUTHENTICATE TEAM AND FLEET AND DISPATCHER
 * -----------------------------------------------
 */
exports.authenticateTeamIdWithFleetAndDispatcher = function (user_id, team_id, callback) {
    var sql = "SELECT * FROM `tb_fleet_teams` WHERE `user_id`=? and `team_id`=? " +
        "UNION " +
        "SELECT * FROM `tb_dispatcher_teams` WHERE `user_id`=? and `team_id`=?";
    connection.query(sql, [user_id, team_id, user_id, team_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
exports.authenticateCustomerWithUser = function (user_id, phone, callback) {
    var sql = "SELECT * " +
        "FROM `tb_customers` WHERE `user_id`=? AND `customer_phone`=? LIMIT 1";
    connection.query(sql, [user_id, phone], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
exports.findCustomer = function (user_id, phone, callback) {
    var sql = "SELECT * " +
        "FROM `tb_customers` WHERE `user_id`=? AND `customer_phone` LIKE '%" + phone + "%' ";
    connection.query(sql, [user_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback([]);
        }
    });
};
exports.authenticateTeamNameWithUser = function (team_name, user_id, callback) {
    var sql = "SELECT * " +
        "FROM `tb_teams` WHERE `user_id`=? AND `team_name`=? LIMIT 1";
    connection.query(sql, [user_id, team_name], function (err, result) {
        //logging.logDatabaseQuery("Authenticating user.", err, result, null);
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
exports.checkDispatcherPermissions = function (dispatcher_id, callback) {
    var sql = "SELECT *,(SELECT GROUP_CONCAT(t_d_t.`team_id` SEPARATOR ',') " +
        " FROM `tb_dispatcher_teams` t_d_t WHERE t_d_t.dispatcher_id = t_d_p.`dispatcher_id` ) as teams " +
        " FROM `tb_dispatcher_permissions` t_d_p WHERE t_d_p.`dispatcher_id` = ? LIMIT 1";
    connection.query(sql, [dispatcher_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
exports.getAllFleetOfSystem = function (callback) {
    var sql = "SELECT `fleet_id`,`device_token` FROM `tb_fleets` WHERE `status` = ? AND `registration_status`=?";
    connection.query(sql, [constants.userFreeStatus.FREE, constants.userActiveStatus.ACTIVE], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
exports.getAllUserOfSystem = function (email, callback) {
    if (email != '') {
        var sql = "SELECT * FROM `tb_users` WHERE `is_dispatcher`=? AND `is_active`=? AND `email`='" + email + "' ";
    } else {
        var sql = "SELECT * FROM `tb_users` WHERE `is_dispatcher`=? AND `is_active`=? ";
    }
    connection.query(sql, [constants.isDispatcherStatus.NO, constants.userActiveStatus.ACTIVE], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
exports.getAllTeamsOfUser = function (user_id, callback) {
    var response = {
        "team_id": 0,
        "team_name": "All Teams"
    };
    var sql = "SELECT team_id,team_name FROM `tb_teams` WHERE `user_id`=?";
    connection.query(sql, [user_id], function (err, result) {
        var teamLength = result.length
        if (teamLength > 0) {
            //if (teamLength > 1) {
            result.splice(0, 0, response);
            //}
            return callback(result);
        } else {
            response = [{
                "team_id": 0,
                "team_name": "No Team Available"
            }];
            return callback(response);
        }
    });
};
exports.getAllTeamsOfDispatcher = function (dispatcher_id, callback) {
    var sql = "SELECT tm.team_id,tm.team_name FROM `tb_dispatcher_teams` dis_tm " +
        "INNER JOIN `tb_teams` tm on tm.team_id=dis_tm.team_id WHERE `dispatcher_id`=? ";
    connection.query(sql, [dispatcher_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            var response = [{
                "team_id": 0,
                "team_name": "No Team Available"
            }];
            return callback(response);
        }
    });
};
exports.authenticateFleetEmailWithUser = function (email, callback) {
    if (typeof email === "undefined" || email === "") {
        email = "0";
    }
    var sql = "SELECT * FROM `tb_fleets` WHERE `email`=? LIMIT 1";
    connection.query(sql, [email], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
}
exports.authenticateUniqueFleetLoginID = function (login_id, callback) {
    var sql = "SELECT * FROM `tb_fleets` WHERE `login_id`=? LIMIT 1";
    connection.query(sql, [login_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
}
/*
 * -----------------------------------------------
 * AUTHENTICATE TEMPLATE KEY
 * -----------------------------------------------
 */
exports.authenticateTemplateKey = function (templateKey, user_id, layout_type, callback) {
    var sql = "SELECT * FROM `tb_templates` WHERE `template_key`=? and `user_id`=? and `layout_type`=? LIMIT 1";
    connection.query(sql, [templateKey, user_id, layout_type], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
}
/*
 * -----------------------------------------------
 * CHANGE FLEET STATUS
 * -----------------------------------------------
 */
exports.changeFleetStatus = function (fleet_id, status) {
    var sql = "UPDATE `tb_fleets` SET `status`=? WHERE `fleet_id`=? LIMIT 1";
    connection.query(sql, [status, fleet_id], function (err, result) {
    });
};
/*
 * -----------------------------------------------
 * CHECK FLEET STATUS
 * -----------------------------------------------
 */
exports.checkFleetStatus = function (fleet_id, callback) {
    var sql = "SELECT `status`,`is_available` FROM `tb_fleets` WHERE `fleet_id`=? LIMIT 1";
    connection.query(sql, [fleet_id], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};
/*
 * -----------------------------------------------
 * DELETE JOB
 * -----------------------------------------------
 */
exports.deleteJob = function (job_id) {
    var sql = "DELETE FROM `tb_jobs` WHERE `job_id`=? LIMIT 1";
    connection.query(sql, [job_id], function (err, result) {
    });

};
exports.getFleetDetails = function (jobID, callback) {
    var sql = "SELECT fleets.`username`,fleets.`phone`,fleets.`fleet_id`,jobs.`job_time`,jobs.`timezone`,jobs.`job_type` " +
        "FROM `tb_fleets` fleets " +
        "INNER JOIN `tb_jobs` jobs WHERE jobs.`job_id`=? AND jobs.`fleet_id`=fleets.`fleet_id`";
    connection.query(sql, [jobID], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
}
exports.getCustomerDetailsFromJobID = function (jobID, callback) {
    var sql = "SELECT customer.`customer_username`,customer.`customer_phone`,customer.`customer_email`,customer.`user_id` " +
        "FROM `tb_customers` customer INNER JOIN `tb_jobs` jobs ON jobs.`customer_id`=customer.`customer_id` " +
        " WHERE jobs.`job_id`=?";
    connection.query(sql, [jobID], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
}
/*
 * -----------------------------------------------
 * SET NOTIFICATION TEXT
 * -----------------------------------------------
 */
exports.setNotification = function (user_id, text, title, jobID) {
    var sql = "INSERT INTO `tb_notifications` (`notification_text`,`user_id`,`notification_title`,`job_id`) VALUES (?,?,?,?)";
    connection.query(sql, [text, user_id, title, jobID], function (err, result) {

    });
};
/*
 * -----------------------------------------------
 * SET TASK HISTORY BY FLEET
 * -----------------------------------------------
 */
exports.setTaskHistory = function (fleet_id, type, job_id, description) {
    var sql = "SELECT `username`,`first_name`,`last_name`,`latitude`,`longitude` FROM `tb_fleets` WHERE `fleet_id`=? LIMIT 1";
    connection.query(sql, [fleet_id], function (err, result) {
        if (result && result.length > 0) {
            var sql = "INSERT INTO `tb_task_history` (`job_id`,`fleet_id`,`fleet_name`,`type`,`latitude`,`longitude`,`description`) VALUES (?,?,?,?,?,?,?)";
            connection.query(sql, [job_id, fleet_id, result[0].username, type, result[0].latitude, result[0].longitude, description], function (err, result) {
                if (err) {
                    console.log("ERROR IN SETTING TASK HISTORY", err);
                }
                return result.insertId;
            });
        } else {
            if (job_id) {
                var sql = "INSERT INTO `tb_task_history` (`job_id`,`type`,`description`) VALUES (?,?,?)";
                connection.query(sql, [job_id, type, description], function (err, result) {
                    return result.insertId;
                });
            }
        }
    });
};
/*
 * -----------------------------------------------
 * SET TASK HISTORY BY FLEET CALLBACK
 * -----------------------------------------------
 */
exports.setTaskHistoryCallback = function (fleet_id, type, job_id, description, callback) {
    var sql = "SELECT `username`,`first_name`,`last_name`,`latitude`,`longitude` FROM `tb_fleets` WHERE `fleet_id`=? LIMIT 1";
    connection.query(sql, [fleet_id], function (err, result) {
        if (result && result.length > 0) {
            var sql = "INSERT INTO `tb_task_history` (`job_id`,`fleet_name`,`fleet_id`,`type`,`latitude`,`longitude`,`description`) VALUES (?,?,?,?,?,?,?)";
            connection.query(sql, [job_id, result[0].username, fleet_id, type, result[0].latitude, result[0].longitude, description], function (err, result) {
                callback(result.insertId);
            });
        }
    });
};
/*
 * -----------------------------------------------
 * UPDATE TASK HISTORY BY FLEET CALLBACK
 * -----------------------------------------------
 */
exports.updateTaskHistoryCallback = function (fleet_id, type, job_id, description, id, callback) {
    var sql = "SELECT `username`,`first_name`,`last_name`,`latitude`,`longitude` FROM `tb_fleets` " +
        " WHERE `fleet_id`=? LIMIT 1";
    connection.query(sql, [fleet_id], function (err, result) {
        if (result && result.length > 0) {
            var sql = "UPDATE `tb_task_history` SET `type` = ?,`latitude` = ? ,`longitude` = ? WHERE `id`=?";
            connection.query(sql, [type, result[0].latitude, result[0].longitude, id], function (err, result) {
                callback(id);
            });
        }
    });
};
/*
 * -----------------------------------------------
 * DELETE TASK HISTORY BY FLEET CALLBACK
 * -----------------------------------------------
 */
exports.deleteTaskHistory = function (fleet_id, type, job_id, description, id, callback) {
    var sql = "SELECT `username`,`first_name`,`last_name`,`latitude`,`longitude` FROM `tb_fleets` WHERE `fleet_id`=? LIMIT 1";
    connection.query(sql, [fleet_id], function (err, result) {
        if (result && result.length > 0) {
            var sql = "SELECT * FROM `tb_task_history` WHERE `id`=? ";
            connection.query(sql, [id], function (err, tb_task_historyResult) {
                var sql = "INSERT INTO `tb_task_history` (`job_id`,`fleet_name`,`fleet_id`,`type`,`latitude`,`longitude`,`description`) VALUES (?,?,?,?,?,?,?)";
                connection.query(sql, [job_id, result[0].username, fleet_id, type, result[0].latitude, result[0].longitude, tb_task_historyResult[0].description], function (err, result) {
                    connection.query(" DELETE FROM `tb_task_history` WHERE `id`=? ", [id], function (err, delteresult) {
                    });
                    callback(result.insertId);
                });
            });
        }
    });
};
/*
 * -----------------------------------------------
 * CHECK WHETHER PICKUP HAS DELIVERED OR NOT
 * -----------------------------------------------
 */
exports.checkPickupComplete = function (job_id, fleet_id, callback) {
    var response = {
        status: true,
        job_id: 0
    }
    var sql = "SELECT `has_pickup`,`pickup_delivery_relationship`,`job_type` " +
        "FROM `tb_jobs` " +
        "WHERE `job_id`=? AND `fleet_id`=? AND `job_status`<>?";
    connection.query(sql, [job_id, fleet_id, constants.jobStatus.DELETED], function (err, result) {
        if (result && result.length > 0) {
            if (result[0].has_pickup == 1 && (constants.jobType.DELIVERY == result[0].job_type)) {
                var sql = "SELECT `job_status`,`job_id` " +
                    "FROM `tb_jobs` " +
                    "WHERE `pickup_delivery_relationship`=? and `job_type` = ? and `fleet_id`=? AND `job_status`<>?";
                connection.query(sql, [result[0].pickup_delivery_relationship, constants.jobType.PICKUP, fleet_id, constants.jobStatus.DELETED], function (err, endResult) {
                    if (err) {
                        logging.addErrorLog('Error in fetching job_status in checkPickupComplete in commonFunction', job_id + "---" + fleet_id, endResult);
                        logging.logDatabaseQueryError('Error in fetching job_status in checkPickupComplete in commonFunction', endResult);
                        return callback(response);
                    }
                    if (endResult && endResult.length > 0) {
                        if ((endResult[0].job_status == constants.jobStatus.ENDED) || (endResult[0].job_status == constants.jobStatus.PARTIAL) || (endResult[0].job_status == constants.jobStatus.FAILED)
                            || (endResult[0].job_status == constants.jobStatus.CANCEL)) {
                            return callback(response)
                        } else {
                            response.status = false;
                            response.job_id = endResult[0].job_id;
                            return callback(response);
                        }
                    } else {
                        return callback(response);
                    }
                });
            } else {
                return callback(response);
            }
        } else {
            response.status = false;
            return callback(response);
        }
    });
};
/*
 * -----------------------------------------------
 * SET SESSION WITH TASK START
 * -----------------------------------------------
 */
exports.setTaskSession = function (job_id, fleet_id, job_status, job_acknowledged, callback) {
    var sql = "SELECT `session_id` FROM `tb_task_session` WHERE `job_id`=? AND `fleet_id`=? LIMIT 1";
    connection.query(sql, [job_id, fleet_id], function (err, session_id_Result) {

        if (session_id_Result && session_id_Result.length > 0) {
            var sql = "UPDATE `tb_task_session` SET `job_id`=?,`fleet_id`=?,`job_status`=?,`job_acknowledged`=? " +
                "WHERE `session_id`=? LIMIT 1";
            connection.query(sql, [job_id, fleet_id, job_status, job_acknowledged, session_id_Result[0].session_id], function (err, result) {
                callback(session_id_Result[0].session_id);
            });
        } else {
            var sql = "INSERT INTO `tb_task_session` (`job_id`,`fleet_id`,`job_status`,`job_acknowledged`) VALUES (?,?,?,?)";
            connection.query(sql, [job_id, fleet_id, job_status, job_acknowledged], function (err, result) {
                callback(result.insertId);
            });
        }
    });
};
/*
 * -----------------------------------------------
 * UPDATE TASK SESSION
 * -----------------------------------------------
 */
exports.updateTaskSession = function (session_id, job_id, fleet_id, job_status, job_acknowledged, callback) {
    var sql = "UPDATE `tb_task_session` SET `job_id`=?,`fleet_id`=?,`job_status`=?,`job_acknowledged`=? " +
        "WHERE `session_id`=? LIMIT 1";
    connection.query(sql, [job_id, fleet_id, job_status, job_acknowledged, session_id], function (err, result) {
        callback(true);
    });
};
/*
 * ---------------------------
 * UPDATE NOTIFICATION COUNT
 * ---------------------------
 */
exports.updateNotificationCount = function (user_id, callback) {
    var sql = "UPDATE `tb_users` SET `notification_count`= `notification_count` + 1 WHERE `user_id`=? LIMIT 1";
    connection.query(sql, [user_id], function (err, result) {
    });

};
/*
 * -----------------------------------------------
 * CHECK PICKUP AND DELIVERY TIME
 * -----------------------------------------------
 */
exports.checkPickupAndDeliveryTime = function (job_pickup_datetime, job_delivery_datetime, timezone, callback) {
    var pickup = true, delivery = true;
    var today = moment(new Date()).format('YYYY-MM-DD HH:m:s');
    today = commonFunc.convertTimeIntoLocal(today, timezone);
    today = moment(today).format('YYYY-MM-DD HH:m:s');

    today = commonFunc.subtractHOUR(today);

    today = today.getTime();
    job_pickup_datetime = new Date(job_pickup_datetime).getTime();
    job_delivery_datetime = new Date(job_delivery_datetime).getTime();

    if (job_pickup_datetime < today && job_pickup_datetime != "") {
        pickup = false;
    }
    if (job_delivery_datetime < today && job_delivery_datetime != "") {
        delivery = false;
    }

    var response = {
        "pickup": pickup,
        "delivery": delivery
    }
    callback(response);
};
/*
 * -----------------------------------------------------------------------------
 * Uploading image to s3 bucket
 * INPUT : file parameter
 * OUTPUT : image path
 * -----------------------------------------------------------------------------
 */
exports.uploadImageToS3Bucket = function (file, folder, callback) {
    try {
        var job_image_name = file.name.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '')
        var job_image_size = file.size;
        var timestamp = new Date().getTime().toString();
        var str = '';
        var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        var size = chars.length;
        for (var i = 0; i < 4; i++) {
            var randomnumber = Math.floor(Math.random() * size);
            str = chars[randomnumber] + str;
        }
        job_image_name = job_image_name.replace(/\s/g, '');
        job_image_name = str + timestamp + "-" + job_image_name;
        file.name = job_image_name;
        var fs = require('node-fs');
        var AWS = require('aws-sdk');

        var filename = file.name; // actual filename of file
        var path = file.path; //will be put into a temp directory
        var mimeType = file.type;

        var accessKeyId = config.get('s3BucketCredentials.accessKeyId');
        var secretAccessKeyId = config.get('s3BucketCredentials.secretAccessKey');
        var bucketName = config.get('s3BucketCredentials.bucket');

        fs.readFile(path, function (error, file_buffer) {
            if (error) {
                return callback(0);
            }

            filename = file.name;
            AWS.config.update({accessKeyId: accessKeyId, secretAccessKey: secretAccessKeyId});
            var s3bucket = new AWS.S3();
            var params = {
                Bucket: bucketName,
                Key: folder + '/' + filename,
                Body: file_buffer,
                ACL: 'public-read',
                ContentType: mimeType
            };
            s3bucket.putObject(params, function (err, data) {
                if (err) {
                    return callback(0);
                }
                else {
                    return callback(filename);
                }
            });
        });
    } catch (e) {
        return callback(0);
    }
}
/*
 * -----------------------------------------------------------------------------
 * Uploading Thumb image to s3 bucket
 * INPUT : file parameter
 * OUTPUT : image path
 * -----------------------------------------------------------------------------
 */
exports.uploadThumbImageToS3Bucket = function (file, folder, callback) {
    var job_image_name = file.name.replace(/[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi, '');
    var job_image_size = file.size;

    var timestamp = new Date().getTime().toString();
    var str = '';
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    var size = chars.length;
    for (var i = 0; i < 4; i++) {
        var randomnumber = Math.floor(Math.random() * size);
        str = chars[randomnumber] + str;
    }
    job_image_name = job_image_name.replace(/\s/g, '');
    job_image_name = str + timestamp + "-" + job_image_name;
    file.name = job_image_name;
    var fs = require('fs');
    var AWS = require('aws-sdk');
    var gm = require('gm').subClass({imageMagick: true});
    var path = file.path; //will be put into a temp directory
    var tmp_path = file.path + "-tmpPath"; //will be put into a temp directory
    gm(path)
        .resize(150, 150, "!")
        .autoOrient()
        .write(tmp_path, function (err) {
            console.log(err)
            if (!err) {
                var filename = file.name; // actual filename of file
                var mimeType = file.type;

                var accessKeyId = config.get('s3BucketCredentials.accessKeyId');
                var secretAccessKeyId = config.get('s3BucketCredentials.secretAccessKey');
                var bucketName = config.get('s3BucketCredentials.bucket');
                fs.readFile(tmp_path, function (error, file_buffer) {
                    if (error) {
                        console.log(error)
                        return callback(0);
                    }
                    else {
                        filename = "thumb-" + file.name;
                        AWS.config.update({accessKeyId: accessKeyId, secretAccessKey: secretAccessKeyId});
                        var s3bucket = new AWS.S3();
                        var params = {
                            Bucket: bucketName,
                            Key: folder + '/' + filename,
                            Body: file_buffer,
                            ACL: 'public-read',
                            ContentType: mimeType
                        };

                        s3bucket.putObject(params, function (err, data) {
                            if (err) {
                                callback(0);
                            }
                            else {
                                return callback(filename);
                            }
                        });
                    }
                });
            }
            else {
                callback(0);
            }
        });
};
exports.sendMessageByTwillio = function (to, msg) {
    if (client === undefined) {
        getClient();
    }
    client.messages.create(
        {to: to, from: config.get('twillioCredentials.fromNumber'), body: msg},
        function (err, message) {
            console.log("Twilio error: " + JSON.stringify(err));
            console.log("Twilio message: " + JSON.stringify(message));
            //if(err){
            //    console.log("Error in sending message: " + JSON.stringify(err));
            //}
        }
    );
}
function getClient() {
    var accountSid = config.get('twillioCredentials.accountSid');
    var authToken = config.get('twillioCredentials.authToken');

    //require the Twilio module and create a REST client
    client = require('twilio')(accountSid, authToken);
}
function getPlivoClient() {
    var plivo = require('plivo');
    plivoClient = plivo.RestAPI({
        authId: config.get('plivoCredentials.accountSid'),
        authToken: config.get('plivoCredentials.authToken')
    });
}

exports.sendMessageByPlivo = function (to, msg) {

    if (to != "+910000000000") {
        if ((to.length > 9) && (to.startsWith('+1'))) {
            module.exports.sendMessageByTwillio(to, msg);
        } else {
            var params = {
                'src': config.get('plivoCredentials.fromNumber'), // Caller Id
                'dst': to,
                'text': msg,
                'type': "sms"
            };
            if (plivoClient === undefined) {
                getPlivoClient();
            }
            plivoClient.send_message(params, function (status, response) {
                console.log('Status: ', status);
                console.log('API Response:\n', response);
            });
        }
    }
}

if (typeof String.prototype.startsWith != 'function') {
    // see below for better implementation!
    String.prototype.startsWith = function (str) {
        return this.indexOf(str) === 0;
    };
}

exports.timeDifferenceInDays = function (date1, date2) {
    var t1 = new Date(date1).toDateString();
    var t2 = new Date(date2).toDateString();
    return parseInt((t2 - t1) / 86400000);
};

exports.generateRandomString = function () {
    var text = "";
    var possible = "0123456789";

    for (var i = 0; i < 4; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

exports.generateIncreasingIntegerRandomNumber = function (date1, date2) {
    var value = (new Date().getTime()).toString();
    if ((date1) && (date1 != '0000-00-00 00:00:00')) {
        value = (new Date(date1).getTime()).toString();
    } else {
        value = (new Date(date2).getTime()).toString();
    }
    value = md5(value + new Date().getTime() + module.exports.generateRandomStringAndNumbers());
    return value;
};

exports.generateRandomStringAndNumbers = function () {
    var text = "";
    var possible = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for (var i = 0; i < 8; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

exports.setCharAt = function (str, index, chr) {
    if (index > str.length - 1) return str;
    return str.substr(0, index) + chr + str.substr(index + 1);
}

function removeInvalidIds(allIds) {
    // done to handle the case where array is passed after stringifying
    allIds = allIds.toString();
    allIds = allIds.split(',');

    var i = 0;
    var isInvalid = false;
    var regularExp = /@facebook.com/i;
    var index = allIds.length;
    while (index--) {
        allIds[index] = allIds[index].trim();
        isInvalid = regularExp.test(allIds[index]);
        if (isInvalid === true) {
            allIds.splice(index, 1);
        }
    }
    return allIds;
}

exports.sendPlainTextEmail = function (to, cc, bcc, subject, message, callback) {

    var nodemailer = require("nodemailer");
    if (smtpTransport === undefined) {
        smtpTransport = nodemailer.createTransport({
            //service: config.get('emailCredentials.service'),
            host: config.get('emailCredentials.host'),
            port: config.get('emailCredentials.port'),
            auth: {
                user: config.get('emailCredentials.senderEmail'),
                pass: config.get('emailCredentials.senderPassword')
            }
        });
    }

    if (to) {
        to = removeInvalidIds(to);
    }
    if (cc) {
        cc = removeInvalidIds(cc);
    }
    if (bcc) {
        bcc = removeInvalidIds(bcc);
    }

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailCredentials.From'), // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        text: message // plaintext body
        //html: "<b>Hello world ?</b>" // html body
    };

    if (cc) {
        mailOptions.cc = cc;
    }
    if (bcc) {
        mailOptions.bcc = bcc;
    }

    // send mail with defined transport object
    if (to.length > 0 || cc.length > 0 || bcc.length > 0) {
        smtpTransport.sendMail(mailOptions, function (error, response) {
            console.log("Sending Mail Error: " + JSON.stringify(error));
            console.log("Sending Mail Response: " + JSON.stringify(response));
            return process.nextTick(callback.bind(null, error, response));
        });
    }

    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages
};


exports.sendHtmlEmail = function (to, cc, bcc, subject, htmlContent, callback) {
    if (smtpTransport === undefined) {
        smtpTransport = nodemailer.createTransport({
            host: config.get('emailCredentials.host'),
            port: config.get('emailCredentials.port'),
            auth: {
                user: config.get('emailCredentials.senderEmail'),
                pass: config.get('emailCredentials.senderPassword')
            }
        });
    }

    if (to) {
        to = removeInvalidIds(to);
    }
    if (cc) {
        cc = removeInvalidIds(cc);
    }
    if (bcc) {
        bcc = removeInvalidIds(bcc);
    }

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailCredentials.From'),
        to: to,
        subject: subject,
        html: htmlContent
    };

    if (cc) {
        mailOptions.cc = cc;
    }
    if (bcc) {
        mailOptions.bcc = bcc;
    }

    // send mail with defined transport object
    if (to.length > 0 || cc.length > 0 || bcc.length > 0) {
        smtpTransport.sendMail(mailOptions, function (error, response) {
            console.log("Sending Mail Error: " + JSON.stringify(error));
            console.log("Sending Mail Response: " + JSON.stringify(response));
            return process.nextTick(callback.bind(null, error, response));
        });
    }

    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages
};


exports.sendEmailForPassword = function (receiverMailId, message, subject, callback) {
    if (smtpTransport === undefined) {
        smtpTransport = nodemailer.createTransport({
            host: config.get('emailCredentials.host'),
            port: config.get('emailCredentials.port'),
            debug: true,
            auth: {
                user: config.get('emailCredentials.senderEmail'),
                pass: config.get('emailCredentials.senderPassword')
            }
        });
    }

    receiverMailId = removeInvalidIds(receiverMailId);

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailCredentials.From'), // sender address
        to: receiverMailId, // list of receivers
        subject: subject, // Subject line
        text: message // plaintext body
        //html: "<b>Hello world ?</b>" // html body
    };

    // send mail with defined transport object
    if (receiverMailId.length > 0) {
        smtpTransport.sendMail(mailOptions, function (error, response) {
            console.log("Sending Mail Error: " + error);
            console.log("Sending Mail Response: " + response);
            if (error) {
                return callback(0);
            } else {
                return callback(1);
            }

        });
    }

    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages
};

exports.sendHtmlContent = function (receiverMailId, html, subject, from, callback) {

    if (receiverMailId) {
        if (smtpTransport === undefined) {
            smtpTransport = nodemailer.createTransport({
                //service: config.get('emailCredentials.service'),
                host: config.get('emailCredentials.host'),
                port: config.get('emailCredentials.port'),
                auth: {
                    user: config.get('emailCredentials.senderEmail'),
                    pass: config.get('emailCredentials.senderPassword')
                }
            });
        }

        receiverMailId = removeInvalidIds(receiverMailId);

        if (typeof (from) === undefined || from == null || from == "") {
            from = config.get('emailCredentials.From');
        }

        // setup e-mail data with unicode symbols
        var mailOptions = {
            from: from, // sender address
            to: receiverMailId, // list of receivers
            subject: subject, // Subject line
            html: html // html body
        }

        // send mail with defined transport object
        if (receiverMailId.length > 0) {

            smtpTransport.sendMail(mailOptions, function (error, response) {
                console.log("Sending Mail Error: " + JSON.stringify(error));
                console.log("Sending Mail Response: " + JSON.stringify(response));
                if (error) {
                    return callback(0);
                } else {
                    return callback(1);
                }
            });
        }
    } else {
        return callback(0);
    }
    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages
};


exports.sendHtmlContent_UseBCC = function (receiverMailId, html, subject, callback) {
    if (smtpTransport === undefined) {
        smtpTransport = nodemailer.createTransport({
            service: config.get('emailCredentials.service'),
            auth: {
                user: config.get('emailCredentials.senderEmail'),
                pass: config.get('emailCredentials.senderPassword')
            }
        });
    }

    receiverMailId = removeInvalidIds(receiverMailId);

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: config.get('emailCredentials.from'), // sender address
        bcc: receiverMailId, // list of receivers
        subject: subject, // Subject line
        html: html // html body
    }

    // send mail with defined transport object
    if (receiverMailId.length > 0) {
        smtpTransport.sendMail(mailOptions, function (error, response) {
            if (error) {
                return callback(0);
            } else {
                return callback(1);
            }
        });
    }

    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages
};


exports.sendMessage = function (contact_number, message) {
    var accountSid = config.get('twillioCredentials.accountSid');
    var authToken = config.get('twillioCredentials.authToken');

    var client = require('twilio')(accountSid, authToken);
    client.messages.create({
        to: contact_number, // Any number Twilio can deliver to
        from: config.get('twillioCredentials.fromNumber'),
        body: message// body of the SMS message
    }, function (err, response) {
        if (err) {
            console.log("Sms service: Error: " + err);
            console.log("Sms service: Response: " + response);
        }
    });
};


exports.encrypt = function (text) {
    var crypto = require('crypto');
    var cipher = crypto.createCipher('aes-256-cbc', 'd6F3Efeq');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
};

exports.toHHMMSS = function (seconds) {
    var sec_num = parseInt(seconds);
    var days = Math.floor(sec_num / (3600 * 24));
    var hours = Math.floor((sec_num - days * 86400) / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    var string = '';
    if (days > 0) {
        string += days + ' day ';
    }
    if (hours > 0) {
        string += hours + ' hour ';
    }
    if (minutes > 0) {
        string += minutes + ' min ';
    }
    if (seconds > 0) {
        string += seconds + ' sec';
    }
    return string;
}

exports.toKM = function (meters) {
    var meters = parseInt(meters);
    var km = (meters / 1000).toFixed(2);
    var km = km + ' km ';
    return (km);
}

exports.getUserInformation = function (userId, callback) {
    var getInformation =
        'SELECT * ' +
        'FROM `tb_users` ' +
        'WHERE `user_id` = ?';
    connection.query(getInformation, [userId], function (err, user) {
        if (err) {
            return process.nextTick(callback.bind(null, err, user));
        }

        if (user.length == 0) {
            return process.nextTick(callback.bind(null, err, null));
        }

        return process.nextTick(callback.bind(null, err, user[0]));
    });
};


exports.calculateDistance = function (lat1, long1, lat2, long2, callback) {
    var dist = require('geolib');
    var from = {latitude: lat1, longitude: long1};
    var to = {latitude: lat2, longitude: long2};
    var result = dist.getDistance(from, to);
    callback(result);
};

/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toRad) === "undefined") {
    Number.prototype.toRad = function () {
        return this * Math.PI / 180;
    }
}

// start and end are objects with latitude and longitude
//decimals (default 2) is number of decimals in the output
//return is distance in kilometers.
exports.getDistance = function (lat1, lon1, lat2, lon2, decimals) {
    decimals = decimals || 2;
    var earthRadius = 6371; // km
    lat1 = parseFloat(lat1);
    lat2 = parseFloat(lat2);
    lon1 = parseFloat(lon1);
    lon2 = parseFloat(lon2);

    var dLat = (lat2 - lat1).toRad();
    var dLon = (lon2 - lon1).toRad();
    var lat1 = lat1.toRad();
    var lat2 = lat2.toRad();

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = earthRadius * c;
    return Math.round(d * Math.pow(10, decimals)) / Math.pow(10, decimals);
};

exports.sortByKeyAsc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
};

exports.sortByKeyDesc = function (array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x > y) ? -1 : ((x < y) ? 1 : 0));
    });
};

function sortAsc(array, key) {
    return array.sort(function (a, b) {
        var x = a[key];
        var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}


// Format the raw address obtained using google API
exports.formatLocationAddress = function (raw_address) {
    var pickup_location_address = 'Unnamed';

    var arr_formatted_address = raw_address.split(',');

    var arr_formatted_address_length = arr_formatted_address.length;
    var arr_pickup_location_address = [];
    for (var i = 0; i < arr_formatted_address_length; i++) {
        var flag = 0;
        for (var j = 0; j < arr_formatted_address_length; j++) {
            if ((i != j) && (arr_formatted_address[j].indexOf(arr_formatted_address[i]) > -1)) {
                flag = 1;
                break;
            }
        }
        if (flag == 0) {
            arr_pickup_location_address.push(arr_formatted_address[i]);
        }
    }

    pickup_location_address = arr_pickup_location_address.toString();
    return pickup_location_address;
};


// Get the address of the location using the location's latitude and longitude
exports.getLocationAddress = function (latitude, longitude, callback) {
    var geocoderProvider = 'google';
    var httpAdapter = 'https';
    var extra = {
        apiKey: config.get('googleCredentials.api_key'),
        formatter: null
    };
    var geocoder = require('node-geocoder')(geocoderProvider, httpAdapter, extra);
    geocoder.geocode(latitude + ',' + longitude, function (err, res) {
        console.log("geocoder Result", res);
        callback(res);
    });
};


// Get the latitude and longitude of address
exports.getLatLngFromAddress = function (address, callback) {
    if (address) {
        var geocoderProvider = 'google';
        var httpAdapter = 'https';
        var extra = {
            apiKey: config.get('googleCredentials.api_key'),
            formatter: null
        };
        var geocoder = require('node-geocoder')(geocoderProvider, httpAdapter, extra);
        geocoder.geocode(address, function (err, res) {
            if (err) {
                console.log("Error in geocoder Result", err);
                callback([]);
            } else {
                callback(res);
            }
        });
    } else {
        callback([]);
    }
};

// Get ETA between two address
exports.getEstimateTimeOfArrival = function (source, destination, mode, callback) {

    request('https://maps.googleapis.com/maps/api/distancematrix/json?origins=' + source + '&destinations=' + destination + '&mode=' + mode + '&language=en-EN' +
        '&sensor=false' +
        '&key=' + config.get('googleCredentials.api_key') + '', function (error, response, body) {
        var result = [];
        if (!error && response.statusCode == 200) {
            body = JSON.parse(body);
            console.log(body)
            body.rows.forEach(function (rows) {
                if (rows.elements[0].status == 'OK') {
                    var estimate_time = rows.elements[0];
                    estimate_time.duration.seconds = estimate_time.duration.value;
                    estimate_time.distance.metres = estimate_time.distance.value;
                    delete estimate_time.status;
                    delete estimate_time.duration.value;
                    delete estimate_time.distance.value;
                    result.push(estimate_time);
                }
            })
        }
        callback(result);
    });
};


// ADD DAYS TO CURRENT DATE

exports.addDays = function (days) {
    var newDate = new Date();
    newDate.setTime(newDate.getTime() + (86400000 * days)); // add a date
    newDate.setHours(23, 59, 59, 999);
    return new Date(newDate)
}
// ADD MINUTES TO CURRENT DATE
exports.addMinutes = function (date, minutes) {
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() + (60000 * minutes)); // add Minutes
    return new Date(newDate)
}


// Send the notification to the iOS device for customer
exports.sendIosPushNotification = function (tone, iosDeviceToken, message, flag, payload, deviceType) {
    try {
        if (payload.address) {
            payload.address = '';
        }
        var status = 1, msg = message, snd = tone, apns = require('apn');
        var options = {
            cert: constants.appVersions[deviceType].path,
            certData: null,
            key: constants.appVersions[deviceType].path,
            keyData: null,
            passphrase: 'click',
            ca: null,
            pfx: null,
            pfxData: null,
            gateway: constants.appVersions[deviceType].gateway,
            port: 2195,
            rejectUnauthorized: true,
            enhanced: true,
            cacheLength: 100,
            autoAdjustCache: true,
            connectionTimeout: 0,
            ssl: true
        };
        //var deviceToken = new apns.Device(iosDeviceToken);
        var apnsConnection = new apns.Connection(options);
        var note = new apns.Notification();

        note.expiry = Math.floor(Date.now() / 1000) + 3600;
        if (flag) note.contentAvailable = flag;
        note.sound = snd;
        note.alert = msg;
        note.newsstandAvailable = status;
        note.payload = payload;

        apnsConnection.pushNotification(note, iosDeviceToken);

        // Handle these events to confirm that the notification gets
        // transmitted to the APN server or find error if any
        function log(type) {
            return function () {
                if (debugging_enabled)
                    console.log("iOS PUSH NOTIFICATION RESULT: ", type);
            }
        }

        apnsConnection.on('transmissionError', function (err, n, c) {
            console.log('transmissionError', err, n, c);
        });
        apnsConnection.on('error', log('error'));
        apnsConnection.on('transmitted', log('transmitted'));
        apnsConnection.on('timeout', log('timeout'));
        apnsConnection.on('connected', log('connected'));
        apnsConnection.on('disconnected', log('disconnected'));
        apnsConnection.on('socketError', log('socketError'));
        apnsConnection.on('transmissionError', log('transmissionError'));
        apnsConnection.on('cacheTooSmall', log('cacheTooSmall'));
    } catch (e) {
        console.log("========= Error in Notification =======");
        console.log(e);
    }

};


// Send the notxification to the android device
exports.sendAndroidPushNotification = function (deviceToken, message, deviceType) {

    var gcm = require('node-gcm');
    var message = new gcm.Message({
        delayWhileIdle: false,
        timeToLive: 2419200,
        data: {
            message: message,
            brand_name: constants.appVersions[deviceType].brand_name
        }
    });
    var sender = new gcm.Sender(constants.appVersions[deviceType].path);
    var registrationIds = [];
    if (typeof deviceToken === 'object') {
        registrationIds = deviceToken;
    } else {
        registrationIds.push(deviceToken);
    }

    sender.send(message, registrationIds, 4, function (err, result) {
        if (debugging_enabled) {
            console.log("ANDROID NOTIFICATION RESULT: " + JSON.stringify(result));
            console.log("ANDROID NOTIFICATION ERROR: " + JSON.stringify(err));
        }
    });
};


// Send notification to the user with the given user ID
// ASSUMPTION: The payload is same for both the devices
exports.sendNotification = function (fleet_id, message, flag, payload, offline_driver_msg) {
    var get_user_device_info = "SELECT `noti_tone`,`fleet_id`,`device_type`,`device_token`,`is_available`,`phone`,`username` FROM `tb_fleets` WHERE `fleet_id`=?";
    connection.query(get_user_device_info, [fleet_id], function (err, result_user) {
        //logging.logDatabaseQueryError("Get device information for the driver", err, result_user, null);
        if (result_user[0].device_token) {
            module.exports.sendNotificationToDevice(result_user[0].noti_tone, result_user[0].device_type, result_user[0].device_token, message, flag, payload);
        } else {
            if (result_user[0].phone) {
                offline_driver_msg = offline_driver_msg.replace(/\[Fleet name]/g, (result_user[0].username).split(' ')[0]);
                module.exports.sendMessageByPlivo(result_user[0].phone, offline_driver_msg);
            }
        }
    });
};

exports.sendNotificationToDevice = function (tone, deviceType, userDeviceToken, message, flag, payload) {
    if (deviceType == constants.deviceType.ANDROID && userDeviceToken != '') {
        module.exports.sendAndroidPushNotification(userDeviceToken, payload, deviceType);
    }
    else if (deviceType == constants.deviceType.iOS && userDeviceToken != '' && userDeviceToken != null && (userDeviceToken != 'deviceToken')) {
        if ((userDeviceToken.length % 2) == 0) {
            module.exports.sendIosPushNotification(tone, userDeviceToken, message, flag, payload, deviceType);
        }
    }
    // In case of white labelled Apps.
    else {
        if (( deviceType % 2 ) == 0) {
            module.exports.sendAndroidPushNotification(userDeviceToken, payload, deviceType);
        } else {
            module.exports.sendIosPushNotification(tone, userDeviceToken, message, flag, payload, deviceType);
        }
    }
};

exports.capitalizeFirstLetter = function (string) {
    if (string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    } else {
        return "Hey";
    }
}

exports.timeDifferenceInDays = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 86400000);
};

exports.timeDifferenceInWords = function (gps, available, date1) {

    var time = date1, final;
    if (time < 60) {
        if (time <= 1) {
            time = "1 second ago";
        } else {
            time = time + " seconds ago";
        }
    } else if (time < 3600) {
        var diff = parseInt(time / 60);
        if (diff <= 1) {
            time = "1 minute ago";
        } else {
            time = diff + " minutes ago";
        }
    } else if (time < 86400) {
        var diff = parseInt(time / 3600);
        if (diff <= 1) {
            time = "1 hour ago";
        } else {
            time = diff + " hours ago";
        }
    } else if (time > 86400) {
        var diff = parseInt(time / 86400);
        if (diff <= 1) {
            time = "1 day ago";
        } else {
            time = diff + " days ago";
        }
    }
    final = time;
    if (available) {
        time = time.toString().split(' ago').join('');
        if (gps) {
            if (date1 < 60) {
                if (date1 <= 1) {
                    date1 = "1 second ago";
                } else {
                    date1 = date1 + " seconds ago";
                }
            } else {
                var diff = parseInt(date1 / 60);
                if (diff <= 1) {
                    date1 = "1 minute ago";
                } else if (diff < 3) {
                    date1 = diff + " minutes ago";
                } else if (diff >= 3 && diff < 8) {
                    date1 = "Poor Connectivity";
                } else if (diff > 8) {
                    date1 = "Connection Lost";
                }
            }
        } else {
            date1 = "GPS Turned Off";
        }
        final = date1;
    }
    return final;
};

exports.timeDifferenceInHours = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 3600000);
};

exports.timeDifferenceInMinutes = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 60000);
};

exports.timeDifferenceInSeconds = function (date1, date2) {
    var t1 = new Date(date1);
    var t2 = new Date(date2);
    return parseInt((t2 - t1) / 1000);
};

exports.changeTimezoneFromIstToUtc = function (date) {
    var temp = new Date(date);
    return new Date(temp.getTime() - (3600000 * 5.5)).toISOString();
};

exports.changeTimezoneFromUtcToIst = function (date) {
    var temp = new Date(date);
    return new Date(temp.getTime() + (3600000 * 5.5)).toISOString();
};

exports.getMysqlStyleDateString = function (jsDate) {
    var year = jsDate.getFullYear().toString();
    var month = (jsDate.getMonth() + 1).toString();
    month = month.length == 1 ? '0' + month : month;
    var date = jsDate.getDate().toString();
    date = date.length == 1 ? '0' + date : date;
    return year + '-' + month + '-' + date;
}

exports.convertTimeIntoLocal = function (date, timezone) {
    if (timezone == undefined || date == '0000-00-00 00:00:00') {
        return date;
    }
    else {
        var newDate = new Date(date), operator = timezone[0], millies = (timezone * 60 * 1000);
        if (operator == "-") {
            newDate.setTime(newDate.getTime() - millies)
        } else {
            newDate.setTime(newDate.getTime() - millies)
        }
        return newDate;
    }
}

exports.convertTimeIntoUTC = function (date, timezone) {
    if (timezone == undefined || date == '0000-00-00 00:00:00') {
        return date;
    }
    else {
        var newDate = new Date(date), millies = (timezone * 60 * 1000);
        newDate.setTime(newDate.getTime() + (millies));
        return newDate;
    }
}

/* Email Formatting */
exports.emailFormatting = function (senderID, message1, andriodAppLinkPath, iOSAppLinkPath, forgotlink, callback) {
    var msg = '<div style="width:700px !important;margin:auto;color:#424242;font-size:17px;background-color:whitesmoke;border: 1px solid #a1a1a1;padding: 10px 10px;border-radius: 4px;">'
    msg += '<p style="text-decoration:none;"> Hi <b>' + senderID + ',</b></p>';
    msg += '<div><p style="text-decoration:none;margin-top:10px"> ' + message1 + ' </p></div>';
    if (forgotlink != '') {
        msg += '<div><p style="text-decoration:none;margin-top:25px">Forgot your password, huh? No big deal.<br>To create a new password, just follow this link:</p></div>';
    }
    if (forgotlink != '') {
        msg += '<div><p style="text-decoration:none;margin-top:25px"> <a href=' + forgotlink + '>Click here to reset your password.</a></p></div>';
        msg += '<hr>';
        msg += '<div><p style="text-decoration:none;margin-top:25px"> You received this email, because it was requested by a Tookan admin. This is part of the procedure to create a new password on the system. If you DID NOT request a new password then please ignore this email and your password will remain the same.</p></div>';
    }
    msg += '<hr>';
    msg += '<table align="center"><tbody>';
    msg += '<tr>';
    msg += '<td>';
    msg += '<a href="' + andriodAppLinkPath + '"><img style="width: 132px;height: 39px;" src="https://api.tookanapp.com/appstore.png"/></a>';
    msg += '</td>';
    msg += '<td>';
    msg += '<a href="' + iOSAppLinkPath + '"><img style="width: 132px;height: 43px;"  src="https://api.tookanapp.com/googleplay.jpeg"/></a>';
    msg += '</td>';
    msg += '</tr>';
    msg += '</tbody></table>';
    msg += '<b>Best,</b><br><b>Team Tookan</b>';
    //msg += '<br><br>--<br>Sent by Tookan. 114 Sansome St Ste 250 San Francisco CA 94104, USA';
    msg += '</div></div>';
    return callback(msg);
}

/* Email Formatting */
exports.emailPlainFormatting = function (senderID, message1, andriodAppLinkPath, iOSAppLinkPath, forgotlink, callback) {
    var msg = '<div>'

    if (senderID != '' || senderID == null) {
        senderID = senderID.split(' ')[0];
        msg += '<p> Hello ' + senderID + ',</p>';
    } else {
        msg += '<p > Hello,</p>';
    }
    msg += '<div><p style="margin-top:10px"> ' + message1 + ' </p></div>';
    if (forgotlink != '') {
        msg += '<div><p style="text-decoration:none;margin-top:25px">Forgot your password, huh? No big deal.<br>To create a new password, just follow this link:</p></div>';
    }
    if (forgotlink != '') {
        msg += '<div><p style="text-decoration:none;margin-top:25px"> <a href=' + forgotlink + '>Click here to reset your password.</a></p></div>';
        msg += '<hr>';
        msg += '<div><p style="text-decoration:none;margin-top:25px"> Somebody recently asked to reset your BRAND_NAME account password. This is part of the procedure to create a new password on the system. If you DID NOT request a new password then please ignore this email and your password will remain the same.</p></div>';
    }
    msg += '<b>Best,</b><br><b>Team BRAND_NAME</b>';
    //msg += '<br><br>--<br>Sent by Tookan. 114 Sansome St Ste 250 San Francisco CA 94104, USA';
    msg += '</div></div>';
    return callback(msg);
}
/* SEND EMAIL AND SMS TO CUSTOMER */

exports.sendTemplateEmailAndSMS = function (user_id, template_key, jobID) {
    commonFunc.authenticateUserIdAndJobId(user_id, jobID, function (auth_result) {
        if (auth_result && auth_result[0]) {
            var layout_type = 1, to_email = auth_result[0].customer_email, to_phone = auth_result[0].customer_phone;
            if (auth_result[0].job_type == constants.jobType.DELIVERY) {
                layout_type = 1;
                to_email = auth_result[0].customer_email;
                to_phone = auth_result[0].customer_phone;
            } else if (auth_result[0].job_type == constants.jobType.PICKUP) {
                layout_type = 0;
                to_email = auth_result[0].job_pickup_email;
                to_phone = auth_result[0].job_pickup_phone;
            } else if (auth_result[0].job_type == constants.jobType.FOS) {
                layout_type = 3;
                to_email = auth_result[0].customer_email;
                to_phone = auth_result[0].customer_phone;
            } else if (auth_result[0].job_type == constants.jobType.APPOINTMENT) {
                layout_type = 2;
                to_email = auth_result[0].customer_email;
                to_phone = auth_result[0].customer_phone;
            }
            if (user_id == 853 && layout_type == 1 && template_key == "SUCCESSFUL") {
                extras.customerOrientedFunction(user_id, template_key, layout_type, to_email, to_phone, jobID)
            } else {
                var counter = 0;
                commonFunc.getTemplateMessage(user_id, template_key, layout_type, function (getTemplateMessageResult) {
                    if (getTemplateMessageResult == 0) {
                    } else {
                        var sms_text = 'Disabled', email_subject = 'Disabled', email_text = 'Disabled', user_email = 'Disabled', hook = 'Disabled';
                        if (getTemplateMessageResult[0].sms_enabled == 1) {
                            sms_text = getTemplateMessageResult[0].sms_text;
                        }
                        if (getTemplateMessageResult[0].hook_enabled == 1) {
                            hook = getTemplateMessageResult[0].hook;
                        }
                        if (getTemplateMessageResult[0].email_enabled == 1) {
                            email_text = getTemplateMessageResult[0].email_text;
                            email_subject = getTemplateMessageResult[0].email_subject;
                            user_email = getTemplateMessageResult[0].email;
                        }
                        commonFunc.getTemplateVariables(getTemplateMessageResult[0].template_id, function (getTemplateVariablesResult) {
                            var getTemplateVariablesResultLength = getTemplateVariablesResult.length;
                            for (var i = 0; i < getTemplateVariablesResultLength; i++) {
                                var variable_name = getTemplateVariablesResult[i].variable_name;
                                switch (variable_name) {
                                    case '[CustomerName]' :
                                        getCustomerDetailsFromJobID(jobID, function (getCustomerDetailsFromJobIDResult) {
                                            if (getCustomerDetailsFromJobIDResult && getCustomerDetailsFromJobIDResult[0].customer_username) {
                                                getCustomerDetailsFromJobIDResult[0].customer_username = getCustomerDetailsFromJobIDResult[0].customer_username.split(' ')[0];
                                                email_text = email_text.replace(/\[CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                                email_subject = email_subject.replace(/\[CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                                sms_text = sms_text.replace(/\[CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[Date]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_time) {
                                                var job_date = moment(getJobDetailsFromJobIDResult[0].job_time).format('MMM Do');
                                                email_text = email_text.replace(/\[Date]/g, job_date);
                                                email_subject = email_subject.replace(/\[Date]/g, job_date);
                                                sms_text = sms_text.replace(/\[Date]/g, job_date);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[CompletedDate]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].completed_datetime) {
                                                var timezone = getJobDetailsFromJobIDResult[0].timezone;
                                                var today = moment(new Date()).format('YYYY-MM-DD HH:m:s');
                                                today = commonFunc.convertTimeIntoLocal(today, timezone);
                                                var completed_datetime = moment(today).format('MMM Do');
                                                email_text = email_text.replace(/\[CompletedDate]/g, completed_datetime);
                                                email_subject = email_subject.replace(/\[CompletedDate]/g, completed_datetime);
                                                sms_text = sms_text.replace(/\[CompletedDate]/g, completed_datetime);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[CompletedTime]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].completed_datetime) {
                                                var timezone = getJobDetailsFromJobIDResult[0].timezone;
                                                var today = moment(new Date()).format('YYYY-MM-DD HH:m:s');
                                                today = commonFunc.convertTimeIntoLocal(today, timezone);
                                                var completed_datetime = moment(today).format('h:mm a');
                                                email_text = email_text.replace(/\[CompletedTime]/g, completed_datetime);
                                                email_subject = email_subject.replace(/\[CompletedTime]/g, completed_datetime);
                                                sms_text = sms_text.replace(/\[CompletedTime]/g, completed_datetime);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[CustomerAddress]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getCustomerDetailsFromJobIDResult) {
                                            if (getCustomerDetailsFromJobIDResult && getCustomerDetailsFromJobIDResult[0].job_address) {
                                                email_text = email_text.replace(/\[CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                                email_subject = email_subject.replace(/\[CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                                sms_text = sms_text.replace(/\[CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[StartTime]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_pickup_datetime && getJobDetailsFromJobIDResult[0].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                var start_time = moment(getJobDetailsFromJobIDResult[0].job_pickup_datetime).format('h:mm a');
                                                email_text = email_text.replace(/\[StartTime]/g, start_time);
                                                email_subject = email_subject.replace(/\[StartTime]/g, start_time);
                                                sms_text = sms_text.replace(/\[StartTime]/g, start_time);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[StartDate]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_pickup_datetime && getJobDetailsFromJobIDResult[0].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                var start_time = moment(getJobDetailsFromJobIDResult[0].job_pickup_datetime).format('MMM Do');
                                                email_text = email_text.replace(/\[StartDate]/g, start_time);
                                                email_subject = email_subject.replace(/\[StartDate]/g, start_time);
                                                sms_text = sms_text.replace(/\[StartDate]/g, start_time);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[EndTime]' :
                                    case '[Time]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_delivery_datetime && getJobDetailsFromJobIDResult[0].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                var end_time = moment(getJobDetailsFromJobIDResult[0].job_delivery_datetime).format('h:mm a');
                                                email_text = email_text.replace(/\[EndTime]/g, end_time);
                                                email_subject = email_subject.replace(/\[EndTime]/g, end_time);
                                                sms_text = sms_text.replace(/\[EndTime]/g, end_time);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[EndDate]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_delivery_datetime && getJobDetailsFromJobIDResult[0].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                var end_time = moment(getJobDetailsFromJobIDResult[0].job_delivery_datetime).format('MMM Do');
                                                email_text = email_text.replace(/\[EndDate]/g, end_time);
                                                email_subject = email_subject.replace(/\[EndDate]/g, end_time);
                                                sms_text = sms_text.replace(/\[EndDate]/g, end_time);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[TaskDescription]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_description) {
                                                email_text = email_text.replace(/\[TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                                email_subject = email_subject.replace(/\[TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                                sms_text = sms_text.replace(/\[TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        })
                                        break;
                                    case '[NewLine]' :
                                        email_text = email_text.replace(/\[NewLine]/g, "<br>");
                                        email_subject = email_subject.replace(/\[NewLine]/g, "<br>");
                                        sms_text = sms_text.replace(/\[NewLine]/g, "\n");
                                        counter++;
                                        replaceCustomFields(user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        break;
                                    case '[DriverName]' :
                                    case '[AgentName]' :
                                    case '[FleetName]' :
                                        commonFunc.getFleetDetailsFromJobID(jobID, function (getFleetDetailsFromJobIDResult) {
                                            if (getFleetDetailsFromJobIDResult && getFleetDetailsFromJobIDResult[0].username) {
                                                email_text = email_text.replace(/\[DriverName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                email_subject = email_subject.replace(/\[DriverName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                sms_text = sms_text.replace(/\[DriverName]/g, getFleetDetailsFromJobIDResult[0].username);

                                                email_text = email_text.replace(/\[AgentName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                email_subject = email_subject.replace(/\[AgentName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                sms_text = sms_text.replace(/\[AgentName]/g, getFleetDetailsFromJobIDResult[0].username);

                                                email_text = email_text.replace(/\[FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                email_subject = email_subject.replace(/\[FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                sms_text = sms_text.replace(/\[FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        });
                                        break;
                                    case '[DriverPhone]' :
                                    case '[AgentPhone]' :
                                    case '[FleetPhone]' :
                                        commonFunc.getFleetDetailsFromJobID(jobID, function (getFleetDetailsFromJobIDResult) {
                                            if (getFleetDetailsFromJobIDResult && getFleetDetailsFromJobIDResult[0].phone) {
                                                email_text = email_text.replace(/\[DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                email_subject = email_subject.replace(/\[DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                sms_text = sms_text.replace(/\[DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);

                                                email_text = email_text.replace(/\[AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                email_subject = email_subject.replace(/\[AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                sms_text = sms_text.replace(/\[AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);

                                                email_text = email_text.replace(/\[FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                email_subject = email_subject.replace(/\[FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                sms_text = sms_text.replace(/\[FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        });
                                        break;
                                    case '[TrackingLink]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {

                                            commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (user) {

                                                var longurl = config.get('trackLink') + getJobDetailsFromJobIDResult[0].job_hash;
                                                if (user[0].domain) {
                                                    longurl = user[0].domain + config.get('trackingPage') + getJobDetailsFromJobIDResult[0].job_hash;
                                                }
                                                module.exports.getGoogleShortenUrl(longurl, function (shortenUrl) {
                                                    if (shortenUrl) {
                                                        email_text = email_text.replace(/\[TrackingLink]/g, shortenUrl);
                                                        email_subject = email_subject.replace(/\[TrackingLink]/g, shortenUrl);
                                                        sms_text = sms_text.replace(/\[TrackingLink]/g, shortenUrl);
                                                        counter++;
                                                        replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                    }
                                                })
                                            })
                                        })
                                        break;
                                    case '[OrgName]' :
                                    case '[CompanyName]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            module.exports.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                if (getUserDetailsResult && getUserDetailsResult[0].company_name) {
                                                    email_text = email_text.replace(/\[OrgName]/g, getUserDetailsResult[0].company_name);
                                                    email_subject = email_subject.replace(/\[OrgName]/g, getUserDetailsResult[0].company_name);
                                                    sms_text = sms_text.replace(/\[OrgName]/g, getUserDetailsResult[0].company_name);

                                                    email_text = email_text.replace(/\[CompanyName]/g, getUserDetailsResult[0].company_name);
                                                    email_subject = email_subject.replace(/\[CompanyName]/g, getUserDetailsResult[0].company_name);
                                                    sms_text = sms_text.replace(/\[CompanyName]/g, getUserDetailsResult[0].company_name);
                                                }
                                                counter++;
                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                            });
                                        });
                                        break;
                                    case '[DispatcherNumber]' :
                                    case '[ManagerNumber]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            module.exports.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                if (getUserDetailsResult && getUserDetailsResult[0].user_id) {
                                                    email_text = email_text.replace(/\[DispatcherNumber]/g, getUserDetailsResult[0].phone);
                                                    email_subject = email_subject.replace(/\[DispatcherNumber]/g, getUserDetailsResult[0].phone);
                                                    sms_text = sms_text.replace(/\[DispatcherNumber]/g, getUserDetailsResult[0].phone);

                                                    email_text = email_text.replace(/\[ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                    email_subject = email_subject.replace(/\[ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                    sms_text = sms_text.replace(/\[ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                }
                                                counter++;
                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                            });
                                        });
                                        break;
                                    case '[Dispatcher]' :
                                    case '[Manager]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            module.exports.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                if (getUserDetailsResult && getUserDetailsResult[0].username) {
                                                    email_text = email_text.replace(/\[Dispatcher]/g, getUserDetailsResult[0].username);
                                                    email_subject = email_subject.replace(/\[Dispatcher]/g, getUserDetailsResult[0].username);
                                                    sms_text = sms_text.replace(/\[Dispatcher]/g, getUserDetailsResult[0].username);

                                                    email_text = email_text.replace(/\[Manager]/g, getUserDetailsResult[0].username);
                                                    email_subject = email_subject.replace(/\[Manager]/g, getUserDetailsResult[0].username);
                                                    sms_text = sms_text.replace(/\[Manager]/g, getUserDetailsResult[0].username);
                                                }
                                                counter++;
                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                            });
                                        });
                                        break;
                                    case '[TaskID]' :
                                        email_text = email_text.replace(/\[TaskID]/g, jobID);
                                        email_subject = email_subject.replace(/\[TaskID]/g, jobID);
                                        sms_text = sms_text.replace(/\[TaskID]/g, jobID);
                                        counter++;
                                        replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        break;
                                    case '[OrderNumber]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].order_id) {
                                                email_text = email_text.replace(/\[OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                                email_subject = email_subject.replace(/\[OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                                sms_text = sms_text.replace(/\[OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        });
                                        break;
                                    case '[TotalTimeTaken]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult && (getJobDetailsFromJobIDResult[0].started_datetime != "0000-00-00 00:00:00") && (getJobDetailsFromJobIDResult[0].completed_datetime != "0000-00-00 00:00:00")) {
                                                var started_time = new Date(getJobDetailsFromJobIDResult[0].started_datetime);
                                                var completed_datetime = new Date(getJobDetailsFromJobIDResult[0].completed_datetime);
                                                var timeDifference = commonFunc.millisecondsToStr(completed_datetime.getTime() - started_time.getTime());
                                                email_text = email_text.replace(/\[TotalTimeTaken]/g, timeDifference);
                                                email_subject = email_subject.replace(/\[TotalTimeTaken]/g, timeDifference);
                                                sms_text = sms_text.replace(/\[TotalTimeTaken]/g, timeDifference);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        });
                                        break;
                                    case '[TotalDistanceTravelled]' :
                                        commonFunc.getJobDetailsFromJobID(jobID, function (getJobDetailsFromJobIDResult) {
                                            if (getJobDetailsFromJobIDResult) {
                                                email_text = email_text.replace(/\[TotalDistanceTravelled]/g, getJobDetailsFromJobIDResult[0].total_distance_travelled);
                                                email_subject = email_subject.replace(/\[TotalDistanceTravelled]/g, getJobDetailsFromJobIDResult[0].total_distance_travelled);
                                                sms_text = sms_text.replace(/\[TotalDistanceTravelled]/g, getJobDetailsFromJobIDResult[0].total_distance_travelled);
                                            }
                                            counter++;
                                            replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        });
                                        break;
                                    default:
                                        counter++;
                                        replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                }
                            }

                            function replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {
                                if (counter == variablesResultLength) {
                                    mongo.getOptionalFieldsForTask(user_id, jobID, {}, function (getOptionalFieldsForTaskResult) {
                                        if ((typeof (getOptionalFieldsForTaskResult.fields.custom_field) != "undefined") && (getOptionalFieldsForTaskResult.fields.custom_field.length > 0)) {
                                            getOptionalFieldsForTaskResult.fields.custom_field.forEach(function (field) {
                                                email_text = email_text.replaceAll('[' + field.template_id + '-' + field.label + ']', field.fleet_data || field.data);
                                                email_subject = email_subject.replaceAll('[' + field.template_id + '-' + field.label + ']', field.fleet_data || field.data);
                                                sms_text = sms_text.replaceAll('[' + field.template_id + '-' + field.label + ']', field.fleet_data || field.data);
                                            })
                                        }
                                        replaceTaskHistory(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength);
                                    });
                                }
                            }

                            function replaceTaskHistory(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {

                                if (counter == variablesResultLength) {
                                    commonFunc.getTaskHistory(jobID, {}, function (getTaskHistoryResult) {
                                        getTaskHistoryResult.task_history.forEach(function (history) {
                                            if (history.type == constants.taskHistoryType.SIGN_IMAGE_ADDED || history.type == constants.taskHistoryType.signature_image_updated) {
                                                history.description = history.description.replaceAll('https://', '');
                                                email_text = email_text.replaceAll('[SignImage]', history.description);
                                                email_subject = email_subject.replaceAll('[SignImage]', history.description);
                                                sms_text = sms_text.replaceAll('[SignImage]', history.description);
                                            }
                                        })
                                        checkCounter(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength);
                                    });
                                }
                            }

                            function checkCounter(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {

                                var regex = sms_text.match(/\[(.*?)\]/g);
                                if (regex && regex.length > 0) {
                                    regex.forEach(function (dat) {
                                        sms_text = sms_text.replaceAll(dat, '-');
                                    })
                                }
                                var regex = email_text.match(/\[(.*?)\]/g);
                                if (regex && regex.length > 0) {
                                    regex.forEach(function (dat) {
                                        email_text = email_text.replaceAll(dat, '-');
                                    })
                                }
                                var regex = email_subject.match(/\[(.*?)\]/g);
                                if (regex && regex.length > 0) {
                                    regex.forEach(function (dat) {
                                        email_subject = email_subject.replaceAll(dat, '-');
                                    })
                                }

                                if (counter == variablesResultLength) {
                                    if ((to_phone) && (sms_text != 'Disabled')) {
                                        sms_text = sms_text.trim().replace(/([^\x00-\xFF]|\s)*$/g, '');
                                        commonFunc.sendMessageByPlivo(to_phone, sms_text);
                                    }
                                    if ((to_email) && (user_email != 'Disabled')) {
                                        commonFunc.sendHtmlContent(to_email, email_text, email_subject, user_email, function (resultMail) {
                                        });
                                    }
                                    if (hook != 'Disabled') {
                                        module.exports.sendPostHook(hook, jobID);
                                    }
                                }
                            }
                        });
                    }
                })
            }
        }
    });
}

String.prototype.replaceAll = function (target, replacement) {
    return this.split(target).join(replacement);
};


exports.sendPostHook = function (hook, jobID) {
    var needle = require('needle'),
        type = getParameterByName(hook, 'type'),
        task_history = getParameterByName(hook, 'task_history'),
        custom_field = getParameterByName(hook, 'custom_field');
    if (typeof task_history === 'undefined' || typeof task_history === undefined || task_history === '') task_history = 0;
    if (typeof custom_field === 'undefined' || typeof custom_field === undefined || custom_field === '') custom_field = 1;
    if (type.toLowerCase() === 'both') {
        module.exports.getBothTasksFromSingleID(jobID, function (data) {
            var final_data = {};
            data = data || [];
            if (data.length && data.length > 1) {
                final_data.pickup = data[0];
                final_data.delivery = data[1];
            } else if (data.length && data[0].job_type == constants.jobType.PICKUP) {
                final_data.pickup = data[0];
            } else if (data.length && data[0].job_type == constants.jobType.DELIVERY) {
                final_data.delivery = data[0];
            } else if (data.length && data[0].job_type == constants.jobType.FOS) {
                final_data.fos = data[0];
            } else if (data.length && data[0].job_type == constants.jobType.APPOINTMENT) {
                final_data.appointment = data[0];
            }
            postData(hook, final_data);
        })
    } else {
        async.waterfall([
            function (cb) {
                module.exports.getJobDetailsFromJobID(jobID, function (jobs) {
                    cb(null, jobs);
                });
            },
            function (jobs, cb) {
                if (custom_field && custom_field == 1) {
                    mongo.getOptionalFieldsForTask(jobs[0].user_id, jobID, {}, function (custom_field) {
                        jobs[0].custom_fields = custom_field.fields.custom_field;
                        cb(null, jobs);
                    });
                } else {
                    jobs[0].custom_field = [];
                    cb(null, jobs);
                }
            },
            function (jobs, cb) {
                if (task_history && task_history == 1) {
                    module.exports.getTaskHistory(jobID, {}, function (task_history) {
                        jobs[0].task_history = task_history.task_history;
                        cb(null, jobs);
                    });
                } else {
                    jobs[0].task_history = [];
                    cb(null, jobs);
                }
            }
        ], function (error, success) {
            if (error) {
                console.log('Something is wrong in sending webhook');
            } else {
                success[0].job_state = constants.jobStatusValue[parseInt(success[0].job_status)];
                if (success[0].started_datetime == "0000-00-00 00:00:00") success[0].started_datetime = '';
                if (success[0].completed_datetime == '0000-00-00 00:00:00') success[0].completed_datetime = '';
                if (success[0].arrived_datetime == "0000-00-00 00:00:00") success[0].arrived_datetime = '';
                if (success[0].acknowledged_datetime == "0000-00-00 00:00:00") success[0].acknowledged_datetime = '';
                if (success[0].completed_datetime) success[0].completed_datetime = (moment(commonFunc.convertTimeIntoLocal(success[0].completed_datetime, success[0].timezone)).format('YYYY-MM-DD HH:mm:s')).toString();
                if (success[0].started_datetime) success[0].started_datetime = (moment(commonFunc.convertTimeIntoLocal(success[0].started_datetime, success[0].timezone)).format('YYYY-MM-DD HH:mm:s')).toString();
                if (success[0].arrived_datetime) success[0].arrived_datetime = (moment(commonFunc.convertTimeIntoLocal(success[0].arrived_datetime, success[0].timezone)).format('YYYY-MM-DD HH:mm:s')).toString();
                if (success[0].acknowledged_datetime) success[0].acknowledged_datetime = (moment(commonFunc.convertTimeIntoLocal(success[0].acknowledged_datetime, success[0].timezone)).format('YYYY-MM-DD HH:mm:s')).toString();
                success[0].job_token = success[0].pickup_delivery_relationship;
                postData(hook, success[0]);
            }
        });
    }

    function postData(hook, data) {
        if (hook) {
            hook.split(',').forEach(function (d) {
                needle.post(d.toString().trim(), data,
                    function (err, resp, body) {

                    });
            });
        }
    }
}

function getParameterByName(url, name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(url);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/*
 * -----------------------
 * GET TEMPLETE MESSAGE
 * -----------------------
 */
exports.getTemplateMessage = function (user_id, template_key, layout_type, callback) {
    var sql = "SELECT template.`template_id`,template.`sms_text`,template.`email_text`,template.`email_subject`,template.`hook`,template.`hook_enabled`," +
        "template.`email_subject`,template.`email_enabled`,template.`sms_enabled`,template.`layout_type`," +
        "users.`email` FROM `tb_templates` template " +
        "INNER JOIN `tb_users` users ON users.`user_id`=template.`user_id` " +
        " WHERE template.`user_id`=? AND template.`template_key`=? AND template.`layout_type`=? LIMIT 1";
    connection.query(sql, [user_id, template_key, layout_type], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};

/*
 * ---------------------------
 * GET TEMPLATE VARIABLES
 * ---------------------------
 */
exports.getTemplateVariables = function (templateID, callback) {
    var sql = "SELECT `variable_name` FROM `tb_template_variables` WHERE `template_id`=?";
    connection.query(sql, [templateID], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};


/*
 * --------------------------------
 * GET CUSTOMER DETAILS FROM JOB ID
 * --------------------------------
 */
function getCustomerDetailsFromJobID(jobID, callback) {
    var sql = "SELECT customers.`customer_username` FROM `tb_customers` customers INNER JOIN `tb_jobs` jobs " +
        "WHERE jobs.`job_id`=? AND jobs.`customer_id`=customers.`customer_id`";
    connection.query(sql, [jobID], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};

/*
 * -----------------------------
 * GET FLEET DETAILS FROM JOB ID
 * -----------------------------
 */
exports.getFleetDetailsFromJobID = function (jobID, callback) {
    var sql = "SELECT fleets.`username`,fleets.`phone` FROM `tb_fleets` fleets " +
        "INNER JOIN `tb_jobs` jobs " +
        "WHERE jobs.`job_id`=? AND jobs.`fleet_id`=fleets.`fleet_id`";
    connection.query(sql, [jobID], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};

/*
 * ---------------------------
 * GET JOB DETAILS FROM JOB ID
 * ---------------------------
 */
exports.getJobDetailsFromJobID = function (jobID, callback) {
    var sql = "SELECT job.*,user.`tookan_shared_secret`,user.`distance_in`," +
        " fleet.username as fleet_name,fleet.email as fleet_email,fleet.phone as fleet_phone " +
        " FROM `tb_jobs` job " +
        " LEFT JOIN tb_fleets fleet ON fleet.fleet_id=job.fleet_id " +
        " INNER JOIN tb_users user ON user.user_id=job.user_id " +
        " WHERE `job_id` = ? ";
    connection.query(sql, [jobID], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};


/*
 * ---------------------------
 * GET JOB DETAILS FROM JOB ID
 * ---------------------------
 */
exports.checkJobIDWithProfile = function (jobID, task, callback) {
    var sql = "SELECT IF( `image` IS NULL,'',`image`) as `image`,`creation_datetime`,`job_id`,`notes`,`latitude`,`longitude`,`appointment_field_data_id` " +
        "FROM `tb_appointment_field_data` WHERE `job_id` = ?";
    connection.query(sql, [jobID], function (err, result) {
        if (result && result.length > 0) {

            var response = {
                "job_images": result,
                "task": task
            }
            return callback(response);
        } else {
            var response = {
                "job_images": [],
                "task": task
            }
            return callback(response);
        }
    });
};


/*
 * ---------------------------
 * GET ACKNOWLEDGED IMAGES FROM JOB ID
 * ---------------------------
 */
exports.checkAcknowlegedImages = function (jobID, task, callback) {
    var sql = "SELECT * FROM `tb_job_images` WHERE `job_id` = ?";
    connection.query(sql, [jobID], function (err, result) {
        if (result && result.length > 0) {

            var response = {
                "job_images": result,
                "task": task
            }
            return callback(response);
        } else {
            var response = {
                "job_images": [],
                "task": task
            }
            return callback(response);
        }
    });
};

/*
 * ---------------------------
 * GET TASK HISTORY FROM JOB ID
 * ---------------------------
 */
exports.getTaskHistory = function (jobID, task, callback) {
    var sql = "SELECT * FROM `tb_task_history` WHERE `job_id` = ? ";
    connection.query(sql, [jobID], function (err, result) {
        if (result && result.length > 0) {
            result.forEach(function (fl) {
                if (fl.description) {
                    var reason = fl.description.split('#-#');
                    if (reason.length > 1) {
                        fl.description = reason[0];
                        fl.reason = reason[1];
                    }
                }
            })
            var response = {
                "task_history": result,
                "task": task
            }
        } else {
            var response = {
                "task_history": [],
                "task": task
            }
        }
        return callback(response);
    });
};

exports.updateCompletedRoutedTasks = function (data, callback) {
    if (data.routed_comp_tasks) {
        var sql = "UPDATE tb_jobs SET `is_routed`=? " +
            " WHERE `job_id` IN (" + data.routed_comp_tasks + ") " +
            " AND `user_id` = ?";
        connection.query(sql, [2, data.user_id], function (err, result) {
            callback(true);
        })
    } else {
        callback(true);
    }
}
exports.updateUnServedTasksFromRoutific = function (data, callback) {
    if (data.unserved.num_unserved) {
        var k, unserved_tasks = [];
        for (k in data.unserved.unserved) {
            unserved_tasks.push(k);
        }
        async.map(unserved_tasks, function (da, cb) {
            da = da.replace('_pickup', '');
            da = da.replace('_dropoff', '');
            var sql = "UPDATE tb_jobs SET `is_routed`=0 " +
                " WHERE `pickup_delivery_relationship`=? " +
                " AND `user_id` = ?";
            connection.query(sql, [da, data.user_id], function (err, result) {
                cb(true);
            })
        }, function (err, results) {
            callback(true);
        });
    } else {
        callback(true);
    }
}
/*
 * ---------------------------
 * UPDATE TASK ROUTED STATUS
 * ---------------------------
 */
exports.updateRoutedStatusForTasks = function (data, callback) {
    async.map(data.fleet_tasks, function (da, cb) {
        var req = {};
        req.body = {
            job_id: da.job_id,
            fleet_id: da.fleet_id,
            access_token: data.access_token,
            is_routed: 1,
            notify: da.notify
        };
        if (da.team_id) {
            req.body.team_id = da.team_id
        }
        var resp = {
            send: function (obj) {
                cb(null, obj)
            }
        }
        jobs.edit_task(req, resp);
    }, function (err, results) {
        callback(true);
    });
};
/*
 * ------------------------------------------------------
 * GET FLEET MOVEMENT BETWEEN TASK STARTS TILL COMPLETION
 * ------------------------------------------------------
 */
exports.getFleetMovement = function (reduce, fleet_id, start, end, task, callback) {
    var sql, bindParams;
    if (start == '') {
        var response = {
            "fleet_movement": [],
            "task": task
        }
        return callback(response);
    } else {
        if (end == '') {
            sql = "SELECT `latitude` , `longitude` FROM `tb_fleet_movements` WHERE `fleet_id` = ? and `creation_datetime`>=? and `creation_datetime`<=NOW()";
            bindParams = [fleet_id, start];
        } else {
            sql = "SELECT `latitude` , `longitude` FROM `tb_fleet_movements` WHERE `fleet_id` = ? and `creation_datetime`>=? and `creation_datetime`<=? ";
            bindParams = [fleet_id, start, end];
        }
        connection.query(sql, bindParams, function (err, result) {
            if (result && result.length > 0) {
                if (reduce) {
                    var history_length = parseInt(Math.round(result.length / 50));
                    if (history_length > 1) {
                        result = result.filter(function (el, index) {
                            return index % history_length === 1;
                        });
                    }
                    var task_history_image = 'https://maps.googleapis.com/maps/api/staticmap?size=800x400&format=pngg&sensor=false&path=color%3a0x0000FF99%7Cweight:7';
                    result.forEach(function (value) {
                        task_history_image = task_history_image + '%7C' + value.latitude + ',' + value.longitude;
                    });
                }
                var response = {
                    "fleet_movement": result,
                    "task": task,
                    "image": task_history_image
                }
                return callback(response);
            } else {
                var response = {
                    "fleet_movement": [],
                    "task": task,
                    "image": ""
                }
                return callback(response);
            }
        });
    }
};
/*
 * ---------------------------
 * GET USER DETAILS FROM USERID
 * ---------------------------
 */
exports.getUserDetails = function (userID, callback) {
    var sql = "SELECT * FROM `tb_users` WHERE `user_id` = ? LIMIT 1";
    connection.query(sql, [userID], function (err, result) {
        if (result && result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};

/*
 * -----------------------------------
 * INSERT TEMPLATE INFORMATION OF USER
 * -----------------------------------
 */
exports.insertTemplateInformation = function (userID) {
    var template_key, email_message, email_subject, sms_text;
    var variable_names, email_enabled, sms_enabled, layout_type;
    for (var i = 0; i < 20; i++) {

        // DELIVERY
        if (i == 0) {
            template_key = 'REQUEST_RECEIVED';
            email_message = 'Hi [CustomerName],<br><br>Your request has been received and it is scheduled for delivery on [EndDate] before [EndTime].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your request has been received!';
            sms_text = 'Hi [CustomerName], your request has been received and it is scheduled for delivery on [EndDate] before [EndTime].';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 1, sms_enabled = 1, layout_type = 1;
        } else if (i == 1) {
            template_key = 'AGENT_STARTED';
            email_message = 'Hey [CustomerName],<br><br>Your order is on it’s way. To view [AgentName]’s location live on the map open <br>[TrackingLink]<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your order is on it’s way!';
            sms_text = 'Hey [CustomerName] your order is on it’s way. To view [AgentName]’s location live on the map open [TrackingLink]';
            variable_names = ['[TaskID]', '[AgentName]', '[AgentPhone]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 1, sms_enabled = 1, layout_type = 1;
        } else if (i == 2) {
            template_key = 'AGENT_ARRIVED';
            email_message = 'Hi [CustomerName],<br><br>Your order has reached it`s destination. Please collect your order from [AgentName].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your order has reached it’s destination!';
            sms_text = 'Hi [CustomerName]! your order has reached it’s destination. Please collect your order from [AgentName].';
            variable_names = ['[TaskID]', '[AgentName]', '[AgentPhone]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 0, sms_enabled = 0, layout_type = 1;
        } else if (i == 3) {
            template_key = 'SUCCESSFUL';
            email_message = 'Hi [CustomerName],<br><br>Your order was successfully delivered today at [CompletedTime]. Please rate your experience <br>[TrackingLink]<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your order has been delivered!';
            sms_text = 'Hi [CustomerName]! Your order was successfully delivered today at [CompletedTime]. Please rate your experience [TrackingLink]';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]', '[CompletedDate]', '[CompletedTime]', '[TotalTimeTaken]', '[TotalDistanceTravelled]', '[SignImage]'];
            email_enabled = 1, sms_enabled = 1, layout_type = 1;
        } else if (i == 4) {
            template_key = 'FAILED';
            email_message = 'Hi [CustomerName],<br><br>We tried, but were unable to deliver your today at [CompletedTime]. Please contact us at [ManagerNumber].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'We tried, but were unable to deliver your today';
            sms_text = 'We tried, but were unable to deliver your today at [CompletedTime]. Please contact us at [ManagerNumber].';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]', '[CompletedDate]', '[CompletedTime]', '[TotalTimeTaken]', '[TotalDistanceTravelled]', '[ManagerNumber]'];
            email_enabled = 0, sms_enabled = 0, layout_type = 1;
        }
        // PICKUP
        if (i == 5) {
            template_key = 'REQUEST_RECEIVED';
            email_message = 'Hi [CustomerName],<br><br>Your pick-up request has been received and it is scheduled for [StartDate] before [StartTime].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your pick-up request has been received!';
            sms_text = 'Hi [CustomerName], your pick-up request has been received and it is scheduled for [StartDate] before [StartTime].';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 1, sms_enabled = 1, layout_type = 0;
        } else if (i == 6) {
            template_key = 'AGENT_STARTED';
            email_message = 'Hey [CustomerName],<br><br>Our member [AgentName] is on it’s way. To track the location live on the map open <br>[TrackingLink]<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Our member [AgentName] is on it’s way!';
            sms_text = 'Hi [CustomerName], our member [AgentName] is on it’s way. To track the location live on the map open [TrackingLink]';
            variable_names = ['[TaskID]', '[AgentName]', '[AgentPhone]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 1, sms_enabled = 1, layout_type = 0;
        } else if (i == 7) {
            template_key = 'AGENT_ARRIVED';
            email_message = 'Hi [CustomerName],<br><br>Our fleet member [AgentName] has reached the destination. Please say hi and handover the pick-up consignment.<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Our fleet member [AgentName] has reached it’s destination!';
            sms_text = 'Hi [CustomerName], our fleet member [AgentName] has reached the destination. Please say hi and handover the pick-up consignment.';
            variable_names = ['[TaskID]', '[AgentName]', '[AgentPhone]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 0, sms_enabled = 0, layout_type = 0;
        } else if (i == 8) {
            template_key = 'SUCCESSFUL';
            email_message = 'Hi [CustomerName],<br><br>Your consignment was successfully picked up today at [CompletedTime]. Please rate your experience <br>[TrackingLink]<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your consignment was successfully picked!';
            sms_text = 'Hi [CustomerName]! Your consignment was successfully picked up today at [CompletedTime]. Please rate your experience [TrackingLink]';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]', '[CompletedDate]', '[CompletedTime]', '[TotalTimeTaken]', '[TotalDistanceTravelled]', '[SignImage]'];
            email_enabled = 1, sms_enabled = 1, layout_type = 0;
        } else if (i == 9) {
            template_key = 'FAILED';
            email_message = 'Hi [CustomerName],<br><br>We tried, but were unable to pick-up your consignment today at [CompletedTime]. Please contact us at [ManagerNumber].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'We tried, but were unable to pick-up your consignment';
            sms_text = 'Hi [CustomerName]! We tried, but were unable to pick-up your consignment today at [CompletedTime]. Please contact us at [ManagerNumber]';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]', '[CompletedDate]', '[CompletedTime]', '[TotalTimeTaken]', '[TotalDistanceTravelled]', '[ManagerNumber]'];
            email_enabled = 0, sms_enabled = 0, layout_type = 0;
        }


        // APPOINTMENT
        if (i == 10) {
            template_key = 'REQUEST_RECEIVED';
            email_message = 'Hi [CustomerName],<br><br>Your service request has been received and it is scheduled for [StartDate] before [StartTime].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your service request has been received!';
            sms_text = 'Hi [CustomerName], your service request has been received and it is scheduled for [StartDate] before [StartTime].';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 0, sms_enabled = 1, layout_type = 3;
        } else if (i == 11) {
            template_key = 'AGENT_STARTED';
            email_message = 'Hey [CustomerName],<br><br>Our service agent [AgentName] is on it’s way. To track the location live on the map open <br>[TrackingLink]<br>Best,<br>Team [CompanyName]';
            email_subject = 'Our service agent [AgentName] is on it’s way!';
            sms_text = 'Hi [CustomerName], our service agent [AgentName] is on it’s way. To track the location live on the map open [TrackingLink]';
            variable_names = ['[TaskID]', '[AgentName]', '[AgentPhone]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 0, sms_enabled = 1, layout_type = 3;
        } else if (i == 12) {
            template_key = 'AGENT_ARRIVED';
            email_message = 'Hi [CustomerName],<br><br>Our service agent [AgentName] has reached the destination. Please say hi.<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Our service agent [AgentName] has reached it’s destination!';
            sms_text = 'Hi [CustomerName], our service agent [AgentName] has reached the destination. Please say hi and handover the pick-up consignment.';
            variable_names = ['[TaskID]', '[AgentName]', '[AgentPhone]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 0, sms_enabled = 0, layout_type = 3;
        } else if (i == 13) {
            template_key = 'SUCCESSFUL';
            email_message = 'Hi [CustomerName],<br><br>Your service was successfully completed today at [CompletedTime]. Please rate your experience <br>[TrackingLink]<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your service was successfully complete!';
            sms_text = 'Hi [CustomerName]! Your service was successfully complete up today at [CompletedTime]. Please rate your experience [TrackingLink]';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]', '[CompletedDate]', '[CompletedTime]', '[TotalTimeTaken]', '[TotalDistanceTravelled]', '[SignImage]'];
            email_enabled = 0, sms_enabled = 1, layout_type = 3;
        } else if (i == 14) {
            template_key = 'FAILED';
            email_message = 'Hi [CustomerName],<br><br>We tried, but were unable to complete your service request today at [CompletedTime]. Please contact us at [ManagerNumber].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'We tried, but were unable to complete your service request';
            sms_text = 'Hi [CustomerName]! We tried, but were unable to complete your service request today at [CompletedTime]. Please contact us at [ManagerNumber]';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]', '[CompletedDate]', '[CompletedTime]', '[TotalTimeTaken]', '[TotalDistanceTravelled]', '[ManagerNumber]'];
            email_enabled = 0, sms_enabled = 0, layout_type = 3;
        }

        // FIELD WORKFLOW
        if (i == 15) {
            template_key = 'REQUEST_RECEIVED';
            email_message = 'Hi [CustomerName],<br><br>Your service request has been received and it is scheduled for [StartDate] before [StartTime].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your service request has been received!';
            sms_text = 'Hi [CustomerName], your service request has been received and it is scheduled for [StartDate] before [StartTime].';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 0, sms_enabled = 1, layout_type = 2;
        } else if (i == 16) {
            template_key = 'AGENT_STARTED';
            email_message = 'Hey [CustomerName],<br><br>Our agent [AgentName] is on it`s way. To track the location live on the map open <br>[TrackingLink]<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Our agent [AgentName] is on it’s way!';
            sms_text = 'Hi [CustomerName], our agent [AgentName] is on it’s way. To track the location live on the map open [TrackingLink]';
            variable_names = ['[TaskID]', '[AgentName]', '[AgentPhone]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 0, sms_enabled = 1, layout_type = 2;
        } else if (i == 17) {
            template_key = 'AGENT_ARRIVED';
            email_message = 'Hi [CustomerName],<br><br>Our agent [AgentName] has reached the destination. Please say hi.<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Our agent [AgentName] has reached it’s destination!';
            sms_text = 'Hi [CustomerName], our agent [AgentName] has reached the destination. Please say hi and handover the pick-up consignment.';
            variable_names = ['[TaskID]', '[AgentName]', '[AgentPhone]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]'];
            email_enabled = 0, sms_enabled = 0, layout_type = 2;
        } else if (i == 18) {
            template_key = 'SUCCESSFUL';
            email_message = 'Hi [CustomerName],<br><br>Your service was successfully completed today at [CompletedTime]. Please rate your experience <br>[TrackingLink]<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'Your service was successfully complete!';
            sms_text = 'Hi [CustomerName]! Your service was successfully complete up today at [CompletedTime]. Please rate your experience [TrackingLink]';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]', '[CompletedDate]', '[CompletedTime]', '[TotalTimeTaken]', '[TotalDistanceTravelled]', '[SignImage]'];
            email_enabled = 0, sms_enabled = 1, layout_type = 2;
        } else if (i == 19) {
            template_key = 'FAILED';
            email_message = 'Hi [CustomerName],<br><br>We tried, but were unable to complete your service request today at [CompletedTime]. Please contact us at [ManagerNumber].<br><br>Best,<br>Team [CompanyName]';
            email_subject = 'We tried, but were unable to complete your service request';
            sms_text = 'Hi [CustomerName]! We tried, but were unable to complete your service request today at [CompletedTime]. Please contact us at [ManagerNumber]';
            variable_names = ['[TaskID]', '[CustomerName]', '[CustomerAddress]', '[CompanyName]', '[TrackingLink]', '[StartDate]', '[StartTime]', '[EndDate]', '[EndTime]', '[CompletedDate]', '[CompletedTime]', '[TotalTimeTaken]', '[TotalDistanceTravelled]', '[ManagerNumber]'];
            email_enabled = 0, sms_enabled = 0, layout_type = 2;
        }

        insertIntoTemplateTables(userID, template_key, email_message, email_subject, sms_text, variable_names, email_enabled, sms_enabled, layout_type);
    }
};


function insertIntoTemplateTables(userID, template_key, email_message, email_subject, sms_text, variable_names, email_enabled, sms_enabled, layout_type) {
    var sql = "INSERT INTO `tb_templates` (`template_key`,`email_text`,`email_subject`,`sms_text`,`user_id`,`email_enabled`,`sms_enabled`,`layout_type`) VALUES (?,?,?,?,?,?,?,?)";
    connection.query(sql, [template_key, email_message, email_subject, sms_text, userID, email_enabled, sms_enabled, layout_type], function (err, result) {
        var variable_names_length = variable_names.length;
        for (var j = 0; j < variable_names_length; j++) {
            var sql = "INSERT INTO `tb_template_variables` (`template_id`,`variable_name`) VALUES (?,?)";
            connection.query(sql, [result.insertId, variable_names[j]], function (err, result) {

            });
        }
    });
}

/*
 *------------------
 * ADD ONE HOUR
 *------------------
 */
exports.addHOUR = function (date) {
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() + 3600000); // add an hour
    return new Date(newDate);
}
/*
 *------------------
 * ADD ETA SECONDS
 *------------------
 */
exports.addSeconds = function (date, seconds) {
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() + (seconds * 1000)); // add seconds
    return new Date(newDate)
}

/*
 *------------------
 * SUBTRACT ONE HOUR
 *------------------
 */
exports.subtractHOUR = function (date) {
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() - 3600000); // subtract an hour
    return new Date(newDate);
}

/*
 *------------------
 * millisecondsToStr
 *------------------
 */
exports.millisecondsToStr = function (milliseconds) {
    // TIP: to find current time in milliseconds, use:
    // var  current_time_milliseconds = new Date().getTime();

    function numberEnding(number) {
        return (number > 1) ? 's' : '';
    }

    var temp = Math.floor(milliseconds / 1000);
    var years = Math.floor(temp / 31536000);
    if (years) {
        return years + ' year' + numberEnding(years);
    }
    //TODO: Months! Maybe weeks?
    var days = Math.floor((temp %= 31536000) / 86400);
    if (days) {
        return days + ' day' + numberEnding(days);
    }
    var hours = Math.floor((temp %= 86400) / 3600);
    if (hours) {
        return hours + ' hour' + numberEnding(hours);
    }
    var minutes = Math.floor((temp %= 3600) / 60);
    if (minutes) {
        return minutes + ' minute' + numberEnding(minutes);
    }
    var seconds = temp % 60;
    if (seconds) {
        return seconds + ' second' + numberEnding(seconds);
    }
    return 'less than a second'; //'just now' //or other string you like;
}

/*
 *------------------
 * metersTOunit
 *------------------
 */
exports.metersToUnit = function (metres, unit) {

    function numberEnding(number) {
        return (number > 1) ? 's' : '';
    }

    if (unit.toLowerCase() == 'mile') {
        var mile = (metres / 1609.344000000865).toFixed(2);
        return mile + ' Mile' + numberEnding(mile);
    } else {
        var km = (metres / 1000).toFixed(2);
        return km + ' Km' + numberEnding(km);
    }
}
/*
 * -----------------------------------------------
 * AUTHENTICATE ACTIVE JOBID AND FLEET
 * -----------------------------------------------
 */
exports.authenticateActiveFleetIDAndJobID = function (fleet_id, fleet_array, callback) {
    var sql = "SELECT jobs.*, customer.* FROM `tb_task_session` tb_task_s " +
        " inner join tb_jobs jobs on jobs.job_id = tb_task_s.job_id left join tb_customers customer on customer.customer_id = jobs.customer_id " +
        " WHERE tb_task_s.`fleet_id`=? and tb_task_s.`job_acknowledged`=? ";
    connection.query(sql, [fleet_id, 0], function (err, result) {
        if (result && result.length > 0) {
            var response = {
                "active_task": result,
                "fleet": fleet_array
            }
            return callback(response);
        } else {
            var response = {
                "active_task": [],
                "fleet": fleet_array
            }
            return callback(response);
        }
    });
};

/*---------------------------
 * Generate and Save PDF File
 * ---------------------------
 */
exports.generateAndSavePdf = function (htmlData, htmlFilePath, filePath, callback) {
    var fs = require("fs");
    fs.writeFile(htmlFilePath, htmlData, function (err, data) {
        console.log(err, data);
        if (err) {
            logging.addErrorLog(err, null, null);
        }
        else {
            phantom.create(function (ph) {
                ph.createPage(function (page) {
                    page.set('viewportSize', {width: 1440, height: 2036});
                    page.open(htmlFilePath, function (status) {
                        page.render(filePath, function () {
                            ph.exit();
                            return callback(true);
                        });
                    });
                });
            });
        }
    });
}

/*---------------------------
 * SEND EMAIL WITH ATTACHMENT
 * ---------------------------
 */
exports.sendEmailWithAttachment = function (filePath, receiverMailId, html, subject, from, callback) {

    var attachments = [];
    filePath = filePath.split('spaces');
    filePath.forEach(function (f) {
        attachments.push({
            path: f
        })
    })
    if (smtpTransport === undefined) {
        smtpTransport = nodemailer.createTransport({
            host: config.get('emailCredentials.host'),
            port: config.get('emailCredentials.port'),
            auth: {
                user: config.get('emailCredentials.senderEmail'),
                pass: config.get('emailCredentials.senderPassword')
            }
        });
    }
    receiverMailId = removeInvalidIds(receiverMailId);

    if (typeof (from) === undefined || from == null || from == "") {
        from = config.get('emailCredentials.From');
    }

    // setup e-mail data with unicode symbols
    var mailOptions = {
        from: from, // sender address
        to: receiverMailId, // list of receivers
        subject: subject, // Subject line
        html: html,// html body,
        attachments: attachments
    }

    // send mail with defined transport object
    if (receiverMailId.length > 0) {

        smtpTransport.sendMail(mailOptions, function (error, response) {
            console.log("Sending Mail Error: " + JSON.stringify(error));
            console.log("Sending Mail Response: " + JSON.stringify(response));
            if (error) {
                return callback(0);
            } else {
                return callback(1);
            }
        });
    }

    // if you don't want to use this transport object anymore, uncomment following line
    //smtpTransport.close(); // shut down the connection pool, no more messages
};

exports.calculateAvgRating = function (rating, tasks) {
    if (rating == 0 && tasks == 0) {
        return 0;
    } else {
        return Math.ceil(rating / tasks);
    }
}
exports.sendUsTaskCreationEmail = function (message, subject) {
    if (process.env.NODE_ENV === 'live') {
        async.parallel([
                function (callback) {
                    commonFunc.sendHtmlContent(constants.EmailIds.SANJAY, message, subject, "", function (new_job_creation_result) {
                        callback(null, new_job_creation_result)
                    });
                },
                function (callback) {
                    commonFunc.sendHtmlContent(constants.EmailIds.ARSH, message, subject, "", function (new_job_creation_result) {
                        callback(null, new_job_creation_result)
                    });
                },
                function (callback) {
                    commonFunc.sendHtmlContent(constants.EmailIds.SUMEET, message, subject, "", function (new_job_creation_result) {
                        callback(null, new_job_creation_result)
                    });
                }],
            function (err, results) {

            });
    }
}

/*
 * -----------------------------------------------
 * UPDATE FLEET MOVEMENTS
 * -----------------------------------------------
 */
exports.updateFleetMovement = function (str) {
    if (str) {
        var sql = "INSERT INTO `tb_fleet_movements` (`fleet_id`,`latitude`,`longitude`) " +
            "VALUES " + str + " ";
        connection2.query(sql, function (err, result) {
        });
    }
};

/*
 * -----------------------------------------------
 * UPDATE FLEET LOCATION
 * -----------------------------------------------
 */
exports.updateFleetLocation = function (fleet_id, data) {
    var sql = "UPDATE `tb_fleets` SET `latitude`=?,`longitude`=?,`battery_level`=?,`has_gps_accuracy`=?,`has_mock_loc`=?," +
        " `has_network`=?,`location_update_datetime`=NOW() WHERE `fleet_id`=? LIMIT 1";
    connection2.query(sql, [data.lat, data.lng, data.bat_lvl, data.gps, data.mock, data.net, fleet_id], function (err, result) {
        socketResponse.sendFleetSocketResponseFromFleetID(fleet_id, data);
    });
};

exports.insertTotalDistanceTravelled = function (job_id) {
    var sql = "select * from  `tb_jobs` WHERE `job_id`=? LIMIT 1";
    connection.query(sql, [job_id], function (err, result) {
        if (result && result.length > 0) {
            if (result[0].fleet_id) {
                if (result[0].started_datetime == "0000-00-00 00:00:00") {
                    result[0].started_datetime = "";
                }
                if (result[0].completed_datetime == "0000-00-00 00:00:00") {
                    result[0].completed_datetime = "";
                }
                module.exports.getFleetMovement(0, result[0].fleet_id, result[0].started_datetime, result[0].completed_datetime, result, function (distance_travelled) {
                    var fleetMovementLength = distance_travelled.fleet_movement.length;
                    if (fleetMovementLength > 0) {
                        var distance = 0;
                        for (var j = 0; j < fleetMovementLength - 1; j++) {
                            commonFunc.calculateDistance(distance_travelled.fleet_movement[j].latitude, distance_travelled.fleet_movement[j].longitude,
                                distance_travelled.fleet_movement[j + 1].latitude, distance_travelled.fleet_movement[j + 1].longitude, function (calculateDistanceResult) {
                                    distance += calculateDistanceResult;
                                });
                        }
                        var sql = "UPDATE `tb_jobs` SET `total_distance_travelled`=? WHERE `job_id`=? LIMIT 1";
                        connection.query(sql, [distance, job_id], function (err, result) {
                        });
                    }
                })
            }
        }
    });
}

/*
 * --------------------------------------------------
 * ADD CUSTOMER TO AGILE CRM
 * --------------------------------------------------
 */
exports.addToAgileCRM = function (req) {
    if (process.env.NODE_ENV === 'live') {
        var name = req.body.name.split(' ');
        var tags = '[ "direct-signup" ]';
        var property = '[ { "type": "SYSTEM", "name": "first_name", "value": "' + name[0] + '" },' +
            '{ "type": "SYSTEM", "name": "last_name", "value": "' + name[1] + '" },' +
            '{ "type": "SYSTEM", "name": "phone", "value": "' + req.body.phone + '" },' +
            '{ "type": "SYSTEM", "name": "email", "value": "' + req.body.email + '" },' +
            '{ "type": "SYSTEM", "name": "address", "value": "' + req.body.company_address + '" },' +
            '{ "type": "CUSTOM", "name": "source", "value": "' + req.body.source + '" },' +
            '{ "type": "CUSTOM", "name": "medium", "value": "' + req.body.medium + '" },' +
            '{ "type": "SYSTEM", "name": "company", "value": "' + req.body.company_name + '" } ]'
        var sendRequest = 'curl https://tookan.agilecrm.com/dev/api/contacts  -H "Content-Type: application/json"  ' +
            '-d \'{ "tags": ' + tags + ' , "properties": ' + property + ' }\'  -v -u saral@tookanapp.com:fc8607rqpkgni7f5iivr46f1qp -X POST';
        var exec = require('child_process').exec;
        exec(sendRequest, function (err, stdout, stderr) {
        });
    }
};
/*
 * -------------------------------------
 * ADD TO mail.jugnoo subscriber list
 * --------------------------------------
 */
exports.addToSubscriberList = function (req) {
    if (process.env.NODE_ENV === 'live') {
        var needle = require('needle');
        needle.post('http://tookanapp.com/wp-content/themes/wpclicklabs/cta/tookan_sendy.php', {
                name: req.body.first_name,
                email: req.body.email,
                list: 414
            },
            function (err, resp, body) {
                console.log(body);
            });
    }
};

exports.delay = function (arg, callback) {
    setTimeout(function () {
        callback(arg);
    }, 300);
}
exports.delay2 = function (arg, callback) {
    setTimeout(function () {
        callback(arg);
    }, 5);
}

exports.fleetPendingTasks = function (fleet_id, date, fleet_array, callback) {
    var sql = "SELECT jobs.`timezone`,DATE(jobs.`job_time`) as `job_time`,jobs.`job_id`,jobs.`job_status`,jobs.`pickup_delivery_relationship` " +
        "FROM `tb_jobs` jobs " +
        "WHERE jobs.`fleet_id`=? AND DATE(jobs.`job_time`) = ? ";
    connection.query(sql, [fleet_id, date], function (err, result_jobs) {
        if (err) {
            logging.logDatabaseQueryError("Error in viewing fleet info : ", err, result_jobs);
        } else {
            result_jobs = _lodash.uniq(result_jobs, 'pickup_delivery_relationship');
            var result_jobs_length = result_jobs.length;
            var incomplete = 0;
            if (result_jobs_length > 0) {
                for (var i = 0; i < result_jobs_length; i++) {
                    if ((result_jobs[i].job_status != constants.jobStatus.END) && (result_jobs[i].job_status != constants.jobStatus.FAILED)
                        && (result_jobs[i].job_status != constants.jobStatus.CANCEL)) {
                        incomplete++;
                    }
                }
                var response = {
                    "incomplete": incomplete,
                    "fleet": fleet_array
                }
                return callback(response);
            } else {
                var response = {
                    "incomplete": 0,
                    "fleet": fleet_array
                }
                return callback(response);
            }
        }
    });
}

exports.formatPhoneNumber = function (phoneNumber, phoneCode) {
    try {
        var phone = require('node-phonenumber');
        var phoneUtil = phone.PhoneNumberUtil.getInstance();
        var phoneNumber = phoneUtil.parse(phoneNumber, phoneCode);
        var toNumber = phoneUtil.format(phoneNumber, phone.PhoneNumberFormat.INTERNATIONAL);
        return toNumber;
    } catch (e) {
        return phoneNumber;
    }
}

exports.eliminateCharacters = function (data) {
    var datalength = data.length;
    for (var i = 0; i < datalength; i++) {
        var k, keys = Object.keys(data[i]);
        for (k in  keys) {
            if ((data[i].hasOwnProperty(keys[k])) && (typeof data[i][keys[k]] === 'string')) {
                data[i][keys[k]] = (data[i][keys[k]]).replace(/'/g, "`");
                data[i][keys[k]] = (data[i][keys[k]]).replace(/"/g, "`");
            }
        }
    }
    return data;
}

exports.convertToDateString = function (value) {
    var monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    value = value.insert(-2, '-').split('-');
    return monthNames[value[1] - 1] + ' ' + value[0];
}

String.prototype.insert = function (index, string) {
    var ind = index < 0 ? this.length + index : index;
    return this.substring(0, ind) + string + this.substring(ind, this.length);
};

exports.rangeWeek = function (dateStr) {
    var dt = new Date();
    var end = moment(dt).format("dddd, MMMM Do");
    dt.setDate(dt.getDate() - dateStr);
    var start = moment(dt).format("dddd, MMMM Do");
    return start + " - " + end;
}

var task_mail_enabled = false;
exports.sendTaskInfoToDispatcher = function (job_id) {
    if (task_mail_enabled) {
        var sql = "SELECT * FROM tb_jobs WHERE job_id = ? LIMIT 1";
        connection.query(sql, [job_id], function (err, jobs) {
            if (err) {
                console.log("ERROR in sending mail to dispatcher......", jobs);
            }
            if (jobs && jobs.length > 0) {
                var job = jobs[0];
                if (job.team_id && (job.dispatcher_id == null)) {
                    var sql = "SELECT u.* FROM `tb_users` u left join tb_dispatcher_teams d " +
                        "on u.user_id = d.dispatcher_id  WHERE d.team_id = ?";
                    connection.query(sql, [job.team_id], function (err, user) {
                        user.forEach(function (ur) {
                            var msg = "A new task task has been assigned to your team by admin.";
                            commonFunc.emailPlainFormatting(ur.username, msg, '', '', '', function (returnMessage) {
                                commonFunc.sendHtmlContent(ur.email, returnMessage, "New task has assigned", "", function (result) {

                                });
                            });
                        });
                    });
                }
            }
        })
    }
}

exports.getGoogleShortenUrl = function (data, callback) {
    try {
        var googl = require('goo.gl');
        googl.setKey(config.get('googleCredentials.api_key'));

        // Shorten a long url and output the result
        googl.shorten(data)
            .then(function (shortUrl) {
                callback(shortUrl);
            })
            .catch(function (err) {
                callback(data);
            });

    } catch (e) {
        callback(data);
    }
}

exports.reverseTaskHistory = function (type) {
    if (type == "text_added") {
        return "Note Added"
    }
    if (type == "signature_image_added") {
        return "Signature Image Added"
    }
    if (type == "signature_image_updated") {
        return "Signature Image Updated"
    }
    if (type == "image_added") {
        return "Image Added"
    }
    if (type == "custom_field_updated") {
        return "Custom Field Updated"
    } else {
        return type
    }
}

exports.uploadImageToBucketFromURL = function (file, folder, filename, callback) {
    var fs = require('node-fs');
    var AWS = require('aws-sdk');
    var accessKeyId = config.get('s3BucketCredentials.accessKeyId');
    var secretAccessKeyId = config.get('s3BucketCredentials.secretAccessKey');
    var bucketName = config.get('s3BucketCredentials.bucket');

    fs.readFile(file, function (error, file_buffer) {
        console.log("error", error)
        if (error) {
            return callback(0);
        }
        AWS.config.update({accessKeyId: accessKeyId, secretAccessKey: secretAccessKeyId});
        var s3bucket = new AWS.S3();
        var params = {
            Bucket: bucketName,
            Key: folder + '/' + filename,
            Body: file_buffer,
            ACL: 'public-read',
            ContentType: 'application/pdf'
        };
        s3bucket.putObject(params, function (err, data) {
            console.log("err", err)
            if (err) {
                return callback(0);
            }
            else {
                return callback(filename);
            }
        });
    });

}

/*
 * ---------------------------------------------------
 * GET FLEET INFO IF ASSOCIATED WITH ANY ANOTHER TEAM
 * ---------------------------------------------------
 */
exports.getFleetIfAlreadyWithTeam = function (fleet_id, user_id, callback) {
    var sql = "SELECT * FROM `tb_fleet_teams` " +
        " WHERE `fleet_id`=? and `user_id`=? LIMIT 1";
    connection.query(sql, [fleet_id, user_id], function (err, result) {
        var response = {
            fleet_id: fleet_id
        }
        if (result && result.length > 0) {
            response.data = result
            return callback(response);
        } else {
            response.data = 0
            return callback(response);
        }
    });
};

exports.fileDelete = function (file) {
    var fs = require('fs');
    fs.exists(file, function (exists) {
        if (exists) {
            try {
                fs.unlinkSync(file);
            } catch (e) {
                console.log("Unable to delete file", file);
            }
        }
    });
}

/*
 * ---------------------------------------------------
 * Process Custom field in case of custom changes
 * ---------------------------------------------------
 */
exports.processCustomField = function (data) {
    if (data.data_type == "Checklist") {
        if (data.data) {
            try {
                var d = JSON.parse(data.data);
            } catch (e) {
                var d = []
            }
            var dat = []
            d.forEach(function (s) {
                if (s.check == "true") {
                    dat.push(s.value);
                }
            })
            data.data = JSON.stringify(dat.join(', '));
        }
        if (data.fleet_data) {
            try {
                var f = JSON.parse(data.fleet_data);
            } catch (e) {
                var f = []
            }
            var fdat = []
            f.forEach(function (s) {
                if (s.check == "true") {
                    fdat.push(s.value);
                }
            })
            data.fleet_data = JSON.stringify(fdat.join(', '));
        }
        return data;
    } else {
        return data;
    }
}


/*
 * -----------------
 * GET VERSION DATA
 * -----------------
 */
exports.getVersion2 = function (user_id, callback) {
    var sql = "SELECT * " +
        "FROM `tb_version2` " +
        "WHERE `user_id`=?";
    connection.query(sql, [user_id], function (err, versions) {
        callback(versions);
    });
};

/*
 * -----------------
 * GET ADD ONS
 * -----------------
 */
exports.get_add_ons = function (user_id, add_on_type, template_id, callback) {
    var sql = "SELECT * " +
        "FROM `tb_user_add_ons` " +
        "WHERE `user_id`=? and `add_on_type`=? and `template_id`=? and `is_active`=1";
    connection.query(sql, [user_id, add_on_type, template_id], function (err, add_on) {
        if (add_on && add_on.length) {
            callback(add_on);
        } else {
            callback(0);
        }
    });
};