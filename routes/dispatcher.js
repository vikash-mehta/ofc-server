var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var async = require('async');

/*
 * ----------------------------------
 * CREATION OF NEW DISPATCHER
 * ----------------------------------
 */

exports.add_dispatcher = function (req, res) {

    var access_token = req.body.access_token;
    var email = req.body.email;
    var name = req.body.name;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var phone = req.body.phone;
    var timezone = req.body.timezone;
    var task_access = req.body.task_access;
    var add_driver_access = req.body.add_driver_access;
    var dispatcher_teams = req.body.dispatcher_teams;
    if (typeof(dispatcher_teams) === "undefined") {
        dispatcher_teams = 0
    }
    var manvalues = [first_name, name, email, access_token, timezone, add_driver_access, task_access, phone, dispatcher_teams];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var dispatcher_id = null;
        async.waterfall([
                function (cb) {
                    commonFunc.authenticateUserAccessToken(access_token, function (user) {
                        if (user == 0) {
                            cb(null, {
                                err: 1
                            });
                        } else {
                            cb(null, user);
                        }
                    });
                },
                function (user, cb) {
                    if (user.err) {
                        cb(null, user);
                    } else {
                        commonFunc.checkAccountExpiry(user[0].user_id, function (checkExp) {
                            if (checkExp) {
                                cb(null, {
                                    err: 2
                                });
                            } else {
                                cb(null, user);
                            }
                        });
                    }
                },
                function (user, cb) {
                    if (user.err) {
                        cb(null, user);
                    } else {
                        if (user[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            cb(null, {
                                err: 3
                            });
                        } else {
                            cb(null, user);
                        }
                    }
                },
                function (user, cb) {
                    if (user.err) {
                        cb(null, user);
                    } else {
                        commonFunc.authenticateUserEmail(email, function (auth) {
                            if (auth != 0) {
                                cb(null, {
                                    err: 4
                                });
                            } else {
                                cb(null, user);
                            }
                        });
                    }
                },
                function (user, cb) {
                    if (user.err) {
                        cb(null, user);
                    } else {
                        user[0].brand_name = config.get('projectName');
                        if (user[0].is_whitelabel) {
                            commonFunc.getVersion2(user[0].user_id, function (versions) {
                                if (versions && versions.length) {
                                    versions.forEach(function (vr) {
                                        user[0].brand_name = vr.brand_name;
                                    })
                                    cb(null, user);
                                } else {
                                    cb(null, user);
                                }
                            })
                        } else {
                            cb(null, user);
                        }
                    }
                },
                function (user, cb) {
                    if (user.err) {
                        cb(null, user);
                    } else {

                        var verification_token = md5(email);
                        user[0].randomNumber = commonFunc.generateRandomString();
                        var encrypted_pass = md5(user[0].brand_name.split(' ')[0] + user[0].randomNumber);
                        var access_token = md5(encrypted_pass + new Date());

                        var sql = "INSERT INTO `tb_users` (`first_name`,`last_name`,`billing_plan`,`layout_type`,`phone`,`is_dispatcher`,`dispatcher_user_id`,`username`,`company_name`,";
                        sql += "`country`,`country_phone_code`,`company_address`,`access_token`,`email`,`password`,`is_whitelabel`,`brand_image`,`has_routing`,";
                        sql += "`verification_token`,`verification_status`,`expiry_datetime`,`last_login_datetime`,`timezone`,`fav_icon`,`domain`)";
                        sql += " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                        connection.query(sql, [first_name, last_name, user[0].billing_plan, user[0].layout_type, phone, constants.isDispatcherStatus.YES, user[0].user_id, name,
                            user[0].company_name, user[0].country, user[0].country_phone_code, user[0].company_address, access_token, email, encrypted_pass,
                            user[0].is_whitelabel, user[0].brand_image, user[0].has_routing, verification_token, constants.userVerificationStatus.VERIFY,
                            user[0].expiry_datetime, new Date(), timezone, user[0].fav_icon, user[0].domain], function (err, result_insert) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in inserting user info : ", err, result_insert);
                                cb(null, {
                                    err: 5
                                });
                            } else {
                                user[0].dispatcher = result_insert.insertId;
                                cb(null, user);
                            }
                        });
                    }
                },
                function (user, cb) {
                    if (user.err) {
                        cb(null, user);
                    } else {

                        var webAppLink = config.get('webAppLink');
                        if (user[0].domain) webAppLink = user[0].domain;
                        var msg = 'Welcome to ' + user[0].brand_name;
                        if (user[0].brand_name == 'Tookan') {
                            msg += ' - Delivery Management Solution. ';
                        } else {
                            msg += '.';
                        }
                        msg += 'You have been added as a manager by <b>' + user[0].username + '</b>.<br><br>';
                        msg += '<b>DashboardLink:</b> ' + webAppLink + ' <br><br>';
                        msg += "Use the credentials below to sign-up: <br>";
                        msg += "<b>Username : </b>" + email + "<br>";
                        msg += "<b>Password : </b>" + user[0].brand_name.split(' ')[0] + user[0].randomNumber;

                        var sql = "INSERT INTO `tb_dispatcher_permissions` (`dispatcher_id`,`user_id`,`view_driver`,`view_task`,`view_team`,`view_notification`,`create_driver`,";
                        sql += "`create_task`,`create_team`,`update_notification`)";
                        sql += " VALUES (?,?,?,?,?,?,?,?,?,?)";
                        connection.query(sql, [user[0].dispatcher, user[0].user_id, 0, task_access, 0, 0, add_driver_access, 1, 0, 0], function (err, adding_dispatcher) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in inserting dispatcher permissions : ", err, adding_dispatcher);
                                cb(null, {
                                    err: 5
                                });
                            } else {
                                commonFunc.emailPlainFormatting(name, msg, '', '', '', function (returnMessage) {
                                    returnMessage = returnMessage.replace(/BRAND_NAME/g,user[0].brand_name);
                                    commonFunc.sendHtmlContent(email, returnMessage, "Welcome to " + user[0].brand_name + "! Added as Manager", user[0].email, function (sendHtmlContentResult) {
                                        cb(null, user);
                                    });
                                });
                            }
                        })
                    }
                },
                function (user, cb) {
                    if (user.err) {
                        cb(null, user);
                    } else {

                        if (dispatcher_teams == 0) {
                            cb(null, user);
                        } else {

                            var dispatcher_teams_array = dispatcher_teams.split(','), dispatcher_teams_array_len = dispatcher_teams_array.length;
                            var counter = 0;
                            for (var i = 0; i < dispatcher_teams_array_len; i++) {
                                commonFunc.authenticateUserIdAndTeamId(user[0].user_id, dispatcher_teams_array[i], function (auth) {
                                    if (auth != 0) {
                                        var sql = "INSERT INTO `tb_dispatcher_teams` (`team_id`,`user_id`,`dispatcher_id`) VALUES (?,?,?)";
                                        connection.query(sql, [auth[0].team_id, user[0].user_id, user[0].dispatcher], function (err, result_insert_dispatcher_teams) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in inserting dispatcher teams info : ", err, result_insert_dispatcher_teams);
                                            }
                                        });
                                    }
                                    counter++;
                                    sendResponse(counter, dispatcher_teams_array_len);
                                });
                            }
                            function sendResponse(counter, dispatcher_teams_array_len) {
                                if (counter == dispatcher_teams_array_len) {
                                    cb(null, user);
                                }
                            }
                        }
                    }
                }


            ],
            function (error, user) {
                if (error) {
                    responses.authenticationError(res);
                    return;
                } else {
                    if (user.err) {
                        if (user.err == 1) {
                            responses.authenticationErrorResponse(res);
                            return;
                        } else if (user.err == 2) {
                            responses.accountExpiryErrorResponse(res);
                            return;
                        } else if (user.err == 3) {
                            responses.invalidAccessError(res);
                            return;
                        } else if (user.err == 4) {
                            responses.authenticationAlreadyExists(res);
                            return;
                        } else if (user.err == 5) {
                            responses.sendError(res);
                            return;
                        } else {
                            responses.authenticationError(res);
                            return;
                        }
                    } else {
                        responses.actionCompleteResponse(res);
                        return;
                    }
                }
            }
        )
    }
};


/*
 * ----------------------------------
 * UPDATE DISPATCHER
 * ----------------------------------
 */

exports.update_dispatcher = function (req, res) {

    var access_token = req.body.access_token;
    var email = req.body.email;
    var name = req.body.name;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var phone = req.body.phone;
    var timezone = req.body.timezone;
    var task_access = req.body.task_access;
    var add_driver_access = req.body.add_driver_access;
    var dispatcher_id = req.body.dispatcher_id;
    var dispatcher_teams = req.body.dispatcher_teams;
    if (typeof(dispatcher_teams) === "undefined") {
        dispatcher_teams = 0
    }
    var manvalues = [first_name, name, access_token, timezone, task_access, add_driver_access, dispatcher_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var dispatcher_user_id = result[0].dispatcher_user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            responses.invalidAccessError(res);
                        } else {
                            commonFunc.authenticateDispatcherIdAndUserId(dispatcher_id, user_id, function (authenticateDispatcherIdAndUserIdResult) {
                                if (authenticateDispatcherIdAndUserIdResult != 0) {
                                    if (authenticateDispatcherIdAndUserIdResult[0].email == email) {
                                        step_in(email, user_id, name, first_name, last_name, phone, dispatcher_id);
                                    } else {
                                        commonFunc.authenticateUserEmail(email, function (authenticateUserEmailResult) {
                                            if (authenticateUserEmailResult != 0) {
                                                responses.authenticationAlreadyExists(res);
                                                return;
                                            } else {
                                                step_in(email, user_id, name, first_name, last_name, phone, dispatcher_id);
                                            }
                                        });
                                    }
                                } else {
                                    responses.authenticationError(res);
                                }
                            });
                        }

                        function step_in(email, user_id, name, first_name, last_name, phone, dispatcher_id) {
                            var sql = "UPDATE `tb_users` SET `first_name`=?,`last_name`=?,`username`=?,`email`=?,`phone`=? " +
                                "WHERE `dispatcher_user_id`=? and `user_id`=? LIMIT 1";
                            connection.query(sql, [first_name, last_name, name, email, phone, user_id, dispatcher_id], function (err, result_insert) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in updating dispatcher info in tb_users : ", err, result_insert);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var sql = "UPDATE `tb_dispatcher_permissions` SET `view_driver`=?,`view_task`=?,`view_team`=?,`view_notification`=?,`create_driver`=?,";
                                    sql += "`create_task`=?,`create_team`=?,`update_notification`=? ";
                                    sql += " WHERE `dispatcher_id` = ? LIMIT 1";
                                    connection.query(sql, [0, task_access, 0, 0, add_driver_access, 1, 0, 0, dispatcher_id], function (err, result_adding_dispatcher) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in updating dispatcher permissions : ", err, result_adding_dispatcher);
                                            responses.sendError(res);
                                            return;
                                        } else {
                                            if (dispatcher_teams == 0) {
                                                responses.actionCompleteResponse(res);
                                                return;
                                            } else {
                                                var sql = "DELETE FROM `tb_dispatcher_teams` WHERE `dispatcher_id`=?";
                                                connection.query(sql, [dispatcher_id], function (err, result_delete_dispatcher_teams) {
                                                    if (err) {
                                                        logging.logDatabaseQueryError("Error in deleting dispatcher teams info : ", err, result_delete_dispatcher_teams);
                                                        responses.sendError(res);
                                                        return;
                                                    } else {
                                                        var dispatcher_teams_array = dispatcher_teams.split(',');
                                                        var dispatcher_teams_array_len = dispatcher_teams_array.length;
                                                        var counter = 0;
                                                        for (var i = 0; i < dispatcher_teams_array_len; i++) {
                                                            commonFunc.authenticateUserIdAndTeamId(user_id, dispatcher_teams_array[i], function (authenticateUserIdAndTeamIdResult) {
                                                                if (authenticateUserIdAndTeamIdResult != 0) {
                                                                    var sql = "INSERT INTO `tb_dispatcher_teams` (`team_id`,`user_id`,`dispatcher_id`) VALUES (?,?,?)";
                                                                    connection.query(sql, [authenticateUserIdAndTeamIdResult[0].team_id, user_id, dispatcher_id], function (err, result_insert_dispatcher_teams) {
                                                                        if (err) {
                                                                            logging.logDatabaseQueryError("Error in inserting fleet teams info : ", err, result_insert_dispatcher_teams);
                                                                        }
                                                                    });
                                                                }
                                                                counter++;
                                                                sendResponse(counter, dispatcher_teams_array_len);
                                                            });
                                                        }
                                                    }
                                                });
                                                function sendResponse(counter, dispatcher_teams_array_len) {
                                                    if (counter == dispatcher_teams_array_len) {
                                                        responses.actionCompleteResponse(res);
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};


/*
 * ----------------------
 * VIEW ALL DISPATCHER
 * ----------------------
 */

exports.view_all_dispatcher = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var dispatcher_user_id = result[0].dispatcher_user_id;
                var dispatcher_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            dispatcher_id = user_id;
                            user_id = dispatcher_user_id;
                            responses.invalidAccessError(res);
                        } else {
                            step_in(user_id);
                        }

                        function step_in(user_id) {

                            var sql = "SELECT IF(teams.`team_id` IS NULL,0,teams.`team_id`) as `team_id`,teams.`team_name`,users.`username`,users.`user_id` as `dispatcher_id`,users.`email`," +
                                "users.`first_name`,users.`last_name`,users.`phone`,users.`username`, " +
                                "tb_dis_per.`view_driver`,tb_dis_per.`view_team`,tb_dis_per.`view_notification`,tb_dis_per.`view_task`, " +
                                "tb_dis_per.`create_driver`,tb_dis_per.`create_task`,tb_dis_per.`update_notification`,tb_dis_per.`create_team` " +
                                "FROM `tb_users` users " +
                                "INNER JOIN `tb_dispatcher_permissions` tb_dis_per ON users.`user_id` = tb_dis_per.`dispatcher_id` " +
                                "LEFT JOIN `tb_dispatcher_teams` tb_dis_teams ON users.`user_id` = tb_dis_teams.`dispatcher_id` " +
                                "LEFT JOIN `tb_teams` teams ON teams.`team_id` = tb_dis_teams.`team_id` " +
                                "WHERE `dispatcher_user_id`=?";
                            connection.query(sql, [user_id], function (err, result_dispatchers) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in viewing all dispatcher info : ", err, result_dispatchers);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    if (result_dispatchers.length > 0) {
                                        var dispatchers = {};
                                        var dispatcherArray = [];
                                        result_dispatchers.forEach(function (dispatcher) {
                                            if (dispatchers[dispatcher.dispatcher_id]) {
                                                var team = {};
                                                if (dispatcher.team_id != 0) {
                                                    team.team_id = dispatcher.team_id;
                                                    team.team_name = dispatcher.team_name;
                                                    dispatchers[dispatcher.dispatcher_id].teams.push(team);
                                                }
                                            } else {
                                                dispatchers[dispatcher.dispatcher_id] = {};
                                                dispatchers[dispatcher.dispatcher_id].dispatcher_id = dispatcher.dispatcher_id;
                                                dispatchers[dispatcher.dispatcher_id].username = dispatcher.username;
                                                dispatchers[dispatcher.dispatcher_id].first_name = dispatcher.first_name;
                                                dispatchers[dispatcher.dispatcher_id].last_name = dispatcher.last_name;
                                                dispatchers[dispatcher.dispatcher_id].email = dispatcher.email;
                                                dispatchers[dispatcher.dispatcher_id].phone = dispatcher.phone;
                                                dispatchers[dispatcher.dispatcher_id].view_driver = dispatcher.view_driver;
                                                dispatchers[dispatcher.dispatcher_id].view_team = dispatcher.view_team;
                                                dispatchers[dispatcher.dispatcher_id].view_notification = dispatcher.view_notification;
                                                dispatchers[dispatcher.dispatcher_id].view_task = dispatcher.view_task;
                                                dispatchers[dispatcher.dispatcher_id].create_driver = dispatcher.create_driver;
                                                dispatchers[dispatcher.dispatcher_id].create_task = dispatcher.create_task;
                                                dispatchers[dispatcher.dispatcher_id].update_notification = dispatcher.update_notification;
                                                dispatchers[dispatcher.dispatcher_id].create_team = dispatcher.create_team;
                                                dispatchers[dispatcher.dispatcher_id].teams = [];
                                                var team = {};
                                                if (dispatcher.team_id != 0) {
                                                    team.team_id = dispatcher.team_id;
                                                    team.team_name = dispatcher.team_name;
                                                    dispatchers[dispatcher.dispatcher_id].teams.push(team);
                                                }
                                            }
                                        });
                                        for (var key in dispatchers) {
                                            dispatcherArray.push(dispatchers[key]);
                                        }
                                        var response = {
                                            "message": constants.responseMessages.ACTION_COMPLETE,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": dispatcherArray
                                        };
                                        res.send(JSON.stringify(response));
                                        return;
                                    } else {
                                        var response = {
                                            "message": constants.responseMessages.NO_DISPATCHERS_AVAILABLE,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": {}
                                        };
                                        res.send(JSON.stringify(response));
                                        return;
                                    }
                                }
                            });
                        }
                    }
                });

            }
        });
    }
};


/*
 * --------------------------
 * DELETE DISPATCHER
 * --------------------------
 */

exports.delete_dispatcher = function (req, res) {

    var access_token = req.body.access_token;
    var dispatcher_id = req.body.dispatcher_id;
    var manvalues = [access_token, dispatcher_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var dispatcher_user_id = result[0].dispatcher_user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            responses.invalidAccessError(res);
                        } else {
                            commonFunc.authenticateDispatcherIdAndUserId(dispatcher_id, user_id, function (authenticateDispatcherIdAndUserIdResult) {
                                if (authenticateDispatcherIdAndUserIdResult != 0) {
                                    step_in(user_id, dispatcher_user_id);
                                } else {
                                    responses.authenticationError(res);
                                }
                            });
                        }

                        function step_in(user_id, dispatcher_user_id) {

                            var sql = "DELETE FROM `tb_users` WHERE `user_id`=? and `dispatcher_user_id`=? AND `is_dispatcher`=1 LIMIT 1";
                            connection.query(sql, [dispatcher_id, user_id], function (err, result_teams) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in deleting dispatcher info : ", err, result_teams);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    responses.actionCompleteResponse(res);
                                    return;
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};