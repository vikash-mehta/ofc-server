var commonFunc = require('./commonfunction');
var cron = require('./cron');
var md5 = require('MD5');
var Handlebars = require('handlebars');
var responses = require('./responses');
var logging = require('./logging');
var mongo = require('./mongo');
var moment = require('moment');
var parseFormat = require('moment-parseformat');
var schedule = require('node-schedule');
var async = require('async');
var autoAssign = require('./auto-assignment');
var socketResponse = require('./socketResponses');
var teams = require('./teams');
/*
 * -------------------------------
 * ETA BETWEEN PICKUP AND DELIVERY
 * -------------------------------
 */

exports.find_customer_with_phone = function (req, res) {

    var access_token = req.body.access_token;
    var customer_phone = req.body.customer_phone;
    var manvalues = [access_token, customer_phone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    user_id = result[0].dispatcher_user_id;
                }
                commonFunc.findCustomer(user_id, customer_phone, function (cust) {
                    cust.forEach(function (customer) {
                        if (customer.customer_username == "dummy") customer.customer_username = ''
                        if (customer.customer_email == "dummy") customer.customer_email = ''
                        if (customer.customer_phone == "+910000000000") customer.customer_phone = ''
                    })
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": cust
                    };
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
}


/*
 * -------------------------------
 * ETA BETWEEN PICKUP AND DELIVERY
 * -------------------------------
 */

exports.getETA = function (req, res) {

    var access_token = req.body.access_token;
    var pickup_address = req.body.pickup_address;
    var delivery_address = req.body.delivery_address;
    var manvalues = [access_token, pickup_address, delivery_address];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.getEstimateTimeOfArrival(pickup_address, delivery_address, constants.travellingMode.DRIVING, function (getEstimateTimeOfArrivalResult) {
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": {
                            "ETA": getEstimateTimeOfArrivalResult
                        }
                    };
                    res.send(JSON.stringify(response));
                    return;
                });
            }
        });
    }
}


/*
 * ------------------------------------
 * CHECK AVAILABILITY AND ETA OF FLEETS
 * ------------------------------------
 */

exports.check_fleet_availability = function (req, res) {

    var access_token = req.body.access_token;
    var pickup_datetime = req.body.pickup_datetime;
    var pickup_address = req.body.pickup_address;
    var delivery_datetime = req.body.delivery_datetime;
    var delivery_address = req.body.delivery_address;
    var manvalues = [access_token, pickup_datetime, pickup_address, delivery_datetime, delivery_address];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {

                        if (result[0].constraint_type == constants.constraintType.PICKUP_HARD) {

                            commonFunc.getAllFleets(user_id, function (getAllFleetsResult) {
                                var resultArray = [], count = 0, ETAofCompleteCurrentJobViaPickingNewJob = 0, ETAofCompleteCurrentJobViaPickingNewJobDistance = 0, delayTime = 0, delayDistance = 0;
                                var fleetsSize = getAllFleetsResult.length;
                                getAllFleetsResult.forEach(function (fleet) {
                                    checkFleetAvailability(fleet, pickup_datetime, delivery_datetime, function (checkFleetAvailabilityResult) {
                                        if (checkFleetAvailabilityResult.status == true) {
                                            //Calculate Distance between current location of fleet and running job delivery point
                                            commonFunc.getEstimateTimeOfArrival(checkFleetAvailabilityResult.job.job_latitude + "," + checkFleetAvailabilityResult.job.job_longitude, checkFleetAvailabilityResult.fleet.latitude + "," + checkFleetAvailabilityResult.fleet.longitude, constants.travellingMode.DRIVING, function (getETAofCurrentJobResult) {

                                                setTimeout(function () {
                                                    //Calculate Distance between current location of fleet and new job's pickup point
                                                    commonFunc.getEstimateTimeOfArrival(checkFleetAvailabilityResult.fleet.latitude + "," + checkFleetAvailabilityResult.fleet.longitude, pickup_address, constants.travellingMode.DRIVING, function (getETAofNewJobResult) {

                                                        setTimeout(function () {
                                                            //Calculate Distance between new job's pickup point and running job delivery point
                                                            commonFunc.getEstimateTimeOfArrival(pickup_address, checkFleetAvailabilityResult.job.job_latitude + "," + checkFleetAvailabilityResult.job.job_longitude, constants.travellingMode.DRIVING, function (getETAofNewPickupAndOldDeliveryResult) {

                                                                if (getETAofNewPickupAndOldDeliveryResult.duration) {
                                                                    ETAofCompleteCurrentJobViaPickingNewJob = getETAofNewPickupAndOldDeliveryResult.duration.seconds + getETAofNewJobResult.duration.seconds;
                                                                    delayTime = (ETAofCompleteCurrentJobViaPickingNewJob) - getETAofCurrentJobResult.duration.seconds;
                                                                }
                                                                if (getETAofNewPickupAndOldDeliveryResult.distance) {
                                                                    ETAofCompleteCurrentJobViaPickingNewJobDistance = getETAofNewPickupAndOldDeliveryResult.distance.metres + getETAofNewJobResult.distance.metres;
                                                                    delayDistance = (ETAofCompleteCurrentJobViaPickingNewJobDistance) - getETAofCurrentJobResult.distance.metres;
                                                                }
                                                                resultArray.push({
                                                                    "delayTime": commonFunc.toKM(delayTime),
                                                                    "ETAofCompleteCurrentJobViaPickingNewJob": commonFunc.toHHMMSS(ETAofCompleteCurrentJobViaPickingNewJob),
                                                                    "delayDistance": commonFunc.toKM(delayDistance),
                                                                    "ETAofCompleteCurrentJobViaPickingNewJobDistance": commonFunc.toKM(ETAofCompleteCurrentJobViaPickingNewJobDistance),
                                                                    "ETAofNewJobPickupToOldJobDelivery": getETAofNewPickupAndOldDeliveryResult,
                                                                    "ETAofNewJob": getETAofNewJobResult,
                                                                    "ETAofCurrentJob": getETAofCurrentJobResult,
                                                                    "busy_status": checkFleetAvailabilityResult.status,
                                                                    "fleet_info": checkFleetAvailabilityResult.fleet
                                                                });
                                                                if (count == fleetsSize - 1) {
                                                                    result();
                                                                } else {
                                                                    count++;
                                                                }
                                                            });
                                                        }, 3000);
                                                    });
                                                }, 3000);
                                            });
                                        }
                                        else if (checkFleetAvailabilityResult.status == false) {
                                            commonFunc.getEstimateTimeOfArrival(pickup_address, checkFleetAvailabilityResult.fleet.latitude + "," + checkFleetAvailabilityResult.fleet.longitude, constants.travellingMode.DRIVING, function (getETAResult) {
                                                resultArray.push({
                                                    "ETA": getETAResult,
                                                    "busy_status": checkFleetAvailabilityResult.status,
                                                    "fleet_info": checkFleetAvailabilityResult.fleet
                                                });
                                                if (count == fleetsSize - 1) {
                                                    result();
                                                } else {
                                                    count++;
                                                }
                                            })
                                        }

                                        function result() {
                                            var response = {
                                                "message": constants.responseMessages.ACTION_COMPLETE,
                                                "status": constants.responseFlags.ACTION_COMPLETE,
                                                "data": resultArray
                                            };
                                            res.send(JSON.stringify(response));
                                            return;
                                        }
                                    });
                                });
                            });
                        }
                        else if (result[0].constraint_type == constants.constraintType.DELIVERY_HARD || result[0].constraint_type == constants.constraintType.PICKUP_AND_DELIVERY_HARD) {

                            commonFunc.getAllFleets(user_id, function (getAllFleetsResult) {
                                var resultArray = [], count = 0, newPathTime = 0, newPathDistance = 0, DelayTimeOldviaNew = 0, DelayDistanceOldviaNew = 0;
                                var fleetsSize = getAllFleetsResult.length;
                                getAllFleetsResult.forEach(function (fleet) {
                                    checkFleetAvailability(fleet, pickup_datetime, delivery_datetime, function (checkFleetAvailabilityResult) {
                                        if (checkFleetAvailabilityResult.status == true) {
                                            //Calculate Distance between current location of fleet and running job delivery point
                                            commonFunc.getEstimateTimeOfArrival(checkFleetAvailabilityResult.job.job_latitude + "," + checkFleetAvailabilityResult.job.job_longitude, checkFleetAvailabilityResult.fleet.latitude + "," + checkFleetAvailabilityResult.fleet.longitude, constants.travellingMode.DRIVING, function (getETAofCurrentJobResult) {

                                                setTimeout(function () {
                                                    //Calculate Distance between current location of fleet and new job's pickup point
                                                    commonFunc.getEstimateTimeOfArrival(checkFleetAvailabilityResult.fleet.latitude + "," + checkFleetAvailabilityResult.fleet.longitude, pickup_address, constants.travellingMode.DRIVING, function (getETAofNewJobResult) {

                                                        setTimeout(function () {
                                                            //Calculate Distance between new job's pickup point and new job's delivery point
                                                            commonFunc.getEstimateTimeOfArrival(pickup_address, delivery_address, constants.travellingMode.DRIVING, function (getETAofNewPickupAndNewDeliveryResult) {

                                                                setTimeout(function () {
                                                                    //Calculate Distance between new job's delivery point and running job delivery point
                                                                    commonFunc.getEstimateTimeOfArrival(delivery_address, checkFleetAvailabilityResult.job.job_latitude + "," + checkFleetAvailabilityResult.job.job_longitude, constants.travellingMode.DRIVING, function (getETAofNewDeliveryAndOldDeliveryResult) {


                                                                        if (getETAofCurrentJobResult.duration) {
                                                                            newPathTime = getETAofNewJobResult.duration.seconds + getETAofNewPickupAndNewDeliveryResult.duration.seconds + getETAofNewDeliveryAndOldDeliveryResult.duration.seconds;
                                                                            DelayTimeOldviaNew = (newPathTime) - getETAofCurrentJobResult.duration.seconds;
                                                                        }
                                                                        if (getETAofCurrentJobResult.distance) {
                                                                            newPathDistance = getETAofNewJobResult.distance.metres + getETAofNewPickupAndNewDeliveryResult.distance.metres + getETAofNewDeliveryAndOldDeliveryResult.distance.metres;
                                                                            DelayDistanceOldviaNew = (newPathDistance) - getETAofCurrentJobResult.distance.metres;
                                                                        }
                                                                        resultArray.push({
                                                                            "DelayDistanceOldviaNew": commonFunc.toKM(DelayDistanceOldviaNew),
                                                                            "DelayTimeOldviaNew": commonFunc.toHHMMSS(DelayTimeOldviaNew),
                                                                            "ETAofNewJob": getETAofNewPickupAndNewDeliveryResult,
                                                                            "ETAofCurrentJob": getETAofCurrentJobResult,
                                                                            "busy_status": checkFleetAvailabilityResult.status,
                                                                            "fleet_info": checkFleetAvailabilityResult.fleet
                                                                        });
                                                                        if (count == fleetsSize - 1) {
                                                                            result();
                                                                        } else {
                                                                            count++;
                                                                        }

                                                                    });
                                                                }, 3000);
                                                            });
                                                        }, 3000);
                                                    });
                                                }, 3000);
                                            });
                                        }
                                        else if (checkFleetAvailabilityResult.status == false) {
                                            var totalTimeToComplete = 0, totalDistanceToComplete = 0;

                                            //Calculate Distance between new pickup and current location of fleet
                                            commonFunc.getEstimateTimeOfArrival(pickup_address, checkFleetAvailabilityResult.fleet.latitude + "," + checkFleetAvailabilityResult.fleet.longitude, constants.travellingMode.DRIVING, function (getETACurrentLocationToPickupResult) {

                                                setTimeout(function () {
                                                    //Calculate Distance between new pickup and delivery address
                                                    commonFunc.getEstimateTimeOfArrival(pickup_address, delivery_address, constants.travellingMode.DRIVING, function (getETAPickupToDeliveryResult) {

                                                        if (getETACurrentLocationToPickupResult.duration) {
                                                            totalTimeToComplete = getETACurrentLocationToPickupResult.duration.seconds + getETAPickupToDeliveryResult.duration.seconds;
                                                        }
                                                        if (getETAPickupToDeliveryResult.distance) {
                                                            totalDistanceToComplete = getETACurrentLocationToPickupResult.distance.metres + getETAPickupToDeliveryResult.distance.metres
                                                        }
                                                        resultArray.push({
                                                            "totalTimeToComplete": commonFunc.toHHMMSS(totalTimeToComplete),
                                                            "totalDistanceToComplete": commonFunc.toKM(totalDistanceToComplete),
                                                            "ETAcurrentLocationToPickup": getETACurrentLocationToPickupResult,
                                                            "ETApickupToDelivery": getETAPickupToDeliveryResult,
                                                            "busy_status": checkFleetAvailabilityResult.status,
                                                            "fleet_info": checkFleetAvailabilityResult.fleet
                                                        });
                                                        if (count == fleetsSize - 1) {
                                                            result();
                                                        } else {
                                                            count++;
                                                        }
                                                    })
                                                }, 3000);
                                            });
                                        }

                                        function result() {
                                            var response = {
                                                "message": constants.responseMessages.ACTION_COMPLETE,
                                                "status": constants.responseFlags.ACTION_COMPLETE,
                                                "data": resultArray
                                            };
                                            res.send(JSON.stringify(response));
                                            return;
                                        }
                                    });
                                });
                            });

                        }
                    }
                });
            }
        });
    }
}

/*
 * ----------------------------------
 * CREATION OF NEW JOB (LAYOUT WISE)
 * ---------------------------------
 */

exports.create_task = function (req, res) {
    if (req.query && req.query.access_token) {
        req.body.access_token = req.query.access_token;
    }
    var access_token = req.body.access_token,
        customer_email = req.body.customer_email,
        customer_phone = req.body.customer_phone,
        customer_username = req.body.customer_username,
        customer_address = req.body.customer_address,
        job_description = req.body.job_description,
        job_delivery_datetime = req.body.job_delivery_datetime,
        latitude = req.body.latitude,
        longitude = req.body.longitude,
        job_pickup_name = req.body.job_pickup_name,
        job_pickup_phone = req.body.job_pickup_phone,
        job_pickup_datetime = req.body.job_pickup_datetime,
        job_pickup_latitude = req.body.job_pickup_latitude,
        job_pickup_longitude = req.body.job_pickup_longitude,
        job_pickup_address = req.body.job_pickup_address,
        job_pickup_email = req.body.job_pickup_email,
        fleet_id = req.body.fleet_id,
        timezone = req.body.timezone,
        job_id = req.body.job_id,
        has_pickup = req.body.has_pickup,
        has_delivery = req.body.has_delivery,
        layout_type = req.body.layout_type,
        tracking_link = req.body.tracking_link,
        order_id = req.body.order_id,
        fields = req.body.meta_data,
        pickup_fields = req.body.pickup_meta_data,
        team_id = req.body.team_id,
        auto_assignment = req.body.auto_assignment,
        ref_images = req.body.ref_images,
        p_ref_images = req.body.p_ref_images;
    if ((typeof req.body.sock === "undefined") || req.body.sock === '') {
        req.body.sock = 1;
    }
    if ((typeof auto_assignment === "undefined") || auto_assignment === '') {
        auto_assignment = 1;
    }
    console.log("create task body === ", req.body, fields);

    if (typeof fields === "undefined" || fields === '') {
        fields = [];
    } else if (typeof fields === "string") {
        fields = fields.replace(/'/g, '"');
        fields = JSON.parse(fields);
    }
    if (typeof pickup_fields === "undefined" || pickup_fields === '') {
        pickup_fields = [];
    } else if (typeof pickup_fields === "string") {
        pickup_fields = pickup_fields.replace(/'/g, '"');
        pickup_fields = JSON.parse(pickup_fields);
    }
    if (typeof ref_images === "undefined" || ref_images === '' || typeof ref_images != "object") {
        ref_images = [];
    }
    if (typeof p_ref_images === "undefined" || p_ref_images === '' || typeof  p_ref_images != "object") {
        p_ref_images = [];
    }
    if (typeof(fleet_id) === "undefined" || fleet_id === "") {
        fleet_id = null
    }
    if (typeof(job_pickup_name) === "undefined" || job_pickup_name === "") {
        job_pickup_name = "";
    }
    if (typeof(tracking_link) === "undefined" || tracking_link === "") {
        tracking_link = '';
    }
    if (typeof(job_pickup_datetime) === "undefined" || job_pickup_datetime === "") {
        job_pickup_datetime = '0000-00-00 00:00:00';
    }
    if (typeof(job_delivery_datetime) === "undefined" || job_delivery_datetime === "") {
        job_delivery_datetime = '0000-00-00 00:00:00';
    }
    if (typeof(job_pickup_latitude) === "undefined" || job_pickup_latitude === "") {
        job_pickup_latitude = 0;
    }
    if (typeof(job_pickup_longitude) === "undefined" || job_pickup_longitude === "") {
        job_pickup_longitude = 0;
    }
    if (typeof(latitude) === "undefined" || latitude === "") {
        latitude = 0;
    }
    if (typeof(longitude) === "undefined" || longitude === "") {
        longitude = 0;
    }
    if (typeof(job_pickup_address) === "undefined" || job_pickup_address === "") {
        job_pickup_address = "";
    }
    var manvalues = [access_token, timezone, layout_type, has_pickup, has_delivery];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var dispatcher_id = null, accepted_fleet_id = [], accepted_fleet_name = [];
        async.waterfall([
            function (cb) {
                commonFunc.authenticateUserAccessToken(access_token, function (user) {
                    if (user == 0) {
                        console.log("create task response === ", req.body, user);
                        cb(null, {
                            err: 1
                        });
                    } else {
                        cb(null, user);
                    }
                });
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    user[0].user = user[0].user_id;
                    if (user[0].is_dispatcher == constants.isDispatcherStatus.YES)  user[0].user = user[0].dispatcher_user_id;
                    if (typeof(job_pickup_phone) === "undefined" || job_pickup_phone === "") {
                        job_pickup_phone = 0;
                    } else {
                        job_pickup_phone = commonFunc.formatPhoneNumber(job_pickup_phone, user[0].country_phone_code.toUpperCase());
                    }
                    commonFunc.checkAccountExpiry(user[0].user, function (checkExp) {
                        if (checkExp == 1) {
                            cb(null, {
                                err: 2
                            });
                        } else {
                            cb(null, user);
                        }
                    });
                }
            },
            //function (user, cb) {
            //    if (user.err) {
            //        cb(null, user);
            //    } else {
            //        if (parseInt(layout_type) == parseInt(user[0].layout_type)) {
            //            cb(null, user);
            //        } else {
            //            cb(null, {
            //                err: 6
            //            });
            //        }
            //    }
            //},
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (typeof team_id === "undefined" || team_id === '') {
                        teams.getTeams(user[0].user_id, user[0].is_dispatcher, user[0].dispatcher_user_id, function (allteams) {
                            if (allteams.message == constants.responseMessages.ACTION_COMPLETE && allteams.data.length == 1) {
                                team_id = allteams.data[0].team_id;
                            } else {
                                team_id = 0;
                            }
                            cb(null, user);
                        })
                    } else {
                        cb(null, user);
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    var pickup_custom_field_template = req.body.pickup_custom_field_template;
                    if (typeof pickup_custom_field_template === "undefined" || pickup_custom_field_template === '') {
                        cb(null, user)
                    } else {
                        mongo.getTemplateItems(user[0].user, layout_type, pickup_custom_field_template, function (template) {
                            if (pickup_fields && typeof pickup_fields === "object" && pickup_fields.length) {
                                var labels = [];
                                template.items.forEach(function (da) {
                                    if (da.label) labels.push(da.label.toLowerCase());
                                })
                                pickup_fields.forEach(function (metaData) {
                                    if (metaData.label) {
                                        var ind = labels.indexOf(metaData.label.toLowerCase());
                                        if (ind >= 0) {
                                            template.items[ind].data = metaData.data
                                        }
                                    }
                                })
                            }
                            pickup_fields = template;
                            cb(null, user)
                        })
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    var custom_field_template = req.body.custom_field_template;
                    if (typeof custom_field_template === "undefined" || custom_field_template === '') {
                        cb(null, user)
                    } else {
                        mongo.getTemplateItems(user[0].user, layout_type, custom_field_template, function (template) {
                            if (fields && typeof fields === "object" && fields.length) {
                                var labels = [];
                                template.items.forEach(function (da) {
                                    if (da.label) labels.push(da.label.toLowerCase());
                                })
                                fields.forEach(function (metaData) {
                                    if (metaData.label) {
                                        var ind = labels.indexOf(metaData.label.toLowerCase());
                                        if (ind >= 0) {
                                            template.items[ind].data = metaData.data
                                        }
                                    }
                                })
                            }
                            fields = template;
                            cb(null, user)
                        })
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (user[0].billing_plan == constants.billingPlan.FREE_LIMITED) {

                        commonFunc.numberOfTasksForCurrentMonth(user[0].user, function (check) {
                            if (check && check.length > 0) {
                                if (check[0].tasks_count > check[0].num_tasks) {
                                    cb(null, {
                                        err: 3
                                    });
                                } else {
                                    cb(null, user)
                                }
                            } else {
                                cb(null, user)
                            }
                        })
                    } else {
                        cb(null, user)
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if ((has_delivery == constants.hasDelivery.YES) || (layout_type == constants.layoutType.FOS) || (layout_type == constants.layoutType.APPOINTMENT)) {
                        if (typeof job_delivery_datetime === "undefined" || job_delivery_datetime === "") {
                            job_delivery_datetime = '0000-00-00 00:00:00';
                        }
                        job_delivery_datetime = moment(job_delivery_datetime, parseFormat(job_delivery_datetime)).format('YYYY-MM-DD HH:mm');
                        if (job_delivery_datetime == "Invalid date" || job_delivery_datetime == '0000-00-00 00:00:00') {
                            cb(null, {
                                err: 4
                            });
                        } else {
                            if ((typeof customer_address === undefined) || (typeof customer_address === "undefined") || customer_address === "") {
                                has_delivery = '0';
                                cb(null, user)
                            } else {
                                if (typeof(latitude) === "undefined" || latitude == "" || typeof(longitude) === "undefined" || longitude == "" || latitude == 0 || longitude == 0) {
                                    commonFunc.getLatLngFromAddress(customer_address, function (getLatLngFromAddressResult) {
                                        if (getLatLngFromAddressResult.length == 0) {
                                            latitude = user[0].company_latitude;
                                            longitude = user[0].company_longitude;
                                            cb(null, user)
                                        } else {
                                            latitude = getLatLngFromAddressResult[0].latitude;
                                            longitude = getLatLngFromAddressResult[0].longitude;
                                            cb(null, user)
                                        }
                                    })
                                } else {
                                    if (commonFunc.isValidLat(latitude) && commonFunc.isValidLng(latitude)) {
                                        cb(null, user)
                                    } else {
                                        cb(null, {
                                            err: 80
                                        });
                                    }
                                }
                            }
                        }
                    } else {
                        cb(null, user)
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (has_pickup == constants.hasPickup.YES) {
                        if (typeof job_pickup_datetime === "undefined" || job_pickup_datetime === "")
                            job_pickup_datetime = '0000-00-00 00:00:00';
                        job_pickup_datetime = moment(job_pickup_datetime, parseFormat(job_pickup_datetime)).format('YYYY-MM-DD HH:mm');
                        if (job_pickup_datetime == "Invalid date" || job_pickup_datetime == '0000-00-00 00:00:00') {
                            cb(null, {
                                err: 5
                            });
                        } else {
                            if ((typeof job_pickup_address === undefined) || (typeof job_pickup_address === "undefined") || job_pickup_address === "") {
                                has_pickup = '0';
                                cb(null, user)
                            } else {
                                if (typeof(job_pickup_latitude) === "undefined" || job_pickup_latitude == "" || typeof(job_pickup_longitude) === "undefined" || job_pickup_longitude == "" || job_pickup_latitude == 0 || job_pickup_longitude == 0) {
                                    commonFunc.getLatLngFromAddress(job_pickup_address, function (getLatLngFromAddressResult) {
                                        if (getLatLngFromAddressResult.length == 0) {
                                            job_pickup_latitude = user[0].company_latitude;
                                            job_pickup_longitude = user[0].company_longitude;
                                            cb(null, user)

                                        } else {
                                            job_pickup_latitude = getLatLngFromAddressResult[0].latitude;
                                            job_pickup_longitude = getLatLngFromAddressResult[0].longitude;
                                            cb(null, user)
                                        }
                                    })
                                } else {
                                    if (commonFunc.isValidLat(job_pickup_latitude) && commonFunc.isValidLng(job_pickup_longitude)) {
                                        cb(null, user)
                                    } else {
                                        cb(null, {
                                            err: 80
                                        });
                                    }
                                }
                            }
                        }
                    } else {
                        cb(null, user)
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (user[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        commonFunc.checkDispatcherPermissions(user[0].user, function (chkPerm) {
                            if (chkPerm && chkPerm.length && chkPerm[0].create_task == constants.hasPermissionStatus.NO) {
                                cb(null, {
                                    err: 7
                                });
                            }
                            else {
                                commonFunc.getRegisteredFleet(user[0].user, fleet_id, function (getRegisteredFleetResult) {
                                    if (getRegisteredFleetResult != 0) {
                                        var getRegisteredFleetResultLength = getRegisteredFleetResult.length;
                                        for (var i = 0; i < getRegisteredFleetResultLength; i++) {
                                            if (getRegisteredFleetResult[i].is_active) {
                                                accepted_fleet_id.push(getRegisteredFleetResult[i].fleet_id);
                                            }
                                        }
                                    }
                                    cb(null, user);
                                })
                            }
                        });
                    }
                    else {
                        commonFunc.getRegisteredFleet(user[0].user, fleet_id, function (getRegisteredFleetResult) {
                            if (getRegisteredFleetResult != 0) {
                                var getRegisteredFleetResultLength = getRegisteredFleetResult.length;
                                for (var i = 0; i < getRegisteredFleetResultLength; i++) {
                                    if (getRegisteredFleetResult[i].is_active) {
                                        accepted_fleet_id.push(getRegisteredFleetResult[i].fleet_id);
                                        accepted_fleet_name.push(getRegisteredFleetResult[i].username);
                                    }
                                }
                            }
                            cb(null, user);
                        })
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (team_id) {
                        commonFunc.authenticateFleetIDAndTeamID(user[0].user, team_id, accepted_fleet_id, function (auth) {
                            accepted_fleet_id = [], accepted_fleet_name = [];
                            if (auth != 0) {
                                var authlen = auth.length;
                                for (var i = 0; i < authlen; i++) {
                                    accepted_fleet_id.push(auth[i].fleet_id);
                                    accepted_fleet_name.push(auth[i].username);
                                }
                            }
                            cb(null, user);
                        })
                    } else {
                        cb(null, user);
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    commonFunc.checkPickupAndDeliveryTime(job_pickup_datetime, job_delivery_datetime, timezone, function (checkTime) {
                        if (checkTime.pickup == false) {
                            cb(null, {
                                err: 8
                            });
                        } else if (checkTime.delivery == false) {
                            cb(null, {
                                err: 9
                            });
                        } else {
                            async.parallel({
                                deliveryCustomer: function (callback) {
                                    if ((has_delivery == constants.hasDelivery.YES) || (layout_type == constants.layoutType.APPOINTMENT) || (layout_type == constants.layoutType.FOS)) {
                                        module.exports.addUpdateCustomer(user[0].user, customer_phone, customer_username, customer_email, customer_address, latitude, longitude, function (customerAddition) {
                                            callback(null, customerAddition);
                                        })
                                    } else {
                                        callback(null, 0);
                                    }
                                },
                                pickupCustomer: function (callback) {
                                    if (has_pickup == constants.hasPickup.YES) {
                                        module.exports.addUpdateCustomer(user[0].user, job_pickup_phone, job_pickup_name, job_pickup_email, job_pickup_address, job_pickup_latitude, job_pickup_longitude, function (customerAddition) {
                                            callback(null, customerAddition);
                                        })
                                    } else {
                                        callback(null, 0);
                                    }
                                }
                            }, function (customer_err, results) {
                                user[0].deliveryCustomer = results.deliveryCustomer;
                                user[0].pickupCustomer = results.pickupCustomer;
                                cb(null, user);
                            });

                        }
                    });
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    var job_hash = md5(new Date() + commonFunc.generateRandomString()), pickup_delivery_relationship = commonFunc.generateIncreasingIntegerRandomNumber(job_pickup_datetime, job_delivery_datetime);
                    has_pickup = parseInt(has_pickup), has_delivery = parseInt(has_delivery), layout_type = parseInt(layout_type);
                    if ((layout_type == constants.layoutType.PICKUP_AND_DELIVERY) && (has_pickup == constants.hasPickup.NO) && (has_delivery == constants.hasDelivery.NO)) {
                        cb(null, {
                            err: 10
                        });
                    } else if ((layout_type == constants.layoutType.FOS) && (has_pickup == constants.hasPickup.NO) && (has_delivery == constants.hasDelivery.NO)) {
                        if (customer_address) {
                            var jobT = constants.jobType.FOS, jobtime = job_pickup_datetime
                            job_hash = md5(new Date() + commonFunc.generateRandomString());
                            insertJobDetails(auto_assignment, team_id, {
                                    fields: fields,
                                    ref_images: ref_images
                                }, user[0].layout_type, order_id, access_token, job_pickup_email, has_delivery, {
                                    send_push: req.body.notify || 1,
                                    start_address: job_pickup_address,
                                    end_address: customer_address,
                                    sock: req.body.sock
                                },
                                customer_email, customer_username, customer_phone, customer_address, job_id, accepted_fleet_id, dispatcher_id, pickup_delivery_relationship, job_pickup_name,
                                has_pickup, job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude, job_pickup_address, job_hash, latitude, longitude, job_description,
                                job_pickup_datetime, job_delivery_datetime, user[0].user, user[0].deliveryCustomer, timezone, jobtime, accepted_fleet_name, customer_username, function (insertJobDetailsResult) {
                                    if (insertJobDetailsResult.status) {

                                        commonFunc.insertORupdateTaskForCurrentMonth(user[0].user, 1);
                                        commonFunc.sendTaskInfoToDispatcher(insertJobDetailsResult.job_id);

                                        user[0].result_job_id = insertJobDetailsResult.job_id;
                                        user[0].job_hash = job_hash;
                                        user[0].result_customer_name = customer_username;
                                        user[0].result_customer_address = customer_address;
                                        user[0].result_job_pickup_name = job_pickup_name;
                                        user[0].result_job_pickup_address = job_pickup_address;
                                        user[0].result_job_token = pickup_delivery_relationship;

                                        if (tracking_link) {
                                            tracking_link = config.get('trackLink') + job_hash;
                                            if (user[0].domain) tracking_link = user[0].domain + config.get('trackingPage') + job_hash;

                                            commonFunc.getGoogleShortenUrl(tracking_link, function (shortenUrl) {
                                                if (shortenUrl) tracking_link = shortenUrl;
                                                user[0].result_tracking_link = tracking_link;
                                                cb(null, user);
                                            });
                                        } else {
                                            cb(null, user);
                                        }
                                    } else {
                                        cb(null, {
                                            err: 12
                                        });
                                    }
                                });
                        } else {
                            cb(null, {
                                err: 11
                            });
                        }
                    } else if ((layout_type == constants.layoutType.APPOINTMENT) && (has_pickup == constants.hasPickup.NO) && (has_delivery == constants.hasDelivery.NO)) {
                        if (customer_address) {
                            var jobT = constants.jobType.APPOINTMENT, jobtime = job_pickup_datetime;
                            job_hash = md5(new Date() + commonFunc.generateRandomString());
                            insertJobDetails(auto_assignment, team_id, {
                                    fields: fields,
                                    ref_images: ref_images
                                }, user[0].layout_type, order_id, access_token, job_pickup_email, has_delivery, {
                                    send_push: req.body.notify || 1,
                                    start_address: job_pickup_address,
                                    end_address: customer_address,
                                    sock: req.body.sock
                                }, customer_email, customer_username, customer_phone, customer_address, job_id,
                                accepted_fleet_id, dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup, job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude,
                                job_pickup_address, job_hash, latitude, longitude, job_description, job_pickup_datetime, job_delivery_datetime, user[0].user, user[0].deliveryCustomer,
                                timezone, jobtime, accepted_fleet_name, customer_username, function (insertJobDetailsResult) {
                                    if (insertJobDetailsResult.status) {

                                        commonFunc.insertORupdateTaskForCurrentMonth(user[0].user, 1);
                                        commonFunc.sendTaskInfoToDispatcher(insertJobDetailsResult.job_id);

                                        user[0].result_job_id = insertJobDetailsResult.job_id;
                                        user[0].job_hash = job_hash;
                                        user[0].result_customer_name = customer_username;
                                        user[0].result_customer_address = customer_address;
                                        user[0].result_job_pickup_name = job_pickup_name;
                                        user[0].result_job_pickup_address = job_pickup_address;
                                        user[0].result_job_token = pickup_delivery_relationship;

                                        if (tracking_link) {
                                            tracking_link = config.get('trackLink') + job_hash;
                                            if (user[0].domain) tracking_link = user[0].domain + config.get('trackingPage') + job_hash;

                                            commonFunc.getGoogleShortenUrl(tracking_link, function (shortenUrl) {
                                                if (shortenUrl) tracking_link = shortenUrl;
                                                user[0].result_tracking_link = tracking_link;
                                                cb(null, user);
                                            });
                                        } else {
                                            cb(null, user);
                                        }
                                    } else {
                                        cb(null, {
                                            err: 12
                                        });
                                    }
                                });
                        } else {
                            cb(null, {
                                err: 11
                            });
                        }
                    } else if ((layout_type == constants.layoutType.PICKUP_AND_DELIVERY) && (has_pickup == constants.hasPickup.YES) && (has_delivery == constants.hasDelivery.NO)) {

                        var jobT = constants.jobType.PICKUP, jobtime = job_pickup_datetime;
                        job_hash = md5(new Date() + commonFunc.generateRandomString());
                        insertJobDetails(auto_assignment, team_id, {
                                fields: pickup_fields,
                                ref_images: p_ref_images
                            }, user[0].layout_type, order_id, access_token, job_pickup_email, has_delivery, {
                                send_push: req.body.notify || 1,
                                start_address: job_pickup_address,
                                end_address: customer_address,
                                sock: req.body.sock
                            }, job_pickup_email, job_pickup_name, job_pickup_phone, customer_address, job_id, accepted_fleet_id,
                            dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup, job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude,
                            job_pickup_address, job_hash, latitude, longitude, job_description, job_pickup_datetime, job_delivery_datetime, user[0].user, user[0].pickupCustomer,
                            timezone, jobtime, accepted_fleet_name, customer_username, function (insertJobDetailsResult) {
                                if (insertJobDetailsResult.status) {

                                    commonFunc.insertORupdateTaskForCurrentMonth(user[0].user, 1);
                                    commonFunc.sendTaskInfoToDispatcher(insertJobDetailsResult.job_id);

                                    user[0].result_job_id = insertJobDetailsResult.job_id;
                                    user[0].job_hash = job_hash;
                                    user[0].result_customer_name = customer_username;
                                    user[0].result_customer_address = customer_address;
                                    user[0].result_job_pickup_name = job_pickup_name;
                                    user[0].result_job_pickup_address = job_pickup_address;
                                    user[0].result_job_token = pickup_delivery_relationship;

                                    if (tracking_link) {
                                        tracking_link = config.get('trackLink') + job_hash;

                                        if (user[0].domain) tracking_link = user[0].domain + config.get('trackingPage') + job_hash;

                                        commonFunc.getGoogleShortenUrl(tracking_link, function (shortenUrl) {
                                            if (shortenUrl) tracking_link = shortenUrl;

                                            user[0].result_tracking_link = tracking_link;
                                            cb(null, user);
                                        });
                                    } else {
                                        cb(null, user);
                                    }
                                } else {
                                    cb(null, {
                                        err: 12
                                    });
                                }
                            });
                    } else if ((layout_type == constants.layoutType.PICKUP_AND_DELIVERY) && (has_pickup == constants.hasPickup.NO) && (has_delivery == constants.hasDelivery.YES)) {

                        var jobT = constants.jobType.DELIVERY, jobtime = job_delivery_datetime
                        job_hash = md5(new Date() + commonFunc.generateRandomString());
                        insertJobDetails(auto_assignment, team_id, {
                                fields: fields,
                                ref_images: ref_images
                            }, user[0].layout_type, order_id, access_token, job_pickup_email, has_delivery, {
                                send_push: req.body.notify || 1,
                                start_address: job_pickup_address,
                                end_address: customer_address,
                                sock: req.body.sock
                            }, customer_email, customer_username, customer_phone, customer_address, job_id, accepted_fleet_id,
                            dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup, job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude,
                            job_pickup_address, job_hash, latitude, longitude, job_description, job_pickup_datetime, job_delivery_datetime, user[0].user, user[0].deliveryCustomer,
                            timezone, jobtime, accepted_fleet_name, customer_username, function (insertJobDetailsResult) {
                                if (insertJobDetailsResult.status) {

                                    commonFunc.insertORupdateTaskForCurrentMonth(user[0].user, 1);
                                    commonFunc.sendTaskInfoToDispatcher(insertJobDetailsResult.job_id);

                                    user[0].result_job_id = insertJobDetailsResult.job_id;
                                    user[0].job_hash = job_hash;
                                    user[0].result_customer_name = customer_username;
                                    user[0].result_customer_address = customer_address;
                                    user[0].result_job_pickup_name = job_pickup_name;
                                    user[0].result_job_pickup_address = job_pickup_address;
                                    user[0].result_job_token = pickup_delivery_relationship;

                                    if (tracking_link) {
                                        tracking_link = config.get('trackLink') + job_hash;

                                        if (user[0].domain) tracking_link = user[0].domain + config.get('trackingPage') + job_hash;

                                        commonFunc.getGoogleShortenUrl(tracking_link, function (shortenUrl) {
                                            if (shortenUrl) tracking_link = shortenUrl;
                                            user[0].result_tracking_link = tracking_link;
                                            cb(null, user);
                                        });
                                    }
                                    else {
                                        cb(null, user);
                                    }
                                } else {
                                    cb(null, {
                                        err: 12
                                    });
                                }
                            });
                    } else if ((layout_type == constants.layoutType.PICKUP_AND_DELIVERY) && (has_pickup == constants.hasPickup.YES) && (has_delivery == constants.hasDelivery.YES)) {
                        /*
                         * Insert Pickup Task
                         */
                        var jobT = constants.jobType.PICKUP, jobtime = job_pickup_datetime;
                        insertJobDetails(auto_assignment, team_id, {
                                fields: pickup_fields,
                                ref_images: p_ref_images
                            }, user[0].layout_type, order_id, access_token, job_pickup_email, has_delivery, {
                                send_push: req.body.notify || 1,
                                start_address: job_pickup_address,
                                end_address: customer_address,
                                sock: req.body.sock
                            }, job_pickup_email, job_pickup_name, job_pickup_phone, job_pickup_address, job_id, accepted_fleet_id,
                            dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup, job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude,
                            job_pickup_address, job_hash, latitude, longitude, job_description, job_pickup_datetime, job_delivery_datetime, user[0].user, user[0].pickupCustomer,
                            timezone, jobtime, accepted_fleet_name, customer_username, function (insertPickupJobDetailsResult) {
                                if (insertPickupJobDetailsResult.status) {
                                    /*
                                     * Insert Delivery Task
                                     */
                                    jobT = constants.jobType.DELIVERY;
                                    jobtime = job_delivery_datetime;
                                    var delivery_job_hash = md5(new Date() + commonFunc.generateRandomString());
                                    insertJobDetails(auto_assignment, team_id, {
                                            fields: fields,
                                            ref_images: ref_images
                                        }, user[0].layout_type, order_id, access_token, job_pickup_email, has_delivery, {
                                            send_push: req.body.notify || 0,
                                            start_address: job_pickup_address,
                                            end_address: customer_address,
                                            sock: req.body.sock
                                        }, customer_email, customer_username, customer_phone, customer_address, job_id, accepted_fleet_id,
                                        dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup, job_pickup_phone, jobT, job_pickup_latitude,
                                        job_pickup_longitude, job_pickup_address, delivery_job_hash, latitude, longitude, job_description, job_pickup_datetime,
                                        job_delivery_datetime, user[0].user, user[0].deliveryCustomer, timezone, jobtime, accepted_fleet_name, customer_username, function (insertJobDetailsResult) {
                                            if (insertJobDetailsResult.status) {

                                                commonFunc.insertORupdateTaskForCurrentMonth(user[0].user, 1);
                                                commonFunc.sendTaskInfoToDispatcher(insertJobDetailsResult.job_id);

                                                user[0].result_job_id = insertPickupJobDetailsResult.job_id;
                                                user[0].result_pickup_job_id = insertPickupJobDetailsResult.job_id;
                                                user[0].result_delivery_job_id = insertJobDetailsResult.job_id;
                                                user[0].pickup_hash = job_hash;
                                                user[0].delivery_hash = delivery_job_hash;
                                                user[0].result_customer_name = customer_username;
                                                user[0].result_customer_address = customer_address;
                                                user[0].result_job_pickup_name = job_pickup_name;
                                                user[0].result_job_pickup_address = job_pickup_address;
                                                user[0].result_job_token = pickup_delivery_relationship;

                                                var deliverytracinglink = config.get('trackLink') + delivery_job_hash;
                                                if (tracking_link) {
                                                    tracking_link = config.get('trackLink') + job_hash;

                                                    if (user[0].domain) tracking_link = user[0].domain + config.get('trackingPage') + job_hash;
                                                    if (user[0].domain) deliverytracinglink = user[0].domain + config.get('trackingPage') + delivery_job_hash;

                                                    commonFunc.getGoogleShortenUrl(tracking_link, function (shortenUrl) {
                                                        if (shortenUrl) tracking_link = shortenUrl;

                                                        commonFunc.getGoogleShortenUrl(deliverytracinglink, function (drshortenUrl) {
                                                            if (drshortenUrl)  deliverytracinglink = drshortenUrl;

                                                            user[0].pickup_tracking_link = tracking_link;
                                                            user[0].delivery_tracing_link = deliverytracinglink;
                                                            cb(null, user);
                                                        });
                                                    });
                                                } else {
                                                    cb(null, user);
                                                }
                                            }
                                        });
                                } else {
                                    cb(null, {
                                        err: 12
                                    });
                                }
                            });
                    }

                }
            }
        ], function (error, user) {
            if (error) {
                responses.authenticationError(res);
                return;
            } else {
                if (user.err) {
                    if (user.err == 1) {
                        responses.authenticationErrorResponse(res);
                        return;
                    } else if (user.err == 2) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else if (user.err == 3) {
                        var response = {
                            "message": constants.responseMessages.TASKS_COUNT_ERROR,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 4) {
                        var response = {
                            "message": constants.responseMessages.INVALID_DATE_FORMAT,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 5) {
                        var response = {
                            "message": constants.responseMessages.INVALID_DATE_FORMAT,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 6) {
                        var response = {
                            "message": constants.responseMessages.WORKFLOW_NOT_MATCHED,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 7) {
                        var response = {
                            "message": constants.responseMessages.INVALID_ACCESS,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 8) {
                        var response = {
                            "message": constants.responseMessages.INVALID_PICKUP,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "job_pickup_name": job_pickup_name,
                                "job_pickup_address": job_pickup_address
                            }
                        }
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 9) {
                        var response = {
                            "message": constants.responseMessages.INVALID_DELIVERY,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 10) {
                        var response = {
                            "message": constants.responseMessages.NO_PICKUP_OR_DELIVERY_ERROR,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 11) {
                        responses.parameterMissingResponse(res);
                        return;
                    } else if (user.err == 13) {
                        var response = {
                            "message": constants.responseMessages.TOTAL_TASK_COUNT_REACHED,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 80) {
                        var response = {
                            "message": constants.responseMessages.INVALID_POINT,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                "customer_name": customer_username,
                                "customer_address": customer_address
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                    else { // user.err = 12
                        responses.authenticationError(res);
                        return;
                    }
                } else {
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": {}
                    };
                    if (user[0].result_job_id) response.data.job_id = user[0].result_job_id;
                    if (user[0].result_pickup_job_id) response.data.pickup_job_id = user[0].result_pickup_job_id;
                    if (user[0].result_delivery_job_id) response.data.delivery_job_id = user[0].result_delivery_job_id;
                    if (user[0].pickup_hash) response.data.job_hash = user[0].pickup_hash;
                    if (user[0].delivery_hash) response.data.job_hash = user[0].delivery_hash;
                    if (user[0].result_customer_name) response.data.customer_name = user[0].result_customer_name;
                    if (user[0].result_customer_address) response.data.customer_address = user[0].result_customer_address;
                    if (user[0].result_job_pickup_name) response.data.job_pickup_name = user[0].result_job_pickup_name;
                    if (user[0].result_job_pickup_address) response.data.job_pickup_address = user[0].result_job_pickup_address;
                    if (user[0].result_job_token) response.data.job_token = user[0].result_job_token;
                    if (user[0].result_tracking_link) response.data.tracking_link = user[0].result_tracking_link;
                    if (user[0].pickup_tracking_link) response.data.pickup_tracking_link = user[0].pickup_tracking_link;
                    if (user[0].delivery_tracing_link) response.data.delivery_tracing_link = user[0].delivery_tracing_link;
                    res.send(JSON.stringify(response));
                    return;
                }
            }
        })
    }
}


exports.addUpdateCustomer = function (user_id, phone, username, email, address, latitude, longitude, callback) {
    if (phone) {
        phone = phone.toString().split(' ').join('');
        phone = phone.toString().split('-').join('')
    }
    commonFunc.authenticateCustomerWithUser(user_id, phone, function (authCustomer) {
        var access_token = md5(phone + (new Date()).getMilliseconds);
        if (authCustomer && authCustomer.length) {
            var sql = "UPDATE `tb_customers` SET `customer_latitude`=?,`customer_longitude`=?,`access_token`=?,`customer_username`=?,`customer_email`=?,`customer_address`=? ";
            sql += "WHERE `user_id` =? AND `customer_id`=?";
            connection.query(sql, [latitude, longitude, access_token, username, email, address, user_id, authCustomer[0].customer_id], function (err, customer_updating) {
                if (err) {
                    console.log(err)
                    return;
                } else {
                    callback(authCustomer[0].customer_id);
                }
            });
        } else {
            var sql = "INSERT INTO `tb_customers` (`customer_latitude`,`customer_longitude`,`access_token`,`customer_username`,`customer_email`,`customer_phone`,`customer_address`,";
            sql += "`user_id`) VALUES (?,?,?,?,?,?,?,?)";
            connection.query(sql, [latitude, longitude, access_token, username, email, phone, address, user_id], function (err, customer_insert) {
                if (err) {
                    console.log(err)
                    return;
                } else {
                    callback(customer_insert.insertId);
                }
            });
        }
    });
}

/*
 * ----------------------------
 * INSERT JOB DETAILS
 * ----------------------------
 */

function insertJobDetails(auto_assignment, team_id, mongoData, actual_layout_type, order_id, access_token, job_pickup_email, has_delivery, send_push,
                          customer_email, customer_username1, customer_phone, customer_address, job_id, fleet_id, dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup, job_pickup_phone, jobT, job_pickup_latitude,
                          job_pickup_longitude, job_pickup_address, job_hash, latitude, longitude, job_description, job_pickup_datetime, job_delivery_datetime,
                          userid, lastInsertCustomerId, timezone, jobtime, fleet_name, customer_username, callback) {
    var job_time_utc = commonFunc.convertTimeIntoUTC(jobtime, timezone), fleets = [], fleet_names = [], index = 0;
    var job_status = constants.jobStatus.UPCOMING, list_of_job_ids = [], num_fleets = fleet_id.length;
    if (num_fleets == 0) {
        fleets[0] = null;
        fleet_names[0] = null;
        job_status = constants.jobStatus.UNASSIGNED;
    } else {
        fleets = fleet_id;
        fleet_names = fleet_name;
    }
    num_fleets = fleets.length;
    for (var i = 0; i < num_fleets; i++) {
        if (fleets[i]) {
            pickup_delivery_relationship = pickup_delivery_relationship + fleets[i];
        }
        var sql = "INSERT INTO `tb_jobs` (`team_id`,`order_id`,`job_status`,`job_pickup_email`,`has_delivery`,`fleet_id`,`dispatcher_id`,`pickup_delivery_relationship`," +
            "`job_pickup_name`,`has_pickup`,`job_pickup_phone`,`job_type`,`job_pickup_latitude`,`job_pickup_longitude`,`job_pickup_address`,`job_hash`,`job_latitude`," +
            "`job_longitude`,`job_address`,`job_description`,`job_pickup_datetime`,`job_delivery_datetime`,`user_id`,";
        sql += "`customer_id`,`timezone`,`job_time`,`job_time_utc`,`customer_username`,`customer_phone`,`customer_email`) " +
            "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(sql, [team_id, order_id, job_status, job_pickup_email, has_delivery, fleets[i], dispatcher_id, pickup_delivery_relationship, job_pickup_name,
            has_pickup, job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude, job_pickup_address, job_hash, latitude, longitude, customer_address,
            job_description, job_pickup_datetime, job_delivery_datetime, userid, lastInsertCustomerId, timezone, jobtime, job_time_utc, customer_username1, customer_phone, customer_email], function (err, result_insert) {
            if (err) {
                console.log(err);
                logging.logDatabaseQueryError("Error in inserting jobs info : ", err, result_insert);
            } else {
                mongo.getLayoutOptionalFieldsForUser(userid, actual_layout_type, function (OptionalFields) {
                    if ((OptionalFields.length > 0) && (typeof (OptionalFields[0].fields) != 'undefined')
                        && (typeof (OptionalFields[0].fields.app_optional_fields) != 'undefined') &&
                        (OptionalFields[0].fields.app_optional_fields.length > 0)) {

                        /*
                         Set check and remove fleet_data from fields
                         */
                        var dataValues = {}
                        if (mongoData.fields.items) {
                            mongoData.fields.items.forEach(function (field) {
                                if (mongoData.fields.items.hasOwnProperty('fleet_data')) {
                                    delete field['fleet_data'];
                                }
                                dataValues[field.label] = field.data || '';
                            });
                        } else {
                            mongoData.fields.items = []
                        }

                        /*
                         Check for extras fields
                         */
                        if (!mongoData.fields.extras) {
                            mongoData.fields.extras = {}
                        } else {

                            if (mongoData.fields.extras.invoice_html) {
                                mongoData.fields.extras.invoice_html = renderHtml(dataValues, mongoData.fields.extras.invoice_html)
                            }
                        }

                        /*
                         Check for jpeg/png images and links
                         */
                        if (mongoData.ref_images) {
                            var ref_images = [];
                            mongoData.ref_images.forEach(function (image) {
                                var regex = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif))/i;
                                if (regex.test(image)) {
                                    ref_images.push(image);
                                }
                            });
                            mongoData.ref_images = ref_images;
                        } else {
                            mongoData.ref_images = []
                        }

                        var value = {
                            app_optional_fields: OptionalFields[0].fields.app_optional_fields,
                            custom_field: mongoData.fields.items,
                            extras: mongoData.fields.extras,
                            ref_images: mongoData.ref_images
                        }
                    }
                    else {
                        var value = {
                            app_optional_fields: constants.defaultOptionalValue.DEFAULT,
                            custom_field: [],
                            extras: {},
                            ref_images: []
                        }
                        mongo.insertDefaultWorkflow(userid, actual_layout_type);
                    }

                    // Insert Task extra
                    // Data into mongo DB
                    // Fetch it when send to mobile APP.
                    mongo.insertTaskOptionalFieldsMONGO(userid, actual_layout_type, result_insert.insertId, value);

                    if (job_id) {
                        commonFunc.deleteJob(job_id);
                    }
                    index++;
                    if ((fleets[0] == null) && (OptionalFields.length > 0) &&
                        (typeof (OptionalFields[0].auto_assign) != 'undefined') &&
                        (parseInt(OptionalFields[0].auto_assign.is_enabled)) && (auto_assignment == 1)) {
                        var auto_assignment_data = {
                            job_id: result_insert.insertId,
                            broadcast_type: parseInt(OptionalFields[0].auto_assign.broadcast_type),
                            expires_in: parseInt(OptionalFields[0].auto_assign.expires_in),
                            user_id: userid,
                            job_pickup_latitude: job_pickup_latitude,
                            job_pickup_longitude: job_pickup_longitude,
                            job_pickup_datetime: job_pickup_datetime,
                            job_delivery_datetime: job_delivery_datetime,
                            latitude: latitude,
                            longitude: longitude,
                            has_pickup: has_pickup,
                            has_delivery: has_delivery,
                            accept_button: value.app_optional_fields[0].value || 0,
                            send_push: send_push.send_push,
                            dispatcher_id: dispatcher_id,
                            team_id: team_id,
                            job_type: jobT
                        }
                        autoAssign.auto_assign_task(auto_assignment_data, function (auto_assign_task_result) {
                            list_of_job_ids.push({job_id: result_insert.insertId});
                            sendResponseToDashboardAndFleet(constants.autoAssign.YES, team_id, value, userid, index, num_fleets, fleets, send_push, list_of_job_ids, job_pickup_email, has_delivery,
                                customer_email, customer_phone, customer_address, job_id, fleet_id, dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup,
                                job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude, job_pickup_address, job_hash, latitude, longitude, job_description,
                                job_pickup_datetime, job_delivery_datetime, lastInsertCustomerId, timezone, jobtime, job_status, fleet_names, access_token, customer_username,
                                job_time_utc);
                        });

                    } else {
                        list_of_job_ids.push({job_id: result_insert.insertId});
                        sendResponseToDashboardAndFleet(constants.autoAssign.NO, team_id, value, userid, index, num_fleets, fleets, send_push, list_of_job_ids, job_pickup_email, has_delivery,
                            customer_email, customer_phone, customer_address, job_id, fleet_id, dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup,
                            job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude, job_pickup_address, job_hash, latitude, longitude, job_description,
                            job_pickup_datetime, job_delivery_datetime, lastInsertCustomerId, timezone, jobtime, job_status, fleet_names, access_token, customer_username,
                            job_time_utc);
                    }
                });

            }
        });
    }
    function sendResponseToDashboardAndFleet(auto_assignment, teamID, mongo_data, user_id, index, num_fleets, fleets, send_push, job_ids_list, job_pickup_email, has_delivery,
                                             customer_email, customer_phone, customer_address, job_id, fleet_id, dispatcher_id, pickup_delivery_relationship, job_pickup_name, has_pickup,
                                             job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude, job_pickup_address, job_hash, latitude, longitude, job_description,
                                             job_pickup_datetime, job_delivery_datetime, lastInsertCustomerId, timezone, jobtime, job_status, fleet_names, access_token, customer_username, job_time_utc) {

        if (index == num_fleets) {

            commonFunc.sendTemplateEmailAndSMS(user_id, 'REQUEST_RECEIVED', job_ids_list[0].job_id);

            for (var i = 0; i < num_fleets; i++) {
                (function (i) {
                    if (fleets[i] != null) {
                        // SCHEDULE FLEET NOTIFICATION
                        cron.scheduleFleetNotification(user_id, job_ids_list[i].job_id, fleet_id[i], jobtime, job_time_utc, jobT, timezone);

                        if (send_push.send_push) {
                            // Send a push notification to the fleet that a
                            // new job has been created for him
                            var message_fleet = 'A new task has been assigned.';
                            var driverOfflineMessage = 'Hi [Fleet name], a new task has been assigned to you. Kindly log into app to see the details.';
                            var payload_fleet = {
                                flag: constants.notificationFlags.JOB_ASSIGN,
                                message: message_fleet,
                                job_id: job_ids_list[i].job_id,
                                job_type: jobT,
                                cust_name: jobT == constants.jobType.PICKUP ? job_pickup_name : customer_username,
                                start_address: send_push.start_address,
                                start_time: moment(job_pickup_datetime, parseFormat(job_pickup_datetime)).format('YYYY-MM-DD HH:mm:ss'),
                                end_time: moment(job_delivery_datetime, parseFormat(job_delivery_datetime)).format('YYYY-MM-DD HH:mm:ss'),
                                end_address: send_push.end_address,
                                accept_button: mongo_data.app_optional_fields[0].value || 0,
                                d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                            };
                            commonFunc.sendNotification(fleets[i], message_fleet, false, payload_fleet, driverOfflineMessage);
                        }
                    }
                    if (send_push.sock)
                        socketResponse.sendSocketResponse(job_ids_list[i].job_id);
                })(i);
            }
            callback({
                status: true,
                job_id: job_ids_list[0].job_id
            });
        }
    }
}


//Render HTML

function renderHtml(values, html) {
    return Handlebars.compile(html)(values);
}
/*
 * ----------------------------
 * CHECK AVAILABILITY OF FLEET
 * ----------------------------
 */

function checkFleetAvailability(fleet, job_pickup_datetime, job_delivery_datetime, callback) {
    var check_code = "SELECT * FROM `tb_jobs` WHERE (`job_pickup_datetime`>=? || `job_delivery_datetime`<=?) AND `fleet_id`=? AND `job_status` IN (0,1) LIMIT 1";
    connection.query(check_code, [job_pickup_datetime, job_delivery_datetime, fleet.fleet_id], function (err, result) {
        console.log(result);
        if (err) {
            logging.logDatabaseQueryError("Checking for fleet availability ", err, result);
            return;
        } else {
            if (result.length > 0) {
                var response = {
                    "job": result[0],
                    "fleet": {
                        "fleet_id": fleet.fleet_id,
                        "email": fleet.email,
                        "username": fleet.username,
                        "phone": fleet.phone,
                        "latitude": fleet.latitude,
                        "longitude": fleet.longitude,
                        "fleet_image": fleet.fleet_image
                    },
                    "status": true
                };
                callback(response);
            }
            else {
                var response = {
                    "job": result[0],
                    "fleet": {
                        "fleet_id": fleet.fleet_id,
                        "email": fleet.email,
                        "username": fleet.username,
                        "phone": fleet.phone,
                        "latitude": fleet.latitude,
                        "longitude": fleet.longitude,
                        "fleet_image": fleet.fleet_image
                    },
                    "status": false
                };
                callback(response);
            }
        }
    });
}


/*
 * ----------------------------
 * CHECK FLEET CURRENT JOBS
 * ----------------------------
 */

exports.checkFleetCurrentJobs = function (req, res) {

    var access_token = req.body.access_token;
    var job_pickup_datetime = req.body.job_pickup_datetime;
    var job_delivery_datetime = req.body.job_delivery_datetime;
    var fleet_id = req.body.fleet_id;
    if (typeof(job_pickup_datetime) === "undefined") {
        job_pickup_datetime = null
    }
    if (typeof(job_delivery_datetime) === "undefined") {
        job_delivery_datetime = null
    }
    var manvalues = [fleet_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = '';
                        var bindParams = [];
                        var added_onehour_job_pickup_datetime;
                        var subtracted_onehour_job_pickup_datetime;
                        var added_onehour_job_delivery_datetime;
                        var subtracted_onehour_job_delivery_datetime;
                        if (job_pickup_datetime != null && job_delivery_datetime == null) {
                            added_onehour_job_pickup_datetime = commonFunc.addHOUR(job_pickup_datetime);
                            subtracted_onehour_job_pickup_datetime = commonFunc.subtractHOUR(job_pickup_datetime);
                            sql += "SELECT `job_id`,`job_type`,`job_time`,`job_status`,`job_pickup_datetime`,`job_delivery_datetime` FROM `tb_jobs` WHERE (`job_time`>=? && `job_time`<=?) AND `fleet_id`=? AND `job_status` IN (0,1) ";
                            bindParams = [subtracted_onehour_job_pickup_datetime, added_onehour_job_pickup_datetime, fleet_id, fleet_id];
                        } else if (job_pickup_datetime == null && job_delivery_datetime != null) {
                            added_onehour_job_delivery_datetime = commonFunc.addHOUR(job_delivery_datetime);
                            subtracted_onehour_job_delivery_datetime = commonFunc.subtractHOUR(job_delivery_datetime);
                            sql += "SELECT `job_id`,`job_type`,`job_time`,`job_status`,`job_pickup_datetime`,`job_delivery_datetime` FROM `tb_jobs` WHERE (`job_time`>=? && `job_time`<=?) AND `fleet_id`=? AND `job_status` IN (0,1) ";
                            bindParams = [subtracted_onehour_job_delivery_datetime, added_onehour_job_delivery_datetime, fleet_id];
                        } else if (job_pickup_datetime != null && job_delivery_datetime != null) {
                            added_onehour_job_pickup_datetime = commonFunc.addHOUR(job_pickup_datetime);
                            subtracted_onehour_job_pickup_datetime = commonFunc.subtractHOUR(job_pickup_datetime);
                            added_onehour_job_delivery_datetime = commonFunc.addHOUR(job_delivery_datetime);
                            subtracted_onehour_job_delivery_datetime = commonFunc.subtractHOUR(job_delivery_datetime);
                            sql += "SELECT `job_id`,`job_type`,`job_time`,`job_status`,`job_pickup_datetime`,`job_delivery_datetime` FROM `tb_jobs` WHERE (`job_time`>=? && `job_time`<=?) AND `fleet_id`=? AND `job_status` IN (0,1) " +
                                "UNION " +
                                "SELECT `job_id`,`job_type`,`job_time`,`job_status`,`job_pickup_datetime`,`job_delivery_datetime` FROM `tb_jobs` WHERE (`job_time`>=? && `job_time`<=?) AND `fleet_id`=? AND `job_status` IN (0,1)";
                            bindParams = [subtracted_onehour_job_pickup_datetime, added_onehour_job_pickup_datetime, fleet_id, subtracted_onehour_job_delivery_datetime, added_onehour_job_delivery_datetime, fleet_id];
                        }
                        connection.query(sql, bindParams, function (err, fleetCurrentJobsResult) {
                            console.log(fleetCurrentJobsResult);
                            if (err) {
                                logging.logDatabaseQueryError("Checking for fleet current jobs ", err, fleetCurrentJobsResult);
                                return;
                            } else {
                                var result_jobs_length = fleetCurrentJobsResult.length;
                                for (var i = 0; i < result_jobs_length; i++) {
                                    if (fleetCurrentJobsResult[i].job_time != '0000-00-00 00:00:00') {
                                        fleetCurrentJobsResult[i].job_time = fleetCurrentJobsResult[i].job_time.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                    }
                                }
                                responses.actionCompleteResponse(res);
                                return;
                            }
                        });
                    }
                });
            }
        });
    }
}


/*
 *------------------
 * ADD ETA SECONDS
 *------------------
 */
function addSeconds(date, seconds) {
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() + (seconds * 1000)); // add seconds
    return new Date(newDate)
}

/*
 * -------------------------------
 * DELETE PARTICULAR JOB
 * -------------------------------
 */

exports.delete_job = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var manvalues = [access_token, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var user_id = result[0].user_id, fleet_id, index = 0, allResponses = [];
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            user_id = result[0].dispatcher_user_id;
                        }
                        checkTaskAndDelete(index);
                        function changeIndex() {
                            index++;
                            return checkTaskAndDelete(index);
                        }

                        var tasks = job_id.toString().split(','), num_tasks = tasks.length;

                        function checkTaskAndDelete(item) {
                            if (item > num_tasks - 1) {
                                res.send(JSON.stringify(allResponses.length > 1 ? allResponses : allResponses[0]));
                            } else {
                                commonFunc.delay2(item, function (i) {
                                    (function (i) {
                                        commonFunc.authenticateUserIdAndJobId(user_id, tasks[i], function (authenticateUserIdAndJobIdResult) {
                                            if (authenticateUserIdAndJobIdResult == 0) {
                                                var response = {
                                                    "message": constants.responseMessages.JOB_NOT_MAPPED_WITH_YOU,
                                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                    "data": {}
                                                };
                                                allResponses.push(response);
                                                changeIndex();
                                            } else {
                                                fleet_id = authenticateUserIdAndJobIdResult[0].fleet_id;
                                                deleteTask(tasks[i], fleet_id, authenticateUserIdAndJobIdResult, function (deleteTaskResult) {
                                                    var response = {
                                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                                        "data": {}
                                                    };
                                                    allResponses.push(response);
                                                    changeIndex();
                                                });
                                            }
                                        });
                                    })(i);
                                });
                            }
                        }

                        function deleteTask(job_id, fleet_id, authRes, callback) {
                            var sql = "UPDATE `tb_jobs` SET `job_status`=? WHERE `job_id`=? LIMIT 1";
                            connection.query(sql, [constants.jobStatus.DELETED, job_id], function (err, result_delete_job) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in deleting job information  : ", err, result_delete_job);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    commonFunc.setTaskHistory(fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Deleted at");
                                    var team_id = "-2", teams = {};
                                    teams[team_id] = {};
                                    teams[team_id].jobs = {};
                                    teams[team_id].jobs[job_id] = {};
                                    teams[team_id].jobs[job_id] = job_id;

                                    if (fleet_id != null) {

                                        var sql = "SELECT `fleet_task_notify_id` FROM `tb_fleet_task_notify` " +
                                            "WHERE `job_id`=? AND `fleet_id`=? AND `user_id`=?";
                                        connection.query(sql, [job_id, fleet_id, user_id], function (err, schedules) {
                                            schedules.forEach(function (sd) {
                                                cron.deleteSchedule('schedule' + sd.fleet_task_notify_id);
                                            });
                                            var deleteSql = "DELETE FROM `tb_fleet_task_notify` " +
                                                "WHERE `job_id`=? AND `fleet_id`=? AND `user_id`=?";
                                            connection.query(deleteSql, [job_id, fleet_id, user_id], function (err, result_delete_schedule) {
                                            });
                                        });

                                        //Delete from routed tasks List if Routed
                                        if (authRes[0].is_routed) {
                                            mongo.deleteFromRoutedTasks(fleet_id, job_id);
                                        }

                                        // Send a push notification to the fleet that a
                                        //  job has been deleted for him
                                        var message_fleet = 'Task has been deleted.';
                                        var driverOfflineMessage = 'Hi [Fleet name], A task has been deleted. Kindly log into app to see the details.';
                                        var payload_fleet = {
                                            flag: constants.notificationFlags.JOB_DELETED,
                                            message: message_fleet,
                                            job_id: job_id,
                                            end_time: authRes[0].job_delivery_datetime,
                                            start_time: authRes[0].job_pickup_datetime,
                                            job_type: authRes[0].job_type,
                                            cust_name: authRes[0].job_type == constants.jobType.PICKUP ? authRes[0].job_pickup_name : authRes[0].customer_username,
                                            start_address: authRes[0].job_pickup_address,
                                            end_address: authRes[0].job_address,
                                            accept_button: 0,
                                            d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                        };
                                        var notificationFlag_driver = 1;
                                        commonFunc.sendNotification(fleet_id, message_fleet, notificationFlag_driver, payload_fleet, driverOfflineMessage);
                                    }
                                    callback(true);
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};

/*
 * -------------------------
 * EDIT TASK WITH NEW LAYOUT
 * -------------------------
 */

exports.edit_task = function (req, res) {

    var access_token = req.body.access_token,
        customer_address = req.body.customer_address,
        latitude = req.body.latitude,
        longitude = req.body.longitude,
        job_id = req.body.job_id,
        fields = req.body.meta_data,
        ref_images = req.body.ref_images;
    if (req.body.hasOwnProperty('latitude') && req.body.hasOwnProperty('longitude')) {
        req.body.job_latitude = req.body.latitude;
        req.body.job_longitude = req.body.longitude;
    }
    if (req.body.hasOwnProperty('customer_address')) {
        req.body.job_address = req.body.customer_address;
    }
    if (typeof req.body.notify === "undefined" || req.body.notify === '') {
        req.body.notify = 1;
    }
    console.log("REQUEST ", JSON.stringify(req.body), fields);
    if (typeof fields === "undefined" || fields == '') {
        fields = [];
    } else if (typeof fields === "string") {
        fields = fields.replace(/'/g, '"');
        fields = JSON.parse(fields);
    }
    if (typeof ref_images === "undefined" || ref_images === '' || typeof ref_images != "object") {
        ref_images = [];
    }
    if (req.body.hasOwnProperty('job_description')) {
        req.body.job_description = req.body.job_description.replace(/'/g, "`");
        req.body.job_description = req.body.job_description.replace(/"/g, "`");
    }
    if (req.body.hasOwnProperty('job_delivery_datetime')) {
        req.body.job_delivery_datetime = moment(req.body.job_delivery_datetime, parseFormat(req.body.job_delivery_datetime)).format('YYYY-MM-DD HH:mm');
    }
    if (req.body.hasOwnProperty('job_pickup_datetime')) {
        req.body.job_pickup_datetime = moment(req.body.job_pickup_datetime, parseFormat(req.body.job_pickup_datetime)).format('YYYY-MM-DD HH:mm');
    }
    var manvalues = [access_token, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var accepted_fleet_id = [], accepted_fleet_name = [], user_id = result[0].user_id, layout_type = result[0].layout_type;

                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var dispatcher_id = null;
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            commonFunc.checkDispatcherPermissions(result[0].user_id, function (chk) {
                                if (chk && chk.length && chk[0].create_task == constants.hasPermissionStatus.NO) {
                                    responses.invalidAccessError(res);
                                }
                                else {
                                    dispatcher_id = user_id;
                                    user_id = result[0].dispatcher_user_id;
                                    authorizeUser(access_token, user_id, job_id, layout_type, accepted_fleet_id, accepted_fleet_name, req);
                                }
                            })
                        } else {
                            authorizeUser(access_token, user_id, job_id, layout_type, accepted_fleet_id, accepted_fleet_name, req);
                        }

                        // Authorize user and step in
                        function authorizeUser(access_token, user_id, job_id, layout_type, accepted_fleet_id, accepted_fleet_name, req) {
                            commonFunc.authenticateUserIdAndJobId(user_id, job_id, function (job_data) { //Checks for task mapping
                                if (job_data == 0) {
                                    var response = {
                                        "message": constants.responseMessages.JOB_NOT_MAPPED_WITH_YOU,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                } else {
                                    job_data[0].current_fleet_id = job_data[0].fleet_id
                                    job_data[0].task_time = job_data[0].job_time;
                                    job_data.forEach(function (data) {
                                        var keys = Object.keys(req.body);
                                        for (var i in keys) {
                                            if (req.body.hasOwnProperty(keys[i]) && data.hasOwnProperty(keys[i])) {
                                                data[keys[i]] = req.body[keys[i]];
                                            }
                                        }
                                    })
                                    commonFunc.getRegisteredFleet(job_data[0].user_id, job_data[0].fleet_id, function (getRegisteredFleetResult) {
                                        if (getRegisteredFleetResult != 0) {
                                            var getRegisteredFleetResultLength = getRegisteredFleetResult.length;
                                            for (var i = 0; i < getRegisteredFleetResultLength; i++) {
                                                if (getRegisteredFleetResult[i].is_active) {
                                                    accepted_fleet_id.push(getRegisteredFleetResult[i].fleet_id);
                                                    accepted_fleet_name.push(getRegisteredFleetResult[i].username);
                                                }
                                            }
                                        }
                                        validateTeams(job_data[0].team_id, access_token, layout_type, user_id, accepted_fleet_id, accepted_fleet_name, job_data, req);
                                    });
                                }
                            });
                        }

                        function validateTeams(team_id, access_token, layout_type, user_id, accepted_fleet_id, accepted_fleet_name, job_data, req) {
                            if (team_id) {
                                commonFunc.authenticateFleetIDAndTeamID(user_id, team_id, accepted_fleet_id, function (authenticateFleetIDAndTeamIDResult) {
                                    accepted_fleet_id = [], accepted_fleet_name = [];
                                    if (authenticateFleetIDAndTeamIDResult != 0) {
                                        var authenticateFleetIDAndTeamIDResultLength = authenticateFleetIDAndTeamIDResult.length;
                                        for (var i = 0; i < authenticateFleetIDAndTeamIDResultLength; i++) {
                                            accepted_fleet_id.push(authenticateFleetIDAndTeamIDResult[i].fleet_id);
                                            accepted_fleet_name.push(authenticateFleetIDAndTeamIDResult[i].username);
                                        }
                                    }
                                    addCustomer(access_token, layout_type, user_id, accepted_fleet_id, accepted_fleet_name, job_data, req);
                                })
                            } else {
                                addCustomer(access_token, layout_type, user_id, accepted_fleet_id, accepted_fleet_name, job_data, req);
                            }
                        }


                        function addCustomer(access_token, layout_type, user_id, fleet_id, fleet_name, job_data, req) {
                            async.parallel({
                                customerId1: function (callback) {
                                    if ((job_data[0].has_pickup == constants.hasPickup.NO) || (layout_type == constants.layoutType.APPOINTMENT) || (layout_type == constants.layoutType.FOS)) {
                                        module.exports.addUpdateCustomer(user_id, job_data[0].customer_phone, job_data[0].customer_username, job_data[0].customer_email, job_data[0].job_address, job_data[0].job_latitude, job_data[0].job_longitude, function (customerAddition) {
                                            callback(null, customerAddition);
                                        })
                                    } else {
                                        callback(null, 0);
                                    }
                                },
                                customerId2: function (callback) {
                                    if (job_data[0].has_pickup == constants.hasPickup.YES) {
                                        module.exports.addUpdateCustomer(user_id, job_data[0].job_pickup_phone, job_data[0].job_pickup_name, job_data[0].job_pickup_email, job_data[0].job_pickup_address, job_data[0].job_pickup_latitude, job_data[0].job_pickup_longitude, function (customerAddition) {
                                            callback(null, customerAddition);
                                        })
                                    } else {
                                        callback(null, 0);
                                    }

                                }
                            }, function (err, results) {
                                var customer_id = results.customerId1 == 0 ? results.customerId2 : results.customerId1;
                                updatetask(job_data[0].access_token, layout_type, customer_id, user_id, fleet_id, fleet_name, job_data, req);
                            });
                        }

                        function updatetask(access_token, layout_type, lastInsertCustomerId, user_id, fleet_id, fleet_name, job_data, req) {
                            var job_type = job_data[0].job_type, jobT, jobtime;
                            if (job_type == constants.jobType.PICKUP) {
                                jobT = constants.jobType.PICKUP;
                                jobtime = job_data[0].job_pickup_datetime;
                            } else if (job_type == constants.jobType.DELIVERY) {
                                jobT = constants.jobType.DELIVERY;
                                jobtime = job_data[0].job_delivery_datetime
                            } else if (job_type == constants.jobType.APPOINTMENT) {
                                jobT = constants.jobType.APPOINTMENT;
                                jobtime = job_data[0].job_pickup_datetime;
                            } else if (job_type == constants.jobType.FOS) {
                                jobT = constants.jobType.FOS;
                                jobtime = job_data[0].job_pickup_datetime;
                            }
                            updateJobDetails({
                                    fields: fields,
                                    ref_images: ref_images
                                }, layout_type, job_data[0].access_token, job_data[0].job_pickup_email, req.body.notify, job_data[0].job_status, jobT, job_data[0].dispatcher_id, job_data[0].job_pickup_name, job_data[0].job_pickup_phone,
                                job_data[0].job_pickup_latitude, job_data[0].job_pickup_longitude, job_data[0].job_pickup_address, job_data[0].job_latitude, job_data[0].job_longitude, job_data[0].job_address, job_data[0].job_description,
                                job_data[0].job_pickup_datetime, job_data[0].job_delivery_datetime, fleet_id, user_id, lastInsertCustomerId, job_data[0].timezone, jobtime, job_data[0].pickup_delivery_relationship,
                                job_data[0].job_id, fleet_name, job_data[0].customer_username, job_data[0].customer_email, job_data[0].customer_phone, job_data, function (updateJobDetailsResult) {
                                    if (updateJobDetailsResult) {
                                        responses.actionCompleteResponse(res);
                                    }
                                });
                        }
                    }
                });
            }
        });
    }
};


/*
 * --------------------
 * UPDATE JOB DETAILS
 * --------------------
 */

function updateJobDetails(mongoData, layout_type, access_token, job_pickup_email, send_push, jobstatus, jobT, dispatcher_id, job_pickup_name, job_pickup_phone, job_pickup_latitude,
                          job_pickup_longitude, job_pickup_address, latitude, longitude, customer_address, job_description, job_pickup_datetime,
                          job_delivery_datetime, fleet_id, userid, lastInsertCustomerId, timezone, jobtime, pickup_delivery_relationship, job_id,
                          fleet_name, customer_username, customer_email, customer_phone, job_data, callback) {

    var job_time_utc = commonFunc.convertTimeIntoUTC(jobtime, timezone), fleets = [], fleet_names = [], index = 0, num_fleets = fleet_id.length;
    if (num_fleets == 0) {
        fleets[0] = null;
        fleet_names[0] = null;
    } else {
        fleets = fleet_id;
        fleet_names = fleet_name;
    }
    num_fleets = fleets.length;
    for (var i = 0; i < num_fleets; i++) {

        if (job_data[0].current_fleet_id && job_data[0].is_routed == 1 &&
            ((job_data[0].current_fleet_id != fleets[i]) || (new Date(job_data[0].task_time).getTime() != new Date(jobtime).getTime()) )) {
            job_data[0].is_routed = 0;
            mongo.deleteFromRoutedTasks(job_data[0].fleet_id, job_id);
        }
        if (fleets[i] && jobstatus == constants.jobStatus.UNASSIGNED) {
            jobstatus = constants.jobStatus.ASSIGNED;
        }
        if (fleets[i] == null) {
            jobstatus = constants.jobStatus.UNASSIGNED;
        }
        if (jobstatus == constants.jobStatus.UNASSIGNED) {
            fleets[i] = null;
        }
        if (jobstatus == constants.jobStatus.DECLINE) {
            jobstatus = constants.jobStatus.ASSIGNED;
        }
        var sql = "UPDATE `tb_jobs` SET `team_id`=?,`job_pickup_email`=?,`job_status`=?,`job_type`=?,`dispatcher_id`=?,`job_pickup_name`=?,`job_pickup_phone`=?," +
            "`job_pickup_latitude`=?,`job_pickup_longitude`=?,`job_pickup_address`=?,`job_latitude`=?,`job_longitude`=?,`job_address`=?,`job_description`=?," +
            "`job_pickup_datetime`=?,`job_delivery_datetime`=?,`fleet_id`=?,`user_id`=?,`is_routed`=? ";
        if (jobstatus == constants.jobStatus.UNASSIGNED) {
            sql += ",fleet_id = null ";
        }
        if (jobstatus == constants.jobStatus.ARRIVED) {
            sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN NOW() ELSE `acknowledged_datetime` END ";
            sql += ",`started_datetime` = CASE WHEN `started_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `started_datetime` END ";
            sql += ",`arrived_datetime` = NOW() ";
        }
        if (jobstatus == constants.jobStatus.STARTED) {
            sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN NOW() ELSE `acknowledged_datetime` END ";
            sql += ",`started_datetime` = NOW() ";
        }
        if ((jobstatus == constants.jobStatus.ENDED) || (jobstatus == constants.jobStatus.FAILED) || (jobstatus == constants.jobStatus.CANCEL) || (jobstatus == constants.jobStatus.DECLINE)) {
            sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `acknowledged_datetime` END ";
            sql += ",`started_datetime` = CASE WHEN `started_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `started_datetime` END ";
            sql += ",`arrived_datetime` = CASE WHEN `arrived_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `arrived_datetime` END ";
            sql += ",`completed_datetime` = NOW() ";
        }
        sql += ",`customer_id`=?,`timezone`=?,`job_time`=?,`job_time_utc`=?,`customer_username`=?,`customer_phone`=?,`customer_email`=? WHERE `job_id`=? LIMIT 1 ";
        connection.query(sql, [job_data[0].team_id, job_pickup_email, jobstatus, jobT, dispatcher_id, job_pickup_name, job_pickup_phone, job_pickup_latitude, job_pickup_longitude,
            job_pickup_address, latitude, longitude, customer_address, job_description, job_pickup_datetime, job_delivery_datetime, fleets[i], userid, job_data[0].is_routed,
            lastInsertCustomerId, timezone, jobtime, job_time_utc, customer_username, customer_phone, customer_email, job_id], function (err, update_jobs) {

            index++;
            sendresponseback(mongoData, layout_type, job_data[0].team_id, userid, index, num_fleets, fleets, send_push, job_id, job_pickup_email, customer_address, fleet_id, dispatcher_id, pickup_delivery_relationship,
                job_pickup_name, job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude, job_pickup_address, latitude, longitude, job_description,
                job_pickup_datetime, job_delivery_datetime, lastInsertCustomerId, timezone, jobtime, jobstatus, fleet_names, access_token, customer_username, job_time_utc);

        });
    }
    function sendresponseback(mongoData, layout_type, teamID, user_id, index, num_fleets, fleets, send_push, job_id, job_pickup_email, customer_address, fleet_id, dispatcher_id, pickup_delivery_relationship, job_pickup_name,
                              job_pickup_phone, jobT, job_pickup_latitude, job_pickup_longitude, job_pickup_address, latitude, longitude, job_description,
                              job_pickup_datetime, job_delivery_datetime, lastInsertCustomerId, timezone, jobtime, jobstatus, fleet_names, access_token, customer_username, job_time_utc) {
        if (index == num_fleets) {
            for (var i = 0; i < num_fleets; i++) {
                (function (i) {
                    // SCHEDULE FLEET NOTIFICATION
                    cron.updateScheduleFleetNotification(user_id, job_id, fleet_id[i], jobtime, job_time_utc, jobT, timezone)

                    mongo.getLayoutOptionalFieldsForUser(user_id, layout_type, function (optionalFields) {

                        mongo.getOptionalFieldsForTask(user_id, job_id, {}, function (metaData) {

                            if (metaData.fields.custom_field && metaData.fields.custom_field.length && metaData.fields.custom_field[0].template_id) {
                                mongo.processOptionalLayout(optionalFields, metaData.fields.custom_field[0].template_id, function (cal) {
                                    mongoData.fields.extras = cal.extras;
                                })
                            }
                            if (!mongoData.fields.items) mongoData.fields.items = metaData.fields.custom_field;
                            if (!mongoData.fields.extras) mongoData.fields.extras = metaData.fields.extras || {};
                            if (!mongoData.ref_images) mongoData.ref_images = metaData.fields.ref_images || [];

                            /*
                             Check for jpeg/png images and links
                             */
                            if (mongoData.ref_images) {
                                var ref_images = [];
                                mongoData.ref_images.forEach(function (image) {
                                    var regex = /(https?:\/\/.*\.(?:png|jpg|jpeg|tif|gif))/i;
                                    if (regex.test(image)) {
                                        ref_images.push(image);
                                    }
                                });
                                mongoData.ref_images = ref_images;
                            } else {
                                mongoData.ref_images = []
                            }
                            var dataValues = {}
                            if (mongoData.fields.items) {
                                mongoData.fields.items.forEach(function (field) {
                                    dataValues[field.label] = field.data || '';
                                });
                            } else {
                                mongoData.fields.items = []
                            }
                            if (!mongoData.fields.extras) {
                                mongoData.fields.extras = {}
                            } else {
                                if (mongoData.fields.extras.invoice_html) {
                                    mongoData.fields.extras.invoice_html = renderHtml(dataValues, mongoData.fields.extras.invoice_html)
                                }
                            }
                            if ((optionalFields.length > 0) && (typeof (optionalFields[0].fields) != 'undefined')
                                && (typeof (optionalFields[0].fields.app_optional_fields) != 'undefined') && (optionalFields[0].fields.app_optional_fields.length > 0)) {
                                var value = {
                                    app_optional_fields: optionalFields[0].fields.app_optional_fields,
                                    custom_field: mongoData.fields.items,
                                    extras: mongoData.fields.extras,
                                    ref_images: mongoData.ref_images
                                }
                            } else {
                                var value = {
                                    app_optional_fields: constants.defaultOptionalValue.DEFAULT,
                                    custom_field: [],
                                    extras: {},
                                    ref_images: []
                                }
                                mongo.insertDefaultWorkflow(user_id, layout_type);
                            }
                            mongoData = value;
                            mongo.insertTaskOptionalFieldsMONGO(user_id, layout_type, job_id, value);

                            if ((fleets[i] != null) && (send_push)) {
                                var message_fleet = '', driverOfflineMessage = '', flag = constants.notificationFlags.REASSIGN;
                                if (job_data[0].current_fleet_id != fleets[i]) {
                                    message_fleet = 'A new task has been assigned.';
                                    driverOfflineMessage = 'Hi [Fleet name], a new task has been assigned to you. Kindly log into app to see the details.';
                                } else {
                                    if (job_data[0].is_routed == 1) {
                                        flag = constants.notificationFlags.SILENT;
                                    } else {
                                        message_fleet = 'Task has been rescheduled.';
                                        driverOfflineMessage = 'Hi [Fleet name], a task has been rescheduled to you. Kindly log into app to see the details.';
                                    }
                                }
                                var payload_fleet = {
                                    flag: flag,
                                    message: message_fleet,
                                    job_id: job_id,
                                    job_type: jobT,
                                    cust_name: jobT == constants.jobType.PICKUP ? job_pickup_name : customer_username,
                                    start_address: job_pickup_address,
                                    start_time: moment(job_pickup_datetime, parseFormat(job_pickup_datetime)).format('YYYY-MM-DD HH:mm:ss'),
                                    end_address: customer_address,
                                    end_time: moment(job_delivery_datetime, parseFormat(job_delivery_datetime)).format('YYYY-MM-DD HH:mm:ss'),
                                    accept_button: mongoData.app_optional_fields[0].value || 0,
                                    d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                };
                                commonFunc.sendNotification(fleets[i], message_fleet, false, payload_fleet, driverOfflineMessage);

                                if ((job_data[0].current_fleet_id) && (job_data[0].current_fleet_id != fleets[i])) {
                                    var message_fleet1 = "Task has been deleted.";
                                    var payload_fleet = {
                                        flag: constants.notificationFlags.JOB_DELETED,
                                        message: message_fleet1,
                                        job_id: job_id,
                                        job_type: jobT,
                                        cust_name: jobT == constants.jobType.PICKUP ? job_pickup_name : customer_username,
                                        start_address: job_pickup_address,
                                        start_time: moment(job_pickup_datetime, parseFormat(job_pickup_datetime)).format('YYYY-MM-DD HH:mm:ss'),
                                        end_address: customer_address,
                                        end_time: moment(job_delivery_datetime, parseFormat(job_delivery_datetime)).format('YYYY-MM-DD HH:mm:ss'),
                                        accept_button: mongoData.app_optional_fields[0].value || 0,
                                        d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                    };
                                    commonFunc.sendNotification(job_data[0].current_fleet_id, message_fleet1, false, payload_fleet, driverOfflineMessage);
                                }
                            }
                        });
                    });
                })(i);
            }
            socketResponse.sendSocketResponse(job_id);
            callback(true);
        }
    }
}


/*
 * --------------------------
 * VIEW NOTIFICATION JOB DETAILS
 * --------------------------
 */
exports.view_notification_job_details = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var manvalues = [access_token, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    notification_job_details(result[0].dispatcher_user_id);
                } else {
                    notification_job_details(user_id);
                }
                function notification_job_details(user_id) {
                    commonFunc.authenticateUserIdAndJobId(user_id, job_id, function (authenticateUserIdAndJobIdResult) {
                        if (authenticateUserIdAndJobIdResult == 0) {
                            responses.authenticationErrorFleetAndJobID(res);
                            return;
                        } else {
                            var sql = "SELECT  fleets.`fleet_id`,fleets.`username` as `fleet_name`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`completed_by_admin`," +
                                "jobs.`pickup_delivery_relationship`,jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`,customers.`customer_id`,customers.`customer_username`,customers.`customer_phone`,customers.`customer_email`" +
                                " FROM `tb_jobs` jobs INNER JOIN `tb_customers` customers ON customers.`customer_id`= jobs.`customer_id` LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
                                "WHERE jobs.`user_id`=? AND jobs.`job_id`=?";
                            connection.query(sql, [user_id, job_id], function (err, result_jobs) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in view_notification_job_details info : ", err, result_jobs);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    if (result_jobs[0].job_pickup_datetime != '0000-00-00 00:00:00') {
                                        result_jobs[0].job_pickup_datetime = result_jobs[0].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                    }
                                    if (result_jobs[0].job_delivery_datetime != '0000-00-00 00:00:00') {
                                        result_jobs[0].job_delivery_datetime = result_jobs[0].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                    }
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": result_jobs
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            });
                        }
                    });
                }
            }
        });
    }
};

/*
 *------------------------
 * GET ALL FLEET MOVEMENTS
 * -----------------------
 */
exports.view_all_fleet_points = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var user_id = req.body.user_id;
    var manvalues = [access_token, job_id, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    user_id = result[0].dispatcher_user_id;
                }
                var sql = "SELECT jobs.`fleet_id`," +
                    " jobs.`started_datetime`,jobs.`completed_datetime`,jobs.`arrived_datetime`,jobs.`acknowledged_datetime` " +
                    " FROM `tb_jobs` jobs " +
                    " WHERE jobs.`user_id`=? AND jobs.`job_id`=?";
                connection.query(sql, [user_id, job_id], function (err, task) {
                    if (err) {
                        responses.sendError(res);
                        return;
                    } else {
                        if (task && task.length > 0) {
                            if (task[0].fleet_id) {
                                if (task[0].started_datetime == "0000-00-00 00:00:00") task[0].started_datetime = "";
                                if (task[0].completed_datetime == '0000-00-00 00:00:00') task[0].completed_datetime = "";
                                if (task[0].arrived_datetime == "0000-00-00 00:00:00") task[0].arrived_datetime = "";
                                if (task[0].acknowledged_datetime == "0000-00-00 00:00:00") task[0].acknowledged_datetime = "";
                                commonFunc.getFleetMovement(0, task[0].fleet_id, task[0].started_datetime, task[0].completed_datetime, task[0], function (getFleetMovementResult) {
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": getFleetMovementResult.fleet_movement
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                });
                            } else {
                                responses.noDataFoundError(res);
                            }
                        } else {
                            responses.noDataFoundError(res);
                        }
                    }
                })
            }
        })
    }
}
/*
 * --------------------------
 * VIEW TASK PROFILE
 * --------------------------
 */
exports.view_task_profile = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var user_id = req.body.user_id;
    var manvalues = [access_token, job_id, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    view_task_details(result[0].dispatcher_user_id);
                } else {
                    view_task_details(user_id);
                }

                function view_task_details(user_id) {
                    var sql = "SELECT  fleets.`fleet_id` as job_details_by_fleet,fleets.`fleet_id`,fleets.`username` as `fleet_name`," +
                        " tm.team_name," +
                        " jobs.`total_distance_travelled`,jobs.`user_id`,jobs.`customer_username`,jobs.`customer_phone`,jobs.`customer_email`,jobs.`job_hash`,jobs.`team_id`,jobs.`job_type`," +
                        " jobs.`is_customer_rated`,jobs.`customer_comment`,jobs.`fleet_rating` as `customer_rating`,jobs.`job_pickup_email`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`," +
                        " jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`completed_by_admin`," +
                        " jobs.`pickup_delivery_relationship`,jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`," +
                        " jobs.`started_datetime`,jobs.`completed_datetime`,jobs.`arrived_datetime`,jobs.`acknowledged_datetime` " +
                        " FROM `tb_jobs` jobs " +
                        " LEFT JOIN `tb_teams` tm ON tm.team_id = jobs.team_id " +
                        " LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
                        " WHERE jobs.`user_id`=? AND jobs.`job_id`=? ";
                    connection.query(sql, [user_id, job_id], function (err, profile) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in viewing task profile info : ", err, profile);
                            responses.sendError(res);
                            return;
                        } else {
                            var finalResult = [], task_history = [], fleet_movement = [];

                            if (profile.length > 0) {
                                fieldTasks(profile);
                            } else {
                                responses.invalidAccessError(res);
                                return;
                            }

                            function fieldTasks(profile) {
                                var result_task_profile_length = profile.length;
                                var counter = 0;
                                for (var i = 0; i < result_task_profile_length; i++) {
                                    if (profile[i].customer_username == "dummy") profile[i].customer_username = "";
                                    if (profile[i].customer_email == "dummy") profile[i].customer_email = "";
                                    if (profile[i].customer_phone == "dummy") profile[i].customer_phone = "";
                                    if (profile[i].started_datetime == "0000-00-00 00:00:00") profile[i].started_datetime = "";
                                    if (profile[i].completed_datetime == '0000-00-00 00:00:00') profile[i].completed_datetime = "";
                                    if (profile[i].arrived_datetime == "0000-00-00 00:00:00") profile[i].arrived_datetime = "";
                                    if (profile[i].acknowledged_datetime == "0000-00-00 00:00:00") profile[i].acknowledged_datetime = "";
                                    if (profile[i].job_description == null) profile[i].job_description = "";
                                    if ((profile[i].started_datetime != "") && (profile[i].completed_datetime != "")) {
                                        var started_time = new Date(profile[i].started_datetime);
                                        var completed_datetime = new Date(profile[i].completed_datetime);
                                        var timeDifference = commonFunc.millisecondsToStr(completed_datetime.getTime() - started_time.getTime());
                                        profile[i].total_time_spent_at_task_till_completion = timeDifference;
                                    }
                                    profile[i].total_distance_travelled = commonFunc.metersToUnit(profile[i].total_distance_travelled, result[0].distance_in);
                                    profile[i].tracking_link = config.get('webAppLink') + "/tracking/index.html?jobID=" + profile[i].job_hash;
                                    profile[i].job_pickup_datetime = moment(profile[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a');
                                    profile[i].job_delivery_datetime = moment(profile[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a');

                                    mongo.getOptionalFieldsForTask(profile[i].user_id, profile[i].job_id, profile[i], function (options) {
                                        var go_to_task = {};
                                        commonFunc.authenticateUserIdAndJobToken(options.task.user_id, options.task.pickup_delivery_relationship, function (auth) {
                                            if (options.task.job_type == constants.jobType.PICKUP && auth.length > 1) {
                                                go_to_task.job_type = "Delivery";
                                                go_to_task.job_id = auth[1].job_id;
                                            } else if (options.task.job_type == constants.jobType.DELIVERY && auth.length > 1) {
                                                go_to_task.job_type = "Pickup";
                                                go_to_task.job_id = auth[0].job_id;
                                            }
                                            options.task.link_task = go_to_task;
                                            options.task.fields = options.fields;
                                            counter++;
                                            getTaskHistory(counter, result_task_profile_length, options.task);
                                        })
                                    });
                                }
                            }

                            function getTaskHistory(counter, profile_length, profile) {
                                task_history.push(profile);
                                if (counter == profile_length) {
                                    var new_counter = 0;
                                    for (var i = 0; i < counter; i++) {
                                        commonFunc.getTaskHistory(task_history[i].job_id, task_history[i], function (history) {
                                            history.task.task_history = history.task_history;
                                            new_counter++;
                                            getFleetMovement(new_counter, counter, history.task);
                                        });
                                    }
                                }
                            }

                            function getFleetMovement(counter, profile_length, profile) {
                                fleet_movement.push(profile);
                                if (counter == profile_length) {
                                    var new_counter = 0;
                                    for (var i = 0; i < counter; i++) {
                                        commonFunc.getFleetMovement(1, fleet_movement[i].fleet_id, fleet_movement[i].started_datetime, fleet_movement[i].completed_datetime, fleet_movement[i], function (movement) {
                                            movement.task.fleet_movement = movement.fleet_movement;
                                            movement.task.fleet_history_image = movement.image;
                                            new_counter++;
                                            sendFinalResponse(new_counter, counter, movement.task);
                                        });
                                    }
                                }
                            }

                            function sendFinalResponse(counter, profile_length, profile) {
                                finalResult.push(profile);
                                if (counter == profile_length) {
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": finalResult
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            }
                        }
                    })
                }
            }
        })
    }
}

/*
 * ------------------
 * VIEW TASK DETAILS
 * ------------------
 */
exports.view_task_details = function (req, res) {
    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var user_id = req.body.user_id;
    var manvalues = [access_token, job_id, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.getBothTasksFromSingleID(job_id, function (result) {
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": result
                    };
                    res.send(JSON.stringify(response));
                    return;
                })
            }
        });
    }
}
/*
 * -------------------------------
 * VIEW TASK DETAILS WITH ORDER ID
 * -------------------------------
 */
exports.view_task_from_order_id = function (req, res) {
    var access_token = req.body.access_token,
        order_id = req.body.order_id,
        user_id = req.body.user_id;
    var manvalues = [access_token, order_id, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.authenticateUserIdAndOrderId(result[0].user_id, order_id, function (order) {
                    if (order == 0) {
                        responses.noDataFoundError(res);
                        return;
                    } else {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": order
                        };
                        res.send(JSON.stringify(response));
                        return;
                    }
                })
            }
        })
    }
}
/*
 * --------------------------
 * ASSIGN FLEET TO TASK
 * --------------------------
 */
exports.assign_fleet_to_task = function (req, res) {
    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var fleet_id = req.body.fleet_id;
    var team_id = req.body.team_id;
    if (typeof fleet_id === "undefined" || fleet_id === '') {
        fleet_id = null;
    }
    var manvalues = [access_token, job_id, team_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var user_id = result[0].user_id;
                var dispatcher_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkPerm) {
                        if (chkPerm && chkPerm.length && chkPerm[0].create_task == constants.hasPermissionStatus.NO) {
                            responses.invalidAccessError(res);
                        }
                        else {
                            dispatcher_id = user_id;
                            user_id = result[0].dispatcher_user_id;
                            authorizeUser(access_token, user_id, job_id, fleet_id, team_id);
                        }
                    })
                } else {
                    authorizeUser(access_token, user_id, job_id, fleet_id, team_id);
                }

                function authorizeUser(access_token, user_id, job_id, fleet_id, team_id) {
                    commonFunc.authenticateUserIdAndJobId(user_id, job_id, function (authenticateUserIdAndJobIdResult) { //Checks for task mapping
                        if (authenticateUserIdAndJobIdResult == 0) {
                            var response = {
                                "message": constants.responseMessages.JOB_NOT_MAPPED_WITH_YOU,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                        } else {
                            commonFunc.authenticateFleetIdAndUserId(fleet_id, user_id, function (authenticateFleetIdAndUserIdResult) { //Checks for fleet mapping
                                if (authenticateFleetIdAndUserIdResult == 0) {
                                    responses.invalidAccessError(res);
                                } else {
                                    if (authenticateUserIdAndJobIdResult[0].job_status == constants.jobStatus.ENDED) {
                                        var response = {
                                            "message": constants.responseMessages.JOB_COMPLETED,
                                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                            "data": {}
                                        };
                                        res.send(JSON.stringify(response));
                                    } else {
                                        var fleets = [], fleet_name = [];
                                        if (fleet_id) {
                                            fleets.push(fleet_id);
                                            fleet_name.push(authenticateFleetIdAndUserIdResult[0].username);
                                        } else {
                                            fleets.push(null);
                                            fleet_name.push(null);
                                        }
                                        authenticateUserIdAndJobIdResult[0].team_id = team_id;
                                        authenticateUserIdAndJobIdResult[0].current_fleet_id = authenticateUserIdAndJobIdResult[0].fleet_id;
                                        mongo.getOptionalFieldsForTask(user_id, job_id, authenticateFleetIdAndUserIdResult, function (getOptionalFieldsForTaskResult) {
                                            updateJobDetails({
                                                    fields: getOptionalFieldsForTaskResult.fields.custom_field,
                                                    ref_images: []
                                                }, result[0].layout_type, access_token, authenticateUserIdAndJobIdResult[0].job_pickup_email, 1, constants.jobStatus.UPCOMING, authenticateUserIdAndJobIdResult[0].job_type,
                                                authenticateUserIdAndJobIdResult[0].dispatcher_id, authenticateUserIdAndJobIdResult[0].job_pickup_name, authenticateUserIdAndJobIdResult[0].job_pickup_phone,
                                                authenticateUserIdAndJobIdResult[0].job_pickup_latitude, authenticateUserIdAndJobIdResult[0].job_pickup_longitude, authenticateUserIdAndJobIdResult[0].job_pickup_address,
                                                authenticateUserIdAndJobIdResult[0].job_latitude, authenticateUserIdAndJobIdResult[0].job_longitude, authenticateUserIdAndJobIdResult[0].job_address, authenticateUserIdAndJobIdResult[0].job_description,
                                                authenticateUserIdAndJobIdResult[0].job_pickup_datetime, authenticateUserIdAndJobIdResult[0].job_delivery_datetime, fleets, user_id, authenticateUserIdAndJobIdResult[0].customer_id,
                                                authenticateUserIdAndJobIdResult[0].timezone, authenticateUserIdAndJobIdResult[0].job_time, authenticateUserIdAndJobIdResult[0].pickup_delivery_relationship,
                                                job_id, fleet_name, authenticateUserIdAndJobIdResult[0].customer_username, authenticateUserIdAndJobIdResult[0].customer_email, authenticateUserIdAndJobIdResult[0].customer_phone, authenticateUserIdAndJobIdResult, function (updateJobDetailsResult) {
                                                    if (updateJobDetailsResult == true) {
                                                        responses.actionCompleteResponse(res);
                                                    }
                                                });
                                        });
                                    }
                                }
                            });
                        }
                    });
                }
            }
        });
    }
};


/*
 * --------------------------
 * ASSIGN FLEET TO TASK (TOKEN)
 * --------------------------
 */
exports.reassign_fleet_to_task = function (req, res) {
    var access_token = req.body.access_token;
    var job_token = req.body.job_token;
    var fleet_id = req.body.fleet_id;
    var fields = req.body.fields;
    if (typeof fields === "undefined" || fields === '') {
        fields = [];
    }
    var manvalues = [access_token, job_token, fleet_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var dispatcher_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkPerm) {
                        if (chkPerm && chkPerm.length && chkPerm[0].create_task == constants.hasPermissionStatus.NO) {
                            responses.invalidAccessError(res);
                        }
                        else {
                            dispatcher_id = user_id;
                            user_id = result[0].dispatcher_user_id;
                            authorizeUser(access_token, user_id, job_token, fleet_id);
                        }
                    })
                } else {
                    authorizeUser(access_token, user_id, job_token, fleet_id);
                }

                function authorizeUser(access_token, user_id, job_token, fleet_id) {

                    commonFunc.authenticateUserIdAndJobToken(user_id, job_token, function (authenticateUserIdAndJobIdResult) { //Checks for task mapping
                        if (authenticateUserIdAndJobIdResult == 0) {
                            var response = {
                                "message": constants.responseMessages.JOB_NOT_MAPPED_WITH_YOU,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        } else {
                            commonFunc.authenticateFleetIdAndUserId(fleet_id, user_id, function (authenticateFleetIdAndUserIdResult) { //Checks for fleet mapping
                                if (authenticateFleetIdAndUserIdResult == 0) {
                                    responses.invalidAccessError(res);
                                } else {
                                    var sql = "UPDATE `tb_jobs` SET `fleet_id`=? WHERE `pickup_delivery_relationship`=?";
                                    connection.query(sql, [fleet_id, job_token], function (err, update_jobs) {

                                        var accept_button = 0;
                                        if (fields.length > 0) {
                                            accept_button = fields.app_optional_fields[0].value;
                                        }
                                        if (authenticateUserIdAndJobIdResult[1].fleet_id != fleet_id) {
                                            var message_fleet = 'A new task has been assigned.';
                                            var driverOfflineMessage = 'Hi [Fleet name], a new task has been assigned to you. Kindly log into app to see the details.';
                                        } else {
                                            var message_fleet = 'Task has been rescheduled.';
                                            var driverOfflineMessage = 'Hi [Fleet name], a task has been rescheduled to you. Kindly log into app to see the details.';
                                        }
                                        var payload_fleet = {
                                            flag: constants.notificationFlags.REASSIGN,
                                            message: message_fleet,
                                            job_id: authenticateUserIdAndJobIdResult[0].job_id,
                                            job_type: 0,
                                            cust_name: authenticateUserIdAndJobIdResult[0].job_type == constants.jobType.PICKUP ? authenticateUserIdAndJobIdResult[0].job_pickup_name : authenticateUserIdAndJobIdResult[0].customer_username,
                                            start_address: authenticateUserIdAndJobIdResult[0].job_pickup_address,
                                            start_time: authenticateUserIdAndJobIdResult[0].job_pickup_datetime,
                                            end_address: authenticateUserIdAndJobIdResult[0].job_address,
                                            end_time: authenticateUserIdAndJobIdResult[0].job_delivery_datetime,
                                            accept_button: accept_button,
                                            d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                        };
                                        commonFunc.sendNotification(fleet_id, message_fleet, notificationFlag_driver, payload_fleet, driverOfflineMessage);
                                        message_fleet = "Task has been deleted.";
                                        payload_fleet.message = message_fleet;
                                        payload_fleet.flag = constants.notificationFlags.JOB_DELETED;
                                        payload_fleet.job_id = authenticateUserIdAndJobIdResult[0].job_id;
                                        commonFunc.sendNotification(authenticateUserIdAndJobIdResult[0].fleet_id, message_fleet, false, payload_fleet, driverOfflineMessage);
                                        payload_fleet.job_id = authenticateUserIdAndJobIdResult[1].job_id;
                                        commonFunc.sendNotification(authenticateUserIdAndJobIdResult[0].fleet_id, message_fleet, false, payload_fleet, driverOfflineMessage);

                                    });
                                    responses.actionCompleteResponse(res);
                                    return;
                                }
                            });
                        }
                    });
                }
            }
        });
    }
};


///*
// * ----------------------------
// * CHECK AVAILABILITY OF FLEET
// * ----------------------------
// */
//
//function checkFleetAvailability(fleet_id,job_pickup_datetime,job_delivery_datetime,callback){
//    var check_code = "SELECT `fleet_id` FROM `tb_jobs` WHERE ((`job_pickup_datetime`<=? AND `job_delivery_datetime`>=?) || (`job_pickup_datetime`<=? AND `job_delivery_datetime`>=?)) AND `fleet_id`=? AND `job_status` IN (0,1) LIMIT 1";
//    connection.query(check_code, [job_pickup_datetime,job_pickup_datetime,job_delivery_datetime,job_delivery_datetime,fleet_id], function(err, result){
//       console.log(result);
//        if(err) {
//            logging.logDatabaseQueryError("Checking for fleet availability ", err, result);
//            responses.sendError(res);
//            return;
//        } else {
//            if (result.length > 0) {
//                var response = {
//                    "fleet_id": result[0].fleet_id,
//                    "status": true
//                };
//                callback(response);
//            }
//            else {
//                var response = {
//                    "fleet_id": 0,
//                    "status": false
//                };
//                callback(response);
//            }
//        }
//    });
//}


/*
 * -------------------------------
 * GET TASK OPTIONS
 * -------------------------------
 */

exports.get_task_options = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var task = req.body.task;
    if (typeof task === "undefined" || task === '') {
        task = {};
    }
    var manvalues = [access_token, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);

    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);

            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);

                    } else {
                        commonFunc.authenticateUserIdAndJobId(user_id, job_id, function (authenticateUserIdAndJobIdResult) {
                            if (authenticateUserIdAndJobIdResult == 0) {
                                responses.authenticationErrorFleetAndJobID(res);
                            } else {
                                mongo.getOptionalFieldsForTask(user_id, job_id, task, function (getOptionalFieldsForTaskResult) {
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": getOptionalFieldsForTaskResult.fields
                                    };
                                    res.send(JSON.stringify(response));
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};


/*
 * -----------------------------
 * UPLOAD REFERENCE IMAGE
 * -----------------------------
 */

exports.upload_ref_images = function (req, res) {
    var access_token = req.body.access_token,
        user_id = req.body.user_id;
    var manvalues = [access_token, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationError(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        if (req.files && req.files.ref_image) {
                            if (req.files.ref_image.size < 3072000) {
                                commonFunc.uploadImageToS3Bucket(req.files.ref_image, config.get('s3BucketCredentials.folder.taskImages'), function (refimage) {
                                    if (refimage == 0) {
                                        responses.uploadError(res);
                                    } else {
                                        var ref_image = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.taskImages') + '/' + refimage;
                                        var response = {
                                            "message": constants.responseMessages.ACTION_COMPLETE,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": {
                                                ref_image: ref_image
                                            }
                                        };
                                        res.send(JSON.stringify(response));
                                        return;
                                    }
                                });
                            } else {
                                var response = {
                                    "message": constants.responseMessages.SIZE_EXCEEDS,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {}
                                };
                                res.send(JSON.stringify(response));
                                return;
                            }
                        } else {
                            responses.uploadError(res);
                        }
                    }
                });
            }
        });
    }
};

/*
 GET EXTRAS
 */
exports.get_extras = function (req, res) {
    var access_token = req.body.access_token,
        job_id = req.body.job_id;
    var manvalues = [access_token, job_id];
    async.waterfall([
        function (cb) {
            var checkblank = commonFunc.checkBlank(manvalues);
            if (checkblank) {
                cb(null, {
                    err: 101
                })
            } else {
                cb(null, {
                    err: 0
                })
            }
        },
        function (fleet, cb) {
            if (fleet.err) {
                cb(null, fleet);
            } else {
                commonFunc.authenticateFleetAcessToken(access_token, function (fleet) {
                    if (fleet == 0) {
                        cb(null, {
                            err: 1
                        });
                    } else {
                        cb(null, fleet);
                    }
                });
            }
        },
        function (fleet, cb) {
            if (fleet.err) {
                cb(null, fleet);
            } else {
                commonFunc.checkAccountExpiry(fleet[0].user_id, function (checkExp) {
                    if (checkExp) {
                        cb(null, {
                            err: 2
                        });
                    } else {
                        cb(null, fleet);
                    }
                });
            }
        },
        function (fleet, cb) {
            if (fleet.err) {
                cb(null, fleet);
            } else {
                commonFunc.authenticateUserIdAndJobId(fleet[0].user_id, job_id, function (auth) {
                    if (auth) {
                        cb(null, fleet);
                    } else {
                        cb(null, {
                            err: 3
                        });
                    }
                });
            }
        },
        function (fleet, cb) {
            if (fleet.err) {
                cb(null, fleet);
            } else {
                if (fleet[0].has_invoicing_module) {


                    mongo.getLayoutOptionalFieldsForUser(fleet[0].user_id, fleet[0].layout_type, function (optionalFields) {

                        mongo.getOptionalFieldsForTask(fleet[0].user_id, job_id, {}, function (custom) {

                            if (custom.fields.custom_field && custom.fields.custom_field.length && custom.fields.custom_field[0].template_id) {

                                mongo.processOptionalLayout(optionalFields, custom.fields.custom_field[0].template_id, function (cal) {
                                    var dataValues = {}
                                    custom.fields.custom_field.forEach(function (field) {
                                        dataValues[field.label] = field.fleet_data || field.data || '';
                                    });
                                    if (cal.extras.invoice_html) cal.extras.invoice_html = renderHtml(dataValues, cal.extras.invoice_html)
                                    custom.fields.extras = cal.extras;
                                    fleet[0].extras = cal.extras;
                                })

                            }
                            mongo.insertTaskOptionalFieldsMONGO(fleet[0].user_id, fleet[0].layout_type, job_id, custom.fields);

                            cb(null, fleet);

                        });


                    });
                } else {
                    cb(null, {
                        err: 3
                    });
                }
            }
        }
    ], function (error, fleet) {
        if (error) {
            responses.authenticationError(res);
            return;
        } else {
            if (fleet.err) {
                if (fleet.err == 1) {
                    responses.authenticationErrorResponse(res);
                    return;
                } else if (fleet.err == 2) {
                    responses.accountExpiryErrorResponse(res);
                    return;
                } else if (fleet.err == 3) {
                    responses.invalidAccessError(res);
                    return;
                } else if (fleet.err == 101) {
                    responses.parameterMissingResponse(res);
                    return;
                } else {
                    responses.authenticationError(res);
                    return;
                }
            } else {
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": fleet[0].extras
                };
                res.send(JSON.stringify(response));
            }
        }
    })
}