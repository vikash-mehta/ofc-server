var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var mongo = require('./mongo');

/*
 * --------------------------
 * ACCOUNT SETTINGS
 * --------------------------
 */
exports.account_settings = function (req, res) {

    var access_token = req.body.access_token,
        name = req.body.name,
        first_name = req.body.first_name,
        last_name = req.body.last_name,
        phone = req.body.phone,
        org_name = req.body.org_name,
        org_address = req.body.org_address,
        country = req.body.country,
        is_company_image_view = req.body.is_company_image_view,
        is_driver_image_view = req.body.is_driver_image_view,
        is_image = req.body.is_image,
        call_fleet_as = req.body.call_fleet_as,
        country_phone_code = req.body.country_phone_code,
        email = req.body.email;
    var manvalues = [access_token, is_image];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                if (call_fleet_as) {
                    call_fleet_as = commonFunc.capitalizeFirstLetter(call_fleet_as);
                }
                if (result[0].email == email) {
                    change_settings(country_phone_code, call_fleet_as, org_name, org_address, country, is_company_image_view, is_driver_image_view, email, user_id, name,
                        first_name, last_name, phone, is_image);
                } else {
                    commonFunc.authenticateUserEmail(email, function (authenticateUserEmailResult) {
                        if (authenticateUserEmailResult != 0) {
                            responses.authenticationAlreadyExists(res);
                            return;
                        } else {
                            change_settings(country_phone_code, call_fleet_as, org_name, org_address, country, is_company_image_view, is_driver_image_view, email, user_id, name,
                                first_name, last_name, phone, is_image);
                        }
                    });
                }
                function change_settings(country_phone_code, call_fleet_as, org_name, org_address, country, is_company_image_view, is_driver_image_view, email, user_id, name, first_name, last_name, phone, is_image) {
                    if (is_image == 1 && req.files && req.files.company_image) {
                        commonFunc.uploadImageToS3Bucket(req.files.company_image, config.get('s3BucketCredentials.folder.userCompanyImages'), function (company_image_image) {
                            if (company_image_image == 0) {
                                responses.uploadError(res);
                                return;
                            } else {
                                var companyImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.userCompanyImages') + '/' + company_image_image;
                                var sql = "UPDATE `tb_users` SET `first_name`=?,`last_name`=?,`email`=?,`country_phone_code`=?,`call_fleet_as`=?, `username` = ?, `phone` = ?, `company_name` = ?, `company_address` = ?, " +
                                    "`country` = ?, `is_company_image_view` = ?, `is_driver_image_view` = ?, `company_image` = ? " +
                                    " WHERE `user_id` = ? LIMIT 1";
                                connection.query(sql, [first_name, last_name, email, country_phone_code, call_fleet_as, name, phone, org_name, org_address, country, is_company_image_view, is_driver_image_view, companyImage, user_id], function (err, result_update) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in updating account information : ", err, result_update);
                                        responses.sendError(res);
                                        return;
                                    } else {
                                        var response = {
                                            "message": constants.responseMessages.ACTION_COMPLETE,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": {
                                                "company_image": companyImage
                                            }
                                        };
                                        res.send(JSON.stringify(response));
                                        return;
                                    }
                                });
                            }
                        });
                    }
                    else {
                        var sql = " UPDATE `tb_users` SET `first_name`=?,`last_name`=?, `email`=?, `country_phone_code`=?,`call_fleet_as`=?, `username` = ?, `phone` = ?, " +
                            "`company_name` = ?, `company_address` = ?, `country` = ?, `is_company_image_view` = ?, `is_driver_image_view` = ? " +
                            " WHERE `user_id` = ? LIMIT 1";
                        connection.query(sql, [first_name, last_name, email, country_phone_code, call_fleet_as, name, phone, org_name, org_address, country, is_company_image_view, is_driver_image_view, user_id], function (err, result_update) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating account information : ", err, result_update);
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                                return;
                            }
                        });
                    }
                }
            }
        });
    }
}

/*
 * --------------------------
 * Company Geo Settings
 * --------------------------
 */
exports.company_geo_settings = function (req, res) {

    var access_token = req.body.access_token;
    var company_latitude = req.body.company_latitude
    var company_longitude = req.body.company_longitude
    var company_address = req.body.company_address

    var manvalues = [access_token, company_address, company_latitude, company_address];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var sql = "UPDATE `tb_users` SET `company_address`=?,`company_latitude`= ?,`company_longitude`=? WHERE `user_id`=? LIMIT 1";
                connection.query(sql, [company_address, company_latitude, company_longitude, user_id], function (err, result) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in updating company information : ", err, result);
                        responses.sendError(res);
                        return;
                    } else {
                        responses.actionCompleteResponse(res);
                        return;
                    }
                });
            }
        });
    }
}

/*
 * -----------------------
 * CHANGING WORKFLOW
 * -----------------------
 */

exports.update_workflow = function (req, res) {

    var access_token = req.body.access_token;
    var layout_type = req.body.layout_type;
    var fields = req.body.fields;
    var auto_assign = req.body.auto_assign;
    var manvalues = [access_token, layout_type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            responses.invalidAccessError(res);
                        } else {
                            mongo.insertORUpdateWorkflowMONGO(user_id, layout_type, fields, auto_assign);
                            module.exports.updateWorkflow(layout_type, user_id, function (updateWorkflowResult) {
                                res.send(JSON.stringify(updateWorkflowResult));
                            });
                        }
                    }
                });
            }
        });
    }
};

exports.updateWorkflow = function (layout_type, user_id, callback) {
    var sql = "UPDATE `tb_users` SET `layout_type` = ? " +
        "WHERE (`user_id`=? || `dispatcher_user_id`=?)";
    connection.query(sql, [layout_type, user_id, user_id], function (err, update_workflow) {
        if (err) {
            logging.logDatabaseQueryError("Error in updating workflow : ", err, update_workflow);
            var response = {
                "message": constants.responseMessages.ERROR_IN_EXECUTION,
                "status": constants.responseFlags.ERROR_IN_EXECUTION,
                "data": {}
            };
            callback(response);
        } else {
            var response = {
                "message": constants.responseMessages.ACTION_COMPLETE,
                "status": constants.responseFlags.ACTION_COMPLETE,
                "data": {}
            };
            callback(response);
        }
    });
}

/*
 * -----------------------
 * UPDATE ACTION
 * -----------------------
 */

exports.update_action = function (req, res) {

    var access_token = req.body.access_token;
    var action_type = req.body.action_type;
    var value = req.body.value;
    var manvalues = [access_token, action_type, value];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "UPDATE `tb_users` SET `" + action_type + "`=? WHERE `user_id` = ? LIMIT 1";
                        connection.query(sql, [value, user_id], function (err, result_update) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating action_type information : ", err, result_update);
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                                return;
                            }
                        });
                    }
                });
            }
        });
    }
};


exports.get_user_layout_fields = function (req, res) {
    var access_token = req.body.access_token;
    var layout_type = req.body.layout_type;
    var manvalues = [access_token, layout_type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    result[0].user_id = result[0].dispatcher_user_id;
                }
                mongo.getLayoutOptionalFieldsForUser(result[0].user_id, layout_type, function (getLayoutOptionalFieldsForUserResult) {
                    var response = {
                        "message": constants.responseMessages.ACTION_COMPLETE,
                        "status": constants.responseFlags.ACTION_COMPLETE,
                        "data": getLayoutOptionalFieldsForUserResult
                    };
                    res.send(JSON.stringify(response));
                })
            }
        });
    }
}