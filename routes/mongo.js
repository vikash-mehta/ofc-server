var commonFunc = require('./commonfunction');
var responses = require('./responses');
var moment = require('moment');
var async = require('async');
/*
 * ----------------------------------
 * INSERT OR UPDATE WORKFLOW OF USERS
 * ----------------------------------
 */

exports.insertORUpdateWorkflowMONGO = function (user_id, layout_type, fields, auto_assign) {

    user_id = parseInt(user_id);
    layout_type = parseInt(layout_type);

    if (typeof fields.custom_field === 'undefined') {
        fields.custom_field = [];
    }
    db.collection(config.get('mongoCollections.appOptionalFields')).find({
        user_id: user_id,
        "layout_type": layout_type
    }).toArray(function (err, checkValueIFexists) {
        if (checkValueIFexists.length > 0) {
            db.collection(config.get('mongoCollections.appOptionalFields')).update({
                "user_id": user_id,
                "layout_type": layout_type
            }, {$set: {"fields": fields, "auto_assign": auto_assign}});
        } else {
            db.collection(config.get('mongoCollections.appOptionalFields')).insert({
                "user_id": user_id,
                "layout_type": layout_type,
                "fields": fields,
                "auto_assign": auto_assign
            });
        }
    });
}
/*
 * ----------------------------------
 * INSERT TASK OPTIONAL FIELDS
 * ----------------------------------
 */
exports.insertTaskOptionalFieldsMONGO = function (user_id, layout_type, job_id, fields) {
    user_id = parseInt(user_id);
    layout_type = parseInt(layout_type);
    job_id = parseInt(job_id);
    db.collection(config.get('mongoCollections.taskOptionalFields')).find({
        user_id: user_id,
        job_id: job_id
    }).toArray(function (err, result) {
        if (result.length > 0) {
            db.collection(config.get('mongoCollections.taskOptionalFields')).update({
                "user_id": user_id,
                "job_id": job_id
            }, {$set: {"fields": fields}});
        }
        else {
            db.collection(config.get('mongoCollections.taskOptionalFields')).insert({
                "user_id": user_id,
                "layout_type": layout_type,
                "job_id": job_id,
                "fields": fields
            }, function (err, inserted) {
                //console.log(err, inserted)
            });
        }
    })
}
/*
 * ----------------------------------
 * FETCH OPTIONAL FIELDS FOR TASK
 * ----------------------------------
 */
exports.getOptionalFieldsForTask = function (user_id, job_id, task, callback) {

    user_id = parseInt(user_id);
    job_id = parseInt(job_id);
    db.collection(config.get('mongoCollections.taskOptionalFields')).find({
        user_id: user_id,
        job_id: job_id
    }).toArray(function (err, result) {
        if (result.length > 0) {
            var response = {
                "fields": result[0].fields,
                "task": task
            }
            return callback(response);
        } else {
            var response = {
                "fields": {
                    app_optional_fields: constants.defaultOptionalValue.DEFAULT,
                    custom_field: [],
                    extras : {}
                },
                "task": task
            }
            return callback(response);
        }
    });
}
/*
 * ------------------------------------------------
 * FETCH OPTIONAL FIELDS FOR USERS WITH LAYOUT TYPE
 * ------------------------------------------------
 */
exports.getLayoutOptionalFieldsForUser = function (user_id, layout_type, callback) {

    user_id = parseInt(user_id);
    layout_type = parseInt(layout_type);
    db.collection(config.get('mongoCollections.appOptionalFields')).find({
        user_id: user_id,
        layout_type: layout_type
    }).toArray(function (err, result) {
        if (result.length > 0) {
            return callback(result);
        } else {
            return callback([]);
        }
    });
}

/*
 * ------------------------------
 * UPDATE CUSTOM FIELDS OF TASK
 * ------------------------------
 */

exports.updateTaskCustomField = function (user_id, job_id, custom_field_label, data, status, callback) {

    user_id = parseInt(user_id);
    job_id = parseInt(job_id);
    db.collection(config.get('mongoCollections.taskOptionalFields')).find({
        user_id: user_id,
        job_id: job_id
    }).toArray(function (err, result) {
        if (result.length > 0) {
            var fields = result[0].fields, i;
            if (status == "insert") {
                fields.custom_field.forEach(function (customData, index) {
                    if (customData.label == custom_field_label) {
                        i = index;
                    }
                });
                if (typeof fields.custom_field[i].fleet_data === 'undefined' || fields.custom_field[i].fleet_data === '') {
                    fields.custom_field[i].fleet_data = [];
                } else {
                    try {
                        fields.custom_field[i].fleet_data = JSON.parse(fields.custom_field[i].fleet_data);
                    } catch (e) {
                        console.log('fields.custom_field[i].fleet_data error occured due to ', e);
                        fields.custom_field[i].fleet_data = []
                    }
                }
                if (data && typeof fields.custom_field[i].fleet_data === 'object' && fields.custom_field[i].fleet_data.length>=0) {
                    fields.custom_field[i].fleet_data.push(data);
                    fields.custom_field[i].fleet_data = JSON.stringify(fields.custom_field[i].fleet_data);
                }
                db.collection(config.get('mongoCollections.taskOptionalFields')).update({
                    user_id: user_id,
                    job_id: job_id
                }, {$set: {"fields": fields}}, function (err) {
                    if (data) fields.custom_field[i].fleet_data = data
                    var response = {
                        status: true,
                        data: fields.custom_field[i]
                    }
                    callback(response);
                });
            } else if (status == "update") {
                fields.custom_field.forEach(function (customData, index) {
                    if (customData.label == custom_field_label) {
                        customData.fleet_data = data;
                        i = index;
                    }
                });
                db.collection(config.get('mongoCollections.taskOptionalFields')).update({
                    user_id: user_id,
                    job_id: job_id
                }, {$set: {"fields": fields}}, function (err) {
                    fields.custom_field[i].fleet_data = data
                    var response = {
                        status: true,
                        data: fields.custom_field[i]
                    }
                    callback(response);
                });
            }
        }
        else {
            callback(false);
        }
    })
}

/*
 * ----------------------------------
 * INSERT DEFAULT WORKFLOW
 * ----------------------------------
 */

exports.insertDefaultWorkflow = function (user_id, layout_type) {
    user_id = parseInt(user_id);
    layout_type = parseInt(layout_type);
    var fields = {
        app_optional_fields: constants.defaultOptionalValue.DEFAULT,
        custom_field: []
    }
    var auto_assign = {};
    db.collection(config.get('mongoCollections.appOptionalFields')).find({
        user_id: user_id,
        "layout_type": layout_type
    }).toArray(function (err, checkValueIFexists) {
        if (checkValueIFexists.length > 0) {
            db.collection(config.get('mongoCollections.appOptionalFields')).update({
                "user_id": user_id,
                "layout_type": layout_type
            }, {$set: {"fields": fields, "auto_assign": auto_assign}});
        } else {
            db.collection(config.get('mongoCollections.appOptionalFields')).insert({
                "user_id": user_id,
                "layout_type": layout_type,
                "fields": fields,
                "auto_assign": auto_assign
            });
        }
    });
}

/*----------------------------------
 SET DEFAULT CUSTOM TEMPLATES
 *----------------------------------
 */

exports.setDefaultCustomTemplate = function (req, res) {
    var user_id = parseInt(req.body.user_id);
    var layout_type = parseInt(req.body.layout_type);
    db.collection(config.get('mongoCollections.appOptionalFields')).find().toArray(function (err, checkValueIFexists) {
        if (checkValueIFexists.length > 0) {
            checkValueIFexists.forEach(function (val) {
                if (val.fields.custom_field && val.fields.custom_field.length > 0) {
                    var new_cf = [
                        {
                            'Template 1': {
                                template: 'template 1',
                                template_id: 'template 1',
                                items: val.fields.custom_field
                            }
                        }
                    ]
                    val.fields.custom_field = new_cf;
                    db.collection(config.get('mongoCollections.appOptionalFields')).update({
                        "user_id": val.user_id,
                        "layout_type": val.layout_type
                    }, {$set: {"fields": val.fields}});
                }
            });
        }
        responses.actionCompleteResponse(res);
    });
}

/*----------------------------------
 SET DEFAULT CHANGES
 *----------------------------------
 */
exports.setDefaultAppOptionalFields = function (req, res) {
    db.collection(config.get('mongoCollections.appOptionalFields')).find(

    ).toArray(function (err, checkValueIFexists) {
            if (checkValueIFexists.length > 0) {
                checkValueIFexists.forEach(function (val) {
                    if (val.fields.app_optional_fields && val.fields.app_optional_fields.length > 0) {
                        var arrived = {
                            "label": "arrived",
                            "value": 1
                        }
                        var slider = {
                            "label": "slider",
                            "value": 0
                        }
                        var confirm = {
                            "label": "confirm",
                            "value": 1
                        }
                        val.fields.app_optional_fields.forEach(function (ap) {
                            if (ap.label == "true" || ap.label == true || ap.label == "1") {
                                ap.label = 1;
                            } else if (ap.label == "false" || ap.label == false || ap.label == "0") {
                                ap.label = 0;
                            }
                            if (ap.value == "true" || ap.value == true || ap.value == "1") {
                                ap.value = 1;
                            } else if (ap.value == "false" || ap.value == false || ap.value == "0") {
                                ap.value = 0;
                            }
                            if (ap.required == "true" || ap.required == true || ap.required == "1") {
                                ap.required = 1;
                            } else if (ap.required == "false" || ap.required == false || ap.required == "0") {
                                ap.required = 0;
                            }
                            if (ap.label == "arrived") {
                                arrived = ap
                            }
                            if (ap.label == "slider") {
                                arrived = ap
                            }
                            if (ap.label == "confirm") {
                                arrived = ap
                            }
                        })
                        val.fields.app_optional_fields.push(arrived);
                        val.fields.app_optional_fields.push(slider);
                        val.fields.app_optional_fields.push(confirm);
                    }
                    if (val.fields.custom_field && val.fields.custom_field.length > 0) {
                        val.fields.custom_field.forEach(function (field) {
                            Object.keys(field).forEach(function (key) {
                                var obj = field[key];
                                delete field[key];
                                field[key.toLowerCase()] = obj;
                                field[key.toLowerCase()]['template_id'] = key.toLowerCase();
                                field[key.toLowerCase()]['template'] = key.toLowerCase();
                                field[key.toLowerCase()].items.forEach(function (item) {
                                    if (item.value == "true" || item.value == true || item.value == "1") {
                                        item.value = 1;
                                    } else if (item.value == "false" || item.value == false || item.value == "0") {
                                        item.value = 0;
                                    }
                                    if (item.required == "true" || item.required == true || item.required == "1") {
                                        item.required = 1;
                                    } else if (item.required == "false" || item.required == false || item.required == "0") {
                                        item.required = 0;
                                    }
                                    item.template_id = key.toLowerCase();
                                })
                            });
                        })
                    }
                    db.collection(config.get('mongoCollections.appOptionalFields')).update({
                        "user_id": val.user_id,
                        "layout_type": val.layout_type
                    }, {$set: {"fields": val.fields}});
                });
            }
            responses.actionCompleteResponse(res);
        });
}

/*----------------------------------
 SET DEFAULT CHANGES
 *----------------------------------
 */

exports.setDefaultTaskOptionalFields = function (req, res) {
    db.collection(config.get('mongoCollections.taskOptionalFields')).find(

    ).toArray(function (err, checkValueIFexists) {
            if (checkValueIFexists.length > 0) {
                checkValueIFexists.forEach(function (val) {
                    if (val.fields.app_optional_fields && val.fields.app_optional_fields.length > 0) {
                        var arrived = {
                            "label": "arrived",
                            "value": 1
                        }
                        var slider = {
                            "label": "slider",
                            "value": 0
                        }
                        var confirm = {
                            "label": "confirm",
                            "value": 1
                        }
                        val.fields.app_optional_fields.forEach(function (ap) {
                            if (ap.label == "true" || ap.label == true || ap.label == "1") {
                                ap.label = 1;
                            } else if (ap.label == "false" || ap.label == false || ap.label == "0") {
                                ap.label = 0;
                            }
                            if (ap.value == "true" || ap.value == true || ap.value == "1") {
                                ap.value = 1;
                            } else if (ap.value == "false" || ap.value == false || ap.value == "0") {
                                ap.value = 0;
                            }
                            if (ap.required == "true" || ap.required == true || ap.required == "1") {
                                ap.required = 1;
                            } else if (ap.required == "false" || ap.required == false || ap.required == "0") {
                                ap.required = 0;
                            }
                            if (ap.label == "arrived") {
                                arrived = ap
                            }
                            if (ap.label == "slider") {
                                arrived = ap
                            }
                            if (ap.label == "confirm") {
                                arrived = ap
                            }
                        })
                        val.fields.app_optional_fields.push(arrived);
                        val.fields.app_optional_fields.push(slider);
                        val.fields.app_optional_fields.push(confirm);
                    }
                    if (val.fields.custom_field && val.fields.custom_field.length > 0) {
                        val.fields.custom_field.forEach(function (cu) {
                            if (cu.value == "true" || cu.value == true || cu.value == "1") {
                                cu.value = 1;
                            } else if (cu.value == "false" || cu.value == false || cu.value == "0") {
                                cu.value = 0;
                            }
                            if (cu.required == "true" || cu.required == true || cu.required == "1") {
                                cu.required = 1;
                            } else if (cu.required == "false" || cu.required == false || cu.required == "0") {
                                cu.required = 0;
                            }
                        })
                    }
                    db.collection(config.get('mongoCollections.taskOptionalFields')).update({
                        "job_id": val.job_id
                    }, {$set: {"fields": val.fields}});
                });
            }
            responses.actionCompleteResponse(res);
        });
}

/*---------------------------------------
 SELECT CUSTOM TEMPLATES FROM TEMPLATE ID
 *---------------------------------------
 */

exports.getTemplateFromTemplateID = function (req, res) {
    var user_id = parseInt(req.body.user_id);
    var layout_type = parseInt(req.body.layout_type);
    var template_id = parseInt(req.body.template_id);
    module.exports.getTemplateItems(user_id, layout_type, template_id, function (getTemplateResult) {
        var response = {
            "message": constants.responseMessages.ACTION_COMPLETE,
            "status": constants.responseFlags.ACTION_COMPLETE,
            "data": getTemplateResult
        };
        res.send(JSON.stringify(response));
    })
}

exports.getTemplateItems = function (user_id, layout_type, template_id, callback) {
    user_id = parseInt(user_id);
    layout_type = parseInt(layout_type);
    db.collection(config.get('mongoCollections.appOptionalFields')).find({
        user_id: user_id,
        layout_type: layout_type
    }).toArray(function (err, chkExists) {
        module.exports.processOptionalLayout(chkExists, template_id, function(c){
            callback(c);
        })
    });
}

exports.processOptionalLayout = function(data, template_id, callback){
    var resp = {
        items : [],
        extras : {}
    }
    if (data.length > 0) {
        if (data[0].fields.custom_field && data[0].fields.custom_field.length > 0) {
            data[0].fields.custom_field.forEach(function (field) {
                Object.keys(field).forEach(function (key) {
                    if (template_id.toLowerCase() == key.toLowerCase()) {
                        resp.items = field[key].items || [];
                        resp.extras = field[key].extras || {};
                    }
                });
            });
            callback(resp);
        } else {
            callback(resp);
        }
    } else {
        callback(resp);
    }
}

//insert or update Routed Tasks

exports.insertOrUpdateRoutedTasks = function (data, callback) {
    data.routed = _lodash.map(data.routed , function(val, key){ return { fleet_id : key, tasks: val.tasks, polylines: val.polylines}});
    async.map(data.routed, function (da, cb) {
        da.fleet_id = da.fleet_id.toString();
        data.layout_type = data.layout_type.toString();
        db.collection(config.get('mongoCollections.routedTasks')).find({
            fleet_id: da.fleet_id,
            layout_type: data.layout_type,
            date: data.date
        }).toArray(function (err, values) {
            if (err) {
                cb(null, false);
            }
            if (values.length > 0) {
                values.forEach(function (val) {
                    if (!data.manual) {
                        da.tasks = da.tasks.concat(val.routed);
                    }
                    if (!data.polylines) {
                        da.polylines = []
                    }
                    da.tasks = _lodash.uniq(da.tasks);
                    if (data.routed_comp_tasks) {
                        data.routed_comp_tasks.split(',').forEach(function (jb) {
                            var index = _lodash.findIndex(da.tasks, function (val) {
                                return val == jb;
                            })
                            if (index >= 0) {
                                da.tasks.splice(index, 1);
                            }
                        })
                    }
                })
                da.polylines = da.polylines.filter(function (e) {
                    return (e === undefined || e === null || e === '') ? false : ~e;
                });
                da.tasks = da.tasks.filter(function (e) {
                    return (e === undefined || e === null || e === '') ? false : ~e;
                });
                db.collection(config.get('mongoCollections.routedTasks')).update({
                    fleet_id: da.fleet_id,
                    layout_type: data.layout_type,
                    date: data.date
                }, {$set: {routed: da.tasks, polylines: da.polylines}});
                cb(null, true);
            }
            else {
                db.collection(config.get('mongoCollections.routedTasks')).insert({
                    fleet_id: da.fleet_id,
                    layout_type: data.layout_type,
                    date: data.date,
                    routed: da.tasks,
                    polylines: da.polylines
                });
                cb(null, true);
            }
        });
    }, function (err, results) {
        results = _lodash.uniq(results);
        if (results.length == 1 && results) {
            callback(true);
        } else {
            callback(false);
        }
    });
}

// GetRoutedTasksOfFleet

exports.getRoutedTasksOfFleet = function (data, callback) {
    data.layout_type = data.layout_type.toString();
    async.map(data.fleet_id, function (da, cb) {
        da = da.toString();
        db.collection(config.get('mongoCollections.routedTasks')).find({
            fleet_id: da,
            layout_type: data.layout_type,
            date: data.date
        }).toArray(function (err, values) {
            var tasks = {}
            if (err) {
                callback(null, tasks);
            } else {
                if (values.length > 0) {
                    tasks[da] = {
                        routed: values[0].routed,
                        polylines: values[0].polylines
                    };
                }
            }
            cb(null, tasks);
        });
    }, function (err, results) {
        var fl_tasks = {};
        results.forEach(function (task) {
            var k;
            for (k in task) {
                if (task.hasOwnProperty(k)) {
                    fl_tasks[k] = task[k]
                }
            }
        })
        callback(fl_tasks);
    });
}

// deleteFromRoutedTasks

exports.deleteFromRoutedTasks = function (fleet_id, job_id) {
    commonFunc.getJobDetailsFromJobID(job_id, function (data) {
        if (data) data = data[0];
        db.collection(config.get('mongoCollections.routedTasks')).find({
            fleet_id: fleet_id.toString() || data.fleet_id.toString(),
            date: moment(data.job_time).format('YYYY-MM-DD')
        }).toArray(function (err, values) {
            if (err) {
            } else {
                if (values.length > 0) {
                    var index = 0;
                    recall(index);
                    function changeIndex() {
                        index++;
                        return recall(index);
                    }

                    function recall(item) {
                        if (item > values.length - 1) {

                        } else {
                            commonFunc.delay(item, function (i) {
                                (function (i) {
                                    var index = _lodash.findLastIndex(values[i].routed, function (chr) {
                                        return chr == data.job_id.toString();
                                    });
                                    values[i].routed.splice(index, 1);
                                    db.collection(config.get('mongoCollections.routedTasks')).update({
                                            fleet_id: values[i].fleet_id,
                                            date: values[i].date,
                                            layout_type: values[i].layout_type
                                        }, {$set: {routed: values[i].routed}},
                                        function () {
                                            changeIndex();
                                        });
                                })(i);
                            });
                        }
                    }
                }
            }
        });
    });
}