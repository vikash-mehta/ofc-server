/**
 * Created by harsh on 12/3/14.
 */

var logging = require('./logging');


exports.parameterMissingResponse = function (res) {
    var response = {
        "message": constants.responseMessages.PARAMETER_MISSING,
        "status": constants.responseFlags.PARAMETER_MISSING,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.actionCompleteResponse = function (res) {
    var response = {
        "message": constants.responseMessages.ACTION_COMPLETE,
        "status": constants.responseFlags.ACTION_COMPLETE,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.authenticationErrorResponse = function (res){
    var response = {
        "message": constants.responseMessages.INVALID_ACCESS_TOKEN,
        "status": constants.responseFlags.INVALID_ACCESS_TOKEN,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.invalidUrlErrorResponse = function (res){
    var response = {
        "message": constants.responseMessages.INVALID_URL,
        "status": constants.responseFlags.INVALID_ACCESS_TOKEN,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.accountExpiryErrorResponse = function (res){
    var response = {
        "message": constants.responseMessages.ACCOUNT_EXPIRE,
        "status": constants.responseFlags.ACCOUNT_EXPIRE,
        "data":{
            billing_popup : {
                "skip_link": 0,
                "show_billing_popup": 1,
                "days_left": 0
            }
        }
    }
    res.send(JSON.stringify(response));
};

exports.authenticationErrorFleetAndJobID = function (res){
    var response = {
        "message": constants.responseMessages.JOB_NOT_MAPPED_WITH_YOU,
        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.checkFleetAvailabilityResponse = function (res){
    var response = {
        "message": constants.responseMessages.FLEET_NOT_AVAILABLE,
        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.authenticateEmailNotExists = function (res){
    var response = {
        "message": constants.responseMessages.EMAIL_NOT_EXISTS,
        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.authenticationFleetResponse = function (res){
    var response = {
        "message": constants.responseMessages.FLEET_EMAIL_NOT_INVITED_BY_ANYONE,
        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.authenticationAlreadyExists = function (res){
    var response = {
        "message": constants.responseMessages.EMAIL_REGISTERED_ALREADY,
        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.authenticationTeamAlreadyExists = function (res){
    var response = {
        "message": constants.responseMessages.TEAM_NAME_ALREADY_REGISTERED_WITH_YOU,
        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.sendError = function (res) {
    var response = {
        "message": constants.responseMessages.ERROR_IN_EXECUTION,
        "status": constants.responseFlags.ERROR_IN_EXECUTION,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.authenticationError = function (res) {
    var response = {
        "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
        "data" : {}
    };
    res.send(JSON.stringify(response));
};

exports.uploadError = function (res) {
    var response = {
        "message": constants.responseMessages.UPLOAD_ERROR,
        "status": constants.responseFlags.UPLOAD_ERROR,
        "data": {}
    };
    res.send(JSON.stringify(response));
};

exports.noDataFoundError = function (res) {
    var response = {
        "message": constants.responseMessages.NO_DATA_FOUND,
        "status": constants.responseFlags.USER_NOT_FOUND,
        "data": {}
    };
    res.send(JSON.stringify(response));
};

exports.invalidAccessError = function (res) {
    var response = {
        "message": constants.responseMessages.INVALID_ACCESS,
        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
        "data": {}
    };
    res.send(JSON.stringify(response));
}
