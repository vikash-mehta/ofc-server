var commonFunc = require('./commonfunction');
var responses = require('./responses');
var jobs = require('./jobs');
var moment = require('moment');
var request = require('request');
var xml2json = require('xml2json');
/*
 * ------------------------
 * INTEGRATION WITH SHOPIFY
 * ------------------------
 */
exports.shopify = function (req, res) {
    commonFunc.authenticateUserAccessToken(req.query.access_token, function (result_user) {
        if (result_user == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            var address,customer='',order_id='';
            if (req.body && req.body.customer && req.body.customer.email){
                customer = req.body.customer.email
            }
            if (req.body && req.body.name){
                order_id = req.body.name
            }

            if (req.body && req.body.shipping_address) {
                address = req.body.shipping_address;
            } else {
                address = req.body.billing_address;
            }
            var description = "Order ID: " + order_id + "\n\nItems: \n";
            if (req.body && req.body.line_items) {
            req.body.line_items.forEach(function (item) {
                description += item.quantity + "x " + item.name + (item.vendor == null ? "" : " - from store: " + item.vendor) + "\n";
            })
            }
            if (req.body && req.body.total_line_items_price) {
                description += "\nTotal Price: " + req.body.total_line_items_price;
            }
            if (req.body && req.body.note) {
                description += "\n\nNotes: " + req.body.note;
            }

            var req1 = {};
            req1.body = {
                "customer_email": customer,
                "customer_username": address.name,
                "customer_phone": commonFunc.formatPhoneNumber(address.phone, result_user[0].country_phone_code.toUpperCase()),
                "customer_address": address.address1 + "," + (address.address2 == null ? "" : address.address2 + ",") + address.city +
                (address.province_code == null ? "" : ", " + address.province_code) + (address.zip == null ? "" : ", " + address.zip) +
                (address.country == null ? "" : ", " + address.country),
                "job_description": description,
                "job_delivery_datetime": moment(commonFunc.convertTimeIntoLocal(commonFunc.addMinutes(new Date(), 45), result_user[0].timezone)).format('YYYY-MM-DD HH:mm'),
                "latitude": address.latitude,
                "longitude": address.longitude,

                "timezone": result_user[0].timezone,
                "order_id": order_id,
                "has_pickup": "0",
                "has_delivery": "1",
                "layout_type": "0",
                "tracking_link": 1,
                "access_token": result_user[0].access_token
            }
            var resp = {
                send: function (obj) {
                    res.send("DONE");
                }
            }
            jobs.create_task(req1, resp);
        }
    });
};


/*
 * ------------------------
 * INTEGRATION WITH ZAPIER
 * ------------------------
 */

exports.zapier = function (req, res) {
    console.log(req.query, req.body)
    commonFunc.authenticateUserAccessToken(req.query.access_token, function (result_user) {
        if (result_user == 0) {
            res.statusCode = 401;
            res.end('<html><body>INVALID CREDENTIALS</body></html>');
        } else {
            var response = {
                "message": constants.responseMessages.ACTION_COMPLETE,
                "status": constants.responseFlags.ACTION_COMPLETE,
                "data": result_user
            };
            res.send(JSON.stringify(response));
        }
    });
};

/*
 * -----------------------------
 * INTEGRATION WITH UNICOMMERCE
 * -----------------------------
 */
exports.unicommerce = function (req, res) {
    //var date = new Date(),newDate = new Date();
    var time = 15;
    if (req.query && req.query.time && req.query.access_token) {
        //newDate.setTime(newDate.getTime() - (parseInt(req.query.time) * 60000)); // subtract minutes
        time = req.query.time;
    }
    //date = moment(date).format('YYYY-MM-DD HH:mm:s');
    //newDate = moment(newDate).format('YYYY-MM-DD HH:mm:s');
    //date = date.split(' ');
    //date = date[0] + 'T' + date[1] + 'Z';
    //newDate = newDate.split(' ');
    //newDate = newDate[0] + 'T' + newDate[1] + 'Z';

    var req_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
        'xmlns:ser="http://uniware.unicommerce.com/services/"> <soapenv:Header> <wsse:Security soapenv:mustUnderstand="1" ' +
        'xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" ' +
        'xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"> ' +
        '<wsse:UsernameToken wsu:Id="UsernameToken-B9CEE788A6A77174A314370186594982"> ' +
        '<wsse:Username>tookan</wsse:Username> ' +
        '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">a3ccaf36-b463-4bf4-807a-b87ca27d41e6</wsse:Password> ' +
        '<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">mFtSgEzDGBSTwyOfP1duvw==</wsse:Nonce> ' +
        '</wsse:UsernameToken> </wsse:Security> ' +
        '</soapenv:Header> ' +
        '<soapenv:Body> ' +
        '<ser:SearchSaleOrderRequest>' +
        '<ser:SearchOptions>' +
        '<ser:DisplayStart>0</ser:DisplayStart>' +
        '</ser:SearchOptions>' +
        '<ser:UpdatedSinceInMinutes>' + time + '</ser:UpdatedSinceInMinutes>' +
        '</ser:SearchSaleOrderRequest>' +
        '</soapenv:Body> ' +
        '</soapenv:Envelope>';
    request.post({
        headers: {'content-type': 'text/xml'},
        url: 'https://bazaarcart.unicommerce.com/services/soap/uniware16.wsdl',
        body: req_data
    }, function (error, response, body) {
        var index = 0, data = xml2json.toJson(body.toString());
        data = JSON.parse(data);
        if (data['SOAP-ENV:Envelope']['SOAP-ENV:Body']['SOAP-ENV:Fault']) {
            responses.noDataFoundError(res);
        } else {
            var order_count = data['SOAP-ENV:Envelope']['SOAP-ENV:Body']['SearchSaleOrderResponse']['TotalRecords'];
            if (order_count == 1) {
                recall(index);
                function changeIndex() {
                    index++;
                    return recall(index);
                }

                function recall(item) {
                    if (item > order_count - 1) {
                        responses.actionCompleteResponse(res);
                        return;
                    } else {
                        commonFunc.delay(item, function (i) {
                            (function (i) {
                                getUnicommerceSalesOrderDetail(req.query.access_token, data['SOAP-ENV:Envelope']['SOAP-ENV:Body']['SearchSaleOrderResponse']['SaleOrders']['SaleOrder']['Code'],
                                    function (back) {
                                        changeIndex();
                                    })
                            })(i);
                        });
                    }
                }
            }
            else if (order_count > 1) {
                recall1(index);
                function changeIndex1() {
                    index++;
                    return recall1(index);
                }

                function recall1(item) {
                    if (item > order_count - 1) {
                        responses.actionCompleteResponse(res);
                    } else {
                        commonFunc.delay(item, function (i) {
                            (function (i) {
                                getUnicommerceSalesOrderDetail(req.query.access_token, data['SOAP-ENV:Envelope']['SOAP-ENV:Body']['SearchSaleOrderResponse']['SaleOrders']['SaleOrder'][i]['Code'],
                                    function (back) {
                                        changeIndex1();
                                    })
                            })(i);
                        });
                    }
                }
            }
            else {
                responses.noDataFoundError(res);
            }
        }
    });
};

function getUnicommerceSalesOrderDetail(acc_token, code, callback) {
    var req_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
        'xmlns:ser="http://uniware.unicommerce.com/services/"> ' +
        '<soapenv:Header> ' +
        '<wsse:Security soapenv:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" ' +
        'xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"> ' +
        '<wsse:UsernameToken wsu:Id="UsernameToken-B9CEE788A6A77174A314370186594982"> ' +
        '<wsse:Username>tookan</wsse:Username> ' +
        '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">a3ccaf36-b463-4bf4-807a-b87ca27d41e6</wsse:Password> ' +
        '<wsse:Nonce EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary">mFtSgEzDGBSTwyOfP1duvw==</wsse:Nonce> </wsse:UsernameToken> ' +
        '</wsse:Security> </soapenv:Header> <soapenv:Body> ' +
        '<ser:GetSaleOrderRequest> ' +
        '<ser:SaleOrder> ' +
        '<ser:Code>' + code + '</ser:Code> ' +
        '</ser:SaleOrder> ' +
        '</ser:GetSaleOrderRequest> ' +
        '</soapenv:Body> </soapenv:Envelope>';
    request.post({
        headers: {'content-type': 'text/xml'},
        url: 'https://bazaarcart.unicommerce.com/services/soap/uniware16.wsdl',
        body: req_data
    }, function (error, response, body) {
        var data = xml2json.toJson(body.toString());
        data = JSON.parse(data);
        createTookanTask(acc_token, data, callback);
    });
}

function createTookanTask(acc_token, response, callback) {
    commonFunc.authenticateUserAccessToken(acc_token, function (result_user) {
        if (result_user == 0) {
            callback(0);
        } else {
            if (response['SOAP-ENV:Envelope']['SOAP-ENV:Body']['GetSaleOrderResponse']['Successful'] &&
                response['SOAP-ENV:Envelope']['SOAP-ENV:Body']['GetSaleOrderResponse']['Successful'] == "true" &&
                response['SOAP-ENV:Envelope']['SOAP-ENV:Body']['GetSaleOrderResponse']['SaleOrder']['ShippingPackages']['ShippingPackage'] &&
                response['SOAP-ENV:Envelope']['SOAP-ENV:Body']['GetSaleOrderResponse']['SaleOrder']['ShippingPackages']['ShippingPackage']['StatusCode'] == 'PACKED') {

                var payment_method = 'Prepaid', amount = 0;
                if (response['SOAP-ENV:Envelope']['SOAP-ENV:Body']['GetSaleOrderResponse']['SaleOrder']['CashOnDelivery']) {
                    payment_method = 'COD';
                }
                var orders = response['SOAP-ENV:Envelope']['SOAP-ENV:Body']['GetSaleOrderResponse']['SaleOrder']['SaleOrderItems']['SaleOrderItem'];
                if (orders && typeof orders == "object") {
                    if (orders.length) {
                        orders.forEach(function (am) {
                            amount += parseFloat(am['TotalPrice']);
                        })
                    } else {
                        amount = orders['TotalPrice'];
                    }
                }
                var req1 = {}, resp = response['SOAP-ENV:Envelope']['SOAP-ENV:Body']['GetSaleOrderResponse']['SaleOrder'],
                    name = '', address = '', phone = '0';
                if (resp['Addresses']['Address'] && typeof resp['Addresses']['Address'] == "object") {
                    if (resp['Addresses']['Address'].length) {
                        name = resp['Addresses']['Address'][0]['Name'];
                        if (resp['Addresses']['Address'][0]['AddressLine1']) address += resp['Addresses']['Address'][0]['AddressLine1'] + ", ";
                        if (resp['Addresses']['Address'][0]['AddressLine2']) address += resp['Addresses']['Address'][0]['AddressLine2'] + ", ";
                        if (resp['Addresses']['Address'][0]['City']) address += resp['Addresses']['Address'][0]['City'] + ", ";
                        if (resp['Addresses']['Address'][0]['State']) address += resp['Addresses']['Address'][0]['State'] + ", ";
                        if (resp['Addresses']['Address'][0]['Country']) address += resp['Addresses']['Address'][0]['Country'] + ", ";
                        if (resp['Addresses']['Address'][0]['Pincode']) address += resp['Addresses']['Address'][0]['Pincode'];
                        phone = resp['Addresses']['Address'][0]['Phone'];
                    } else {
                        name = resp['Addresses']['Address']['Name'];
                        if (resp['Addresses']['Address']['AddressLine1']) address += resp['Addresses']['Address']['AddressLine1'] + ", ";
                        if (resp['Addresses']['Address']['AddressLine2']) address += resp['Addresses']['Address']['AddressLine2'] + ", ";
                        if (resp['Addresses']['Address']['City']) address += resp['Addresses']['Address']['City'] + ", ";
                        if (resp['Addresses']['Address']['State']) address += resp['Addresses']['Address']['State'] + ", ";
                        if (resp['Addresses']['Address']['Country']) address += resp['Addresses']['Address']['Country'] + ", ";
                        if (resp['Addresses']['Address']['Pincode']) address += resp['Addresses']['Address']['Pincode'];
                        phone = resp['Addresses']['Address']['Phone'];
                    }
                }
                req1.body = {
                    "customer_email": resp['NotificationEmail'],
                    "customer_username": name,
                    "customer_phone": commonFunc.formatPhoneNumber(phone, result_user[0].country_phone_code.toUpperCase()),
                    "customer_address": address,
                    "job_description": "Unicommerce Code :" + resp['Code'] + "\n" + "Payment Method :" + payment_method + "\n" + "Amount :" + amount,
                    "job_delivery_datetime": moment(commonFunc.convertTimeIntoLocal(commonFunc.addMinutes(new Date(), 60), result_user[0].timezone)).format('YYYY-MM-DD HH:mm'),

                    "timezone": result_user[0].timezone,
                    "order_id": resp['Code'],
                    "has_pickup": "0",
                    "has_delivery": "1",
                    "layout_type": "0",
                    "tracking_link": 1,
                    "access_token": result_user[0].access_token
                }
                var resp = {
                    send: function (obj) {
                        callback(obj);
                    }
                }
                jobs.create_task(req1, resp);
            } else {
                callback(0);
            }
        }
    });
}
/*
 * --------------
 * ZAPIER HOOK
 * --------------
 */
exports.zapier_hook = function (req, res) {
    if (req.query && req.query.access_token) {
        commonFunc.authenticateUserAccessToken(req.query.access_token, function (result_user) {
            if (result_user == 0) {
                res.statusCode = 401;
                res.end('<html><body>Invalid Credentails.</body></html>');
            } else {
                var target_url = req.body.target_url + "?type=both";
                var layout_type = result_user[0].layout_type, updated_layout_type;
                if (layout_type == constants.layoutType.PICKUP_AND_DELIVERY) {
                    updated_layout_type = '0,1';
                } else if (layout_type == constants.layoutType.APPOINTMENT) {
                    updated_layout_type = '2';
                } else if (layout_type == constants.layoutType.FOS) {
                    updated_layout_type = '3';
                }
                var sql = "SELECT * FROM `tb_templates` " +
                    "WHERE `user_id`=? AND `template_key`=? AND `layout_type` IN (" + updated_layout_type + ") ";
                connection.query(sql, [result_user[0].user_id, 'SUCCESSFUL'], function (err, values) {
                    if (err) {
                        responses.sendError(res);
                    } else {
                        if (values.length > 0) {
                            var index = 0;
                            recall(index);
                            function changeIndex() {
                                index++;
                                return recall(index);
                            }
                            function recall(item) {
                                if (item > values.length - 1) {
                                    responses.actionCompleteResponse(res);
                                } else {
                                    commonFunc.delay(item, function (i) {
                                        (function (i) {
                                            if (values[i].hook) {
                                                var hooks = values[i].hook.split(',');
                                                if (req.body.event){
                                                    hooks.push(target_url);
                                                    target_url = _lodash.uniq(hooks).join(',');
                                                }else{
                                                    var index = _lodash.findLastIndex(hooks, function (chr) {
                                                        return chr == target_url.toString();
                                                    });
                                                    hooks.splice(index, 1);
                                                    target_url = _lodash.uniq(hooks).join(',');
                                                }
                                            }
                                            console.log("target_url",target_url,hooks,values[i].layout_type)
                                            var sql = "UPDATE `tb_templates` SET `hook`=? " +
                                                "WHERE `user_id`=? AND `template_key`=? AND `layout_type`=? ";
                                            connection.query(sql, [target_url, result_user[0].user_id, 'SUCCESSFUL',values[i].layout_type], function (err, update_user_sms_text) {
                                                changeIndex();
                                            });
                                        })(i);
                                    });
                                }
                            }
                        }else{
                            responses.noDataFoundError(res);
                        }
                    }
                });
            }
        });
    } else {
        responses.noDataFoundError(res);
    }
};