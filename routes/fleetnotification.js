var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses   = require('./responses');
var logging = require('./logging');
var moment = require('moment');

/*
 * --------------------------
 * VIEW FLEET NOTIFICATIONS
 * --------------------------
 */

exports.view_fleet_notifications = function(req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "SELECT `fleet_notification_id`,`notification_text`,`minutes_before_task_starts`,`user_id`,`is_enabled` " +
                            "FROM `tb_fleet_notifications` WHERE `user_id`=?";
                        connection.query(sql, [user_id], function (err, fleet_notifications_result) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in finding fleet_notifications_result : ", err, fleet_notifications_result);
                                responses.sendError(res);
                                return;
                            } else {
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data" : {
                                        "notifications":fleet_notifications_result
                                    }
                                };
                                res.send(JSON.stringify(response));
                            }
                        });
                    }
                });
            }
        });
    }
};

/*
 * --------------------------
 * INSERT FLEET NOTIFICATIONS
 * --------------------------
 */

exports.insert_fleet_notification = function(req, res) {

    var access_token = req.body.access_token;
    var notification_text = req.body.notification_text;
    var minutes_before_task_starts = req.body.minutes_before_task_starts;
    var manvalues = [access_token,notification_text,minutes_before_task_starts];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "INSERT INTO `tb_fleet_notifications` (`notification_text`,`minutes_before_task_starts`,`user_id`) " +
                            "VALUES (?,?,?)";
                        connection.query(sql, [notification_text,minutes_before_task_starts,user_id], function (err, fleet_notifications_insertion_result) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in inserting fleet_notifications_result : ", err, fleet_notifications_insertion_result);
                                responses.sendError(res);
                                return;
                            } else {
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data" : {}
                                };
                                res.send(JSON.stringify(response));
                            }
                        });
                    }
                });
            }
        });
    }
};

/*
 * --------------------------
 * UPDATE FLEET NOTIFICATIONS
 * --------------------------
 */

exports.update_fleet_notification = function(req, res) {

    var access_token = req.body.access_token;
    var notification_text = req.body.notification_text;
    var minutes_before_task_starts = req.body.minutes_before_task_starts;
    var is_enabled = req.body.is_enabled;
    var fleet_notification_id = req.body.fleet_notification_id;
    var manvalues = [access_token,notification_text,minutes_before_task_starts,fleet_notification_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "UPDATE `tb_fleet_notifications` " +
                            "SET `notification_text`=?,`minutes_before_task_starts`=?,`is_enabled`=? " +
                            "WHERE `user_id`=? AND `fleet_notification_id`=?";
                        connection.query(sql, [notification_text,minutes_before_task_starts,is_enabled,user_id,fleet_notification_id], function (err, fleet_noti_update) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating fleet_notifications_result : ", err, fleet_noti_update);
                                responses.sendError(res);
                                return;
                            } else {
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data" : {}
                                };
                                res.send(JSON.stringify(response));
                            }
                        });
                    }
                });
            }
        });
    }
};

/*
 * --------------------------
 * DELETE FLEET NOTIFICATION
 * --------------------------
 */

exports.delete_fleet_notification = function(req, res) {

    var access_token = req.body.access_token;
    var fleet_notification_id = req.body.fleet_notification_id;
    var manvalues = [access_token,fleet_notification_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "DELETE FROM `tb_fleet_notifications` " +
                            "WHERE `user_id`=? AND `fleet_notification_id`=?";
                        connection.query(sql, [user_id,fleet_notification_id], function (err, fleet_delete_notification) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in deleting fleet_notifications_result : ", err, fleet_delete_notification);
                                responses.sendError(res);
                                return;
                            } else {
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data" : {}
                                };
                                res.send(JSON.stringify(response));
                            }
                        });
                    }
                });
            }
        });
    }
};