var commonFunc = require('./commonfunction');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var fs = require('fs');
var md5 = require('MD5');
var async = require('async');
var stripe = require("stripe")(config.get('stripeCredentials.StripeKey'));
var commonFuncMailer = require('../templates/commonfunctionMailer');
/*
 * --------------------
 * ENABLE BILLING POPUP
 * --------------------
 */
exports.enable_billing_popup = function (req, res) {

    var email = req.body.email;
    if (typeof email === "undefined" || email === '') {
        email = '';
    }
    commonFunc.getAllUserOfSystem(email, function (all_users) {
        if (all_users == 0) {
            responses.noDataFoundError(res);
        } else {
            var internal_mail = '<table border=1><tr><td><b>Client Name</b></td><td><b>Billing Plan</b></td><td><b>Fleets</b></td><td><b>Tasks</b></td><td><b>Amount</b></td></tr>';
            var all_users_length = all_users.length,index = 0;
            billingHistory(index);
            function changeIndex() {
                index++;
                return billingHistory(index);
            }

            function billingHistory(item) {
                if (item > all_users_length - 1) {
                    internal_mail += '</table>';
                    commonFunc.sendHtmlContent('sumeet@clicklabs.in,sanjay@click-labs.com,arsh@tookanapp.com,saral@tookanapp.com', internal_mail, "Client`s Billing Summary", "", function (result) {});
                    responses.actionCompleteResponse(res);
                } else {
                    commonFunc.delay2(item, function (i) {
                        (function (i) {
                            if (all_users[i].billing_plan == constants.billingPlan.TRIAL) {
                                var timediff = commonFunc.timeDifferenceInDays(new Date(), all_users[i].expiry_datetime);
                                all_users[i].username = all_users[i].username ? all_users[i].username : "";
                                if ((timediff == 0 ) || (all_users[i].expiry_datetime < new Date())) {
                                    var sql = "UPDATE `tb_users` SET `show_billing_popup`=1,`is_active`=? WHERE `user_id`=? LIMIT 1";
                                    connection.query(sql, [constants.userActiveStatus.INACTIVE, all_users[i].user_id], function (err, result_update) {
                                        if (all_users[i].has_mails_enabled) commonFuncMailer.send_template_mail(all_users[i].access_token, all_users[i].username, all_users[i].email, "your-tookan-trial-has-expired", timediff);
                                        changeIndex();
                                    });
                                }
                                else if ((all_users[i].expiry_datetime > new Date()) && (timediff == constants.reminderAccountExpiry.FIRST )) {
                                    if (all_users[i].has_mails_enabled) commonFuncMailer.send_template_mail(all_users[i].access_token, all_users[i].username, all_users[i].email, "your-trial-expires-in-days-1st", timediff);
                                    changeIndex();
                                }
                                else if ((all_users[i].expiry_datetime > new Date()) && (timediff <= constants.reminderAccountExpiry.SECOND) && (timediff > constants.reminderAccountExpiry.THIRD) && (all_users[i].send_expiry_reminder == 0)) {
                                    var sql = "UPDATE `tb_users` SET `show_billing_popup`=1,`send_expiry_reminder`=1 WHERE `user_id`=? LIMIT 1";
                                    connection.query(sql, [all_users[i].user_id], function (err, result_update) {
                                        if (all_users[i].has_mails_enabled) commonFuncMailer.send_template_mail(all_users[i].access_token, all_users[i].username, all_users[i].email, "your-tookan-trial-expires-in-days-2nd", timediff);
                                        changeIndex();
                                    });
                                }
                                else if ((all_users[i].expiry_datetime > new Date()) && (timediff <= constants.reminderAccountExpiry.THIRD) && (all_users[i].send_expiry_reminder == 1)) {
                                    var sql = "UPDATE `tb_users` SET `show_billing_popup`=1,`send_expiry_reminder`=2 WHERE `user_id`=? LIMIT 1";
                                    connection.query(sql, [all_users[i].user_id], function (err, result_update) {
                                        if (all_users[i].has_mails_enabled) commonFuncMailer.send_template_mail(all_users[i].access_token, all_users[i].username, all_users[i].email, "your-tookan-trial-expires-in-days-3rd", timediff);
                                        changeIndex();
                                    });
                                }
                                else if ((all_users[i].expiry_datetime > new Date()) && (timediff == constants.reminderAccountExpiry.FORTH )) {
                                    var html = "Hi,<br><br>";
                                    html += all_users[i].username + "`s account about to expire in 3 days.<br><br>";
                                    html += "<b>Email : </b>" + all_users[i].email + "<br>";
                                    html += "<b>Company name : </b>" + all_users[i].company_name + "<br>";
                                    html += "<b>Expiration Date : </b>" + all_users[i].expiry_datetime + "<br>";
                                    html += "<b>Phone : </b>" + all_users[i].phone + "<br>";
                                    commonFunc.sendHtmlContent('saral@tookanapp.com,sohit@tookanapp.com', html, all_users[i].username + "`s account about to expire in 3 days", "", function (result) {});

                                    if (all_users[i].has_mails_enabled) commonFuncMailer.send_template_mail(all_users[i].access_token, all_users[i].username, all_users[i].email, "account-expiry-your-tookan-trial-is-expiring-clientname", timediff);

                                    changeIndex();
                                }
                                else {
                                    changeIndex();
                                }
                            }
                            else if (all_users[i].billing_plan == constants.billingPlan.FREE_LIMITED) {

                                var amount = 0;
                                var sql = "SELECT COUNT(*) as fleet_count FROM `tb_fleets` WHERE `user_id`=" + all_users[i].user_id + " AND `registration_status`=" + constants.userActiveStatus.ACTIVE + " " +
                                    "AND `is_active`=" + constants.userActiveStatus.ACTIVE + " AND `is_deleted`= " + constants.userDeleteStatus.NO + " ";
                                connection.query(sql, function (err, result_fleet_count) {
                                    var fleet_count = parseInt(result_fleet_count[0].fleet_count) - parseInt(all_users[i].num_fleets);
                                    if (fleet_count < 0) {
                                        fleet_count = 0;
                                    }
                                    var bindparams = "DATE_SUB(NOW(),INTERVAL 1 DAY)," + all_users[i].billing_plan + "," + all_users[i].user_id + "," + fleet_count + "," +
                                        "(SELECT count(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " and DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY)))," + amount + "";
                                    var sql = "INSERT INTO `tb_billing_history` (billing_date,billing_plan,user_id,fleets,tasks,amount) VALUES (" + bindparams + ")";
                                    connection.query(sql, function (err, result_update) {
                                        changeIndex();
                                    });

                                });
                            } else if (all_users[i].billing_plan == constants.billingPlan.FLEET_BASED) {
                                var amount = all_users[i].per_fleet_cost;
                                if (all_users[i].expiry_datetime > new Date()) {
                                    amount = 0;
                                }
                                var sql = "SELECT COUNT(*) as fleet_count FROM `tb_fleets` WHERE `user_id`=" + all_users[i].user_id + " AND `is_deleted`= " + constants.userDeleteStatus.NO + " " +
                                    "AND `registration_status`=" + constants.userActiveStatus.ACTIVE + " AND `is_active`=" + constants.userActiveStatus.ACTIVE + " ";
                                connection.query(sql, function (err, result_fleet_count) {
                                    //var fleet_count = parseInt(result_fleet_count[0].fleet_count) - parseInt(config.get('freeFleetCount'));
                                    var fleet_count = parseInt(result_fleet_count[0].fleet_count);
                                    //if (fleet_count < 0) {
                                    //    fleet_count = 0;
                                    //}
                                    var bindparams = "DATE_SUB(NOW(),INTERVAL 1 DAY)" +
                                        "," + all_users[i].billing_plan + "," + all_users[i].user_id + "," + fleet_count + "," +
                                        "(SELECT count(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " and DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY)))," +
                                        "(" + amount + "* " + fleet_count + ")/30 ";
                                    var sql = "INSERT INTO `tb_billing_history` (billing_date,billing_plan,user_id,fleets,tasks,amount) VALUES (" + bindparams + ")";
                                    connection.query(sql, function (err, result_update) {
                                        var billing_history_sql = "select * from tb_billing_history where billing_history_id=?";
                                        connection.query(billing_history_sql, [result_update.insertId], function (error, getbillinghistory) {

                                            internal_mail += '<tr><td>' + all_users[i].username + '</td><td>' + all_users[i].billing_plan + '</td><td>' + getbillinghistory[0].fleets + '</td><td>' + getbillinghistory[0].tasks + '</td><td>' + getbillinghistory[0].amount + '</td></tr>';

                                            var sql = "SELECT * FROM `tb_invoice_history` WHERE  `billing_month` = EXTRACT( YEAR_MONTH FROM NOW() - INTERVAL 1 DAY ) and user_id=" + all_users[i].user_id + "";
                                            connection.query(sql, function (err, tb_invoice_history_result) {
                                                if (tb_invoice_history_result.length > 0) {
                                                    var sql = "UPDATE `tb_invoice_history` SET `unit_amount`=" + amount + ",`quantity`=" + fleet_count + ", `amount` = `amount` + (" + amount + "*(" + fleet_count + "))/30 WHERE `user_id`=" + all_users[i].user_id + " and `billing_month`=  EXTRACT( YEAR_MONTH FROM NOW())";
                                                    connection.query(sql, function (err, tb_invoice_history_update_result) {
                                                        changeIndex();
                                                    })
                                                } else {
                                                    var invoice_bindparms = " " + all_users[i].user_id + "," + amount + "," + fleet_count + ",((" + amount + "*(" + fleet_count + "))/30),(EXTRACT( YEAR_MONTH FROM NOW()- INTERVAL 1 DAY ))";
                                                    var sql = "INSERT INTO `tb_invoice_history` (`user_id`,`unit_amount`,`quantity`,`amount`,`billing_month`) VALUES (" + invoice_bindparms + ")";
                                                    connection.query(sql, function (err, tb_invoice_history_update_result) {
                                                        console.log(err)
                                                        changeIndex();
                                                    })
                                                }
                                            });
                                        });
                                    });
                                });
                            } else if (all_users[i].billing_plan == constants.billingPlan.TASK_BASED) {
                                var amount = all_users[i].per_task_cost;
                                if (all_users[i].expiry_datetime > new Date()) {
                                    amount = 0;
                                }
                                var sql = "SELECT COUNT(*) as fleet_count FROM `tb_fleets` WHERE `user_id`=" + all_users[i].user_id + " AND `is_deleted`= " + constants.userDeleteStatus.NO + " " +
                                    "AND `registration_status`=" + constants.userActiveStatus.ACTIVE + " AND `is_active`=" + constants.userActiveStatus.ACTIVE + " ";
                                connection.query(sql, function (err, result_fleet_count) {
                                    //var fleet_count = parseInt(result_fleet_count[0].fleet_count) - parseInt(config.get('freeFleetCount'));
                                    var fleet_count = parseInt(result_fleet_count[0].fleet_count);
                                    //if (fleet_count < 0) {
                                    //    fleet_count = 0;
                                    //}
                                    var bindparams = "DATE_SUB(NOW(),INTERVAL 1 DAY)" +
                                        "," + all_users[i].billing_plan + "," + all_users[i].user_id + "," +
                                        "(" + fleet_count + ")," +
                                        "(SELECT count(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " and " +
                                        "DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY)))," +
                                        "" + amount + "*(SELECT count(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " and " +
                                        "DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY)))";
                                    var sql = "INSERT INTO `tb_billing_history` (billing_date,billing_plan,user_id,fleets,tasks,amount) VALUES (" + bindparams + ")";
                                    connection.query(sql, function (err, result_update) {

                                        var billing_history_sql = "select * from tb_billing_history where billing_history_id=?";
                                        connection.query(billing_history_sql, [result_update.insertId], function (error, getbillinghistory) {

                                            internal_mail += '<tr><td>' + all_users[i].username + '</td><td>' + all_users[i].billing_plan + '</td><td>' + getbillinghistory[0].fleets + '</td><td>' + getbillinghistory[0].tasks + '</td><td>' + getbillinghistory[0].amount + '</td></tr>';

                                            var sql = "SELECT * FROM `tb_invoice_history` WHERE `billing_month` = EXTRACT( YEAR_MONTH FROM NOW() - INTERVAL 1 DAY ) and user_id=" + all_users[i].user_id + "";
                                            connection.query(sql, function (err, tb_invoice_history_result) {
                                                if (tb_invoice_history_result.length > 0) {
                                                    var sql = "UPDATE `tb_invoice_history` SET `unit_amount`=" + amount + ",`quantity`=`quantity`+(SELECT count(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " and " +
                                                        "DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY))), `amount` = `amount` + (" + amount + "*((SELECT count(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " and " +
                                                        "DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY))))) WHERE `user_id`=" + all_users[i].user_id + " and `billing_month`=  EXTRACT( YEAR_MONTH FROM NOW()- INTERVAL 1 DAY )";
                                                    connection.query(sql, function (err, tb_invoice_history_update_result) {
                                                        changeIndex();
                                                    })
                                                } else {
                                                    var invoice_bindparms = " " + amount + ",(SELECT count(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " and " +
                                                        "DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY))), " + all_users[i].user_id + ",((" + amount + "*(SELECT count(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " and " +
                                                        "DATE(`creation_datetime`) = DATE(DATE_SUB(NOW(),INTERVAL 1 DAY))))),(EXTRACT( YEAR_MONTH FROM NOW()- INTERVAL 1 DAY ))";
                                                    var sql = "INSERT INTO `tb_invoice_history` (`unit_amount`,`quantity`,`user_id`,`amount`,`billing_month`) VALUES (" + invoice_bindparms + ")";
                                                    connection.query(sql, function (err, tb_invoice_history_update_result) {
                                                        changeIndex();
                                                    })
                                                }
                                            });
                                        });
                                    });
                                });
                            }
                        })(i);
                    });
                }
            }
        }
    })
};
/*
 * -----------------------
 * SKIP BILLING POPUP
 * -----------------------
 */
exports.skip_billing_popup = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "UPDATE `tb_users` SET `show_billing_popup`=0 WHERE `access_token`=? LIMIT 1";
                connection.query(sql, [access_token], function (err, skipbilling) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in skip_billing_popup : ", err, skipbilling);
                        logging.addErrorLog("Error in skip_billing_popup : ", err, skipbilling);
                        responses.sendError(res);
                        return;
                    } else {
                        responses.actionCompleteResponse(res);
                        return;
                    }
                });
            }
        });
    }
};
/*
 * -----------------------
 * ADD USER CREDIT CARD
 * -----------------------
 */
exports.add_credit_card = function (req, res) {

    var access_token = req.body.access_token;
    var stripe_token = req.body.stripe_token;
    var manValues = [access_token, stripe_token];
    var checkblank = commonFunc.checkBlank(manValues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }
    commonFunc.authenticateUserAccessToken(access_token, function (users) {
        if (users == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }
        var user_id = users[0].user_id, email = users[0].email;
        if (users[0].is_dispatcher == constants.isDispatcherStatus.YES) {
            responses.invalidAccessError(res);
        } else {
            var billingData = {
                amount: 1, name: "", email: "", billing_plan: "",
                company_name: "", company_address: "", company_phone: "",
                invoice_history_id: "", billing_month: "", unit_amount: "",
                quantity: "", user_id: ""
            }

            var sql = "SELECT `user_id`,`customer_id` FROM `tb_user_credit_card` WHERE `user_id`=? LIMIT 1";
            connection.query(sql, [user_id], function (err, tb_user_credit_card_result) {

                if (tb_user_credit_card_result.length > 0) {
                    updateCustomerStripe(email, email, stripe_token, tb_user_credit_card_result[0].customer_id, function (err, result) {
                        if (result.success == false) {
                            var response = {
                                "message": result.message,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                        } else {
                            billingData.customer_id = result.customer.id;
                            billingData.card_token = result.customer.creditCards[0].token;
                            doTransactionStripe(billingData, function (err, paymentResult) {
                                if (paymentResult.success) {
                                    console.log("update customer response");
                                    var sql = "UPDATE `tb_user_credit_card` SET `card_token`=?, `last4_digits`=?, " +
                                        "`brand`=?, `funding`=?,`expiry_date`=?,`customer_id`=?,`is_active`=? " +
                                        "WHERE `user_id`=? LIMIT 1";
                                    connection.query(sql, [result.customer.creditCards[0].token, result.sources.data[0].last4, result.sources.data[0].brand,
                                        result.sources.data[0].funding, result.sources.data[0].exp_month + "-" + result.sources.data[0].exp_year, result.customer.id, 1, user_id], function (err, result) {
                                        logging.addlog("Credit card updated", req.body, result, null);
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in updating credit card", err, result, null);
                                            logging.addErrorLog("Error in updating credit card", err, result, null);
                                            responses.sendError(res);
                                            return;
                                        }
                                        var response = {
                                            "message": constants.responseMessages.CARD_UPDATE_SUCCESSFULLY,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": {}
                                        };
                                        logging.addlog(response, null);
                                        res.send(JSON.stringify(response));
                                    });
                                } else {
                                    var response = {
                                        "message": paymentResult.error,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                }
                            });
                        }
                    });
                } else {

                    createCustomerStripe(email, email, stripe_token, function (err, result) {
                        if (result.success == false) {
                            var response = {
                                "message": result.message,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        } else {
                            billingData.customer_id = result.customer.id;
                            billingData.card_token = result.customer.creditCards[0].token;
                            doTransactionStripe(billingData, function (err, paymentResult) {
                                if (paymentResult.success) {
                                    var sql = "INSERT INTO `tb_user_credit_card`(`user_id`, `card_token`, `last4_digits`,`brand`,`funding`,`expiry_date`,`customer_id`) VALUES (?,?,?,?,?,?,?)";
                                    connection.query(sql, [user_id, result.customer.creditCards[0].token, result.sources.data[0].last4, result.sources.data[0].brand, result.sources.data[0].funding, result.sources.data[0].exp_month + "-" + result.sources.data[0].exp_year, result.customer.id], function (err, result) {
                                        logging.addlog("Credit card added", req.body, result, null);
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in adding credit card", err, result, null);
                                            logging.addErrorLog("Error in adding credit card", err, result, null);
                                            responses.sendError(res);
                                            return;
                                        }
                                        var response = {
                                            "message": constants.responseMessages.CARD_ADDED_SUCCESSFULLY,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": {}
                                        };
                                        logging.addlog(response, null);
                                        res.send(JSON.stringify(response));
                                    });
                                } else {
                                    var response = {
                                        "message": paymentResult.error,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}
function createCustomerStripe(email, name, strip_token, callback) {
    stripe.customers.create({
        card: strip_token,
        email: email,
        description: "Customer create for Tookan"
    }, function (err, result) {
        console.log(err)
        if (err) {
            result = err;
            result.success = false

            return callback(err, result);
        } else {

            result.customer = {};
            result.customer.id = result.id;
            result.customer.creditCards = result.sources.data;

            console.log(result.sources.data[0]);
            result.customer.creditCards[0].token = result.sources.data[0].id;
            result.customer.creditCards[0].verification = {};
            result.customer.creditCards[0].verification.creditCard = result.sources.data[0]

            result.success = true;
            return callback(null, result);
        }
    });
}
/*
 * -----------------------
 * UPDATE USER CREDIT CARD
 * -----------------------
 */
exports.update_credit_card = function (req, res) {
    var access_token = req.body.access_token;
    var stripe_token = req.body.stripe_token;
    var manValues = [access_token, stripe_token];
    var checkblank = commonFunc.checkBlank(manValues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }
    commonFunc.authenticateUserAccessToken(access_token, function (users) {
        if (users == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }
        var user_id = users[0].user_id, email = users[0].email;
        if (users[0].is_dispatcher == constants.isDispatcherStatus.YES) {
            responses.invalidAccessError(res);
        } else {
            var sql = "SELECT `user_id`,`customer_id` FROM `tb_user_credit_card` WHERE `user_id`=? LIMIT 1";
            connection.query(sql, [user_id], function (err, tb_user_credit_card_result) {

                if (tb_user_credit_card_result.length > 0) {
                    updateCustomerStripe(email, email, stripe_token, tb_user_credit_card_result[0].customer_id, function (err, result) {
                        if (result.success == false) {
                            var response = {
                                "message": result.message,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        }
                        console.log("update customer response");
                        var sql = "UPDATE `tb_user_credit_card` SET `card_token`=?, `last4_digits`=?, " +
                            "`brand`=?, `funding`=?,`expiry_date`=?,`customer_id`=? " +
                            "WHERE `user_id`=? LIMIT 1";
                        connection.query(sql, [result.customer.creditCards[0].token, result.sources.data[0].last4, result.sources.data[0].brand, result.sources.data[0].funding, result.sources.data[0].exp_month + "-" + result.sources.data[0].exp_year, result.customer.id, user_id], function (err, result) {
                            logging.addlog("Credit card updated", req.body, result, null);
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating credit card", err, result, null);
                                logging.addErrorLog("Error in updating credit card", err, result, null);
                                responses.sendError(res);
                                return;
                            }
                            var response = {
                                "message": constants.responseMessages.CARD_UPDATE_SUCCESSFULLY,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {}
                            };
                            logging.addlog(response, null);
                            res.send(JSON.stringify(response));
                        });
                    });
                } else {

                    var response = {
                        "message": constants.responseMessages.CARD_NOT_ADDED_ERROR,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {}
                    };
                    logging.addlog(response, null);
                    res.send(JSON.stringify(response));


                }
            });
        }
    });
}
function updateCustomerStripe(email, name, strip_token, customer_id, callback) {
    stripe.customers.update(customer_id, {
        card: strip_token,
        email: email,
        description: "Customer update for Tookan."
    }, function (err, result) {
        console.log(err)
        console.log(result)
        if (err) {
            result = err;
            result.success = false

            return callback(err, result);
        } else {

            result.customer = {};
            result.customer.id = result.id;
            result.customer.creditCards = result.sources.data;

            console.log(result.sources.data[0]);
            result.customer.creditCards[0].token = result.sources.data[0].id;
            result.customer.creditCards[0].verification = {};
            result.customer.creditCards[0].verification.creditCard = result.sources.data[0];

            result.success = true;
            return callback(null, result);
        }
    });
}
/*
 * -----------------------
 * CHANGE BILLING PLAN
 * -----------------------
 */
exports.change_billing_plan = function (req, res) {

    console.log("request change_billing_plan === ", req.body);
    var access_token = req.body.access_token;
    var billing_plan = parseInt(req.body.billing_plan);
    var manValues = [access_token, billing_plan];
    var checkblank = commonFunc.checkBlank(manValues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    }
    commonFunc.authenticateUserAccessToken(access_token, function (users) {
        if (users == 0) {
            responses.authenticationErrorResponse(res);
            return;
        }
        var user_id = users[0].user_id;
        var num_fleets = users[0].num_fleets;
        if (users[0].is_dispatcher == constants.isDispatcherStatus.YES) {
            responses.invalidAccessError(res);
        } else {
            if (billing_plan == constants.billingPlan.FREE_LIMITED) {

                commonFunc.getAllFleets(user_id, function (getAllFleetsResult) {
                    var fleets_count = getAllFleetsResult.length;
                    if (fleets_count > num_fleets) {
                        var message = constants.responseMessages.EXCEED_FLEET_COUNT;
                        message = message.replace(/<%FLEET%>/g, num_fleets + " " + users[0].call_fleet_as);
                        var response = {
                            "message": message,
                            "status": constants.responseFlags.EXCEED_FLEET_COUNT,
                            "data": {
                                fleets: getAllFleetsResult
                            }
                        };
                        res.send(JSON.stringify(response));
                    } else {
                        commonFunc.insertORupdateTaskForCurrentMonth(user_id, 0);
                        updateBillingPlan(billing_plan, user_id, function (response) {
                            res.send(JSON.stringify(response));
                        });
                    }
                })
            } else {
                var sql = "SELECT `user_id` FROM `tb_user_credit_card` WHERE `user_id`=? LIMIT 1";
                connection.query(sql, [user_id], function (err, tb_user_credit_card_result) {

                    if (tb_user_credit_card_result.length > 0) {
                        updateBillingPlan(billing_plan, user_id, function (response) {
                            res.send(JSON.stringify(response));
                        });
                    } else {
                        var response = {
                            "message": constants.responseMessages.CARD_NOT_ADDED_ERROR,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                        logging.addlog(response, null);
                        res.send(JSON.stringify(response));
                    }
                });
            }
        }
    });
}
function updateBillingPlan(billing_plan, user_id, callback) {
    var sql = "UPDATE `tb_users` SET `billing_plan`=?,`is_active`=?,`show_billing_popup`=0 WHERE (`user_id`=? || `dispatcher_user_id`=?)";
    connection.query(sql, [billing_plan, constants.userActiveStatus.ACTIVE, user_id, user_id], function (err, billing_plan_updated_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in Billing Plan Updated", err, billing_plan_updated_result, null);
            logging.addErrorLog("Error in Billing Plan Updated", err, billing_plan_updated_result, null);
        }
        var response = {
            "message": constants.responseMessages.UPDATE_BILLING_PLAN,
            "status": constants.responseFlags.ACTION_COMPLETE,
            "data": {}
        };
        callback(response);
    });
}
/*
 * ---------------------------------------------------
 * DEDUCT PAYMENT FROM USERS ACCOUNT ON MONTHLY BASIS
 * ---------------------------------------------------
 */
exports.deduct_monthly_payment = function (req, res) {
    var sql = "SELECT user_cc.*,users.*,inv_his.* FROM `tb_users` users " +
        "INNER JOIN `tb_user_credit_card` user_cc ON user_cc.user_id = users.user_id " +
        "INNER JOIN `tb_invoice_history` inv_his on users.user_id=inv_his.user_id " +
        "WHERE inv_his.is_billed = 0 ";
    connection.query(sql, function (err, get_all_payments) {
        logging.addlog("GET ALL CREDIT CARDS OF USER", req.body, get_all_payments, null);
        if (err) {
            logging.addErrorLog("ERROR IN GETTING ALL CREDIT CARDS OF USER", err, get_all_payments, null);
            responses.sendError(res);
            return;
        }
        var get_all_payments_length = get_all_payments.length, index = 0;
        deductMonthlyPayment(index);
        function changeIndex() {
            index++;
            return deductMonthlyPayment(index);
        }

        function deductMonthlyPayment(item) {
            if (item > get_all_payments_length - 1) {
                responses.actionCompleteResponse(res);
            } else {
                commonFunc.delay2(item, function (i) {
                    (function (i) {
                        var data = {
                            amount: get_all_payments[i].amount,
                            name: get_all_payments[i].username,
                            email: get_all_payments[i].email,
                            billing_plan: get_all_payments[i].billing_plan,
                            company_name: get_all_payments[i].company_name,
                            company_address: get_all_payments[i].company_address,
                            company_phone: get_all_payments[i].phone,
                            customer_id: get_all_payments[i].customer_id,
                            card_token: get_all_payments[i].card_token,
                            id: get_all_payments[i].invoice_history_id,
                            billing_month: get_all_payments[i].billing_month,
                            unit_amount: get_all_payments[i].unit_amount,
                            quantity: get_all_payments[i].quantity,
                            user_id: get_all_payments[i].user_id
                        }

                        doTransactionStripe(data, function (err, paymentResult) {
                            if (paymentResult.success) {
                                var randomString = commonFunc.generateRandomStringAndNumbers();
                                var invoice = "INV-" + paymentResult.billing_month + "-" + paymentResult.invoice_history_id + "-" + randomString;
                                var invoiceTemplatePath = config.get('invoiceTemplate') + invoice + ".html";
                                var invoicePDF = invoice + ".pdf";
                                var invoicePath = config.get('invoices') + invoicePDF;

                                var sql = "UPDATE `tb_invoice_history` " +
                                    " SET `is_billed`=1,`invoice`=?,`billed_on`=NOW() " +
                                    " WHERE `invoice_history_id`=? LIMIT 1";
                                connection.query(sql, [invoicePDF, paymentResult.invoice_history_id], function (err, billing_done) {
                                    logging.addlog("Deducted Bill for user ---- ", err, billing_done + "---" + paymentResult.invoice_history_id, null);

                                    var sql = "UPDATE `tb_user_credit_card` " +
                                        " SET `is_active`=? " +
                                        " WHERE `user_id`=? LIMIT 1";
                                    connection.query(sql, [1, data.user_id], function (err, done) {

                                        // SEND INVOICE EMAIL
                                        commonFuncMailer.invoice_pdf_html(moment(new Date()).format('DD-MM-YYYY'), invoice, data.name, data.email, data.company_name, data.company_address,
                                            data.company_phone, data.billing_plan, data.unit_amount, data.quantity, data.amount, data.amount, function (returnMessage) {
                                                commonFunc.generateAndSavePdf(returnMessage, invoiceTemplatePath, invoicePath, function (generateAndSavePdfResult) {
                                                    if (generateAndSavePdfResult) {
                                                        commonFunc.uploadImageToBucketFromURL(invoicePath, config.get('s3BucketCredentials.folder.invoices'), invoicePDF, function (ImageResult) {
                                                            if (ImageResult != 0) {
                                                                commonFuncMailer.billing_mail_text(invoice, data.name, data.email, data.company_name, data.company_address, data.company_phone, data.billing_plan, data.unit_amount, data.quantity, data.amount, data.amount, function (billing_mail_text_Result) {
                                                                    commonFunc.sendEmailWithAttachment(invoicePath, paymentResult.email, billing_mail_text_Result, "Invoice Statement for Month : " + commonFunc.convertToDateString(paymentResult.billing_month), "", function (result) {
                                                                        fs.unlink(invoiceTemplatePath);
                                                                        fs.unlink(invoicePath);
                                                                        changeIndex();
                                                                    })
                                                                });
                                                            }else{
                                                                var sql = "UPDATE `tb_invoice_history` " +
                                                                    " SET `is_billed`=5,`invoice`=?,`billed_on`=NOW() " +
                                                                    " WHERE `invoice_history_id`=? LIMIT 1";
                                                                connection.query(sql, [invoicePDF, paymentResult.invoice_history_id], function (err, billing_done) {
                                                                    changeIndex();
                                                                });
                                                            }
                                                        });
                                                    }else{
                                                        var sql = "UPDATE `tb_invoice_history` " +
                                                            " SET `is_billed`=6,`invoice`=?,`billed_on`=NOW() " +
                                                            " WHERE `invoice_history_id`=? LIMIT 1";
                                                        connection.query(sql, [invoicePDF, paymentResult.invoice_history_id], function (err, billing_done) {
                                                            changeIndex();
                                                        });
                                                    }
                                                })
                                            });
                                    });
                                });
                            } else {
                                var sql = "UPDATE `tb_invoice_history` SET `is_billed`=2,`invoice`=?,`billed_on`=NOW() WHERE `invoice_history_id`=? LIMIT 1";
                                connection.query(sql, [paymentResult.error, paymentResult.invoice_history_id], function (err, billing_done) {
                                    var sql = "UPDATE `tb_user_credit_card` SET `is_active`=? WHERE `user_id`=? LIMIT 1";
                                    connection.query(sql, [0, data.user_id], function (err, done) {
                                        changeIndex();
                                    });
                                });
                            }
                        });
                    })(i);
                });
            }
        }
    });
}
function doTransactionStripe(field, callback) {
    field.amount_in_cents = field.amount * 100;
    if (field.amount_in_cents < 50) {
        var result = {};
        result.success = true;
        result.invoice_history_id = field.id;
        result.quantity = field.quantity;
        result.unit_amount = field.unit_amount;
        result.billing_month = field.billing_month;
        result.email = field.email;
        result.amount = 0;
        result.name = field.name;
        return callback(null, result);
    }
    else {
        stripe.charges.create({
            amount: Math.round(field.amount_in_cents),
            currency: "usd",
            customer: field.customer_id, // obtained with Stripe.js
            description: field.desc || "Charge for Tookan"
        }, function (err, result) {
            console.log("Error in payment: " + err)
            if (err) {
                result = {};
                result.success = false;
                result.invoice_history_id = field.id;
                result.quantity = field.quantity;
                result.unit_amount = field.unit_amount;
                result.billing_month = field.billing_month;
                result.email = field.email;
                result.amount = field.amount;
                result.name = field.name;
                result.error = err.message;
                return callback(err, result)
            }
            if (result.paid == true) {
                result.success = true;
                result.invoice_history_id = field.id;
                result.quantity = field.quantity;
                result.unit_amount = field.unit_amount;
                result.billing_month = field.billing_month;
                result.email = field.email;
                result.amount = field.amount;
                result.name = field.name;
            }
            return callback(err, result)
        });
    }
}
/*
 * ---------------------------------------------------
 * Deduct individual payment like integrations etc.
 * ---------------------------------------------------
 */
exports.deduct_individual_payment = function (req, res) {
    var user_id = req.body.user_id,
        amount = req.body.amount,
        desc = req.body.desc,
        invoice = req.body.invoice_number,
        unit = req.body.unit_price,
        quantity = req.body.quantity,
        billing_plan = req.body.billing_plan,
        manvalues = [user_id, amount, desc, invoice, unit, quantity],
        checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.getUserDetails(user_id, function (users) {
            if (users == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT user_cc.*,users.* FROM `tb_users` users " +
                    "INNER JOIN `tb_user_credit_card` user_cc ON user_cc.user_id = users.user_id " +
                    "WHERE users.`user_id`= ? ";
                connection.query(sql, [users[0].user_id], function (err, get_all_payments) {
                    if (err) {
                        responses.sendError(res);
                        return;
                    }
                    var data = {
                        amount: amount,
                        name: get_all_payments[0].username,
                        email: get_all_payments[0].email,
                        billing_plan: billing_plan || get_all_payments[0].billing_plan,
                        company_name: get_all_payments[0].company_name,
                        company_address: get_all_payments[0].company_address,
                        company_phone: get_all_payments[0].phone,
                        customer_id: get_all_payments[0].customer_id,
                        card_token: get_all_payments[0].card_token,
                        id: 0,
                        billing_month: '',
                        unit_amount: unit,
                        quantity: quantity,
                        user_id: get_all_payments[0].user_id,
                        desc: desc
                    }
                    doTransactionStripe(data, function (err, paymentResult) {
                        if (paymentResult.success) {
                            var invoiceTemplatePath = config.get('invoiceTemplate') + invoice + ".html";
                            var invoicePDF = invoice + ".pdf";
                            var invoicePath = config.get('invoices') + invoicePDF;
                            commonFuncMailer.invoice_pdf_html(moment(new Date()).format('DD-MM-YYYY'), invoice, data.name, data.email, data.company_name, data.company_address,
                                data.company_phone, data.billing_plan, data.unit_amount, data.quantity, data.amount, data.amount, function (returnMessage) {
                                    commonFunc.generateAndSavePdf(returnMessage, invoiceTemplatePath, invoicePath, function (generateAndSavePdfResult) {
                                        if (generateAndSavePdfResult == true) {
                                            res.setHeader('Content-Type', 'application/octet-stream');
                                            res.setHeader('Content-Disposition', 'attachment; filename=' + invoice + '.pdf;');
                                            var filestream = fs.createReadStream(invoicePath);
                                            filestream.pipe(res);
                                            fs.unlink(invoiceTemplatePath);
                                            fs.unlink(invoicePath);
                                        }
                                    })
                                });

                        } else {
                            responses.authenticationError(res);
                        }
                    });
                });
            }
        });
    }
}
exports.create_token = function (req, res) {

    stripe.tokens.create({
        card: {
            "number": '4242424242424242',
            "exp_month": 12,
            "exp_year": 2016,
            "cvc": '123'
        }
    }, function (err, token) {
        res.send(JSON.stringify(token));
    });
}
/*
 * -----------------------
 * USER'S BILLING HISTORY
 * -----------------------
 */
exports.billing_history = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                async.parallel([
                        function (callback) {
                            var sql = "SELECT users.`timezone`,users.`expiry_datetime`,users.`username`,users.`billing_plan`,users.`company_name`,users.`company_address`," +
                                "user_cc.`last4_digits`,user_cc.`brand`,user_cc.`funding`,user_cc.`expiry_date` " +
                                "FROM `tb_users` users LEFT JOIN `tb_user_credit_card` user_cc ON users.`user_id`=user_cc.`user_id` " +
                                "WHERE users.`user_id`=?";
                            connection.query(sql, [result[0].user_id], function (err, tb_user_credit_card_result) {
                                if (tb_user_credit_card_result.length > 0) {
                                    tb_user_credit_card_result[0].days_left = parseInt(commonFunc.timeDifferenceInDays(new Date(), tb_user_credit_card_result[0].expiry_datetime));
                                    if (tb_user_credit_card_result[0].billing_plan != constants.billingPlan.TRIAL) {
                                        tb_user_credit_card_result[0].expiry_datetime = "Unlimited";
                                        tb_user_credit_card_result[0].days_left = "Unlimited";
                                        var today = commonFunc.convertTimeIntoLocal(new Date(), tb_user_credit_card_result[0].timezone);
                                        var date = new Date(today), y = date.getFullYear(), m = date.getMonth();
                                        var dateArray = (new Date(y, m + 1, 1).toDateString()).split(' ');
                                        tb_user_credit_card_result[0].next_billing_date = dateArray[1] + " " + dateArray[2] + "," + dateArray[3];

                                    }
                                    callback(null, tb_user_credit_card_result);
                                } else {
                                    callback(null, []);
                                }
                            });
                        },
                        function (callback) {
                            var sql = "SELECT `amount`,`is_billed`,`billing_month` FROM `tb_invoice_history` " +
                                "WHERE `user_id`=? AND `is_billed`= 0 ORDER BY `billed_on` DESC";
                            connection.query(sql, [result[0].user_id], function (err, unbilled_result) {
                                if (unbilled_result.length > 0) {
                                    var unbilled_amount = 0;
                                    unbilled_result.forEach(function (unbill) {
                                        unbill.billing_month = commonFunc.convertToDateString(unbill.billing_month);
                                        unbilled_amount += unbill.amount;
                                    })
                                    var response = {
                                        "total_unbilled_amount": unbilled_amount,
                                        "data": unbilled_result
                                    };
                                    callback(null, response);
                                } else {
                                    callback(null, []);
                                }
                            });
                        },
                        function (callback) {
                            var sql = "SELECT billing_date,billing_plan,user_id,fleets,tasks,amount " +
                                " FROM  `tb_billing_history` WHERE `user_id` =? ORDER BY `creation_datetime` DESC LIMIT 1";
                            connection.query(sql, [result[0].user_id], function (err, billing_history) {
                                if (billing_history.length > 0) {
                                    callback(null, billing_history);
                                } else {
                                    callback(null, []);
                                }
                            });
                        },
                        function (callback) {
                            var sql = "SELECT `invoice`,`billing_month`,`billed_on`,`is_billed`,`invoice_history_id`,`amount` " +
                                "FROM `tb_invoice_history` WHERE `user_id`=? AND `is_billed`=1 ORDER BY `billed_on` DESC";
                            connection.query(sql, [result[0].user_id], function (err, tb_user_invoice_result) {
                                if (tb_user_invoice_result.length > 0) {

                                    tb_user_invoice_result.forEach(function (invoice) {
                                        invoice.billing_month = commonFunc.convertToDateString(invoice.billing_month);
                                        invoice.invoice_link = config.get('invoiceLink') + invoice.invoice;
                                        invoice.invoice = invoice.invoice.substr(0, invoice.invoice.lastIndexOf('.'));
                                        invoice.billed_on = invoice.billed_on
                                    })
                                    callback(null, tb_user_invoice_result)
                                } else {
                                    callback(null, []);
                                }
                            });
                        },
                        function (callback) {
                            var sql = "SELECT `per_fleet_cost`,`per_task_cost`,(SELECT count(*) FROM `tb_fleets` WHERE `user_id` = ? AND `registration_status` = ? AND `is_active` = ? AND `is_deleted`= ?) as fleets," +
                                "(SELECT count(*) FROM `tb_jobs` WHERE `user_id` = ? and DATE(`creation_datetime`) >= DATE(DATE_SUB(NOW(),INTERVAL 30 DAY)))  as tasks FROM `tb_users` " +
                                "WHERE `user_id`=? LIMIT 1";
                            connection.query(sql, [result[0].user_id, constants.userVerificationStatus.VERIFY, constants.userVerificationStatus.VERIFY, constants.userDeleteStatus.NO, result[0].user_id, result[0].user_id], function (err, recommended_plan) {
                                if (recommended_plan.length > 0) {
                                    var fleets_cost = (recommended_plan[0].fleets * recommended_plan[0].per_fleet_cost);
                                    var tasks_cost = (recommended_plan[0].tasks * recommended_plan[0].per_task_cost);
                                    var response = {
                                        "fleets_count": recommended_plan[0].fleets,
                                        "tasks_count": recommended_plan[0].tasks
                                    }
                                    if (fleets_cost > tasks_cost) {
                                        response.plan = constants.billingPlan.TASK_BASED
                                        callback(null, response);
                                    } else {
                                        response.plan = constants.billingPlan.FLEET_BASED
                                        callback(null, response);
                                    }
                                } else {
                                    callback(null, []);
                                }
                            });
                        }],
                    function (err, asyncResults) {
                        if (asyncResults[1].length > 0) {
                            asyncResults[0][0].un_billed = asyncResults[1];
                        }
                        if (asyncResults[2].length > 0) {
                            asyncResults[0][0].last_bill = asyncResults[2];
                        }
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {
                                "billing_details": asyncResults[0],
                                "billing_history": asyncResults[3],
                                "recommended_plan": asyncResults[4]
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    });
            }
        });
    }
};
/*
 * -----------------------
 * DEACTIVE_FLEET ACCOUNT
 * -----------------------
 */
exports.deactive_fleet_account = function (req, res) {

    var access_token = req.body.access_token;
    var fleet_ids = req.body.fleet_ids;
    var billing_plan = req.body.billing_plan;
    var manvalues = [access_token, fleet_ids];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var num_fleets = result[0].num_fleets
                var sql = "UPDATE `tb_fleets` SET `access_token`=?,`is_active`=?,`device_token`=? " +
                    "WHERE `fleet_id` NOT IN (" + fleet_ids + ") AND `user_id` =?";
                connection.query(sql, [md5(new Date() + commonFunc.generateRandomString()), constants.userActiveStatus.INACTIVE, null, result[0].user_id], function (err, deactive_account) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in updating fleet information  : ", err, deactive_account);
                        responses.sendError(res);
                        return;
                    } else {
                        commonFunc.getAllFleets(result[0].user_id, function (getAllFleetsResult) {
                            var fleets_count = getAllFleetsResult.length;
                            if (fleets_count > num_fleets) {
                                var message = constants.responseMessages.EXCEED_FLEET_COUNT;
                                message = message.replace(/<%FLEET%>/g, num_fleets + " " + result[0].call_fleet_as);
                                var response = {
                                    "message": message,
                                    "status": constants.responseFlags.EXCEED_FLEET_COUNT,
                                    "data": {
                                        fleets: getAllFleetsResult
                                    }
                                };
                                res.send(JSON.stringify(response));
                            } else {
                                updateBillingPlan(billing_plan, result[0].user_id, function (response) {
                                    res.send(JSON.stringify(response));
                                });
                            }
                        })
                    }
                });
            }
        });
    }
};