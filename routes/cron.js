var commonFunc = require('./commonfunction');
var cron = require('./cron');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var md5 = require('MD5');
var schedule = require('node-schedule');
var commonFuncMailer = require('../templates/commonfunctionMailer');

function sendPushNotification(fleet_id, timeDiffMinutes, jobType, job_id, notification_text) {
    if (fleet_id != null) {
        commonFunc.getJobDetailsFromJobID(job_id, function (getJobDetailsFromJobIDResult) {
            if (getJobDetailsFromJobIDResult!=0) {
                notification_text = notification_text.replace(/\[TIME]/g, Math.round(timeDiffMinutes));
                var message_fleet = notification_text;
                var driverOfflineMessage = 'Hi [Fleet name], this is to notify you regarding the Task ID #' + job_id + '. You have ' + timeDiffMinutes + ' minutes left to start the job.';
                var payload_fleet = {
                    flag: constants.notificationFlags.TASK_REMINDER,
                    message: notification_text,
                    job_id: job_id,
                    job_type: getJobDetailsFromJobIDResult[0].job_type,
                    cust_name: getJobDetailsFromJobIDResult[0].job_type==constants.jobType.PICKUP?getJobDetailsFromJobIDResult[0].job_pickup_name:getJobDetailsFromJobIDResult[0].customer_username,
                    end_time: getJobDetailsFromJobIDResult[0].job_delivery_datetime == "0000-00-00 00:00:00" ? getJobDetailsFromJobIDResult[0].job_delivery_datetime : getJobDetailsFromJobIDResult[0].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                    start_time: getJobDetailsFromJobIDResult[0].job_pickup_datetime == "0000-00-00 00:00:00" ? getJobDetailsFromJobIDResult[0].job_pickup_datetime : getJobDetailsFromJobIDResult[0].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, ''),
                    start_address: getJobDetailsFromJobIDResult[0].job_pickup_address,
                    end_address: getJobDetailsFromJobIDResult[0].job_address,
                    accept_button: 0,
                    d:moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                };
                commonFunc.sendNotification(fleet_id, message_fleet, false, payload_fleet, driverOfflineMessage);
            }
        });
    }
}
function scheduleNotificationBeforeStartTime(id, job_id, fleet_id, job_time, job_time_utc, job_type, timezone, notification_fires_on_utc, notification_text) {
    console.log("schdeule job on === " + notification_fires_on_utc);
    // Schedule Job for fleet before task starts
    schedule.scheduleJob("schedule" + id.toString(), notification_fires_on_utc, function () {
        var date = new Date();
        var timeDiffMinutes = commonFunc.timeDifferenceInMinutes(date, job_time_utc);
        sendPushNotification(fleet_id, timeDiffMinutes, job_type, job_id, notification_text);
        // Delete entry of schedule from table
        cron.deleteScheduleFleetNotification(id);
    });
};

/*******
 ==============================================================================
 GET ALL TASK FOR TODAY ONLY AND SCHEDULE NOTIFICATION INCASE OF SERVER RESTART
 ==============================================================================
 *******/
exports.getAllTasksForTodayAndScheduleNotificationInCaseOfServerRestart = function (callback) {
    var sql = "SELECT noti.* FROM `tb_fleet_task_notify` noti ";
    connection.query(sql, function (err, result_all_today_tasks) {
        if (err) {
            logging.logDatabaseQueryError("Error in result_all_today_tasks", err, result_all_today_tasks);
            callback(err);
        } else {
            var jobs_length = result_all_today_tasks.length;
            if (jobs_length > 0) {
                var counter = 0;
                for (var i = 0; i < jobs_length; i++) {
                    (function (i) {
                        if ((new Date(result_all_today_tasks[i].job_time_utc).toDateString() == new Date().toDateString()) && (commonFunc.timeDifferenceInMinutes(new Date(), result_all_today_tasks[i].job_time_utc) > parseInt(result_all_today_tasks[i].minutes_before_task_starts))) {

                            scheduleNotificationBeforeStartTime(result_all_today_tasks[i].fleet_task_notify_id, result_all_today_tasks[i].job_id, result_all_today_tasks[i].fleet_id,
                                result_all_today_tasks[i].job_time, result_all_today_tasks[i].job_time_utc, result_all_today_tasks[i].job_type, result_all_today_tasks[i].timezone,
                                result_all_today_tasks[i].notification_fires_on_utc, result_all_today_tasks[i].notification_text);

                        }
                        counter++;
                        sendResponse(counter, jobs_length);

                    })(i);
                }
                function sendResponse(counter, jobs_length) {
                    if (counter == jobs_length) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {}
                        };
                        callback(response);
                    }
                }
            } else {
                var response = {
                    "message": constants.responseMessages.NO_DATA_FOUND,
                    "status": constants.responseFlags.USER_NOT_FOUND,
                    "data": {}
                };
                callback(response);
            }
        }
    });
};
/******
 ===================================================
 INSERT TASK FOR FLEET NOTIFICATION FOR TODAY ONLY
 ===================================================
 *******/
exports.getAllTasksForTodayAndScheduleNotification = function (req, res) {
    var sql = "SELECT job.`user_id`,job.`job_id`,job.`fleet_id`,job.`job_time`,job.`job_type`,job.`timezone`,job.`job_time_utc` " +
        " FROM `tb_jobs` job " +
        " WHERE job.`job_status` = ? AND DATE(job.`job_time_utc`) >= DATE(NOW())";
    connection.query(sql, [constants.jobStatus.UPCOMING], function (err, result_all_today_tasks) {
        if (err) {
            logging.logDatabaseQueryError("Error in result_all_today_tasks cron", err, result_all_today_tasks);
            responses.sendError(res);
            return;
        } else {

            var jobs_length = result_all_today_tasks.length;
            if (jobs_length > 0) {
                var counter = 0, bindparams, newsql;
                for (var i = 0; i < jobs_length; i++) {
                    (function (i) {

                        cron.getAllFleetNotifications(result_all_today_tasks[i].user_id, function (fleet_notification_result) {
                            var fleet_notification_result_length = fleet_notification_result.length;

                            for (var j = 0; j < fleet_notification_result_length; j++) {
                                (function (j) {

                                    var job_time_utc = new Date(result_all_today_tasks[i].job_time_utc);
                                    var notification_fires_on_utc = new Date();
                                    notification_fires_on_utc.setTime(job_time_utc.getTime() - (parseInt(fleet_notification_result[j].minutes_before_task_starts) * 60 * 1000));
                                    newsql = "INSERT INTO `tb_fleet_task_notify` (`notification_text`,`minutes_before_task_starts`,`user_id`,`job_id`,`fleet_id`,`job_time`,`job_time_utc`,`job_type`,`timezone`,`notification_fires_on_utc`) " +
                                        "VALUES (?,?,?,?,?,?,?,?,?,?)";
                                    connection.query(newsql, [fleet_notification_result[j].notification_text, fleet_notification_result[j].minutes_before_task_starts, result_all_today_tasks[i].user_id, result_all_today_tasks[i].job_id, result_all_today_tasks[i].fleet_id,
                                        result_all_today_tasks[i].job_time, result_all_today_tasks[i].job_time_utc, result_all_today_tasks[i].job_type, result_all_today_tasks[i].timezone, notification_fires_on_utc
                                    ], function (err, insert_result_all_today_tasks) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in inserting tb_fleet_task_notify  ===== ", err, insert_result_all_today_tasks)
                                        }

                                        scheduleNotificationBeforeStartTime(insert_result_all_today_tasks.insertId, result_all_today_tasks[i].job_id, result_all_today_tasks[i].fleet_id,
                                            result_all_today_tasks[i].job_time, result_all_today_tasks[i].job_time_utc, result_all_today_tasks[i].job_type, result_all_today_tasks[i].timezone,
                                            notification_fires_on_utc, fleet_notification_result[j].notification_text);
                                    });
                                })(j);
                            }
                            counter++;
                            sendResponse(counter, jobs_length);
                        });
                    })(i);
                }
                function sendResponse(counter, jobs_length) {
                    if (counter == jobs_length) {
                        responses.actionCompleteResponse(res);
                        return;
                    }
                }
            } else {
                responses.noDataFoundError(res);
            }
        }
    });
};

/******
 ===================================================
 INSERT TASK FOR FLEET NOTIFICATION FOR TODAY ONLY
 ===================================================
 *******/
exports.scheduleFleetNotification = function (user_id, job_id, fleet_id, job_time, job_time_utc, job_type, timezone) {
    cron.getAllFleetNotifications(user_id, function (fleet_noti) {
        var fleet_notification_result_length = fleet_noti.length;
        for (var j = 0; j < fleet_notification_result_length; j++) {
            (function (j) {
                if ((new Date(job_time_utc).toDateString() == new Date().toDateString()) && (commonFunc.timeDifferenceInMinutes(new Date(), job_time_utc) > parseInt(fleet_noti[j].minutes_before_task_starts))) {
                    var job_time_utc_duplicate = new Date(job_time_utc);
                    var notification_fires_on_utc = new Date();
                    notification_fires_on_utc.setTime(job_time_utc_duplicate.getTime() - (parseInt(fleet_noti[j].minutes_before_task_starts) * 60 * 1000));
                    var newsql = "INSERT INTO `tb_fleet_task_notify` (`notification_text`,`minutes_before_task_starts`,`user_id`,`job_id`,`fleet_id`,`job_time`" +
                        ",`job_time_utc`,`job_type`,`timezone`,`notification_fires_on_utc`) VALUES (?,?,?,?,?,?,?,?,?,?)";
                    connection.query(newsql, [fleet_noti[j].notification_text, fleet_noti[j].minutes_before_task_starts, user_id, job_id, fleet_id, job_time, job_time_utc,
                        job_type, timezone, notification_fires_on_utc], function (err, insert_result_all_today_tasks) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in scheduling fleet notification  ===== ", err, insert_result_all_today_tasks)
                        }
                        console.log("tb_fleet_task_notify  ===== " + insert_result_all_today_tasks.insertId);
                        scheduleNotificationBeforeStartTime(insert_result_all_today_tasks.insertId, job_id, fleet_id, job_time, job_time_utc, job_type, timezone,
                            notification_fires_on_utc, fleet_noti[j].notification_text);
                    });
                }
            })(j);
        }
    });
}

/******
 ===================================================
 UPDATE TASK FOR FLEET NOTIFICATION FOR TODAY ONLY
 ===================================================
 *******/
exports.updateScheduleFleetNotification = function (user_id, job_id, fleet_id, job_time, job_time_utc, job_type, timezone) {
    var sql = "SELECT * FROM `tb_fleet_task_notify` " +
        "WHERE `job_id`=? AND `fleet_id`=? AND `user_id`=?";
    connection.query(sql, [job_id, fleet_id, user_id], function (err, schedules) {
        schedules.forEach(function (sd) {
            try {
                cron.deleteSchedule('schedule' + sd.fleet_task_notify_id);
            } catch (e) {
                console.log("========= Error in Cancelling  updateScheduleFleetNotification=======");
                console.log(e);
            }
        });
        var deleteSql = "DELETE FROM `tb_fleet_task_notify` " +
            "WHERE `job_id`=? AND `fleet_id`=? AND `user_id`=?";
        connection.query(deleteSql, [job_id, fleet_id, user_id], function (err, result_delete_schedule) {
            cron.scheduleFleetNotification(user_id, job_id, fleet_id, job_time, job_time_utc, job_type, timezone);
        });

    });

}
/******
 ===================================
 DELETE TASK FOR FLEET NOTIFICATION
 ===================================
 *******/
exports.deleteScheduleFleetNotification = function (fleet_task_notify_id) {
    var deleteSql = "DELETE FROM `tb_fleet_task_notify` WHERE `fleet_task_notify_id`=? LIMIT 1";
    connection.query(deleteSql, [fleet_task_notify_id], function (err, result) {
    });
}

/******
 ===================================================
 DELETE TASK FOR FLEET NOTIFICATION AFTER TASK STARTS
 ===================================================
 *******/
exports.deleteScheduleFleetNotificationAfterTaskStart = function (job_id, fleet_id, user_id) {

    var sql = "SELECT * FROM `tb_fleet_task_notify` " +
        "WHERE `job_id`=? AND `fleet_id`=? AND `user_id`=?";
    connection.query(sql, [job_id, fleet_id, user_id], function (err, schedules) {
        if (schedules.length > 0) {
            var deleteSql = "DELETE FROM `tb_fleet_task_notify` WHERE `fleet_task_notify_id`=? LIMIT 1";
            connection.query(deleteSql, [schedules[0].fleet_task_notify_id], function (err, result) {
                try {
                    cron.deleteSchedule('schedule' + schedules[0].fleet_task_notify_id);
                } catch (e) {
                    console.log("========= Error in Cancelling  deleteScheduleFleetNotificationAfterTaskStart=======");
                    console.log(e);
                }
            });
        }
    });

}
/******
 ========================================
 GET ALL FLEET NOTIFICATIONS SET BY USER
 ========================================
 *******/
exports.getAllFleetNotifications = function (user_id, callback) {
    var sql = "SELECT `notification_text`,`minutes_before_task_starts` " +
        "FROM `tb_fleet_notifications` " +
        "WHERE `user_id` = ? AND `is_enabled`=1";
    connection.query(sql, [user_id], function (err, fleet_notification_result) {
        callback(fleet_notification_result)
    });
}

/******
 ==========================================
 INSERT DEFAULT FLEET NOTIFICATION TO USER
 ==========================================
 *******/
exports.insertDefaultFleetNotification = function (user_id) {
    var message = "Upcoming Task Reminder: [TIME] minutes left"
    var sql = "INSERT INTO tb_fleet_notifications (`notification_text`,`minutes_before_task_starts`,`user_id`) " +
        "VALUES (?,?,?) ";
    connection.query(sql, [message, 30, user_id], function (err, fleet_notification_result) {
    });
}

exports.deleteSchedule = function(scheduleIndex){
    if (schedule.scheduledJobs[scheduleIndex]) {
        schedule.scheduledJobs[scheduleIndex].cancel();
        delete schedule.scheduledJobs[scheduleIndex];
    }
}

/**
 * =================================
 SEND MAIL TO DEACTIVATED CUSTOMERS
 ===================================
 */
exports.sendMailTemplateToDeactivatedCustomers = function (req, res){
    var sql = "SELECT * FROM `tb_users` WHERE `is_dispatcher`=? AND `is_active`=? AND DATE(`expiry_datetime`)= DATE(NOW() - INTERVAL 1 DAY)";
    connection.query(sql, [constants.isDispatcherStatus.NO, constants.userActiveStatus.INACTIVE], function (err, non_active_customer) {
        if (non_active_customer.length > 0) {
            non_active_customer.forEach(function(users){
                if (users.has_mails_enabled) commonFuncMailer.send_template_mail(users.access_token, users.username, users.email, "fname-have-60-seconds-to-share-why", "");
            })
            responses.actionCompleteResponse(res);
        }else{
            responses.noDataFoundError(res);
        }
    });
}