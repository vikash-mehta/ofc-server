var commonFunc = require('./commonfunction');
var responses = require('./responses');

exports.heartbeat = function (req, res) {

    var sql = "SELECT *,(TIMESTAMPDIFF(MINUTE,`location_update_datetime`,NOW())) as diff_minutes " +
        " FROM `tb_fleets` " +
        " WHERE `registration_status` = ? " +
        " AND `device_token` <> '' " +
        " AND `is_deleted` = ? " +
        " AND `is_active` = ? ";
    connection.query(sql, [constants.userActiveStatus.ACTIVE, constants.userDeleteStatus.NO, constants.userActiveStatus.ACTIVE], function (err, fleets) {
        if (err) {
            responses.sendError(res);
            return;
        } else {
            if (fleets && fleets.length > 0) {
                var size = 1000, android_device_tokens = [], ios_tokens_content0 = [], ios_tokens_content1 = [], payload = {
                    flag: constants.notificationFlags.HEARTBEAT,
                    message: "heartbeat"
                };
                fleets.forEach(function (f) {

                    // Send Notification  to Android.
                    if (f.device_token && f.device_type == constants.deviceType.ANDROID && f.app_versioncode > 217) {
                        android_device_tokens.push(f.device_token);
                    }// Send Notification to iOS.
                    else if (f.device_token && f.device_type == constants.deviceType.iOS && f.device_token != 'deviceToken' && f.app_versioncode > 214) {
                        if (f.diff_minutes >= 2 && f.diff_minutes <= 3) {
                            ios_tokens_content0.push(f.device_token)
                        } else if (f.diff_minutes >= 5 && f.diff_minutes < 7) {
                            ios_tokens_content1.push(f.device_token)
                        } else if (f.diff_minutes >= 10 && f.diff_minutes < 12) {
                            ios_tokens_content1.push(f.device_token)
                        } else if (f.diff_minutes >= 20 && f.diff_minutes < 22) {
                            ios_tokens_content1.push(f.device_token)
                        }
                    }
                })
                // Send Bulk Notifications to Android Devices
                if (android_device_tokens && android_device_tokens.length) {
                    var arrays = [];
                    while (android_device_tokens.length > 0)
                        arrays.push(android_device_tokens.splice(0, size));
                    arrays.forEach(function (d_t) {
                        commonFunc.sendAndroidPushNotification(d_t, payload);
                    });
                }
                // Send Bulk Notifications to iOS Devices SILENT PUSH
                if (ios_tokens_content0 && ios_tokens_content0.length) {
                    var array = [];
                    while (ios_tokens_content0.length > 0)
                        array.push(ios_tokens_content0.splice(0, size));
                    array.forEach(function (d_t) {
                        commonFunc.sendIosPushNotification('', d_t, '', true, payload)
                    });
                }
                // Send Bulk Notifications to iOS Devices NOT_SILENT PUSH
                if (ios_tokens_content1 && ios_tokens_content1.length) {
                    var array1 = [];
                    while (ios_tokens_content1.length > 0)
                        array1.push(ios_tokens_content0.splice(0, size));
                    array1.forEach(function (d_t) {
                        commonFunc.sendIosPushNotification('ping.caf', d_t, '', false, payload)
                    });
                }
                responses.actionCompleteResponse(res);
            } else {
                responses.noDataFoundError(res);
            }
        }
    });
}