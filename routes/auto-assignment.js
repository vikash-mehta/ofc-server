var commonFunc = require('./commonfunction');
var jobs = require('./jobs');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var schedule = require('node-schedule');
var async = require('async');
var geolib = require('geolib');
var socketResponse = require('./socketResponses');
var validator = require('validator');
/*----------------------------
 AUTO ASSIGNMENT FUNCTION
 ----------------------------
 */
exports.auto_assign_task = function (data, callback) {
    var update = "UPDATE `tb_jobs` SET `auto_assignment`=? WHERE job_id = ? LIMIT 1";
    connection.query(update, [constants.autoAssign.YES, data.job_id], function (err, update_auto_assignment) {
        if (err) {
            console.log(err);
            console.log("Error in updating 1 in auto assignment field in tb_jobs", update_auto_assignment);
        }
        getAgents(data, function (getAllNearestAvailableAgentsResult) {
            var fleets_length = getAllNearestAvailableAgentsResult.length
            if (fleets_length > 0) {
                var fleet_ids = [], fleet_names = [];
                getAllNearestAvailableAgentsResult.forEach(function (fl) {
                    fleet_ids.push(fl.fleet_id);
                    fleet_names.push(fl.fleet_name);
                })
                if (data.broadcast_type == constants.broadcastType.SEND_TO_ALL) {
                    commonFunc.setTaskHistory(null, constants.taskHistoryType.TASK_PUSH_NOTIFY, data.job_id, "Sent Parallel Notification to : " + fleet_names.join(", "));
                    async.map(fleet_ids, function (fleet_id, callback) {
                        sendPush(data, fleet_id);
                        scheduleAutoAssignmentSocketResponse(data.job_id, commonFunc.addSeconds(new Date(), 300));
                        callback(null, true);
                    }, function (err, results) {
                        callback(null, true);
                    });
                } else if (data.broadcast_type == constants.broadcastType.ONE_BY_ONE) {
                    if (fleets_length > 0) {
                        if (data.send_push == 1) {
                            var schedule = "INSERT into `tb_schedules` (`user_id`,`accept_button`,`job_id`,`expires_in`,`fleets`,`enrolled_fleets`,`is_processed`,`broadcast_type`) " +
                                "  VALUES (?,?,?,?,?,?,?,?)";
                            connection.query(schedule, [data.user_id, data.accept_button, data.job_id, data.expires_in, fleet_ids.toString(),
                                fleet_ids.toString(), 0, data.broadcast_type], function (err, schedules) {
                                if (err) {
                                    console.log(err);
                                }
                                module.exports.processAgentsOneAtATime();
                                var time = ((fleets_length * data.expires_in) + 10);
                                scheduleAutoAssignmentSocketResponse(data.job_id, commonFunc.addSeconds(new Date(), time));
                                callback(null, true);
                            });
                        } else {
                            callback(null, true);
                        }
                    }
                } else {
                    callback(null, true);
                }
            } else {
                scheduleAutoAssignmentSocketResponse(data.job_id, commonFunc.addSeconds(new Date(), 10));
                callback(null, true);
            }
        });
    });
}
exports.processAgentsOneAtATime = function () {
    var schedule = "SELECT * FROM `tb_schedules` WHERE `is_processed` = 0";
    connection.query(schedule, function (err, schedules) {
        if (err) {
            console.log(err);
        }
        if (schedules.length > 0) {
            schedules.forEach(function (sch) {
                var fleets = [];
                fleets = sch.fleets.split(',');
                if (fleets.length > 0) {
                    sendPush(sch, fleets[0]);
                    fleets.shift();
                    if (fleets.length > 0) {
                        var schedule = "UPDATE `tb_schedules` SET `fleets`=? WHERE `schedule_id` = ? LIMIT 1";
                        connection.query(schedule, [fleets.toString(), sch.schedule_id], function (err, schedules) {
                            if (err) {
                                console.log(err);
                            }
                            var date = new Date();
                            var new_time = commonFunc.addSeconds(date, sch.expires_in);
                            scheduleNextAgent(fleets[0], fleets, sch.job_id, new_time, sch.expires_in, sch.accept_button, sch.broadcast_type);
                        });
                    } else {
                        var schedule = "UPDATE `tb_schedules` SET `fleets`=?,`is_processed` = 1 WHERE `schedule_id` = ? LIMIT 1";
                        connection.query(schedule, [null, sch.schedule_id], function (err, schedules) {
                            if (err) {
                                console.log(err);
                            }
                        });
                    }
                }
            })
        }
    })
}
function scheduleNextAgent(fleet_id, fleets, job_id, new_time, expires_in, accept_button, broadcast_type) {

    schedule.scheduleJob("schedule_auto_assign_" + job_id.toString() + "_" + fleet_id.toString(), new_time, function () {
        var inp = {
            job_id: job_id,
            expires_in: expires_in,
            accept_button: accept_button,
            broadcast_type: broadcast_type
        }
        //commonFunc.setTaskHistory(fleet_id, constants.taskHistoryType.TASK_PUSH_NOTIFY, job_id, "Sent Notification to Agent : " + fleet_id);
        sendPush(inp, fleet_id);
        if (fleets.length > 1) {
            fleets.shift();
            var schedule = "UPDATE `tb_schedules` SET `fleets`=? WHERE `job_id` = ? LIMIT 1";
            connection.query(schedule, [fleets.toString(), job_id], function (err, schedules) {
                if (err) {
                    console.log(err);
                }
                var date = new Date();
                var new_time = commonFunc.addSeconds(date, expires_in);
                scheduleNextAgent(fleets[0], fleets, job_id, new_time, expires_in, accept_button, broadcast_type);
            });
        } else {
            var schedule = "UPDATE `tb_schedules` SET `fleets`=?,`is_processed` = 1 WHERE `job_id` = ? LIMIT 1";
            connection.query(schedule, [null, job_id], function (err, schedules) {
                if (err) {
                    console.log(err);
                }
            });
        }
    });
}


function sendPush(input, fleet_id) {
    // Send a push notification to the fleet that a
    // new job has been created for him
    commonFunc.getJobDetailsFromJobID(input.job_id, function (getJobDetailsFromJobIDResult) {
        if (getJobDetailsFromJobIDResult.length > 0) {
            var message_fleet = 'A new task has been assigned.';
            var driverOfflineMessage = 'Hi [Fleet name], a new task has been assigned to you. Kindly log into app to see the details.';
            var data = getJobDetailsFromJobIDResult[0];
            if (data.has_pickup == constants.hasPickup.YES && data.has_delivery == constants.hasDelivery.YES && data.job_type == constants.jobType.DELIVERY) {
            } else {
                if (data.has_pickup == constants.hasPickup.YES && data.has_delivery == constants.hasDelivery.YES && data.job_type == constants.jobType.PICKUP) {
                    var sql = "SELECT `job_status`,`job_id`,`job_address` " +
                        "FROM `tb_jobs` " +
                        "WHERE `pickup_delivery_relationship`=? and `job_type` = ? and `job_status`<>?";
                    connection.query(sql, [data.pickup_delivery_relationship, constants.jobType.DELIVERY, constants.jobStatus.DELETED], function (err, deliver_address) {
                        if (deliver_address.length > 0) {
                            var payload_fleet = {
                                flag: constants.notificationFlags.JOB_ASSIGN,
                                message: message_fleet,
                                job_id: input.job_id,
                                job_type: data.job_type,
                                cust_name: data.job_type == constants.jobType.PICKUP ? data.job_pickup_name : data.customer_username,
                                start_address: data.job_pickup_address,
                                start_time: moment(data.job_pickup_datetime).format('YYYY-MM-DD HH:mm:ss'),
                                end_time: moment(data.job_delivery_datetime).format('YYYY-MM-DD HH:mm:ss'),
                                end_address: deliver_address[0].job_address,
                                accept_button: input.accept_button,
                                d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                                timer: input.broadcast_type == constants.broadcastType.ONE_BY_ONE ? input.expires_in : '-'
                            };
                            commonFunc.sendNotification(fleet_id, message_fleet, false, payload_fleet, driverOfflineMessage);
                        } else {

                        }
                    });
                } else {
                    var payload_fleet = {
                        flag: constants.notificationFlags.JOB_ASSIGN,
                        message: message_fleet,
                        job_id: input.job_id,
                        job_type: data.job_type,
                        cust_name: data.job_type == constants.jobType.PICKUP ? data.job_pickup_name : data.customer_username,
                        start_address: data.job_pickup_address,
                        start_time: moment(data.job_pickup_datetime).format('YYYY-MM-DD HH:mm:ss'),
                        end_time: moment(data.job_delivery_datetime).format('YYYY-MM-DD HH:mm:ss'),
                        end_address: data.job_address,
                        accept_button: input.accept_button,
                        d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
                        timer: input.broadcast_type == constants.broadcastType.ONE_BY_ONE ? input.expires_in : '-'
                    };
                    commonFunc.sendNotification(fleet_id, message_fleet, false, payload_fleet, driverOfflineMessage);
                }
            }
        }
    });
}
exports.cancelJobSchedule = function (job_id) {

    var sch = "SELECT * FROM tb_schedules WHERE `job_id` = ? LIMIT 1";
    connection.query(sch, [job_id], function (err, schedules) {
        if (err) {
            console.log(err);
        }
        if (schedules.length > 0) {
            var fleets = [];
            fleets = schedules[0].enrolled_fleets.split(',');
            fleets.forEach(function (f) {
                var scheduleIndex = "schedule_auto_assign_" + schedules[0].job_id.toString() + "_" + f.toString();
                if (schedule.scheduledJobs[scheduleIndex]) {
                    schedule.scheduledJobs[scheduleIndex].cancel();
                    delete schedule.scheduledJobs[scheduleIndex];
                }
            })
        }
    });
}
/*
 FIND ALL NEAREST AVAILABLE AGENTS DEPENDING UPON TASK TIME
 */
function getAgents(data, callback) {
    data.job_lat = data.has_pickup == constants.hasPickup.YES ? data.job_pickup_latitude : data.latitude;
    data.job_lng = data.has_pickup == constants.hasPickup.YES ? data.job_pickup_longitude : data.longitude;
    if (data.job_type == constants.jobType.PICKUP || data.job_type == constants.jobType.DELIVERY) {
        data.start_date = data.has_pickup == constants.hasPickup.YES ? data.job_pickup_datetime : data.job_delivery_datetime;
        data.start_date = commonFunc.subtractHOUR(data.start_date);
        data.end_date = commonFunc.addHOUR(data.start_date);
    } else {
        data.start_date = commonFunc.subtractHOUR(data.job_pickup_datetime);
        data.end_date = commonFunc.addHOUR(data.job_delivery_datetime);
    }
    var fleet_ids = [], final_fleets = [];
    commonFunc.getAllNearestAvailableAgents(data, function (getAllFleetsResult) {
        getAllFleetsResult.forEach(function (fleets) {

            var j = data.job_lat + "," + data.job_lng;
            var f = fleets.latitude + "," + fleets.longitude ;

            if (data.job_lat && data.job_lng && commonFunc.isValidPoints(j)
                && fleets.latitude && fleets.longitude && commonFunc.isValidPoints(f)){
                fleets.fleet_distance = geolib.getDistance({
                    latitude: parseFloat(data.job_lat),
                    longitude: parseFloat(data.job_lng)
                }, {
                    latitude: parseFloat(fleets.latitude),
                    longitude: parseFloat(fleets.longitude)
                });
                fleets.fleet_distance = geolib.convertUnit('km', fleets.fleet_distance, 2);
                fleets.sort = Math.round((fleets.fleet_distance * 0.60) + (fleets.tasks * 0.40))
                fleet_ids.push(fleets);
            }
        });
        fleet_ids = _lodash.sortByOrder(fleet_ids, ['sort'], ['asc']);
        fleet_ids.forEach(function (fleet) {
            final_fleets.push({
                "fleet_id": fleet.fleet_id,
                "fleet_name": fleet.username
            });
        })
        callback(final_fleets);
    });
}
function disableAutoAssign(job_id) {
    var sch = "SELECT * FROM tb_jobs WHERE `job_id`=? LIMIT 1";
    connection.query(sch, [job_id], function (err, job) {
        if (err) {
            console.log(err);
        }
        if (job.length > 0) {
            if (job[0].job_status == constants.jobStatus.UNASSIGNED) {
                deleteDisableAutoAssignSchedule(job_id);
                /* UPDATE tb_jobs  2 in auto assignment field*/
                var update = "UPDATE `tb_jobs` SET `auto_assignment`=? WHERE job_id = ? LIMIT 1";
                connection.query(update, [constants.autoAssign.NO_ONE_ACCEPTED, job_id], function (err, update_auto_assignment) {
                    if (err) {
                        console.log(err);
                        console.log("Error in updating 2 in auto assignment field in tb_jobs", update_auto_assignment);
                    }
                    socketResponse.sendSocketResponse(job_id);
                });
            }
        }
    });
}
function deleteDisableAutoAssignSchedule(job_id) {
    /* DELETE FROM tb_disable_auto_assignment */
    var deleteFromSchedule = "DELETE FROM `tb_disable_auto_assignment` WHERE job_id = ? LIMIT 1";
    connection.query(deleteFromSchedule, [job_id], function (err, deleteFromScheduleResult) {
        if (err) {
            console.log(err);
            console.log("Error in deleting from tb_disable_auto_assignment", deleteFromScheduleResult);
        }
    });
}
function scheduleAutoAssignmentSocketResponse(job_id, time) {
    commonFunc.getBothTasksFromSingleID(job_id, function (result) {
        if (result) {
            result.forEach(function (jobs) {
                var sch = "INSERT INTO `tb_disable_auto_assignment` (`job_id`,`fires_on`) VALUES (?,?) ";
                connection.query(sch, [jobs.job_id, time], function (err, insert_dis_auto_assign) {
                    if (err) {
                        console.log(err);
                    }
                    schedule.scheduleJob("schedule_disable_auto_assign_" + jobs.job_id.toString(), time, function () {
                        disableAutoAssign(jobs.job_id);
                    });
                });
            })
        }
    })
}
exports.scheduleAllPendingDisableAutoAssign = function () {
    var sch = "SELECT * FROM `tb_disable_auto_assignment` ";
    connection.query(sch, function (err, schedules) {
        if (err) {
            console.log(err);
        }
        schedules.forEach(function (sch) {
            console.log(new Date() + "=====" + sch.fires_on);
            schedule.scheduleJob("schedule_disable_auto_assign_" + sch.job_id.toString(), sch.fires_on, function () {
                disableAutoAssign(sch.job_id);
            });
        })
    });
}
exports.cancelDisableAutoAssignSchedule = function (job_id) {
    deleteDisableAutoAssignSchedule(job_id);
    var scheduleIndex = "schedule_disable_auto_assign_" + job_id.toString();
    if (schedule.scheduledJobs[scheduleIndex]) {
        schedule.scheduledJobs[scheduleIndex].cancel();
        delete schedule.scheduledJobs[scheduleIndex];
    }
}