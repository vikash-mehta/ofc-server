var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
/*
 * --------------------------
 * RESET NOTIFICATION COUNT
 * --------------------------
 */

exports.reset_notification_count = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "UPDATE `tb_users` SET `notification_count`=? WHERE `user_id`=? LIMIT 1";
                        connection.query(sql, [0, result[0].user_id], function (err, result_notifications_status) {
                            if (err) {
                                logging.logDatabaseQueryError("setting notification status as viewed : ", err, result_notifications_status);
                                responses.sendError(res);
                                return;
                            } else {
                                var sql = "SELECT `notification_id`,`notification_text`,`notification_title`,`creation_datetime`,`job_id` FROM `tb_notifications` WHERE `user_id`=? AND `notification_status`=?";
                                connection.query(sql, [result[0].user_id, constants.notificationStatus.NOT_VIEWED], function (err, result_notificaion) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in checking notifications information : ", err, result_notificaion);
                                        responses.sendError(res);
                                        return;
                                    } else {
                                        result_notificaion = commonFunc.sortByKeyDesc(result_notificaion, 'creation_datetime');
                                        result_notificaion = result_notificaion.slice(0, 30);
                                        result_notificaion.forEach(function (notifications) {
                                            notifications.job_delivery_datetime = moment(notifications.job_delivery_datetime).format('MMM Do, h:mm a');
                                            notifications.creation_datetime = moment(commonFunc.convertTimeIntoLocal(notifications.creation_datetime, result[0].timezone)).format('MMM Do, h:mm a');
                                        })
                                        var response = {
                                            "message": constants.responseMessages.ACTION_COMPLETE,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": {
                                                "notifications": result_notificaion
                                            }
                                        };
                                        res.send(JSON.stringify(response));
                                        return;
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};

/*
 * --------------------------
 * RESET NOTIFICATION STATUS
 * --------------------------
 */

exports.reset_notification_status = function (req, res) {

    var access_token = req.body.access_token;
    var notification_id = req.body.notification_id;
    var manvalues = [access_token, notification_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "UPDATE `tb_notifications` SET `notification_status`=?,`toaster_status`=? WHERE `notification_id`=? LIMIT 1";
                        connection.query(sql, [constants.notificationStatus.VIEWED, constants.notificationStatus.VIEWED, notification_id], function (err, result_notifications_status) {
                            if (err) {
                                logging.logDatabaseQueryError("setting notification status as viewed : ", err, result_notifications_status);
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                                return;
                            }
                        });
                    }
                });
            }
        });
    }
};

/*
 * --------------------------
 * CHECK NOTIFICATIONS
 * --------------------------
 */

exports.check_notifications = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var notification_count = result[0].notification_count;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "SELECT `notification_id`,`notification_text`,`notification_title`,`creation_datetime`,`job_id` FROM `tb_notifications` WHERE `user_id`=? AND `notification_status`=? AND `toaster_status`=?";
                        connection.query(sql, [user_id, constants.notificationStatus.NOT_VIEWED, constants.notificationStatus.NOT_VIEWED], function (err, result_notificaion) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in checking notifications information : ", err, result_notificaion);
                                responses.sendError(res);
                                return;
                            } else {
                                result_notificaion = commonFunc.sortByKeyDesc(result_notificaion, 'creation_datetime');
                                result_notificaion = result_notificaion.slice(0, 30);
                                result_notificaion.forEach(function (notifications) {
                                    notifications.job_delivery_datetime = moment(notifications.job_delivery_datetime).format('MMM Do, h:mm a');
                                    notifications.creation_datetime = moment(commonFunc.convertTimeIntoLocal(notifications.creation_datetime, result[0].timezone)).format('MMM Do, h:mm a');
                                })
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data": {
                                        "notifications": result_notificaion,
                                        "notification_count": notification_count
                                    }
                                };
                                res.send(JSON.stringify(response));
                                return;
                            }
                        });
                    }
                });
            }
        });
    }
};


/*
 * --------------------------
 * EMIT CHECK NOTIFICATIONS
 * --------------------------
 */

exports.emit_notifications = function (user_id, fleet_id, callback) {
    var sql = "SELECT `notification_id`,`notification_text`,`notification_title`,`creation_datetime`,`job_id` " +
        "FROM `tb_notifications` WHERE `user_id`=? AND `notification_status`=? AND `toaster_status`=? ";
    connection.query(sql, [user_id, constants.notificationStatus.NOT_VIEWED, constants.notificationStatus.NOT_VIEWED], function (err, result_notificaion) {
        if (err) {
            logging.logDatabaseQueryError("Error in checking notifications information : ", err, result_notificaion);
        } else {
            var response = {
                "message": constants.responseMessages.ACTION_COMPLETE,
                "status": constants.responseFlags.ACTION_COMPLETE,
                "data": {}
            }
            if (result_notificaion && result_notificaion.length > 0) {
                result_notificaion = commonFunc.sortByKeyDesc(result_notificaion, 'creation_datetime');
                response.data.notification_text = result_notificaion[0].notification_text;
                response.data.notification_title = result_notificaion[0].notification_title;
                response.data.notification_id = result_notificaion[0].notification_id;
            }
        }
        callback(response);
    });
}


/*
 * --------------------------
 * RESET TOASTER STATUS
 * --------------------------
 */

exports.reset_toaster_status = function (req, res) {

    var access_token = req.body.access_token;
    var notification_id = req.body.notification_id;
    var user_id = req.body.user_id;
    var manvalues = [access_token, notification_id, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var sql = "UPDATE `tb_notifications` SET `toaster_status`=? WHERE `user_id`=?";
                connection.query(sql, [constants.notificationStatus.VIEWED, user_id], function (err, result_notifications_status) {
                    if (err) {
                        logging.logDatabaseQueryError("setting toaster status as viewed : ", err, result_notifications_status);
                        responses.sendError(res);
                    } else {
                        responses.actionCompleteResponse(res);
                    }
                });
            }
        });
    }
};