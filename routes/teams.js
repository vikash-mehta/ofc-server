var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
/*
 * -------------
 * CREATE TEAMS
 * -------------
 */
exports.create_team = function (req, res) {
    var access_token = req.body.access_token;
    var team_name = req.body.team_name;
    var battery_usage = req.body.battery_usage;
    var fleet_ids = req.body.fleet_ids;
    var dispatcher_ids = req.body.dispatcher_ids;
    if (typeof fleet_ids === 'undefined') {
        fleet_ids = 0;
    }
    if (typeof dispatcher_ids === 'undefined') {
        dispatcher_ids = 0;
    }
    var manvalues = [access_token, team_name];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                module.exports.createTeam(result, team_name, battery_usage, fleet_ids, dispatcher_ids, function (createTeamResult) {
                    if (createTeamResult.fleet_not_assigned) {
                        createTeamResult.message = constants.responseMessages.FLEET_ALREADY_IN_OTHER_TEAM
                    }
                    res.send(JSON.stringify(createTeamResult));
                })
            }
        })
    }
}
exports.createTeam = function (result, team_name, battery_usage, fleet_ids, dispatcher_ids, callback) {
    var fleet_not_assigned = 0;
    commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
        if (checkAccountExpiryResult == 1) {
            var response = {
                "message": constants.responseMessages.ACCOUNT_EXPIRE,
                "status": constants.responseFlags.ACCOUNT_EXPIRE,
                "data": {
                    billing_popup: {
                        "skip_link": 0,
                        "show_billing_popup": 1,
                        "days_left": 0
                    }
                }
            }
            callback(response);
        } else {
            commonFunc.authenticateTeamNameWithUser(team_name, result[0].user_id, function (authenticateTeamNameWithUserResult) {
                if (authenticateTeamNameWithUserResult != 0) {
                    var response = {
                        "message": constants.responseMessages.TEAM_NAME_ALREADY_REGISTERED_WITH_YOU,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {}
                    };
                    callback(response);
                } else {
                    var UserID = result[0].user_id, userid = result[0].user_id;
                    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        UserID = result[0].dispatcher_user_id;
                        commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkPerm) {
                            if (chkPerm && chkPerm.length && chkPerm[0].create_team == constants.hasPermissionStatus.NO) {
                                var response = {
                                    "message": constants.responseMessages.INVALID_ACCESS,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {}
                                };
                                callback(response);
                            }
                            else {
                                step_in(UserID, userid);
                            }
                        })
                    } else {
                        step_in(UserID, userid);
                    }

                    function step_in(UserID, userid) {
                        var sql = "INSERT INTO `tb_teams` (`team_name`,`battery_usage`,`user_id`,`created_by`) VALUES (?,?,?,?)";
                        connection.query(sql, [team_name, battery_usage, UserID, userid], function (err, result_insert_teams) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in inserting teams info : ", err, result_insert_teams);
                                var response = {
                                    "message": constants.responseMessages.ERROR_IN_EXECUTION,
                                    "status": constants.responseFlags.ERROR_IN_EXECUTION,
                                    "data": {}
                                };
                                callback(response);
                            } else {
                                var team_id = result_insert_teams.insertId;
                                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                                    var sql = "INSERT INTO `tb_dispatcher_teams` (`team_id`,`user_id`,`dispatcher_id`) VALUES (?,?,?)";
                                    connection.query(sql, [team_id, UserID, userid], function (err, result_insert_dispatcher_teams) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in inserting dispatcher teams info : ", err, result_insert_dispatcher_teams);
                                            var response = {
                                                "message": constants.responseMessages.ERROR_IN_EXECUTION,
                                                "status": constants.responseFlags.ERROR_IN_EXECUTION,
                                                "data": {}
                                            };
                                            callback(response);
                                        } else {
                                            fleetInsertionfunction(fleet_ids);
                                        }
                                    });
                                }
                                else {
                                    fleetInsertionfunction(fleet_ids);
                                }
                                function fleetInsertionfunction(fleet_ids) {
                                    if (fleet_ids == 0) {
                                        dispatcherInsertionfunction(dispatcher_ids);
                                    } else {
                                        var fleet_ids_array = fleet_ids.split(','), fleet_ids_array_len = fleet_ids_array.length, index = 0;
                                        recall(index);
                                        function changeIndex() {
                                            index++;
                                            return recall(index);
                                        }

                                        function recall(item) {
                                            if (item > fleet_ids_array_len - 1) {
                                                dispatcherInsertionfunction(dispatcher_ids);
                                            } else {
                                                commonFunc.delay(item, function (i) {
                                                    (function (i) {
                                                        commonFunc.authenticateDeactivateFleetIdAndUserId(fleet_ids_array[i], userid, function (resultAuthenticateFleetIdAndUserId) {
                                                            if (resultAuthenticateFleetIdAndUserId != 0) {
                                                                commonFunc.getFleetIfAlreadyWithTeam(resultAuthenticateFleetIdAndUserId[0].fleet_id, userid, function (getFleetIfAlreadyWithTeamResult) {
                                                                    if (getFleetIfAlreadyWithTeamResult.data == 0) {
                                                                        var sql = "INSERT INTO `tb_fleet_teams` (`team_id`,`user_id`,`fleet_id`) VALUES (?,?,?)";
                                                                        connection.query(sql, [team_id, userid, getFleetIfAlreadyWithTeamResult.fleet_id], function (err, result_insert_fleet_teams) {
                                                                            if (err) {
                                                                                logging.logDatabaseQueryError("Error in inserting fleet teams info : ", err, result_insert_fleet_teams);
                                                                            }
                                                                            changeIndex();
                                                                        });
                                                                    } else {
                                                                        fleet_not_assigned = 1;
                                                                        changeIndex();
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    })(i);
                                                });
                                            }
                                        }
                                    }
                                }

                                function dispatcherInsertionfunction(dispatcher_ids) {
                                    if (dispatcher_ids == 0) {
                                        module.exports.getTeams(result[0].user_id, result[0].is_dispatcher, result[0].dispatcher_user_id, function (getTeamsResult) {
                                            getTeamsResult.fleet_not_assigned = fleet_not_assigned;
                                            callback(getTeamsResult);
                                        });
                                    } else {
                                        var dispatcher_ids_array = dispatcher_ids.split(','), dispatcher_ids_array_len = dispatcher_ids_array.length, index = 0;
                                        recallDisp(index);
                                        function changeIndexDisp() {
                                            index++;
                                            return recallDisp(index);
                                        }

                                        function recallDisp(item) {
                                            if (item > dispatcher_ids_array_len - 1) {
                                                module.exports.getTeams(result[0].user_id, result[0].is_dispatcher, result[0].dispatcher_user_id, function (getTeamsResult) {
                                                    getTeamsResult.fleet_not_assigned = fleet_not_assigned;
                                                    callback(getTeamsResult);
                                                });
                                            } else {
                                                commonFunc.delay(item, function (i) {
                                                    (function (i) {
                                                        commonFunc.authenticateDispatcherIdAndUserId(dispatcher_ids_array[i], userid, function (authenticateDispatcherIdAndUserIdResult) {
                                                            if (authenticateDispatcherIdAndUserIdResult != 0) {
                                                                var sql = "INSERT INTO `tb_dispatcher_teams` (`team_id`,`user_id`,`dispatcher_id`) VALUES (?,?,?)";
                                                                connection.query(sql, [team_id, userid, authenticateDispatcherIdAndUserIdResult[0].user_id], function (err, result_insert_fleet_teams) {
                                                                    if (err) {
                                                                        logging.logDatabaseQueryError("Error in inserting fleet teams info : ", err, result_insert_fleet_teams);
                                                                    }
                                                                    changeIndexDisp();
                                                                });
                                                            }
                                                            else {
                                                                changeIndexDisp();
                                                            }
                                                        });
                                                    })(i);
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    });
}
/*
 * --------------------------
 * UPDATE TEAMS
 * --------------------------
 */
exports.update_team = function (req, res) {
    var access_token = req.body.access_token;
    var team_name = req.body.team_name;
    var team_id = req.body.team_id;
    var battery_usage = req.body.battery_usage;
    var fleet_ids = req.body.fleet_ids;
    var dispatcher_ids = req.body.dispatcher_ids;
    if (typeof fleet_ids === 'undefined') {
        fleet_ids = 0;
    }
    if (typeof dispatcher_ids === 'undefined') {
        dispatcher_ids = 0;
    }
    var manvalues = [access_token, team_id, team_name];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var user_id = result[0].user_id;
                var dispatcher_user_id = result[0].dispatcher_user_id;
                var dispatcher_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            dispatcher_id = user_id;
                            user_id = dispatcher_user_id;
                            step_in(team_id, user_id);
                        } else {
                            step_in(team_id, user_id);
                        }
                        function step_in(team_id, user_id) {
                            commonFunc.authenticateUserIdAndTeamId(user_id, team_id, function (result) {
                                if (result == 0) {
                                    responses.invalidAccessError(res);
                                } else {
                                    var fleet_not_assigned = 0;
                                    var sql = "DELETE FROM `tb_fleet_teams` WHERE `team_id`=?";
                                    connection.query(sql, [team_id], function (err, result_delete_fleet_teams) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in deleting fleet teams info : ", err, result_delete_fleet_teams);
                                            responses.sendError(res);
                                            return;
                                        } else {
                                            var sql = "UPDATE `tb_teams` SET `team_name`=?,`battery_usage`=? WHERE `team_id`=? LIMIT 1";
                                            connection.query(sql, [team_name, battery_usage, team_id], function (err, result_update) {
                                                if (err) {
                                                    logging.logDatabaseQueryError("Error in updating teams info : ", err, result_update);
                                                    responses.sendError(res);
                                                    return;
                                                } else {
                                                    if (fleet_ids == 0) {
                                                        dispatcherInsertionfunction(dispatcher_ids);
                                                    } else {
                                                        var fleet_ids_array = fleet_ids.split(','), fleet_ids_array_len = fleet_ids_array.length, index = 0;
                                                        recall(index);
                                                        function changeIndex() {
                                                            index++;
                                                            return recall(index);
                                                        }

                                                        function recall(item) {
                                                            if (item > fleet_ids_array_len - 1) {
                                                                dispatcherInsertionfunction(dispatcher_ids);
                                                            } else {
                                                                commonFunc.delay(item, function (i) {
                                                                    (function (i) {
                                                                        commonFunc.authenticateDeactivateFleetIdAndUserId(fleet_ids_array[i], user_id, function (resultAuthenticateFleetIdAndUserId) {
                                                                            if (resultAuthenticateFleetIdAndUserId != 0) {
                                                                                commonFunc.getFleetIfAlreadyWithTeam(resultAuthenticateFleetIdAndUserId[0].fleet_id, user_id, function (getFleetIfAlreadyWithTeamResult) {
                                                                                    if (getFleetIfAlreadyWithTeamResult.data == 0) {
                                                                                        var sql = "INSERT INTO `tb_fleet_teams` (`team_id`,`user_id`,`fleet_id`) VALUES (?,?,?)";
                                                                                        connection.query(sql, [team_id, user_id, getFleetIfAlreadyWithTeamResult.fleet_id], function (err, result_insert_fleet_teams) {
                                                                                            if (err) {
                                                                                                logging.logDatabaseQueryError("Error in inserting fleet teams info : ", err, result_insert_fleet_teams);
                                                                                            }
                                                                                            changeIndex();
                                                                                        });
                                                                                    } else {
                                                                                        fleet_not_assigned = 1;
                                                                                        changeIndex();
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    })(i);
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    });

                                    function dispatcherInsertionfunction(dispatcher_ids) {
                                        if (dispatcher_ids == 0) {
                                            module.exports.getTeams(result[0].user_id, result[0].is_dispatcher, result[0].dispatcher_user_id, function (getTeamsResult) {
                                                getTeamsResult.fleet_not_assigned = fleet_not_assigned;
                                                if (getTeamsResult.fleet_not_assigned) {
                                                    getTeamsResult.message = constants.responseMessages.FLEET_ALREADY_IN_OTHER_TEAM
                                                }
                                                res.send(JSON.stringify(getTeamsResult));
                                            });
                                        } else {
                                            sql = "DELETE FROM `tb_dispatcher_teams` WHERE `team_id`=?";
                                            connection.query(sql, [team_id], function (err, result_delete_dispatcher_teams) {
                                                if (err) {
                                                    logging.logDatabaseQueryError("Error in deleting dispatcher teams info : ", err, result_delete_dispatcher_teams);
                                                    responses.sendError(res);
                                                    return;
                                                } else {
                                                    var dispatcher_ids_array = dispatcher_ids.split(','), dispatcher_ids_array_len = dispatcher_ids_array.length, index = 0;
                                                    recallDisp(index);
                                                    function changeIndexDisp() {
                                                        index++;
                                                        return recallDisp(index);
                                                    }

                                                    function recallDisp(item) {
                                                        if (item > dispatcher_ids_array_len - 1) {
                                                            module.exports.getTeams(user_id, result[0].is_dispatcher, result[0].dispatcher_user_id, function (getTeamsResult) {
                                                                getTeamsResult.fleet_not_assigned = fleet_not_assigned;
                                                                if (getTeamsResult.fleet_not_assigned) {
                                                                    getTeamsResult.message = constants.responseMessages.FLEET_ALREADY_IN_OTHER_TEAM
                                                                }
                                                                res.send(JSON.stringify(getTeamsResult));
                                                            });
                                                        } else {
                                                            commonFunc.delay(item, function (i) {
                                                                (function (i) {
                                                                    commonFunc.authenticateDispatcherIdAndUserId(dispatcher_ids_array[i], user_id, function (authenticateDispatcherIdAndUserIdResult) {
                                                                        if (authenticateDispatcherIdAndUserIdResult != 0) {
                                                                            var sql = "INSERT INTO `tb_dispatcher_teams` (`team_id`,`user_id`,`dispatcher_id`) VALUES (?,?,?)";
                                                                            connection.query(sql, [team_id, user_id, authenticateDispatcherIdAndUserIdResult[0].user_id], function (err, result_insert_fleet_teams) {
                                                                                if (err) {
                                                                                    logging.logDatabaseQueryError("Error in inserting fleet teams info : ", err, result_insert_fleet_teams);
                                                                                }
                                                                                changeIndexDisp();
                                                                            });
                                                                        }
                                                                        else {
                                                                            changeIndexDisp();
                                                                        }
                                                                    });
                                                                })(i);
                                                            });
                                                        }
                                                    }
                                                }
                                            });
                                        }
                                    }

                                }
                            });
                        }
                    }
                });
            }
        });
    }
};
/*
 * --------------------------
 * DELETE TEAMS
 * --------------------------
 */
exports.delete_team = function (req, res) {

    var access_token = req.body.access_token;
    var team_id = req.body.team_id;
    var is_force = req.body.is_force;
    if (typeof(is_force) === "undefined" || is_force === "") {
        is_force = 0;
    }
    var manvalues = [access_token, team_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                var dispatcher_user_id = result[0].dispatcher_user_id;
                var dispatcher_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            dispatcher_id = user_id;
                            user_id = dispatcher_user_id;
                            responses.invalidAccessError(res);
                        } else {
                            step_in(team_id, user_id);
                        }

                        function step_in(team_id, user_id) {
                            commonFunc.authenticateUserIdAndTeamId(user_id, team_id, function (authenticateUserIdAndTeamIdResult) {
                                if (authenticateUserIdAndTeamIdResult == 0) {
                                    responses.invalidAccessError(res);
                                } else {
                                    commonFunc.authenticateTeamIdWithFleetAndDispatcher(user_id, team_id, function (authenticateTeamIdWithFleetAndDispatcherResult) {
                                        if (authenticateTeamIdWithFleetAndDispatcherResult == 0 || is_force == 1) {

                                            var sql = "DELETE FROM `tb_teams` WHERE `team_id`=? LIMIT 1";
                                            connection.query(sql, [team_id], function (err, result_teams) {
                                                if (err) {
                                                    logging.logDatabaseQueryError("Error in deleting teams info : ", err, result_teams);
                                                    responses.sendError(res);
                                                    return;
                                                } else {
                                                    var sql = "UPDATE tb_jobs SET team_id = ? WHERE team_id = ? ";
                                                    connection.query(sql, [0, team_id], function (err, update_teams) {
                                                        if (err) {
                                                            logging.logDatabaseQueryError("Error in updating teams after deleting team : ", err, update_teams);
                                                            responses.sendError(res);
                                                            return;
                                                        } else {
                                                            module.exports.getTeams(user_id, result[0].is_dispatcher, result[0].dispatcher_user_id, function (getTeamsResult) {
                                                                res.send(JSON.stringify(getTeamsResult));
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            var message = constants.responseMessages.DELETE_TEAM_WARNING;
                                            message = message.replace(/<%FLEET%>/g, result[0].call_fleet_as);
                                            var response = {
                                                "message": message,
                                                "status": constants.responseFlags.SHOW_WARNING,
                                                "data": {}
                                            };
                                            res.send(JSON.stringify(response));
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};
exports.getTeams = function (user_id, is_dispatcher, dispatcher_user_id, callback) {
    commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
        if (checkAccountExpiryResult == 1) {
            var response = {
                "message": constants.responseMessages.ACCOUNT_EXPIRE,
                "status": constants.responseFlags.ACCOUNT_EXPIRE,
                "data": {
                    billing_popup: {
                        "skip_link": 0,
                        "show_billing_popup": 1,
                        "days_left": 0
                    }
                }
            }
            callback(response);
        } else {
            var sql = "SELECT teams.`battery_usage`,teams.`team_id`,teams.`team_name`,IF (fleets.`fleet_thumb_image` IS NULL,'',fleets.`fleet_thumb_image`) as `fleet_thumb_image`," +
                " IF (fleets.`fleet_image` IS NULL,'',fleets.`fleet_image`) as `fleet_image`,IF (fleets.`username` IS NULL,'',fleets.`username`) as `fleet_name`," +
                " fleets.`has_gps_accuracy`,fleets.`is_active`,fleets.`email`,fleets.`status`,fleets.`registration_status`,fleets.`is_available`,fleets.`latitude`,fleets.`longitude`, " +
                " TIMESTAMPDIFF(SECOND,fleets.`location_update_datetime`,NOW()) as `last_updated_location_time`,fleets.`phone`, " +
                " IF (fleets.`fleet_id` IS NULL, 0, fleets.`fleet_id`) as `fleet_id`, fleets.`is_deleted` FROM `tb_teams` teams LEFT JOIN `tb_fleet_teams` " +
                " fleet_teams ON fleet_teams.`team_id`= teams.`team_id` LEFT JOIN `tb_fleets` fleets ON fleet_teams.`fleet_id`=fleets.`fleet_id` " +
                " WHERE teams.`user_id`=? ";
            if (is_dispatcher == constants.isDispatcherStatus.YES) {
                commonFunc.checkDispatcherPermissions(user_id, function (chkPerm) {
                    if (chkPerm && chkPerm.length && chkPerm[0].view_team == constants.hasPermissionStatus.NO) {
                        sql = "SELECT teams.`battery_usage`,teams.`team_id`,teams.`team_name`,IF (fleets.`fleet_thumb_image` IS NULL,'',fleets.`fleet_thumb_image`) as `fleet_thumb_image`, " +
                            " IF (fleets.`fleet_image` IS NULL,'',fleets.`fleet_image`) as `fleet_image`,IF (fleets.`username` IS NULL,'',fleets.`username`) as `fleet_name`," +
                            " fleets.`has_gps_accuracy`,fleets.`is_active`,fleets.`email`,fleets.`status`,fleets.`registration_status`,fleets.`is_available`,fleets.`latitude`,fleets.`longitude`, " +
                            " TIMESTAMPDIFF(SECOND,fleets.`location_update_datetime`,NOW()) as `last_updated_location_time`,fleets.`phone`, " +
                            " IF (fleets.`fleet_id` IS NULL, 0, fleets.`fleet_id`) as `fleet_id`, fleets.`is_deleted` FROM `tb_dispatcher_teams` dis_teams LEFT JOIN `tb_teams` " +
                            " teams ON dis_teams.`team_id`= teams.`team_id` LEFT JOIN `tb_fleet_teams` fleet_teams ON fleet_teams.`team_id`=teams.`team_id` " +
                            " LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`=fleet_teams.`fleet_id` " +
                            " WHERE dis_teams.`dispatcher_id`=?";
                        step_in(sql, user_id);
                    }
                    else {
                        step_in(sql, dispatcher_user_id);
                    }
                })
            } else {
                step_in(sql, user_id);
            }

            function step_in(sql, user, is_deleted) {
                connection.query(sql, [user, is_deleted], function (err, result_teams) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in viewing teams info : ", err, result_teams);
                        var response = {
                            "message": constants.responseMessages.ERROR_IN_EXECUTION,
                            "status": constants.responseFlags.ERROR_IN_EXECUTION,
                            "data": {}
                        };
                        callback(response);
                    } else {
                        if (result_teams.length > 0) {
                            var teams = {},teamArray = [];
                            result_teams.forEach(function (team) {
                                if (teams[team.team_id]) {
                                    var fleet = {};
                                    if (team.fleet_id != 0 && team.is_deleted == 0) {
                                        fleet.fleet_id = team.fleet_id;
                                        fleet.fleet_name = team.fleet_name;
                                        fleet.phone = team.phone;
                                        fleet.fleet_image = team.fleet_image;
                                        fleet.fleet_thumb_image = team.fleet_thumb_image;
                                        fleet.is_deleted = team.is_deleted;
                                        fleet.status = team.status;
                                        fleet.email = team.email;
                                        fleet.is_available = team.is_available;
                                        fleet.is_active = team.is_active;
                                        fleet.latitude = team.latitude;
                                        fleet.longitude = team.longitude;
                                        fleet.last_updated_location_time = team.last_updated_location_time;
                                        team.last_updated_location_time = parseInt(team.last_updated_location_time);
                                        if (team.last_updated_location_time.toString() == "NaN") {
                                            fleet.last_updated_timings = constants.highest.VALUE;
                                            fleet.last_updated_location_time = 'Not updated.';
                                        } else {
                                            fleet.last_updated_timings = team.last_updated_location_time;
                                            fleet.last_updated_location_time = commonFunc.timeDifferenceInWords(team.has_gps_accuracy, team.is_available, team.last_updated_location_time);
                                        }
                                        fleet.registration_status = team.registration_status;
                                        teams[team.team_id].fleets.push(fleet);
                                    }
                                } else {
                                    teams[team.team_id] = {};
                                    teams[team.team_id].team_id = team.team_id;
                                    teams[team.team_id].team_name = team.team_name;
                                    teams[team.team_id].battery_usage = team.battery_usage;
                                    teams[team.team_id].fleets = []
                                    var fleet = {};
                                    if (team.fleet_id != 0 && team.is_deleted == 0) {
                                        fleet.fleet_id = team.fleet_id;
                                        fleet.fleet_name = team.fleet_name;
                                        fleet.fleet_image = team.fleet_image;
                                        fleet.phone = team.phone;
                                        fleet.fleet_thumb_image = team.fleet_thumb_image;
                                        fleet.is_deleted = team.is_deleted;
                                        fleet.status = team.status;
                                        fleet.email = team.email;
                                        fleet.is_available = team.is_available;
                                        fleet.is_active = team.is_active;
                                        fleet.latitude = team.latitude;
                                        fleet.longitude = team.longitude;
                                        fleet.last_updated_location_time = team.last_updated_location_time;
                                        team.last_updated_location_time = parseInt(team.last_updated_location_time);
                                        if (team.last_updated_location_time.toString() == "NaN") {
                                            fleet.last_updated_timings = constants.highest.VALUE;
                                            fleet.last_updated_location_time = 'Not updated.';
                                        } else {
                                            fleet.last_updated_timings = team.last_updated_location_time;
                                            fleet.last_updated_location_time = commonFunc.timeDifferenceInWords(team.has_gps_accuracy, team.is_available, team.last_updated_location_time);
                                        }
                                        fleet.registration_status = team.registration_status;
                                        teams[team.team_id].fleets.push(fleet)
                                    }
                                }
                            });
                            for (var key in teams) {
                                teamArray.push(teams[key]);
                            }
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": teamArray
                            };
                            callback(response);
                        } else {
                            var response = {
                                "message": constants.responseMessages.NO_TEAMS_AVAILABLE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {}
                            };
                            callback(response);
                        }
                    }
                });
            }
        }
    });
}
/*
 * ------------
 * VIEW TEAMS
 * ------------
 */
exports.view_team = function (req, res) {
    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                module.exports.getTeams(result[0].user_id, result[0].is_dispatcher, result[0].dispatcher_user_id, function (getTeamResult) {
                    res.send(JSON.stringify(getTeamResult));
                });
            }
        });
    }
};