var commonFunc = require('./commonfunction');
var responses = require('./responses');
var jobs = require('./jobs');
var mongo = require('./mongo');
var moment = require('moment');
var Routific = require("routific");
var async = require("async");
var request = require('request');
/*
 * ------------------------
 * SAMPLE ROUTE MANAGEMENT
 * ------------------------
 */

exports.route_management = function (req, res) {

// Load the demo data. https://routific.com/demo.json
    var data = require('./routificJson.json')
    var client = new Routific.Client({token: config.get('routific.token')});
    var vrp = new Routific.Pdp();

// Insert all visits from the demo.json
    for (var visitID in data.visits) {
        vrp.addVisit(visitID, data.visits[visitID]);
    }

// Insert all vehicles from the demo.json
    for (var vehicleID in data.fleet) {
        vrp.addVehicle(vehicleID, data.fleet[vehicleID]);
    }
    vrp.data.options = {
        polylines: true
    }
// Process the route
    client.route(vrp, function (error, solution) {
        if (error) throw error
        //console.log(JSON.stringify(solution, null, 2));
        res.send(JSON.stringify(solution, null, 2));
    });
};
/*
 * ------------------------
 *  ROUTE MANAGEMENT
 * ------------------------
 */
exports.manage_route = function (req, res) {
    var access_token = req.body.access_token,
        user_id = req.body.user_id,
        date = req.body.date,
        routed_comp_tasks = req.body.routed_comp_tasks,
        data = JSON.parse(req.body.data),
        manual = req.body.manual,
        team_id = req.body.team_id;
    if (typeof manual === 'undefined' || manual === '') {
        manual = 0;
    }
    if (typeof team_id === 'undefined' || team_id === '') {
        team_id = 0;
    }
    var manvalues = [access_token, user_id, data, date];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                if (result[0].has_routing) {
                    var client = new Routific.Client({token: config.get('routific.token')}),
                        routing = new Routific.Vrp();
                    if (result[0].layout_type == constants.layoutType.PICKUP_AND_DELIVERY) {
                        routing = new Routific.Pdp();
                    }
                    // Insert all jobs from data
                    for (var visitID in data.visits) {
                        routing.addVisit(visitID, data.visits[visitID]);
                    }
                    // Insert all vehicles from data
                    for (var vehicleID in data.fleet) {
                        routing.addVehicle(vehicleID, data.fleet[vehicleID]);
                    }
                    routing.data.options = {
                        polylines: true
                    }
                    // Process the route
                    client.route(routing, function (error, solution) {
                        console.log(solution, error)
                        var routific_result = (JSON.parse(JSON.stringify(solution || {})))
                        if (error) {
                            var response = {
                                "message": replaceStringsInError(result, error.toString()),
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send((response));
                            return;
                        } else {
                            var target, sql_fleet_tasks = [], mongo_fleet_tasks = {};
                            async.waterfall([
                                    function (cb) {
                                        for (target in solution.solution) {
                                            if (solution.solution.hasOwnProperty(target)) {
                                                solution.solution[target].splice(0, 1);

                                                mongo_fleet_tasks = mongo_fleet_tasks || {};
                                                mongo_fleet_tasks[target] = mongo_fleet_tasks[target] || {};
                                                mongo_fleet_tasks[target]['tasks'] = mongo_fleet_tasks[target]['tasks'] || [];

                                                solution.solution[target].forEach(function (val, index) {
                                                    if (val.location_name && val.location_name != '@bolt-santa-fake-gift') {
                                                        // Insert tasks updation w.r.t. fleet_id and job_id in mysql
                                                        sql_fleet_tasks.push({
                                                            fleet_id: target,
                                                            job_id: val.location_name,
                                                            notify: 0,
                                                            team_id: team_id
                                                        });
                                                        // Insert tasks array w.r.t. fleet_id in mongo
                                                        mongo_fleet_tasks[target]['tasks'].push(val.location_name);
                                                    }
                                                });
                                                if (sql_fleet_tasks.length) sql_fleet_tasks[sql_fleet_tasks.length - 1].notify = 1;
                                            }
                                        }
                                        cb(null, solution.solution);
                                    },
                                    function (da, cb) {
                                        // Polylines
                                        for (target in solution.polylines) {
                                            if (solution.polylines.hasOwnProperty(target)) {

                                                mongo_fleet_tasks[target] = mongo_fleet_tasks[target] || {};
                                                mongo_fleet_tasks[target]['polylines'] = mongo_fleet_tasks[target]['polylines'] || [];

                                                solution.polylines[target].forEach(function (val, index) {
                                                    if (val) mongo_fleet_tasks[target]['polylines'].push(val);
                                                });
                                            }
                                        }
                                        cb(null, solution);
                                    }],
                                function (err, success) {
                                    if (err) {
                                        responses.authenticationError(res);
                                        return;
                                    } else {
                                        //Insert Or Update Routed Data into DB
                                        putDataFromRoutificToDB({
                                            solution: routific_result,
                                            user_id: result[0].user_id,
                                            access_token: result[0].access_token,
                                            layout_type: result[0].layout_type,
                                            date: date,
                                            routed: mongo_fleet_tasks,
                                            fleet_tasks: sql_fleet_tasks,
                                            manual: manual,
                                            routed_comp_tasks: routed_comp_tasks,
                                            polylines: 1,
                                            unserved: {
                                                num_unserved: routific_result.num_unserved,
                                                unserved: routific_result.unserved
                                            }
                                        }, function (resultData) {
                                            if (resultData) {
                                                var response = {
                                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                                    "data": resultData.solution
                                                };
                                                res.send(JSON.stringify(response));
                                            } else {
                                                responses.authenticationError(res);
                                            }
                                        });
                                    }
                                });

                        }
                    });
                } else {
                    responses.invalidAccessError(res);
                }
            }
        })
    }
}
//function to send data to our database after getting from Routific.
function putDataFromRoutificToDB(data, callback) {
    async.parallel(
        [
            function (cb) {
                mongo.insertOrUpdateRoutedTasks(data, function (c) {
                    cb(null, c);
                })
            },
            function (cb) {
                commonFunc.updateRoutedStatusForTasks(data, function (c) {
                    cb(null, c);
                })
            },
            function (cb) {
                commonFunc.updateCompletedRoutedTasks(data, function (c) {
                    cb(null, c);
                })
            },
            function (cb) {
                commonFunc.updateUnServedTasksFromRoutific(data, function (c) {
                    cb(null, c);
                })
            }],
        function (err, result) {
            if (err) {
                callback(false)
            } else if (result && result[0] && result[1] && result[2] && result[3]) {
                callback(data)
            } else {
                callback(false)
            }
        });
}
/*
 *
 * THIS API IS FOR MANUAL ROUTING
 *
 */
exports.manual_route = function (req, res) {
    var access_token = req.body.access_token,
        user_id = req.body.user_id,
        date = req.body.date,
        fleet_id = req.body.fleet_id,
        jobs = req.body.solution,
        data = req.body.data;
    var manvalues = [access_token, user_id, jobs, date, fleet_id, data];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                if (result[0].has_routing) {
                    async.waterfall([
                        function (cb) {
                            var sql = "SELECT * FROM `tb_jobs` WHERE `job_id` IN (" + jobs.toString() + ") ORDER BY FIELD(`job_id`, " + jobs.toString() + " ) ";
                            connection.query(sql, function (err, tasks) {
                                if (err) {
                                    cb(null, -2);
                                } else {
                                    if (tasks && tasks.length) {
                                        var dump_tasks = {}, flag = 1;
                                        tasks.forEach(function (task, index) {
                                            dump_tasks[task.pickup_delivery_relationship] = dump_tasks[task.pickup_delivery_relationship] || [];
                                            dump_tasks[task.pickup_delivery_relationship].push({
                                                job_id: task.job_id,
                                                job_type: task.job_type,
                                                index: index
                                            });
                                            if (dump_tasks[task.pickup_delivery_relationship].length == 2) {
                                                if (dump_tasks[task.pickup_delivery_relationship][0] &&
                                                    dump_tasks[task.pickup_delivery_relationship][0]['job_type'] == constants.jobType.DELIVERY) {
                                                    flag = 0;
                                                }
                                            }
                                        })
                                        if (flag == 0) {
                                            cb(null, -1);
                                        } else {
                                            cb(null, 1);
                                        }
                                    } else {
                                        cb(null, -2);
                                    }
                                }
                            });
                        },
                        function (up, cb) {
                            if (up == -1) {
                                cb(null, -1);
                            } else if (up == -2) {
                                cb(null, -2);
                            } else {
                                request({
                                    url: "https://api.routific.com/v1/min-idle",
                                    method: "POST",
                                    headers: {
                                        'content-type': 'application/json',
                                        'Authorization': 'bearer ' + config.get('routific.token')
                                    },
                                    json: true,
                                    body: data
                                }, function (error, response, body) {
                                    cb(error, body);
                                });
                            }
                        }
                    ], function (error, solution) {
                        if (solution == -1) {
                            var response = {
                                "message": constants.responseMessages.DELIVERY_NOT_FIRST,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        } else if (solution == -2) {
                            var response = {
                                "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        } else {
                            console.log(error, solution)
                            var routific_result = (JSON.parse(JSON.stringify(solution || {})))
                            if (error) {
                                var response = {
                                    "message": replaceStringsInError(result, error.toString()),
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {}
                                };
                                res.send((response));
                                return;
                            } else {
                                var target, sql_fleet_tasks = [], mongo_fleet_tasks = {};
                                async.waterfall([
                                        function (cb) {
                                            for (target in solution.solution) {
                                                if (solution.solution.hasOwnProperty(target)) {
                                                    solution.solution[target].splice(0, 1);
                                                    solution.solution[target].forEach(function (val, index) {
                                                        if (val.location_name != '@bolt-santa-fake-gift') {
                                                            // Insert tasks updation w.r.t. fleet_id and job_id in mysql
                                                            sql_fleet_tasks.push({
                                                                fleet_id: target,
                                                                job_id: val.location_name,
                                                                notify: 0
                                                            });
                                                            // Insert tasks array w.r.t. fleet_id in mongo
                                                            mongo_fleet_tasks = mongo_fleet_tasks || {};
                                                            mongo_fleet_tasks[target] = mongo_fleet_tasks[target] || {};
                                                            mongo_fleet_tasks[target]['tasks'] = mongo_fleet_tasks[target]['tasks'] || [];
                                                            mongo_fleet_tasks[target]['tasks'].push(val.location_name);
                                                        }
                                                    });
                                                    if (sql_fleet_tasks.length) sql_fleet_tasks[sql_fleet_tasks.length - 1].notify = 1;
                                                }
                                            }
                                            cb(null, solution.solution);
                                        },
                                        function (da, cb) {
                                            // Polylines
                                            for (target in solution.polylines) {
                                                if (solution.polylines.hasOwnProperty(target)) {
                                                    solution.polylines[target].forEach(function (val, index) {
                                                        mongo_fleet_tasks[target] = mongo_fleet_tasks[target] || {};
                                                        mongo_fleet_tasks[target]['polylines'] = mongo_fleet_tasks[target]['polylines'] || [];
                                                        mongo_fleet_tasks[target]['polylines'].push(val);
                                                    });
                                                }
                                            }
                                            cb(null, solution);
                                        }],
                                    function (err, success) {
                                        if (err) {
                                            responses.authenticationError(res);
                                            return;
                                        } else {
                                            //Insert Or Update Routed Data into DB
                                            putDataFromRoutificToDB({
                                                solution: routific_result,
                                                user_id: result[0].user_id,
                                                access_token: result[0].access_token,
                                                layout_type: result[0].layout_type,
                                                date: date,
                                                routed: mongo_fleet_tasks,
                                                fleet_tasks: sql_fleet_tasks,
                                                manual: 0,
                                                polylines: 1,
                                                unserved: {
                                                    num_unserved: routific_result.num_unserved,
                                                    unserved: routific_result.unserved
                                                }
                                            }, function (resultData) {
                                                if (resultData) {
                                                    var response = {
                                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                                        "data": resultData.solution
                                                    };
                                                    res.send(JSON.stringify(response));
                                                    return;
                                                } else {
                                                    responses.authenticationError(res);
                                                }
                                            });
                                        }
                                    });
                            }
                        }
                    });
                } else {
                    responses.invalidAccessError(res);
                }
            }
        });
    }
}

function replaceStringsInError(data, msg) {
    msg = msg.replaceAll('visits', 'tasks');
    msg = msg.replaceAll('vehicles', data[0].call_fleet_as);
    msg = msg.replaceAll('drivers', data[0].call_fleet_as);
    msg = msg.replaceAll('Job undefined not found!', 'Apologies, we are having some trouble with optimization. Please try once again.');
    msg = msg.replaceAll('Error:', '');
    return msg;
}
String.prototype.replaceAll = function (target, replacement) {
    return this.split(target).join(replacement);
};
/*
 * ------------------------
 * UPDATE ROUTING FOR USER
 * ------------------------
 */
exports.enable_routing = function (req, res) {
    var access_token = req.body.access_token;
    var user_id = req.body.user_id;
    var manvalues = [access_token, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        if (access_token == 'tookan123!@#') {
            var sql = "UPDATE `tb_users` SET `has_routing`=1 WHERE `user_id`=? || `dispatcher_user_id`=?";
            connection.query(sql, [user_id, user_id], function (err, update) {
                if (err) {
                    responses.sendError(res);
                } else {
                    responses.actionCompleteResponse(res);
                }
            });
        } else {
            responses.invalidAccessError(res);
        }
    }
}