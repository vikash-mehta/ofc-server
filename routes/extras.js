var commonFunc = require('./commonfunction');
var responses = require('./responses');
var mongo = require('./mongo');
var moment = require('moment');
/*
 * --------------------------------------------------
 * ADD TEMPLATES OF USER
 * --------------------------------------------------
 */
exports.add_templates = function (req, res) {
    var email = req.body.email;
    if (typeof email === "undefined" || email === '') {
        email = '';
    }
    commonFunc.getAllUserOfSystem(email, function (all_users) {
        if (all_users == 0) {
            responses.noDataFoundError(res);
        } else {
            var all_users_length = all_users.length;
            var counter = 0;
            for (var i = 0; i < all_users_length; i++) {

                (function (i) {
                    commonFunc.insertTemplateInformation(all_users[i].user_id);
                    counter++;
                    sendResponse(counter, all_users_length);
                })(i);
            }
            function sendResponse(counter, all_users_length) {
                if (counter == all_users_length) {
                    responses.actionCompleteResponse(res);
                    return;
                }
            }
        }
    })
};

/*
 * -------------------
 * SEND TEMPLATE MAIL
 * -------------------
 */

exports.send_template_mail = function (req, res) {
    var mandrill = require('mandrill-api/mandrill');
    var m = new mandrill.Mandrill(config.get('emailCredentials.senderPassword'));
    var params = {
        "template_name": "weekly-analytics-report",
        "template_content": [
            {
                "name": "weekly-analytics-report",
                "content": "Weekly Analytics Report"
            }
        ],
        "message": {
            "global_merge_vars": [{
                "name": "ABCD",
                "content": "Sumeet"
            }],
            "from_email": "contact@tookanapp.com",
            "to": [{"email": "sumeet@clicklabs.in"}],
            "subject": "Weekly Analytics Report"
        }
    }
    m.messages.sendTemplate(params, function (res) {
        console.log(res);
    }, function (err) {
        console.log(err);
    });
    responses.actionCompleteResponse(res);
    return;
};


exports.checking_hook = function (req, res) {
    console.log("checking hook", req.body);
    responses.actionCompleteResponse(res);
}

exports.setFleetAvgRating = function (req, res) {
    var sql = "SELECT * FROM `tb_fleets` WHERE `is_active` = 1 AND `is_deleted` = 0 and `registration_status` = 1";
    connection.query(sql, function (err, fleets) {
        if (fleets.length > 0) {

            var index = 0;
            insertAvgRating(index);
            function changeIndex() {
                index++;
                return insertAvgRating(index);
            }

            function insertAvgRating(item) {
                if (item > fleets.length - 1) {
                    responses.actionCompleteResponse(res);
                } else {
                    commonFunc.delay2(item, function (i) {
                        (function (i) {
                            var sql = "SELECT IF (sum(fleet_rating) IS NULL,0, sum(fleet_rating)) as sum, count(*) as total FROM `tb_jobs` " +
                                " WHERE `fleet_id`=? and `is_customer_rated`=1";
                            connection.query(sql, [fleets[i].fleet_id], function (err, result) {
                                if (result.length > 0) {
                                    var sql = "UPDATE tb_fleets set total_rating = ?, total_rated_tasks = ? where fleet_id = ?";
                                    connection.query(sql, [result[0].sum, result[0].total, fleets[i].fleet_id], function (err, result) {
                                        changeIndex();
                                    });
                                }
                            });
                        })(i);
                    });
                }
            }
        }
    });
}


exports.checkGoogleShortenUrl = function (req, res) {
    var GoogleUrl = require('google-url');
    var googleUrl = new GoogleUrl({key: 'AIzaSyA2wlFJLpUnB06yImZ0f8F4bSKO_erP9h8'});
    googleUrl.shorten(req.body.data, function (err, shortUrl) {
        console.log(shortUrl);
    });
}

exports.customerOrientedFunction = function (user_id, template_key, layout_type, to_email, to_phone, jobID) {
    if (layout_type == 1) {
        var sql = "SELECT job.* FROM `tb_jobs` job  " +
            "WHERE job.job_id =? LIMIT 1";
        connection.query(sql, [jobID], function (err, result) {
            if (result.length > 0) {
                var job = result[0];
                commonFunc.authenticateUserIdAndJobToken(job.user_id, job.pickup_delivery_relationship, function (data) {
                    if (data.length > 1) {
                        var p_job_id = data[0].job_id, d_job_id = data[1].job_id, counter = 0, total_time = 0, total_distance = 0;
                        data.forEach(function (da) {
                            if (da && (da.started_datetime != "0000-00-00 00:00:00") && (da.completed_datetime != "0000-00-00 00:00:00")) {
                                var started_time = new Date(da.started_datetime);
                                var completed_datetime = new Date(da.completed_datetime);
                                total_time = total_time + (completed_datetime.getTime() - started_time.getTime());
                            }
                            total_distance = total_distance + da.total_distance_travelled;
                        })
                        total_time = commonFunc.millisecondsToStr(total_time);
                        if (total_distance){
                            total_distance = (total_distance/1000).toFixed(2) + " KM";
                        }
                        commonFunc.getTemplateMessage(user_id, template_key, layout_type, function (getTemplateMessageResult) {

                            if (getTemplateMessageResult != 0) {

                                var sms_text = 'Disabled', email_subject = 'Disabled', email_text = 'Disabled', user_email = 'Disabled', hook = 'Disabled';
                                if (getTemplateMessageResult[0].sms_enabled == 1) {
                                    sms_text = getTemplateMessageResult[0].sms_text;
                                }
                                if (getTemplateMessageResult[0].hook_enabled == 1) {
                                    hook = getTemplateMessageResult[0].hook;
                                }
                                if (getTemplateMessageResult[0].email_enabled == 1) {
                                    email_text = getTemplateMessageResult[0].email_text;
                                    email_subject = getTemplateMessageResult[0].email_subject;
                                    user_email = getTemplateMessageResult[0].email;
                                }
                                commonFunc.getTemplateVariables(getTemplateMessageResult[0].template_id, function (getTemplateVariablesResult) {
                                    var getTemplateVariablesResultLength = getTemplateVariablesResult.length;
                                    for (var i = 0; i < getTemplateVariablesResultLength; i++) {
                                        var variable_name = getTemplateVariablesResult[i].variable_name;
                                        switch (variable_name) {
                                            case '[P-CustomerName]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getCustomerDetailsFromJobIDResult) {
                                                    if (getCustomerDetailsFromJobIDResult && getCustomerDetailsFromJobIDResult[0].customer_username) {
                                                        getCustomerDetailsFromJobIDResult[0].customer_username = getCustomerDetailsFromJobIDResult[0].customer_username.split(' ')[0];
                                                        email_text = email_text.replace(/\[P-CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                                        email_subject = email_subject.replace(/\[P-CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                                        sms_text = sms_text.replace(/\[P-CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-Date]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_time) {
                                                        var job_date = moment(getJobDetailsFromJobIDResult[0].job_time).format('MMM Do');
                                                        email_text = email_text.replace(/\[P-Date]/g, job_date);
                                                        email_subject = email_subject.replace(/\[P-Date]/g, job_date);
                                                        sms_text = sms_text.replace(/\[P-Date]/g, job_date);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-CompletedDate]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].completed_datetime) {
                                                        var timezone = getJobDetailsFromJobIDResult[0].timezone;
                                                        var today = moment(new Date()).format('YYYY-MM-DD HH:m:s');
                                                        today = commonFunc.convertTimeIntoLocal(today, timezone);
                                                        var completed_datetime = moment(today).format('MMM Do');
                                                        email_text = email_text.replace(/\[P-CompletedDate]/g, completed_datetime);
                                                        email_subject = email_subject.replace(/\[P-CompletedDate]/g, completed_datetime);
                                                        sms_text = sms_text.replace(/\[P-CompletedDate]/g, completed_datetime);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-CompletedTime]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].completed_datetime) {
                                                        var timezone = getJobDetailsFromJobIDResult[0].timezone;
                                                        var today = moment(new Date()).format('YYYY-MM-DD HH:m:s');
                                                        today = commonFunc.convertTimeIntoLocal(today, timezone);
                                                        var completed_datetime = moment(today).format('h:mm a');
                                                        email_text = email_text.replace(/\[P-CompletedTime]/g, completed_datetime);
                                                        email_subject = email_subject.replace(/\[P-CompletedTime]/g, completed_datetime);
                                                        sms_text = sms_text.replace(/\[P-CompletedTime]/g, completed_datetime);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-CustomerAddress]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getCustomerDetailsFromJobIDResult) {
                                                    if (getCustomerDetailsFromJobIDResult && getCustomerDetailsFromJobIDResult[0].job_address) {
                                                        email_text = email_text.replace(/\[P-CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                                        email_subject = email_subject.replace(/\[P-CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                                        sms_text = sms_text.replace(/\[P-CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-StartTime]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_pickup_datetime && getJobDetailsFromJobIDResult[0].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                        var start_time = moment(getJobDetailsFromJobIDResult[0].job_pickup_datetime).format('h:mm a');
                                                        email_text = email_text.replace(/\[P-StartTime]/g, start_time);
                                                        email_subject = email_subject.replace(/\[P-StartTime]/g, start_time);
                                                        sms_text = sms_text.replace(/\[P-StartTime]/g, start_time);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-StartDate]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_pickup_datetime && getJobDetailsFromJobIDResult[0].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                        var start_time = moment(getJobDetailsFromJobIDResult[0].job_pickup_datetime).format('MMM Do');
                                                        email_text = email_text.replace(/\[P-StartDate]/g, start_time);
                                                        email_subject = email_subject.replace(/\[P-StartDate]/g, start_time);
                                                        sms_text = sms_text.replace(/\[P-StartDate]/g, start_time);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-EndTime]' :
                                            case '[P-Time]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_delivery_datetime && getJobDetailsFromJobIDResult[0].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                        var end_time = moment(getJobDetailsFromJobIDResult[0].job_delivery_datetime).format('h:mm a');
                                                        email_text = email_text.replace(/\[P-EndTime]/g, end_time);
                                                        email_subject = email_subject.replace(/\[P-EndTime]/g, end_time);
                                                        sms_text = sms_text.replace(/\[P-EndTime]/g, end_time);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-EndDate]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_delivery_datetime && getJobDetailsFromJobIDResult[0].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                        var end_time = moment(getJobDetailsFromJobIDResult[0].job_delivery_datetime).format('MMM Do');
                                                        email_text = email_text.replace(/\[P-EndDate]/g, end_time);
                                                        email_subject = email_subject.replace(/\[P-EndDate]/g, end_time);
                                                        sms_text = sms_text.replace(/\[P-EndDate]/g, end_time);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-TaskDescription]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_description) {
                                                        email_text = email_text.replace(/\[P-TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                                        email_subject = email_subject.replace(/\[P-TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                                        sms_text = sms_text.replace(/\[P-TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                })
                                                break;
                                            case '[P-NewLine]' :
                                                email_text = email_text.replace(/\[P-NewLine]/g, "<br>");
                                                email_subject = email_subject.replace(/\[P-NewLine]/g, "<br>");
                                                sms_text = sms_text.replace(/\[P-NewLine]/g, "\n");
                                                counter++;
                                                replaceCustomFields(user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                break;
                                            case '[P-DriverName]' :
                                            case '[P-AgentName]' :
                                            case '[P-FleetName]' :
                                                commonFunc.getFleetDetailsFromJobID(p_job_id, function (getFleetDetailsFromJobIDResult) {
                                                    if (getFleetDetailsFromJobIDResult && getFleetDetailsFromJobIDResult[0].username) {
                                                        email_text = email_text.replace(/\[P-DriverName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                        email_subject = email_subject.replace(/\[P-DriverName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                        sms_text = sms_text.replace(/\[P-DriverName]/g, getFleetDetailsFromJobIDResult[0].username);

                                                        email_text = email_text.replace(/\[P-AgentName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                        email_subject = email_subject.replace(/\[P-AgentName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                        sms_text = sms_text.replace(/\[P-AgentName]/g, getFleetDetailsFromJobIDResult[0].username);

                                                        email_text = email_text.replace(/\[P-FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                        email_subject = email_subject.replace(/\[P-FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                        sms_text = sms_text.replace(/\[P-FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                });
                                                break;
                                            case '[P-DriverPhone]' :
                                            case '[P-AgentPhone]' :
                                            case '[P-FleetPhone]' :
                                                commonFunc.getFleetDetailsFromJobID(p_job_id, function (getFleetDetailsFromJobIDResult) {
                                                    if (getFleetDetailsFromJobIDResult && getFleetDetailsFromJobIDResult[0].phone) {
                                                        email_text = email_text.replace(/\[P-DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                        email_subject = email_subject.replace(/\[P-DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                        sms_text = sms_text.replace(/\[P-DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);

                                                        email_text = email_text.replace(/\[P-AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                        email_subject = email_subject.replace(/\[P-AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                        sms_text = sms_text.replace(/\[P-AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);

                                                        email_text = email_text.replace(/\[P-FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                        email_subject = email_subject.replace(/\[P-FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                        sms_text = sms_text.replace(/\[P-FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                });
                                                break;
                                            case '[P-TrackingLink]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {

                                                    commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function(user) {

                                                        var longurl = config.get('trackLink') + getJobDetailsFromJobIDResult[0].job_hash;
                                                        if (user[0].domain) {
                                                            longurl = user[0].domain + config.get('trackingPage') + getJobDetailsFromJobIDResult[0].job_hash;
                                                        }

                                                        commonFunc.getGoogleShortenUrl(longurl, function (shortenUrl) {
                                                            if (shortenUrl) {
                                                                email_text = email_text.replace(/\[P-TrackingLink]/g, shortenUrl);
                                                                email_subject = email_subject.replace(/\[P-TrackingLink]/g, shortenUrl);
                                                                sms_text = sms_text.replace(/\[P-TrackingLink]/g, shortenUrl);
                                                                counter++;
                                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            }
                                                        });
                                                    });
                                                });
                                                break;
                                            case '[P-OrgName]' :
                                            case '[P-CompanyName]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                        if (getUserDetailsResult && getUserDetailsResult[0].company_name) {
                                                            email_text = email_text.replace(/\[P-OrgName]/g, getUserDetailsResult[0].company_name);
                                                            email_subject = email_subject.replace(/\[P-OrgName]/g, getUserDetailsResult[0].company_name);
                                                            sms_text = sms_text.replace(/\[P-OrgName]/g, getUserDetailsResult[0].company_name);

                                                            email_text = email_text.replace(/\[P-CompanyName]/g, getUserDetailsResult[0].company_name);
                                                            email_subject = email_subject.replace(/\[P-CompanyName]/g, getUserDetailsResult[0].company_name);
                                                            sms_text = sms_text.replace(/\[P-CompanyName]/g, getUserDetailsResult[0].company_name);
                                                        }
                                                        counter++;
                                                        replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                    });
                                                });
                                                break;
                                            case '[P-DispatcherNumber]' :
                                            case '[P-ManagerNumber]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                        if (getUserDetailsResult && getUserDetailsResult[0].user_id) {
                                                            email_text = email_text.replace(/\[P-DispatcherNumber]/g, getUserDetailsResult[0].phone);
                                                            email_subject = email_subject.replace(/\[P-DispatcherNumber]/g, getUserDetailsResult[0].phone);
                                                            sms_text = sms_text.replace(/\[P-DispatcherNumber]/g, getUserDetailsResult[0].phone);

                                                            email_text = email_text.replace(/\[P-ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                            email_subject = email_subject.replace(/\[P-ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                            sms_text = sms_text.replace(/\[P-ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                        }
                                                        counter++;
                                                        replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                    });
                                                });
                                                break;
                                            case '[P-Dispatcher]' :
                                            case '[P-Manager]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                        if (getUserDetailsResult && getUserDetailsResult[0].username) {
                                                            email_text = email_text.replace(/\[P-Dispatcher]/g, getUserDetailsResult[0].username);
                                                            email_subject = email_subject.replace(/\[P-Dispatcher]/g, getUserDetailsResult[0].username);
                                                            sms_text = sms_text.replace(/\[P-Dispatcher]/g, getUserDetailsResult[0].username);

                                                            email_text = email_text.replace(/\[P-Manager]/g, getUserDetailsResult[0].username);
                                                            email_subject = email_subject.replace(/\[P-Manager]/g, getUserDetailsResult[0].username);
                                                            sms_text = sms_text.replace(/\[P-Manager]/g, getUserDetailsResult[0].username);
                                                        }
                                                        counter++;
                                                        replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                    });
                                                });
                                                break;
                                            case '[P-TaskID]' :
                                                email_text = email_text.replace(/\[P-TaskID]/g, p_job_id);
                                                email_subject = email_subject.replace(/\[P-TaskID]/g, p_job_id);
                                                sms_text = sms_text.replace(/\[P-TaskID]/g, p_job_id);
                                                counter++;
                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                break;
                                            case '[P-OrderNumber]' :
                                                commonFunc.getJobDetailsFromJobID(p_job_id, function (getJobDetailsFromJobIDResult) {
                                                    if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].order_id) {
                                                        email_text = email_text.replace(/\[P-OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                                        email_subject = email_subject.replace(/\[P-OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                                        sms_text = sms_text.replace(/\[P-OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                                    }
                                                    counter++;
                                                    replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                });
                                                break;
                                            case '[TotalTimeTaken]' :
                                                email_text = email_text.replace(/\[TotalTimeTaken]/g, total_time);
                                                email_subject = email_subject.replace(/\[TotalTimeTaken]/g, total_time);
                                                sms_text = sms_text.replace(/\[TotalTimeTaken]/g, total_time);

                                                counter++;
                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                break;
                                            case '[TotalDistanceTravelled]' :

                                                email_text = email_text.replace(/\[TotalDistanceTravelled]/g, total_distance);
                                                email_subject = email_subject.replace(/\[TotalDistanceTravelled]/g, total_distance);
                                                sms_text = sms_text.replace(/\[TotalDistanceTravelled]/g, total_distance);

                                                counter++;
                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);

                                                break;
                                            default:
                                                counter++;
                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                        }
                                    }

                                    function replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {

                                        if (counter == variablesResultLength) {
                                            mongo.getOptionalFieldsForTask(user_id, p_job_id, {}, function (getOptionalFieldsForTaskResult) {
                                                if ((typeof (getOptionalFieldsForTaskResult.fields.custom_field) != "undefined") && (getOptionalFieldsForTaskResult.fields.custom_field.length > 0)) {
                                                    getOptionalFieldsForTaskResult.fields.custom_field.forEach(function (field) {
                                                        email_text = email_text.replaceAll('[' + field.template_id + '-' + field.label + ']', field.fleet_data || field.data);
                                                        email_subject = email_subject.replaceAll('[' + field.template_id + '-' + field.label + ']', field.fleet_data || field.data);
                                                        sms_text = sms_text.replaceAll('[' + field.template_id + '-' + field.label + ']', field.fleet_data || field.data);
                                                    })
                                                }
                                                replaceTaskHistory(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength);
                                            });
                                        }
                                    }

                                    function replaceTaskHistory(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {

                                        if (counter == variablesResultLength) {
                                            commonFunc.getTaskHistory(p_job_id, {}, function (getTaskHistoryResult) {
                                                getTaskHistoryResult.task_history.forEach(function (history) {
                                                    if (history.type == constants.taskHistoryType.SIGN_IMAGE_ADDED || history.type == constants.taskHistoryType.SIGN_IMAGE_UPDATED) {
                                                        history.description = history.description.replaceAll('https://', '');
                                                        email_text = email_text.replaceAll('[P-SignImage]', history.description);
                                                        email_subject = email_subject.replaceAll('[P-SignImage]', history.description);
                                                        sms_text = sms_text.replaceAll('[P-SignImage]', history.description);
                                                    }
                                                })
                                                checkCounter(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength);
                                            });
                                        }
                                    }

                                    function checkCounter(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {

                                        if (counter == variablesResultLength) {

                                            counter = 0;
                                            commonFunc.getTemplateVariables(getTemplateMessageResult[0].template_id, function (getTemplateVariablesResult) {
                                                var getTemplateVariablesResultLength = getTemplateVariablesResult.length;
                                                for (var i = 0; i < getTemplateVariablesResultLength; i++) {
                                                    var variable_name = getTemplateVariablesResult[i].variable_name;
                                                    switch (variable_name) {
                                                        case '[D-CustomerName]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getCustomerDetailsFromJobIDResult) {
                                                                if (getCustomerDetailsFromJobIDResult && getCustomerDetailsFromJobIDResult[0].customer_username) {
                                                                    getCustomerDetailsFromJobIDResult[0].customer_username = getCustomerDetailsFromJobIDResult[0].customer_username.split(' ')[0];
                                                                    email_text = email_text.replace(/\[D-CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                                                    email_subject = email_subject.replace(/\[D-CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                                                    sms_text = sms_text.replace(/\[D-CustomerName]/g, getCustomerDetailsFromJobIDResult[0].customer_username);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-Date]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_time) {
                                                                    var job_date = moment(getJobDetailsFromJobIDResult[0].job_time).format('MMM Do');
                                                                    email_text = email_text.replace(/\[D-Date]/g, job_date);
                                                                    email_subject = email_subject.replace(/\[D-Date]/g, job_date);
                                                                    sms_text = sms_text.replace(/\[D-Date]/g, job_date);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-CompletedDate]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].completed_datetime) {
                                                                    var timezone = getJobDetailsFromJobIDResult[0].timezone;
                                                                    var today = moment(new Date()).format('YYYY-MM-DD HH:m:s');
                                                                    today = commonFunc.convertTimeIntoLocal(today, timezone);
                                                                    var completed_datetime = moment(today).format('MMM Do');
                                                                    email_text = email_text.replace(/\[D-CompletedDate]/g, completed_datetime);
                                                                    email_subject = email_subject.replace(/\[D-CompletedDate]/g, completed_datetime);
                                                                    sms_text = sms_text.replace(/\[D-CompletedDate]/g, completed_datetime);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-CompletedTime]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].completed_datetime) {
                                                                    var timezone = getJobDetailsFromJobIDResult[0].timezone;
                                                                    var today = moment(new Date()).format('YYYY-MM-DD HH:m:s');
                                                                    today = commonFunc.convertTimeIntoLocal(today, timezone);
                                                                    var completed_datetime = moment(today).format('h:mm a');
                                                                    email_text = email_text.replace(/\[D-CompletedTime]/g, completed_datetime);
                                                                    email_subject = email_subject.replace(/\[D-CompletedTime]/g, completed_datetime);
                                                                    sms_text = sms_text.replace(/\[D-CompletedTime]/g, completed_datetime);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-CustomerAddress]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getCustomerDetailsFromJobIDResult) {
                                                                if (getCustomerDetailsFromJobIDResult && getCustomerDetailsFromJobIDResult[0].job_address) {
                                                                    email_text = email_text.replace(/\[D-CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                                                    email_subject = email_subject.replace(/\[D-CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                                                    sms_text = sms_text.replace(/\[D-CustomerAddress]/g, getCustomerDetailsFromJobIDResult[0].job_address);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-StartTime]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_pickup_datetime && getJobDetailsFromJobIDResult[0].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                                    var start_time = moment(getJobDetailsFromJobIDResult[0].job_pickup_datetime).format('h:mm a');
                                                                    email_text = email_text.replace(/\[D-StartTime]/g, start_time);
                                                                    email_subject = email_subject.replace(/\[D-StartTime]/g, start_time);
                                                                    sms_text = sms_text.replace(/\[D-StartTime]/g, start_time);
                                                                }
                                                                counter++;
                                                                replaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-StartDate]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_pickup_datetime && getJobDetailsFromJobIDResult[0].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                                    var start_time = moment(getJobDetailsFromJobIDResult[0].job_pickup_datetime).format('MMM Do');
                                                                    email_text = email_text.replace(/\[D-StartDate]/g, start_time);
                                                                    email_subject = email_subject.replace(/\[D-StartDate]/g, start_time);
                                                                    sms_text = sms_text.replace(/\[D-StartDate]/g, start_time);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-EndTime]' :
                                                        case '[D-Time]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_delivery_datetime && getJobDetailsFromJobIDResult[0].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                                    var end_time = moment(getJobDetailsFromJobIDResult[0].job_delivery_datetime).format('h:mm a');
                                                                    email_text = email_text.replace(/\[EndTime]/g, end_time);
                                                                    email_subject = email_subject.replace(/\[EndTime]/g, end_time);
                                                                    sms_text = sms_text.replace(/\[EndTime]/g, end_time);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-EndDate]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_delivery_datetime && getJobDetailsFromJobIDResult[0].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                                    var end_time = moment(getJobDetailsFromJobIDResult[0].job_delivery_datetime).format('MMM Do');
                                                                    email_text = email_text.replace(/\[D-EndDate]/g, end_time);
                                                                    email_subject = email_subject.replace(/\[D-EndDate]/g, end_time);
                                                                    sms_text = sms_text.replace(/\[D-EndDate]/g, end_time);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-TaskDescription]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].job_description) {
                                                                    email_text = email_text.replace(/\[D-TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                                                    email_subject = email_subject.replace(/\[D-TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                                                    sms_text = sms_text.replace(/\[D-TaskDescription]/g, getJobDetailsFromJobIDResult[0].job_description);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            })
                                                            break;
                                                        case '[D-NewLine]' :
                                                            email_text = email_text.replace(/\[D-NewLine]/g, "<br>");
                                                            email_subject = email_subject.replace(/\[D-NewLine]/g, "<br>");
                                                            sms_text = sms_text.replace(/\[D-NewLine]/g, "\n");
                                                            counter++;
                                                            dreplaceCustomFields(user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            break;
                                                        case '[D-DriverName]' :
                                                        case '[D-AgentName]' :
                                                        case '[D-FleetName]' :
                                                            commonFunc.getFleetDetailsFromJobID(d_job_id, function (getFleetDetailsFromJobIDResult) {
                                                                if (getFleetDetailsFromJobIDResult && getFleetDetailsFromJobIDResult[0].username) {
                                                                    email_text = email_text.replace(/\[D-DriverName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                                    email_subject = email_subject.replace(/\[D-DriverName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                                    sms_text = sms_text.replace(/\[D-DriverName]/g, getFleetDetailsFromJobIDResult[0].username);

                                                                    email_text = email_text.replace(/\[D-AgentName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                                    email_subject = email_subject.replace(/\[D-AgentName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                                    sms_text = sms_text.replace(/\[D-AgentName]/g, getFleetDetailsFromJobIDResult[0].username);

                                                                    email_text = email_text.replace(/\[D-FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                                    email_subject = email_subject.replace(/\[D-FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                                    sms_text = sms_text.replace(/\[D-FleetName]/g, getFleetDetailsFromJobIDResult[0].username);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            });
                                                            break;
                                                        case '[D-DriverPhone]' :
                                                        case '[D-AgentPhone]' :
                                                        case '[D-FleetPhone]' :
                                                            commonFunc.getFleetDetailsFromJobID(d_job_id, function (getFleetDetailsFromJobIDResult) {
                                                                if (getFleetDetailsFromJobIDResult && getFleetDetailsFromJobIDResult[0].phone) {
                                                                    email_text = email_text.replace(/\[D-DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                                    email_subject = email_subject.replace(/\[D-DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                                    sms_text = sms_text.replace(/\[D-DriverPhone]/g, getFleetDetailsFromJobIDResult[0].phone);

                                                                    email_text = email_text.replace(/\[D-AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                                    email_subject = email_subject.replace(/\[D-AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                                    sms_text = sms_text.replace(/\[D-AgentPhone]/g, getFleetDetailsFromJobIDResult[0].phone);

                                                                    email_text = email_text.replace(/\[D-FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                                    email_subject = email_subject.replace(/\[D-FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                                    sms_text = sms_text.replace(/\[D-FleetPhone]/g, getFleetDetailsFromJobIDResult[0].phone);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            });
                                                            break;
                                                        case '[D-TrackingLink]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function(user) {

                                                                    var longurl = config.get('trackLink') + getJobDetailsFromJobIDResult[0].job_hash;
                                                                    if (user[0].domain) {
                                                                        longurl = user[0].domain + config.get('trackingPage') + getJobDetailsFromJobIDResult[0].job_hash;
                                                                    }
                                                                    commonFunc.getGoogleShortenUrl(longurl, function (shortenUrl) {
                                                                        if (shortenUrl) {
                                                                            email_text = email_text.replace(/\[D-TrackingLink]/g, shortenUrl);
                                                                            email_subject = email_subject.replace(/\[D-TrackingLink]/g, shortenUrl);
                                                                            sms_text = sms_text.replace(/\[D-TrackingLink]/g, shortenUrl);
                                                                            counter++;
                                                                            dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                                        }
                                                                    });
                                                                });
                                                            });
                                                            break;
                                                        case '[D-OrgName]' :
                                                        case '[D-CompanyName]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                                    if (getUserDetailsResult && getUserDetailsResult[0].company_name) {
                                                                        email_text = email_text.replace(/\[D-OrgName]/g, getUserDetailsResult[0].company_name);
                                                                        email_subject = email_subject.replace(/\[D-OrgName]/g, getUserDetailsResult[0].company_name);
                                                                        sms_text = sms_text.replace(/\[D-OrgName]/g, getUserDetailsResult[0].company_name);

                                                                        email_text = email_text.replace(/\[D-CompanyName]/g, getUserDetailsResult[0].company_name);
                                                                        email_subject = email_subject.replace(/\[D-CompanyName]/g, getUserDetailsResult[0].company_name);
                                                                        sms_text = sms_text.replace(/\[D-CompanyName]/g, getUserDetailsResult[0].company_name);
                                                                    }
                                                                    counter++;
                                                                    dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                                });
                                                            });
                                                            break;
                                                        case '[D-DispatcherNumber]' :
                                                        case '[D-ManagerNumber]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                                    if (getUserDetailsResult && getUserDetailsResult[0].user_id) {
                                                                        email_text = email_text.replace(/\[D-DispatcherNumber]/g, getUserDetailsResult[0].phone);
                                                                        email_subject = email_subject.replace(/\[D-DispatcherNumber]/g, getUserDetailsResult[0].phone);
                                                                        sms_text = sms_text.replace(/\[D-DispatcherNumber]/g, getUserDetailsResult[0].phone);

                                                                        email_text = email_text.replace(/\[D-ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                                        email_subject = email_subject.replace(/\[D-ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                                        sms_text = sms_text.replace(/\[D-ManagerNumber]/g, getUserDetailsResult[0].phone);
                                                                    }
                                                                    counter++;
                                                                    dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                                });
                                                            });
                                                            break;
                                                        case '[D-Dispatcher]' :
                                                        case '[D-Manager]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                commonFunc.getUserDetails(getJobDetailsFromJobIDResult[0].user_id, function (getUserDetailsResult) {
                                                                    if (getUserDetailsResult && getUserDetailsResult[0].username) {
                                                                        email_text = email_text.replace(/\[D-Dispatcher]/g, getUserDetailsResult[0].username);
                                                                        email_subject = email_subject.replace(/\[D-Dispatcher]/g, getUserDetailsResult[0].username);
                                                                        sms_text = sms_text.replace(/\[D-Dispatcher]/g, getUserDetailsResult[0].username);

                                                                        email_text = email_text.replace(/\[D-Manager]/g, getUserDetailsResult[0].username);
                                                                        email_subject = email_subject.replace(/\[D-Manager]/g, getUserDetailsResult[0].username);
                                                                        sms_text = sms_text.replace(/\[D-Manager]/g, getUserDetailsResult[0].username);
                                                                    }
                                                                    counter++;
                                                                    dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                                });
                                                            });
                                                            break;
                                                        case '[D-TaskID]' :
                                                            email_text = email_text.replace(/\[D-TaskID]/g, d_job_id);
                                                            email_subject = email_subject.replace(/\[D-TaskID]/g, d_job_id);
                                                            sms_text = sms_text.replace(/\[D-TaskID]/g, d_job_id);
                                                            counter++;
                                                            dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            break;
                                                        case '[D-OrderNumber]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && getJobDetailsFromJobIDResult[0].order_id) {
                                                                    email_text = email_text.replace(/\[D-OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                                                    email_subject = email_subject.replace(/\[D-OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                                                    sms_text = sms_text.replace(/\[D-OrderNumber]/g, getJobDetailsFromJobIDResult[0].order_id);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            });
                                                            break;
                                                        case '[D-TotalTimeTaken]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult && (getJobDetailsFromJobIDResult[0].started_datetime != "0000-00-00 00:00:00") && (getJobDetailsFromJobIDResult[0].completed_datetime != "0000-00-00 00:00:00")) {
                                                                    var started_time = new Date(getJobDetailsFromJobIDResult[0].started_datetime);
                                                                    var completed_datetime = new Date(getJobDetailsFromJobIDResult[0].completed_datetime);
                                                                    var timeDifference = commonFunc.millisecondsToStr(completed_datetime.getTime() - started_time.getTime());
                                                                    email_text = email_text.replace(/\[D-TotalTimeTaken]/g, timeDifference);
                                                                    email_subject = email_subject.replace(/\[D-TotalTimeTaken]/g, timeDifference);
                                                                    sms_text = sms_text.replace(/\[D-TotalTimeTaken]/g, timeDifference);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            });
                                                            break;
                                                        case '[D-TotalDistanceTravelled]' :
                                                            commonFunc.getJobDetailsFromJobID(d_job_id, function (getJobDetailsFromJobIDResult) {
                                                                if (getJobDetailsFromJobIDResult) {
                                                                    email_text = email_text.replace(/\[D-TotalDistanceTravelled]/g, getJobDetailsFromJobIDResult[0].total_distance_travelled);
                                                                    email_subject = email_subject.replace(/\[D-TotalDistanceTravelled]/g, getJobDetailsFromJobIDResult[0].total_distance_travelled);
                                                                    sms_text = sms_text.replace(/\[D-TotalDistanceTravelled]/g, getJobDetailsFromJobIDResult[0].total_distance_travelled);
                                                                }
                                                                counter++;
                                                                dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                            });
                                                            break;
                                                        default:
                                                            counter++;
                                                            dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, getTemplateVariablesResultLength);
                                                    }
                                                }

                                                function dreplaceCustomFields(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {

                                                    if (counter == variablesResultLength) {
                                                        mongo.getOptionalFieldsForTask(user_id, d_job_id, {}, function (getOptionalFieldsForTaskResult) {
                                                            if ((typeof (getOptionalFieldsForTaskResult.fields.custom_field) != "undefined") && (getOptionalFieldsForTaskResult.fields.custom_field.length > 0)) {
                                                                getOptionalFieldsForTaskResult.fields.custom_field.forEach(function (field) {
                                                                    email_text = email_text.replaceAll('[' + field.template_id+ '-' + field.label + ']', field.fleet_data || field.data);
                                                                    email_subject = email_subject.replaceAll('[' + field.template_id + '-' + field.label + ']', field.fleet_data || field.data);
                                                                    sms_text = sms_text.replaceAll('[' + field.template_id + '-' + field.label + ']', field.fleet_data || field.data);
                                                                })
                                                            }
                                                            dreplaceTaskHistory(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength);
                                                        });
                                                    }
                                                }

                                                function dreplaceTaskHistory(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {

                                                    if (counter == variablesResultLength) {
                                                        commonFunc.getTaskHistory(d_job_id, {}, function (getTaskHistoryResult) {
                                                            getTaskHistoryResult.task_history.forEach(function (history) {
                                                                if (history.type == constants.taskHistoryType.SIGN_IMAGE_ADDED || history.type == constants.taskHistoryType.SIGN_IMAGE_UPDATED) {
                                                                    history.description = history.description.replaceAll('https://', '');
                                                                    email_text = email_text.replaceAll('[D-SignImage]', history.description);
                                                                    email_subject = email_subject.replaceAll('[D-SignImage]', history.description);
                                                                    sms_text = sms_text.replaceAll('[D-SignImage]', history.description);
                                                                }
                                                            })
                                                            sendData(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength);
                                                        });
                                                    }
                                                }

                                                function sendData(hook, user_email, sms_text, email_text, email_subject, counter, variablesResultLength) {

                                                    if (counter == variablesResultLength) {
                                                        var regex =  sms_text.match(/\[(.*?)\]/g);if (regex && regex.length>0){regex.forEach(function(dat){sms_text = sms_text.replaceAll(dat, '-');})}
                                                        var regex =  email_text.match(/\[(.*?)\]/g);if (regex && regex.length>0){regex.forEach(function(dat){email_text = email_text.replaceAll(dat, '-');})}
                                                        var regex =  email_subject.match(/\[(.*?)\]/g);if (regex && regex.length>0){regex.forEach(function(dat){email_subject = email_subject.replaceAll(dat, '-');})}
                                                        if ((to_phone) && (sms_text != 'Disabled')) {
                                                            sms_text = sms_text.trim().replace(/([^\x00-\xFF]|\s)*$/g, '');
                                                            commonFunc.sendMessageByPlivo(to_phone, sms_text);
                                                        }
                                                        if ((to_email) && (user_email != 'Disabled')) {
                                                            commonFunc.sendHtmlContent(to_email, email_text, email_subject, user_email, function (resultMail) {
                                                            });
                                                        }
                                                        if (hook != 'Disabled') {
                                                            commonFunc.sendPostHook(hook, jobID);
                                                        }
                                                    }
                                                }
                                            });

                                        }
                                    }
                                });
                            }
                        })
                    }
                })
            }
        });
    }
}


exports.shift_mandrill_templates = function (req, res) {
    var Mandrill = require('machinepack-mandrill');

// Get all mandrill templates from one account and add them to another.
    Mandrill.migrateTemplates({
        srcApiKey: 'WmJiNRkCtHYK59a7yVJz6g',
        destApiKey: '1cv3L-kA9CmJwGNgmk9UPQ'
    }).exec({
// An unexpected error occurred.
        error: function (err){
            console.log("err",err)
        },
// OK.
        success: function (result){
            console.log("success",result)
        }
    });
}
