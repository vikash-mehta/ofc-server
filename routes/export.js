var commonFunc = require('./commonfunction');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var md5 = require('MD5');
var json2csv = require('json2csv');
var jobs = require('./jobs');
var mongo = require('./mongo');

/*
 * -------------------------
 * EXPORT TASKS INTO CSV
 * -------------------------
 */
exports.export_tasks = function (req, res) {

    var access_token = req.body.access_token;
    var type = req.body.type;
    var job_id = req.body.job_id;
    var include_task_history = parseInt(req.body.include_task_history);
    if (typeof(include_task_history) === "undefined" || include_task_history === "") {
        include_task_history = 0;
    }
    var manvalues = [access_token, type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var layout_type = result[0].layout_type, job_type;
                if (layout_type == constants.layoutType.PICKUP_AND_DELIVERY) {
                    job_type = constants.jobType.PICKUP + "," + constants.jobType.DELIVERY;
                } else if (layout_type == constants.layoutType.APPOINTMENT) {
                    job_type = constants.jobType.APPOINTMENT;
                } else if (layout_type == constants.layoutType.FOS) {
                    job_type = constants.jobType.FOS;
                }
                var sql = "SELECT  fleets.`fleet_id`,fleets.`username` as `fleet_name`,tm.`team_name`," +
                    " jobs.`order_id`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`completed_by_admin`," +
                    " jobs.`user_id`,jobs.`pickup_delivery_relationship`,jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`," +
                    " jobs.`total_distance_travelled`,jobs.`team_id`,jobs.`timezone`,jobs.`customer_id`,jobs.`customer_username`,jobs.`customer_phone`,jobs.`customer_email`" +
                    " FROM `tb_jobs` jobs " +
                    " LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
                    " LEFT JOIN `tb_teams` tm ON tm.`team_id`= jobs.`team_id` " +
                    " WHERE jobs.`user_id`=? AND `job_type` IN (" + job_type + ") AND `job_status`<>" + constants.jobStatus.DELETED + " ";
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    commonFunc.checkDispatcherPermissions(result[0].user_id, function (disPerm) {
                        var teams = "0," + disPerm[0].teams;
                        if (disPerm && disPerm.length && disPerm[0].view_task == constants.hasPermissionStatus.NO) {
                            teams = disPerm[0].teams;
                        }
                        sql = "SELECT  fleets.`fleet_id`,fleets.`username` as `fleet_name`,tm.`team_name`," +
                            " jobs.`order_id`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`completed_by_admin`," +
                            " jobs.`user_id`,jobs.`pickup_delivery_relationship`,jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`," +
                            " jobs.`total_distance_travelled`,jobs.`team_id`,jobs.`timezone`,jobs.`customer_id`,jobs.`customer_username`,jobs.`customer_phone`,jobs.`customer_email`" +
                            " FROM `tb_jobs` jobs " +
                            " LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
                            " LEFT JOIN `tb_teams` tm ON tm.`team_id` = jobs.`team_id` " +
                            " WHERE jobs.`team_id` IN (" + teams + ") AND jobs.`user_id`= ? AND jobs.`job_type` IN (" + job_type + ") AND jobs.`job_status`<>" + constants.jobStatus.DELETED + " ";
                        step_in(sql, result[0].dispatcher_user_id, result);
                    })
                } else {
                    step_in(sql, result[0].user_id, result);
                }

                function step_in(sql, user, result) {
                    if (type == "custom" || type == "today") {
                        req.body.start_date = req.body.start_date + ' 00:00:00'
                        req.body.end_date = req.body.end_date + ' 23:59:59'
                        sql += " AND `job_time`>='" + req.body.start_date + "' AND `job_time`<='" + req.body.end_date + "' ";
                    }
                    if (job_id) {
                        sql += " AND `job_id` = " + job_id + " ";
                    }
                    connection.query(sql, [user], function (err, result_jobs) {
                        if (err) {
                            responses.sendError(res);
                            return;
                        } else {
                            var result_jobs_length = result_jobs.length;
                            if (result_jobs_length > 0) {
                                if (result[0].layout_type == constants.layoutType.PICKUP_AND_DELIVERY) {
                                    processPDdata(result_jobs, result_jobs_length, user, layout_type, include_task_history, res);
                                } else {
                                    processAptdata(result_jobs, result_jobs_length, user, layout_type, include_task_history, res);
                                }
                            } else {
                                responses.noDataFoundError(res);
                            }
                        }
                    });
                }
            }
        });
    }
};

function exportData(dataObject, fields, res) {
    json2csv({data: dataObject, fields: fields}, function (err, csv) {
        if (err) console.log(err);
        res.setHeader('Content-Type', 'application/octet-stream');
        res.setHeader('Content-Disposition', 'attachment; filename=tasks.csv;');
        res.end(csv, 'binary');
    });
}

function processAptdata(result_jobs, result_jobs_length, user_id, layout_type, include_task_history, res) {
    var counter = 0, task_history = [], final = [];
    var fields = ['Task_ID', 'Order_ID', 'Relationship', 'Team_Name', 'Task_Type', 'Notes', 'Agent_Name', 'Distance', 'Customer_Name', 'Customer_Address', 'Start_Time', 'End_Time', 'Task_Status'];

    mongo.getLayoutOptionalFieldsForUser(user_id, layout_type, function (optionalFields) {
        if (optionalFields.length > 0) {
            if ((typeof (optionalFields[0].fields.custom_field) != 'undefined') && (optionalFields[0].fields.custom_field.length > 0)) {
                fields.push('Custom_Template_ID');
                var custom_field = optionalFields[0].fields.custom_field;
                custom_field.forEach(function (field) {
                    Object.keys(field).forEach(function (key) {
                        field[key].items.forEach(function (item) {
                            if (item.label) {
                                fields.push(item.label);
                            }
                        })
                    });
                });
            }
            if (include_task_history) {
                fields.push('Type', 'History_Description', 'Location', 'Time');
            }
            extractData(result_jobs, result_jobs_length);
        } else {
            if (include_task_history) {
                fields.push('Type', 'History_Description', 'Location', 'Time');
            }
            extractData(result_jobs, result_jobs_length);
        }
    })
    function extractData(result_jobs, result_jobs_length) {
        for (var i = 0; i < result_jobs_length; i++) {
            if (result_jobs[i].job_description == null) result_jobs[i].job_description = '';
            result_jobs[i].Task_ID = result_jobs[i].job_id;
            result_jobs[i].Relationship = result_jobs[i].pickup_delivery_relationship;
            result_jobs[i].Distance = (result_jobs[i].total_distance_travelled == null ||  result_jobs[i].total_distance_travelled == 'null') ? '0' : result_jobs[i].total_distance_travelled;
            result_jobs[i].Order_ID = result_jobs[i].order_id == null ? "-" : result_jobs[i].order_id;
            result_jobs[i].Team_Name = (result_jobs[i].team_id == 0 || result_jobs[i].team_id == null) ? "All Teams" : result_jobs[i].team_name;
            result_jobs[i].Notes = result_jobs[i].job_description;
            result_jobs[i].Task_Type = (result_jobs[i].job_type == constants.jobType.PICKUP ? "Pick-up" : result_jobs[i].job_type == constants.jobType.DELIVERY ? "Delivery" : result_jobs[i].job_type == constants.jobType.APPOINTMENT ? "Appointment" : result_jobs[i].job_type == constants.jobType.FOS ? "FOS" : "");
            result_jobs[i].Agent_Name = result_jobs[i].fleet_name == null ? "-" : result_jobs[i].fleet_name;
            result_jobs[i].Customer_Name = result_jobs[i].job_type == constants.jobType.PICKUP ? result_jobs[i].job_pickup_name : result_jobs[i].customer_username;
            result_jobs[i].Customer_Address = result_jobs[i].job_type == constants.jobType.PICKUP ? result_jobs[i].job_pickup_address : result_jobs[i].job_address;
            result_jobs[i].Customer_Name = result_jobs[i].Customer_Name == null ? "-" : result_jobs[i].Customer_Name;

            if (result_jobs[i].job_status == constants.jobStatus.UNASSIGNED) {
                result_jobs[i].Task_Status = 'Unassigned';
            } else if (result_jobs[i].job_status == constants.jobStatus.UPCOMING) {
                result_jobs[i].Task_Status = 'Assigned';
            } else if (result_jobs[i].job_status == constants.jobStatus.STARTED) {
                result_jobs[i].Task_Status = 'In Transit';
            } else if (result_jobs[i].job_status == constants.jobStatus.ENDED) {
                result_jobs[i].Task_Status = 'Completed';
            } else if (result_jobs[i].job_status == constants.jobStatus.FAILED) {
                result_jobs[i].Task_Status = 'Failed';
            } else if (result_jobs[i].job_status == constants.jobStatus.ARRIVED) {
                result_jobs[i].Task_Status = 'Arrived';
            } else if (result_jobs[i].job_status == constants.jobStatus.PARTIAL) {
                result_jobs[i].Task_Status = 'Partial';
            } else if (result_jobs[i].job_status == constants.jobStatus.ACCEPTED) {
                result_jobs[i].Task_Status = 'Accepted';
            } else if (result_jobs[i].job_status == constants.jobStatus.DECLINE) {
                result_jobs[i].Task_Status = 'Declined';
            } else if (result_jobs[i].job_status == constants.jobStatus.CANCEL) {
                result_jobs[i].Task_Status = 'Canceled';
            }
            if (result_jobs[i].job_pickup_datetime != '0000-00-00 00:00:00' && result_jobs[i].job_pickup_datetime != null) {
                result_jobs[i].job_pickup_datetime = result_jobs[i].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
            }
            if (result_jobs[i].job_pickup_datetime == '0000-00-00 00:00:00') {
                result_jobs[i].job_pickup_datetime = '-';
            }
            if (result_jobs[i].job_delivery_datetime != '0000-00-00 00:00:00' && result_jobs[i].job_delivery_datetime != null) {
                result_jobs[i].job_delivery_datetime = result_jobs[i].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
            }
            if (result_jobs[i].job_delivery_datetime == '0000-00-00 00:00:00') {
                result_jobs[i].job_delivery_datetime = '-';
            }
            result_jobs[i].Start_Time = result_jobs[i].job_pickup_datetime;
            result_jobs[i].End_Time = result_jobs[i].job_delivery_datetime;

            mongo.getOptionalFieldsForTask(result_jobs[i].user_id, result_jobs[i].job_id, result_jobs[i], function (custom_fields) {
                custom_fields.task.fields = custom_fields.fields;
                counter++;
                getTaskHistory(counter, result_jobs_length, custom_fields.task);
            });
        }
    }

    function getTaskHistory(counter, result_task_profile_length, result_task_profile) {
        task_history.push(result_task_profile);
        if (counter == result_task_profile_length) {
            var index = 0, allResponses = [];
            getHistory(index);
            function changeIndex() {
                index++;
                getHistory(index);
            }

            function getHistory(item) {
                if (item > task_history.length - 1) {
                    findCustomData(allResponses);
                } else {
                    commonFunc.delay2(item, function (i) {
                        (function (i) {
                            commonFunc.getTaskHistory(task_history[i].job_id, task_history[i], function (getTaskHistoryResult) {
                                getTaskHistoryResult.task.task_history = getTaskHistoryResult.task_history;
                                allResponses.push(getTaskHistoryResult.task);
                                changeIndex();
                            });
                        })(i);
                    });
                }
            }
        }
    }

    function findCustomData(task) {
        var index = 0, resultFinal = [];
        findCustom(index);
        function customFieldIndex() {
            index++;
            findCustom(index);
        }

        function findCustom(item) {
            if (item > task.length - 1) {
                exportData(resultFinal, fields, res);
            } else {
                commonFunc.delay2(item, function (i) {
                    (function (i) {
                        if ((typeof (task[i].fields.custom_field) != "undefined") && (task[i].fields.custom_field.length > 0)) {
                            task[i].fields.custom_field.forEach(function (field) {
                                field = commonFunc.processCustomField(field);
                                task[i][field.label] = field.fleet_data || field.data || '-';
                                task[i]['Custom_Template_ID'] = field.template_id || '-';
                            })
                        }
                        if (include_task_history) {

                            if (task[i].task_history.length > 0) {
                                var historyIndex = 0;
                                insertTaskHistory(historyIndex);
                                function changeTaskHistoryIndex() {
                                    historyIndex++;
                                    insertTaskHistory(historyIndex);
                                }

                                function insertTaskHistory(item1) {
                                    if (item1 > task[i].task_history.length - 1) {
                                        customFieldIndex();
                                    } else {
                                        commonFunc.delay2(item1, function (j) {
                                            (function (j) {
                                                if (task[i].task_history[j].type == constants.taskHistoryType.CUSTOM_FIELD) {
                                                    var desc = JSON.parse(task[i].task_history[j].description);
                                                    if (desc.length) {
                                                        task[i].task_history[j].description = desc[0].fleet_data;
                                                    } else {
                                                        task[i].task_history[j].description = desc.fleet_data;
                                                    }
                                                }
                                                task[i].Type = task[i].task_history[j].type;
                                                task[i].History_Description = task[i].task_history[j].description;
                                                task[i].Location = "http://maps.google.com/maps/api/staticmap?markers=" + task[i].task_history[j].latitude + "," + task[i].task_history[j].longitude + "&size=700x500&zoom=16";
                                                task[i].Time = commonFunc.convertTimeIntoLocal(task[i].task_history[j].creation_datetime, task[i].timezone).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                if (j > 0) {
                                                    task[i].Task_Type = '';
                                                    task[i].Order_ID = '';
                                                    task[i].Fleet_Name = '';
                                                    task[i].Notes = '';
                                                    task[i].Start_Time = '';
                                                    task[i].End_Time = '';
                                                    task[i].Customer_Name = '';
                                                    task[i].Customer_Address = '';
                                                    task[i].Task_Status = '';
                                                    task[i].Team_Name = '';
                                                    task[i].Agent_Name = '';
                                                    task[i].Relationship = '';
                                                    task[i].Distance = '';
                                                    if ((typeof (task[i].fields.custom_field) != "undefined") && (task[i].fields.custom_field.length > 0)) {
                                                        task[i].fields.custom_field.forEach(function (field) {
                                                            task[i][field.label] = '';
                                                            task[i]['Custom_Template_ID'] = '';
                                                        })
                                                    }
                                                }
                                                resultFinal.push(JSON.parse(JSON.stringify(task[i])));
                                                changeTaskHistoryIndex();
                                            })(j);
                                        });
                                    }
                                }
                            } else {
                                task[i].Type = '-';
                                task[i].History_Description = '-';
                                task[i].Location = '-';
                                task[i].Time = '-';
                                resultFinal.push(JSON.parse(JSON.stringify(task[i])));
                                customFieldIndex();
                            }

                        } else {
                            resultFinal.push(JSON.parse(JSON.stringify(task[i])));
                            customFieldIndex();
                        }
                    })(i);
                });
            }
        }
    }
}

function processPDdata(result_jobs, result_jobs_length, user_id, layout_type, include_task_history, res) {
    var counter = 0, task_history = [];
    var fields = ['Task_ID', 'Order_ID', 'Team_Name', 'Relationship', 'Task_Type', 'Notes', 'Agent_Name', 'Distance', 'Pick_up_From', 'Pick_up_Before', 'Customer_Name', 'Customer_Address', 'Complete_Before', 'Task_Status'];

    mongo.getLayoutOptionalFieldsForUser(user_id, layout_type, function (Optionalfields) {
        if (Optionalfields.length > 0) {
            if ((typeof (Optionalfields[0].fields.custom_field) != 'undefined') && (Optionalfields[0].fields.custom_field.length > 0)) {
                fields.push('Custom_Template_ID');
                var custom_field = Optionalfields[0].fields.custom_field;
                custom_field.forEach(function (field) {
                    Object.keys(field).forEach(function (key) {
                        field[key].items.forEach(function (item) {
                            if (item.label) {
                                fields.push(item.label);
                            }
                        })
                    });
                });
            }
            if (include_task_history) {
                fields.push('Type', 'History_Description', 'Location', 'Time');
            }
            extractData(result_jobs, result_jobs_length);
        } else {
            if (include_task_history) {
                fields.push('Type', 'History_Description', 'Location', 'Time');
            }
            extractData(result_jobs, result_jobs_length);
        }

    })
    function extractData(result_jobs, result_jobs_length) {
        for (var i = 0; i < result_jobs_length; i++) {
            result_jobs[i].Task_ID = result_jobs[i].job_id;
            result_jobs[i].Relationship = result_jobs[i].pickup_delivery_relationship;
            result_jobs[i].Distance = (result_jobs[i].total_distance_travelled == null ||  result_jobs[i].total_distance_travelled == 'null') ? '0' : result_jobs[i].total_distance_travelled;
            result_jobs[i].Order_ID = result_jobs[i].order_id == null ? "-" : result_jobs[i].order_id;
            result_jobs[i].Team_Name = (result_jobs[i].team_id == 0 || result_jobs[i].team_id == null) ? "All Teams" : result_jobs[i].team_name;
            result_jobs[i].Notes = result_jobs[i].job_description;
            result_jobs[i].Task_Type = (result_jobs[i].job_type == constants.jobType.PICKUP ? "Pick-up" : result_jobs[i].job_type == constants.jobType.DELIVERY ? "Delivery" : result_jobs[i].job_type == constants.jobType.APPOINTMENT ? "Appointment" : result_jobs[i].job_type == constants.jobType.FOS ? "FOS" : "");
            result_jobs[i].Agent_Name = result_jobs[i].fleet_name == null ? "-" : result_jobs[i].fleet_name;
            result_jobs[i].Customer_Name = result_jobs[i].job_type == constants.jobType.PICKUP ? result_jobs[i].job_pickup_name : result_jobs[i].customer_username;
            result_jobs[i].Customer_Name = result_jobs[i].Customer_Name == null ? "-" : result_jobs[i].Customer_Name;
            result_jobs[i].Customer_Address = result_jobs[i].job_type == constants.jobType.PICKUP ? result_jobs[i].job_pickup_address : result_jobs[i].job_address;
            result_jobs[i].Pick_up_From = result_jobs[i].job_pickup_name;
            result_jobs[i].Pick_up_From = result_jobs[i].Pick_up_From == null ? "-" : result_jobs[i].Pick_up_From;
            if (result_jobs[i].job_status == constants.jobStatus.UNASSIGNED) {
                result_jobs[i].Task_Status = 'Unassigned';
            } else if (result_jobs[i].job_status == constants.jobStatus.UPCOMING) {
                result_jobs[i].Task_Status = 'Assigned';
            } else if (result_jobs[i].job_status == constants.jobStatus.STARTED) {
                result_jobs[i].Task_Status = 'In Transit';
            } else if (result_jobs[i].job_status == constants.jobStatus.ENDED) {
                result_jobs[i].Task_Status = 'Completed';
            } else if (result_jobs[i].job_status == constants.jobStatus.FAILED) {
                result_jobs[i].Task_Status = 'Failed';
            } else if (result_jobs[i].job_status == constants.jobStatus.ARRIVED) {
                result_jobs[i].Task_Status = 'Arrived';
            } else if (result_jobs[i].job_status == constants.jobStatus.PARTIAL) {
                result_jobs[i].Task_Status = 'Partial';
            } else if (result_jobs[i].job_status == constants.jobStatus.ACCEPTED) {
                result_jobs[i].Task_Status = 'Accepted';
            } else if (result_jobs[i].job_status == constants.jobStatus.DECLINE) {
                result_jobs[i].Task_Status = 'Declined';
            } else if (result_jobs[i].job_status == constants.jobStatus.CANCEL) {
                result_jobs[i].Task_Status = 'Canceled';
            }
            if (result_jobs[i].job_pickup_datetime != '0000-00-00 00:00:00' && result_jobs[i].job_pickup_datetime != null) {
                result_jobs[i].job_pickup_datetime = result_jobs[i].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
            }
            if (result_jobs[i].job_pickup_datetime == '0000-00-00 00:00:00') {
                result_jobs[i].job_pickup_datetime = '-';
            }
            if (result_jobs[i].job_delivery_datetime != '0000-00-00 00:00:00' && result_jobs[i].job_delivery_datetime != null) {
                result_jobs[i].job_delivery_datetime = result_jobs[i].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
            }
            if (result_jobs[i].job_delivery_datetime == '0000-00-00 00:00:00') {
                result_jobs[i].job_delivery_datetime = '-';
            }

            result_jobs[i].Pick_up_Before = result_jobs[i].job_pickup_datetime;
            result_jobs[i].Complete_Before = result_jobs[i].job_delivery_datetime;

            mongo.getOptionalFieldsForTask(result_jobs[i].user_id, result_jobs[i].job_id, result_jobs[i], function (custom_fields) {
                custom_fields.task.fields = custom_fields.fields;
                counter++;
                getTaskHistory(counter, result_jobs_length, custom_fields.task);
            });
        }
    }

    function getTaskHistory(counter, result_task_profile_length, result_task_profile) {
        task_history.push(result_task_profile);
        if (counter == result_task_profile_length) {
            var index = 0, allResponses = [];
            getHistory(index);
            function changeIndex() {
                index++;
                getHistory(index);
            }

            function getHistory(item) {
                if (item > task_history.length - 1) {
                    findCustomData(allResponses);
                } else {
                    commonFunc.delay2(item, function (i) {
                        (function (i) {
                            commonFunc.getTaskHistory(task_history[i].job_id, task_history[i], function (getTaskHistoryResult) {
                                getTaskHistoryResult.task.task_history = getTaskHistoryResult.task_history;
                                allResponses.push(getTaskHistoryResult.task);
                                changeIndex();
                            });
                        })(i);
                    });
                }
            }
        }
    }

    function findCustomData(task) {
        var index = 0, resultFinal = [];
        findCustom(index);
        function changeCustomIndex() {
            index++;
            findCustom(index);
        }

        function findCustom(item) {
            if (item > task.length - 1) {
                exportData(resultFinal, fields, res);
            } else {
                commonFunc.delay2(item, function (i) {
                    (function (i) {
                        if ((typeof (task[i].fields.custom_field) != "undefined") && (task[i].fields.custom_field.length > 0)) {
                            task[i].fields.custom_field.forEach(function (field) {
                                field = commonFunc.processCustomField(field);
                                task[i][field.label] = field.fleet_data || field.data || '-';
                                task[i]['Custom_Template_ID'] = field.template_id || '-';
                            })
                        }

                        if (include_task_history) {

                            if (task[i].task_history.length > 0) {
                                var historyIndex = 0;
                                insertTaskHistory(historyIndex);
                                function changeTaskHistoryIndex() {
                                    historyIndex++;
                                    insertTaskHistory(historyIndex);
                                }

                                function insertTaskHistory(item1) {
                                    if (item1 > task[i].task_history.length - 1) {
                                        changeCustomIndex();
                                    } else {
                                        commonFunc.delay2(item1, function (j) {
                                            (function (j) {
                                                if (task[i].task_history[j].type == constants.taskHistoryType.CUSTOM_FIELD) {
                                                    var desc = JSON.parse(task[i].task_history[j].description);
                                                    if (desc.length) {
                                                        task[i].task_history[j].description = desc[0].fleet_data;
                                                    } else {
                                                        task[i].task_history[j].description = desc.fleet_data;
                                                    }
                                                }
                                                task[i].Type = task[i].task_history[j].type;
                                                task[i].History_Description = task[i].task_history[j].description;
                                                task[i].Location = "http://maps.google.com/maps/api/staticmap?markers=" + task[i].task_history[j].latitude + "," + task[i].task_history[j].longitude + "&size=700x500&zoom=16";
                                                task[i].Time = commonFunc.convertTimeIntoLocal(task[i].task_history[j].creation_datetime, task[i].timezone).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                if (j > 0) {
                                                    task[i].Task_Type = '';
                                                    task[i].Order_ID = '';
                                                    task[i].Fleet_Name = '';
                                                    task[i].Notes = '';
                                                    task[i].Pick_up_From = '';
                                                    task[i].Pick_up_Before = '';
                                                    task[i].Customer_Name = '';
                                                    task[i].Customer_Address = '';
                                                    task[i].Complete_Before = '';
                                                    task[i].Task_Status = '';
                                                    task[i].Team_Name = '';
                                                    task[i].Agent_Name = '';
                                                    task[i].Relationship = '';
                                                    task[i].Distance = '';
                                                    if ((typeof (task[i].fields.custom_field) != "undefined") && (task[i].fields.custom_field.length > 0)) {
                                                        task[i].fields.custom_field.forEach(function (field) {
                                                            task[i][field.label] = '';
                                                            task[i]['Custom_Template_ID'] = '';
                                                        })
                                                    }
                                                }
                                                resultFinal.push(JSON.parse(JSON.stringify(task[i])));
                                                changeTaskHistoryIndex();
                                            })(j);
                                        });
                                    }
                                }
                            } else {
                                task[i].Type = '-';
                                task[i].History_Description = '-';
                                task[i].Location = '-';
                                task[i].Time = '-';
                                resultFinal.push(JSON.parse(JSON.stringify(task[i])));
                                changeCustomIndex();
                            }

                        } else {
                            resultFinal.push(JSON.parse(JSON.stringify(task[i])));
                            changeCustomIndex();
                        }
                    })(i);
                });
            }
        }
    }
}

