var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var schedule = require('node-schedule');
var async = require('async');
var sockets = require('./socket');
var notifications = require('./notification');
var customers = require('./customers');

function findTaskDescForSocketResponse(job_id, callback) {
    var sql = "SELECT job.* " +
        " FROM `tb_jobs` job " +
        " WHERE job.job_id = ? ";
    connection.query(sql, [job_id], function (err, job_result) {
        if (err) {
            console.log("ERROR IN FINDING JOB IN findUserForSocketResponse === ", job_result);
        }
        var teams = {}, fleet = {}, job, jobs = {}, driver = {};
        if (job_result.length > 0) {
            job = job_result[0];
            teams[job.team_id] = {};
            var sql = "SELECT " +
                " fleet.*,TIMESTAMPDIFF(SECOND,fleet.`location_update_datetime`,NOW()) as `last_updated_location_time` " +
                " FROM tb_fleets fleet " +
                " WHERE fleet.fleet_id = ? ";
            connection.query(sql, [job.fleet_id], function (err, fleet_result) {
                if (err) {
                    console.log("ERROR IN FINDING fleet IN findUserForSocketResponse === ", fleet_result);
                }
                driver.fleet_name = null;
                if (fleet_result.length > 0) {
                    driver = fleet_result[0];
                }
                if (job.fleet_id) {
                    teams[job.team_id].fleets = {};
                    teams[job.team_id].fleets[driver.fleet_id] = {};
                    fleet.fleet_id = driver.fleet_id;
                    fleet.fleet_name = driver.username;
                    fleet.fleet_thumb_image = driver.fleet_thumb_image;
                    fleet.longitude = driver.longitude;
                    fleet.battery_level = driver.battery_level;
                    fleet.store_version = driver.store_version;
                    fleet.latitude = driver.latitude;
                    fleet.status = driver.status;
                    fleet.is_available = driver.is_available;
                    fleet.device_type = driver.device_type;
                    fleet.avg_cust_rating = commonFunc.calculateAvgRating(driver.total_rating, driver.total_rated_tasks);
                    fleet.fleet_status_color = constants.fleetStatusColor[driver.status][driver.is_available];
                    driver.last_updated_location_time = parseInt(driver.last_updated_location_time);
                    if (driver.last_updated_location_time.toString() == "NaN") {
                        fleet.last_updated_timings = constants.highest.VALUE;
                        fleet.last_updated_location_time = 'Not updated.';
                    } else {
                        fleet.last_updated_timings = driver.last_updated_location_time;
                        fleet.last_updated_location_time = commonFunc.timeDifferenceInWords(driver.has_gps_accuracy, driver.is_available,driver.last_updated_location_time);
                    }
                    teams[job.team_id].fleets[driver.fleet_id] = fleet;
                }
                teams[job.team_id].jobs = {};
                teams[job.team_id].jobs[job.job_id] = {};
                jobs.job_id = job.job_id;
                jobs.team_id = job.team_id;
                jobs.is_routed = job.is_routed;
                jobs.job_status = job.job_status;
                jobs.job_type = job.job_type;
                jobs.fleet_id = job.fleet_id;
                jobs.fleet_name = driver.username;
                jobs.job_latitude = job.job_latitude;
                jobs.job_longitude = job.job_longitude;
                jobs.job_address = job.job_address;
                jobs.job_pickup_latitude = job.job_pickup_latitude;
                jobs.job_pickup_longitude = job.job_pickup_longitude;
                jobs.job_pickup_address = job.job_pickup_address;
                jobs.pickup_delivery_relationship = job.pickup_delivery_relationship;
                jobs.job_pickup_name = job.job_pickup_name;
                jobs.job_delivery_datetime = job.job_delivery_datetime
                jobs.job_pickup_datetime = job.job_pickup_datetime
                if (jobs.job_pickup_datetime && jobs.job_pickup_datetime != null && jobs.job_pickup_datetime != '0000-00-00 00:00:00') {
                    jobs.job_pickup_datetime = jobs.job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                }
                if (jobs.job_delivery_datetime && jobs.job_delivery_datetime != null && jobs.job_delivery_datetime != '0000-00-00 00:00:00') {
                    jobs.job_delivery_datetime = jobs.job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                }
                jobs.customer_username = job.customer_username == "dummy" ? "" : job.customer_username;
                jobs.auto_assignment = job.auto_assignment;
                if (job.job_time) jobs.job_time = new Date(job.job_time).getTime();
                teams[job.team_id].jobs[job.job_id] = jobs;
                callback({
                    user_id: job.user_id,
                    fleet_id: job.fleet_id,
                    team_id: job.team_id,
                    data: teams
                });
            });
        }
    })
}

exports.sendSocketResponse = function (job_id) {

    findTaskDescForSocketResponse(job_id, function (resp) {
        //User Notification
        notifications.emit_notifications(resp.user_id, resp.fleet_id, function (emitNotificationsResult) {
            sockets.get_redis_client_users(function (userSockets) {
                commonFunc.getAllUserAndTheirDispatchersWithPermission(resp.user_id, resp.team_id, function (getAllDispatchersOfUserResult) {
                    getAllDispatchersOfUserResult.forEach(function (users) {
                        if (users.is_dispatcher && users.view_task){
                            resp.data.disp_perm = 1;
                        }
                        userSockets.forEach(function (sockets) {
                            if (sockets.token == users.access_token) {
                                var response = {
                                    "socket_id": sockets.socket_id,
                                    "response": JSON.stringify(resp.data),
                                    "notification": emitNotificationsResult
                                }

                                process.emit('dashboardResponse', response);
                                process.emit('notificationResponse', response);
                                console.log("fleet response socket emit", users.access_token, sockets.socket_id);
                            }
                        });
                    });
                });
            });
        });
    });

    //Customer Notification
    commonFunc.getJobDetailsFromJobID(job_id,function(data) {
        if (data && data.length && data[0].job_hash && data[0].fleet_id) {
            customers.fetch_job_details(data[0].job_hash,function(respons) {
                if (respons && respons.data) {
                    sockets.get_redis_client_customers(function (customerSockets) {
                        customerSockets.forEach(function (sockets) {
                            if (sockets.fleet == data[0].fleet_id) {
                                var response = {
                                    "socket_id": sockets.socket_id,
                                    "response": JSON.stringify(respons.data)
                                }
                                process.emit('emitFleetTaskUpdateToCustomer', response);
                                console.log("customer socket while task update emit", sockets.fleet, sockets.socket_id);
                            }
                        })
                    })
                }
            })
        }
    });
}

exports.sendSocketResponseForPickAndDelivery = function(job_id){
    commonFunc.getBothTasksFromSingleID(job_id,function(result){
        if (result){
            result.forEach(function(jobs){
                module.exports.sendSocketResponse(jobs.job_id);
            })
        }
    })
}

exports.sendFleetSocketResponseFromFleetID = function (fleet_id, data) {
    var sql = "SELECT user_id,fleet_id,username,total_rating,total_rated_tasks,status,is_available,has_gps_accuracy,latitude,longitude,battery_level," +
        " TIMESTAMPDIFF(SECOND,fleet.`location_update_datetime`,NOW()) as `last_updated_location_time` " +
        " FROM  `tb_fleets` fleet " +
        " WHERE fleet.`fleet_id`=?";
    connection.query(sql, [fleet_id], function (err, result) {
        if (result.length > 0) {
            result[0].fleet_status_color = constants.fleetStatusColor[result[0].status][result[0].is_available];
            result[0].last_updated_location_time = parseInt(result[0].last_updated_location_time);
            if (result[0].last_updated_location_time.toString() == "NaN") {
                result[0].last_updated_location_time = 'Not updated.';
                result[0].last_updated_timings = constants.highest.VALUE;
            } else {
                result[0].last_updated_timings = result[0].last_updated_location_time;
                result[0].last_updated_location_time = commonFunc.timeDifferenceInWords(result[0].has_gps_accuracy,result[0].is_available,result[0].last_updated_location_time);
            }
            //User Notification
            sockets.get_redis_client_users(function (userSockets) {
                commonFunc.getAllUserAndTheirDispatchers(result[0].user_id, function (getAllDispatchersOfUserResult) {
                    getAllDispatchersOfUserResult.forEach(function (users) {
                        userSockets.forEach(function (sockets) {
                            if (sockets.token == users.access_token) {
                                var response = {
                                    "socket_id": sockets.socket_id,
                                    "response": {
                                        fleet_id : result[0].fleet_id,
                                        latitude : result[0].latitude,
                                        longitude : result[0].longitude,
                                        fleet_status_color : result[0].fleet_status_color,
                                        last_updated_timings : result[0].last_updated_timings,
                                        last_updated_location_time : result[0].last_updated_location_time,
                                        battery_level : result[0].battery_level
                                    }
                                }
                                process.emit('emitFleetLocationUpdateResponse', response);
                            }
                        });
                    });
                });
            });
            //Customer Notification
            sockets.get_redis_client_customers(function (customerSockets) {
                customerSockets.forEach(function (sockets) {
                    if (sockets.fleet == fleet_id) {
                        var response = {
                            "socket_id": sockets.socket_id,
                            "response": JSON.stringify(data.location)
                        }
                        process.emit('emitCustomerFleetLocationUpdateResponse', response);
                        console.log("customer socket fleet_location emit", sockets.fleet, sockets.socket_id);
                    }
                });
            });
        }
    });
}

/*---------------------------
 * SEND SOCKET RESPONSE (FLEET ONLY)
 * ---------------------------
 */
exports.sendFleetSocketResponse = function (user_id, fleet_id) {
    var teams = {};
    var sql = "SELECT teams.`team_id`,fleet.fleet_id,fleet.username,fleet.fleet_image,fleet.fleet_thumb_image,fleet.longitude,fleet.latitude," +
        " fleet.status,fleet.is_available,fleet.email,fleet.phone,TIMESTAMPDIFF(SECOND,fleet.`location_update_datetime`,NOW()) as `last_updated_location_time`, " +
        " fleet.`battery_level`,fleet.`has_gps_accuracy`,fleet.`has_network`,fleet.`device_desc`,fleet.`has_mock_loc`,fleet.`device_os`,fleet.`device_type`, " +
        " fleet.`total_rating`,fleet.`total_rated_tasks`,fleet.`store_version` " +
        " FROM `tb_fleet_teams` teams INNER JOIN `tb_fleets` fleet ON  fleet.fleet_id = teams.fleet_id " +
        " WHERE teams.`user_id`=? AND teams.`fleet_id`=?";
    connection.query(sql, [user_id, fleet_id], function (err, fleet_teams_result) {
        if (err) {
            console.log(err);
        }
        fleet_teams_result.forEach(function (team) {
            var fleet = {};
            if (fleet_id != 0) {
                teams[team.team_id] = {};
                teams[team.team_id].fleets = {};
                teams[team.team_id].fleets[fleet_id] = {};
                fleet.fleet_id = fleet_id;
                fleet.fleet_name = team.username;
                fleet.fleet_thumb_image = team.fleet_thumb_image;
                fleet.longitude = team.longitude;
                fleet.latitude = team.latitude;
                fleet.status = team.status;
                fleet.is_available = team.is_available;
                fleet.battery_level = team.battery_level;
                fleet.device_type = team.device_type;
                fleet.store_version = team.store_version;
                fleet.avg_cust_rating = commonFunc.calculateAvgRating(team.total_rating, team.total_rated_tasks);
                fleet.fleet_status_color = constants.fleetStatusColor[team.status][team.is_available];
                team.last_updated_location_time = parseInt(team.last_updated_location_time);
                if (team.last_updated_location_time.toString() == "NaN") {
                    fleet.last_updated_timings = constants.highest.VALUE;
                    fleet.last_updated_location_time = 'Not updated.';

                } else {
                    fleet.last_updated_timings = team.last_updated_location_time;
                    fleet.last_updated_location_time = commonFunc.timeDifferenceInWords(team.has_gps_accuracy,team.is_available,team.last_updated_location_time);
                }

                teams[team.team_id].fleets[fleet_id] = fleet;
            }
        });
        sockets.get_redis_client_users(function (userSockets) {
            commonFunc.getAllUserAndTheirDispatchers(user_id, function (getAllDispatchersOfUserResult) {
                getAllDispatchersOfUserResult.forEach(function (users) {
                    userSockets.forEach(function (sockets) {
                        if (sockets.token == users.access_token) {
                            var response = {
                                "socket_id": sockets.socket_id,
                                "response": JSON.stringify(teams)
                            }
                            process.emit('dashboardResponse', response);
                        }
                    });
                });
            });
        });
    });
}

exports.sendNotificationSocketResponse = function (user_id, fleet_id) {
    notifications.emit_notifications(user_id, fleet_id, function (emitNotificationsResult) {
        sockets.get_redis_client_users(function (userSockets) {
            commonFunc.getAllUserAndTheirDispatchers(user_id, function (getAllDispatchersOfUserResult) {
                getAllDispatchersOfUserResult.forEach(function (users) {
                    userSockets.forEach(function (sockets) {
                        if (sockets.token == users.access_token) {
                            var response = {
                                "socket_id": sockets.socket_id,
                                "notification": emitNotificationsResult
                            }
                            process.emit('notificationResponse', response);
                        }
                    });
                });
            });
        });
    });
}

exports.refresh_api = function (user_id) {
    sockets.get_redis_client_users(function (userSockets) {
        commonFunc.getAllUserAndTheirDispatchers(user_id, function (getAllDispatchersOfUserResult) {
            getAllDispatchersOfUserResult.forEach(function (users) {
                userSockets.forEach(function (sockets) {
                    if (sockets.token == users.access_token) {
                        var response = {
                            "socket_id": sockets.socket_id,
                            "message": "Refresh API"
                        }
                        process.emit('refreshAPI', response);
                    }
                });
            });
        });
    });
}
