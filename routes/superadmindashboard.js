var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var async = require('async');
var users = require('./users');
/*
 * -----------------------------------------------
 * SUPER ADMIN USER LOGIN VIA EMAIL AND PASSWORD
 * -----------------------------------------------
 */
exports.super_admin_login = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var manvalues = [email, password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var encrypted_pass = md5(password);
        var sql = "SELECT * FROM `tb_admin` WHERE `email`=?  LIMIT 1";
        connection.query(sql, [email], function (err, result_admin_info) {
            if (err) {
                logging.logDatabaseQueryError("Error in fetching super admin Info : ", err, result_admin_info);
                logging.addErrorLog("Error in fetching admin Info : ", err, result_admin_info);
                responses.sendError(res);
                return;
            } else {
                if (result_admin_info.length == 0) {
                    var response = {
                        "message": constants.responseMessages.INVALID_EMAIL_ID,
                        "status": constants.responseFlags.INVALID_EMAIL_ID,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    if (result_admin_info[0].password != encrypted_pass) {
                        var response = {
                            "message": constants.responseMessages.WRONG_PASSWORD,
                            "status": constants.responseFlags.WRONG_PASSWORD,
                            "data": {}
                        };
                        res.send(JSON.stringify(response));
                    } else {
                        var response = {
                            "message": constants.responseMessages.LOGIN_SUCCESSFULLY,
                            "status": constants.responseFlags.LOGIN_SUCCESSFULLY,
                            "data": result_admin_info
                        };
                        res.send(JSON.stringify(response));
                    }
                }
            }
        });
    }
}
/*
 * ---------------
 * VIEW ALL USERS
 * ---------------
 */
exports.view_all_users_new = function (req, res) {
    var admin_access_token = req.body.admin_access_token;
    var manvalues = [admin_access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        authenticateAdminAccessToken(admin_access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT users.`internal_user`,users.`access_token`,users.`last_login_datetime`,users.`layout_type`,users.`is_first_time_login`,users.`is_dispatcher`,users.`is_active`,users.`user_id`,users.`username`,users.`email`,users.`timezone`," +
                    "users.`phone`,users.`expiry_datetime`,users.`company_name`,users.`company_address`,users.`country`,users.`company_image`,users.`is_company_image_view`,users.`creation_datetime`," +
                    "users.`is_driver_image_view`,users.`constraint_type`," +
                    " (SELECT COUNT(`user_id`) FROM `tb_users` WHERE dispatcher_user_id = users.`user_id` AND is_dispatcher = 1) as `dispatcher_count`," +
                    "(SELECT COUNT(`fleet_id`) FROM `tb_fleets` WHERE user_id = users.`user_id`) as `fleet_count`, (SELECT COUNT(`job_id`) FROM `tb_jobs` WHERE user_id = users.`user_id`) as `job_count` " +
                    "FROM `tb_users` users WHERE users.`is_dispatcher`=0 AND users.`internal_user`=0";
                connection.query(sql, function (err, result_users) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching all user Info : ", err, result_users);
                        responses.sendError(res);
                        return;
                    } else {
                        var users_length = result_users.length;
                        if (users_length > 0) {
                            result_users = commonFunc.sortByKeyDesc(result_users, 'creation_datetime');
                            for (var i = 0; i < users_length; i++) {
                                result_users[i].creation_datetime = (commonFunc.convertTimeIntoLocal(result_users[i].creation_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                result_users[i].expiry_datetime = (commonFunc.convertTimeIntoLocal(result_users[i].expiry_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                result_users[i].last_login_datetime = result_users[i].last_login_datetime == '0000-00-00 00:00:00' ? '0000-00-00 00:00:00' : (commonFunc.convertTimeIntoLocal(result_users[i].last_login_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            }

                        }
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result_users
                        }
                        res.send(JSON.stringify(response));
                    }
                });
            }
        });
    }
}



/*
 * ------------------------------
 * VIEW ALL USERS WITH PAGINATION
 * ------------------------------
 */

exports.view_users_with_pagination = function (req, res) {

    var admin_access_token = req.query.admin_access_token;
    authenticateAdminAccessToken(admin_access_token, function (result) {
        if (result == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            var QueryBuilder = require('datatable');
            var tableDefinition = {
                sSelectSql: "" +
                "tb_users.user_id," +
                "tb_users.access_token," +
                "tb_users.username," +
                "tb_users.internal_user," +
                "tb_users.email," +
                "tb_users.phone," +
                "tb_users.company_name," +
                "tb_users.is_dispatcher," +
                "tb_users.company_address," +
                "tb_users.last_login_datetime," +
                "tb_users.creation_datetime," +
                "(SELECT COUNT(`user_id`) FROM `tb_users` WHERE dispatcher_user_id = tb_users.`user_id` AND is_dispatcher = 1) as disp_count ," +
                "(SELECT COUNT(`fleet_id`) FROM `tb_fleets` WHERE user_id = tb_users.`user_id`) as `fleet_count`," +
                "(SELECT COUNT(`job_id`) FROM `tb_jobs` WHERE user_id = tb_users.`user_id`) as job_count" ,

                sFromSql: "tb_users",
                sWhereAndSql: "tb_users.internal_user=0 and tb_users.is_dispatcher=0",
                aSearchColumns: [
                    "tb_users.user_id", "tb_users.username",
                    "tb_users.internal_user", "tb_users.email",
                    "tb_users.phone",
                    "tb_users.company_name",
                    "tb_users.company_address",
                    "tb_users.last_login_datetime",
                    "tb_users.creation_datetime"
                ],
                sCountColumnName: "tb_jobs.`job_id`",
                aoColumnDefs: [
                    {mData: "user_id", bSearchable: true},
                    {mData: "username", bSearchable: true},
                    {mData: "internal_user", bSearchable: true},
                    {mData: "email", bSearchable: true},
                    {mData: "phone", bSearchable: true},
                    {mData: "company_name", bSearchable: true},
                    {mData: "company_address", bSearchable: true},
                    {mData: "last_login_datetime", bSearchable: true},
                    {mData: "creation_datetime", bSearchable: true}
                ]
            };
            var queryBuilder = new QueryBuilder(tableDefinition);
            var requestQuery = req.query;
            var queries = queryBuilder.buildQuery(requestQuery);
            if (queries.length > 2) {
                queries = queries.splice(1);
            }
            queries = queries.join(" ");
            connection.query(queries, function (err, resultAllRidesData) {
                if (err) {
                    logging.logDatabaseQueryError("Error in fetching users in view_users_with_pagination in super-admin : ", err, resultAllRidesData);
                    responses.sendError(res);
                    return;
                }
                var resultAllRides = resultAllRidesData.pop();
                var resultAllRidesLength = resultAllRides.length;
                if (resultAllRidesLength > 0) {
                    var arrResultRides = new Array();
                    for (var i = 0; i < resultAllRidesLength; i++) {
                        arrResultRides[i] = new Array();

                        arrResultRides[i][0] = resultAllRides[i].user_id;
                        arrResultRides[i][1] = resultAllRides[i].username;
                        arrResultRides[i][2] = resultAllRides[i].internal_user;
                        arrResultRides[i][3] = resultAllRides[i].email;
                        arrResultRides[i][4] = resultAllRides[i].phone;
                        arrResultRides[i][5] = resultAllRides[i].company_name;
                        arrResultRides[i][6] = resultAllRides[i].company_address;
                        arrResultRides[i][7] = resultAllRides[i].last_login_datetime;
                        arrResultRides[i][8] = resultAllRides[i].creation_datetime;
                        arrResultRides[i][9] = resultAllRides[i].disp_count;
                        arrResultRides[i][10] = resultAllRides[i].fleet_count;
                        arrResultRides[i][11] = resultAllRides[i].job_count;
                        arrResultRides[i][12] = "<a target=_blank ng-click=loginAs('"+resultAllRides[i].access_token+"')>User</a>";
                    }
                    var response = {
                        "iTotalDisplayRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "sEcho": 0,
                        "aaData": arrResultRides
                    };
                    res.send(JSON.stringify(response));
                    return;

                } else {
                    var response = {
                        "iTotalDisplayRecords": 0,
                        "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "sEcho": requestQuery.sEcho,
                        "aaData": []
                    };
                    res.send(JSON.stringify(response));
                    return;
                }
            });
        }
    });
};





/*
 * ----------------
 * VIEW ALL USERS
 * ----------------
 */
exports.view_all_users = function (req, res) {
    var sql = "SELECT users.`last_login_datetime`,users.`layout_type`,users.`is_first_time_login`,users.`is_dispatcher`,users.`is_active`,users.`user_id`,users.`username`,users.`email`,users.`timezone`," +
        "users.`phone`,users.`expiry_datetime`,users.`company_name`,users.`company_address`,users.`country`,users.`company_image`,users.`is_company_image_view`,users.`creation_datetime`," +
        "users.`is_driver_image_view`,users.`constraint_type`, (SELECT COUNT(`user_id`) FROM `tb_users` WHERE dispatcher_user_id = users.`user_id` AND is_dispatcher = 1) as `dispatcher_count`," +
        "(SELECT COUNT(`fleet_id`) FROM `tb_fleets` WHERE user_id = users.`user_id`) as `fleet_count`, (SELECT COUNT(`job_id`) FROM `tb_jobs` WHERE user_id = users.`user_id`) as `job_count`, " +
        "(SELECT COUNT(`job_id`) FROM `tb_jobs` WHERE user_id = users.`user_id` AND `completed_datetime` <> '0000-00-00 00:00:00' ) as `completed_task_count` " +
        "FROM `tb_users` users WHERE users.`is_dispatcher`=0 ORDER BY `creation_datetime` DESC";
    connection.query(sql, function (err, result_users) {
        if (err) {
            console.log(err);
            logging.logDatabaseQueryError("Error in fetching user Info : ", err, result_users);
            responses.sendError(res);
            return;
        } else {
            var response = "<table border='1'><tr><td><b>User ID</b></td><td><b>Name</b></td><td><b>Email</b></td><td><b>Phone</b></td><td><b>Company Name</b></td>" +
                "<td><b>Creation Datetime</b></td><td><b>Expiry Datetime</b></td><td><b>Last Login</b></td><td><b>Fleets</b></td><td><b>Dispatchers</b></td><td><b>Jobs</b></td>" +
                "<td><b>Completed Tasks</b></td></tr>";
            var users_length = result_users.length;
            for (var i = 0; i < users_length; i++) {
                var creation_datetime = commonFunc.convertTimeIntoLocal(result_users[i].creation_datetime, result_users[i].timezone);
                var expiry_datetime = commonFunc.convertTimeIntoLocal(result_users[i].expiry_datetime, result_users[i].timezone);
                var last_login_datetime = result_users[i].last_login_datetime == '0000-00-00 00:00:00' ? result_users[i].last_login_datetime : (commonFunc.convertTimeIntoLocal(result_users[i].last_login_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                if (result_users[i].email.indexOf('clicklabs') > 1 || result_users[i].email.indexOf('tookanapp') > 1 || result_users[i].email.indexOf('click-labs') > 1) {
                    response += "<tr style='background-color:lightgray' >";
                } else {
                    response += "<tr>";
                }
                response += "<td>" + result_users[i].user_id + "</td>";
                response += "<td>" + result_users[i].username + "</td>";
                response += "<td>" + result_users[i].email + "</td>";
                response += "<td>" + result_users[i].phone + "</td>";
                response += "<td>" + result_users[i].company_name + "</td>";
                response += "<td>" + creation_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '') + "</td>";
                response += "<td>" + expiry_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '') + "</td>";
                response += "<td>" + last_login_datetime + "</td>";
                response += "<td><center>" + result_users[i].fleet_count + "    <a href='view_all_fleets_of_user?user_id=" + result_users[i].user_id + "'><br>View</a></center></td>";
                response += "<td><center>" + result_users[i].dispatcher_count + "    <a href='view_all_dispatchers_of_user?user_id=" + result_users[i].user_id + "'><br>View</a></center></td>";
                response += "<td><center>" + result_users[i].job_count + "    <a href='view_all_jobs_of_user?user_id=" + result_users[i].user_id + "'><br>View</a></center></td>";
                response += "<td><center>" + result_users[i].completed_task_count + "</center></td>";
                response += "</tr>";
            }
            response += "</table>";
            res.send(response);
        }
    });
}
/*
 * ----------------------------------
 * VIEW ALL USERS TODAY
 * ----------------------------------
 */
exports.users_today_activity = function (req, res) {
    var selectedDate = "NOW()";
    if (req.query.date) {
        selectedDate = "'" + req.query.date + "'";
    }
    var sql = "SELECT users.`last_login_datetime`,users.`layout_type`,users.`is_first_time_login`,users.`is_dispatcher`,users.`is_active`,users.`user_id`,users.`username`,users.`email`,users.`timezone`," +
        "users.`phone`,users.`expiry_datetime`,users.`company_name`,users.`company_address`,users.`country`,users.`company_image`,users.`is_company_image_view`,users.`creation_datetime`," +
        "users.`is_driver_image_view`,users.`constraint_type`," +
        " (SELECT COUNT(`user_id`) FROM `tb_users` WHERE dispatcher_user_id = users.`user_id` AND is_dispatcher = 1 AND (DATE(`creation_datetime`) = DATE(" + selectedDate + ")) ) as `today_dispatcher_count`," +
        "(SELECT COUNT(`fleet_id`) FROM `tb_fleets` WHERE user_id = users.`user_id` AND (DATE(`creation_datetime`) = DATE(" + selectedDate + "))) as `today_fleet_count`, " +
        "(SELECT COUNT(`job_id`) FROM `tb_jobs` WHERE user_id = users.`user_id` AND (DATE(`creation_datetime`) = DATE(" + selectedDate + "))) as `today_job_count`, " +
        "(SELECT COUNT(`job_id`) FROM `tb_jobs` WHERE user_id = users.`user_id` AND DATE(`creation_datetime`) = DATE(" + selectedDate + ") AND `completed_datetime` <> '0000-00-00 00:00:00' ) as `completed_task_count` " +
        "FROM `tb_users` users WHERE users.`is_dispatcher`=0 ORDER BY `creation_datetime` DESC";
    connection.query(sql, function (err, result_users) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching user Info : ", err, result_users);
            responses.sendError(res);
            return;
        } else {
            var disp = 0, fleet = 0, tasks = 0, comp_tasks = 0;
            var response = "<table border='1'>" +
                "<tr>" +
                "<td><b>User ID</b></td>" +
                "<td><b>Name</b></td>" +
                "<td><b>Email</b></td>" +
                "<td><b>Work Flow</b></td>" +
                "<td><b>Phone</b></td>" +
                "<td><b>Company Name</b></td>" +
                "<td><b>Creation Datetime</b></td>" +
                "<td><b>Expiry Datetime</b></td>" +
                "<td><b>Last Login</b></td>" +
                "<td><b>Today Fleets</b></td>" +
                "<td><b>Today Dispatchers</b></td>" +
                "<td><b>Today Jobs</b></td>" +
                "<td><b>Today Completed Tasks</b></td>" +
                "</tr>";
            var users_length = result_users.length;
            for (var i = 0; i < users_length; i++) {
                if ((result_users[i].today_fleet_count == 0) && (result_users[i].today_dispatcher_count == 0) && (result_users[i].today_job_count == 0)) {
                }
                else {
                    tasks += parseInt(result_users[i].today_job_count);
                    fleet += parseInt(result_users[i].today_fleet_count);
                    disp += parseInt(result_users[i].today_dispatcher_count);
                    comp_tasks += parseInt(result_users[i].completed_task_count);
                    var creation_datetime = commonFunc.convertTimeIntoLocal(result_users[i].creation_datetime, result_users[i].timezone);
                    var expiry_datetime = commonFunc.convertTimeIntoLocal(result_users[i].expiry_datetime, result_users[i].timezone);
                    var last_login_datetime = result_users[i].last_login_datetime == '0000-00-00 00:00:00' ? result_users[i].last_login_datetime : (commonFunc.convertTimeIntoLocal(result_users[i].last_login_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                    if (result_users[i].email.indexOf('clicklabs') > 1 || result_users[i].email.indexOf('tookanapp') > 1 || result_users[i].email.indexOf('click-labs') > 1) {
                        response += "<tr style='background-color:lightgray' >";
                    } else {
                        response += "<tr>";
                    }
                    response += "<td>" + result_users[i].user_id + "</td>";
                    response += "<td>" + result_users[i].username + "</td>";
                    response += "<td>" + result_users[i].email + "</td>";
                    response += "<td>";
                    if (result_users[i].layout_type == constants.layoutType.PICKUP_AND_DELIVERY) {
                        response += "PICKUP_AND_DELIVERY";
                    } else if (result_users[i].layout_type == constants.layoutType.APPOINTMENT) {
                        response += "APPOINTMENT";
                    } else if (result_users[i].layout_type == constants.layoutType.FOS) {
                        response += "FOS";
                    }
                    response += "</td>";
                    response += "<td>" + result_users[i].phone + "</td>";
                    response += "<td>" + result_users[i].company_name + "</td>";
                    response += "<td>" + creation_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '') + "</td>";
                    response += "<td>" + expiry_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '') + "</td>";
                    response += "<td>" + last_login_datetime + "</td>";
                    if (result_users[i].today_fleet_count > 0) {
                        response += "<td><center>" + result_users[i].today_fleet_count + "    <a href='view_all_fleets_of_user?user_id=" + result_users[i].user_id + "'><br>View</a></center></td>";
                    } else {
                        response += "<td></td>";
                    }
                    if (result_users[i].today_dispatcher_count > 0) {
                        response += "<td><center>" + result_users[i].today_dispatcher_count + "    <a href='view_all_dispatchers_of_user?user_id=" + result_users[i].user_id + "'><br>View</a></center></td>";
                    } else {
                        response += "<td></td>";
                    }
                    if (result_users[i].today_job_count > 0) {
                        response += "<td><center>" + result_users[i].today_job_count + "    <a href='view_all_jobs_of_user?user_id=" + result_users[i].user_id + "'><br>View</a></center></td>";
                    } else {
                        response += "<td></td>";
                    }
                    if (result_users[i].completed_task_count > 0) {
                        response += "<td><center>" + result_users[i].completed_task_count + "</center></td>";
                    } else {
                        response += "<td></td>";
                    }

                    response += "</tr>";
                }
            }
            response +="<tr>" +
            "<td><b></b></td>" +
            "<td><b></b></td>" +
            "<td><b></b></td>" +
            "<td><b></b></td>" +
            "<td><b></b></td>" +
            "<td><b></b></td>" +
            "<td><b></b></td>" +
            "<td><b></b></td>" +
            "<td><b></b></td>" +
            "<td><b style='font-size: 50px'>"+fleet+"</b></td>" +
            "<td><b style='font-size: 50px'>"+disp+"</b></td>" +
            "<td><b style='font-size: 50px'>"+tasks+"</b></td>" +
            "<td><b style='font-size: 50px'>"+comp_tasks+"</b></td>" +
            "</tr>";
            response += "</table>";
            res.send(response);
        }
    });
}
/*
 * ----------------------------------
 * VIEW ALL FLEETS OF USER (NEW)
 * ----------------------------------
 */
exports.view_all_fleets_of_user_new = function (req, res) {

    var admin_access_token = req.body.admin_access_token;
    var user_id = req.body.user_id;
    var manvalues = [admin_access_token, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        authenticateAdminAccessToken(admin_access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT fleets.*,users.`internal_user` FROM `tb_fleets` fleets INNER JOIN tb_users users ON users.`user_id` = fleets.`user_id` " +
                    "WHERE fleets.`user_id` = ? AND users.`internal_user` = 0";
                connection.query(sql, [user_id], function (err, result_fleets) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching fleets Info : ", err, result_fleets);
                        responses.sendError(res);
                        return;
                    } else {
                        var users_length = result_fleets.length;
                        if (users_length > 0) {
                            result_fleets = commonFunc.sortByKeyDesc(result_fleets, 'creation_datetime');
                            for (var i = 0; i < users_length; i++) {
                                result_fleets[i].creation_datetime = (commonFunc.convertTimeIntoLocal(result_fleets[i].creation_datetime, result_fleets[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                result_fleets[i].last_login_datetime = result_fleets[i].last_login_datetime == '0000-00-00 00:00:00' ? result_fleets[i].last_login_datetime : (commonFunc.convertTimeIntoLocal(result_fleets[i].last_login_datetime, result_fleets[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            }
                        }
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result_fleets
                        }
                        res.send(JSON.stringify(response));
                    }
                });
            }
        });
    }
}


/*
 * ----------------------------------
 * VIEW ALL FLEETS OF USER
 * ----------------------------------
 */
exports.view_all_fleets_of_user = function (req, res) {

    var sql = "SELECT * FROM `tb_fleets` WHERE `user_id` = ?";
    connection.query(sql, [req.query.user_id], function (err, result_fleets) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching fleets Info : ", err, result_fleets);
            responses.sendError(res);
            return;
        } else {
            var response = "<table border='1'><tr><td><b>Fleet ID</b></td><td><b>Name</b></td><td><b>Email</b></td><td><b>Registration Status</b></td>" +
                "<td><b>Phone</b></td><td><b>Creation datetime</b></td><td><b>Last Login</b></td></tr>";
            var users_length = result_fleets.length;
            for (var i = 0; i < users_length; i++) {
                var creation_datetime = commonFunc.convertTimeIntoLocal(result_fleets[i].creation_datetime, result_fleets[i].timezone);
                var last_login_datetime = result_fleets[i].last_login_datetime;
                response += "<tr>";
                response += "<td>" + result_fleets[i].fleet_id + "</td>";
                response += "<td>" + result_fleets[i].username + "</td>";
                response += "<td>" + result_fleets[i].email + "</td>";
                response += "<td>" + result_fleets[i].registration_status + "</td>";
                response += "<td>" + result_fleets[i].phone + "</td>";
                response += "<td>" + creation_datetime + "</td>";
                response += "<td>" + last_login_datetime + "</td>";
                response += "</tr>";
            }
            response += "</table>";
            res.send(response);
            return;
        }
    });

}


/*
 * ----------------------------------
 * VIEW ALL JOBS OF USER (NEW)
 * ----------------------------------
 */

exports.view_all_jobs_of_user_new = function (req, res) {

    var admin_access_token = req.body.admin_access_token;
    var user_id = req.body.user_id;
    var manvalues = [admin_access_token, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        authenticateAdminAccessToken(admin_access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT jobs.*,users.`internal_user` FROM `tb_jobs` jobs INNER JOIN tb_users users ON users.`user_id` = jobs.`user_id` " +
                    "WHERE jobs.`user_id` = ? ";
                connection.query(sql, [user_id], function (err, result_jobs) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching jobs Info : ", err, result_jobs);
                        responses.sendError(res);
                        return;
                    } else {
                        var jobs_length = result_jobs.length;
                        if (jobs_length > 0) {
                            result_jobs = commonFunc.sortByKeyDesc(result_jobs, 'creation_datetime');
                            for (var i = 0; i < jobs_length; i++) {
                                result_jobs[i].creation_datetime = (commonFunc.convertTimeIntoLocal(result_jobs[i].creation_datetime, result_jobs[i].timezone));
                            }
                        }
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result_jobs
                        }
                        res.send(JSON.stringify(response));
                    }
                });
            }
        });
    }
}

/*
 * ----------------------------------
 * VIEW ALL JOBS OF USER
 * ----------------------------------
 */

exports.view_all_jobs_of_user = function (req, res) {

    var sql = "SELECT * FROM `tb_jobs` WHERE `user_id` = ? ORDER BY `creation_datetime` DESC";
    connection.query(sql, [req.query.user_id], function (err, result_jobs) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching jobs Info : ", err, result_jobs);
            responses.sendError(res);
            return;
        } else {
            var response = "<table border='1'><tr><td><b>Job ID</b></td><td><b>Job Type</b></td><td><b>Job Status</b></td><td><b>Creation Datetime</b></td>" +
                "<td><b>Job Datetime</b></td></tr>";
            var users_length = result_jobs.length;
            for (var i = 0; i < users_length; i++) {
                var creation_datetime = commonFunc.convertTimeIntoLocal(result_jobs[i].creation_datetime, result_jobs[i].timezone);
                response += "<tr>";
                response += "<td>" + result_jobs[i].job_id + "</td>";
                response += "<td>" + result_jobs[i].job_type + "</td>";
                response += "<td>" + result_jobs[i].job_status + "</td>";
                response += "<td>" + creation_datetime + "</td>";
                response += "<td>" + result_jobs[i].job_time + "</td>";
                response += "</tr>";
            }
            response += "</table>";
            res.send(response);
        }
    });

}

/*
 * ----------------------------------
 * VIEW ALL DISPATCHERS OF USER (New)
 * ----------------------------------
 */

exports.view_all_dispatchers_of_user_new = function (req, res) {

    var admin_access_token = req.body.admin_access_token;
    var user_id = req.body.user_id;
    var manvalues = [admin_access_token, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        authenticateAdminAccessToken(admin_access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "SELECT users.`internal_user`,users.`last_login_datetime`,users.`layout_type`,users.`is_first_time_login`,users.`is_dispatcher`,users.`is_active`,users.`user_id`,users.`username`,users.`email`,users.`timezone`," +
                    "users.`phone`,users.`expiry_datetime`,users.`company_name`,users.`company_address`,users.`country`,users.`company_image`,users.`is_company_image_view`,users.`creation_datetime`," +
                    "users.`is_driver_image_view`,users.`constraint_type`, " +
                    "(SELECT COUNT(`fleet_id`) FROM `tb_fleets` WHERE dispatcher_id = users.`user_id`) as `fleet_count`, (SELECT COUNT(`job_id`) FROM `tb_jobs` WHERE dispatcher_id = users.`user_id`) as `job_count` " +
                    "FROM `tb_users` users WHERE users.`is_dispatcher`=1 AND users.`dispatcher_user_id`=?";
                connection.query(sql, [user_id], function (err, result_users) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in fetching user Info : ", err, result_users);
                        responses.sendError(res);
                        return;
                    } else {
                        var users_length = result_users.length;
                        if (users_length > 0) {
                            result_users = commonFunc.sortByKeyDesc(result_users, 'creation_datetime');
                            for (var i = 0; i < users_length; i++) {
                                result_users[i].creation_datetime = (commonFunc.convertTimeIntoLocal(result_users[i].creation_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                result_users[i].expiry_datetime = (commonFunc.convertTimeIntoLocal(result_users[i].expiry_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                result_users[i].last_login_datetime = result_users[i].last_login_datetime == '0000-00-00 00:00:00' ? '0000-00-00 00:00:00' : (commonFunc.convertTimeIntoLocal(result_users[i].last_login_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                            }
                        }
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": result_users
                        }
                        res.send(JSON.stringify(response));
                    }
                });
            }
        });
    }
}
/*
 * ----------------------------------
 * VIEW ALL DISPATCHERS OF USER
 * ----------------------------------
 */
exports.view_all_dispatchers_of_user = function (req, res) {

    var sql = "SELECT users.`last_login_datetime`,users.`layout_type`,users.`is_first_time_login`,users.`is_dispatcher`,users.`is_active`,users.`user_id`,users.`username`,users.`email`,users.`timezone`," +
        "users.`phone`,users.`expiry_datetime`,users.`company_name`,users.`company_address`,users.`country`,users.`company_image`,users.`is_company_image_view`,users.`creation_datetime`," +
        "users.`is_driver_image_view`,users.`constraint_type`, " +
        "(SELECT COUNT(`fleet_id`) FROM `tb_fleets` WHERE dispatcher_id = users.`user_id`) as `fleet_count`, (SELECT COUNT(`job_id`) FROM `tb_jobs` WHERE dispatcher_id = users.`user_id`) as `job_count` " +
        "FROM `tb_users` users WHERE users.`is_dispatcher`=1 AND users.`dispatcher_user_id`=? ORDER BY `creation_datetime` DESC";
    connection.query(sql, [req.query.user_id], function (err, result_users) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching user Info : ", err, result_users);
            responses.sendError(res);
            return;
        } else {
            var response = "<table border='1'><tr><td><b>User ID</b></td><td><b>Name</b></td><td><b>Email</b></td><td><b>Phone</b></td><td><b>Company Name</b></td>" +
                "<td><b>Creation Datetime</b></td><td><b>Expiry Datetime</b></td><td><b>Last Login</b></td><td><b>Fleets</b></td><td><b>Jobs</b></td></tr>";
            var users_length = result_users.length;
            for (var i = 0; i < users_length; i++) {
                var creation_datetime = commonFunc.convertTimeIntoLocal(result_users[i].creation_datetime, result_users[i].timezone);
                var expiry_datetime = commonFunc.convertTimeIntoLocal(result_users[i].expiry_datetime, result_users[i].timezone);
                var last_login_datetime = result_users[i].last_login_datetime == '0000-00-00 00:00:00' ? result_users[i].last_login_datetime : (commonFunc.convertTimeIntoLocal(result_users[i].last_login_datetime, result_users[i].timezone)).toISOString().replace(/T/, ' ').replace(/\..+/, '');
                response += "<tr>";
                response += "<td>" + result_users[i].user_id + "</td>";
                response += "<td>" + result_users[i].username + "</td>";
                response += "<td>" + result_users[i].email + "</td>";
                response += "<td>" + result_users[i].phone + "</td>";
                response += "<td>" + result_users[i].company_name + "</td>";
                response += "<td>" + creation_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '') + "</td>";
                response += "<td>" + expiry_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '') + "</td>";
                response += "<td>" + last_login_datetime + "</td>";
                response += "<td><center>" + result_users[i].fleet_count + "</center></td>";
                response += "<td><center>" + result_users[i].job_count + "</center></td>";
                response += "</tr>";
            }
            response += "</table>";
            res.send(response);
            return;
        }
    });

}


/*
 * ----------------------------------
 * SEND BUILD UPDATE
 * ----------------------------------
 */

exports.send_update_push = function (req, res) {

    var device_type = req.body.device_type;
    var message = req.body.message;
    var manvalues = [device_type, message];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var sql = "SELECT `fleet_id` FROM `tb_fleets` " +
            "WHERE `device_type`=? AND `device_token` != 'deviceToken' AND `device_token` IS NOT NULL";
        connection.query(sql, [device_type], function (err, result_fleets) {
            if (err) {
                logging.logDatabaseQueryError("Error in fetching deviceToken Info : ", err, result_fleets);
                responses.sendError(res);
                return;
            } else {
                var fleets_length = result_fleets.length;
                if (fleets_length > 0) {
                    var counter = 0;
                    result_fleets.forEach(function (fleets) {

                        var payload_fleet = {
                            flag: constants.notificationFlags.BUILD_UPDATE,
                            message: message,
                            job_id: "",
                            d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                        };
                        var notificationFlag_driver = 1;
                        commonFunc.sendNotification(fleets.fleet_id, message, notificationFlag_driver, payload_fleet);
                        counter++;
                    });
                    if (counter == fleets_length) {
                        responses.actionCompleteResponse(res);
                        return;
                    }
                } else {
                    responses.actionCompleteResponse(res);
                    return;
                }
            }
        });
    }
}


/*
 * ---------------------
 * DASHBOARD's REPORTS
 * ---------------------
 */
exports.dashboard_report = function (req, res) {

    var admin_access_token = req.body.admin_access_token;
    var start_time = req.body.start_time;
    var end_time = req.body.end_time;
    var manvalues = [admin_access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        authenticateAdminAccessToken(admin_access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                start_time = start_time.split(" ")[0] + " 00:00:00";
                end_time = end_time.split(" ")[0] + " 23:59:59";
                async.parallel
                ([
                        //calling total users
                        function (callback) {
                            total_users(req, res, function (total_users_result) {
                                callback(null, total_users_result)
                            });
                        },
                        //calling total tasks function
                        function (callback) {
                            total_tasks(req, res, function (total_tasks_result) {
                                callback(null, total_tasks_result)
                            });
                        },
                        //calling total fleets function
                        function (callback) {
                            total_fleets(req, res, function (total_fleets_result) {
                                callback(null, total_fleets_result)
                            });
                        },
                        //calling total tasks for time specified
                        function (callback) {
                            total_tasks_for_time(start_time, end_time, req, res, function (total_tasks_for_time_result) {
                                callback(null, total_tasks_for_time_result)
                            });
                        },
                        function (callback) {
                            total_fleets_for_time(start_time, end_time, req, res, function (total_fleets_for_time_result) {
                                callback(null, total_fleets_for_time_result)
                            });
                        },
                        function (callback) {
                            total_users_for_time(start_time, end_time, req, res, function (total_users_for_time_result) {
                                callback(null, total_users_for_time_result)
                            });
                        },
                        //calling total tasks graph
                        function (callback) {
                            total_tasks_for_graph(start_time, end_time, req, res, function (total_tasks_for_graph_result) {
                                callback(null, total_tasks_for_graph_result)
                            });
                        },
                        //calling total fleet graph
                        function (callback) {
                            total_fleets_for_graph(start_time, end_time, req, res, function (total_fleets_for_graph_result) {
                                callback(null, total_fleets_for_graph_result)
                            });
                        }],
                    function (err, results) {
                        var response = {
                            "message": constants.responseMessages.ACTION_COMPLETE,
                            "status": constants.responseFlags.ACTION_COMPLETE,
                            "data": {
                                "total_users": results[0],
                                "tasks": results[1],
                                "total_fleets": results[2],
                                "total_tasks_for_time": results[3],
                                "total_fleets_for_time": results[4],
                                "total_users_for_time": results[5],
                                "total_tasks_graph": results[6],
                                "total_fleets_graph": results[7]
                            }
                        };
                        res.send(JSON.stringify(response));
                    });
            }
        });
    }
};
/*
 * --------------------------------
 * TOTAL USER OF THE SYSTEM
 * --------------------------------
 */
function total_users(req, res, callback) {

    var sql = "SELECT COUNT(`user_id`) as `users` FROM `tb_users` WHERE `is_dispatcher`=? and `internal_user`=0";
    connection.query(sql, [constants.isDispatcherStatus.NO], function (err, total_users) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total users : ", err, total_users);
            responses.sendError(res);
            return;
        }
        if (total_users.length > 0) {
            var response = total_users;
        } else {
            var response = '';
        }
        callback(response)
    });
}

/*
 * ------------------------------
 * TOTAL TASKS OF THE SYSTEM
 * ------------------------------
 */
function total_tasks(req, res, callback) {

    var sql = "SELECT jobs.`job_status`,jobs.`job_type` FROM `tb_jobs` jobs INNER JOIN `tb_users` users ON users.user_id=jobs.user_id  " +
        "WHERE users.internal_user=0";
    connection.query(sql, function (err, total_tasks_result) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total tasks : ", err, total_tasks_result);
            responses.sendError(res);
            return;
        }
        if (total_tasks_result.length > 0) {
            var total_tasks_result_length = total_tasks_result.length, successful_tasks = 0, pickup = 0, delivery = 0, appointment = 0;
            for (var i = 0; i < total_tasks_result_length; i++) {
                if (total_tasks_result[i].job_status == constants.jobStatus.ENDED) {
                    successful_tasks++;
                }
                if (total_tasks_result[i].job_type == constants.jobType.PICKUP) {
                    pickup++;
                } else if (total_tasks_result[i].job_type == constants.jobType.DELIVERY) {
                    delivery++;
                } else if (total_tasks_result[i].job_type == constants.jobType.APPOINTMENT) {
                    appointment++;
                }
            }
            var response = [{
                "total_tasks": total_tasks_result_length,
                "successful_tasks": successful_tasks,
                "pickups": pickup,
                "deliveries": delivery,
                "appointments": appointment
            }];
        } else {
            var response = '';
        }
        callback(response)
    });
}

/*
 * --------------------------------
 * TOTAL FLEETS OF THE SYSTEM
 * --------------------------------
 */

function total_fleets(req, res, callback) {

    var sql = "SELECT fleets.`fleet_id`,fleets.`registration_status`,users.`internal_user` FROM `tb_fleets` fleets " +
        "INNER JOIN `tb_users` users ON users.`user_id`=fleets.`user_id` " +
        "WHERE users.`internal_user`=0";
    connection.query(sql, function (err, total_fleets) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total fleets : ", err, total_fleets);
            responses.sendError(res);
            return;
        }
        var total_fleets_length = total_fleets.length, successful_register = 0, pending_registration = 0;
        if (total_fleets_length > 0) {
            for (var i = 0; i < total_fleets_length; i++) {
                if (total_fleets[i].registration_status == constants.userVerificationStatus.VERIFY) {
                    successful_register++;
                } else if (total_fleets[i].registration_status == constants.userVerificationStatus.NOT_VERIFY) {
                    pending_registration++;
                }
            }
            var response = [{
                "fleets": successful_register,
                "total_fleets_including_pending": parseInt(successful_register) + parseInt(pending_registration)
            }]

        } else {
            var response = '';
        }
        callback(response)
    });
}


/*
 * --------------------------------
 * TOTAL USER OF THE SYSTEM FOR PERIOD
 * --------------------------------
 */

function total_users_for_time(start_time, end_time, req, res, callback) {

    var sql = "SELECT COUNT(`user_id`) as `users` FROM `tb_users` WHERE `is_dispatcher`=? and `internal_user`=0 AND `creation_datetime`>=? AND `creation_datetime`<=?";
    connection.query(sql, [constants.isDispatcherStatus.NO, start_time, end_time], function (err, total_users) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total users : ", err, total_users);
            responses.sendError(res);
            return;
        }
        if (total_users.length > 0) {
            var response = total_users;
        } else {
            var response = '';
        }
        callback(response)
    });
}

/*
 * --------------------------------
 * TOTAL FLEETS OF THE SYSTEM FOR TIME PERIOD
 * --------------------------------
 */

function total_fleets_for_time(start_time, end_time, req, res, callback) {

    var sql = "SELECT fleets.`fleet_id`,fleets.`registration_status`,users.`internal_user` FROM `tb_fleets` fleets " +
        "INNER JOIN `tb_users` users ON users.`user_id`=fleets.`user_id` " +
        "WHERE users.`internal_user`=0 AND fleets.`creation_datetime`>=? AND fleets.`creation_datetime`<=?";
    connection.query(sql, [start_time, end_time], function (err, total_fleets) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total fleets : ", err, total_fleets);
            responses.sendError(res);
            return;
        }
        var total_fleets_length = total_fleets.length, successful_register = 0, pending_registration = 0;
        if (total_fleets_length > 0) {
            for (var i = 0; i < total_fleets_length; i++) {
                if (total_fleets[i].registration_status == constants.userVerificationStatus.VERIFY) {
                    successful_register++;
                } else if (total_fleets[i].registration_status == constants.userVerificationStatus.NOT_VERIFY) {
                    pending_registration++;
                }
            }
            var response = [{
                "fleets": successful_register,
                "total_fleets_including_pending": parseInt(successful_register) + parseInt(pending_registration)
            }]

        } else {
            var response = [];
        }
        callback(response)
    });
}

/*
 * -------------------------------------------------
 * TOTAL TASKS WITHIN TIME SPECIFIED OF THE SYSTEM
 * -------------------------------------------------
 */

function total_tasks_for_time(start_time, end_time, req, res, callback) {

    var sql = "SELECT COUNT(jobs.`job_id`) as `tasks` FROM `tb_jobs` jobs INNER JOIN `tb_users` users ON users.user_id=jobs.user_id  " +
        "WHERE users.internal_user=0 AND jobs.`creation_datetime`>=? AND jobs.`creation_datetime`<=?";
    connection.query(sql, [start_time, end_time], function (err, total_tasks) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total tasks within time: ", err, total_tasks);
            responses.sendError(res);
            return;
        }
        if (total_tasks.length > 0) {
            var response = total_tasks;
        } else {
            var response = '';
        }
        callback(response)
    });
}

/*
 * -------------------------------------------------
 * TOTAL TASKS WITHIN TIME SPECIFIED OF THE SYSTEM (GRAPH)
 * -------------------------------------------------
 */

function total_tasks_for_graph(start_time, end_time, req, res, callback) {

    var sql = "SELECT jobs.`job_id`,jobs.`creation_datetime`FROM `tb_jobs` jobs INNER JOIN `tb_users` users ON users.`user_id`=jobs.`user_id`  " +
        "WHERE users.`internal_user`=0 AND jobs.`creation_datetime`>=? AND jobs.`creation_datetime`<=?";
    connection.query(sql, [start_time, end_time], function (err, data) {
        if (err) {
            logging.logDatabaseQueryError("Error in fetching total tasks graph within time: ", err, data);
            responses.sendError(res);
            return;
        }
        if (data.length > 0) {

            var total = [], dates = [], start_date = new Date(start_time), end_date = new Date(end_time);
            getConsistentDatesIncluding(dates, start_date, end_date);

            var tasks = 0, date_length = dates.length, data_length = data.length, dataObjectearnings = {};
            for (var i = 0; i < date_length; i++) {
                tasks = 0;
                for (var j = 0; j < data_length; j++) {
                    if (new Date(data[j].creation_datetime).toDateString() == dates[i]) {
                        tasks++;
                        dataObjectearnings[dates[i]] = tasks;
                    }
                }
            }
            dates.forEach(function (date) {
                if (dataObjectearnings[date]) {
                    total.push(dataObjectearnings[date]);
                } else {
                    total.push(0);
                }
            });
            var counter = 0;
            dates.forEach(function (date) {
                dates[counter] = moment(date).format('MM/DD/YYYY');
                counter++;
            })
            var response = [{"dates": dates, "tasks": total}];
        } else {
            var response = [];
        }
        callback(response)
    });
}


/*
 * -------------------------------------------------
 * TOTAL fleets WITHIN TIME SPECIFIED OF THE SYSTEM (GRAPH)
 * -------------------------------------------------
 */

function total_fleets_for_graph(start_time, end_time, req, res, callback) {

    var previuosSQL = "SELECT COUNT(fleets.fleet_id) as previous_fleets FROM `tb_fleets` fleets INNER JOIN `tb_users` users ON users.user_id=fleets.user_id" +
        " WHERE users.internal_user=0  AND fleets.`creation_datetime` < ? AND fleets.`registration_status`=?"
    connection.query(previuosSQL, [start_time, constants.userVerificationStatus.VERIFY], function (err, previousFleets) {

        var sql = "SELECT fleets.`fleet_id`,fleets.`creation_datetime` FROM `tb_fleets` fleets INNER JOIN `tb_users` users ON users.user_id=fleets.user_id  " +
            "WHERE users.internal_user=0 AND fleets.`creation_datetime`>=? AND fleets.`creation_datetime`<=? AND fleets.`registration_status`=?";
        connection.query(sql, [start_time, end_time, constants.userVerificationStatus.VERIFY], function (err, data) {
            if (err) {
                logging.logDatabaseQueryError("Error in fetching total fleets graph within time: ", err, data);
                responses.sendError(res);
                return;
            }

            if (data.length > 0) {
                var total = [], dates = [], start_date = new Date(start_time), end_date = new Date(end_time);
                getConsistentDatesIncluding(dates, start_date, end_date);
                var fleets = previousFleets[0].previous_fleets, data_length = data.length, date_length = dates.length, dataObjectearnings = {};
                for (var i = 0; i < date_length; i++) {
                    //fleets=0;
                    for (var j = 0; j < data_length; j++) {
                        if (new Date(data[j].creation_datetime).toDateString() == dates[i]) {
                            fleets++;
                        }
                        dataObjectearnings[dates[i]] = fleets;
                    }
                }
                dates.forEach(function (date) {
                    if (dataObjectearnings[date]) {
                        total.push(dataObjectearnings[date]);
                    } else {
                        total.push(0);
                    }
                });
                var counter = 0;
                dates.forEach(function (date) {
                    dates[counter] = moment(date).format('MM/DD/YYYY');
                    counter++;
                })
                var response = [{"dates": dates, "fleets": total}];
            } else {
                var response = [];
            }
            callback(response)
        });
    });
}


/*
 * ------------------------------
 * VIEW ALL TASKS WITH PAGINATION
 * ------------------------------
 */

exports.view_all_tasks = function (req, res) {

    var admin_access_token = req.query.admin_access_token;
    authenticateAdminAccessToken(admin_access_token, function (result) {
        if (result == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            var QueryBuilder = require('datatable');
            var tableDefinition = {
                sSelectSql: "" +
                "tb_users.username," +
                "tb_users.email," +
                "tb_fleets.fleet_id," +
                "tb_fleets.username as fleet_name," +
                "tb_jobs.job_pickup_name," +
                "tb_jobs.job_latitude," +
                "tb_jobs.job_pickup_name," +
                "tb_jobs.job_pickup_phone," +
                "tb_jobs.job_longitude," +
                "tb_jobs.job_address," +
                "tb_jobs.job_status," +
                "tb_jobs.job_address," +
                "tb_jobs.job_description," +
                "tb_jobs.has_pickup," +
                "tb_jobs.completed_by_admin," +
                "tb_jobs.pickup_delivery_relationship," +
                "tb_jobs.job_pickup_datetime," +
                "tb_jobs.job_id," +
                "tb_jobs.has_pickup," +
                "tb_jobs.job_delivery_datetime," +
                "tb_jobs.job_type," +
                "tb_jobs.job_pickup_latitude," +
                "tb_jobs.job_pickup_longitude," +
                "tb_jobs.job_pickup_address," +
                "tb_jobs.job_pickup_email," +
                "tb_jobs.customer_id," +
                "tb_jobs.customer_username," +
                "tb_jobs.job_address," +
                "tb_jobs.customer_phone," +
                "tb_jobs.customer_email," +
                "tb_jobs.job_delivery_datetime ",

                sFromSql: "`tb_jobs` LEFT JOIN `tb_fleets` ON tb_fleets.`fleet_id`= tb_jobs.`fleet_id` LEFT JOIN `tb_users` ON tb_users.`user_id`= tb_jobs.`user_id`",
                sWhereAndSql: "tb_users.internal_user = 0",
                aSearchColumns: [
                    "tb_users.username", "tb_users.email", "tb_jobs.job_id", "tb_jobs.job_type", "tb_fleets.username", "tb_jobs.job_pickup_name", "tb_jobs.job_pickup_address",
                    "tb_jobs.job_pickup_datetime", "tb_jobs.customer_username", "tb_jobs.job_address", "tb_jobs.job_delivery_datetime", "tb_jobs.job_status"
                ],
                sCountColumnName: "tb_jobs.`job_id`",
                aoColumnDefs: [
                    {mData: "username", bSearchable: true},
                    {mData: "email", bSearchable: true},
                    {mData: "job_id", bSearchable: true},
                    {mData: "job_type", bSearchable: true},
                    {mData: "tb_fleets.username", bSearchable: true},
                    {mData: "job_pickup_name", bSearchable: true},
                    {mData: "job_pickup_address", bSearchable: true},
                    {mData: "job_pickup_datetime", bSearchable: true},
                    {mData: "customer_username", bSearchable: true},
                    {mData: "tb_jobs", bSearchable: true},
                    {mData: "job_delivery_datetime", bSearchable: true},
                    {mData: "job_status", bSearchable: true}
                ]
            };
            var queryBuilder = new QueryBuilder(tableDefinition);
            var requestQuery = req.query;
            var queries = queryBuilder.buildQuery(requestQuery);
            queries = queries.join(" ");
            connection.query(queries, function (err, resultAllRidesData) {
                if (err) {
                    logging.logDatabaseQueryError("Error in fetching jobs in view_all_tasks in super-admin : ", err, resultAllRidesData);
                    responses.sendError(res);
                    return;
                }
                var resultAllRides = resultAllRidesData.pop();
                var resultAllRidesLength = resultAllRides.length;
                if (resultAllRidesLength > 0) {
                    var arrResultRides = new Array();
                    for (var i = 0; i < resultAllRidesLength; i++) {
                        arrResultRides[i] = new Array();

                        if ((resultAllRides[i].customer_username == "dummy") || (resultAllRides[i].customer_username == null)) {
                            resultAllRides[i].customer_username = "";
                        }
                        if ((resultAllRides[i].customer_email == "dummy") || (resultAllRides[i].customer_email == null)) {
                            resultAllRides[i].customer_email = "";
                        }
                        if ((resultAllRides[i].customer_phone == "+910000000000") || (resultAllRides[i].customer_phone == null)) {
                            resultAllRides[i].customer_phone = "";
                        }

                        arrResultRides[i][0] = resultAllRides[i].job_id;
                        arrResultRides[i][1] = resultAllRides[i].username;
                        arrResultRides[i][2] = resultAllRides[i].email;
                        arrResultRides[i][3] = resultAllRides[i].job_type == constants.jobType.PICKUP ? "Pick-up" : resultAllRides[i].job_type == constants.jobType.DELIVERY ? "Delivery" : resultAllRides[i].job_type == constants.jobType.APPOINTMENT ? "Appointment" : "";
                        arrResultRides[i][4] = resultAllRides[i].fleet_id ? resultAllRides[i].fleet_name : '-';
                        arrResultRides[i][5] = resultAllRides[i].job_type == constants.jobType.PICKUP ? resultAllRides[i].has_pickup == constants.hasPickup.YES ? resultAllRides[i].job_pickup_name : "-" : resultAllRides[i].job_type == constants.jobType.DELIVERY ? "-" : "";
                        arrResultRides[i][6] = resultAllRides[i].job_type == constants.jobType.PICKUP ? resultAllRides[i].has_pickup == constants.hasPickup.YES ? resultAllRides[i].job_pickup_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a') : "-" : resultAllRides[i].job_type == constants.jobType.DELIVERY ? "-" : "";
                        arrResultRides[i][7] = resultAllRides[i].job_type == constants.jobType.DELIVERY ? resultAllRides[i].customer_username : resultAllRides[i].job_type == constants.jobType.PICKUP ? "-" : "";
                        arrResultRides[i][8] = resultAllRides[i].job_type == constants.jobType.PICKUP ? resultAllRides[i].job_pickup_address : resultAllRides[i].job_address;
                        arrResultRides[i][9] = resultAllRides[i].job_type == constants.jobType.DELIVERY ? resultAllRides[i].job_delivery_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a') : resultAllRides[i].job_type == constants.jobType.PICKUP ? "-" : "";
                        var job_status = "";
                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                            job_status = '<div class="label circle-success-unassigned" >Unassigned</div>';
                        } else if (resultAllRides[i].job_status == constants.jobStatus.UPCOMING) {
                            job_status = '<div class="label circle-success-assigned" >Assigned</div>';
                        } else if (resultAllRides[i].job_status == constants.jobStatus.STARTED) {
                            job_status = '<div class="label circle-success-intransit" >In Transit</div>';
                        } else if (resultAllRides[i].job_status == constants.jobStatus.ENDED) {
                            job_status = '<div class="label circle-success-completed" >Completed</div>';
                        } else if (resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                            job_status = '<div class="label circle-success-failed" >Failed</div>';
                        } else if (resultAllRides[i].job_status == constants.jobStatus.ARRIVED) {
                            job_status = '<div class="label circle-success-arrived" >Arrived</div>';
                        } else if (resultAllRides[i].job_status == constants.jobStatus.PARTIAL) {
                            job_status = '<div class="label circle-success-partial" >Partial</div>';
                        }


                        arrResultRides[i][10] = job_status;
                    }
                    var response = {
                        "iTotalDisplayRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "sEcho": 0,
                        "aaData": arrResultRides
                    };
                    res.send(JSON.stringify(response));
                    return;

                } else {
                    var response = {
                        "iTotalDisplayRecords": 0,
                        "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                        "sEcho": requestQuery.sEcho,
                        "aaData": []
                    };
                    res.send(JSON.stringify(response));
                    return;
                }
            });
        }
    });
};


/*
 * -----------------------------------------------
 * AUTHENTICATE Admin ACCESS TOKEN
 * -----------------------------------------------
 */
function authenticateAdminAccessToken(adminAccessToken, callback) {
    var sql = "SELECT * FROM `tb_admin` WHERE `access_token`=? LIMIT 1";
    connection.query(sql, [adminAccessToken], function (err, result) {
        if (result.length > 0) {
            return callback(result);
        } else {
            return callback(0);
        }
    });
};

function getConsistentDatesIncluding(dates, start_date, end_date) {
    dates.push(new Date(start_date).toDateString());
    var i = 0;
    while (commonFunc.timeDifferenceInDays(dates[i], end_date) > 0) {
        dates.push(addDay(dates[i]));
        i++;
    }
}


function addDay(date) {
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() + 86400000); // add a date
    return new Date(newDate).toDateString();
}