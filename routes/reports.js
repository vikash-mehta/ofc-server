var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var async = require('async');
/*
 * -----------------------------------------------
 * HEAT MAP OF TASKS
 * -----------------------------------------------
 */

exports.heat_map_tasks = function (req, res) {

    var access_token = req.body.access_token;
    var start_date = req.body.start_datetime;
    var end_date = req.body.end_datetime;
    var timezone = req.body.timezone;
    var manvalues = [access_token, start_date, end_date, timezone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var dates = [];
                var start_date1 = new Date(start_date);
                var end_date1 = new Date(end_date);
                //var start_date_duplicate = start_date1.setTime(start_date1.getTime()-(timezone*60*1000));
                //var end_date_duplicate = end_date1.setTime(end_date1.getTime()-(timezone*60*1000));
                getConsistentDatesIncluding(dates, start_date1, end_date1);

                var sql = "SELECT `job_type`,`job_status`,job_delivery_datetime " +
                    "FROM `tb_jobs` " +
                    "WHERE `job_delivery_datetime`>= ? AND `job_delivery_datetime` <= ?  AND `user_id` = ?";
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkPerm) {
                        if (chkPerm && chkPerm.length && chkPerm[0].view_driver == constants.hasPermissionStatus.NO) {
                            sql = "SELECT `job_type`,`job_status`,job_delivery_datetime " +
                                "FROM `tb_jobs` " +
                                "WHERE `job_delivery_datetime`>= ? AND `job_delivery_datetime` <= ?  AND `dispatcher_id` = ?";
                            step_in(sql, result[0].user_id);
                        }
                        else {
                            step_in(sql, result[0].dispatcher_user_id);
                        }
                    })
                } else {
                    step_in(sql, result[0].user_id);
                }


                function step_in(sql, user) {

                    connection.query(sql, [start_date, end_date, user], function (err, data) {
                        var upcoming = 0, intransit = 0, ended = 0, failed = 0, partial = 0, pickups = 0, deliveries = 0, unassigned = 0;
                        var prev_date = dates[0];
                        var upcomingArray = [], intransitArray = [], endedArray = [], failedArray = [], partialArray = [], pickupsArray = [], deliveriesArray = [], unassignedArray = [];
                        var flag;
                        var date_length = data.length;
                        var dataObjectupcoming = {}, dataObjectintransit = {}, dataObjectended = {}, dataObjectfailed = {}, dataObjectpartial = {}, dataObjectpickups = {}, dataObjectdeliveries = {}, dataObjectunassigned = {};
                        for (var i = 0; i < date_length; i++) {
                            if (new Date(data[i].job_delivery_datetime).toDateString() == prev_date) {

                                if (data[i].job_status == constants.jobStatus.UPCOMING) {
                                    upcoming++;
                                } else if (data[i].job_status == constants.jobStatus.STARTED) {
                                    intransit++;
                                } else if (data[i].job_status == constants.jobStatus.ENDED) {
                                    ended++;
                                } else if (data[i].job_status == constants.jobStatus.FAILED) {
                                    failed++;
                                } else if (data[i].job_status == constants.jobStatus.PARTIAL) {
                                    partial++;
                                }

                                if (data[i].job_type == constants.jobType.PICKUP) {
                                    pickups++;
                                } else if (data[i].job_type == constants.jobType.DELIVERY) {
                                    deliveries++;
                                } else if (data[i].job_type == constants.jobType.UNASSIGNED) {
                                    unassigned++;
                                }
                                flag = 0;
                            }
                            else {
                                dataObjectupcoming[prev_date] = upcoming;
                                dataObjectintransit[prev_date] = intransit;
                                dataObjectended[prev_date] = ended;
                                dataObjectfailed[prev_date] = failed;
                                dataObjectpartial[prev_date] = partial;
                                dataObjectpickups[prev_date] = pickups;
                                dataObjectdeliveries[prev_date] = deliveries;
                                dataObjectunassigned[prev_date] = unassigned;

                                upcoming = 0, intransit = 0, ended = 0, failed = 0, partial = 0, pickups = 0, deliveries = 0, unassigned = 0;
                                if (data[i].job_status == constants.jobStatus.UPCOMING) {
                                    upcoming = 1;
                                } else if (data[i].job_status == constants.jobStatus.STARTED) {
                                    intransit = 1;
                                } else if (data[i].job_status == constants.jobStatus.ENDED) {
                                    ended = 1;
                                } else if (data[i].job_status == constants.jobStatus.FAILED) {
                                    failed = 1;
                                } else if (data[i].job_status == constants.jobStatus.PARTIAL) {
                                    partial = 1;
                                }

                                if (data[i].job_type == constants.jobType.PICKUP) {
                                    pickups = 1;
                                } else if (data[i].job_type == constants.jobType.DELIVERY) {
                                    deliveries = 1;
                                } else if (data[i].job_type == constants.jobType.UNASSIGNED) {
                                    unassigned = 1;
                                }
                                prev_date = new Date(data[i].job_delivery_datetime).toDateString();
                                flag = 1;
                            }
                        }

                        if (flag == 0) {
                            dataObjectupcoming[prev_date] = upcoming;
                            dataObjectintransit[prev_date] = intransit;
                            dataObjectended[prev_date] = ended;
                            dataObjectfailed[prev_date] = failed;
                            dataObjectpartial[prev_date] = partial;
                            dataObjectpickups[prev_date] = pickups;
                            dataObjectdeliveries[prev_date] = deliveries;
                            dataObjectunassigned[prev_date] = unassigned;
                        }

                        dates.forEach(function (date) {
                            if (dataObjectupcoming[date]) {
                                upcomingArray.push(dataObjectupcoming[date]);
                            } else {
                                upcomingArray.push(0);
                            }
                            if (dataObjectintransit[date]) {
                                intransitArray.push(dataObjectintransit[date]);
                            } else {
                                intransitArray.push(0);
                            }
                            if (dataObjectended[date]) {
                                endedArray.push(dataObjectended[date]);
                            } else {
                                endedArray.push(0);
                            }
                            if (dataObjectfailed[date]) {
                                failedArray.push(dataObjectfailed[date]);
                            } else {
                                failedArray.push(0);
                            }
                            if (dataObjectpartial[date]) {
                                partialArray.push(dataObjectpartial[date]);
                            } else {
                                partialArray.push(0);
                            }
                            if (dataObjectpickups[date]) {
                                pickupsArray.push(dataObjectpickups[date]);
                            } else {
                                pickupsArray.push(0);
                            }
                            if (dataObjectdeliveries[date]) {
                                deliveriesArray.push(dataObjectdeliveries[date]);
                            } else {
                                deliveriesArray.push(0);
                            }
                            if (dataObjectunassigned[date]) {
                                unassignedArray.push(dataObjectunassigned[date]);
                            } else {
                                unassignedArray.push(0);
                            }
                        });

                        var job_status_response = [];
                        var job_type_response = [];
                        var tasks = [];
                        var upcomingArray1 = [], intransitArray1 = [], endedArray1 = [], failedArray1 = [], partialArray1 = [], pickupsArray1 = [], deliveriesArray1 = [], unassignedArray1 = [], successfulTasks = [];
                        date_length = dates.length;
                        for (var i = 0; i < date_length; i++) {
                            dates[i] = moment(dates[i]).format('MM-DD');

                            var newString = [
                                dates[i], upcomingArray[i]
                            ]
                            upcomingArray1.push(newString);
                            newString = [
                                dates[i], intransitArray[i]
                            ]
                            intransitArray1.push(newString)
                            newString = [
                                dates[i], endedArray[i]
                            ]
                            endedArray1.push(newString)
                            newString = [
                                dates[i], failedArray[i]
                            ]
                            failedArray1.push(newString)
                            newString = [
                                dates[i], partialArray[i]
                            ]
                            partialArray1.push(newString)
                            newString = [
                                dates[i], pickupsArray[i]
                            ]
                            pickupsArray1.push(newString)
                            newString = [
                                dates[i], deliveriesArray[i]
                            ]
                            deliveriesArray1.push(newString)
                            newString = [
                                dates[i], unassignedArray[i]
                            ]
                            unassignedArray1.push(newString)
                            newString = [
                                dates[i], endedArray[i] + partialArray[i]
                            ]
                            successfulTasks.push(newString)
                        }
                        var firstResponse = {
                            "label": "Upcoming Tasks",
                            "color": "#51bff2",
                            "data": upcomingArray1
                        };
                        job_status_response.push(firstResponse);
                        var secondResponse = {
                            "label": "Intransit Tasks",
                            "color": "#4a8ef1",
                            "data": intransitArray1
                        };
                        job_status_response.push(secondResponse);
                        var thirdResponse = {
                            "label": "Ended Tasks",
                            "color": "#4a8ef1",
                            "data": endedArray1
                        };
                        job_status_response.push(thirdResponse);
                        var forthResponse = {
                            "label": "Failed Tasks",
                            "color": "#4a8ef1",
                            "data": failedArray1
                        };
                        job_status_response.push(forthResponse);
                        var fifthResponse = {
                            "label": "Partial Tasks",
                            "color": "#4a8ef1",
                            "data": partialArray1
                        };
                        job_status_response.push(fifthResponse);
                        var sixthResponse = {
                            "label": "Pickup Tasks",
                            "color": "#4a8ef1",
                            "data": pickupsArray1
                        };
                        job_type_response.push(sixthResponse);
                        var seventhResponse = {
                            "label": "Delivery Tasks",
                            "color": "#4a8ef1",
                            "data": deliveriesArray1
                        };
                        job_type_response.push(seventhResponse);
                        var eightedResponse = {
                            "label": "Unassigned Tasks",
                            "color": "#4a8ef1",
                            "data": unassignedArray1
                        };
                        job_type_response.push(eightedResponse);

                        var response = {
                            "label": "Successful Tasks",
                            "color": "#27c24c",
                            "data": successfulTasks
                        };
                        tasks.push(response);
                        var response = {
                            "label": "Failed Tasks",
                            "color": "#cd2626",
                            "data": failedArray1
                        };
                        tasks.push(response);

                        var finalResponse = {
                            "job_status": job_status_response,
                            "job_type": job_type_response,
                            "tasks": tasks
                        }
                        res.send(JSON.stringify(finalResponse));
                        return;
                    });
                }
            }
        });
    }
};

function getConsistentDatesIncluding(dates, start_date, end_date) {
    dates.push(new Date(start_date).toDateString());
    var i = 0;
    while (commonFunc.timeDifferenceInDays(dates[i], end_date) > 0) {
        dates.push(addDay(dates[i]));
        i++;
    }
}

function addDay(date) {
    var newDate = new Date(date);
    newDate.setTime(newDate.getTime() + 86400000); // add a date
    return new Date(newDate).toDateString();
}

/*
 * -----------------------------------------------
 * HEAT MAP ONTIME OR DELAYED TASKS OF FLEET
 * -----------------------------------------------
 */

exports.heat_map_deliveries_ontime_or_delayed = function (req, res) {

    var access_token = req.body.access_token;
    var start_date = req.body.start_datetime;
    var end_date = req.body.end_datetime;
    var team_ids = req.body.team_ids;
    var fleet_ids = req.body.fleet_ids;
    var timezone = req.body.timezone;
    var manvalues = [access_token, start_date, end_date, timezone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var dates = [];
                var start_date1 = new Date(start_date);
                var end_date1 = new Date(end_date);
                getConsistentDatesIncluding(dates, start_date1, end_date1);

                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    result[0].user_id = result[0].dispatcher_user_id
                }
                var sql = "SELECT `job_id`,`job_type`,`job_status`,`job_delivery_datetime`,`arrived_datetime`,`completed_datetime`,`job_time`,`timezone` " +
                    "FROM `tb_jobs` " +
                    "WHERE DATE(`job_time`)>= ? AND DATE(`job_time`) <= ? AND `team_id` IN (" + team_ids + ") AND `fleet_id` IN (" + fleet_ids + ") AND `user_id` = ? " +
                    "AND `job_status` IN (" + constants.jobStatus.ENDED + "," + constants.jobStatus.FAILED + "," + constants.jobStatus.CANCEL + ")";
                step_in(sql, start_date1, end_date1, result[0].user_id);

                function step_in(sql, start_date1, end_date1, user) {
                    connection.query(sql, [start_date1, end_date1, user], function (err, data) {
                        var delayed = 0, ontime = 0;
                        var onTimeDeliveries = [], DelayDeliveries = [];
                        var data_length = data.length;
                        var date_length = dates.length;
                        var dataObjectOnTimeDeliveries = {};
                        var dataObjectDelayDeliveries = {};
                        for (var i = 0; i < date_length; i++) {
                            ontime = 0;
                            delayed = 0;
                            for (var j = 0; j < data_length; j++) {
                                if (new Date(data[j].completed_datetime).toDateString() == dates[i]) {
                                    data[j].completed_datetime = commonFunc.convertTimeIntoLocal(new Date(data[j].completed_datetime),(data[j].timezone));
                                    if (new Date(data[j].completed_datetime).getTime() <= new Date(data[j].job_time).getTime()) {
                                        ontime++;
                                    } else if (new Date(data[j].completed_datetime).getTime() > new Date(data[j].job_time).getTime()) {
                                        delayed++;
                                    }
                                }
                                dataObjectOnTimeDeliveries[dates[i]] = ontime;
                                dataObjectDelayDeliveries[dates[i]] = delayed;
                            }

                        }
                        dates.forEach(function (date) {
                            if (dataObjectOnTimeDeliveries[date]) {
                                onTimeDeliveries.push(dataObjectOnTimeDeliveries[date]);
                            } else {
                                onTimeDeliveries.push(0);
                            }
                            if (dataObjectDelayDeliveries[date]) {
                                DelayDeliveries.push(dataObjectDelayDeliveries[date]);
                            } else {
                                DelayDeliveries.push(0);
                            }
                        });
                        var response = [];
                        var ontimedeliveriesArray = [];
                        for (var i = 0; i < date_length; i++) {
                            dates[i] = moment(dates[i]).format('MM-DD');
                            var newString = [
                                dates[i], onTimeDeliveries[i]
                            ]
                            ontimedeliveriesArray.push(newString);
                        }
                        var firstResponse = {
                            "label": "On-Time Deliveries",
                            "color": "#51bff2",
                            "data": ontimedeliveriesArray
                        };
                        response.push(firstResponse);
                        var delaydeliveriesArray = [];
                        for (var i = 0; i < date_length; i++) {
                            var newString = [
                                dates[i], DelayDeliveries[i]
                            ]
                            delaydeliveriesArray.push(newString)
                        }
                        var secondResponse = {
                            "label": "Delayed Deliveries",
                            "color": "#4a8ef1",
                            "data": delaydeliveriesArray
                        };
                        response.push(secondResponse);
                        res.send(JSON.stringify(response));
                        return;
                    });
                }
            }
        });
    }
};


/*
 * -----------------------------------------------------
 * ANALYTICS ONTIME v/s DELAYED & SUCCESSFUL v/s FAILED
 * -----------------------------------------------------
 */

exports.heat_map_analytics = function (req, res) {

    var access_token = req.body.access_token;
    var start_date = req.body.start_datetime;
    var end_date = req.body.end_datetime;
    var team_ids = req.body.team_ids;
    var fleet_ids = req.body.fleet_ids;
    var timezone = req.body.timezone;
    var manvalues = [access_token, start_date, end_date, timezone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {

                if (!fleet_ids){
                    responses.actionCompleteResponse(res);
                    return;
                }else {
                    var start_date1 = new Date(start_date);
                    var end_date1 = new Date(end_date);
                    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        result[0].user_id = result[0].dispatcher_user_id
                    }
                    var sql = "SELECT job.`customer_id`,CASE WHEN job.`customer_username` IN ('dummy') THEN '-' ELSE job.`customer_username` END AS customer_username " +
                        " ,job.`fleet_id`,job.`job_id`,job.`job_type`,job.`job_status`,job.`job_delivery_datetime`," +
                        "job.`arrived_datetime`,job.`completed_datetime`,job.`job_time`,job.`timezone` " +
                        "FROM `tb_jobs` job " +
                        "WHERE DATE(job.`job_time`)>= ? AND DATE(job.`job_time`) <= ? AND `team_id` IN (" + team_ids + ") AND job.`fleet_id` IN (" + fleet_ids + ") AND job.`user_id` = ? " +
                        "AND job.`job_status` IN (" + constants.jobStatus.ENDED + "," + constants.jobStatus.FAILED + "," + constants.jobStatus.CANCEL + ")";
                    heatMapAnalytics(sql, start_date1, end_date1, result[0].user_id, fleet_ids, res);
                }
            }
        });
    }
};


function heatMapAnalytics(sql, start_date, end_date, user, fleet_ids, res) {
    connection.query(sql, [start_date, end_date, user], function (err, data) {
        if (err) {
            logging.logDatabaseQueryError("Error in heatMapAnalytics : ", err, data);
            responses.sendError(res);
            return;
        }

        data.forEach(function(data){
            data.completed_datetime = commonFunc.convertTimeIntoLocal(new Date(data.completed_datetime),(data.timezone));
            data.arrived_datetime = commonFunc.convertTimeIntoLocal(new Date(data.arrived_datetime),(data.timezone));
        });

        async.parallel
        ([
                function (callback) {

                    var sql = "SELECT `username`,`fleet_id` " +
                        "FROM tb_fleets " +
                        "WHERE fleet_id IN (" + fleet_ids + ") AND `user_id`=? AND `registration_status` = ?";
                    connection.query(sql, [user,constants.userVerificationStatus.VERIFY], function (err, fleet_information) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in finding fleets information : ", err, fleet_information);
                        }
                        var fleetNewArary = [];
                        fleet_information.forEach(function (fleets) {
                            fleetNewArary.push(fleets.fleet_id);
                        });
                        fleet_ids = fleetNewArary.toString();

                        var dataLength = data.length;
                        var fleetInformationLength = fleet_information.length;
                        for (var i = 0; i < dataLength; i++) {
                            for (var j = 0; j < fleetInformationLength; j++) {
                                if (fleet_information[j].fleet_id == data[i].fleet_id) {
                                    data[i].fleet_name = fleet_information[j].username;
                                }
                            }
                        }
                        fleetResponse(user, fleet_ids, fleet_information, data, function (fleetResponseResult) {
                            callback(null, fleetResponseResult)
                        });
                    });
                },
                function (callback) {
                    dateResponse(start_date, end_date, data, function (dateResponseResult) {
                        callback(null, dateResponseResult)
                    });
                }],
            function (err, results) {
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": {
                        "tasks": data,
                        "fleet_response": results[0],
                        "dates_response": results[1]
                    }
                };
                res.send(JSON.stringify(response));
            });
    });
}

function fleetResponse(user, fleet_ids, fleetResponseResult, data, callback) {
    var delayed = 0, ontime = 0, successful = 0, failed = 0, cancelled = 0, total_tasks = 0;
    var enroute_time = 0, enroute_distance = 0, time_in_sec = 0, distance_in_meters = 0;
    var onTime = [], Delay = [], Success = [], Cancelled = [], Failed = [], TotalTask = [];
    var A = [], B = [], C = [], D = [];
    var data_length = data.length;
    var fleetArray = fleet_ids.split(',');
    fleetArray = fleetArray.sort(function (a, b) {
        return a - b;
    });
    var fleets_length = fleetArray.length;
    var dataObjectOnTime = {}, dataObjectDelay = {}, dataObjectSuccess = {}, dataObjectFailed = {}, dataObjectCancelled = {},dataObjectTotalTasks = {};
    var dataObjectEnrouteTime = {}, dataObjectEnrouteDistance = {}, dataObjectTimeInSec = {}, dataObjectDistanceInMeters = {};
    for (var i = 0; i < fleets_length; i++) {
        ontime = 0, delayed = 0, successful = 0, failed = 0, cancelled = 0, total_tasks = 0;
        enroute_time = 0, enroute_distance = 0, time_in_sec = 0, distance_in_meters = 0;
        for (var j = 0; j < data_length; j++) {
            if (data[j].fleet_id == fleetArray[i]) {
                if (data[j].arrived_datetime) {
                    if (new Date(data[j].arrived_datetime).getTime() <= new Date(data[j].job_time).getTime()) {
                        ontime++;
                    } else if (new Date(data[j].arrived_datetime).getTime() > new Date(data[j].job_time).getTime()) {
                        delayed++;
                    }
                }
                if (data[j].job_status) {
                    if (data[j].job_status == constants.jobStatus.ENDED) {
                        successful++;
                    } else if (data[j].job_status == constants.jobStatus.FAILED) {
                        failed++;
                    }else if (data[j].job_status == constants.jobStatus.CANCEL) {
                        cancelled++;
                    }
                }
                if (data[j].enroute_time) {
                    enroute_time += parseInt(data[j].enroute_time);
                }
                if (data[j].enroute_distance) {
                    enroute_distance += parseInt(data[j].enroute_distance);
                }
                if (data[j].time_in_sec) {
                    time_in_sec += parseInt(data[j].time_in_sec);
                }
                if (data[j].distance_in_metres) {
                    distance_in_meters += parseInt(data[j].distance_in_metres);
                }

                if ((data[j].job_status == constants.jobStatus.ENDED) || (data[j].job_status == constants.jobStatus.FAILED)
                    || (data[j].job_status == constants.jobStatus.CANCEL)) {
                    total_tasks++;
                }
            }
            dataObjectOnTime[fleetResponseResult[i].username] = ontime;
            dataObjectDelay[fleetResponseResult[i].username] = delayed;
            dataObjectSuccess[fleetResponseResult[i].username] = successful;
            dataObjectFailed[fleetResponseResult[i].username] = failed;
            dataObjectCancelled[fleetResponseResult[i].username] = cancelled;
            dataObjectTotalTasks[fleetResponseResult[i].username] = total_tasks;
            dataObjectEnrouteTime[fleetResponseResult[i].username] = enroute_time;
            dataObjectEnrouteDistance[fleetResponseResult[i].username] = enroute_distance;
            dataObjectTimeInSec[fleetResponseResult[i].username] = time_in_sec;
            dataObjectDistanceInMeters[fleetResponseResult[i].username] = distance_in_meters;
        }
    }

    fleetResponseResult.forEach(function (fleet) {
        if (dataObjectOnTime[fleet.username]) {
            onTime.push([fleet.username, dataObjectOnTime[fleet.username], fleet.fleet_id]);
        } else {
            onTime.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectDelay[fleet.username]) {
            Delay.push([fleet.username, dataObjectDelay[fleet.username], fleet.fleet_id]);
        } else {
            Delay.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectSuccess[fleet.username]) {
            Success.push([fleet.username, dataObjectSuccess[fleet.username], fleet.fleet_id]);
        } else {
            Success.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectCancelled[fleet.username]) {
            Cancelled.push([fleet.username, dataObjectCancelled[fleet.username], fleet.fleet_id]);
        } else {
            Cancelled.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectFailed[fleet.username]) {
            Failed.push([fleet.username, dataObjectFailed[fleet.username], fleet.fleet_id]);
        } else {
            Failed.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectTotalTasks[fleet.username]) {
            TotalTask.push([fleet.username, dataObjectTotalTasks[fleet.username], fleet.fleet_id]);
        } else {
            TotalTask.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectEnrouteTime[fleet.username]) {
            A.push([fleet.username, dataObjectEnrouteTime[fleet.username], fleet.fleet_id]);
        } else {
            A.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectEnrouteDistance[fleet.username]) {
            B.push([fleet.username, dataObjectEnrouteDistance[fleet.username], fleet.fleet_id]);
        } else {
            B.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectTimeInSec[fleet.username]) {
            C.push([fleet.username, dataObjectTimeInSec[fleet.username], fleet.fleet_id]);
        } else {
            C.push([fleet.username, 0, fleet.fleet_id]);
        }
        if (dataObjectDistanceInMeters[fleet.username]) {
            D.push([fleet.username, dataObjectDistanceInMeters[fleet.username], fleet.fleet_id]);
        } else {
            D.push([fleet.username, 0, fleet.fleet_id]);
        }
    });

    var response = {
        "data": {
            on_time: onTime,
            delay: Delay,
            success: Success,
            failed: Failed,
            cancelled: Cancelled,
            total_tasks: TotalTask,
            enroute_time: A,
            enroute_distance: B,
            idle_time: C,
            idle_distance: D
        }
    };
    callback(response);

}


function dateResponse(start_date, end_date, data, callback) {

    var dates = [];
    getConsistentDatesIncluding(dates, start_date, end_date);
    var dates_length = dates.length;
    var delayed = 0, ontime = 0, successful = 0, failed = 0, cancelled = 0, total_tasks = 0;
    var enroute_time = 0, enroute_distance = 0, time_in_sec = 0, distance_in_meters = 0;
    var onTime = [], Delay = [], Success = [], Cancelled = [], Failed = [], Total_Tasks = [];
    var A = [], B = [], C = [], D = [];
    var data_length = data.length;

    var dataObjectOnTime = {}, dataObjectDelay = {}, dataObjectSuccess = {}, dataObjectCancelled = {}, dataObjectFailed = {}, dataObjectTotalTasks = {};
    var dataObjectEnrouteTime = {}, dataObjectEnrouteDistance = {}, dataObjectTimeInSec = {}, dataObjectDistanceInMeters = {};
    for (var i = 0; i < dates_length; i++) {
        ontime = 0, delayed = 0, successful = 0, failed = 0, cancelled = 0, total_tasks = 0;
        enroute_time = 0, enroute_distance = 0, time_in_sec = 0, distance_in_meters = 0;
        for (var j = 0; j < data_length; j++) {

            if ((data[j].job_time) && (new Date(data[j].job_time).toDateString() == dates[i])) {
                if (data[j].arrived_datetime) {

                    if (new Date(data[j].arrived_datetime).getTime() <= new Date(data[j].job_time).getTime()) {
                        ontime++;

                    } else if (new Date(data[j].arrived_datetime).getTime() > new Date(data[j].job_time).getTime()) {
                        delayed++;

                    }
                }
                if (data[j].job_status) {
                    if (data[j].job_status == constants.jobStatus.ENDED) {
                        successful++;
                    } else if (data[j].job_status == constants.jobStatus.FAILED) {
                        failed++;
                    }else if (data[j].job_status == constants.jobStatus.CANCEL) {
                        cancelled++;
                    }
                }
                if ((data[j].job_status == constants.jobStatus.ENDED) || (data[j].job_status == constants.jobStatus.FAILED)
                    || (data[j].job_status == constants.jobStatus.CANCEL)) {
                    total_tasks++;
                }
            }
            if ((data[j].updated_datetime) && (new Date(data[j].updated_datetime).toDateString() == dates[i])) {
                if (data[j].enroute_time) {
                    enroute_time += parseInt(data[j].enroute_time);
                }
                if (data[j].enroute_distance) {
                    enroute_distance += parseInt(data[j].enroute_distance);
                }
                if (data[j].time_in_sec) {
                    time_in_sec += parseInt(data[j].time_in_sec);
                }
                if (data[j].distance_in_metres) {
                    distance_in_meters += parseInt(data[j].distance_in_metres);
                }
            }

            dataObjectOnTime[dates[i]] = ontime;
            dataObjectDelay[dates[i]] = delayed;
            dataObjectSuccess[dates[i]] = successful;
            dataObjectFailed[dates[i]] = failed;
            dataObjectCancelled[dates[i]] = cancelled;
            dataObjectTotalTasks[dates[i]] = total_tasks;
            dataObjectEnrouteTime[dates[i]] = enroute_time;
            dataObjectEnrouteDistance[dates[i]] = enroute_distance;
            dataObjectTimeInSec[dates[i]] = time_in_sec;
            dataObjectDistanceInMeters[dates[i]] = distance_in_meters;
        }
    }
    dates.forEach(function (date) {
        if (dataObjectOnTime[date]) {
            onTime.push([date, dataObjectOnTime[date]]);
        } else {
            onTime.push([date, 0]);
        }
        if (dataObjectDelay[date]) {
            Delay.push([date, dataObjectDelay[date]]);
        } else {
            Delay.push([date, 0]);
        }
        if (dataObjectSuccess[date]) {
            Success.push([date, dataObjectSuccess[date]]);
        } else {
            Success.push([date, 0]);
        }
        if (dataObjectCancelled[date]) {
            Cancelled.push([date, dataObjectCancelled[date]]);
        } else {
            Cancelled.push([date, 0]);
        }
        if (dataObjectFailed[date]) {
            Failed.push([date, dataObjectFailed[date]]);
        } else {
            Failed.push([date, 0]);
        }
        if (dataObjectTotalTasks[date]) {
            Total_Tasks.push([date, dataObjectTotalTasks[date]]);
        } else {
            Total_Tasks.push([date, dataObjectTotalTasks[date]]);
        }
        if (dataObjectEnrouteTime[date]) {
            A.push([date, dataObjectEnrouteTime[date]]);
        } else {
            A.push([date, 0]);
        }
        if (dataObjectEnrouteDistance[date]) {
            B.push([date, dataObjectEnrouteDistance[date]]);
        } else {
            B.push([date, 0]);
        }
        if (dataObjectTimeInSec[date]) {
            C.push([date, dataObjectTimeInSec[date]]);
        } else {
            C.push([date, 0]);
        }
        if (dataObjectDistanceInMeters[date]) {
            D.push([date, dataObjectDistanceInMeters[date]]);
        } else {
            D.push([date, 0]);
        }
    });
    var response = {
        "data": {
            on_time: onTime,
            delay: Delay,
            success: Success,
            failed: Failed,
            cancelled : Cancelled,
            total_tasks: Total_Tasks,
            enroute_time: A,
            enroute_distance: B,
            idle_time: C,
            idle_distance: D
        }
    };
    callback(response);
}


/*
 * ---------------------
 * EFFICIENCY REPORT
 * ---------------------
 */

exports.efficiency_report = function (req, res) {

    var access_token = req.body.access_token;
    var start_date = req.body.start_datetime;
    var end_date = req.body.end_datetime;
    var team_ids = req.body.team_ids;
    var fleet_ids = req.body.fleet_ids;
    var timezone = req.body.timezone;
    var manvalues = [access_token, start_date, end_date, timezone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                if (!fleet_ids){
                    responses.actionCompleteResponse(res);
                    return;
                }else {
                    var start_date1 = new Date(start_date), end_date1 = new Date(end_date);
                    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        result[0].user_id = result[0].dispatcher_user_id
                    }
                    var sql = "SELECT job.`customer_id`,CASE WHEN job.`customer_username` IN ('dummy') THEN '-' ELSE job.`customer_username` END AS customer_username " +
                        ",job.`fleet_id`,job.`job_id`,job.`job_type`,job.`job_status`,job.`job_delivery_datetime`," +
                        "job.`arrived_datetime`,job.`completed_datetime`,job.`job_time`,job.`timezone` " +
                        "FROM `tb_jobs` job " +
                        "WHERE DATE(job.`job_time`)>= ? AND DATE(job.`job_time`) <= ? AND job.`team_id` IN (" + team_ids + ") AND job.`fleet_id` IN (" + fleet_ids + ") AND job.`user_id` = ? "+
                        "AND job.`job_status` IN (" + constants.jobStatus.ENDED + "," + constants.jobStatus.FAILED + "," + constants.jobStatus.CANCEL + ")";
                    heatMapAnalytics(sql, start_date1, end_date1, result[0].user_id, fleet_ids, res);

                }
            }
        });
    }
};


/*
 * ---------------------
 * EFFECTIVENESS REPORT
 * ---------------------
 */

exports.effectiveness_report = function (req, res) {

    var access_token = req.body.access_token;
    var start_date = req.body.start_datetime;
    var end_date = req.body.end_datetime;
    var fleet_ids = req.body.fleet_ids;
    var timezone = req.body.timezone;
    var manvalues = [access_token, start_date, end_date, timezone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                if (!fleet_ids){
                    responses.actionCompleteResponse(res);
                    return;
                }else {
                    var start_date1 = new Date(start_date);
                    var end_date1 = new Date(end_date);

                    var sql = "SELECT `fleet_id`,(`time_in_sec`-`enroute_time`) as `time_in_sec` ,(`distance_in_metres`-`enroute_distance`) as `distance_in_metres`,`enroute_distance`,`enroute_time`," +
                        "DATE_ADD(`updated_datetime`, INTERVAL ? MINUTE) as `updated_datetime` FROM `tb_fleet_tracking` " +
                        "WHERE DATE(`updated_datetime`)>= ? AND DATE(`updated_datetime`) <= ? AND `fleet_id` IN (" + fleet_ids + ") ";
                    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        result[0].user_id = result[0].dispatcher_user_id;
                    }
                    effectivenessReport(sql, -timezone, start_date1, end_date1, result[0].user_id, fleet_ids, res);
                }
            }
        });
    }
};

function effectivenessReport(sql, timezone, start_date, end_date, user, fleet_ids, res) {
    connection.query(sql, [timezone, start_date, end_date], function (err, data) {
        if (err) {
            logging.logDatabaseQueryError("Error in effectivenessReport : ", err, data);
            responses.sendError(res);
            return;
        }
        async.parallel
        ([
                function (callback) {

                    var sql = "SELECT `username`,`fleet_id` " +
                        "FROM tb_fleets " +
                        "WHERE fleet_id IN (" + fleet_ids + ") AND `user_id`=? AND `registration_status` = ?";
                    connection.query(sql, [user,constants.userVerificationStatus.VERIFY], function (err, fleet_information) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in finding fleets information : ", err, fleet_information);
                        }
                        var fleetNewArary = [];
                        fleet_information.forEach(function (fleets) {
                            fleetNewArary.push(fleets.fleet_id);
                        });
                        fleet_ids = fleetNewArary.toString();

                        var dataLength = data.length;
                        var fleetInformationLength = fleet_information.length;
                        for (var i = 0; i < dataLength; i++) {
                            for (var j = 0; j < fleetInformationLength; j++) {
                                if (fleet_information[j].fleet_id == data[i].fleet_id) {
                                    data[i].fleet_name = fleet_information[j].username;
                                }
                            }
                        }
                        fleetResponse(user, fleet_ids, fleet_information, data, function (fleetResponseResult) {
                            callback(null, fleetResponseResult)
                        });
                    });
                },
                function (callback) {
                    dateResponse(start_date, end_date, data, function (dateResponseResult) {
                        callback(null, dateResponseResult)
                    });
                }],
            function (err, results) {
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": {
                        "tasks": data,
                        "fleet_response": results[0],
                        "dates_response": results[1]
                    }
                };
                res.send(JSON.stringify(response));
            });
    });
}