
var debugging_enabled = true;


exports.logDatabaseQueryError = function (eventFired, error, result)
{
    if(debugging_enabled)
    {
        console.log(error)
        process.stderr.write("Event: " + eventFired);
        process.stderr.write("\tError: " + JSON.stringify(error));
        process.stderr.write("\tResult: " + JSON.stringify(result));
    }
};

var log4js = require('log4js');
log4js.clearAppenders();

log4js.configure({
    appenders: [
        {
            type: 'dateFile',
            filename: __dirname + config.get('logFiles.allLogsFilePath'),
            pattern: '-yyyy-MM-dd',
            category: 'all_logs',
            alwaysIncludePattern : true
        },
        {
            type: 'dateFile',
            filename: __dirname + config.get('logFiles.errorLogsFilePath'),
            pattern: '-yyyy-MM-dd',
            category: 'error_logs',
            alwaysIncludePattern : true
        }
    ]
});

var logger          = log4js.getLogger('all_logs');
var errorLogger   = log4js.getLogger('error_logs');
logger.setLevel('INFO');
errorLogger.setLevel('INFO');

exports.addErrorLog = function (event, err, input){
    var log = {
        event   : event,
        error   : err,
        input   : input
    }

    errorLogger.info(JSON.stringify(log,undefined,2));
};

exports.addlog = function (event, info, res){

    var log = {
        event   : event,
        input   : info,
        res     : res
    }
    logger.info(JSON.stringify(log,undefined,2));
};