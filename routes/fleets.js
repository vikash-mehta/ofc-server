var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var cron = require('./cron');
var moment = require('moment');
var geolib = require('geolib');
var async = require('async');
var mongo = require('./mongo');
var socketResponse = require('./socketResponses');
var validator = require('validator');
/*
 * ----------------------------------
 * ADDITION OF NEW FLEET
 * ----------------------------------
 */

exports.add_fleet = function (req, res) {

    var access_token = (req.body.access_token == null || req.body.access_token == undefined) ? "" : req.body.access_token.trim();
    var email = (req.body.email == null || req.body.email == undefined) ? "" : validator.toString(req.body.email.trim());
    var name = (req.body.name == null || req.body.name == undefined) ? "" : validator.toString(req.body.name.trim());
    var phone = (req.body.phone == null || req.body.phone == undefined) ? "" : validator.toString(req.body.phone.trim());
    var transport_type = (req.body.transport_type == null || req.body.transport_type == undefined) ? "" : validator.toString(req.body.transport_type.trim());
    var transport_desc = (req.body.transport_desc == null || req.body.transport_desc == undefined) ? "" : validator.toString(req.body.transport_desc.trim());
    var license = (req.body.license == null || req.body.license == undefined) ? "" : validator.toString(req.body.license.trim());
    var color = (req.body.color == null || req.body.color == undefined) ? "" : validator.toString(req.body.color.trim());
    var timezone = (req.body.timezone == null || req.body.timezone == undefined) ? "" : req.body.timezone.toString().trim();
    var team_ids = (req.body.team_ids == null || req.body.team_ids == undefined) ? "" : validator.toString(req.body.team_ids.toString());
    var password = (req.body.password == null || req.body.password == undefined) ? "" : validator.toString(req.body.password.trim());
    var login_id = (req.body.login_id == null || req.body.login_id == undefined) ? "" : validator.toString(req.body.login_id.trim());
    var first_name = (req.body.first_name == null || req.body.first_name == undefined) ? "" : validator.toString(req.body.first_name.trim());
    var last_name = (req.body.last_name == null || req.body.last_name == undefined) ? "" : validator.toString(req.body.last_name.trim());
    if (typeof team_ids === 'undefined' || team_ids === '') {
        team_ids = 0;
    }
    var tags = (req.body.tags == null || req.body.tags == undefined) ? "" : req.body.tags.trim();
    if (typeof tags === undefined || tags === 'undefined' || tags == '')
        tags = ''
    var manvalues = [access_token, login_id, timezone, team_ids], dispatcher_id = null;
    async.waterfall([
        function (cb) {
            var checkblank = commonFunc.checkBlank(manvalues);
            if (checkblank) {
                cb(null, {
                    err: 101
                })
            } else {
                cb(null, {
                    err: 0
                })
            }
        },
        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                commonFunc.authenticateUserAccessToken(access_token, function (user) {
                    if (user == 0) {
                        cb(null, {
                            err: 1
                        });
                    } else {
                        cb(null, user);
                    }
                });
            }
        },
        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                commonFunc.checkAccountExpiry(user[0].user_id, function (checkExp) {
                    if (checkExp) {
                        cb(null, {
                            err: 2
                        });
                    } else {
                        cb(null, user);
                    }
                });
            }
        },
        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                commonFunc.authenticateFleetEmailWithUser(email, function (auth) {
                    if (auth != 0) {
                        cb(null, {
                            err: 3
                        });
                    } else {
                        cb(null, user);
                    }
                });
            }
        },
        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                commonFunc.authenticateUniqueFleetLoginID(login_id, function (auth) {
                    if (auth != 0) {

                        cb(null, {
                            err: 4
                        });

                    } else {
                        cb(null, user);
                    }
                });
            }
        },
        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                if (user[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    commonFunc.checkDispatcherPermissions(user[0].user_id, function (checkPerm) {
                        if (checkPerm && checkPerm.length && checkPerm[0].create_driver == constants.hasPermissionStatus.NO) {
                            cb(null, {
                                err: 5
                            });
                        }
                        else {
                            dispatcher_id = user[0].user_id;
                            user[0].user_id = user[0].dispatcher_user_id;
                            cb(null, user);
                        }
                    })
                } else {
                    cb(null, user);
                }
            }
        },
        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                if (user[0].billing_plan == constants.billingPlan.FREE_LIMITED) {

                    commonFunc.getAllFleets(user[0].user_id, function (getAllFleetsResult) {
                        var fleets_count = getAllFleetsResult.length;
                        if (fleets_count >= user[0].num_fleets) {
                            var message = constants.responseMessages.EXCEED_FLEET_COUNT;
                            message = message.replace(/<%FLEET%>/g, user[0].num_fleets + " " + user[0].call_fleet_as);
                            cb(null, {
                                err: 6,
                                message: message.replace(/<%FLEET%>/g, user[0].num_fleets + " " + user[0].call_fleet_as),
                                getAllFleetsResult: getAllFleetsResult
                            });

                        } else {
                            cb(null, user);
                        }
                    });
                } else {
                    cb(null, user);
                }
            }
        },
        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                user[0].android_link = config.get('androidAppDownloadLink');
                user[0].ios_link = config.get('iOSAppDownloadLink');
                user[0].brand_name = config.get('projectName');
                commonFunc.getVersion2(user[0].user_id, function (versions) {
                    if (versions && versions.length) {
                        versions.forEach(function (vr) {
                            if (vr.device_type && (vr.device_type % 2 == 0)) {
                                user[0].android_link = vr.app_url;
                            } else if (vr.device_type && (vr.device_type % 2 == 1)) {
                                user[0].ios_link = vr.app_url;
                            }
                            user[0].brand_name = vr.brand_name;
                        })

                        cb(null, user);
                    } else {
                        cb(null, user);
                    }
                })
            }
        },
        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                module.exports.addFleet(user, dispatcher_id, name, email, timezone, phone, transport_type, transport_desc,
                    license, color, team_ids, password, 1, login_id, first_name, last_name, function (addFleetResult) {
                        if (addFleetResult) {
                            cb(null, user);
                        } else {
                            cb(null, {
                                err: 7
                            });
                        }
                    })
            }
        }
    ], function (error, user) {
        if (error) {
            responses.authenticationError(res);
            return;
        } else {
            if (user.err) {
                if (user.err == 1) {
                    responses.authenticationErrorResponse(res);
                    return;
                } else if (user.err == 2) {
                    responses.accountExpiryErrorResponse(res);
                    return;
                } else if (user.err == 3) {
                    var response = {
                        "message": constants.responseMessages.FLEET_EMAIL_ALREADY_EXISTS,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else if (user.err == 4) {
                    var response = {
                        "message": constants.responseMessages.LOGIN_ID_ALREADY_EXISTS,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else if (user.err == 5) {
                    responses.invalidAccessError(res);
                    return;
                } else if (user.err == 6) {
                    var response = {
                        "message": user.message,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {
                            fleets: user.getAllFleetsResult
                        }
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else if (user.err == 7) {
                    responses.authenticationError(res);
                    return;
                } else if (user.err == 101) {
                    responses.parameterMissingResponse(res);
                    return;
                } else {
                    responses.authenticationError(res);
                    return;
                }
            } else {
                responses.actionCompleteResponse(res);
                return;
            }
        }
    })
}


exports.addFleet = function (result, dispatcher_id, name, email, timezone, phone, transport_type, transport_desc,
                             license, color, team_ids, password, decrypt_password, login_id, first_name, last_name, callback) {
    team_ids = team_ids.toString();
    var randomNumber = commonFunc.generateRandomString();
    var fleet_password = 'tookan' + randomNumber;
    if (password) {
        fleet_password = password;
    }
    var encrypted_pass = fleet_password;
    if (decrypt_password) {
        encrypted_pass = md5(fleet_password);
    }
    first_name = (first_name == "null" ? '' : first_name)
    transport_type = (transport_type == "null" ? '' : transport_type)
    transport_desc = (transport_desc == "null" ? '' : transport_desc)
    license = (license == "null" ? '' : license)
    color = (color == "null" ? '' : color)
    email = (email == "null" ? '' : email)
    var access_token = md5(encrypted_pass + new Date());
    var msg = 'Welcome to ' + result[0].brand_name;
    if (result[0].brand_name == 'Tookan') {
        msg += ' - Delivery Management Solution. ';
    } else {
        msg += '.';
    }
    msg += 'You have been added as a ' + result[0].call_fleet_as + ' by <b>' + result[0].username + '</b>.';
    msg += 'Please use the appropriate link below to download the App.<br><br>';
    msg += '<b>Android App Link:</b> ' + result[0].android_link + ' <br><br>';
    msg += '<b>iOS App Link:</b> ' + result[0].ios_link + ' <br><br>';
    msg += "Use the credentials below to login: <br>";
    msg += "Username : " + login_id + "<br>";
    if (decrypt_password) {
        msg += "Password : " + fleet_password;
    }
    commonFunc.emailPlainFormatting(first_name, msg, '', '', '', function (returnMessage) {
        returnMessage = returnMessage.replace(/BRAND_NAME/g,result[0].brand_name);
        commonFunc.sendHtmlContent(email, returnMessage, " " + result[0].brand_name + " Invitation - Agent Sign-up", result[0].email, function (resultMail) {

            if (phone) {
                var smsDriverMessage = "Hi " + first_name + ", " +
                    "you have been added as a " + result[0].call_fleet_as + " by " + result[0].username + ". " +
                    "Download iOS App " + result[0].ios_link + " \n" +
                    "Download Android App " + result[0].android_link + " \n" +
                    "Username: " + login_id;
                if (decrypt_password) {
                    smsDriverMessage += " & Pass: " + fleet_password;
                }
                commonFunc.sendMessageByPlivo(phone, smsDriverMessage);
            }

            var fleetImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.fleetProfileImages') + '/' + config.get("s3BucketCredentials.folder.fleetProfileDefaultImage");
            var fleetThumbImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.fleetProfileThumbImages') + '/' + config.get("s3BucketCredentials.folder.fleetProfileThumbDefaultImage");

            var sql = "INSERT INTO `tb_fleets` (`first_name`,`last_name`,`login_id`,`dispatcher_id`,`username`,`fleet_image`,`fleet_thumb_image`,`access_token`,`email`,`password`,";
            sql += "`user_id`,`registration_status`,`last_login_datetime`,`timezone`,`phone`,`transport_type`,`transport_desc`,`license`,`color`)";
            sql += " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            connection.query(sql, [first_name, last_name, login_id, dispatcher_id, name, fleetImage, fleetThumbImage, access_token, email, encrypted_pass,
                result[0].user_id, constants.userVerificationStatus.NOT_VERIFY, new Date(), timezone, phone, transport_type, transport_desc,
                license, color], function (err, result_insert) {
                if (err) {
                    logging.logDatabaseQueryError("Error in inserting fleet info : ", err, result_insert);
                    callback(0);
                } else {
                    if (team_ids == 0) {
                        callback(1);
                    }
                    else {
                        var teams = team_ids.split(','), teams_ids_array = [];
                        teams_ids_array.push(teams[0]); // ONLY ONE TEAM ALLOWED FOR FLEET
                        var teams_ids_array_len = teams_ids_array.length, counter = 0;
                        for (var i = 0; i < teams_ids_array_len; i++) {
                            var sql = "INSERT INTO `tb_fleet_teams` (`team_id`,`user_id`,`fleet_id`) VALUES (?,?,?)";
                            connection.query(sql, [teams_ids_array[i], result[0].user_id, result_insert.insertId], function (err, result_insert_fleet_teams) {

                                if (err) {
                                    logging.logDatabaseQueryError("Error in inserting fleet teams info : ", err, result_insert_fleet_teams);
                                }
                                counter++;
                                sendResponse(counter, teams_ids_array_len);
                            });
                        }
                        function sendResponse(counter, teams_ids_array_len) {
                            if (counter == teams_ids_array_len) {
                                callback(1);
                            }
                        }
                    }
                }
            });
        });
    });
}


/*
 * -----------------------------
 * CREATE NEW FLEET ACCOUNT
 * -----------------------------
 */

exports.create_fleet_account = function (req, res) {

    var email = req.body.email;
    var password = req.body.password;
    var name = req.body.name;
    var timezone = req.body.timezone;
    var device_type = req.body.device_type;
    var device_token = req.body.device_token;
    var fleet_image = req.body.fleet_image;
    var transport_type = req.body.transport_type;
    var transport_desc = req.body.transport_desc;
    var license = req.body.license;
    var phone = req.body.phone;
    var color = req.body.color;
    var manvalues = [phone, name, email, password, device_type, device_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);

    } else {
        commonFunc.authenticateFleetEmail(email, function (result) {
            if (result == 0) {
                responses.authenticationError(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        var date = new Date();
                        if (password.length < config.get('AppPasswordLength')) {
                            var response = {
                                "message": constants.responseMessages.APP_PASSWORD_ERROR,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                        } else {
                            var encrypted_pass = md5(password), access_token = md5(encrypted_pass + new Date());
                            if (req.files && req.files.fleet_image && req.files.fleet_image.name && req.files.fleet_image.size > 0) {
                                commonFunc.uploadImageToS3Bucket(req.files.fleet_image, config.get('s3BucketCredentials.folder.fleetProfileImages'), function (result_fleet_image) {
                                    if (result_fleet_image == 0) {
                                        responses.uploadError(res);
                                    } else {
                                        commonFunc.uploadThumbImageToS3Bucket(req.files.fleet_image, config.get('s3BucketCredentials.folder.fleetProfileThumbImages'), function (result_fleet_thumb_image) {
                                            if (result_fleet_thumb_image == 0) {
                                                responses.uploadError(res);
                                            } else {
                                                var fleetImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.fleetProfileImages') + '/' + result_fleet_image;
                                                var fleetThumbImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.fleetProfileThumbImages') + '/' + result_fleet_thumb_image;
                                                var sql = "UPDATE `tb_fleets` SET `location_update_datetime`=?,`username`=?,`fleet_image`=?,`fleet_thumb_image`=?,`access_token`=?,`email`=?,`password`=?,`phone`=?,";
                                                sql += "`registration_status`=?,`last_login_datetime`=?,`timezone`=?,`device_type`=?,";
                                                sql += "`device_token`=? WHERE `email`=? LIMIT 1";
                                                connection.query(sql, [date, name, fleetImage, fleetThumbImage, access_token, email, encrypted_pass, phone, constants.userVerificationStatus.VERIFY, date, timezone, device_type, device_token, email], function (err, result_insert) {
                                                    if (err) {
                                                        logging.logDatabaseQueryError("Error in inserting fleet info : ", err, result_insert);
                                                        responses.sendError(res);
                                                    } else {
                                                        var response = {
                                                            "message": constants.responseMessages.ACTION_COMPLETE,
                                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                                            "data": {
                                                                access_token: access_token
                                                            }
                                                        };
                                                        res.send(JSON.stringify(response));

                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                var fleetImage = result[0].fleet_image;
                                var fleetThumbImage = result[0].fleet_thumb_image;
                                var sql = "UPDATE `tb_fleets` SET `location_update_datetime`=?,`username`=?,`fleet_image`=?,`fleet_thumb_image`=?,`access_token`=?,`email`=?,`password`=?,`phone`=?,";
                                sql += "`registration_status`=?,`last_login_datetime`=?,`timezone`=?,`device_type`=?,";
                                sql += "`device_token`=?,`transport_type`=?,`transport_desc`=?,`license`=?,`color`=? WHERE `email`=? LIMIT 1";
                                connection.query(sql, [date, name, fleetImage, fleetThumbImage, access_token, email, encrypted_pass, phone, constants.userVerificationStatus.VERIFY, new Date(), timezone, device_type, device_token, transport_type, transport_desc, license, color, email], function (err, result_insert) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in inserting fleet info : ", err, result_insert);
                                        responses.sendError(res);
                                    } else {
                                        var response = {
                                            "message": constants.responseMessages.ACTION_COMPLETE,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": {
                                                access_token: access_token
                                            }
                                        };
                                        res.send(JSON.stringify(response));
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    }
};

/*
 * -----------------------------------------------
 * FLEET LOGIN VIA EMAIL AND PASSWORD
 * -----------------------------------------------
 */

exports.fleet_login = function (req, res) {
    var login_id = req.body.email,
        password = req.body.password,
        device_type = req.body.device_type,
        device_token = req.body.device_token,
        latitude = req.body.latitude,
        longitude = req.body.longitude,
        device_os = req.body.device_os,
        device_desc = req.body.device_desc,
        imei_number = req.body.imei_number,
        store_version = req.body.store_version,
        appVersion = req.body.app_version;
    var manvalues = [appVersion, login_id, password, device_token, device_type, longitude, latitude];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        var encrypted_pass = md5(password);
        var sql = "SELECT ft.`noti_tone`,ft.`store_version`,ft.`is_deleted`,ft.`fleet_id`,ft.`fleet_image`,IF(ft.`fleet_thumb_image` IS NULL,'',ft.`fleet_thumb_image`) as `fleet_thumb_image`,IF(ft.`username` IS NULL,'',ft.`username`) as `username`,ft.`access_token`,ft.`email`,IF(ft.`phone` IS NULL,'',ft.`phone`) as phone,";
        sql += " ft.`password`,IF(ft.`transport_type` IS NULL,'',ft.`transport_type`) as transport_type,IF(ft.`transport_desc` IS NULL,'',ft.`transport_desc`) as transport_desc,IF(ft.`license` IS NULL,'',ft.`license`) as license,IF(ft.`color` IS NULL,'',ft.`color`) as color,";
        sql += " ft.`user_id`,ft.`registration_status`,ft.`last_login_datetime`,ft.`fleet_id`,ft.`is_active`,ft.`first_name`,ft.`last_name`,ft.`login_id`,ft.`device_desc`,ft.`device_os`,ft.`is_available`, ";
        sql += " usr.`has_routing`,usr.`billing_plan`,usr.`has_invoicing_module` ";
        sql += " FROM `tb_fleets` ft ";
        sql += " INNER JOIN `tb_users` usr ON ft.`user_id`=usr.`user_id` ";
        sql += " WHERE ft.`login_id`=? LIMIT 1 ";
        connection.query(sql, [login_id], function (err, result_check) {
            if (err) {
                logging.logDatabaseQueryError("Error in fetching fleet Info : ", err, result_check);
                responses.sendError(res);
            } else {
                var response;
                if (result_check.length == 0) {
                    response = {
                        "message": constants.responseMessages.INVALID_USERNAME,
                        "status": constants.responseFlags.INVALID_USERNAME,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                } else {
                    commonFunc.checkAccountExpiry(result_check[0].user_id, function (checkAccountExpiryResult) {
                        if (checkAccountExpiryResult) {
                            responses.accountExpiryErrorResponse(res);
                        } else {
                            if (result_check[0].is_deleted == constants.userDeleteStatus.YES) {
                                response = {
                                    "message": constants.responseMessages.ACCOUNT_DELETED_ERROR,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {}
                                };
                                res.send(JSON.stringify(response));
                            } else if (result_check[0].password != encrypted_pass) {
                                response = {
                                    "message": constants.responseMessages.INCORRECT_PASSWORD,
                                    "status": constants.responseFlags.WRONG_PASSWORD,
                                    "data": {}
                                };
                                res.send(JSON.stringify(response));
                            } else if (result_check[0].is_active == constants.userActiveStatus.INACTIVE) {
                                response = {
                                    "message": constants.responseMessages.INACTIVE_ACCOUNT,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {}
                                };
                                res.send(JSON.stringify(response));
                            } else {

                                // check for version and user_id
                                checkAppVersion2(result_check[0].fleet_id, appVersion, device_type, function (updateAppPopUp) {


                                    if (updateAppPopUp.user_id) {
                                        if (updateAppPopUp.user_id == result_check[0].user_id) {
                                            finalTouch()
                                        } else {
                                            response = {
                                                "message": constants.responseMessages.ACCOUNT_NOT_REGISTER + updateAppPopUp.brand,
                                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                "data": {}
                                            };
                                            res.send(JSON.stringify(response));
                                            return;
                                        }
                                    } else {
                                        finalTouch()
                                    }

                                    function finalTouch() {

                                        var date = new Date(), access_token = md5(encrypted_pass + new Date());
                                        var sql = "UPDATE `tb_fleets` SET `store_version`=?,`imei_number`=?,`registration_status`=?,`access_token`=?,`location_update_datetime`=?," +
                                            " `latitude`=?,`longitude`=?,`last_login_datetime`=NOW(),`device_type`=?,`device_token`=?,`device_desc`=?,`device_os`=?,`is_available`=1 " +
                                            " WHERE `login_id`=? LIMIT 1";
                                        connection.query(sql, [store_version, imei_number, constants.userVerificationStatus.VERIFY, access_token, date, latitude,
                                            longitude, device_type, device_token, device_desc, device_os, login_id], function (err, result_update) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in updating fleet info : ", err, result_update);
                                                responses.sendError(res);
                                            } else {
                                                var sql = "SELECT ft.`team_id`,team.`team_name`,team.`battery_usage` " +
                                                    " FROM `tb_fleet_teams` ft " +
                                                    " INNER JOIN `tb_teams` team ON team.`team_id` = ft.`team_id` " +
                                                    " WHERE ft.`fleet_id`=? ";
                                                connection.query(sql, [result_check[0].fleet_id], function (err, result_teams) {
                                                    if (err) {
                                                        logging.logDatabaseQueryError("Error in fetching fleet teams : ", err, result_teams);
                                                        responses.sendError(res);
                                                    } else {
                                                        result_check[0].name = result_check[0].username;
                                                        result_check[0].access_token = access_token;
                                                        result_check[0].is_available = constants.availableStatus.AVAILABLE;
                                                        delete result_check[0].username;
                                                        result_check[0].teams = result_teams;
                                                        if (result_teams && result_teams.length)
                                                            result_check[0].battery_usage = result_teams[0].battery_usage
                                                        response = {
                                                            "message": constants.responseMessages.LOGIN_SUCCESSFULLY,
                                                            "status": constants.responseFlags.LOGIN_SUCCESSFULLY,
                                                            "data": {
                                                                "fleet_info": result_check,
                                                                "popup": updateAppPopUp.popup
                                                            }
                                                        };
                                                        socketResponse.sendFleetSocketResponse(result_check[0].user_id, result_check[0].fleet_id);
                                                        res.send(JSON.stringify(response));
                                                        return;
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
    }
};

/*
 * -----------------------------
 * FLEET LOGIN VIA ACCESS_TOKEN
 * -----------------------------
 */

exports.fleet_access_token_login = function (req, res) {

    var email = req.body.access_token,
        device_type = req.body.device_type,
        device_token = req.body.device_token,
        latitude = req.body.latitude,
        longitude = req.body.longitude,
        appVersion = req.body.app_version,
        store_version = req.body.store_version,
        device_os = req.body.device_os,
        imei_number = req.body.imei_number,
        device_desc = req.body.device_desc;
    var manvalues = [appVersion, email, device_type, device_token, latitude, longitude];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(email, function (call_response) {
            if (call_response == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                commonFunc.checkAccountExpiry(call_response[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        if (call_response[0].is_deleted == constants.userDeleteStatus.YES) {
                            response = {
                                "message": constants.responseMessages.ACCOUNT_DELETED_ERROR,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));

                        } else if (call_response[0].is_active == constants.userActiveStatus.INACTIVE) {
                            response = {
                                "message": constants.responseMessages.INACTIVE_ACCOUNT,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                        } else {


                            // check for version and user_id
                            checkAppVersion2(call_response[0].fleet_id, appVersion, device_type, function (updateAppPopUp) {

                                if (updateAppPopUp.user_id) {
                                    if (updateAppPopUp.user_id == call_response[0].user_id) {
                                        finalTouch()
                                    } else {
                                        var response = {
                                            "message": constants.responseMessages.ACCOUNT_NOT_REGISTER + updateAppPopUp.brand,
                                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                            "data": {}
                                        };
                                        res.send(JSON.stringify(response));
                                        return;
                                    }
                                } else {
                                    finalTouch()
                                }

                                function finalTouch() {

                                    var response, date = new Date();
                                    call_response[0].username = call_response[0].username == null ? '' : call_response[0].username;
                                    call_response[0].phone = call_response[0].phone == null ? '' : call_response[0].phone;
                                    call_response[0].transport_type = call_response[0].transport_type == null ? '' : call_response[0].transport_type;
                                    call_response[0].transport_desc = call_response[0].transport_desc == null ? '' : call_response[0].transport_desc;
                                    call_response[0].license = call_response[0].license == null ? '' : call_response[0].license;
                                    call_response[0].color = call_response[0].color == null ? '' : call_response[0].color;
                                    call_response[0].verification_token = call_response[0].verification_token == null ? '' : call_response[0].verification_token;
                                    call_response[0].fleet_thumb_image = call_response[0].fleet_thumb_image == null ? '' : call_response[0].fleet_thumb_image;
                                    var sql = "UPDATE `tb_fleets` SET `store_version`=?,`imei_number`=?,`registration_status`=?,`location_update_datetime`=?,`latitude`=?,`longitude`=?," +
                                        " `last_login_datetime`=NOW(),`device_type`=?,`device_token`=?,`device_desc`=?,`device_os`=? " +
                                        " WHERE `login_id`=? LIMIT 1";
                                    connection.query(sql, [store_version, imei_number, constants.userVerificationStatus.VERIFY, date, latitude, longitude,
                                        device_type, device_token, device_desc, device_os, call_response[0].login_id], function (err, result_update) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in updating fleet info : ", err, result_update);
                                            responses.sendError(res);
                                        } else {
                                            var sql = "SELECT ft.`team_id`,team.`team_name`,team.`battery_usage` " +
                                                " FROM `tb_fleet_teams` ft " +
                                                " INNER JOIN `tb_teams` team ON team.`team_id` = ft.`team_id` " +
                                                " WHERE ft.`fleet_id`=? ";
                                            connection.query(sql, [call_response[0].fleet_id], function (err, result_teams) {
                                                if (err) {
                                                    logging.logDatabaseQueryError("Error in fetching fleet teams : ", err, result_teams);
                                                    responses.sendError(res);
                                                } else {
                                                    call_response[0].name = call_response[0].username;
                                                    delete call_response[0].username;
                                                    delete call_response[0].dispatcher_id;

                                                    call_response[0].teams = result_teams;
                                                    if (result_teams && result_teams.length)
                                                        call_response[0].battery_usage = result_teams[0].battery_usage
                                                    response = {
                                                        "message": constants.responseMessages.LOGIN_SUCCESSFULLY,
                                                        "status": constants.responseFlags.LOGIN_SUCCESSFULLY,
                                                        "data": {
                                                            "fleet_info": call_response,
                                                            "popup": updateAppPopUp.popup
                                                        }
                                                    };
                                                    socketResponse.sendFleetSocketResponse(call_response[0].user_id, call_response[0].fleet_id);
                                                    res.send(JSON.stringify(response));

                                                }
                                            });
                                        }
                                    });
                                }
                            })
                        }
                    }
                });
            }
        });
    }
};


/*
 * -----------------------------
 * EDIT PROFILE INFORMATION
 * -----------------------------
 */

exports.edit_profile_information = function (req, res) {

    var access_token = req.body.access_token;
    var name = req.body.name;
    var timezone = req.body.timezone;
    var device_type = req.body.device_type;
    var device_token = req.body.device_token;
    var fleet_image = req.files.fleet_image;
    var transport_type = req.body.transport_type;
    var transport_desc = req.body.transport_desc;
    var license = req.body.license;
    var color = req.body.color;
    var phone = req.body.phone;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        if (typeof first_name === 'undefined') {
            var names = name.split(' ');
            first_name = names[0];
        }
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationError(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        if (req.files && req.files.fleet_image && req.files.fleet_image.name && req.files.fleet_image.size > 0) {
                            commonFunc.uploadImageToS3Bucket(req.files.fleet_image, config.get('s3BucketCredentials.folder.fleetProfileImages'), function (result_fleet_image) {
                                if (result_fleet_image == 0) {
                                    responses.uploadError(res);
                                } else {
                                    commonFunc.uploadThumbImageToS3Bucket(req.files.fleet_image, config.get('s3BucketCredentials.folder.fleetProfileThumbImages'), function (result_fleet_thumb_image) {
                                        if (result_fleet_thumb_image == 0) {
                                            responses.uploadError(res);
                                        } else {
                                            var fleetImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.fleetProfileImages') + '/' + result_fleet_image;
                                            var fleetThumbImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.fleetProfileThumbImages') + '/' + result_fleet_thumb_image;
                                            var sql = "UPDATE `tb_fleets` SET `first_name`=?,`last_name`=?,`fleet_image`=?,`fleet_thumb_image`=?,`username`=?,`phone`=?,";
                                            sql += "`timezone`=?,`device_type`=?,`device_token`=?,`transport_type`=?,`transport_desc`=?,`license`=?,`color`=? WHERE `access_token`=? LIMIT 1";
                                            connection.query(sql, [first_name, last_name, fleetImage, fleetThumbImage, name, phone, timezone, device_type, device_token, transport_type, transport_desc, license, color, access_token], function (err, result_insert) {
                                                if (err) {
                                                    logging.logDatabaseQueryError("Error in inserting fleet info : ", err, result_insert);
                                                    responses.sendError(res);
                                                } else {
                                                    var response = {
                                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                                        "data": {
                                                            fleetImage: fleetImage
                                                        }
                                                    };
                                                    res.send(JSON.stringify(response));
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        } else {
                            var fleetImage = result[0].fleet_image;
                            var fleetThumbImage = result[0].fleet_thumb_image;
                            var sql = "UPDATE `tb_fleets` SET `first_name`=?,`last_name`=?,`fleet_image`=?,`fleet_thumb_image`=?,`username`=?,`phone`=?,";
                            sql += "`timezone`=?,`device_type`=?,`device_token`=?,`transport_type`=?,`transport_desc`=?,`license`=?,`color`=? WHERE `access_token`=? LIMIT 1";
                            connection.query(sql, [first_name, last_name, fleetImage, fleetThumbImage, name, phone, timezone, device_type, device_token, transport_type, transport_desc, license, color, access_token], function (err, result_insert) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in inserting fleet info : ", err, result_insert);
                                    responses.sendError(res);
                                } else {
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": {
                                            fleetImage: fleetImage
                                        }
                                    };
                                    res.send(JSON.stringify(response));
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};


/*
 * ----------------------
 * CHANGE JOB STATUS
 * ----------------------
 */

exports.change_job_status = function (req, res) {
    var access_token = req.body.access_token;
    var job_status = parseInt(req.body.job_status);
    var job_id = req.body.job_id;
    var reason = req.body.reason;
    if (typeof reason === "undefined" || reason === "") {
        reason = '';
    }
    var custom_fields = req.body.custom_fields;
    if (typeof custom_fields === "undefined" || custom_fields === "") {
        custom_fields = [];
    } else {
        custom_fields = JSON.parse(req.body.custom_fields);
    }
    var manvalues = [access_token, job_status, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        job_status = parseInt(job_status);
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else if (result[0].is_available == constants.availableStatus.NOT_AVAILABLE) {
                var response = {
                    "message": constants.responseMessages.FLEET_OFFLINE_ERROR,
                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                    "data": {}
                };
                res.send(JSON.stringify(response));
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {

                        // Insert Total Distance Travelled to DB.
                        commonFunc.insertTotalDistanceTravelled(job_id);

                        var cf_len = custom_fields.length;
                        if (cf_len > 0) {
                            var index = 0;
                            addCustomField(index);
                            function addCustomField(item) {
                                if (item > (cf_len - 1)) {
                                    processHit(job_id, job_status)
                                } else {
                                    commonFunc.delay(item, function (i) {
                                        (function (i) {
                                            mongo.updateTaskCustomField(result[0].user_id, job_id, custom_fields[i].custom_field_label, custom_fields[i].data, 'update', function (updateTaskCustomFieldResult) {
                                                if (updateTaskCustomFieldResult.status) {
                                                    if (updateTaskCustomFieldResult.data) {
                                                        updateTaskCustomFieldResult.data = commonFunc.processCustomField(updateTaskCustomFieldResult.data);
                                                        var text = result[0].first_name + " has updated custom field " + custom_fields[i].custom_field_label + " to task " + job_id;
                                                        commonFunc.setNotification(result[0].user_id, text, commonFunc.reverseTaskHistory('custom_field_updated'), job_id);
                                                        commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.CUSTOM_FIELD, job_id, JSON.stringify(updateTaskCustomFieldResult.data));
                                                        //console.log(custom_fields[i].custom_field_label, " custom field updated to ", custom_fields[i].data);
                                                        index++;
                                                        addCustomField(index);
                                                    }
                                                }
                                            });
                                        })(i);
                                    });
                                }
                            }
                        } else {
                            processHit(job_id, job_status);
                        }

                        function processHit(job_id, job_status) {

                            if (job_status == constants.jobStatus.IGNORED) { // WHEN JOB IGNORED
                                commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.TASK_PUSH_NOTIFY, job_id, result[0].username + " ignored the task.");
                                responses.actionCompleteResponse(res);
                            } else {
                                commonFunc.authenticateAutoAssignTask(result[0].fleet_id, result[0].username, job_id, job_status, function (authenticateAutoAssignTaskResult) {
                                    if (authenticateAutoAssignTaskResult == 1) {
                                        commonFunc.authenticateFleetIdAndJobId(result[0].fleet_id, job_id, function (authenticateFleetIdAndJobIdResult) {
                                            if (authenticateFleetIdAndJobIdResult == 0) {
                                                responses.authenticationErrorFleetAndJobID(res);
                                            } else {

                                                commonFunc.updateNotificationCount(result[0].user_id);
                                                if (job_status == constants.jobStatus.STARTED) { // WHEN JOB STARTS

                                                    // UPDATE FLEET LOCATION
                                                    commonFunc.updateFleetLocation(result[0].fleet_id, {
                                                        "lat": result[0].latitude,
                                                        "lng": result[0].longitude,
                                                        "bat_lvl": result[0].battery_level,
                                                        "gps": result[0].has_gps_accuracy,
                                                        "net": result[0].has_network,
                                                        "mock": result[0].has_mock_loc
                                                    });

                                                    commonFunc.checkPickupComplete(job_id, result[0].fleet_id, function (checkPickupCompleteResult) {
                                                        if (checkPickupCompleteResult.status == false) {
                                                            var response = {
                                                                "message": constants.responseMessages.PICKUP_NOT_COMPLETED,
                                                                "status": constants.responseFlags.SHOW_WARNING,
                                                                "data": {
                                                                    job_id: checkPickupCompleteResult.job_id
                                                                }
                                                            };
                                                            res.send(JSON.stringify(response));

                                                        } else if (checkPickupCompleteResult.status == true) {

                                                            var text = result[0].username + " has started the task " + job_id;
                                                            commonFunc.setNotification(result[0].user_id, text, "Started the Task", job_id);
                                                            commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Started at");
                                                            commonFunc.changeFleetStatus(result[0].fleet_id, constants.userFreeStatus.BUSY);
                                                            commonFunc.sendTemplateEmailAndSMS(authenticateFleetIdAndJobIdResult[0].user_id, 'AGENT_STARTED', job_id);
                                                            //UPDATE FLEET MOVEMENT
                                                            commonFunc.updateFleetMovement('(' + result[0].fleet_id + ',' + result[0].latitude + ',' + result[0].longitude + ')');
                                                            updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res);
                                                            // DeleteScheduleFleetNotificationAfterTaskStart
                                                            cron.deleteScheduleFleetNotificationAfterTaskStart(job_id, result[0].fleet_id, result[0].user_id)
                                                        }
                                                    });

                                                } else if (job_status == constants.jobStatus.ARRIVED) { // WHEN JOB ARRIVED

                                                    var text = result[0].username + " has reached the destination for the task " + job_id;
                                                    commonFunc.setNotification(result[0].user_id, text, "Reached the Destination", job_id);
                                                    commonFunc.changeFleetStatus(result[0].fleet_id, constants.userFreeStatus.BUSY);
                                                    commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Arrived at");
                                                    commonFunc.sendTemplateEmailAndSMS(authenticateFleetIdAndJobIdResult[0].user_id, 'AGENT_ARRIVED', job_id);
                                                    //UPDATE FLEET MOVEMENT
                                                    commonFunc.updateFleetMovement('(' + result[0].fleet_id + ',' + result[0].latitude + ',' + result[0].longitude + ')');
                                                    updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res);
                                                    // DeleteScheduleFleetNotificationAfterTaskStart
                                                    cron.deleteScheduleFleetNotificationAfterTaskStart(job_id, result[0].fleet_id, result[0].user_id)

                                                }
                                                else if (job_status == constants.jobStatus.ENDED) { // WHEN JOB COMPLETED

                                                    var text = result[0].username + " has successfully completed the task " + job_id;
                                                    commonFunc.setNotification(result[0].user_id, text, "Successfully completed", job_id);
                                                    commonFunc.changeFleetStatus(result[0].fleet_id, constants.userFreeStatus.FREE);
                                                    commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Successful at");
                                                    commonFunc.sendTemplateEmailAndSMS(authenticateFleetIdAndJobIdResult[0].user_id, 'SUCCESSFUL', job_id);
                                                    //UPDATE FLEET MOVEMENT
                                                    commonFunc.updateFleetMovement('(' + result[0].fleet_id + ',' + result[0].latitude + ',' + result[0].longitude + ')');
                                                    updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res);
                                                    // DeleteScheduleFleetNotificationAfterTaskStart
                                                    cron.deleteScheduleFleetNotificationAfterTaskStart(job_id, result[0].fleet_id, result[0].user_id)

                                                } else if (job_status == constants.jobStatus.ACCEPTED) { // WHEN JOB ACCEPTED
                                                    var text = result[0].username + " has accepted the task " + job_id;
                                                    commonFunc.setNotification(result[0].user_id, text, "Task Accepted", job_id);
                                                    commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Accepted at");
                                                    //UPDATE FLEET MOVEMENT
                                                    commonFunc.updateFleetMovement('(' + result[0].fleet_id + ',' + result[0].latitude + ',' + result[0].longitude + ')');
                                                    updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res);


                                                } else if (job_status == constants.jobStatus.DECLINE) { // WHEN JOB DECLINE

                                                    var text = result[0].username + " has declined the task " + job_id;
                                                    commonFunc.setNotification(result[0].user_id, text, "Task Declined", job_id);
                                                    commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Declined at");
                                                    //UPDATE FLEET MOVEMENT
                                                    commonFunc.updateFleetMovement('(' + result[0].fleet_id + ',' + result[0].latitude + ',' + result[0].longitude + ')');
                                                    updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res);
                                                    // DeleteScheduleFleetNotificationAfterTaskStart
                                                    cron.deleteScheduleFleetNotificationAfterTaskStart(job_id, result[0].fleet_id, result[0].user_id)

                                                } else if (job_status == constants.jobStatus.FAILED) { // WHEN JOB FAILED

                                                    var text = result[0].username + " has failed the task " + job_id;
                                                    commonFunc.setNotification(result[0].user_id, text, "Task Failed", job_id);
                                                    commonFunc.changeFleetStatus(result[0].fleet_id, constants.userFreeStatus.FREE);
                                                    commonFunc.sendTemplateEmailAndSMS(authenticateFleetIdAndJobIdResult[0].user_id, 'FAILED', job_id);
                                                    reason = reason ? ("#-#" + reason) : '';
                                                    commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Failed at" + reason);
                                                    //UPDATE FLEET MOVEMENT
                                                    commonFunc.updateFleetMovement('(' + result[0].fleet_id + ',' + result[0].latitude + ',' + result[0].longitude + ')');
                                                    updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res);
                                                    // DeleteScheduleFleetNotificationAfterTaskStart
                                                    cron.deleteScheduleFleetNotificationAfterTaskStart(job_id, result[0].fleet_id, result[0].user_id);

                                                } else if (job_status == constants.jobStatus.PARTIAL) { // WHEN JOB PARTIALLY COMPLETED

                                                    var text = result[0].username + " has partially completed the task " + job_id;
                                                    commonFunc.setNotification(result[0].user_id, text, "Task Partially Completed", job_id);
                                                    commonFunc.changeFleetStatus(result[0].fleet_id, constants.userFreeStatus.FREE);
                                                    commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Partially completed at");
                                                    //UPDATE FLEET MOVEMENT
                                                    commonFunc.updateFleetMovement('(' + result[0].fleet_id + ',' + result[0].latitude + ',' + result[0].longitude + ')');
                                                    updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res);
                                                    // DeleteScheduleFleetNotificationAfterTaskStart
                                                    cron.deleteScheduleFleetNotificationAfterTaskStart(job_id, result[0].fleet_id, result[0].user_id)

                                                } else if (job_status == constants.jobStatus.CANCEL) { // WHEN JOB CANCELLED

                                                    var text = result[0].username + " has canceled the task " + job_id;
                                                    commonFunc.setNotification(result[0].user_id, text, "Task Canceled", job_id);
                                                    commonFunc.changeFleetStatus(result[0].fleet_id, constants.userFreeStatus.FREE);
                                                    reason = reason ? ("#-#" + reason) : '';
                                                    commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.STATE_CHANGED, job_id, "Cancelled at" + reason);
                                                    //UPDATE FLEET MOVEMENT
                                                    commonFunc.updateFleetMovement('(' + result[0].fleet_id + ',' + result[0].latitude + ',' + result[0].longitude + ')');
                                                    updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res);
                                                    // DeleteScheduleFleetNotificationAfterTaskStart
                                                    cron.deleteScheduleFleetNotificationAfterTaskStart(job_id, result[0].fleet_id, result[0].user_id)
                                                }
                                                //});
                                            }
                                        });
                                    } else {
                                        responses.actionCompleteResponse(res);
                                    }
                                })
                            }
                        }
                    }
                })
            }
        })
    }
}


function updateJob(job_id, job_status, result, authenticateFleetIdAndJobIdResult, res) {

    var bindparams;
    var sql = "UPDATE `tb_jobs` SET `job_status`=? ";
    if (job_status == constants.jobStatus.STARTED) {
        sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN NOW() ELSE `acknowledged_datetime` END ";
        sql += ",`started_datetime` = NOW() ";
    }
    if (job_status == constants.jobStatus.ARRIVED) {
        sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN NOW() ELSE `acknowledged_datetime` END ";
        sql += ",`started_datetime` = CASE WHEN `started_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `started_datetime` END ";
        sql += ",`arrived_datetime` = NOW() ";
    }
    if ((job_status == constants.jobStatus.ENDED) || (job_status == constants.jobStatus.FAILED) || (job_status == constants.jobStatus.CANCEL)) {
        sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `acknowledged_datetime` END ";
        sql += ",`started_datetime` = CASE WHEN `started_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `started_datetime` END ";
        sql += ",`arrived_datetime` = CASE WHEN `arrived_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `arrived_datetime` END ";
        sql += ",`completed_datetime` = NOW() ";
    }
    if (job_status == constants.jobStatus.ACCEPTED || job_status == constants.jobStatus.DECLINE) {
        sql += ",`acknowledged_datetime` = NOW() ";
        sql += " WHERE `pickup_delivery_relationship`=? AND `fleet_id`=?";
        bindparams = [job_status, authenticateFleetIdAndJobIdResult[0].pickup_delivery_relationship, result[0].fleet_id];
    } else {
        sql += " WHERE `job_id`=? AND `fleet_id`=? LIMIT 1";
        bindparams = [job_status, job_id, result[0].fleet_id];
    }
    connection.query(sql, bindparams, function (err, result_update) {
        if (err) {
            logging.logDatabaseQueryError("Error in updating jobs info : ", err, result_update);
            responses.sendError(res);
        } else {
            if (job_status == constants.jobStatus.ACCEPTED || job_status == constants.jobStatus.DECLINE) {
                socketResponse.sendSocketResponseForPickAndDelivery(job_id);
            } else {
                socketResponse.sendSocketResponse(job_id);
            }
            responses.actionCompleteResponse(res);
        }
    });
}


/*
 * ------------------
 * ADD TASK DETAILS
 * ------------------
 */

exports.add_task_details = function (req, res) {
    var access_token = req.body.access_token;
    var text = req.body.text;
    if (typeof text === "undefined") {
        text = '';
    }
    var type = req.body.type;
    var job_id = req.body.job_id;
    var manvalues = [access_token, type, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else if (result[0].is_available == constants.availableStatus.NOT_AVAILABLE) {
                var response = {
                    "message": constants.responseMessages.FLEET_OFFLINE_ERROR,
                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                    "data": {}
                };
                res.send(JSON.stringify(response));
            }
            else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        insertTaskDetails(text, req, result[0].fleet_id, type, job_id, function (insertOrUpdateTaskDetailsResult) {
                            var text = result[0].username + " has added " + type.split("_")[0] + " to task " + job_id;
                            commonFunc.setNotification(result[0].user_id, text, commonFunc.reverseTaskHistory(type), job_id);
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {
                                    "insertedID": insertOrUpdateTaskDetailsResult
                                }
                            };
                            //socketResponse.sendNotificationSocketResponse(result[0].user_id, result[0].fleet_id);
                            socketResponse.sendSocketResponse(job_id);
                            res.send(JSON.stringify(response));
                        })
                    }
                });
            }
        });
    }
}


function insertTaskDetails(has_text, req, fleet_id, type, job_id, callback) {
    if (req.files && req.files.image && req.files.image.name && req.files.image.size > 0) {
        commonFunc.uploadImageToS3Bucket(req.files.image, config.get('s3BucketCredentials.folder.taskAcknowledgementImages'), function (imageResult) {
            if (imageResult == 0) {
                responses.uploadError(res);
            } else {
                var Image = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.taskAcknowledgementImages') + '/' + imageResult;
                if (has_text) {
                    Image = has_text + "==Tookan==" + Image;
                }
                commonFunc.setTaskHistoryCallback(fleet_id, type, job_id, Image, function (setTaskHistoryCallbackResult) {
                    callback(setTaskHistoryCallbackResult)
                });
            }
        });
    } else {
        if (has_text) {
            commonFunc.setTaskHistoryCallback(fleet_id, type, job_id, has_text, function (setTaskHistoryCallbackResult) {
                callback(setTaskHistoryCallbackResult)
            });
        } else {
            callback(0)
        }
    }
}


/*
 * ------------------
 * DELETE TASK DETAILS
 * ------------------
 */

exports.delete_task_detail = function (req, res) {

    var access_token = req.body.access_token;
    var type = req.body.type;
    var job_id = req.body.job_id;
    var id = req.body.id;
    var manvalues = [access_token, type, id, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);

            } else if (result[0].is_available == constants.availableStatus.NOT_AVAILABLE) {
                var response = {
                    "message": constants.responseMessages.FLEET_OFFLINE_ERROR,
                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                    "data": {}
                };
                res.send(JSON.stringify(response));
            }
            else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        commonFunc.updateTaskHistoryCallback(result[0].fleet_id, type, job_id, "Image Deleted", id, function (deleteTaskHistoryResult) {
                            var response = {
                                "message": constants.responseMessages.ACTION_COMPLETE,
                                "status": constants.responseFlags.ACTION_COMPLETE,
                                "data": {
                                    "insertedID": deleteTaskHistoryResult
                                }
                            };
                            res.send(JSON.stringify(response));
                        });

                    }
                });
            }
        });
    }
}


/*
 * -------------------
 * UPDATE TASK DETAIL
 * -------------------
 */

exports.update_task_detail = function (req, res) {

    var access_token = req.body.access_token;
    var type = req.body.type;
    var text = req.body.text;
    var job_id = req.body.job_id;
    var id = req.body.id;
    var manvalues = [access_token, type, id, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);

            } else if (result[0].is_available == constants.availableStatus.NOT_AVAILABLE) {
                var response = {
                    "message": constants.responseMessages.FLEET_OFFLINE_ERROR,
                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                    "data": {}
                };
                res.send(JSON.stringify(response));
            }
            else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        commonFunc.updateTaskHistoryCallback(result[0].fleet_id, type + "_from", job_id, "Updated", id, function (deleteTaskHistoryResult) {
                            insertTaskDetails(text, req, result[0].fleet_id, type, job_id, function (insertOrUpdateTaskDetailsResult) {
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data": {
                                        "insertedID": insertOrUpdateTaskDetailsResult
                                    }
                                };
                                res.send(JSON.stringify(response));
                            })
                        });
                    }
                });
            }
        });
    }
}

/*
 * -----------------------------------------------
 * CHANGE FLEET PASSWORD
 * -----------------------------------------------
 */

exports.fleet_change_password = function (req, res) {

    var access_token = req.body.access_token;
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;
    var manvalues = [access_token, old_password, new_password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);

                    } else {
                        if (old_password == new_password) {
                            var response = {
                                "message": constants.responseMessages.SAME_PASSWORD_ERROR,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));

                        } else {
                            var encrypted_old_pass = md5(old_password);
                            var encrypted_new_pass = md5(new_password);
                            var sql = "SELECT `password` FROM `tb_fleets` WHERE `access_token`=? LIMIT 1";
                            connection.query(sql, [access_token], function (err, result_check) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in fetching admin info : ", err, result_check);
                                    responses.sendError(res);

                                } else {
                                    if (result_check[0].password != encrypted_old_pass) {
                                        var response = {
                                            "message": constants.responseMessages.CURRENT_PASSWORD_INCORRECT,
                                            "status": constants.responseFlags.WRONG_PASSWORD,
                                            "data": {}
                                        };
                                        res.send(JSON.stringify(response));

                                    } else {
                                        if (new_password.length < config.get('AppPasswordLength')) {
                                            var response = {
                                                "message": constants.responseMessages.APP_PASSWORD_ERROR,
                                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                "data": {}
                                            };
                                            res.send(JSON.stringify(response));

                                        } else {
                                            var new_access_token = md5(access_token + new Date());
                                            var sql = "UPDATE `tb_fleets` SET `password`=?,`access_token`=? WHERE `access_token`=? LIMIT 1";
                                            connection.query(sql, [encrypted_new_pass, new_access_token, access_token], function (err, result_check) {
                                                if (err) {
                                                    logging.logDatabaseQueryError("Error in updating fleets new password : ", err, result_check);
                                                    responses.sendError(res);

                                                } else {
                                                    var response = {
                                                        "message": constants.responseMessages.PASSWORD_CHANGED_SUCCESSFULLY,
                                                        "status": constants.responseFlags.PASSWORD_CHANGED_SUCCESSFULLY,
                                                        "data": {
                                                            "access_token": new_access_token
                                                        }
                                                    };
                                                    res.send(JSON.stringify(response));

                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};

/*
 * ----------------------------------
 *  FORGOT PASSWORD
 * ----------------------------------
 */

exports.forgot_password = function (req, res) {
    var md5 = require('MD5');
    var token = req.body.token;
    var password = req.body.password;
    var type = req.body.type;
    var email = req.body.email;
    var manValues = [token, password, email, type];
    var checkdata = commonFunc.checkBlank(manValues);
    if (checkdata == 1) {
        responses.parameterMissingResponse(res);

    } else {
        commonFunc.authenticateForgotToken(token, type, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (chk) {
                    if (chk) {
                        responses.accountExpiryErrorResponse(res);

                    } else {
                        password = md5(password);
                        var verificationToken = md5(token + commonFunc.generateRandomString());
                        var engagements = '';
                        if (type == 'changeF') {
                            engagements = "UPDATE `tb_fleets` SET `password` = ?,`verification_token` = ? WHERE ";
                            engagements += "`verification_token` = ?  LIMIT 1";
                        }
                        else if (type == 'changeU') {
                            engagements = "UPDATE `tb_users` SET `password` = ?,`verification_token` = ? WHERE ";
                            engagements += "`verification_token` = ? LIMIT 1";
                        }
                        connection.query(engagements, [password, verificationToken, token], function (err, forgotPasswordResult) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in changing forgot password : ", err, forgotPasswordResult);
                                responses.sendError(res);

                            } else {
                                if (forgotPasswordResult.affectedRows > 0) {
                                    responses.actionCompleteResponse(res);

                                } else {
                                    var response = {
                                        "message": constants.responseMessages.SHOW_ERROR_MESSAGE,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));

                                }
                            }
                        });
                    }
                });
            }
        });
    }
};

/*
 * ----------------------------------
 * FLEET FORGOT PASSWORD FROM EMAIL
 * ----------------------------------
 */
exports.fleet_forgot_password_from_email = function (req, res) {

    var email = req.body.email;
    var manValues = [email];
    var checkdata = commonFunc.checkBlank(manValues);
    if (checkdata == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetEmail(email, function (result) {
            if (result == 0) {
                responses.authenticateEmailNotExists(res);
            } else {
                var version_user_id = result[0].user_id
                commonFunc.getVersion2(version_user_id, function (version) {
                    var brand_name = config.get('projectName');
                    if (version && version.length) {
                        brand_name = version[0].brand_name;
                    }
                    var username = '';
                    if (result[0].username) {
                        username = result[0].username
                    }
                    var md5 = require('MD5');
                    var token = md5(result[0].email);
                    var sql = "UPDATE `tb_fleets` SET `verification_token`=? WHERE `email`=? LIMIT 1";
                    connection.query(sql, [token, email], function (err, response) {

                        var link = config.get("forgotPasswordPageLink");
                        link += "?token=" + token + "&email=" + email + "&type=changeF";

                        commonFunc.emailPlainFormatting(username, '', '', '', link, function (returnMessage) {
                            returnMessage = returnMessage.replace(/BRAND_NAME/g,brand_name);
                            commonFunc.sendHtmlContent(email, returnMessage, "[" + brand_name + "] Forgot Password", result[0].call_fleet_as, function (result) {
                                responses.actionCompleteResponse(res);
                            });
                        })
                    });
                });
            }
        });
    }
};

/*
 * -----------------------------------------------------
 * UPDATE LATITUDE AND LONGITUDE OF FLEET WHILE DELIVERY
 * -----------------------------------------------------
 */

exports.update_fleet_location = function (req, res) {
    //console.log("location ==== ", req.body);
    var access_token = req.body.access_token,
        latitude = req.body.latitude,
        longitude = req.body.longitude,
        location = req.body.location;
    if (typeof location === "undefined") {
        location = [];
    } else {
        location = JSON.parse(req.body.location);
    }
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                result[0].fleet_status_color = constants.fleetStatusColor[result[0].status][result[0].is_available];
                result[0].last_updated_location_time = parseInt(result[0].last_updated_location_time);
                if (result[0].last_updated_location_time.toString() == "NaN") {
                    result[0].last_updated_timings = constants.highest.VALUE;
                    result[0].last_updated_location_time = 'Not updated.';
                } else {
                    result[0].last_updated_timings = result[0].last_updated_location_time;
                    result[0].last_updated_location_time = commonFunc.timeDifferenceInWords(result[0].has_gps_accuracy, result[0].is_available, result[0].last_updated_location_time);
                }
                var locationLength = location.length, locationArray = [];

                // if location is coming in form of array
                if (locationLength > 0) {
                    updateFleetTracking(location, result, function (tracking) {
                        commonFunc.updateFleetLocation(result[0].fleet_id, {
                            "lat": location[locationLength - 1].lat,
                            "lng": location[locationLength - 1].lng,
                            "bat_lvl": location[locationLength - 1].bat_lvl,
                            "gps": location[locationLength - 1].gps,
                            "net": location[locationLength - 1].net,
                            "mock": location[locationLength - 1].mock,
                            "d_acc": location[locationLength - 1].d_acc,
                            "location": {
                                "fleet_id": result[0].fleet_id,
                                "points": location
                            }
                        });
                        responses.actionCompleteResponse(res);
                    })
                } else {
                    var loc = {
                        "lat": latitude,
                        "lng": longitude,
                        "acc": 0,
                        "bat_lvl": null,
                        "gps": 1,
                        "net": 1,
                        "mock": 0,
                        "d_acc": 0,
                        "location": {
                            "fleet_id": result[0].fleet_id,
                            "points": {
                                lat: latitude,
                                lng: longitude
                            }
                        }
                    };
                    commonFunc.updateFleetLocation(result[0].fleet_id, loc);
                    locationArray.push(loc);
                    updateFleetTracking(locationArray, result, function (updateLocationResult) {
                        responses.actionCompleteResponse(res);
                    })
                }
            }
        });
    }
};


function processLocations(fleet_id, time, location, callback) {

    var movements = [], movementStr = '', loc_len = location.length, index = 0,
        dist = 0, time_in_sec = commonFunc.timeDifferenceInSeconds(time, new Date());// in SEC;

    processloop(index);

    function processloop(item) {
        if (item > loc_len - 1) {

            movementStr = movements.join(',');
            callback({
                movements: movementStr,
                time_in_sec: time_in_sec,
                dist: dist,
                latitude: location[item - 1].lat,
                longitude: location[item - 1].lng
            })

        } else {
            commonFunc.delay2(item, function (i) {
                (function (i) {

                    var lat = 0, lng = 0, plat = 0, plng = 0;
                    if (location[i] && location[i].lat) lat = location[i].lat;
                    if (location[i] && location[i].lng) lng = location[i].lng;
                    if (location[i - 1] && location[i - 1].lat) plat = location[i - 1].lat;
                    if (location[i - 1] && location[i - 1].lng) plng = location[i - 1].lng;

                    if (lat && lng && plat && plng) {
                        dist = geolib.getDistance(
                            {
                                latitude: lat,
                                longitude: lng
                            },
                            {
                                latitude: plat,
                                longitude: plng
                            },
                            {
                                unit: 'm'
                            }
                        );// in Meters
                    }
                    var speed = (dist / 1000) / (time_in_sec / 3600);
                    if ((location[i].d_acc) || ((dist > 2 * location[i].acc) && (location[i].acc < 200) && (speed <= 100))) {
                        time_in_sec += time_in_sec;
                        dist += dist;
                        if (location[i].lat && location[i].lng) {
                            movements.push("( " + fleet_id + " , " + location[i].lat + " , " + location[i].lng + " )");
                        }
                    }
                    index++;
                    processloop(index);
                })(i);
            });
        }
    }

}


function updateFleetTracking(location, result, callback) {
    var fleet_id = result[0].fleet_id, user_id = result[0].user_id, date = new Date(), updated_datetime = new Date();

    var sql = "SELECT `latitude`,`longitude`,`fleet_id`,`updated_datetime`,`distance_in_metres` " +
        "FROM `tb_fleet_tracking` " +
        "WHERE `fleet_id` = ? AND DATE(`updated_datetime`)=DATE(NOW())";
    connection2.query(sql, [fleet_id], function (err, tracking) {
        if (err) {
            logging.logDatabaseQueryError("Error in selecting result_select_tb_fleet_tracking  : ", err, tracking);
            callback(0);
        } else {

            if (tracking && tracking.length) {
                updated_datetime = tracking[0].updated_datetime
            }

            // process locations
            processLocations(fleet_id, updated_datetime, location, function (locations) {

                // update fleet movements
                commonFunc.updateFleetMovement(locations.movements);


                // fleet tracking exists for the day
                if (tracking && tracking.length) {

                    var sql = " UPDATE `tb_fleet_tracking` " +
                        "SET `user_id`=?,`updated_datetime`=NOW(),`latitude`=?,`longitude`=?,`distance_in_metres`=`distance_in_metres`+?,`time_in_sec`=`time_in_sec`+? ";
                    if (result[0].status == constants.userFreeStatus.BUSY) {
                        sql += " ,`enroute_distance` = `enroute_distance` + " + locations.dist + ",`enroute_time` = `enroute_time`+ " + locations.time_in_sec + " ";
                    }
                    sql += " WHERE `fleet_id`=? AND DATE(`updated_datetime`)=DATE(NOW())";
                    connection.query(sql, [user_id, locations.latitude, locations.longitude, locations.dist, locations.time_in_sec, fleet_id], function (err, tracking) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in updating result_update_tb_fleet_tracking  : ", err, tracking);
                            callback(0);
                        } else {
                            callback(1);
                        }
                    });
                }
                // fleet tracking not exists for the day
                else {
                    var sql = "INSERT INTO `tb_fleet_tracking` (`user_id`,`latitude`,`longitude`,`fleet_id`,`updated_datetime`,`distance_in_metres`," +
                        "`time_in_sec`,`enroute_distance`,`enroute_time`) " +
                        "VALUES (?,?,?,?,?,?,?,?,?)";
                    connection2.query(sql, [user_id, locations.latitude, locations.longitude, fleet_id, date, 0, 0, 0, 0], function (err, tracking) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in inserting into result_insert_tb_fleet_tracking  : ", err, tracking);
                            callback(0);
                        } else {
                            callback(1);
                        }
                    })
                }
            })
        }
    })
}


/*
 * ---------------------------
 * FLEET LOGOUT FUNCTION
 * ---------------------------
 */

exports.fleet_logout = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);

    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var user_id = result[0].user_id, fleet_id = result[0].fleet_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        var new_access_token = md5(new Date());
                        var sql = "UPDATE `tb_fleets` SET `is_available`=0,`access_token`=?,`device_token`=? WHERE `fleet_id`=? LIMIT 1";
                        connection.query(sql, [new_access_token, null, fleet_id], function (err, result_check) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating fleet information  : ", err, result_check);
                                responses.sendError(res);
                            } else {
                                socketResponse.sendFleetSocketResponse(user_id, fleet_id);
                                responses.actionCompleteResponse(res);

                            }
                        });
                    }
                });
            }
        });
    }
};

/*
 * --------------------------------
 * SET FLEET APP SIDE NOTIFICATION
 * --------------------------------
 */

exports.set_notification_tone = function (req, res) {
    var access_token = req.body.access_token;
    var fleet_id = req.body.fleet_id;
    var tone = req.body.tone;
    var manvalues = [access_token, fleet_id, tone];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAccessTokenANDFleetID(access_token, fleet_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var sql = "UPDATE `tb_fleets` SET `noti_tone`=? WHERE `fleet_id`=? LIMIT 1";
                connection.query(sql, [tone, result[0].fleet_id], function (err, noti_tone) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in setting noti_tone information  : ", err, noti_tone);
                        responses.sendError(res);
                    } else {
                        responses.actionCompleteResponse(res);
                    }
                });
            }
        });
    }
}
/*
 * ---------------------------
 * CHANGE FLEET STATUS
 * ---------------------------
 */

exports.change_fleet_status = function (req, res) {

    var access_token = req.body.access_token;
    var is_available = req.body.is_available;
    var manvalues = [access_token, is_available];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var user_id = result[0].user_id;
                var fleet_id = result[0].fleet_id
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        commonFunc.checkFleetStatus(fleet_id, function (checkFleetStatusResult) {
                            if (checkFleetStatusResult == 0) {
                                responses.authenticateEmailNotExists(res);
                            } else {
                                if ((result[0].is_available) && (checkFleetStatusResult[0].status == constants.userFreeStatus.BUSY)) {
                                    var response = {
                                        "message": constants.responseMessages.ACTION_NOT_ALLOWED,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                } else {
                                    var sql = "UPDATE `tb_fleets` SET `is_available`=? WHERE `fleet_id`=? LIMIT 1";
                                    connection.query(sql, [is_available, fleet_id], function (err, result_check) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in updating fleet is_avalilable information  : ", err, result_check);
                                        } else {
                                            socketResponse.sendFleetSocketResponse(user_id, fleet_id);
                                            responses.actionCompleteResponse(res);
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });
    }
};


/*
 * --------------------------
 * VIEW FLEET PROFILE
 * --------------------------
 */
exports.view_fleet_profile = function (req, res) {

    var access_token = req.body.access_token;
    var fleet_id = req.body.fleet_id;
    var manvalues = [access_token, fleet_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);

    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var user_id = result[0].user_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    user_id = result[0].dispatcher_user_id;
                    step_in(user_id);
                } else {
                    step_in(user_id);
                }
                function step_in(user_id) {

                    commonFunc.getFleetAvalability(user_id, fleet_id, function (getFleetAvalabilityResult) {

                        if (getFleetAvalabilityResult != 0) {

                            var sql = "SELECT  fleets.`email`,fleets.`phone`,fleets.`fleet_id`,fleets.`username` as `fleet_name`,fleets.`fleet_image`,fleets.`fleet_thumb_image`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`," +
                                "jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`pickup_delivery_relationship`," +
                                "jobs.`team_id`," +
                                "jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`,jobs.`job_pickup_email`, " +
                                "jobs.`customer_id`,jobs.`customer_username`,jobs.`customer_phone`,jobs.`customer_email` " +
                                "FROM `tb_fleets` fleets LEFT JOIN `tb_jobs` jobs ON fleets.`fleet_id`= jobs.`fleet_id` " +
                                "WHERE fleets.`fleet_id`=? and jobs.`job_status`<>?";
                            connection.query(sql, [fleet_id, constants.jobStatus.DELETED], function (err, result_jobs) {
                                if (err) {
                                    console.log(err)
                                    responses.sendError(res);
                                } else {
                                    var result_jobs_length = result_jobs.length;
                                    for (var i = 0; i < result_jobs_length; i++) {
                                        if (result_jobs[i].job_id) {
                                            if (result_jobs[i].job_type == constants.jobType.PICKUP) {
                                                result_jobs[i].customer_username = '-';
                                                result_jobs[i].customer_email = '-';
                                                result_jobs[i].customer_phone = '-';
                                            } else {
                                                if (result_jobs[i].customer_username == "dummy") result_jobs[i].customer_username = '';
                                                if (result_jobs[i].customer_email == "dummy") result_jobs[i].customer_email = '';
                                                if (result_jobs[i].customer_phone == "+910000000000") result_jobs[i].customer_phone = '';
                                            }

                                            if (result_jobs[i].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                result_jobs[i].job_pickup_datetime = moment(result_jobs[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a')
                                            }
                                            if (result_jobs[i].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                result_jobs[i].job_delivery_datetime = moment(result_jobs[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a')
                                            }
                                        } else {
                                            delete result_jobs[i].job_pickup_name;
                                            delete result_jobs[i].job_pickup_phone;
                                            delete result_jobs[i].job_latitude;
                                            delete result_jobs[i].job_longitude;
                                            delete result_jobs[i].job_address;
                                            delete result_jobs[i].job_status;
                                            delete result_jobs[i].job_description;
                                            delete result_jobs[i].has_pickup;
                                            delete result_jobs[i].job_pickup_datetime;
                                            delete result_jobs[i].job_id;
                                            delete result_jobs[i].job_delivery_datetime;
                                            delete result_jobs[i].job_type;
                                            delete result_jobs[i].job_pickup_latitude;
                                            delete result_jobs[i].job_pickup_longitude;
                                            delete result_jobs[i].job_pickup_address;
                                            delete result_jobs[i].pickup_delivery_relationship;
                                        }
                                    }
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": result_jobs
                                    };
                                    res.send(JSON.stringify(response));
                                }
                            });
                        } else {
                            responses.invalidAccessError(res);
                        }
                    })
                }
            }
        });
    }
};


/*
 * ----------------------------------------------------
 * Check whether update popup has to be shown or not
 * INPUT : userId, version
 * OUTPUT : popup shown for update
 * ----------------------------------------------------
 */
function checkAppVersion(fleetId, userAppVersion, deviceType, callback) {
    var getVersionInfo =
        'SELECT `id`, `current_ios_version`, `current_android_version`, `last_force_android_version`, `last_force_ios_version`, `current_ios_is_force`, `current_android_is_force`, `current_ios_app_url`, `current_android_app_url` ' +
        'FROM `tb_version` LIMIT 1';
    connection.query(getVersionInfo, function (err, result) {
        if (err) {
            logging.logDatabaseQueryError("Error in viewing checkAppVersion info : ", err, result);
            responses.sendError(res);
        } else {
            var updateAppVersion =
                'UPDATE `tb_fleets` ' +
                'SET `app_versioncode` = ? ' +
                'WHERE `fleet_id`=? LIMIT 1';
            connection.query(updateAppVersion, [userAppVersion, fleetId], function (err, update) {
            });

            if (result.length > 0) {
                var popup = {
                    title: 'Update Version',
                    text: 'Update app with new version!'
                };
                if (deviceType == 0) { // android
                    if (userAppVersion < result[0].current_android_version) {
                        popup.cur_version = result[0].current_android_version;
                        popup.app_url = result[0].current_android_app_url;
                        if (result[0].current_android_is_force == 1) {
                            popup.is_force = 1;
                        }
                        else if (userAppVersion < result[0].last_force_android_version) {
                            popup.is_force = 1;
                        }
                        else {
                            popup.is_force = 0;
                        }
                        return callback(popup);
                    } else {
                        return callback({});
                    }
                }
                else {  // iOS
                    if (userAppVersion < result[0].current_ios_version) {
                        popup.cur_version = result[0].current_ios_version;
                        popup.app_url = result[0].current_ios_app_url;
                        if (result[0].current_ios_is_force == 1) {
                            popup.is_force = 1;
                        }
                        else if (userAppVersion < result[0].last_force_ios_version) {
                            popup.is_force = 1;
                        } else {
                            popup.is_force = 0;
                        }
                        return callback(popup);
                    }
                    else {
                        return callback({});
                    }
                }
            }
        }
    });
}

/*
 * ----------------------------------------------------
 * Check whether update popup has to be shown or not
 * INPUT : userId, version2
 * OUTPUT : popup shown for update
 * ----------------------------------------------------
 */
function checkAppVersion2(fleetId, userAppVersion, deviceType, callback) {

    var getVersionInfo =
        'SELECT * FROM `tb_version2` WHERE `device_type`=? LIMIT 1';
    connection.query(getVersionInfo, [deviceType], function (err, result) {
        if (err) {
            logging.logDatabaseQueryError("Error in viewing checkAppVersion info : ", err, result);
        } else {
            var updateAppVersion =
                'UPDATE `tb_fleets` ' +
                'SET `app_versioncode` = ? ' +
                'WHERE `fleet_id`=? LIMIT 1';
            connection.query(updateAppVersion, [userAppVersion, fleetId], function (err, update) {
            });

            if (result && result.length > 0) {
                var popup = {
                    title: 'Update Version',
                    text: 'Update app with new version!'
                };
                var data = {
                    popup: popup,
                    user_id: result[0].user_id,
                    brand: result[0].brand_name
                }
                if (userAppVersion < result[0].app_version) {
                    popup.cur_version = result[0].app_version;
                    popup.app_url = result[0].app_url;
                    if (result[0].is_force == 1) {
                        popup.is_force = 1;
                    }
                    else if (userAppVersion < result[0].last_force_version) {
                        popup.is_force = 1;
                    }
                    else {
                        popup.is_force = 0;
                    }
                    data.popup = popup;
                    return callback(data);
                } else {
                    data.popup = {};
                    return callback(data);
                }
            } else {
                return callback({
                    popup: {},
                    user_id: 0,
                    brand: ''
                });
            }
        }
    });

}


/*
 * -----------------------------
 * UPDATE CUSTOM FIELD FROM TASK
 * -----------------------------
 */

exports.update_custom_fields = function (req, res) {

    var access_token = req.body.access_token;
    var custom_field_label = req.body.custom_field_label;
    var job_id = req.body.job_id;
    var data = req.body.data;
    var manvalues = [access_token, custom_field_label, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var user_id = result[0].user_id, fleet_id = result[0].fleet_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        if (req.files && req.files.custom_image) {
                            commonFunc.uploadImageToS3Bucket(req.files.custom_image, config.get('s3BucketCredentials.folder.taskImages'), function (custom_images) {
                                if (custom_images == 0) {
                                    responses.uploadError(res);
                                } else {
                                    data = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.taskImages') + '/' + custom_images;
                                    insert_OR_update_task_details(data, "insert");
                                }
                            });
                        } else {
                            insert_OR_update_task_details(data, "update");
                        }

                        function insert_OR_update_task_details(data, status) {
                            mongo.updateTaskCustomField(user_id, job_id, custom_field_label, data, status, function (updateTaskCustomField) {
                                if (updateTaskCustomField.status) {
                                    if (updateTaskCustomField.data) {
                                        updateTaskCustomField.data = commonFunc.processCustomField(updateTaskCustomField.data);
                                        var text = result[0].username + " has updated custom field " + custom_field_label + " to task " + job_id;
                                        commonFunc.setNotification(user_id, text, commonFunc.reverseTaskHistory('custom_field_updated'), job_id);
                                        commonFunc.setTaskHistory(fleet_id, constants.taskHistoryType.CUSTOM_FIELD, job_id, JSON.stringify(updateTaskCustomField.data));
                                        socketResponse.sendSocketResponse(job_id);
                                    }
                                    responses.actionCompleteResponse(res);
                                } else {
                                    responses.invalidAccessError(res);
                                }
                            })
                        }
                    }
                })
            }
        })
    }
}

/*
 * -----------------
 * SET FLEET TAGS
 * -----------------
 */

exports.update_fleet_tags = function (req, res) {
    var access_token = req.body.access_token;
    var user_id = req.body.user_id;
    var fleet_id = req.body.fleet_id;
    var tags = req.body.tags;
    var manvalues = [access_token, fleet_id, tags, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.authenticateFleetIdAndUserId(fleet_id, result[0].user_id, function (auth) {
                    if (auth) {
                        var dumpTag = [];
                        tags.split(',').forEach(function (tag) {
                            dumpTag.push(tag.trim())
                        });
                        tags = dumpTag.join(',');
                        var sql = "UPDATE `tb_fleets` SET `tags`=? WHERE `fleet_id`=? LIMIT 1";
                        connection.query(sql, [tags, fleet_id], function (err, tags) {
                            if (err) {
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                                return;
                            }
                        });
                    } else {
                        responses.invalidAccessError(res);
                        return;
                    }
                });
            }
        });
    }
}

/////////////////////////////////////////////////// API FOR NEXT VERSION FOR APP ///////////////////////////////////////

/*
 * ----------------------
 * VIEW ALL TASKS OF FLEET
 * ----------------------
 */

exports.view_tasks = function (req, res) {

    var access_token = req.body.access_token;
    var month = req.body.month;
    var year = req.body.year;
    var manvalues = [access_token, month, year];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);

                    } else {
                        var firstDay = new Date(year, parseInt(month) - 1, 1);
                        var lastDay = new Date(year, parseInt(month), 0);

                        var fleet_id = result[0].fleet_id;
                        var sql = "SELECT jobs.`timezone`,DATE(jobs.`job_time`) as `job_time`,jobs.`job_id`,jobs.`job_status` " +
                            "FROM `tb_jobs` jobs " +
                            "WHERE jobs.`fleet_id`=? AND  DATE(jobs.`job_time`) >= ? AND  DATE(jobs.`job_time`) <= ?  AND jobs.`job_status` NOT IN (" + constants.jobStatus.UNASSIGNED + "," + constants.jobStatus.DELETED + ")";
                        connection.query(sql, [fleet_id, firstDay, lastDay], function (err, result_jobs) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in viewing fleet info : ", err, result_jobs);
                                responses.sendError(res);
                            } else {
                                var incomplete = 0, total_tasks = 0, jobsData = [];
                                var result_jobs_length = result_jobs.length;
                                for (var dynamicIncrementedDate = new Date(firstDay); dynamicIncrementedDate <= lastDay; dynamicIncrementedDate.setDate(dynamicIncrementedDate.getDate() + 1)) {
                                    var newIncrementedDate = new Date(dynamicIncrementedDate);
                                    total_tasks = 0, incomplete = 0;
                                    for (var i = 0; i < result_jobs_length; i++) {
                                        if (new Date(result_jobs[i].job_time).getTime() == newIncrementedDate.getTime()) {
                                            var date = new Date();
                                            var localCurrentDate = commonFunc.convertTimeIntoLocal(date, result_jobs[i].timezone);
                                            if ((new Date(result_jobs[i].job_time) < localCurrentDate) && ((result_jobs[i].job_status) != constants.jobStatus.ENDED)
                                                && ((result_jobs[i].job_status) != constants.jobStatus.FAILED) && ((result_jobs[i].job_status) != constants.jobStatus.CANCEL)) {
                                                incomplete++;
                                            }
                                            total_tasks++;
                                        }
                                    }
                                    var newIncrementedDateOnly = moment(newIncrementedDate).format("YYYY-MM-DD");
                                    jobsData.push({
                                        'date': newIncrementedDateOnly,
                                        'incomplete_tasks': incomplete,
                                        'total_tasks': total_tasks
                                    })
                                }
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data": jobsData
                                };
                                res.send(JSON.stringify(response));
                            }
                        });
                    }
                });
            }
        });
    }
}


/*
 * ----------------------
 * VIEW ALL JOBS OF FLEET
 * ----------------------
 */

exports.view_tasks_for_date = function (req, res) {
    var access_token = req.body.access_token;
    var date = req.body.date;
    var manvalues = [access_token, date];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        var fleet_id = result[0].fleet_id, has_routing = result[0].has_routing;
                        async.parallel(
                            [
                                function (cb) {
                                    db.collection(config.get('mongoCollections.routedTasks')).find({
                                        fleet_id: fleet_id.toString(),
                                        date: date
                                    }).toArray(function (err, values) {
                                        if (err) {
                                            cb(err, []);
                                        } else {
                                            var route = [], polylines = [];
                                            var resp = {
                                                route: route,
                                                polylines: polylines
                                            }
                                            if (values.length > 0) {
                                                values.forEach(function (val) {
                                                    route = route.concat(val.routed);
                                                    polylines = polylines.concat(val.polylines);
                                                });
                                                route = _lodash.uniq(route);
                                                polylines = _lodash.uniq(polylines);
                                                resp = {
                                                    route: route,
                                                    polylines: polylines
                                                }
                                            }
                                            cb(null, resp);
                                        }
                                    });
                                },
                                function (cb) {
                                    var sql = "SELECT jobs.`job_time`,IF( jobs.`job_pickup_name` IS NULL,'',jobs.`job_pickup_name`) as `job_pickup_name`,IF( jobs.`job_pickup_phone` IS NULL,'',jobs.`job_pickup_phone`) as `job_pickup_phone`,IF( jobs.`job_pickup_latitude` IS NULL,'',jobs.`job_pickup_latitude`) as `job_pickup_latitude`," +
                                        "IF( jobs.`job_pickup_longitude` IS NULL,'',jobs.`job_pickup_longitude`) as `job_pickup_longitude`,IF( jobs.`job_pickup_address` IS NULL,'',jobs.`job_pickup_address`) as `job_pickup_address`,jobs.`has_pickup`,jobs.`pickup_delivery_relationship`,IF( jobs.`job_pickup_email` IS NULL,'',jobs.`job_pickup_email`) as `job_pickup_email`," +
                                        "jobs.`job_type`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_description`,jobs.`job_status`,jobs.`job_delivery_datetime`," +
                                        "jobs.`job_time`,`job_pickup_datetime`,jobs.`job_id`,jobs.`user_id`,jobs.`is_routed`, " +
                                        "IF(jobs.`customer_username` IS NULL, '', jobs.`customer_username`) as `customer_username`,IF(jobs.`customer_phone` IS NULL, '', jobs.`customer_phone`) as `customer_phone`,IF(jobs.`customer_email` IS NULL, '', jobs.`customer_email`) as `customer_email` " +
                                        "FROM `tb_jobs` jobs " +
                                        "WHERE jobs.`fleet_id`=? AND  DATE(jobs.`job_time`)=? AND jobs.`job_status` NOT IN (" + constants.jobStatus.UNASSIGNED + "," + constants.jobStatus.DELETED + ") ";
                                    connection.query(sql, [fleet_id, date], function (err, result_jobs) {
                                        if (err) {
                                            console.log(err);
                                            logging.logDatabaseQueryError("Error in viewing task info : ", err, result_jobs);
                                            cb(err, result_jobs)
                                        } else {
                                            cb(null, result_jobs)
                                        }
                                    });
                                }],
                            function (err, result) {
                                if (err) {
                                    responses.sendError(res);
                                } else {
                                    var final_result = [], un_routed = [], task_length = result[1].length, routed_tasks = [];
                                    for (var i = 0; i < task_length; i++) {
                                        if (result[1][i].is_routed == 1) {
                                            var index = _lodash.findIndex(result[0].route, function (ind) {
                                                return ind.toString() == result[1][i].job_id.toString()
                                            })
                                            if (index >= 0) {
                                                final_result[index] = result[1][i];
                                            } else {
                                                un_routed.push(result[1][i]);
                                            }
                                        } else {
                                            un_routed.push(result[1][i]);
                                        }
                                    }
                                    final_result = final_result.filter(function (e) {
                                        return (e === undefined || e === null || e === '') ? false : ~e;
                                    });
                                    result[0].polylines = result[0].polylines.filter(function (e) {
                                        return (e === undefined || e === null || e === '') ? false : ~e;
                                    });
                                    final_result.forEach(function (task) {
                                        routed_tasks.push(task.job_id);
                                    })
                                    final_result = final_result.concat(un_routed);
                                    viewTaskDescription({
                                        has_routing: has_routing,
                                        data: final_result,
                                        routed_tasks_order: routed_tasks,
                                        polylines: result[0].polylines
                                    }, res);
                                }
                            })
                    }
                })

            }
        })
    }
}
/*
 * -----------------
 * VIEW TASK BY ID
 * -----------------
 */

exports.view_task_via_id = function (req, res) {
    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var manvalues = [access_token, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        var fleet_id = result[0].fleet_id, has_routing = result[0].has_routing;
                        var sql = "SELECT jobs.`job_time`,IF( jobs.`job_pickup_name` IS NULL,'',jobs.`job_pickup_name`) as `job_pickup_name`,IF( jobs.`job_pickup_phone` IS NULL,'',jobs.`job_pickup_phone`) as `job_pickup_phone`,IF( jobs.`job_pickup_latitude` IS NULL,'',jobs.`job_pickup_latitude`) as `job_pickup_latitude`," +
                            "IF( jobs.`job_pickup_longitude` IS NULL,'',jobs.`job_pickup_longitude`) as `job_pickup_longitude`,IF( jobs.`job_pickup_address` IS NULL,'',jobs.`job_pickup_address`) as `job_pickup_address`,jobs.`has_pickup`,jobs.`pickup_delivery_relationship`,IF( jobs.`job_pickup_email` IS NULL,'',jobs.`job_pickup_email`) as `job_pickup_email`," +
                            "jobs.`job_type`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_description`,jobs.`job_status`,jobs.`job_delivery_datetime`," +
                            "jobs.`job_time`,`job_pickup_datetime`,jobs.`job_id`,jobs.`user_id`, " +
                            "IF(jobs.`customer_username` IS NULL, '', jobs.`customer_username`) as `customer_username`,IF(jobs.`customer_phone` IS NULL, '', jobs.`customer_phone`) as `customer_phone`,IF(jobs.`customer_email` IS NULL, '', jobs.`customer_email`) as `customer_email` " +
                            "FROM `tb_jobs` jobs " +
                            "WHERE jobs.`fleet_id`=? AND jobs.`job_id`=? ";
                        connection.query(sql, [fleet_id, job_id], function (err, result_jobs) {
                            if (err) {
                                console.log(err);
                                logging.logDatabaseQueryError("Error in viewing task info : ", err, result_jobs);
                                responses.sendError(res);
                            } else {
                                viewTaskDescription({
                                    has_routing: has_routing,
                                    data: result_jobs,
                                    polylines: [],
                                    routed_tasks_order: []
                                }, res);
                            }
                        });
                    }
                });
            }
        });
    }
};

function viewTaskDescription(data, res) {
    var result_jobs_length = data.data.length, index = 0, finalResult = [];
    if (result_jobs_length > 0) {
        fetchTaskDetail(index);
        function changeIndex() {
            index++;
            return fetchTaskDetail(index);
        }

        function fetchTaskDetail(item) {
            if (item > result_jobs_length - 1) {
                if (!data.has_routing) {
                    finalResult = _lodash.sortByOrder(finalResult, ['job_time', 'job_id'], ['asc', 'asc']);
                }
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": finalResult,
                    "polylines": data.polylines,
                    "routed_tasks_order": data.routed_tasks_order
                };
                res.send(JSON.stringify(response));
                return;
            } else {
                commonFunc.delay2(item, function (i) {
                    (function (i) {
                        if (data.data[i].customer_username == "dummy") data.data[i].customer_username = '';
                        if (data.data[i].customer_email == "dummy") data.data[i].customer_email = '';
                        if (data.data[i].customer_phone == "+910000000000") data.data[i].customer_phone = '';
                        if (!data.data[i].description) data.data[i].description = '';

                        if (data.data[i].job_pickup_datetime && data.data[i].job_pickup_datetime != null && data.data[i].job_pickup_datetime != '0000-00-00 00:00:00') {
                            data.data[i].job_pickup_datetime = data.data[i].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                        }
                        if (data.data[i].job_delivery_datetime && data.data[i].job_delivery_datetime != null && data.data[i].job_delivery_datetime != '0000-00-00 00:00:00') {
                            data.data[i].job_delivery_datetime = data.data[i].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                        }
                        mongo.getOptionalFieldsForTask(data.data[i].user_id, data.data[i].job_id, data.data[i], function (fData) {
                            fData.task.fields = fData.fields == "" ? {} : fData.fields;
                            fData.task.fields.custom_field.forEach(function (cf) {
                                if (cf.fleet_data && typeof cf.fleet_data == 'object') {
                                    cf.fleet_data = ''
                                }
                                fData.task.c_f_template_id = cf.template_id;
                            })
                            if (fData.task.job_type == constants.jobType.PICKUP) {
                                commonFunc.authenticateUserIdAndJobToken(fData.task.user_id, fData.task.pickup_delivery_relationship, function (uData) {
                                    if (uData.length > 1) {
                                        if (fData.task.fields.app_optional_fields && fData.task.fields.app_optional_fields.length > 0) {
                                            var defaultvalue = constants.defaultOptionalValue.DEFAULT;
                                            defaultvalue[0] = fData.task.fields.app_optional_fields[0];
                                            defaultvalue[1].value = fData.task.fields.app_optional_fields[1].value == 1 ? 1 : 0;
                                            defaultvalue[2].value = fData.task.fields.app_optional_fields[2].value == 1 ? 1 : 0;
                                            defaultvalue[3].value = fData.task.fields.app_optional_fields[3].value == 1 ? 1 : 0;
                                            defaultvalue[1].required = 0;
                                            defaultvalue[2].required = 0;
                                            defaultvalue[3].required = 0;
                                            defaultvalue[4].value = fData.task.fields.app_optional_fields[4].value == 1 ? 1 : 0;
                                            defaultvalue[5].value = fData.task.fields.app_optional_fields[5].value == 1 ? 1 : 0;
                                            defaultvalue[6].value = fData.task.fields.app_optional_fields[6].value == 1 ? 1 : 0;
                                            fData.task.fields.app_optional_fields = defaultvalue;
                                            fData.task.delivery_job_id = uData[1].job_id;
                                        }
                                    } else {
                                        fData.task.delivery_job_id = 0;
                                    }
                                    getTaskHistory(fData.task);
                                })
                            } else {
                                getTaskHistory(fData.task);
                            }

                            function getTaskHistory(task) {
                                commonFunc.getTaskHistory(task.job_id, task, function (t_h) {
                                    t_h.task.task_history = t_h.task_history;
                                    finalResult.push(t_h.task);
                                    changeIndex();
                                });
                            }
                        });
                    })(i);
                });
            }
        }
    } else {
        responses.noDataFoundError(res);
    }
}

////////////////////////////////////////////// NOT USED APIS //////////////////////////////////////////////////////////


/*
 * -----------------------------
 * UPLOAD TASK IMAGE
 * -----------------------------
 */

exports.upload_task_image = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var job_image = req.files.job_image;
    var acknowledge_type = req.body.acknowledge_type;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var manvalues = [access_token, job_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationError(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        commonFunc.authenticateFleetIdAndJobId(result[0].fleet_id, job_id, function (authenticateFleetIdAndJobIdResult) {
                            if (authenticateFleetIdAndJobIdResult == 0) {
                                responses.authenticationErrorFleetAndJobID(res);
                            } else {
                                commonFunc.uploadImageToS3Bucket(req.files.job_image, config.get('s3BucketCredentials.folder.taskImages'), function (result_job_image) {
                                    if (result_job_image == 0) {
                                        responses.uploadError(res);
                                    } else {
                                        var jobImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.taskImages') + '/' + result_job_image;
                                        var sql = "INSERT INTO `tb_job_images` (`job_id`,`image`,`latitude`,`longitude`,`acknowledge_type`) VALUES (?,?,?,?,?)";
                                        connection.query(sql, [job_id, jobImage, latitude, longitude, acknowledge_type], function (err, result_insert) {
                                            if (err) {
                                                responses.sendError(res);
                                            } else {
                                                commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.ACK_IMAGE_ADDED, job_id, jobImage);
                                                var response = {
                                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                                    "data": {
                                                        jobImage: jobImage,
                                                        job_image_id: result_insert.insertId
                                                    }
                                                };
                                                res.send(JSON.stringify(response));

                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};


/*
 * -----------------------------
 * DELETE TASK IMAGE
 * -----------------------------
 */

exports.delete_task_image = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var job_image_id = req.body.job_image_id;

    var manvalues = [access_token, job_id, job_image_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationError(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        commonFunc.authenticateJobImageANDFleetID(job_image_id, result[0].fleet_id, function (authenticateJobImageANDFleetIDResult) {
                            if (authenticateJobImageANDFleetIDResult == 0) {
                                responses.authenticationErrorFleetAndJobID(res);

                            } else {
                                var sql = "DELETE FROM `tb_job_images` WHERE `jobs_image_id`=? AND `job_id`=? LIMIT 1";
                                connection.query(sql, [job_image_id, job_id], function (err, delete_tb_job_images) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in deleting job_images information  : ", err, delete_tb_job_images);
                                        responses.sendError(res);

                                    } else {
                                        commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.ACK_IMAGE_DELETED, job_id, authenticateJobImageANDFleetIDResult[0].image);
                                        responses.actionCompleteResponse(res);

                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};


/*
 * -------------------------------------------
 * UPLOAD TASK DETAILS IN CASE OF APPOINTMENT
 * -------------------------------------------
 */

exports.upload_task_details = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var job_image = req.files.job_image;
    var notes = req.body.notes;
    var has_image = req.body.has_image;
    var datetime = req.body.datetime;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;

    var manvalues = [access_token, job_id, has_image, datetime, latitude, longitude];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationError(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        commonFunc.authenticateFleetIdAndJobId(result[0].fleet_id, job_id, function (authenticateFleetIdAndJobIdResult) {
                            if (authenticateFleetIdAndJobIdResult == 0) {
                                responses.authenticationErrorFleetAndJobID(res);
                            } else {
                                if (has_image == 1 && req.files && req.files.job_image) {
                                    commonFunc.uploadImageToS3Bucket(req.files.job_image, config.get('s3BucketCredentials.folder.taskImages'), function (result_job_image) {
                                        if (result_job_image == 0) {
                                            responses.uploadError(res);
                                        } else {
                                            var jobImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.taskImages') + '/' + result_job_image;
                                            var sql = "INSERT INTO `tb_appointment_field_data` (`job_id`,`image`,`notes`,`datetime`,`latitude`,`longitude`) VALUES (?,?,?,?,?,?)";
                                            connection.query(sql, [job_id, jobImage, notes, datetime, latitude, longitude], function (err, result_task_information_insert) {
                                                if (err) {
                                                    logging.logDatabaseQueryError("Error in inserting tb_appointment_field_data info : ", err, result_task_information_insert);
                                                    responses.sendError(res);

                                                } else {
                                                    if (notes) {
                                                        commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.FOS_IMAGE_AND_TEXT_ADDED, job_id, notes + "," + jobImage);
                                                    } else {
                                                        commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.FOS_IMAGE_ADDED, job_id, jobImage);
                                                    }
                                                    var response = {
                                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                                        "data": {
                                                            jobImage: jobImage,
                                                            "appointment_field_data_id": result_task_information_insert.insertId
                                                        }
                                                    };
                                                    res.send(JSON.stringify(response));

                                                }
                                            });
                                        }
                                    });
                                } else {
                                    if (notes) {
                                        commonFunc.setTaskHistory(result[0].fleet_id, constants.taskHistoryType.FOS_TEXT_ADDED, job_id, notes);
                                    }
                                    var sql = "INSERT INTO `tb_appointment_field_data` (`job_id`,`notes`,`datetime`,`latitude`,`longitude`) VALUES (?,?,?,?,?)";
                                    connection.query(sql, [job_id, notes, datetime, latitude, longitude], function (err, result_task_information_insert) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in inserting tb_appointment_field_data info : ", err, result_task_information_insert);
                                            responses.sendError(res);

                                        } else {
                                            var response = {
                                                "message": constants.responseMessages.ACTION_COMPLETE,
                                                "status": constants.responseFlags.ACTION_COMPLETE,
                                                "data": {
                                                    "appointment_field_data_id": result_task_information_insert.insertId
                                                }
                                            };
                                            res.send(JSON.stringify(response));

                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });
    }
};


/*
 * -------------------------------------------
 * EDIT TASK DETAILS IN CASE OF APPOINTMENT
 * -------------------------------------------
 */

exports.edit_task_details = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var job_image = req.files.job_image;
    var notes = req.body.notes;
    var has_image = req.body.has_image;
    var datetime = req.body.datetime;
    var latitude = req.body.latitude;
    var longitude = req.body.longitude;
    var appointment_field_data_id = req.body.appointment_field_data_id;

    var manvalues = [access_token, job_id, has_image, datetime, latitude, longitude, appointment_field_data_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);

    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationError(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        if (has_image == 1 && req.files && req.files.job_image) {
                            commonFunc.uploadImageToS3Bucket(req.files.job_image, config.get('s3BucketCredentials.folder.taskImages'), function (result_job_image) {
                                if (result_job_image == 0) {
                                    responses.uploadError(res);
                                } else {
                                    var jobImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.taskImages') + '/' + result_job_image;
                                    var sql = "UPDATE `tb_appointment_field_data` SET `job_id`=?,`image`=?,`notes`=?,`datetime`=?,`latitude`=?,`longitude`=? " +
                                        "WHERE `appointment_field_data_id` = ? LIMIT 1";
                                    connection.query(sql, [job_id, jobImage, notes, datetime, latitude, longitude, appointment_field_data_id], function (err, result_task_information_update) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in updating tb_appointment_field_data info : ", err, result_task_information_update);
                                            responses.sendError(res);

                                        } else {
                                            var response = {
                                                "message": constants.responseMessages.ACTION_COMPLETE,
                                                "status": constants.responseFlags.ACTION_COMPLETE,
                                                "data": {
                                                    jobImage: jobImage
                                                }
                                            };
                                            res.send(JSON.stringify(response));

                                        }
                                    });
                                }
                            });
                        } else {
                            var sql = "UPDATE `tb_appointment_field_data` SET `job_id`=?,`notes`=?,`datetime`=?,`latitude`=?,`longitude`=? " +
                                "WHERE `appointment_field_data_id` = ? LIMIT 1";
                            connection.query(sql, [job_id, notes, datetime, latitude, longitude, appointment_field_data_id], function (err, result_task_information_update) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in inserting tb_appointment_field_data info : ", err, result_task_information_update);
                                    responses.sendError(res);

                                } else {
                                    responses.actionCompleteResponse(res);

                                }
                            });
                        }
                    }
                });
            }
        });
    }
};

/*
 * -------------------------------
 * DELETE TASK DETAILS
 * -------------------------------
 */

exports.delete_task_details = function (req, res) {

    var access_token = req.body.access_token;
    var appointment_field_data_id = req.body.appointment_field_data_id;

    var manvalues = [access_token, appointment_field_data_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);

    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);

            } else {
                var user_id = result[0].user_id;
                var fleet_id = result[0].fleet_id;
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);

                    } else {
                        commonFunc.authenticateAppointmentFieldIdANDFleetID(appointment_field_data_id, fleet_id, function (authenticateAppointmentFieldIdANDFleetIDResult) {
                            if (authenticateAppointmentFieldIdANDFleetIDResult == 0) {
                                responses.invalidAccessError(res);
                            } else {
                                var sql = "DELETE FROM `tb_appointment_field_data` WHERE `appointment_field_data_id`=? LIMIT 1";
                                connection.query(sql, [appointment_field_data_id], function (err, appointment_field_result) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in deleting tb_appointment_field_data information  : ", err, appointment_field_result);
                                        responses.sendError(res);
                                    } else {
                                        if (authenticateAppointmentFieldIdANDFleetIDResult[0].image == null || authenticateAppointmentFieldIdANDFleetIDResult == "") {
                                            commonFunc.setTaskHistory(fleet_id, constants.taskHistoryType.FOS_TEXT_DELETED, authenticateAppointmentFieldIdANDFleetIDResult[0].job_id, authenticateAppointmentFieldIdANDFleetIDResult[0].notes);
                                        } else {
                                            commonFunc.setTaskHistory(fleet_id, constants.taskHistoryType.FOS_IMAGE_DELETED, authenticateAppointmentFieldIdANDFleetIDResult[0].job_id, authenticateAppointmentFieldIdANDFleetIDResult[0].image);
                                        }
                                        responses.actionCompleteResponse(res);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};


/*
 * ----------------------
 * VIEW ALL JOBS OF FLEET
 * ----------------------
 */

exports.view_jobs = function (req, res) {
    var access_token = req.body.access_token;
    var current_local_date = req.body.current_local_date;
    var manvalues = [access_token, current_local_date];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);

    } else {
        commonFunc.authenticateFleetAcessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);

                    } else {
                        var fleet_id = result[0].fleet_id;
                        var local_date = current_local_date.split(' ');
                        current_local_date = local_date[0] + " 00:00:00";
                        async.parallel([
                                function (callback) {
                                    var sql = "SELECT jobs.`job_time`,IF( jobs.`job_pickup_name` IS NULL,'',jobs.`job_pickup_name`) as `job_pickup_name`,IF( jobs.`job_pickup_phone` IS NULL,'',jobs.`job_pickup_phone`) as `job_pickup_phone`,IF( jobs.`job_pickup_latitude` IS NULL,'',jobs.`job_pickup_latitude`) as `job_pickup_latitude`," +
                                        "IF( jobs.`job_pickup_longitude` IS NULL,'',jobs.`job_pickup_longitude`) as `job_pickup_longitude`,IF( jobs.`job_pickup_address` IS NULL,'',jobs.`job_pickup_address`) as `job_pickup_address`,jobs.`has_pickup`,jobs.`pickup_delivery_relationship`,IF( jobs.`job_pickup_email` IS NULL,'',jobs.`job_pickup_email`) as `job_pickup_email`," +
                                        "jobs.`job_type`,IF( jobs.`job_latitude` IS NULL,'',jobs.`job_latitude`) as `job_latitude`,IF( jobs.`job_longitude` IS NULL,'',jobs.`job_longitude`) as `job_longitude`,jobs.`job_address`,jobs.`job_description`,jobs.`job_status`,jobs.`job_delivery_datetime`," +
                                        "DATE(jobs.`job_time`) as `job_time`,`job_pickup_datetime`,jobs.`job_id`," +
                                        "jobs.`customer_username`,jobs.`customer_phone`,IF(jobs.`customer_email` IS NULL, '', jobs.`customer_email`) as `customer_email` " +
                                        "FROM `tb_jobs` jobs " +
                                        "WHERE jobs.`fleet_id`=? AND  jobs.`job_time` >= ? ORDER BY jobs.`job_time`";
                                    connection.query(sql, [fleet_id, current_local_date], function (err, result_jobs) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in viewing fleet info : ", err, result_jobs);
                                            responses.sendError(res);
                                        } else {
                                            current_local_date = current_local_date.split(' ')[0];
                                            var upcoming = [], started = [], ended = [], jobsData = [];
                                            var result_jobs_length = result_jobs.length;
                                            var nextMonth = new Date(current_local_date);
                                            nextMonth.setMonth(nextMonth.getMonth() + 1)
                                            //Loop for 1 month data
                                            for (var dynamicIncrementedDate = new Date(current_local_date); dynamicIncrementedDate < nextMonth; dynamicIncrementedDate.setDate(dynamicIncrementedDate.getDate() + 1)) {
                                                var newIncrementedDate = new Date(dynamicIncrementedDate);
                                                upcoming = [], started = [], ended = [];
                                                for (var i = 0; i < result_jobs_length; i++) {
                                                    if (new Date(result_jobs[i].job_time).getTime() == newIncrementedDate.getTime()) {
                                                        if (result_jobs[i].customer_username == "dummy")result_jobs[i].customer_username = '';
                                                        if (result_jobs[i].customer_email == "dummy") result_jobs[i].customer_email = '';
                                                        if (result_jobs[i].customer_phone == "+910000000000") result_jobs[i].customer_phone = '';
                                                        if (result_jobs[i].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                            result_jobs[i].job_pickup_datetime = result_jobs[i].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                        }
                                                        if (result_jobs[i].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                            result_jobs[i].job_delivery_datetime = result_jobs[i].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                        }
                                                        upcoming.push(result_jobs[i]);
                                                    }
                                                }
                                                var newIncrementedDateOnly = moment(newIncrementedDate).format("YYYY-MM-DD");
                                                jobsData.push({
                                                    'date': newIncrementedDateOnly,
                                                    'upcoming_jobs': upcoming
                                                })
                                            }
                                        }
                                        callback(null, jobsData);
                                    });
                                },
                                function (callback) {
                                    var sql = "SELECT tb_ts.`session_id`," +
                                        "jobs.`job_time`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,IF( jobs.`job_pickup_address` IS NULL,'',jobs.`job_pickup_address`) as `job_pickup_address`,IF( jobs.`job_pickup_email` IS NULL,'',jobs.`job_pickup_email`) as `job_pickup_email`," +
                                        "jobs.`job_type`,IF( jobs.`job_latitude` IS NULL,'',jobs.`job_latitude`) as `job_latitude`,IF( jobs.`job_longitude` IS NULL,'',jobs.`job_longitude`) as `job_longitude`,jobs.`job_address`,jobs.`job_description`,jobs.`job_status`,jobs.`job_pickup_datetime`,jobs.`job_delivery_datetime`," +
                                        "jobs.`job_id`," +
                                        "jobs.`customer_username`,jobs.`customer_phone`,IF(jobs.`customer_email` IS NULL, '', jobs.`customer_email`) as `customer_email` " +
                                        "FROM `tb_task_session` tb_ts LEFT JOIN `tb_jobs` jobs ON jobs.job_id = tb_ts.job_id " +
                                        "WHERE tb_ts.`job_acknowledged`=? and tb_ts.`fleet_id`=? ";
                                    connection.query(sql, [constants.jobAcknowledged.NO, fleet_id], function (err, result_active_sessions) {
                                        if (err) {
                                            logging.logDatabaseQueryError("Error in finding active sessions : ", err, result_active_sessions);
                                            responses.sendError(res);
                                        } else {
                                            var finalResult = [], Result = [], result_active_sessions_length = result_active_sessions.length;
                                            if (result_active_sessions_length > 0) {
                                                var counter = 0;
                                                for (var i = 0; i < result_active_sessions_length; i++) {
                                                    if (result_active_sessions[i].customer_username == "dummy") result_active_sessions[i].customer_username = '';
                                                    if (result_active_sessions[i].customer_email == "dummy") result_active_sessions[i].customer_email = '';
                                                    if (result_active_sessions[i].customer_phone == "+910000000000") result_active_sessions[i].customer_phone = '';
                                                    if (result_active_sessions[i].job_pickup_datetime != '0000-00-00 00:00:00') {
                                                        result_active_sessions[i].job_pickup_datetime = result_active_sessions[i].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                    }
                                                    if (result_active_sessions[i].job_delivery_datetime != '0000-00-00 00:00:00') {
                                                        result_active_sessions[i].job_delivery_datetime = result_active_sessions[i].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                    }
                                                    commonFunc.checkJobIDWithProfile(result_active_sessions[i].job_id, result_active_sessions[i], function (checkJobIDWithProfileResult) {
                                                        checkJobIDWithProfileResult.task.job_details_by_fleet = checkJobIDWithProfileResult.job_images;
                                                        counter++;
                                                        sendResponse(counter, result_active_sessions_length, checkJobIDWithProfileResult.task);
                                                    });
                                                }
                                            }
                                            else {
                                                callback(null, result_active_sessions);
                                            }
                                            //function sendResponse(counter,result_active_sessions_length,result_active_sessions){
                                            //    finalResult.push(result_active_sessions);
                                            //    if (counter == result_active_sessions_length){
                                            //        callback(null, finalResult);
                                            //    }
                                            //}
                                            function sendResponse(counter, result_task_profile_length, result_task_profile) {
                                                Result.push(result_task_profile);
                                                if (counter == result_task_profile_length) {
                                                    var new_counter = 0;
                                                    for (var i = 0; i < counter; i++) {
                                                        commonFunc.checkAcknowlegedImages(Result[i].job_id, Result[i], function (checkAcknowlegedImagesResult) {
                                                            checkAcknowlegedImagesResult.task.acknowledged_images = checkAcknowlegedImagesResult.job_images;
                                                            new_counter++;
                                                            sendFinalResponse(new_counter, counter, checkAcknowlegedImagesResult.task);
                                                        });
                                                    }

                                                }
                                            }

                                            function sendFinalResponse(counter, result_task_profile_length, result_task_profile) {
                                                finalResult.push(result_task_profile);
                                                if (counter == result_task_profile_length) {
                                                    callback(null, finalResult);
                                                }
                                            }
                                        }
                                    });
                                }
                            ],
                            function (err, results) {
                                var response = {
                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                    "data": {
                                        "job_data": results[0],
                                        "active_session": results[1]
                                    }
                                };
                                res.send(JSON.stringify(response));

                            });
                    }
                });
            }
        });
    }
};