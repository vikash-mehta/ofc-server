var commonFunc = require('./commonfunction');
var cron = require('./cron');
var users = require('./users');
var settings = require('./settings');
var teams = require('./teams');
var fleets = require('./fleets');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var mongo = require('./mongo');
var moment = require('moment');
var async = require('async');
var commonFuncMailer = require('../templates/commonfunctionMailer');
/*
 * ----------------------------------
 * CREATION OF USERS ACCOUNT
 * ----------------------------------
 */
exports.register_user = function (req, res) {
    var email = req.body.email;
    var name = req.body.name;
    var password = req.body.password;
    var company_name = req.body.company_name
    var timezone = req.body.timezone;
    var manvalues = [name, email, password, timezone, company_name];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserEmail(email, function (result) {
            if (result != 0) {
                responses.authenticationAlreadyExists(res);
                return;
            } else {
                if (password.length < config.get('AppPasswordLength')) {
                    var response = {
                        "message": constants.responseMessages.APP_PASSWORD_ERROR,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    var verification_token = md5(email);
                    var msg = 'We are extremely delighted to welcome you to Tookan.<br><br>';
                    msg += "<b>Dashboard Link : </b>" + config.get('webAppLink') + "<br><br>";
                    msg += "Use the credentials below to sign-in: <br>";
                    msg += "<b>Username : </b>" + email + "<br>";
                    msg += "<b>Password : </b>" + password + "<br><br>";

                    msg += "<b>Getting started:</b><br>";
                    msg += "<b>Step 1:</b> Click on the URL given in the email.<br>";
                    msg += "<b>Step 2:</b> Use credentials given in the mail to login.<br>";
                    msg += "<b>Step 3:</b> Enter a new password or skip.<br>";
                    msg += "<b>Step 4:</b> Select workflow according to the nature of the business. (you can change this later in the settings)<br>";
                    msg += "<b>Step 5:</b> In the “Teams” tab, click on the “Create Team” button.<br>";
                    msg += "<b>Step 6:</b> In the “Fleet” tab, click on the “Add Fleet”. Enter the Fleet Name, Email and Phone. The driver will receive a link via SMS & email to download the App. Move to step 7 once the Driver signs-up.<br>";
                    msg += "<b>Step 7:</b> Click “New Task“ on the top panel and enter the task details.<br>";
                    msg += "<b>Step 8:</b> Assign the task to a driver using the drop down.<br><br>";

                    msg += "We look forward to serve you better. For any queries please feel free to reach us at contact@tookanapp.com";
                    commonFunc.emailPlainFormatting(name, msg, '', '', '', function (returnMessage) {
                        commonFunc.sendHtmlContent(email, returnMessage, "Welcome to Tookan!", "", function (result) {
                            var encrypted_pass = md5(password);
                            var access_token = md5(encrypted_pass + new Date());
                            var sql = "INSERT INTO `tb_users` (`username`,`company_name`,`access_token`,`email`,`password`,";
                            sql += "`verification_token`,`verification_status`,`expiry_datetime`,`last_login_datetime`,`timezone`)";
                            sql += " VALUES (?,?,?,?,?,?,?,?,?,?)";
                            connection.query(sql, [name, company_name, access_token, email, encrypted_pass, verification_token, constants.userVerificationStatus.VERIFY, commonFunc.addDays(config.get('expiryDays')), new Date(), timezone], function (err, result_insert) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in inserting user info : ", err, result_insert);
                                    logging.addErrorLog("Error in inserting new admin Info : ", err, result_insert);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    commonFunc.insertTemplateInformation(result_insert.insertId);
                                    cron.insertDefaultFleetNotification(result_insert.insertId);
                                    responses.actionCompleteResponse(res);
                                    return;
                                }
                            });
                        });
                    });
                }
            }
        });
    }
};
exports.register = function (req, res) {
    var step = parseInt(req.body.step);
    if (step == constants.setUpWizardStep.FIRST) {
        register_step1(req, res);
    } else if (step == constants.setUpWizardStep.SECOND) {
        register_step2(req, res);
    } else if (step == constants.setUpWizardStep.THIRD) {
        register_step3(req, res);
    } else {
        responses.authenticationError(res);
        return;
    }
};
function register_step1(req, res) {
    var email = req.body.email;
    var name = req.body.name;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var password = req.body.password;
    var company_name = req.body.company_name
    var company_address = req.body.company_address
    var phone = req.body.phone
    var timezone = req.body.timezone;
    var country_phone_code = req.body.country_phone_code;
    var source = req.body.source;
    var medium = req.body.medium;
    var internal_user = req.body.internal_user;
    var layout_type = req.body.layout_type;
    var manvalues = [first_name, name, email, password, timezone, company_name, phone, layout_type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserEmail(email, function (result) {
            if (result != 0) {
                responses.authenticationAlreadyExists(res);
                return;
            } else {
                if (password.length < config.get('AppPasswordLength')) {
                    var response = {
                        "message": constants.responseMessages.APP_PASSWORD_ERROR,
                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    var verification_token = md5(email), encrypted_pass = md5(password), access_token = md5(encrypted_pass + new Date());
                    var sql = "INSERT INTO `tb_users` (`internal_user`,`source`,`medium`,`first_name`,`last_name`,`country_phone_code`,`setup_wizard_step`,`phone`,`company_address`,`username`,`company_name`,`access_token`,`email`,`password`,";
                    sql += "`verification_token`,`verification_status`,`expiry_datetime`,`last_login_datetime`,`timezone`)";
                    sql += " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    connection.query(sql, [internal_user, source, medium, first_name, last_name, country_phone_code, constants.setUpWizardStep.FIRST, phone, company_address, name, company_name, access_token, email, encrypted_pass, verification_token,
                        constants.userVerificationStatus.VERIFY, commonFunc.addDays(config.get('expiryDays')), new Date(), timezone], function (err, result_insert) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in inserting user info : ", err, result_insert);
                            logging.addErrorLog("Error in inserting new admin Info : ", err, result_insert);
                            responses.sendError(res);
                            return;
                        } else {
                            var team_name = 'Team A';
                            settings.updateWorkflow(layout_type, result_insert.insertId, function (updateWorkflowResult) {
                                if (updateWorkflowResult.status == constants.responseFlags.ACTION_COMPLETE) {
                                    commonFunc.getUserDetails(result_insert.insertId, function (userData) {
                                        teams.createTeam(userData, team_name, 1, 0, 0, function (createTeamResult) {
                                            if (createTeamResult.status == constants.responseFlags.ACTION_COMPLETE) {
                                                commonFunc.insertTemplateInformation(userData[0].user_id);
                                                cron.insertDefaultFleetNotification(userData[0].user_id);
                                                commonFuncMailer.send_template_mail(access_token, name, email, 'tookan-welcome-email', 0);
                                                commonFunc.addToAgileCRM(req);
                                                commonFunc.addToSubscriberList(req);
                                                mongo.insertDefaultWorkflow(userData[0].user_id, layout_type);
                                                var response = {
                                                    "message": constants.responseMessages.ACTION_COMPLETE,
                                                    "status": constants.responseFlags.ACTION_COMPLETE,
                                                    "data": {
                                                        "access_token": access_token
                                                    }
                                                };
                                                res.send(JSON.stringify(response));
                                            } else {
                                                res.send(createTeamResult);
                                            }
                                        });
                                    });
                                } else {
                                    res.send(updateWorkflowResult);
                                }
                            });
                        }
                    });
                }
            }
        });
    }
}
function register_step2(req, res) {
    var access_token = req.body.access_token;
    var team_name = req.body.team_name;
    var layout_type = req.body.layout_type;
    var call_fleet_as = req.body.call_fleet_as;
    var fleet_ids = req.body.fleet_ids;
    var dispatcher_ids = req.body.dispatcher_ids;
    if (typeof fleet_ids === 'undefined') {
        fleet_ids = 0;
    }
    if (typeof dispatcher_ids === 'undefined') {
        dispatcher_ids = 0;
    }
    var manvalues = [access_token, team_name, layout_type, call_fleet_as];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                teams.createTeam(result, team_name, 1, fleet_ids, dispatcher_ids, function (createTeamResult) {
                    if ((createTeamResult.status == constants.responseFlags.ACTION_COMPLETE) || (createTeamResult.status == constants.responseFlags.ACCOUNT_EXPIRE)) {
                        settings.updateWorkflow(layout_type, user_id, function (updateWorkflowResult) {
                            if ((updateWorkflowResult.status == constants.responseFlags.ACTION_COMPLETE) || (createTeamResult.status == constants.responseFlags.ACCOUNT_EXPIRE)) {
                                updateUserSetUpWizardStep(constants.setUpWizardStep.SECOND, user_id, function (updateUserSetUpWizardStepRESULT) {
                                    if (updateUserSetUpWizardStepRESULT == 1) {
                                        var sql = "UPDATE `tb_users` SET `call_fleet_as`=? WHERE `user_id`=? LIMIT 1"
                                        connection.query(sql, [call_fleet_as, user_id], function (err, updateUserSetUpWizardStepRESULT) {
                                            res.send(JSON.stringify(createTeamResult));
                                        });
                                    } else {
                                        res.send(updateUserSetUpWizardStepRESULT);
                                    }
                                });
                            } else {
                                res.send(updateWorkflowResult);
                            }
                        });
                    } else {
                        res.send(createTeamResult);
                    }
                })
            }
        });
    }
}
function register_step3(req, res) {
    var access_token = req.body.access_token;
    var email = req.body.email;
    var name = req.body.name;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var login_id = req.body.login_id;
    var phone = req.body.phone;
    var password = req.body.password;
    var it_self = req.body.it_self;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                var decrypt_password = 1;
                if (it_self == 1) {
                    name = result[0].username;
                    email = result[0].email;
                    login_id = result[0].email;
                    phone = result[0].phone;
                    first_name = result[0].first_name;
                    last_name = result[0].last_name;
                    password = result[0].password;
                    decrypt_password = 0;
                }
                teams.getTeams(result[0].user_id, result[0].is_dispatcher, result[0].dispatcher_user_id, function (getTeamsResult) {
                    var team_id = 0;
                    if (getTeamsResult.data.length > 0) {
                        team_id = getTeamsResult.data[0].team_id;
                    }
                    commonFunc.authenticateFleetEmailWithUser(email, function (fleetEmailWithUserResult) {
                        if (fleetEmailWithUserResult != 0) {
                            var response = {
                                "message": constants.responseMessages.FLEET_EMAIL_ALREADY_EXISTS,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                        } else {
                            commonFunc.authenticateUniqueFleetLoginID(login_id, function (auth) {
                                if (auth != 0) {
                                    var response = {
                                        "message": constants.responseMessages.LOGIN_ID_ALREADY_EXISTS,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {}
                                    };
                                    res.send(JSON.stringify(response));
                                } else {
                                    result[0].android_link = config.get('androidAppDownloadLink');
                                    result[0].ios_link = config.get('iOSAppDownloadLink');
                                    result[0].brand_name = config.get('projectName');
                                    fleets.addFleet(result, null, name, email, result[0].timezone, phone, null, null, null, null, team_id, password, decrypt_password, login_id, first_name, last_name, function (addFleetResult) {
                                        if (addFleetResult == 1) {
                                            updateUserSetUpWizardStep(constants.setUpWizardStep.THIRD, result[0].user_id, function (updateUserSetUpWizardStepRESULT) {
                                                if (updateUserSetUpWizardStepRESULT == 1) {
                                                    var response = {
                                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                                        "data": {
                                                            name: name,
                                                            email: email,
                                                            phone: phone
                                                        }
                                                    };
                                                    res.send(JSON.stringify(response));
                                                    return;
                                                } else {
                                                    responses.authenticationError(res);
                                                }
                                            });
                                        } else {
                                            responses.authenticationError(res);
                                        }
                                    });
                                }
                            });
                        }
                    });
                });
            }
        });
    }
}
function updateUserSetUpWizardStep(step, user_id, callback) {
    var sql = "UPDATE `tb_users` SET `setup_wizard_step`=? WHERE `user_id`=? LIMIT 1"
    connection.query(sql, [step, user_id], function (err, updateUserSetUpWizardStepRESULT) {
        if (err) {
            logging.logDatabaseQueryError("Error in updateUserSetUpWizardStepRESULT user info : ", err, updateUserSetUpWizardStepRESULT);
            logging.addErrorLog("Error in updateUserSetUpWizardStepRESULT admin Info : ", err, updateUserSetUpWizardStepRESULT);
            callback(0);
        } else {
            callback(1);
        }
    });
}
/*
 * -----------------------------------------------
 * USER LOGIN VIA EMAIL AND PASSWORD
 * -----------------------------------------------
 */
exports.user_login = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;
    var timezone = req.body.timezone;
    var manvalues = [email, password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var encrypted_pass = md5(password);
        var sql = "SELECT `domain`,`is_whitelabel`,`brand_image`,`fav_icon`,`distance_in`,`has_routing`,`has_invoicing_module`,`has_completed_tasks`,`has_traffic_layer`,`info_popup`,`map_theme`,`setup_wizard_step`," +
            "`country_phone_code`,`show_billing_popup`,`call_fleet_as`,`layout_type`,`is_first_time_login`,`is_dispatcher`,`notification_count`,`is_active`,`user_id`,`company_latitude`," +
            "`company_longitude`,`billing_plan`,`username`,`access_token`,`email`,`password`,`phone`,`expiry_datetime`,`first_time_login_keys`,`tab_viewed_keys`,`company_name`,`company_address`," +
            "`country`,`company_image`,`is_company_image_view`,`is_driver_image_view`,`constraint_type`,`company_latitude`,`company_longitude`,`first_name`,`last_name`,`creation_datetime` " +
            "FROM `tb_users` " +
            "WHERE `email`=? LIMIT 1";
        connection.query(sql, [email], function (err, user) {
            if (err) {
                logging.logDatabaseQueryError("Error in fetching admin Info : ", err, user);
                responses.sendError(res);
                return;
            } else {
                var response;
                if (user.length == 0) {
                    response = {
                        "message": constants.responseMessages.EMAIL_NOT_EXISTS,
                        "status": constants.responseFlags.INVALID_EMAIL_ID,
                        "data": {}
                    };
                    res.send(JSON.stringify(response));
                    return;
                } else {
                    if (user[0].password != encrypted_pass) {
                        response = {
                            "message": constants.responseMessages.WRONG_PASSWORD,
                            "status": constants.responseFlags.WRONG_PASSWORD,
                            "data": {}
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user[0].verification_status == constants.userVerificationStatus.NOT_VERIFY) {
                        response = {
                            "message": constants.responseMessages.ACCOUNT_NOT_VERIFIED,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {}
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else {
                        var sql = "UPDATE `tb_users` " +
                            "SET `last_login_datetime`=NOW() ";
                        if (timezone) {
                            sql += ",`timezone` = " + timezone + " ";
                        }
                        sql += " WHERE `email`=? LIMIT 1";
                        connection.query(sql, [email], function (err, result_update) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating user info : ", err, result_update);
                                responses.sendError(res);
                                return;
                            } else {
                                checkForCompanyPoints(user, res);
                            }
                        });
                    }
                }
            }
        });
    }
};
function checkForCompanyPoints(user, res) {
    if (user[0].company_latitude) {
        module.exports.user_login_check(user, res);
    } else {
        commonFunc.getLatLngFromAddress(user[0].company_address, function (points) {
            if (points.length != 0) {
                update_company_points(points[0].latitude, points[0].longitude, user[0].user_id);
            }
            module.exports.user_login_check(user, res);
        });
    }
}
function update_company_points(latitude, longitude, user_id) {
    var sql = "UPDATE `tb_users` " +
        "SET `company_latitude`= ?,`company_longitude`=? " +
        "WHERE `user_id`=? LIMIT 1";
    connection.query(sql, [latitude, longitude, user_id], function (err, result) {});
}

// user login check for admin and dispatcher
exports.user_login_check = function (user, res) {

    async.waterfall([

        function (cb) {

            commonFunc.checkAccountExpiry(user[0].user_id, function (chk) {
                if (chk) {
                    user[0].billing_popup = {
                        "skip_link": 0,
                        "show_billing_popup": 1,
                        "days_left": 0
                    }
                } else {
                    var days_left = parseInt(commonFunc.timeDifferenceInDays(new Date(), user[0].expiry_datetime));
                    if (user[0].billing_plan != constants.billingPlan.TRIAL) {
                        user[0].expiry_datetime = "Unlimited";
                        days_left = "Unlimited";
                    }
                    user[0].billing_popup = {
                        "skip_link": 1,
                        "show_billing_popup": user[0].show_billing_popup,
                        "days_left": days_left
                    };
                }
                cb(null, user);
            })

        },

        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                // if user is not a whitelabel customer.
                if (!user[0].is_whitelabel) {
                    user[0].domain = ''
                    user[0].brand_image = ''
                    user[0].fav_icon = ''
                }
                cb(null, user);
            }
        },

        function (user, cb) {
            if (user.err) {
                cb(null, user);
            } else {
                var sql = "SELECT * " +
                    "FROM `tb_user_credit_card` " +
                    "WHERE `user_id`=?";
                connection.query(sql, [user[0].user_id], function (err, card) {
                    if (err) {

                        cb(null, {
                            err: 1
                        })

                    } else {

                        user[0].card_details = {
                            "active": card.length ? card[0].is_active : 1,
                            "message": constants.responseMessages.MONTHLY_BILLING_DECLINED
                        };
                        cb(null, user);
                    }
                });
            }
        },

        function (data, cb) {
            if (data.err) {
                cb(null, data);
            } else {
                if (parseInt(user[0].billing_plan) == parseInt(constants.billingPlan.FREE_LIMITED)) {
                    commonFunc.numberOfTasksForCurrentMonth(user[0].user_id, function (chk) {
                        if (chk.length > 0) {
                            user[0].billing_popup.tasks_left = chk[0].num_tasks - chk[0].tasks_count;
                            user[0].billing_popup.tasks_left = user[0].billing_popup.tasks_left <= 0 ? 0 : user[0].billing_popup.tasks_left;
                            if (user[0].billing_popup.tasks_left <= 0) user[0].billing_popup.show_billing_popup = 1;
                        }
                        cb(null, user);
                    })
                } else {
                    cb(null, user);
                }
            }
        },
        function (user, cb) {

            if (user.err) {

                cb(null, user);

            } else {
                if (user[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    commonFunc.checkDispatcherPermissions(user[0].user_id, function (chkDisp) {
                        if (chkDisp && chkDisp.length) {
                            commonFunc.getAllTeamsOfDispatcher(user[0].user_id, function (teams) {
                                user[0].dispatcher_permissions = chkDisp[0];
                                user[0].teams = teams;
                                cb(null, user);
                            });
                        }else{
                            cb(null, {
                                err: 2
                            })
                        }
                    })
                } else {
                    commonFunc.getAllTeamsOfUser(user[0].user_id, function (teams) {
                        user[0].teams = teams == 0 ? [] : teams;
                        cb(null, user);
                    })
                }
            }

        }

    ], function (error, user) {
        if (error) {
            responses.authenticationError(res);
            return;
        } else {
            if (user.err) {
                if (user.err == 1) {
                    responses.sendError(res);
                    return;
                } else {
                    responses.authenticationError(res);
                    return;
                }
            } else {
                user[0].expiry_date = user[0].expiry_datetime;
                user[0].capacity = 1;
                user[0].name = user[0].username;
                delete user[0].password;
                var response = {
                    "message": constants.responseMessages.LOGIN_SUCCESSFULLY,
                    "status": constants.responseFlags.LOGIN_SUCCESSFULLY,
                    "data": user[0]
                }
                res.send(JSON.stringify(response));
                return;
            }
        }
    })
}
/*
 * -----------------------------------------------
 * USER LOGIN VIA ACCESSTOKEN
 * -----------------------------------------------
 */
exports.user_login_via_access_token = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var sql = "UPDATE `tb_users` SET `last_login_datetime`=NOW() ";
                sql += " WHERE `access_token`=? LIMIT 1";
                connection.query(sql, [access_token], function (err, result_update) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in updating user info : ", err, result_update);
                        responses.sendError(res);
                        return;
                    } else {
                        checkForCompanyPoints(result, res);
                    }
                });
            }
        });
    }
};
/*
 * ----------------------
 * VERIFY USER ACCOUNT
 * ----------------------
 */
exports.verify_user_account = function (req, res) {

    var verification_token = req.body.verification_token;
    var email = req.body.email;
    var manvalues = [verification_token, email];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserToken(verification_token, function (result) {
            if (result == 0) {
                responses.invalidAccessError(res);
            } else {
                var sql = "UPDATE `tb_users` SET `verification_status`=? WHERE `verification_token`=? LIMIT 1";
                connection.query(sql, [constants.userVerificationStatus.VERIFY, verification_token], function (err, result_update) {
                    if (err) {
                        logging.logDatabaseQueryError("Error in updating user info : ", err, result_update);
                        responses.sendError(res);
                        return;
                    } else {
                        responses.actionCompleteResponse(res);
                        return;
                    }
                });
            }

        });
    }
};
/*
 * ----------------------
 * VIEW ALL FLEETS
 * ----------------------
 */
exports.view_all_fleets = function (req, res) {

    var access_token = req.body.access_token;
    var tags = req.body.tags;
    if (typeof tags === undefined || tags === 'undefined' || tags == '')
        tags = ''
    var team_id = req.body.team_id;
    if (typeof team_id === undefined || team_id === 'undefined' || team_id == '')
        team_id = ''
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "SELECT IF(teams.`team_id` IS NULL,0,teams.`team_id`) as `team_id`,teams.`team_name`,IF (fleets.`fleet_thumb_image` IS NULL,'',fleets.`fleet_thumb_image`) as `fleet_thumb_image`,IF (fleets.`fleet_image` IS NULL,'',fleets.`fleet_image`) as `fleet_image`,IF (fleets.`username` IS NULL,'',fleets.`username`) as `fleet_name`," +
                            " fleets.`transport_type`,fleets.`transport_desc`,fleets.`license`,fleets.`color`,fleets.`email`,fleets.`phone`,fleets.`registration_status`,fleets.`is_active`,fleets.`is_available`,fleets.`username`,fleets.`tags`," +
                            "fleets.`first_name`,fleets.`last_name`,fleets.`login_id`,fleets.`fleet_id`,fleets.`status` FROM `tb_fleets` fleets " +
                            "LEFT JOIN `tb_fleet_teams` fleet_teams ON fleet_teams.`fleet_id`= fleets.`fleet_id` " +
                            "LEFT JOIN `tb_teams` teams ON teams.`team_id`=fleet_teams.`team_id` " +
                            " WHERE fleets.`user_id`=? AND fleets.`is_deleted`=?";
                        if (team_id) {
                            sql += " AND teams.team_id = " + team_id + " ";
                        }
                        if (tags) {
                            tags.split(',').forEach(function (tag) {
                                sql += " AND fleets.tags LIKE '%" + tag.trim() + "%' ";
                            });
                        }
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkDisp) {
                                if (chkDisp && chkDisp[0].view_driver == constants.hasPermissionStatus.NO) {

                                    sql = "SELECT IF(tm.`team_id` IS NULL,0,tm.`team_id`) as `team_id`,tm.`team_name`,IF (tft.`fleet_thumb_image` IS NULL,'',tft.`fleet_thumb_image`) as `fleet_thumb_image`,IF (tft.`fleet_image` IS NULL,'',tft.`fleet_image`) as `fleet_image`,IF (tft.`username` IS NULL,'',tft.`username`) as `fleet_name`," +
                                        "tft.`transport_type`,tft.`transport_desc`,tft.`license`,tft.`color`,tft.`email`,tft.`phone`,tft.`registration_status`,tft.`is_active`,tft.`is_available`,tft.`username`,tft.`tags`," +
                                        "tft.`first_name`,tft.`last_name`,tft.`login_id`,tft.`fleet_id`,tft.`status` FROM `tb_users` u " +
                                        "LEFT JOIN tb_dispatcher_teams dt ON dt.dispatcher_id = u.user_id " +
                                        "LEFT JOIN tb_fleet_teams ftm ON ftm.team_id = dt.team_id " +
                                        "LEFT JOIN tb_teams tm ON tm.`team_id` = ftm.`team_id` " +
                                        "LEFT JOIN tb_fleets tft ON tft.fleet_id = ftm.fleet_id " +
                                        " WHERE u.`user_id`=? AND tft.`is_deleted`=?";
                                    if (team_id) {
                                        sql += " AND ftm.team_id = " + team_id + " ";
                                    }
                                    if (tags) {
                                        if (tags) {
                                            tags.split(',').forEach(function (tag) {
                                                sql += " AND fleets.tags LIKE '%" + tag.trim() + "%' ";
                                            });
                                        }
                                    }
                                    checkPermissions(sql, result[0].user_id, constants.userDeleteStatus.NO);
                                }
                                else {
                                    checkPermissions(sql, result[0].dispatcher_user_id, constants.userDeleteStatus.NO);
                                }
                            })
                        } else {
                            checkPermissions(sql, result[0].user_id, constants.userDeleteStatus.NO);
                        }


                        function checkPermissions(sql, user, is_deleted) {
                            connection.query(sql, [user, is_deleted], function (err, result_teams) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in viewing teams info : ", err, result_teams);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var fleets = {};
                                    var fleetArray = [];
                                    result_teams.forEach(function (fleet) {
                                        if (fleets[fleet.fleet_id]) {
                                            var team = {};
                                            if (fleet.team_id != 0) {
                                                team.team_id = fleet.team_id;
                                                team.team_name = fleet.team_name;
                                                fleets[fleet.fleet_id].teams.push(team);
                                            }
                                        } else {
                                            fleets[fleet.fleet_id] = {};
                                            fleets[fleet.fleet_id].fleet_id = fleet.fleet_id;
                                            fleets[fleet.fleet_id].fleet_name = fleet.fleet_name;
                                            fleets[fleet.fleet_id].first_name = fleet.first_name;
                                            fleets[fleet.fleet_id].last_name = fleet.last_name;
                                            fleets[fleet.fleet_id].login_id = fleet.login_id;
                                            fleets[fleet.fleet_id].transport_type = fleet.transport_type;
                                            fleets[fleet.fleet_id].transport_desc = fleet.transport_desc;
                                            fleets[fleet.fleet_id].license = fleet.license;
                                            fleets[fleet.fleet_id].color = fleet.color;
                                            fleets[fleet.fleet_id].username = fleet.username;
                                            fleets[fleet.fleet_id].email = fleet.email;
                                            fleets[fleet.fleet_id].phone = fleet.phone;
                                            fleets[fleet.fleet_id].registration_status = fleet.registration_status;
                                            fleets[fleet.fleet_id].is_active = fleet.is_active;
                                            fleets[fleet.fleet_id].is_available = fleet.is_available;
                                            fleets[fleet.fleet_id].fleet_image = fleet.fleet_image;
                                            fleets[fleet.fleet_id].fleet_thumb_image = fleet.fleet_thumb_image;
                                            fleets[fleet.fleet_id].status = fleet.status;
                                            fleets[fleet.fleet_id].tags = fleet.tags;
                                            fleets[fleet.fleet_id].teams = []
                                            var team = {};
                                            if (fleet.team_id != 0) {
                                                team.team_id = fleet.team_id;
                                                team.team_name = fleet.team_name;
                                                fleets[fleet.fleet_id].teams.push(team);
                                            }
                                        }
                                    });
                                    for (var key in fleets) {
                                        fleetArray.push(fleets[key]);
                                    }
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": fleetArray
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            });
                        }

                    }
                });
            }
        });
    }
};
/*
 * ----------------------
 * VIEW ALL JOBS
 * ----------------------
 */
exports.view_all_jobs = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        module.exports.viewAllJobs(result, function (viewAllJobsResult) {
                            res.send(JSON.stringify(viewAllJobsResult));
                            return;
                        })
                    }
                });
            }
        });
    }
};
exports.viewAllJobs = function (result, callback) {
    var sql = "SELECT  fleets.`fleet_id`,fleets.`username` as `fleet_name`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`completed_by_admin`," +
        "jobs.`pickup_delivery_relationship`,jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`," +
        " jobs.`customer_id`,jobs.`customer_username`,jobs.`customer_phone`,jobs.`customer_email`" +
        " FROM `tb_jobs` jobs " +
        "LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
        "WHERE jobs.`user_id`=? ";
    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
        commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkDisp) {
            if (chkDisp && chkDisp[0].view_task == constants.hasPermissionStatus.NO) {
                sql = "SELECT  fleets.`fleet_id`,fleets.`username` as `fleet_name`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`completed_by_admin`," +
                    " jobs.`pickup_delivery_relationship`,jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`," +
                    " jobs.`customer_id`,jobs.`customer_username`,jobs.`customer_phone`,jobs.`customer_email`" +
                    " FROM `tb_jobs` jobs " +
                    " LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
                    " WHERE jobs.`dispatcher_id`=? ";
                executeQuery(sql, result[0].user_id, function (queryResult) {
                    callback(queryResult);
                });
            }
            else {
                executeQuery(sql, result[0].dispatcher_user_id, function (queryResult) {
                    callback(queryResult);
                });
            }
        })
    } else {
        executeQuery(sql, result[0].user_id, function (queryResult) {
            callback(queryResult);
        });
    }
}
function executeQuery(sql, user, callback) {
    connection.query(sql, [user], function (err, result_jobs) {
        if (err) {
            logging.logDatabaseQueryError("Error in viewing all jobs info : ", err, result_jobs);
            responses.sendError(res);
            return;
        } else {
            var result_jobs_length = result_jobs.length;
            for (var i = 0; i < result_jobs_length; i++) {
                if (result_jobs[i].job_pickup_datetime != '0000-00-00 00:00:00') {
                    result_jobs[i].job_pickup_datetime = result_jobs[i].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                }
                if (result_jobs[i].job_delivery_datetime != '0000-00-00 00:00:00') {
                    result_jobs[i].job_delivery_datetime = result_jobs[i].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                }

            }
            var response = {
                "message": constants.responseMessages.ACTION_COMPLETE,
                "status": constants.responseFlags.ACTION_COMPLETE,
                "data": result_jobs
            };
            callback(response);
        }
    });
}
/*
 * ------------------------------
 * VIEW ALL JOBS WITH PAGINATION
 * ------------------------------
 */
exports.view_all_jobs_with_pagination = function (req, res) {

    var access_token = req.query.access_token;
    commonFunc.authenticateUserAccessToken(access_token, function (result) {
        if (result == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                if (checkAccountExpiryResult == 1) {
                    responses.accountExpiryErrorResponse(res);
                    return;
                } else {
                    var layout_type = result[0].layout_type;
                    var whereSql = "tb_jobs.`job_status` NOT IN ( " + constants.jobStatus.DELETED + ") AND tb_jobs.`job_type` NOT IN (" + constants.jobType.APPOINTMENT + "," + constants.jobType.FOS + ") AND tb_jobs.`user_id` = ";
                    if (layout_type == constants.layoutType.APPOINTMENT) {
                        whereSql = "tb_jobs.`job_status` NOT IN ( " + constants.jobStatus.DELETED + ") AND tb_jobs.`job_type` = " + constants.jobType.APPOINTMENT + " AND tb_jobs.`user_id` = ";
                    }
                    if (layout_type == constants.layoutType.FOS) {
                        whereSql = "tb_jobs.`job_status` NOT IN ( " + constants.jobStatus.DELETED + ") AND tb_jobs.`job_type` = " + constants.jobType.FOS + " AND tb_jobs.`user_id` = ";
                    }
                    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        commonFunc.checkDispatcherPermissions(result[0].user_id, function (disPerm) {
                            var te = "0," + disPerm[0].teams;
                            if (disPerm[0].view_task == constants.hasPermissionStatus.NO) {
                                te = disPerm[0].teams;
                            }
                            whereSql = "tb_jobs.`job_status` NOT IN ( " + constants.jobStatus.DELETED + ") AND tb_jobs.`job_type` NOT IN (" + constants.jobType.APPOINTMENT + "," + constants.jobType.FOS + ") AND tb_jobs.`team_id` IN (" + te + ") AND tb_jobs.`user_id`= ";
                            if (layout_type == constants.layoutType.APPOINTMENT) {
                                whereSql = "tb_jobs.`job_status` NOT IN ( " + constants.jobStatus.DELETED + ") AND tb_jobs.`job_type` = " + constants.jobType.APPOINTMENT + " AND tb_jobs.`team_id` IN (" + te + ") AND tb_jobs.`user_id` = ";
                            }
                            if (layout_type == constants.layoutType.FOS) {
                                whereSql = "tb_jobs.`job_status` NOT IN ( " + constants.jobStatus.DELETED + ") AND tb_jobs.`job_type` = " + constants.jobType.FOS + " AND tb_jobs.`team_id` IN (" + te + ") AND tb_jobs.`user_id` = ";
                            }
                            checkIn(layout_type, whereSql, result[0].dispatcher_user_id);
                        })
                    } else {
                        checkIn(layout_type, whereSql, result[0].user_id);
                    }
                    function checkIn(layout_type, whereSql, user) {
                        var QueryBuilder = require('datatable');
                        var tableDefinition = {
                            sSelectSql: "" +
                            "tb_fleets.fleet_id," +
                            "tb_fleets.username as fleet_name," +
                            "tb_jobs.job_time," +
                            "tb_jobs.job_latitude," +
                            "tb_jobs.job_pickup_name," +
                            "tb_jobs.job_pickup_phone," +
                            "tb_jobs.job_pickup_address," +
                            "tb_jobs.job_longitude," +
                            "tb_jobs.job_address," +
                            "tb_jobs.job_status," +
                            "tb_jobs.job_address," +
                            "tb_jobs.job_description," +
                            "tb_jobs.has_pickup," +
                            "tb_jobs.completed_by_admin," +
                            "tb_jobs.pickup_delivery_relationship," +
                            "tb_jobs.job_pickup_datetime," +
                            "tb_jobs.job_id," +
                            "tb_jobs.has_pickup," +
                            "tb_jobs.job_delivery_datetime," +
                            "tb_jobs.job_description," +
                            "tb_jobs.job_type," +
                            "tb_jobs.job_pickup_latitude," +
                            "tb_jobs.job_pickup_longitude," +
                            "tb_jobs.job_pickup_address," +
                            "tb_jobs.job_pickup_email," +
                            "tb_jobs.customer_id," +
                            "tb_jobs.customer_username," +
                            "tb_jobs.customer_phone," +
                            "tb_jobs.customer_email," +
                            "tb_jobs.team_id," +
                            "tb_jobs.job_delivery_datetime ",

                            sFromSql: "`tb_jobs` LEFT JOIN `tb_fleets` ON tb_fleets.`fleet_id`= tb_jobs.`fleet_id`",
                            sWhereAndSql: whereSql + user,
                            sCountColumnName: "tb_jobs.`job_id`",
                            aSearchColumns: [
                                "tb_jobs.job_id", "tb_jobs.job_type", "tb_fleets.username", "tb_jobs.job_pickup_name", "tb_jobs.job_pickup_datetime", "tb_jobs.job_pickup_address",
                                "tb_jobs.customer_username", "tb_jobs.job_address", "tb_jobs.job_delivery_datetime", "tb_jobs.job_status", "tb_jobs.job_description", "tb_jobs.job_time"
                            ]
                        };
                        if ((layout_type == constants.layoutType.APPOINTMENT) || (layout_type == constants.layoutType.FOS)) {

                            tableDefinition.aoColumnDefs = [
                                {mData: "", bSearchable: false},
                                {mData: "job_id", bSearchable: true},
                                {mData: "job_type", bSearchable: true},
                                {mData: "job_description", bSearchable: true},
                                {mData: "tb_fleets.username", bSearchable: true},
                                {mData: "customer_username", bSearchable: true},
                                {mData: "job_pickup_datetime", bSearchable: true},
                                {mData: "job_delivery_datetime", bSearchable: true},
                                {mData: "job_status", bSearchable: true},
                                {mData: "", bSearchable: false}
                            ]
                        } else {
                            tableDefinition.aoColumnDefs = [
                                {mData: "", bSearchable: false},
                                {mData: "job_id", bSearchable: true},
                                {mData: "job_type", bSearchable: true},
                                {mData: "job_description", bSearchable: true},
                                {mData: "tb_fleets.username", bSearchable: true},
                                {mData: "", bSearchable: false},
                                {mData: "job_address", bSearchable: true},
                                {mData: "job_time", bSearchable: true},
                                {mData: "job_status", bSearchable: true},
                                {mData: "", bSearchable: false}
                            ]
                        }
                        var queryBuilder = new QueryBuilder(tableDefinition);
                        var requestQuery = req.query;
                        var queries = queryBuilder.buildQuery(requestQuery);
                        if (queries.length > 2) {
                            queries = queries.splice(1);
                        }
                        queries = queries.join(" ");
                        connection.query(queries, function (err, resultAllRidesData) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in fetching jobs in view_all_jobs_with_pagination : ", err, resultAllRidesData);
                                responses.sendError(res);
                                return;
                            }
                            var resultAllRides = resultAllRidesData.pop();
                            resultAllRides = commonFunc.eliminateCharacters(resultAllRides);
                            var resultAllRidesLength = resultAllRides.length;
                            if (resultAllRidesLength > 0) {
                                var arrResultRides = new Array();
                                for (var i = 0; i < resultAllRidesLength; i++) {
                                    var column_one = '<input type="checkbox" ng-click="showSelectAllBtn(' + resultAllRides[i].job_id + ')" id="checkbtn' + resultAllRides[i].job_id + '" class="tablecheckbox">';
                                    arrResultRides[i] = new Array();

                                    if ((resultAllRides[i].customer_username == "dummy") || (resultAllRides[i].customer_username == null)) {
                                        resultAllRides[i].customer_username = "";
                                    }
                                    if ((resultAllRides[i].customer_email == "dummy") || (resultAllRides[i].customer_email == null)) {
                                        resultAllRides[i].customer_email = "";
                                    }
                                    if ((resultAllRides[i].customer_phone == "+910000000000") || (resultAllRides[i].customer_phone == null)) {
                                        resultAllRides[i].customer_phone = "";
                                    }
                                    if ((resultAllRides[i].job_description == "null") || (resultAllRides[i].job_description == null)) {
                                        resultAllRides[i].job_description = "-";
                                    }
                                    if ((layout_type == constants.layoutType.APPOINTMENT) || (layout_type == constants.layoutType.FOS)) {

                                        arrResultRides[i][0] = '<a ng-click=viewtaskProfileDialog("' + resultAllRides[i].pickup_delivery_relationship + ',' + resultAllRides[i].job_id + '")>' + resultAllRides[i].job_id + '</a>';
                                        arrResultRides[i][1] = resultAllRides[i].job_type == constants.jobType.PICKUP ? "Pick-up" : resultAllRides[i].job_type == constants.jobType.DELIVERY ?
                                            "Delivery" : resultAllRides[i].job_type == constants.jobType.APPOINTMENT ? "Appointment" : resultAllRides[i].job_type == constants.jobType.FOS ?
                                            "Field Workforce" : "";

                                        if (resultAllRides[i].job_description && resultAllRides[i].job_description.length > 25) {
                                            arrResultRides[i][2] = '<div class="taskDescription" popover="' + resultAllRides[i].job_description + '" popover-placement="bottom" popover-trigger="mouseenter">' + resultAllRides[i].job_description + '</div>';
                                        } else {
                                            arrResultRides[i][2] = resultAllRides[i].job_description;
                                        }
                                        arrResultRides[i][3] = resultAllRides[i].fleet_id ? '<a ng-click="getCustomerProfile(' + resultAllRides[i].fleet_id + ',1)" class="ng-binding" ' + resultAllRides[i].fleet_id + '"> ' + resultAllRides[i].fleet_name + '</a>' : '-';
                                        arrResultRides[i][4] = '<a ng-click="getCustomerProfile(' + resultAllRides[i].customer_id + ',0)" class="ng-binding">' + resultAllRides[i].customer_username + '</a>';
                                        arrResultRides[i][5] = resultAllRides[i].job_type == constants.jobType.PICKUP ? resultAllRides[i].job_pickup_address : resultAllRides[i].job_address;
                                        arrResultRides[i][6] = resultAllRides[i].job_pickup_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a');
                                        arrResultRides[i][7] = resultAllRides[i].job_delivery_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a');


                                        var job_status = "";
                                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                                            job_status = '<div class="label circle-success-unassigned" >Unassigned</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.UPCOMING) {
                                            job_status = '<div class="label circle-success-assigned" >Assigned</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.STARTED) {
                                            job_status = '<div class="label circle-success-intransit" >Started</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ENDED) {
                                            job_status = '<div class="label circle-success-completed" >Successful</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                                            job_status = '<div class="label circle-success-failed" >Failed</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ARRIVED) {
                                            job_status = '<div class="label circle-success-arrived" >In Progress</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.PARTIAL) {
                                            job_status = '<div class="label circle-success-arrived" >In Progress</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ACCEPTED) {
                                            job_status = '<div class="label circle-success-accepted" >Accepted</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.DECLINE) {
                                            job_status = '<div class="label circle-success-declined" >Declined</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.CANCEL) {
                                            job_status = '<div class="label circle-success-cancelled" >Canceled</div>';
                                        }
                                        arrResultRides[i][8] = job_status;
                                        resultAllRides[i].job_latitude = '"' + resultAllRides[i].job_latitude + '"';
                                        resultAllRides[i].job_longitude = '"' + resultAllRides[i].job_longitude + '"';
                                        resultAllRides[i].job_pickup_latitude = '"' + resultAllRides[i].job_pickup_latitude + '"';
                                        resultAllRides[i].job_pickup_longitude = '"' + resultAllRides[i].job_pickup_longitude + '"';
                                        resultAllRides[i].job_description = '"' + resultAllRides[i].job_description + '"';
                                        resultAllRides[i].job_pickup_address = '"' + resultAllRides[i].job_pickup_address + '"';
                                        resultAllRides[i].job_pickup_phone = '"' + resultAllRides[i].job_pickup_phone + '"';
                                        resultAllRides[i].job_address = '"' + resultAllRides[i].job_address + '"';
                                        resultAllRides[i].customer_email = '"' + resultAllRides[i].customer_email + '"';
                                        resultAllRides[i].customer_username = '"' + resultAllRides[i].customer_username + '"';
                                        resultAllRides[i].customer_phone = '"' + resultAllRides[i].customer_phone + '"';
                                        resultAllRides[i].fleet_name = '"' + resultAllRides[i].fleet_name + '"';
                                        resultAllRides[i].job_pickup_name = '"' + resultAllRides[i].job_pickup_name + '"';
                                        resultAllRides[i].job_pickup_email = '"' + resultAllRides[i].job_pickup_email + '"';
                                        resultAllRides[i].pickup_delivery_relationship = '"' + resultAllRides[i].pickup_delivery_relationship + '"';
                                        resultAllRides[i].job_pickup_datetime = '"' + moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a') + '"';
                                        resultAllRides[i].job_delivery_datetime = '"' + moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a') + '"';

                                        var data = resultAllRides[i].team_id + "," + resultAllRides[i].job_id + "," + resultAllRides[i].job_description + "," + resultAllRides[i].job_pickup_address + "," + resultAllRides[i].job_pickup_datetime +
                                            "," + resultAllRides[i].job_pickup_latitude + "," + resultAllRides[i].job_pickup_longitude + "," +
                                            resultAllRides[i].job_latitude + "," + resultAllRides[i].job_longitude
                                            + "," + resultAllRides[i].fleet_name + "," + resultAllRides[i].fleet_id +
                                            "," + resultAllRides[i].customer_username + "," + resultAllRides[i].customer_email + "," + resultAllRides[i].customer_phone + "," + resultAllRides[i].job_address +
                                            "," + resultAllRides[i].job_delivery_datetime + "," + resultAllRides[i].job_pickup_name + "," + resultAllRides[i].job_pickup_phone + "," + resultAllRides[i].job_type
                                            + "," + resultAllRides[i].has_pickup + "," + resultAllRides[i].pickup_delivery_relationship + "," + resultAllRides[i].job_pickup_email + "," + resultAllRides[i].job_status;
                                        var copyData = data + ",1";

                                        var assign_type = '';
                                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                                            assign_type = "<a href='' class='btn btn-default' tooltip='Assign Fleet' ng-click='openEditTaskFleet(" + resultAllRides[i].team_id + "," + resultAllRides[i].job_id + ", null, null)'> <i class='fa fa-user icons'></i> </a>";
                                        } else if (resultAllRides[i].job_status != constants.jobStatus.ENDED) {
                                            assign_type = "<a href='' class='btn btn-default' tooltip='Reassign Fleet' ng-click='openEditTaskFleet(" + resultAllRides[i].team_id + "," + resultAllRides[i].job_id + "," + resultAllRides[i].fleet_id + "," + resultAllRides[i].fleet_name + ")'> <i class='fa fa-user icons'></i> </a>";
                                        }

                                        if (resultAllRides[i].job_status == constants.jobStatus.ENDED || resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                                            arrResultRides[i][9] = assign_type + "<a href='' class='btn btn-default' tooltip='Duplicate' ng-click='openUpdateDialog(" + copyData + ")'><i class='fa fa-copy icons'></i></a><a href='' tooltip='Delete' class='btn btn-default' ng-click='openConfirm(" + resultAllRides[i].job_id + ")'><i class='fa fa-trash icons'></i></a>";
                                        } else {
                                            arrResultRides[i][9] = assign_type + "<a href='' class='btn btn-default' tooltip='Duplicate' ng-click='openUpdateDialog(" + copyData + ")'><i class='fa fa-copy icons'></i></a><a href='' tooltip='Edit' class='btn btn-default' ng-click='openUpdateDialog(" + data + ")'><i class='fa fa-pencil icons'></i></a> <a href='' tooltip='Delete' class='btn btn-default' ng-click='openConfirm(" + resultAllRides[i].job_id + ")'><i class='fa fa-trash icons'></i></a>";
                                        }
                                    }
                                    else {

                                        arrResultRides[i][0] = '<a ng-click=viewtaskProfileDialog("' + resultAllRides[i].pickup_delivery_relationship + ',' + resultAllRides[i].job_id + '")>' + resultAllRides[i].job_id + '</a>';
                                        arrResultRides[i][1] = resultAllRides[i].job_type == constants.jobType.PICKUP ? "Pick-up" : resultAllRides[i].job_type == constants.jobType.DELIVERY ?
                                            "Delivery" : resultAllRides[i].job_type == constants.jobType.APPOINTMENT ? "Appointment" : resultAllRides[i].job_type == constants.jobType.FOS ?
                                            "Field Workforce" : "";
                                        if (resultAllRides[i].job_description && resultAllRides[i].job_description.length > 25) {
                                            arrResultRides[i][2] = '<div class="taskDescription" popover="' + resultAllRides[i].job_description + '" popover-placement="bottom" popover-trigger="mouseenter">' + resultAllRides[i].job_description + '</div>';
                                        } else {
                                            arrResultRides[i][2] = resultAllRides[i].job_description;
                                        }
                                        arrResultRides[i][3] = resultAllRides[i].fleet_id ? '<a ng-click="getCustomerProfile(' + resultAllRides[i].fleet_id + ',1)" class="ng-binding" ' + resultAllRides[i].fleet_id + '"> ' + resultAllRides[i].fleet_name + '</a>' : '-';

                                        if (resultAllRides[i].job_type == constants.jobType.PICKUP) {
                                            arrResultRides[i][4] = '<a ng-click="getCustomerProfile(' + resultAllRides[i].customer_id + ',0)" class="ng-binding">' + resultAllRides[i].job_pickup_name + '</a>';
                                            arrResultRides[i][5] = resultAllRides[i].job_pickup_address;
                                            arrResultRides[i][6] = resultAllRides[i].job_pickup_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a');

                                        } else if (resultAllRides[i].job_type == constants.jobType.DELIVERY) {
                                            arrResultRides[i][4] = '<a ng-click="getCustomerProfile(' + resultAllRides[i].customer_id + ',0)" class="ng-binding">' + resultAllRides[i].customer_username + '</a>';
                                            arrResultRides[i][5] = resultAllRides[i].job_address;
                                            arrResultRides[i][6] = resultAllRides[i].job_delivery_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a');
                                        }
                                        var job_status = "";
                                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                                            job_status = '<div class="label circle-success-unassigned" >Unassigned</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.UPCOMING) {
                                            job_status = '<div class="label circle-success-assigned" >Assigned</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.STARTED) {
                                            job_status = '<div class="label circle-success-intransit" >Started</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ENDED) {
                                            job_status = '<div class="label circle-success-completed" >Successful</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                                            job_status = '<div class="label circle-success-failed" >Failed</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ARRIVED) {
                                            job_status = '<div class="label circle-success-arrived" >In Progress</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.PARTIAL) {
                                            job_status = '<div class="label circle-success-arrived" >In Progress</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ACCEPTED) {
                                            job_status = '<div class="label circle-success-accepted" >Accepted</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.DECLINE) {
                                            job_status = '<div class="label circle-success-declined" >Declined</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.CANCEL) {
                                            job_status = '<div class="label circle-success-cancelled" >Canceled</div>';
                                        }
                                        arrResultRides[i][7] = job_status;
                                        resultAllRides[i].job_latitude = '"' + resultAllRides[i].job_latitude + '"';
                                        resultAllRides[i].job_longitude = '"' + resultAllRides[i].job_longitude + '"';
                                        resultAllRides[i].job_pickup_latitude = '"' + resultAllRides[i].job_pickup_latitude + '"';
                                        resultAllRides[i].job_pickup_longitude = '"' + resultAllRides[i].job_pickup_longitude + '"';
                                        resultAllRides[i].job_description = '"' + resultAllRides[i].job_description + '"';
                                        resultAllRides[i].job_pickup_address = '"' + resultAllRides[i].job_pickup_address + '"';
                                        resultAllRides[i].job_pickup_phone = '"' + resultAllRides[i].job_pickup_phone + '"';
                                        resultAllRides[i].job_address = '"' + resultAllRides[i].job_address + '"';
                                        resultAllRides[i].customer_email = '"' + resultAllRides[i].customer_email + '"';
                                        resultAllRides[i].customer_username = '"' + resultAllRides[i].customer_username + '"';
                                        resultAllRides[i].customer_phone = '"' + resultAllRides[i].customer_phone + '"';
                                        resultAllRides[i].fleet_name = '"' + resultAllRides[i].fleet_name + '"';
                                        resultAllRides[i].job_pickup_name = '"' + resultAllRides[i].job_pickup_name + '"';
                                        resultAllRides[i].job_pickup_email = '"' + resultAllRides[i].job_pickup_email + '"';
                                        resultAllRides[i].pickup_delivery_relationship = '"' + resultAllRides[i].pickup_delivery_relationship + '"';
                                        resultAllRides[i].job_pickup_datetime = '"' + moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a') + '"';
                                        resultAllRides[i].job_delivery_datetime = '"' + moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a') + '"';
                                        var data = resultAllRides[i].team_id + "," + resultAllRides[i].job_id + "," + resultAllRides[i].job_description + "," + resultAllRides[i].job_pickup_address + "," + resultAllRides[i].job_pickup_datetime +
                                            "," + resultAllRides[i].job_pickup_latitude + "," + resultAllRides[i].job_pickup_longitude + "," +
                                            resultAllRides[i].job_latitude + "," + resultAllRides[i].job_longitude
                                            + "," + resultAllRides[i].fleet_name + "," + resultAllRides[i].fleet_id +
                                            "," + resultAllRides[i].customer_username + "," + resultAllRides[i].customer_email + "," + resultAllRides[i].customer_phone + "," + resultAllRides[i].job_address +
                                            "," + resultAllRides[i].job_delivery_datetime + "," + resultAllRides[i].job_pickup_name + "," + resultAllRides[i].job_pickup_phone + "," + resultAllRides[i].job_type
                                            + "," + resultAllRides[i].has_pickup + "," + resultAllRides[i].pickup_delivery_relationship + "," + resultAllRides[i].job_pickup_email + "," + resultAllRides[i].job_status;
                                        var copyData = data + ",1";
                                        var assign_type = '';
                                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                                            assign_type = "<a href='' class='btn btn-default' tooltip='Assign Fleet' ng-click='openEditTaskFleet(" + resultAllRides[i].team_id + "," + resultAllRides[i].job_id + ", null, null)'> <i class='fa fa-user icons'></i> </a>";
                                        } else if (resultAllRides[i].job_status != constants.jobStatus.ENDED) {
                                            assign_type = "<a href='' class='btn btn-default' tooltip='Reassign Fleet' ng-click='openEditTaskFleet(" + resultAllRides[i].team_id + "," + resultAllRides[i].job_id + "," + resultAllRides[i].fleet_id + "," + resultAllRides[i].fleet_name + ")'> <i class='fa fa-user icons'></i> </a>";
                                        }
                                        if (resultAllRides[i].job_status == constants.jobStatus.ENDED || resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                                            arrResultRides[i][8] = assign_type + "<a href='' class='btn btn-default' tooltip='Duplicate' ng-click='openUpdateDialog(" + copyData + ")'><i class='fa fa-copy icons'></i></a><a href='' tooltip='Delete' class='btn btn-default' ng-click='openConfirm(" + resultAllRides[i].job_id + ")'><i class='fa fa-trash icons'></i></a>";
                                        } else {
                                            arrResultRides[i][8] = assign_type + "<a href='' class='btn btn-default' tooltip='Duplicate' ng-click='openUpdateDialog(" + copyData + ")'><i class='fa fa-copy icons'></i></a><a href='' tooltip='Edit' class='btn btn-default' ng-click='openUpdateDialog(" + data + ")'><i class='fa fa-pencil icons'></i></a> <a href='' tooltip='Delete' class='btn btn-default' ng-click='openConfirm(" + resultAllRides[i].job_id + ")'><i class='fa fa-trash icons'></i></a>";
                                        }
                                    }
                                    arrResultRides[i].unshift(column_one);
                                }
                                var response = {
                                    "iTotalDisplayRecords": resultAllRidesData[0][0]['COUNT(*)'],
                                    "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                                    "sEcho": 0,
                                    "aaData": arrResultRides
                                };
                                res.send(JSON.stringify(response));
                                return;
                            } else {
                                var response = {
                                    "iTotalDisplayRecords": 0,
                                    "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                                    "sEcho": requestQuery.sEcho,
                                    "aaData": []
                                };
                                res.send(JSON.stringify(response));
                                return;
                            }
                        });
                    }
                }
            });
        }
    });
};
/*
 * ----------------------------------------
 * VIEW ALL JOBS WITH FILTER AND PAGINATION
 * ----------------------------------------
 */
exports.view_jobs_with_filters_and_pagination = function (req, res) {

    var access_token = req.query.access_token;
    var job_status = req.query.job_status;
    commonFunc.authenticateUserAccessToken(access_token, function (result) {
        if (result == 0) {
            responses.authenticationErrorResponse(res);
            return;
        } else {
            commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                if (checkAccountExpiryResult == 1) {
                    responses.accountExpiryErrorResponse(res);
                    return;
                } else {
                    var layout_type = result[0].layout_type;
                    var whereSql = "tb_jobs.`job_status` IN (" + job_status + ") AND tb_jobs.`job_type` NOT IN (" + constants.jobType.APPOINTMENT + "," + constants.jobType.FOS + ") AND tb_jobs.`user_id` = ";
                    if (layout_type == constants.layoutType.APPOINTMENT) {
                        whereSql = "tb_jobs.`job_status` IN (" + job_status + ") AND tb_jobs.`job_type` = " + constants.jobType.APPOINTMENT + " AND tb_jobs.`user_id` = ";
                    }
                    if (layout_type == constants.layoutType.FOS) {
                        whereSql = "tb_jobs.`job_status` IN (" + job_status + ") AND tb_jobs.`job_type` = " + constants.jobType.FOS + " AND tb_jobs.`user_id` = ";
                    }
                    if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        commonFunc.checkDispatcherPermissions(result[0].user_id, function (disPerm) {
                            var te = "0," + disPerm[0].teams;
                            if (disPerm[0].view_task == constants.hasPermissionStatus.NO) {
                                te = disPerm[0].teams;
                            }
                            whereSql = "tb_jobs.`job_status` IN ( " + job_status + ") AND tb_jobs.`job_type` NOT IN (" + constants.jobType.APPOINTMENT + "," + constants.jobType.FOS + ") AND tb_jobs.`team_id` IN (" + te + ") AND tb_jobs.`user_id`= ";
                            if (layout_type == constants.layoutType.APPOINTMENT) {
                                whereSql = "tb_jobs.`job_status` IN ( " + job_status + ") AND tb_jobs.`job_type` = " + constants.jobType.APPOINTMENT + " AND tb_jobs.`team_id` IN (" + te + ") AND tb_jobs.`user_id` = ";
                            }
                            if (layout_type == constants.layoutType.FOS) {
                                whereSql = "tb_jobs.`job_status`  IN ( " + job_status + ") AND tb_jobs.`job_type` = " + constants.jobType.FOS + " AND tb_jobs.`team_id` IN (" + te + ") AND tb_jobs.`user_id` = ";
                            }
                            checkIn(whereSql, result[0].dispatcher_user_id);

                        })
                    } else {
                        checkIn(whereSql, result[0].user_id);
                    }
                    function checkIn(whereSql, user) {
                        var QueryBuilder = require('datatable');
                        var tableDefinition = {
                            sSelectSql: "" +
                            "tb_fleets.fleet_id," +
                            "tb_fleets.username as fleet_name," +
                            "tb_jobs.job_pickup_name," +
                            "tb_jobs.job_latitude," +
                            "tb_jobs.job_pickup_name," +
                            "tb_jobs.job_pickup_phone," +
                            "tb_jobs.job_pickup_address," +
                            "tb_jobs.job_longitude," +
                            "tb_jobs.job_address," +
                            "tb_jobs.job_status," +
                            "tb_jobs.job_address," +
                            "tb_jobs.job_description," +
                            "tb_jobs.job_time," +
                            "tb_jobs.has_pickup," +
                            "tb_jobs.completed_by_admin," +
                            "tb_jobs.pickup_delivery_relationship," +
                            "tb_jobs.job_pickup_datetime," +
                            "tb_jobs.job_id," +
                            "tb_jobs.has_pickup," +
                            "tb_jobs.job_delivery_datetime," +
                            "tb_jobs.job_description," +
                            "tb_jobs.job_type," +
                            "tb_jobs.job_pickup_latitude," +
                            "tb_jobs.job_pickup_longitude," +
                            "tb_jobs.job_pickup_address," +
                            "tb_jobs.job_pickup_email," +
                            "tb_jobs.customer_id," +
                            "tb_jobs.customer_username," +
                            "tb_jobs.customer_phone," +
                            "tb_jobs.customer_email," +
                            "tb_jobs.team_id," +
                            "tb_jobs.job_delivery_datetime ",

                            sFromSql: "`tb_jobs` LEFT JOIN `tb_fleets` ON tb_fleets.`fleet_id`= tb_jobs.`fleet_id`",
                            sWhereAndSql: whereSql + user,
                            aSearchColumns: [
                                "tb_jobs.job_id", "tb_jobs.job_type", "tb_fleets.username", "tb_jobs.job_pickup_name", "tb_jobs.job_pickup_datetime", "tb_jobs.job_pickup_address",
                                "tb_jobs.customer_username", "tb_jobs.job_address", "tb_jobs.job_delivery_datetime", "tb_jobs.job_status", "tb_jobs.job_description", "tb_jobs.job_time"
                            ],

                            sCountColumnName: "tb_jobs.`job_id`"
                        };
                        if ((layout_type == constants.layoutType.APPOINTMENT) || (layout_type == constants.layoutType.FOS)) {

                            tableDefinition.aoColumnDefs = [
                                {mData: "", bSearchable: false},
                                {mData: "job_id", bSearchable: true},
                                {mData: "job_type", bSearchable: true},
                                {mData: "job_description", bSearchable: true},
                                {mData: "tb_fleets.username", bSearchable: true},
                                {mData: "customer_username", bSearchable: true},
                                {mData: "job_pickup_datetime", bSearchable: true},
                                {mData: "job_delivery_datetime", bSearchable: true},
                                {mData: "job_status", bSearchable: true},
                                {mData: "", bSearchable: false}
                            ]
                        } else {
                            tableDefinition.aoColumnDefs = [
                                {mData: "", bSearchable: false},
                                {mData: "job_id", bSearchable: true},
                                {mData: "job_type", bSearchable: true},
                                {mData: "job_description", bSearchable: true},
                                {mData: "tb_fleets.username", bSearchable: true},
                                {mData: "", bSearchable: false},
                                {mData: "job_address", bSearchable: true},
                                {mData: "job_time", bSearchable: true},
                                {mData: "job_status", bSearchable: true},
                                {mData: "", bSearchable: false}
                            ]
                        }

                        var queryBuilder = new QueryBuilder(tableDefinition);
                        var requestQuery = req.query;
                        var queries = queryBuilder.buildQuery(requestQuery);
                        if (queries.length > 2) {
                            queries = queries.splice(1);
                        }
                        queries = queries.join(" ");
                        connection.query(queries, function (err, resultAllRidesData) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in fetching jobs in view_jobs_with_filters_and_pagination : ", err, resultAllRidesData);
                                responses.sendError(res);
                                return;
                            }
                            var resultAllRides = resultAllRidesData.pop();
                            resultAllRides = commonFunc.eliminateCharacters(resultAllRides);
                            var resultAllRidesLength = resultAllRides.length;
                            if (resultAllRidesLength > 0) {

                                var arrResultRides = new Array();
                                for (var i = 0; i < resultAllRidesLength; i++) {
                                    var column_one = '<input type="checkbox" ng-click="showSelectAllBtn(' + resultAllRides[i].job_id + ')" id="checkbtn' + resultAllRides[i].job_id + '" class="tablecheckbox">';
                                    arrResultRides[i] = new Array();
                                    if ((resultAllRides[i].customer_username == "dummy") || (resultAllRides[i].customer_username == null)) {
                                        resultAllRides[i].customer_username = "";
                                    }
                                    if ((resultAllRides[i].customer_email == "dummy") || (resultAllRides[i].customer_email == null)) {
                                        resultAllRides[i].customer_email = "";
                                    }
                                    if ((resultAllRides[i].customer_phone == "+910000000000") || (resultAllRides[i].customer_phone == null)) {
                                        resultAllRides[i].customer_phone = "";
                                    }
                                    if ((resultAllRides[i].job_description == "null") || (resultAllRides[i].job_description == null)) {
                                        resultAllRides[i].job_description = "-";
                                    }
                                    if ((layout_type == constants.layoutType.APPOINTMENT) || (layout_type == constants.layoutType.FOS)) {
                                        arrResultRides[i][0] = '<a ng-click=viewtaskProfileDialog("' + resultAllRides[i].pickup_delivery_relationship + ',' + resultAllRides[i].job_id + '")>' + resultAllRides[i].job_id + '</a>';
                                        arrResultRides[i][1] = resultAllRides[i].job_type == constants.jobType.PICKUP ? "Pick-up" : resultAllRides[i].job_type == constants.jobType.DELIVERY ?
                                            "Delivery" : resultAllRides[i].job_type == constants.jobType.APPOINTMENT ? "Appointment" : resultAllRides[i].job_type == constants.jobType.FOS ?
                                            "Field Workforce" : "";

                                        if (resultAllRides[i].job_description && resultAllRides[i].job_description.length > 25) {
                                            arrResultRides[i][2] = '<div class="taskDescription" popover="' + resultAllRides[i].job_description + '" popover-placement="bottom" popover-trigger="mouseenter">' + resultAllRides[i].job_description + '</div>';
                                        } else {
                                            arrResultRides[i][2] = resultAllRides[i].job_description;
                                        }
                                        arrResultRides[i][3] = resultAllRides[i].fleet_id ? '<a ng-click="getCustomerProfile(' + resultAllRides[i].fleet_id + ',1)" class="ng-binding" ' + resultAllRides[i].fleet_id + '"> ' + resultAllRides[i].fleet_name + '</a>' : '-';
                                        arrResultRides[i][4] = '<a ng-click="getCustomerProfile(' + resultAllRides[i].customer_id + ',0)" class="ng-binding">' + resultAllRides[i].customer_username + '</a>';
                                        arrResultRides[i][5] = resultAllRides[i].job_type == constants.jobType.PICKUP ? resultAllRides[i].job_pickup_address : resultAllRides[i].job_address;
                                        arrResultRides[i][6] = resultAllRides[i].job_pickup_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a');
                                        arrResultRides[i][7] = resultAllRides[i].job_delivery_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a');
                                        var job_status = "";
                                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                                            job_status = '<div class="label circle-success-unassigned" >Unassigned</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.UPCOMING) {
                                            job_status = '<div class="label circle-success-assigned" >Assigned</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.STARTED) {
                                            job_status = '<div class="label circle-success-intransit" >Started</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ENDED) {
                                            job_status = '<div class="label circle-success-completed" >Successful</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                                            job_status = '<div class="label circle-success-failed" >Failed</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ARRIVED) {
                                            job_status = '<div class="label circle-success-arrived" >In Progress</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.PARTIAL) {
                                            job_status = '<div class="label circle-success-arrived" >In Progress</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ACCEPTED) {
                                            job_status = '<div class="label circle-success-accepted" >Accepted</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.DECLINE) {
                                            job_status = '<div class="label circle-success-declined" >Declined</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.CANCEL) {
                                            job_status = '<div class="label circle-success-cancelled" >Canceled</div>';
                                        }
                                        arrResultRides[i][8] = job_status;
                                        resultAllRides[i].job_latitude = '"' + resultAllRides[i].job_latitude + '"';
                                        resultAllRides[i].job_longitude = '"' + resultAllRides[i].job_longitude + '"';
                                        resultAllRides[i].job_pickup_latitude = '"' + resultAllRides[i].job_pickup_latitude + '"';
                                        resultAllRides[i].job_pickup_longitude = '"' + resultAllRides[i].job_pickup_longitude + '"';
                                        resultAllRides[i].job_description = '"' + resultAllRides[i].job_description + '"';
                                        resultAllRides[i].job_pickup_address = '"' + resultAllRides[i].job_pickup_address + '"';
                                        resultAllRides[i].job_pickup_phone = '"' + resultAllRides[i].job_pickup_phone + '"';
                                        resultAllRides[i].job_address = '"' + resultAllRides[i].job_address + '"';
                                        resultAllRides[i].customer_email = '"' + resultAllRides[i].customer_email + '"';
                                        resultAllRides[i].customer_username = '"' + resultAllRides[i].customer_username + '"';
                                        resultAllRides[i].customer_phone = '"' + resultAllRides[i].customer_phone + '"';
                                        resultAllRides[i].fleet_name = '"' + resultAllRides[i].fleet_name + '"';
                                        resultAllRides[i].job_pickup_name = '"' + resultAllRides[i].job_pickup_name + '"';
                                        resultAllRides[i].job_pickup_email = '"' + resultAllRides[i].job_pickup_email + '"';
                                        resultAllRides[i].pickup_delivery_relationship = '"' + resultAllRides[i].pickup_delivery_relationship + '"';
                                        resultAllRides[i].job_pickup_datetime = '"' + moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a') + '"';
                                        resultAllRides[i].job_delivery_datetime = '"' + moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a') + '"';
                                        var data = resultAllRides[i].team_id + "," + resultAllRides[i].job_id + "," + resultAllRides[i].job_description + "," + resultAllRides[i].job_pickup_address + "," + resultAllRides[i].job_pickup_datetime
                                            + "," + resultAllRides[i].job_pickup_latitude + "," + resultAllRides[i].job_pickup_longitude + "," +
                                            resultAllRides[i].job_latitude + "," + resultAllRides[i].job_longitude
                                            + "," + resultAllRides[i].fleet_name + "," + resultAllRides[i].fleet_id + "," + resultAllRides[i].customer_username + "," + resultAllRides[i].customer_email + "," + resultAllRides[i].customer_phone + "," + resultAllRides[i].job_address
                                            + "," + resultAllRides[i].job_delivery_datetime + "," + resultAllRides[i].job_pickup_name + "," + resultAllRides[i].job_pickup_phone + "," + resultAllRides[i].job_type
                                            + "," + resultAllRides[i].has_pickup + "," + resultAllRides[i].pickup_delivery_relationship + "," + resultAllRides[i].job_pickup_email + "," + resultAllRides[i].job_status;
                                        var copyData = data + ",1";
                                        var assign_type = '';
                                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                                            assign_type = "<a href='' class='btn btn-default' tooltip='Assign Fleet' ng-click='openEditTaskFleet(" + resultAllRides[i].team_id + "," + resultAllRides[i].job_id + ", null, null)'> <i class='fa fa-user icons'></i> </a>";
                                        } else if (resultAllRides[i].job_status != constants.jobStatus.ENDED) {
                                            assign_type = "<a href='' class='btn btn-default' tooltip='Reassign Fleet' ng-click='openEditTaskFleet(" + resultAllRides[i].team_id + "," + resultAllRides[i].job_id + "," + resultAllRides[i].fleet_id + "," + resultAllRides[i].fleet_name + ")'> <i class='fa fa-user icons'></i> </a>";
                                        }
                                        if (resultAllRides[i].job_status == constants.jobStatus.ENDED || resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                                            arrResultRides[i][9] = assign_type + "<a href='' class='btn btn-default' tooltip='Duplicate' ng-click='openUpdateDialog(" + copyData + ")'><i class='fa fa-copy icons'></i></a><a href='' tooltip='Delete' class='btn btn-default' ng-click='openConfirm(" + resultAllRides[i].job_id + ")'><i class='fa fa-trash icons'></i></a>";
                                        } else {
                                            arrResultRides[i][9] = assign_type + "<a href='' class='btn btn-default' tooltip='Duplicate' ng-click='openUpdateDialog(" + copyData + ")'><i class='fa fa-copy icons'></i></a><a href='' tooltip='Edit' class='btn btn-default' ng-click='openUpdateDialog(" + data + ")'><i class='fa fa-pencil icons'></i></a> <a href='' tooltip='Delete' class='btn btn-default' ng-click='openConfirm(" + resultAllRides[i].job_id + ")'><i class='fa fa-trash icons'></i></a>";
                                        }
                                    }
                                    else {
                                        arrResultRides[i][0] = '<a ng-click=viewtaskProfileDialog("' + resultAllRides[i].pickup_delivery_relationship + ',' + resultAllRides[i].job_id + '")>' + resultAllRides[i].job_id + '</a>';
                                        arrResultRides[i][1] = resultAllRides[i].job_type == constants.jobType.PICKUP ? "Pick-up" : resultAllRides[i].job_type == constants.jobType.DELIVERY ?
                                            "Delivery" : resultAllRides[i].job_type == constants.jobType.APPOINTMENT ? "Appointment" : resultAllRides[i].job_type == constants.jobType.FOS ?
                                            "Field Workforce" : "";
                                        if (resultAllRides[i].job_description && resultAllRides[i].job_description.length > 25) {
                                            arrResultRides[i][2] = '<div class="taskDescription" popover="' + resultAllRides[i].job_description + '" popover-placement="bottom" popover-trigger="mouseenter">' + resultAllRides[i].job_description + '</div>';
                                        } else {
                                            arrResultRides[i][2] = resultAllRides[i].job_description;
                                        }
                                        arrResultRides[i][3] = resultAllRides[i].fleet_id ? '<a ng-click="getCustomerProfile(' + resultAllRides[i].fleet_id + ',1)" class="ng-binding" ' + resultAllRides[i].fleet_id + '"> ' + resultAllRides[i].fleet_name + '</a>' : '-';
                                        if (resultAllRides[i].job_type == constants.jobType.PICKUP) {
                                            arrResultRides[i][4] = '<a href=ng-click="getCustomerProfile(' + resultAllRides[i].customer_id + ',0)" class="ng-binding">' + resultAllRides[i].job_pickup_name + '</a>';
                                            arrResultRides[i][5] = resultAllRides[i].job_pickup_address;
                                            arrResultRides[i][6] = resultAllRides[i].job_pickup_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a');

                                        } else if (resultAllRides[i].job_type == constants.jobType.DELIVERY) {
                                            arrResultRides[i][4] = '<a href=ng-click="getCustomerProfile(' + resultAllRides[i].customer_id + ',0)" class="ng-binding">' + resultAllRides[i].customer_username + '</a>';
                                            arrResultRides[i][5] = resultAllRides[i].job_address;
                                            arrResultRides[i][6] = resultAllRides[i].job_delivery_datetime == "0000-00-00 00:00:00" ? "-" : moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a');
                                        }
                                        var job_status = "";
                                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                                            job_status = '<div class="label circle-success-unassigned" >Unassigned</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.UPCOMING) {
                                            job_status = '<div class="label circle-success-assigned" >Assigned</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.STARTED) {
                                            job_status = '<div class="label circle-success-intransit" >Started</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ENDED) {
                                            job_status = '<div class="label circle-success-completed" >Successful</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                                            job_status = '<div class="label circle-success-failed" >Failed</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ARRIVED) {
                                            job_status = '<div class="label circle-success-arrived" >In Progress</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.PARTIAL) {
                                            job_status = '<div class="label circle-success-arrived" >In Progress</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.ACCEPTED) {
                                            job_status = '<div class="label circle-success-accepted" >Accepted</div>';
                                        }
                                        else if (resultAllRides[i].job_status == constants.jobStatus.DECLINE) {
                                            job_status = '<div class="label circle-success-declined" >Declined</div>';
                                        } else if (resultAllRides[i].job_status == constants.jobStatus.CANCEL) {
                                            job_status = '<div class="label circle-success-cancelled" >Canceled</div>';
                                        }
                                        arrResultRides[i][7] = job_status;
                                        resultAllRides[i].job_latitude = '"' + resultAllRides[i].job_latitude + '"';
                                        resultAllRides[i].job_longitude = '"' + resultAllRides[i].job_longitude + '"';
                                        resultAllRides[i].job_pickup_latitude = '"' + resultAllRides[i].job_pickup_latitude + '"';
                                        resultAllRides[i].job_pickup_longitude = '"' + resultAllRides[i].job_pickup_longitude + '"';
                                        resultAllRides[i].job_description = '"' + resultAllRides[i].job_description + '"';
                                        resultAllRides[i].job_pickup_address = '"' + resultAllRides[i].job_pickup_address + '"';
                                        resultAllRides[i].job_pickup_phone = '"' + resultAllRides[i].job_pickup_phone + '"';
                                        resultAllRides[i].job_address = '"' + resultAllRides[i].job_address + '"';
                                        resultAllRides[i].customer_email = '"' + resultAllRides[i].customer_email + '"';
                                        resultAllRides[i].customer_username = '"' + resultAllRides[i].customer_username + '"';
                                        resultAllRides[i].customer_phone = '"' + resultAllRides[i].customer_phone + '"';
                                        resultAllRides[i].fleet_name = '"' + resultAllRides[i].fleet_name + '"';
                                        resultAllRides[i].job_pickup_name = '"' + resultAllRides[i].job_pickup_name + '"';
                                        resultAllRides[i].job_pickup_email = '"' + resultAllRides[i].job_pickup_email + '"';
                                        resultAllRides[i].pickup_delivery_relationship = '"' + resultAllRides[i].pickup_delivery_relationship + '"';
                                        resultAllRides[i].job_pickup_datetime = '"' + moment(resultAllRides[i].job_pickup_datetime).format('MM/DD/YYYY hh:mm a') + '"';
                                        resultAllRides[i].job_delivery_datetime = '"' + moment(resultAllRides[i].job_delivery_datetime).format('MM/DD/YYYY hh:mm a') + '"';
                                        var data = resultAllRides[i].team_id + "," + resultAllRides[i].job_id + "," + resultAllRides[i].job_description + "," + resultAllRides[i].job_pickup_address + "," + resultAllRides[i].job_pickup_datetime
                                            + "," + resultAllRides[i].job_pickup_latitude + "," + resultAllRides[i].job_pickup_longitude + "," +
                                            resultAllRides[i].job_latitude + "," + resultAllRides[i].job_longitude + "," + resultAllRides[i].fleet_name + "," + resultAllRides[i].fleet_id
                                            + "," + resultAllRides[i].customer_username + "," + resultAllRides[i].customer_email + "," + resultAllRides[i].customer_phone + "," + resultAllRides[i].job_address
                                            + "," + resultAllRides[i].job_delivery_datetime + "," + resultAllRides[i].job_pickup_name + "," + resultAllRides[i].job_pickup_phone + "," + resultAllRides[i].job_type
                                            + "," + resultAllRides[i].has_pickup + "," + resultAllRides[i].pickup_delivery_relationship + "," + resultAllRides[i].job_pickup_email + "," + resultAllRides[i].job_status;
                                        var copyData = data + ",1";
                                        var assign_type = '';
                                        if (resultAllRides[i].job_status == constants.jobStatus.UNASSIGNED) {
                                            assign_type = "<a href='' class='btn btn-default' tooltip='Assign Fleet' ng-click='openEditTaskFleet(" + resultAllRides[i].team_id + "," + resultAllRides[i].job_id + ", null, null)'> <i class='fa fa-user icons'></i> </a>";
                                        } else if (resultAllRides[i].job_status != constants.jobStatus.ENDED) {
                                            assign_type = "<a href='' class='btn btn-default' tooltip='Reassign Fleet' ng-click='openEditTaskFleet(" + resultAllRides[i].team_id + "," + resultAllRides[i].job_id + "," + resultAllRides[i].fleet_id + "," + resultAllRides[i].fleet_name + ")'> <i class='fa fa-user icons'></i> </a>";
                                        }
                                        if (resultAllRides[i].job_status == constants.jobStatus.ENDED || resultAllRides[i].job_status == constants.jobStatus.FAILED) {
                                            arrResultRides[i][8] = assign_type + "<a href='' class='btn btn-default' tooltip='Duplicate' ng-click='openUpdateDialog(" + copyData + ")'><i class='fa fa-copy icons'></i></a><a href='' tooltip='Delete' class='btn btn-default' ng-click='openConfirm(" + resultAllRides[i].job_id + ")'><i class='fa fa-trash icons'></i></a>";
                                        } else {
                                            arrResultRides[i][8] = assign_type + "<a href='' class='btn btn-default' tooltip='Duplicate' ng-click='openUpdateDialog(" + copyData + ")'><i class='fa fa-copy icons'></i></a><a href='' tooltip='Edit' class='btn btn-default' ng-click='openUpdateDialog(" + data + ")'><i class='fa fa-pencil icons'></i></a> <a href='' tooltip='Delete' class='btn btn-default' ng-click='openConfirm(" + resultAllRides[i].job_id + ")'><i class='fa fa-trash icons'></i></a>";
                                        }
                                    }
                                    arrResultRides[i].unshift(column_one);
                                }
                                var response = {
                                    "iTotalDisplayRecords": resultAllRidesData[0][0]['COUNT(*)'],
                                    "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                                    "sEcho": 0,
                                    "aaData": arrResultRides
                                };
                                res.send(JSON.stringify(response));
                                return;
                            } else {
                                var response = {
                                    "iTotalDisplayRecords": 0,
                                    "iTotalRecords": resultAllRidesData[0][0]['COUNT(*)'],
                                    "sEcho": requestQuery.sEcho,
                                    "aaData": []
                                };
                                res.send(JSON.stringify(response));
                                return;
                            }
                        });
                    }

                }
            });
        }
    });
};
/*
 * --------------------------
 * VIEW JOBS WITH FILTERS
 * --------------------------
 */
exports.view_jobs_with_filters = function (req, res) {

    var access_token = req.body.access_token;
    var job_status = req.body.job_status;
    var job_type = req.body.job_type;
    var manvalues = [access_token, job_type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {

                        var sql = "SELECT  fleets.`fleet_id`,fleets.`username` as `fleet_name`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`completed_by_admin`," +
                            "jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`," +
                            " jobs.`customer_id`,jobs.`customer_username`,jobs.`customer_phone`,jobs.`customer_email`" +
                            " FROM `tb_jobs` jobs " +
                            "LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
                            "WHERE jobs.`user_id`=? AND `job_type` IN (" + job_type + ") ";
                        if (job_status != "") {
                            sql += "AND jobs.`job_status` IN (" + job_status + ") ";
                        }
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            commonFunc.checkDispatcherPermissions(result[0].user_id, function (disPerm) {
                                if (disPerm && disPerm[0].view_task == constants.hasPermissionStatus.NO) {
                                    sql = "SELECT  fleets.`fleet_id`,fleets.`username` as `fleet_name`,jobs.`job_pickup_name`,jobs.`job_pickup_phone`,jobs.`job_latitude`,jobs.`job_longitude`,jobs.`job_address`,jobs.`job_status`,jobs.`job_description`,jobs.`has_pickup`,jobs.`completed_by_admin`," +
                                        "jobs.`job_pickup_datetime`,jobs.`job_id`,jobs.`job_delivery_datetime`,jobs.`job_type`,jobs.`job_pickup_latitude`,jobs.`job_pickup_longitude`,jobs.`job_pickup_address`," +
                                        " jobs.`customer_id`,jobs.`customer_username`,jobs.`customer_phone`,jobs.`customer_email`" +
                                        "  FROM `tb_jobs` jobs " +
                                        " LEFT JOIN `tb_fleets` fleets ON fleets.`fleet_id`= jobs.`fleet_id` " +
                                        "WHERE jobs.`dispatcher_id`=? AND `job_type` IN (" + job_type + ") ";
                                    if (job_status != "") {
                                        sql += "AND jobs.`job_status` IN (" + job_status + ") ";
                                    }
                                    checkPermissions(sql, result[0].user_id);
                                }
                                else {
                                    checkPermissions(sql, result[0].dispatcher_user_id);
                                }
                            })
                        } else {
                            checkPermissions(sql, result[0].user_id);
                        }


                        function checkPermissions(sql, user) {

                            connection.query(sql, [user], function (err, result_jobs) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in viewing all jobs info : ", err, result_jobs);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var upcoming = [], intransit = [], ended = [], failed = [];
                                    var result_jobs_length = result_jobs.length;
                                    for (var i = 0; i < result_jobs_length; i++) {
                                        if (result_jobs[i].job_pickup_datetime != '0000-00-00 00:00:00') {
                                            result_jobs[i].job_pickup_datetime = result_jobs[i].job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                        }
                                        if (result_jobs[i].job_delivery_datetime != '0000-00-00 00:00:00') {
                                            result_jobs[i].job_delivery_datetime = result_jobs[i].job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                        }
                                        //if (result_jobs[i].job_status == constants.jobStatus.UPCOMING) {
                                        //    upcoming.push(result_jobs[i]);
                                        //} else if (result_jobs[i].job_status == constants.jobStatus.STARTED) {
                                        //    intransit.push(result_jobs[i]);
                                        //} else if (result_jobs[i].job_status == constants.jobStatus.ENDED) {
                                        //    ended.push(result_jobs[i]);
                                        //} else if (result_jobs[i].job_status == constants.jobStatus.FAILED) {
                                        //    failed.push(result_jobs[i]);
                                        //}
                                    }
                                    //result_jobs = {
                                    //    "upcoming": upcoming,
                                    //    "intransit": intransit,
                                    //    "ended": ended,
                                    //    "failed":failed
                                    //}
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": result_jobs
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};
/*
 * -----------------------
 * CHANGE USERS PASSWORD
 * -----------------------
 */
exports.users_change_password = function (req, res) {

    var access_token = req.body.access_token;
    var old_password = req.body.old_password;
    var new_password = req.body.new_password;
    var manvalues = [access_token, old_password, new_password];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (old_password == new_password) {
                            var response = {
                                "message": constants.responseMessages.SAME_PASSWORD_ERROR,
                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                "data": {}
                            };
                            res.send(JSON.stringify(response));
                            return;
                        } else {
                            var encrypted_old_pass = md5(old_password);
                            var encrypted_new_pass = md5(new_password);
                            var sql = "SELECT `password` FROM `tb_users` WHERE `access_token`=? LIMIT 1";
                            connection.query(sql, [access_token], function (err, result_check) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in fetching users info : ", err, result_check);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    if (result_check[0].password != encrypted_old_pass) {
                                        var response = {
                                            "message": constants.responseMessages.CURRENT_PASSWORD_INCORRECT,
                                            "status": constants.responseFlags.WRONG_PASSWORD,
                                            "data": {}
                                        };
                                        res.send(JSON.stringify(response));
                                        return;
                                    } else {
                                        if (new_password.length < config.get('AppPasswordLength')) {
                                            var response = {
                                                "message": constants.responseMessages.APP_PASSWORD_ERROR,
                                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                "data": {}
                                            };
                                            res.send(JSON.stringify(response));
                                            return;
                                        } else {
                                            var new_access_token = md5(access_token + new Date());
                                            var is_first_time_login;
                                            if (result[0].is_first_time_login == constants.isFirstTimeLogin.LAYOUT) {
                                                is_first_time_login = constants.isFirstTimeLogin.LAYOUT
                                            } else {
                                                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                                                    is_first_time_login = constants.isFirstTimeLogin.LAYOUT;
                                                } else {
                                                    is_first_time_login = constants.isFirstTimeLogin.NO;
                                                }
                                            }
                                            var sql = "UPDATE `tb_users` SET `password`=?,`access_token`=?,`is_first_time_login`=? WHERE `access_token`=? LIMIT 1";
                                            connection.query(sql, [encrypted_new_pass, new_access_token, is_first_time_login, access_token], function (err, result_check) {
                                                if (err) {
                                                    logging.logDatabaseQueryError("Error in updating users new password : ", err, result_check);
                                                    responses.sendError(res);
                                                    return;
                                                } else {
                                                    var response = {
                                                        "message": constants.responseMessages.PASSWORD_CHANGED_SUCCESSFULLY,
                                                        "status": constants.responseFlags.PASSWORD_CHANGED_SUCCESSFULLY,
                                                        "data": {
                                                            "access_token": new_access_token
                                                        }
                                                    };
                                                    res.send(JSON.stringify(response));
                                                    return;
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};
/*
 * -----------------------
 * SKIP CHANGE USERS PASSWORD
 * -----------------------
 */
exports.skip_users_change_password = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var is_first_time_login;
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            is_first_time_login = constants.isFirstTimeLogin.LAYOUT;
                        } else {
                            is_first_time_login = constants.isFirstTimeLogin.NO;
                        }

                        var sql = "UPDATE `tb_users` SET `is_first_time_login`=? WHERE `access_token`=? LIMIT 1";
                        connection.query(sql, [is_first_time_login, access_token], function (err, result_check) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in skip change password : ", err, result_check);
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                                return;
                            }
                        });
                    }
                });
            }
        });
    }
};
/*
 * -----------------------
 * SET ONE TIME PROCESSES
 * -----------------------
 */
exports.set_one_time_processes = function (req, res) {

    var access_token = req.body.access_token;
    var layout_type = req.body.layout_type;
    var manvalues = [access_token, layout_type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            responses.invalidAccessError(res);
                        } else {
                            var sql = "UPDATE `tb_users` SET `layout_type` = ?,`is_first_time_login`= ? WHERE `access_token`=? LIMIT 1";
                            connection.query(sql, [layout_type, constants.isFirstTimeLogin.LAYOUT, access_token], function (err, result_check) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in setting ONE TIME PROCESSES : ", err, result_check);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    responses.actionCompleteResponse(res);
                                    return;
                                }
                            });
                        }

                    }
                });
            }
        });
    }
};
/*
 * ----------------------------------
 * USERS FORGOT PASSWORD FROM EMAIL
 * ----------------------------------
 */
exports.users_forgot_password_from_email = function (req, res) {

    var email = req.body.email;
    var manValues = [email];
    var checkdata = commonFunc.checkBlank(manValues);
    if (checkdata == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserEmail(email, function (result) {
            if (result == 0) {
                responses.authenticateEmailNotExists(res);
                return;
            } else {
                var version_user_id = result[0].user_id
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES){
                    version_user_id = result[0].dispatcher_user_id
                }

                commonFunc.getVersion2(version_user_id, function(version) {
                    var brand_name = config.get('projectName');
                    if (version && version.length){
                        brand_name = version[0].brand_name;
                    }
                    var username = '';
                    if (result[0].username) {
                        username = result[0].username
                    }
                    var md5 = require('MD5');
                    var token = md5(result[0].email + commonFunc.generateRandomString());
                    var sql = "UPDATE `tb_users` set `verification_token`=? WHERE `email`=? LIMIT 1";
                    connection.query(sql, [token, email], function (err, response) {

                        var link = config.get("forgotPasswordPageLink");
                        link += "?token=" + token + "&email=" + email + "&type=changeU";

                        commonFunc.emailPlainFormatting(username, '', '', '', link, function (returnMessage) {
                            returnMessage = returnMessage.replace(/BRAND_NAME/g,brand_name);
                            commonFunc.sendHtmlContent(email, returnMessage, "["+brand_name+"] Forgot Password", "", function (result) {
                                responses.actionCompleteResponse(res);
                                return;
                            });
                        })
                    });
                });
            }
        });
    }
};
/*
 * -------------------------
 * VIEW ALL FLEETS LOCATION
 * -------------------------
 */
exports.view_all_fleets_location = function (req, res) {

    var access_token = req.body.access_token;
    var address = req.body.address;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "SELECT `fleet_id`,IF (`fleet_thumb_image` IS NULL,'',`fleet_thumb_image`) as `fleet_thumb_image`,IF (`fleet_image` IS NULL,'',`fleet_image`) as `fleet_image`,`status`,`username`," +
                            "`transport_type`,`email`,`phone`,`registration_status`,`latitude`,`is_available`,`longitude`, TIMESTAMPDIFF(SECOND,`location_update_datetime`,NOW()) as `last_updated_location_time` " +
                            "FROM `tb_fleets` " +
                            "WHERE `user_id`=? AND `registration_status` = ? AND `is_deleted` = ? AND `is_active` = ?";
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            commonFunc.checkDispatcherPermissions(result[0].user_id, function (disPerm) {
                                if (disPerm && disPerm[0].view_driver == constants.hasPermissionStatus.NO) {
                                    sql = "SELECT tft.`fleet_id`,IF (tft.`fleet_thumb_image` IS NULL,'',tft.`fleet_thumb_image`) as `fleet_thumb_image`,IF (tft.`fleet_image` IS NULL,'',tft.`fleet_image`) as `fleet_image`," +
                                        "tft.`email`,tft.`phone`,tft.`registration_status`,tft.`is_active`,tft.`is_available`,tft.`username`,tft.`latitude`,tft.`longitude`,tft.`transport_type`,TIMESTAMPDIFF(SECOND," +
                                        "tft.`location_update_datetime`,NOW()) as `last_updated_location_time`, " +
                                        "tft.`fleet_id`,tft.`status` FROM `tb_users` u " +
                                        "LEFT JOIN tb_dispatcher_teams dt ON dt.dispatcher_id = u.user_id LEFT JOIN tb_fleet_teams ftm ON " +
                                        "ftm.team_id = dt.team_id " +
                                        "LEFT JOIN tb_fleets tft ON tft.fleet_id = ftm.fleet_id " +
                                        "WHERE u.`user_id`=? AND tft.`registration_status` = ? AND tft.`is_deleted` = ? AND tft.`is_active` = ?";

                                    step_in(sql, result[0].user_id, constants.userVerificationStatus.VERIFY, constants.userDeleteStatus.NO, constants.userActiveStatus.ACTIVE);
                                }
                                else {
                                    step_in(sql, result[0].dispatcher_user_id, constants.userVerificationStatus.VERIFY, constants.userDeleteStatus.NO, constants.userActiveStatus.ACTIVE);
                                }
                            })
                        } else {
                            step_in(sql, result[0].user_id, constants.userVerificationStatus.VERIFY, constants.userDeleteStatus.NO, constants.userActiveStatus.ACTIVE);
                        }


                        function step_in(sql, user, status, is_deleted, is_active) {

                            connection.query(sql, [user, status, is_deleted, is_active], function (err, result_fleets) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in searching fleet info : ", err, result_fleets);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    result_fleets = commonFunc.sortByKeyAsc(result_fleets, 'last_updated_location_time');
                                    result_fleets = _lodash.uniq(result_fleets, 'fleet_id');
                                    var result_fleets_length = result_fleets.length;
                                    if (result_fleets_length == 0) {
                                        responses.actionCompleteResponse(res);
                                        return;
                                    } else {
                                        var pointArray = [], final = [];
                                        result_fleets.forEach(function (data) {
                                            pointArray.push(data.latitude + "," + data.longitude);
                                            data.last_updated_location_time = parseInt(data.last_updated_location_time);
                                            if (data.last_updated_location_time == '' || data.last_updated_location_time == null) {
                                                data.last_updated_timings = constants.highest.VALUE;
                                                data.last_updated_location_time = 'Not updated.';
                                            } else {
                                                data.last_updated_timings = data.last_updated_location_time;
                                                data.last_updated_location_time = commonFunc.timeDifferenceInWords(data.has_gps_accuracy, data.is_available, data.last_updated_location_time);
                                            }
                                            data.fleet_status_color = constants.fleetStatusColor[data.status][data.is_available];
                                            data.transport_type = constants.reverseTransportType[data.transport_type] || "Not_defined";
                                        });

                                        processData(result_fleets, pointArray.join("|"), address, result_fleets_length);

                                        function processData(result_fleets, points, address, result_fleets_length) {

                                            if (address) {
                                                commonFunc.getEstimateTimeOfArrival(points, address, constants.travellingMode.DRIVING, function (getETAResult) {
                                                    var index = 0;
                                                    result_fleets.forEach(function (data) {
                                                        data.ETA = getETAResult[index];
                                                        data.distancemetres = constants.highest.VALUE;
                                                        if (getETAResult[index]) {
                                                            data.distancemetres = getETAResult[index].distance.metres;
                                                        }
                                                        index++;
                                                        pendingTasks(index, result_fleets_length);
                                                    });
                                                });

                                            } else {
                                                pendingTasks(result_fleets_length, result_fleets_length);
                                            }

                                            function pendingTasks(counter, length) {
                                                if (counter == length) {
                                                    var index = 0;
                                                    result_fleets.forEach(function (data) {
                                                        var date = moment(commonFunc.convertTimeIntoLocal(new Date(), result[0].timezone)).format('YYYY-MM-DD');
                                                        commonFunc.fleetPendingTasks(data.fleet_id, date, data, function (fleetPendingTasksResult) {
                                                            data.pending_tasks = fleetPendingTasksResult.incomplete;
                                                            index++;
                                                            sendResponse(index, length, fleetPendingTasksResult.fleet);
                                                        })
                                                    });
                                                }
                                            }

                                            function sendResponse(counter, length, task) {
                                                final.push(task)
                                                if (counter == length) {
                                                    final = commonFunc.sortByKeyAsc(final, 'distancemetres');
                                                    var response = {
                                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                                        "data": final
                                                    };
                                                    res.send(JSON.stringify(response));
                                                }
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};
/*
 * -------------------------------
 * BLOCK AND UNBLOCK FLEET ACCOUNT
 * -------------------------------
 */
exports.block_and_unblock_fleet_account = function (req, res) {

    var access_token = req.body.access_token;
    var fleet_id = req.body.fleet_id;
    var block_status = req.body.block_status;

    var manvalues = [access_token, fleet_id, block_status];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var dispatcher_id = null;
        async.waterfall([
            function (cb) {
                commonFunc.authenticateUserAccessToken(access_token, function (user) {
                    if (user == 0) {
                        cb(null, {
                            err: 1
                        });
                    } else {
                        cb(null, user);
                    }
                });
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    commonFunc.checkAccountExpiry(user[0].user_id, function (checkExp) {
                        if (checkExp) {
                            cb(null, {
                                err: 2
                            });
                        } else {
                            cb(null, user);
                        }
                    });
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (user[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        commonFunc.authenticateFleetIdAndDispatcherId(fleet_id, dispatcher_id, function (auth) {
                            if (auth == 0) {
                                cb(null, {
                                    err: 3
                                });
                            } else {
                                dispatcher_id = user[0].user_id;
                                user[0].user_id = user[0].dispatcher_user_id;
                                cb(null, user);
                            }
                        });

                    } else {
                        cb(null, user);
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    commonFunc.authenticateDeactivateFleetIdAndUserId(fleet_id, user[0].user_id, function (auth) {
                        if (auth == 0) {
                            cb(null, {
                                err: 3
                            });
                        } else {

                            user[0].fleet_name = auth[0].username;
                            user[0].fleet_phone = auth[0].phone;
                            user[0].fleet_email = auth[0].email;

                            cb(null, user);
                        }
                    });

                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    user[0].brand_name = config.get('projectName');
                    if (user[0].is_whitelabel) {
                        commonFunc.getVersion2(user[0].user_id, function (versions) {
                            if (versions && versions.length) {
                                versions.forEach(function (vr) {
                                    user[0].brand_name = vr.brand_name;
                                })
                                cb(null, user);
                            } else {
                                cb(null, user);
                            }
                        })
                    } else {
                        cb(null, user);
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (user[0].billing_plan == constants.billingPlan.FREE_LIMITED) {

                        commonFunc.getAllFleets(user[0].user_id, function (getAllFleetsResult) {
                            var fleets_count = getAllFleetsResult.length;
                            if (fleets_count >= user[0].num_fleets) {
                                var message = constants.responseMessages.EXCEED_FLEET_COUNT;
                                message = message.replace(/<%FLEET%>/g, user[0].num_fleets + " " + user[0].call_fleet_as);
                                cb(null, {
                                    err: 4,
                                    message: message.replace(/<%FLEET%>/g, user[0].num_fleets + " " + user[0].call_fleet_as),
                                    getAllFleetsResult: getAllFleetsResult
                                });
                            } else {
                                cb(null, user);
                            }
                        })
                    } else {

                        cb(null, user);

                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    user[0].fleet_name = user[0].fleet_name.split(' ')[0];
                    var msg, drivermsg, subject;
                    var new_access_token = md5(new Date());
                    var sql = "UPDATE `tb_fleets` SET `access_token`=?,`is_active`=?,`device_token`=? WHERE `fleet_id`=? LIMIT 1";
                    connection.query(sql, [new_access_token, block_status, null, fleet_id], function (err, result_check) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in updating fleet information  : ", err, result_check);
                            cb(null, {
                                err: 5
                            });
                        } else {
                            if (block_status == 0) {
                                msg = 'This is to inform you that your ' + user[0].brand_name + ' account has been blocked. Please contact ' + user[0].username + ' for further details.<br><br>';
                                drivermsg = 'Hi ' + user[0].fleet_name + ', your ' + user[0].brand_name + ' account has been blocked. Please contact ' + user[0].username + ' for further details.';
                                subject = '' + user[0].brand_name + ' Account Blocked';
                                if (user[0].fleet_phone) {
                                    commonFunc.sendMessageByPlivo(user[0].fleet_phone, drivermsg);
                                }
                            } else {
                                msg = 'This is to inform you that your ' + user[0].brand_name + ' account has been Unblocked. Please use your existing credentials to login to the App.<br><br>';
                                drivermsg = 'Hi ' + user[0].fleet_name + ', your ' + user[0].brand_name + ' account has been Unblocked. Please use your existing credentials to login to the App.';
                                subject = '' + user[0].brand_name + ' Account Unblocked';
                                if (user[0].fleet_phone) {
                                    commonFunc.sendMessageByPlivo(user[0].fleet_phone, drivermsg);
                                }
                            }

                            commonFunc.emailPlainFormatting(user[0].fleet_name, msg, '', '', '', function (returnMessage) {
                                returnMessage = returnMessage.replace(/BRAND_NAME/g,user[0].brand_name);
                                commonFunc.sendHtmlContent(user[0].fleet_email, returnMessage, subject, user[0].email, function (result) {

                                    cb(null, user);
                                });
                            });
                        }
                    });
                }
            }
        ], function (error, user) {
            if (error) {
                responses.authenticationError(res);
                return;
            } else {
                if (user.err) {
                    if (user.err == 1) {
                        responses.authenticationErrorResponse(res);
                        return;
                    } else if (user.err == 2) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else if (user.err == 3) {
                        responses.invalidAccessError(res);
                        return;
                    } else if (user.err == 4) {
                        var response = {
                            "message": user.message,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                fleets: user.getAllFleetsResult
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 5) {
                        responses.sendError(res);
                        return;
                    } else {
                        responses.authenticationError(res);
                        return;
                    }
                } else {
                    responses.actionCompleteResponse(res);
                    return;
                }
            }
        })
    }
};
/*
 * ----------------------
 * DELETE FLEET ACCOUNT
 * ----------------------
 */
exports.delete_fleet_account = function (req, res) {

    var access_token = req.body.access_token;
    var fleet_id = req.body.fleet_id;
    var manvalues = [access_token, fleet_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        var dispatcher_id = null;
        async.waterfall([
            function (cb) {
                commonFunc.authenticateUserAccessToken(access_token, function (user) {
                    if (user == 0) {
                        cb(null, {
                            err: 1
                        });
                    } else {
                        cb(null, user);
                    }
                });
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    commonFunc.checkAccountExpiry(user[0].user_id, function (checkExp) {
                        if (checkExp) {
                            cb(null, {
                                err: 2
                            });
                        } else {
                            cb(null, user);
                        }
                    });
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (user[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                        commonFunc.authenticateFleetIdAndDispatcherId(fleet_id, dispatcher_id, function (auth) {
                            if (auth == 0) {
                                cb(null, {
                                    err: 3
                                });
                            } else {
                                dispatcher_id = user[0].user_id;
                                user[0].user_id = user[0].dispatcher_user_id;
                                cb(null, user);
                            }
                        });

                    } else {
                        cb(null, user);
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    commonFunc.authenticateDeactivateFleetIdAndUserId(fleet_id, user[0].user_id, function (auth) {
                        if (auth == 0) {
                            cb(null, {
                                err: 3
                            });
                        } else {
                            user[0].fleet_name = auth[0].username;
                            user[0].fleet_phone = auth[0].phone;
                            user[0].fleet_email = auth[0].email;
                            user[0].login_id = auth[0].login_id;
                            cb(null, user);
                        }
                    });

                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    user[0].brand_name = config.get('projectName');
                    if (user[0].is_whitelabel) {
                        commonFunc.getVersion2(user[0].user_id, function (versions) {
                            if (versions && versions.length) {
                                versions.forEach(function (vr) {
                                    user[0].brand_name = vr.brand_name;
                                })
                                cb(null, user);
                            } else {
                                cb(null, user);
                            }
                        })
                    } else {
                        cb(null, user);
                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    if (user[0].billing_plan == constants.billingPlan.FREE_LIMITED) {

                        commonFunc.getAllFleets(user[0].user_id, function (getAllFleetsResult) {
                            var fleets_count = getAllFleetsResult.length;
                            if (fleets_count >= user[0].num_fleets) {
                                var message = constants.responseMessages.EXCEED_FLEET_COUNT;
                                message = message.replace(/<%FLEET%>/g, user[0].num_fleets + " " + user[0].call_fleet_as);
                                cb(null, {
                                    err: 4,
                                    message: message.replace(/<%FLEET%>/g, user[0].num_fleets + " " + user[0].call_fleet_as),
                                    getAllFleetsResult: getAllFleetsResult
                                });
                            } else {
                                cb(null, user);
                            }
                        })
                    } else {

                        cb(null, user);

                    }
                }
            },
            function (user, cb) {
                if (user.err) {
                    cb(null, user);
                } else {
                    user[0].fleet_name = user[0].fleet_name.split(' ')[0];
                    var msg, drivermsg, subject;
                    var new_access_token = md5(new Date());

                    var randomStr = 'DELETED' + commonFunc.generateRandomString() + 'tookan';
                    user[0].fleet_email = randomStr + user[0].fleet_email;
                    user[0].login_id = randomStr + user[0].login_id;

                    var sql = "UPDATE `tb_fleets` SET `access_token`=?,`email`=?, `login_id`=?, `is_deleted`=?, `device_token`=? WHERE `fleet_id`=? LIMIT 1";
                    connection.query(sql, [new_access_token, user[0].fleet_email, user[0].login_id, constants.userDeleteStatus.YES, null, fleet_id], function (err, result_check) {
                        if (err) {
                            logging.logDatabaseQueryError("Error in updating fleet information  : ", err, result_check);
                            cb(null, {
                                err: 5
                            });
                        } else {

                            msg = 'This is to inform you that your ' + user[0].brand_name + ' account has been deleted. Please contact ' + user[0].username + ' for further details.<br><br>';
                            drivermsg = 'Hi ' + user[0].fleet_name + ', your ' + user[0].brand_name + ' account has been deleted. Please contact ' + user[0].username + ' for further details.';
                            subject = '' + user[0].brand_name + ' Account Deleted';

                            if (user[0].fleet_phone) {
                                commonFunc.sendMessageByPlivo(user[0].fleet_phone, drivermsg);
                            }

                            commonFunc.emailPlainFormatting(user[0].fleet_name, msg, '', '', '', function (returnMessage) {
                                returnMessage = returnMessage.replace(/BRAND_NAME/g,user[0].brand_name);
                                commonFunc.sendHtmlContent(user[0].fleet_email, returnMessage, subject, user[0].email, function (result) {

                                    cb(null, user);
                                });
                            });
                        }
                    });
                }
            }
        ], function (error, user) {
            if (error) {
                responses.authenticationError(res);
                return;
            } else {
                if (user.err) {
                    if (user.err == 1) {
                        responses.authenticationErrorResponse(res);
                        return;
                    } else if (user.err == 2) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else if (user.err == 3) {
                        responses.invalidAccessError(res);
                        return;
                    } else if (user.err == 4) {
                        var response = {
                            "message": user.message,
                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                            "data": {
                                fleets: user.getAllFleetsResult
                            }
                        };
                        res.send(JSON.stringify(response));
                        return;
                    } else if (user.err == 5) {
                        responses.sendError(res);
                        return;
                    } else {
                        responses.authenticationError(res);
                        return;
                    }
                } else {
                    responses.actionCompleteResponse(res);
                    return;
                }
            }
        })
    }
};
/*
 * ------------------------
 * VIEWED SCREENED STATUS
 * ------------------------
 */
exports.view_screen = function (req, res) {

    var access_token = req.body.access_token;
    var screen_type = req.body.screen_type;
    var screen_value = req.body.screen_value;
    var manvalues = [access_token, screen_type, screen_value];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var first_time_login_keys = result[0].first_time_login_keys;
                        var tab_viewed_keys = result[0].tab_viewed_keys;
                        if (screen_type == 1) {
                            first_time_login_keys = commonFunc.setCharAt(result[0].first_time_login_keys, screen_value - 1, 1);
                        }
                        else if (screen_type == 2) {
                            tab_viewed_keys = commonFunc.setCharAt(result[0].tab_viewed_keys, screen_value - 1, 1);
                        }
                        var sql = "UPDATE `tb_users` SET `first_time_login_keys`=?,`tab_viewed_keys`=? WHERE `access_token`=? LIMIT 1";
                        connection.query(sql, [first_time_login_keys, tab_viewed_keys, access_token], function (err, result_update) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating user info : ", err, result_update);
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                            }
                        });
                    }
                });
            }
        });
    }
};
/*
 * -----------------------------
 * EDIT FLEET PROFILE INFORMATION
 * -----------------------------
 */
exports.edit_fleet_profile_information = function (req, res) {
    var access_token = req.body.access_token;
    var name = req.body.name;
    var timezone = req.body.timezone;
    var fleet_image = req.files.fleet_image;
    var transport_type = req.body.transport_type;
    var transport_desc = req.body.transport_desc;
    var license = req.body.license;
    var color = req.body.color;
    var phone = req.body.phone;
    var team_ids = req.body.team_ids
    var fleet_id = req.body.fleet_id
    var image_flag = req.body.image_flag
    var first_name = req.body.first_name
    var last_name = req.body.last_name
    var password = req.body.password
    var manvalues = [access_token, fleet_id, image_flag];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id, dispatcher_id, dispatcher_user_id = result[0].dispatcher_user_id;
                first_name = (first_name == "null" ? '' : first_name)
                transport_type = (transport_type == "null" ? '' : transport_type)
                transport_desc = (transport_desc == "null" ? '' : transport_desc)
                license = (license == "null" ? '' : license)
                color = (color == "null" ? '' : color)
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            dispatcher_id = user_id;
                            user_id = dispatcher_user_id;
                            commonFunc.authenticateFleetIdAndDispatcherId(fleet_id, dispatcher_id, function (authenticateFleetIdAndUserIdResult) {
                                if (authenticateFleetIdAndUserIdResult == 0) {
                                    responses.invalidAccessError(res);
                                } else {
                                    step_in(fleet_id, user_id);
                                }
                            });
                        } else {
                            step_in(fleet_id, user_id);
                        }

                        function step_in(fleet_id, user_id) {

                            if (image_flag == 0) {
                                var sql = "UPDATE `tb_fleets` SET `first_name`=?,`last_name`=?,`username`=?,`phone`=?";
                                if (password) {
                                    sql += ",`password` = '" + md5(password) + "' ";
                                }
                                sql += ",`timezone`=?,`transport_type`=?,`transport_desc`=?,`license`=?,`color`=? WHERE `fleet_id`=? LIMIT 1";
                                connection.query(sql, [first_name, last_name, name, phone, timezone, transport_type, transport_desc, license, color, fleet_id], function (err, result_insert) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in updating fleet info : ", err, result_insert);
                                        responses.sendError(res);
                                        return;
                                    } else {
                                        var sql = "DELETE FROM `tb_fleet_teams` WHERE `fleet_id`=? AND `user_id`=?";
                                        connection.query(sql, [fleet_id, user_id], function (err, result_delete) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in deleting tb_fleet_teams info : ", err, result_delete);
                                                responses.sendError(res);
                                                return;
                                            } else {
                                                var teams = team_ids.split(','), teams_ids_array = [];
                                                teams_ids_array.push(teams[0]); // ONLY ONE TEAM ALLOWED FOR FLEET
                                                var teams_ids_array_len = teams_ids_array.length, index = 0;
                                                (function sendResponse() {
                                                    var sql = "INSERT INTO `tb_fleet_teams` (`team_id`,`user_id`,`fleet_id`) VALUES (?,?,?)";
                                                    connection.query(sql, [teams_ids_array[index], user_id, fleet_id], function (err, result_insert_fleet_teams) {
                                                        if (err) {
                                                            logging.logDatabaseQueryError("Error in inserting fleet teams info : ", err, result_insert_fleet_teams);
                                                        }
                                                        index++;
                                                        if (index < teams_ids_array_len) {
                                                            sendResponse();
                                                        }
                                                        else {
                                                            responses.actionCompleteResponse(res);
                                                            return;
                                                        }
                                                    });
                                                })();
                                            }
                                        });
                                    }
                                });
                            } else if (image_flag == 1 && req.files && req.files.fleet_image) {
                                commonFunc.uploadImageToS3Bucket(req.files.fleet_image, config.get('s3BucketCredentials.folder.fleetProfileImages'), function (result_fleet_image) {
                                    if (result_fleet_image == 0) {
                                        responses.uploadError(res);
                                    } else {
                                        commonFunc.uploadThumbImageToS3Bucket(req.files.fleet_image, config.get('s3BucketCredentials.folder.fleetProfileThumbImages'), function (result_fleet_thumb_image) {
                                            if (result_fleet_thumb_image == 0) {
                                                responses.uploadError(res);
                                            } else {
                                                var fleetImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.fleetProfileImages') + '/' + result_fleet_image;
                                                var fleetThumbImage = config.get('s3BucketCredentials.s3URL') + '/' + config.get('s3BucketCredentials.folder.fleetProfileThumbImages') + '/' + result_fleet_thumb_image;

                                                var sql = "UPDATE `tb_fleets` SET `first_name`=?,`last_name`=?,`fleet_image`=?,`fleet_thumb_image`=?,`username`=?,`phone`=? ";
                                                if (password) {
                                                    sql += ",`password`= '" + md5(password) + "' "
                                                }
                                                sql += ",`timezone`=?,`transport_type`=?,`transport_desc`=?,`license`=?,`color`=? " +
                                                    " WHERE `fleet_id`=? LIMIT 1 ";
                                                connection.query(sql, [first_name, last_name, fleetImage, fleetThumbImage, name, phone, timezone, transport_type, transport_desc, license, color, fleet_id], function (err, result_insert) {
                                                    if (err) {
                                                        logging.logDatabaseQueryError("Error in updating fleet info : ", err, result_insert);
                                                        responses.sendError(res);
                                                        return;
                                                    } else {
                                                        var sql = "DELETE FROM `tb_fleet_teams` WHERE `fleet_id`=? AND `user_id`=?";
                                                        connection.query(sql, [fleet_id, result[0].user_id], function (err, result_delete) {
                                                            if (err) {
                                                                logging.logDatabaseQueryError("Error in deleting tb_fleet_teams info : ", err, result_delete);
                                                                responses.sendError(res);
                                                                return;
                                                            } else {
                                                                var teams = team_ids.split(','), teams_ids_array = [];
                                                                teams_ids_array.push(teams[0]); // ONLY ONE TEAM ALLOWED FOR FLEET
                                                                var teams_ids_array_len = teams_ids_array.length, index = 0;
                                                                (function sendResponse() {
                                                                    var sql = "INSERT INTO `tb_fleet_teams` (`team_id`,`user_id`,`fleet_id`) VALUES (?,?,?)";
                                                                    connection.query(sql, [teams_ids_array[index], user_id, fleet_id], function (err, result_insert_fleet_teams) {
                                                                        if (err) {
                                                                            logging.logDatabaseQueryError("Error in inserting fleet teams info : ", err, result_insert_fleet_teams);
                                                                        }
                                                                        index++;
                                                                        if (index < teams_ids_array_len) {
                                                                            sendResponse();
                                                                        }
                                                                        else {
                                                                            responses.actionCompleteResponse(res);
                                                                        }
                                                                    });
                                                                })();
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    }
};
/*
 * ------------------------------
 * UPDATE TASK VIA DASHBOARD
 * ------------------------------
 */
exports.update_task_via_dashboard = function (req, res) {

    var access_token = req.body.access_token;
    var job_status = req.body.job_status;
    var job_id = req.body.job_id;
    var reason = req.body.reason;
    if (typeof reason === "undefined" || reason === "") {
        reason = '';
    }
    var manvalues = [access_token, job_id, job_status];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var user_id = result[0].user_id, index = 0, allResponses = [];
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            user_id = result[0].dispatcher_user_id;
                        }
                        checkTaskAndChangeStatus(index);
                        function changeIndex() {
                            index++;
                            return checkTaskAndChangeStatus(index);
                        }

                        var tasks = job_id.toString().split(','), num_tasks = tasks.length;

                        function checkTaskAndChangeStatus(item) {
                            if (item > num_tasks - 1) {
                                res.send(JSON.stringify(allResponses.length > 1 ? allResponses : allResponses[0]));
                            } else {
                                commonFunc.delay2(item, function (i) {
                                    (function (i) {
                                        commonFunc.authenticateUserIdAndJobId(user_id, tasks[i], function (jobs) {
                                            if (jobs == 0) {
                                                var response = {
                                                    "message": constants.responseMessages.JOB_NOT_MAPPED_WITH_YOU,
                                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                    "data": {}
                                                };
                                                allResponses.push(response);
                                                changeIndex();
                                            } else {
                                                var sql = "UPDATE `tb_jobs` SET `job_status`=? ";
                                                if (job_status == constants.jobStatus.UNASSIGNED) {
                                                    sql += ",fleet_id = null ";
                                                }
                                                if (job_status == constants.jobStatus.ARRIVED) {
                                                    sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN NOW() ELSE `acknowledged_datetime` END ";
                                                    sql += ",`started_datetime` = CASE WHEN `started_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `started_datetime` END ";
                                                    sql += ",`arrived_datetime` = NOW() ";
                                                }
                                                if (job_status == constants.jobStatus.STARTED) {
                                                    // SCHEDULE FLEET NOTIFICATION
                                                    cron.scheduleFleetNotification(user_id, jobs[0].job_id, jobs[0].fleet_id, jobs[0].job_time, jobs[0].job_time_utc, jobs[0].job_type, jobs[0].timezone)
                                                    sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN NOW() ELSE `acknowledged_datetime` END ";
                                                    sql += ",`started_datetime` = NOW() ";
                                                }
                                                if ((job_status == constants.jobStatus.ENDED) || (job_status == constants.jobStatus.FAILED) || (job_status == constants.jobStatus.CANCEL) || (job_status == constants.jobStatus.DECLINE)) {
                                                    // DeleteScheduleFleetNotificationAfterTaskStart
                                                    cron.deleteScheduleFleetNotificationAfterTaskStart(job_id, jobs[0].fleet_id, user_id)
                                                    sql += ",`acknowledged_datetime` = CASE WHEN `acknowledged_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `acknowledged_datetime` END ";
                                                    sql += ",`started_datetime` = CASE WHEN `started_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `started_datetime` END ";
                                                    sql += ",`arrived_datetime` = CASE WHEN `arrived_datetime` = '0000-00-00 00:00:00' THEN  NOW() ELSE `arrived_datetime` END ";
                                                    sql += ",`completed_datetime` = NOW() ";
                                                }
                                                sql += " WHERE `job_id`=? LIMIT 1";
                                                connection.query(sql, [job_status, tasks[i]], function (err, task_update) {
                                                    if (err) {
                                                        logging.logDatabaseQueryError("Error in updating jobs info : ", err, task_update);
                                                        responses.sendError(res);
                                                    } else {
                                                        if ((jobs[0].fleet_id) && (job_status != constants.jobStatus.UNASSIGNED)) {
                                                            // Send a push notification to the fleet that a
                                                            // job has been updated
                                                            var message_fleet = 'A task has been updated.';
                                                            var driverOfflineMessage = 'Hi [Fleet name], a task has been updated. Kindly log into app to see the details.';
                                                            var payload_fleet = {
                                                                flag: constants.notificationFlags.TASK_UPDATE,
                                                                message: 'A task has been updated.',
                                                                job_id: job_id,
                                                                job_type: jobs[0].job_type,
                                                                cust_name: jobs[0].job_type == constants.jobType.PICKUP ? jobs[0].job_pickup_name : jobs[0].customer_username,
                                                                start_address: jobs[0].job_pickup_address,
                                                                end_address: jobs[0].customer_address,
                                                                end_time: jobs[0].job_delivery_datetime,
                                                                start_time: jobs[0].job_pickup_datetime,
                                                                accept_button: 0,
                                                                d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                                            };
                                                            commonFunc.sendNotification(jobs[0].fleet_id, message_fleet, false, payload_fleet, driverOfflineMessage);
                                                        }
                                                        if (job_status == constants.jobStatus.ENDED) {
                                                            commonFunc.changeFleetStatus(jobs[0].fleet_id, constants.userFreeStatus.FREE);
                                                        }

                                                        var comment = "Status updated from " + constants.jobStatusValue[parseInt(jobs[0].job_status)] + " to " + constants.jobStatusValue[parseInt(job_status)]
                                                        if (reason) {
                                                            reason = reason ? ("#-#" + reason) : '';
                                                            comment = comment + reason;
                                                        }
                                                        commonFunc.setTaskHistory(null, constants.taskHistoryType.STATE_CHANGED, tasks[i], comment);

                                                        if (job_status == constants.jobStatus.STARTED) { // WHEN JOB STARTS
                                                            commonFunc.sendTemplateEmailAndSMS(jobs[0].user_id, 'AGENT_STARTED', tasks[i]);
                                                        } else if (job_status == constants.jobStatus.ARRIVED) { // WHEN JOB ARRIVED
                                                            commonFunc.sendTemplateEmailAndSMS(jobs[0].user_id, 'AGENT_ARRIVED', tasks[i]);
                                                        } else if (job_status == constants.jobStatus.ENDED) { // WHEN JOB COMPLETED
                                                            commonFunc.sendTemplateEmailAndSMS(jobs[0].user_id, 'SUCCESSFUL', tasks[i]);
                                                        } else if (job_status == constants.jobStatus.ACCEPTED) { // WHEN JOB ACCEPTED
                                                        } else if (job_status == constants.jobStatus.DECLINE) { // WHEN JOB DECLINE
                                                        } else if (job_status == constants.jobStatus.FAILED) { // WHEN JOB FAILED
                                                            commonFunc.sendTemplateEmailAndSMS(jobs[0].user_id, 'FAILED', tasks[i]);
                                                        } else if (job_status == constants.jobStatus.PARTIAL) { // WHEN JOB PARTIALLY COMPLETED
                                                        } else if (job_status == constants.jobStatus.CANCEL) { // WHEN JOB CANCELLED
                                                        }
                                                        var response = {
                                                            "message": constants.responseMessages.ACTION_COMPLETE,
                                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                                            "data": {}
                                                        };
                                                        allResponses.push(response);
                                                        changeIndex();
                                                    }
                                                });
                                            }
                                        });
                                    })(i);
                                });
                            }
                        }
                    }
                });
            }
        });
    }
}
/*
 * ---------------------------
 * ENABLE OR DISABLE TEMPLATE
 * ---------------------------
 */
exports.enable_or_disable_notification = function (req, res) {

    var access_token = req.body.access_token;
    var status = req.body.status;
    var sms_status = req.body.sms_status;
    var hook_status = req.body.hook_status;
    var template_key = req.body.template_key;
    var layout_type = req.body.layout_type;
    if (typeof(sms_status) === "undefined" || sms_status === "") {
        sms_status = status;
    }
    if (typeof(hook_status) === "undefined" || hook_status === "") {
        hook_status = status;
    }
    var manvalues = [access_token, status, template_key, layout_type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var user_id = result[0].user_id;
                        var sql = "UPDATE `tb_templates` SET `email_enabled`=?,`sms_enabled`=?,`hook_enabled`=? " +
                            " WHERE `template_key`=? AND `user_id`=? AND `layout_type`=? LIMIT 1";
                        connection.query(sql, [status, sms_status, hook_status, template_key, user_id, layout_type], function (err, result_update) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in updating template info : ", err, result_update);
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                            }
                        });
                    }
                });
            }
        });
    }
};
/*
 * -----------------------------------------------------
 * STEP IN (GET ALL DETAILS OF JOBS AND FLEETS WIT TEAM)
 * -----------------------------------------------------
 */
exports.get_jobs_and_fleet_details = function (req, res) {

    var access_token = req.body.access_token;
    var user_id = req.body.user_id;
    var team_id = req.body.team_id;
    var date = req.body.date;
    var manvalues = [access_token, date, team_id, user_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessTokenAndUserId(access_token, user_id, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id, layout_type = result[0].layout_type, job_type;
                if (layout_type == constants.layoutType.PICKUP_AND_DELIVERY) {
                    job_type = constants.jobType.PICKUP + "," + constants.jobType.DELIVERY;
                } else if (layout_type == constants.layoutType.APPOINTMENT) {
                    job_type = constants.jobType.APPOINTMENT;
                } else if (layout_type == constants.layoutType.FOS) {
                    job_type = constants.jobType.FOS;
                }
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                    } else {
                        var teams = {}, sql, is_dispatcher, view_task_perm, total_fleets = 0, active_fleets = 0, unassigned_fleets = 0, unavailable_fleets = 0;
                        teams[team_id] = {};
                        if (team_id == 0) {
                            is_dispatcher = 0;
                            sql = "SELECT `fleet_id` FROM `tb_fleets` " +
                                "WHERE `user_id`=" + user_id + " AND `is_deleted`=" + constants.userDeleteStatus.NO + " " +
                                "AND `registration_status`=" + constants.userVerificationStatus.VERIFY + " AND `is_active`=" + constants.userVerificationStatus.VERIFY + "";
                            if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                                user_id = result[0].dispatcher_user_id;
                                is_dispatcher = 1;
                            }
                            processData(sql, user_id, team_id, is_dispatcher, view_task_perm);
                        }
                        else {
                            is_dispatcher = 0;
                            view_task_perm = 0;
                            sql = "SELECT `fleet_id` FROM `tb_fleet_teams` " +
                                "WHERE `team_id`= " + team_id + " ";
                            if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                                commonFunc.checkDispatcherPermissions(result[0].user_id, function (disPerm) {
                                    if (disPerm && disPerm[0].view_task == constants.hasPermissionStatus.NO) {
                                    } else {
                                        view_task_perm = 1;
                                    }
                                    is_dispatcher = 1;
                                    user_id = result[0].dispatcher_user_id;
                                    sql = "SELECT fleets.`fleet_id` FROM `tb_dispatcher_teams` dis_team " +
                                        "INNER JOIN `tb_fleet_teams` fleets ON fleets.team_id = dis_team.team_id " +
                                        "WHERE dis_team.`team_id`= " + team_id + " ";
                                    processData(sql, user_id, team_id, is_dispatcher, view_task_perm);
                                });
                            } else {
                                processData(sql, user_id, team_id, is_dispatcher, view_task_perm);
                            }
                        }

                        function processData(sql, user_id, team_id, is_dispatcher, view_task_perm) {

                            if (team_id == 0) {
                                if (is_dispatcher == 1) {
                                    responses.noDataFoundError(res);
                                } else {
                                    getDetails(sql, view_task_perm);
                                }
                            } else {
                                commonFunc.authenticateUserIdAndTeamId(user_id, team_id, function (authenticateUserIdAndTeamIdResult) {
                                    if (authenticateUserIdAndTeamIdResult == 0) {
                                        responses.invalidAccessError(res);
                                    } else {
                                        getDetails(sql, view_task_perm);
                                    }
                                });
                            }
                            function getDetails(sql, view_task_perm) {
                                connection.query(sql, function (err, fleet_teams_result) {
                                    if (err) {
                                        console.log(err)
                                        responses.sendError(res);
                                    }
                                    var fleetArray = [], fleet_ids = 0;
                                    if (fleet_teams_result.length > 0) {
                                        fleet_teams_result.forEach(function (fleets) {
                                            fleetArray.push(fleets.fleet_id);
                                        })
                                        fleet_ids = fleetArray.join(',');
                                    }
                                    async.parallel([
                                            function (callback) {

                                                var fleetDataQuery = "SELECT fleets.`fleet_id`,IF (fleets.`fleet_thumb_image` IS NULL,'',fleets.`fleet_thumb_image`) as `fleet_thumb_image`," +
                                                    " IF (fleets.`fleet_image` IS NULL,'',fleets.`fleet_image`) as `fleet_image`,fleets.`status`,fleets.`username`,fleets.`is_active`,fleets.`is_deleted`," +
                                                    " fleets.`email`,fleets.`phone`,fleets.`registration_status`,fleets.`latitude`,fleets.`is_available`,fleets.`longitude`, TIMESTAMPDIFF(SECOND,fleets.`location_update_datetime`,NOW()) as `last_updated_location_time`, " +
                                                    " fleets.`battery_level`,fleets.`has_gps_accuracy`,fleets.`has_network`,fleets.`device_desc`,fleets.`has_mock_loc`,fleets.`device_os`,fleets.`device_type`, " +
                                                    " fleets.`total_rating`,fleets.`total_rated_tasks`,fleets.`store_version` " +
                                                    " FROM `tb_fleets` fleets  " +
                                                    " WHERE  fleets.`user_id`=? AND fleets.`fleet_id` IN (" + fleet_ids + ") ";
                                                connection.query(fleetDataQuery, [user_id], function (err, fleet_data_result) {
                                                    if (err) {
                                                        console.log(err)
                                                        responses.sendError(res);
                                                        return;
                                                    }
                                                    var fleetArray = {};
                                                    if (fleet_data_result.length > 0) {
                                                        total_fleets = fleet_data_result.length;
                                                        fleet_data_result.forEach(function (fleets) {
                                                            var fleet_fields = {};
                                                            if (fleets.is_deleted == constants.userDeleteStatus.NO && fleets.registration_status == constants.userVerificationStatus.VERIFY
                                                                && fleets.is_active == constants.userVerificationStatus.VERIFY) {
                                                                fleetArray[fleets.fleet_id] = {};
                                                                fleet_fields.fleet_id = fleets.fleet_id;
                                                                fleet_fields.fleet_name = fleets.username;
                                                                fleet_fields.fleet_thumb_image = fleets.fleet_thumb_image;
                                                                fleet_fields.latitude = fleets.latitude;
                                                                fleet_fields.longitude = fleets.longitude;
                                                                fleet_fields.status = fleets.status;
                                                                fleet_fields.is_available = fleets.is_available;
                                                                fleet_fields.is_active = fleets.is_active;
                                                                fleet_fields.battery_level = fleets.battery_level;
                                                                fleet_fields.device_type = fleets.device_type;
                                                                fleet_fields.store_version = fleets.store_version;
                                                                fleet_fields.avg_cust_rating = commonFunc.calculateAvgRating(fleets.total_rating, fleets.total_rated_tasks);
                                                                fleet_fields.fleet_status_color = constants.fleetStatusColor[fleets.status][fleets.is_available];
                                                                if (fleets.status == constants.userFreeStatus.BUSY) {
                                                                    active_fleets++;
                                                                }
                                                                if (fleets.status == constants.userFreeStatus.FREE && fleets.is_available == constants.availableStatus.AVAILABLE) {
                                                                    unassigned_fleets++;
                                                                }
                                                                if (fleets.is_available == constants.availableStatus.NOT_AVAILABLE) {
                                                                    unavailable_fleets++;
                                                                }
                                                                fleets.last_updated_location_time = parseInt(fleets.last_updated_location_time);
                                                                if (fleets.last_updated_location_time.toString() == "NaN") {
                                                                    fleet_fields.last_updated_timings = constants.highest.VALUE;
                                                                    fleet_fields.last_updated_location_time = 'Not updated.';
                                                                } else {
                                                                    fleet_fields.last_updated_timings = fleets.last_updated_location_time;
                                                                    fleet_fields.last_updated_location_time = commonFunc.timeDifferenceInWords(fleets.has_gps_accuracy, fleets.is_available, fleets.last_updated_location_time);
                                                                }
                                                                fleetArray[fleets.fleet_id] = fleet_fields
                                                            }
                                                        });
                                                        callback(null, fleetArray)
                                                    } else {
                                                        callback(null, [])
                                                    }
                                                })
                                            },
                                            function (callback) {
                                                var fleetJobDataQuery = "SELECT fleet.`username` as `fleet_name`," +
                                                    "job.`customer_username`,job.`team_id`,job.`job_time`,job.`is_routed`," +
                                                    "job.pickup_delivery_relationship,job.fleet_id,job.job_pickup_name,job.job_latitude,job.job_pickup_phone,job.job_longitude,job.job_pickup_name," +
                                                    "job.job_address,job.job_status,job.job_pickup_datetime,job.job_description,job.job_pickup_latitude,job.job_type," +
                                                    "job.job_pickup_longitude,job.job_pickup_address,job.job_pickup_email,job.job_id,job.job_delivery_datetime,job.auto_assignment " +
                                                    "FROM `tb_jobs` job " +
                                                    "LEFT JOIN `tb_fleets` fleet ON fleet.`fleet_id`=job.`fleet_id` " +
                                                    "WHERE  job.`job_type` IN (" + job_type + ") AND job.`job_status` <> ? AND DATE(job.`job_time`) = ? AND job.`user_id` = ? ";
                                                if (team_id != 0) {
                                                    if (is_dispatcher) {
                                                        if (view_task_perm) {
                                                            fleetJobDataQuery += " AND (job.`team_id` IN (" + team_id + ") || ( job.`team_id` = 0 AND job.`job_status` IN (" + constants.jobStatus.UNASSIGNED + "))) ";
                                                        } else {
                                                            fleetJobDataQuery += " AND job.`team_id` IN (" + team_id + ") ";
                                                        }
                                                    } else {
                                                        fleetJobDataQuery += " AND (job.`team_id` IN (" + team_id + ") || ( job.`team_id` = 0 AND job.`job_status` IN (" + constants.jobStatus.UNASSIGNED + "))) ";
                                                    }
                                                }
                                                connection.query(fleetJobDataQuery, [constants.jobStatus.DELETED, date, user_id], function (err, fleet_job_data_result) {
                                                    if (err) {
                                                        console.log(err)
                                                        responses.sendError(res);
                                                        return;
                                                    }
                                                    var jobArray = {};
                                                    if (fleet_job_data_result.length > 0) {
                                                        fleet_job_data_result.forEach(function (jobs) {
                                                            if (jobs.job_pickup_datetime && jobs.job_pickup_datetime != null && jobs.job_pickup_datetime != '0000-00-00 00:00:00') {
                                                                jobs.job_pickup_datetime = jobs.job_pickup_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                            }
                                                            if (jobs.job_delivery_datetime && jobs.job_delivery_datetime != null && jobs.job_delivery_datetime != '0000-00-00 00:00:00') {
                                                                jobs.job_delivery_datetime = jobs.job_delivery_datetime.toISOString().replace(/T/, ' ').replace(/\..+/, '');
                                                            }
                                                            var jobs_fields = {};
                                                            jobArray[jobs.job_id] = {};
                                                            jobs_fields.job_id = jobs.job_id;
                                                            jobs_fields.team_id = jobs.team_id;
                                                            jobs_fields.auto_assignment = jobs.auto_assignment;
                                                            jobs_fields.job_status = jobs.job_status;
                                                            jobs_fields.job_type = jobs.job_type;
                                                            jobs_fields.fleet_id = jobs.fleet_id;
                                                            jobs_fields.fleet_name = jobs.fleet_name;
                                                            jobs_fields.job_pickup_name = jobs.job_pickup_name;
                                                            jobs_fields.job_latitude = jobs.job_latitude;
                                                            jobs_fields.job_longitude = jobs.job_longitude;
                                                            jobs_fields.job_address = jobs.job_address;
                                                            jobs_fields.job_status = jobs.job_status;
                                                            jobs_fields.is_routed = jobs.is_routed;
                                                            jobs_fields.job_pickup_latitude = jobs.job_pickup_latitude;
                                                            jobs_fields.job_pickup_longitude = jobs.job_pickup_longitude;
                                                            jobs_fields.job_pickup_address = jobs.job_pickup_address;
                                                            jobs_fields.pickup_delivery_relationship = jobs.pickup_delivery_relationship;
                                                            jobs_fields.job_delivery_datetime = jobs.job_delivery_datetime;
                                                            jobs_fields.job_pickup_datetime = jobs.job_pickup_datetime;
                                                            jobs_fields.job_pickup_name = jobs.job_pickup_name;
                                                            jobs_fields.customer_username = jobs.customer_username == "dummy" ? "" : jobs.customer_username;
                                                            if (jobs.job_time) jobs_fields.job_time = new Date(jobs.job_time).getTime();
                                                            jobArray[jobs.job_id] = jobs_fields
                                                        });
                                                        callback(null, jobArray);
                                                    } else {
                                                        callback(null, []);
                                                    }
                                                });
                                            },
                                            function (callback) {
                                                if (fleet_ids) {
                                                    mongo.getRoutedTasksOfFleet({
                                                        date: date,
                                                        layout_type: layout_type,
                                                        fleet_id: fleet_ids.split(',')
                                                    }, function (routedData) {
                                                        callback(null, routedData);
                                                    })
                                                } else {
                                                    callback(null, {});
                                                }
                                            }],
                                        function (err, finalResults) {
                                            var rs = {
                                                "fleets": finalResults[0],
                                                "jobs": finalResults[1],
                                                "route": finalResults[2]
                                            };
                                            teams[team_id] = rs;
                                            var animate = 1;
                                            if (active_fleets > 50) {
                                                animate = 0;
                                            }
                                            var response = {
                                                "message": constants.responseMessages.ACTION_COMPLETE,
                                                "status": constants.responseFlags.ACTION_COMPLETE,
                                                "data": {
                                                    "teams": teams,
                                                    "animate": animate,
                                                    "fleet_data": {
                                                        "total_fleets": total_fleets,
                                                        "active_fleets": active_fleets,
                                                        "unassigned_fleets": unassigned_fleets,
                                                        "unavailable_fleets": unavailable_fleets
                                                    }
                                                }
                                            };
                                            res.send(JSON.stringify(response));
                                        });
                                })
                            }
                        }
                    }
                });
            }
        });
    }
};
/*
 * -----------------------
 * CANCEL ONBOARDING
 * -----------------------
 */
exports.cancel_onboarding = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var sql = "UPDATE `tb_users` SET `is_first_time_login`=0 WHERE `access_token`=? LIMIT 1";
                        connection.query(sql, [access_token], function (err, result_check) {
                            if (err) {
                                logging.logDatabaseQueryError("Error in cancel_onboarding : ", err, result_check);
                                responses.sendError(res);
                                return;
                            } else {
                                responses.actionCompleteResponse(res);
                            }
                        });
                    }
                });
            }
        });
    }
};
/*
 * -----------------------
 * Change Fleet Availability
 * -----------------------
 */
exports.change_fleet_availability_via_dashboard = function (req, res) {

    var access_token = req.body.access_token;
    var fleet_id = req.body.fleet_id;
    var is_available = req.body.is_available;
    var manvalues = [access_token, fleet_id, is_available];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var user_id = result[0].user_id;
                if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                    user_id = result[0].dispatcher_user_id;
                }
                commonFunc.checkAccountExpiry(user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        commonFunc.authenticateFleetIdAndUserId(fleet_id, user_id, function (authenticateFleetIdAndUserIdResult) {
                            if (authenticateFleetIdAndUserIdResult == 0) {
                                responses.invalidAccessError(res);
                            } else {
                                var sql = "UPDATE `tb_fleets` SET `status`=?,`is_available`=? WHERE `fleet_id`=? LIMIT 1";
                                connection.query(sql, [constants.userFreeStatus.FREE, is_available, fleet_id], function (err, result_check) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in updating fleet is_avalilable information  : ", err, result_check);
                                    } else {
                                        // Send a push notification to the fleet that a
                                        var message_fleet = 'You have been put off-duty by the manager.';
                                        var driverOfflineMessage = 'Hi [Fleet name], You have been put off-duty by the manager. Kindly log into to see the details.';
                                        var payload_fleet = {
                                            flag: constants.notificationFlags.MARK_OFFLINE,
                                            message: message_fleet,
                                            is_available: is_available,
                                            d: moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
                                        };
                                        commonFunc.sendNotification(fleet_id, message_fleet, false, payload_fleet, driverOfflineMessage);
                                        responses.actionCompleteResponse(res);
                                        return;
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};