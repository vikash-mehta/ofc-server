UPDATE tb_templates T1 INNER JOIN tb_templates_old T2 ON T1.user_id = T2.user_id AND T1.layout_type = T2.layout_type SET T1.sms_text = T2.sms_text,T1.email_subject = T2.email_subject,T1.email_text = T2.email_text,T1.sms_enabled = T2.sms_enabled,T1.email_enabled = T2.email_enabled WHERE T1.template_key =  "REQUEST_RECEIVED" AND T2.template_key = "ORDER_RECEIVED"

UPDATE tb_templates T1 INNER JOIN tb_templates_old T2 ON T1.user_id = T2.user_id AND T1.layout_type = T2.layout_type SET T1.sms_text = T2.sms_text,T1.email_subject = T2.email_subject,T1.email_text = T2.email_text,T1.sms_enabled = T2.sms_enabled,T1.email_enabled = T2.email_enabled WHERE T1.template_key =  "AGENT_STARTED" AND T2.template_key =  "DELIVERY_INITIATED"

UPDATE tb_templates T1 INNER JOIN tb_templates_old T2 ON T1.user_id = T2.user_id AND T1.layout_type = T2.layout_type SET T1.sms_text = T2.sms_text,T1.email_subject = T2.email_subject,T1.email_text = T2.email_text,T1.sms_enabled = T2.sms_enabled,T1.email_enabled = T2.email_enabled WHERE T1.template_key =  "AGENT_ARRIVED" AND T2.template_key =  "DRIVER_ARRIVED"

UPDATE tb_templates T1 INNER JOIN tb_templates_old T2 ON T1.user_id = T2.user_id AND T1.layout_type = T2.layout_type SET T1.sms_text = T2.sms_text,T1.email_subject = T2.email_subject,T1.email_text = T2.email_text,T1.sms_enabled = T2.sms_enabled,T1.email_enabled = T2.email_enabled WHERE T1.template_key =  "SUCCESSFUL" AND T2.template_key =  "SUCCESSFUL_DELIVERY"

UPDATE tb_templates T1 INNER JOIN tb_templates_old T2 ON T1.user_id = T2.user_id AND T1.layout_type = T2.layout_type SET T1.sms_text = T2.sms_text,T1.email_subject = T2.email_subject,T1.email_text = T2.email_text,T1.sms_enabled = T2.sms_enabled,T1.email_enabled = T2.email_enabled WHERE T1.template_key =  "FAILED" AND T2.template_key =  "FAILED_DELIVERY"

