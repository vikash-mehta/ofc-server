/**
 * The node-module to hold the constants for the server
 */
var constants = require('./constants');
var myContext = this;

function define(obj, name, value) {
    Object.defineProperty(obj, name, {
        value: value,
        enumerable: true,
        writable: false,
        configurable: true
    });
}

var debugging = false;

exports.responseFlags = {};
exports.responseMessages = {};

define(exports.responseMessages, 'PARAMETER_MISSING', 'Insufficient information was supplied. Please check and try again.');
define(exports.responseMessages, 'FLEET_NOT_AVAILABLE', 'Agent not available at specified time.');
define(exports.responseMessages, 'FLEET_EMAIL_ALREADY_EXISTS_WITH_YOU', 'This email ID is already exists in your account.');
define(exports.responseMessages, 'LOGIN_ID_ALREADY_EXISTS', 'This username is already registered. Please try with different Username.');
define(exports.responseMessages, 'FLEET_EMAIL_ALREADY_EXISTS', 'This email ID is already registered. Please try with different ID.');
define(exports.responseMessages, 'FLEET_EMAIL_NOT_INVITED_BY_ANYONE', 'You are not invited by anyone yet.');
define(exports.responseMessages, 'JOB_NOT_MAPPED_WITH_YOU', 'This task is no longer available.');
define(exports.responseMessages, 'REGISTRATION_SUCCESSFUL', 'Please check you email for verification.');
define(exports.responseMessages, 'ACCOUNT_NOT_VERIFIED', 'You have not verified your account yet.');
define(exports.responseMessages, 'ACCOUNT_NOT_REGISTERED_PROPERLY', 'You have not registered properly. Please signup again.');
define(exports.responseMessages, 'ACCOUNT_EXPIRE', 'Your account has been expired. Please choose a plan in billings page to continue.');
define(exports.responseMessages, 'INVALID_ACCESS_TOKEN', 'Session expired. Please logout and login again.');
define(exports.responseMessages, 'INVALID_URL', 'Oops!! URL not found.');
define(exports.responseMessages, 'INVALID_USERNAME', 'This username is not registered with us.');
define(exports.responseMessages, 'WRONG_PASSWORD', 'Incorrect Password.');
define(exports.responseMessages, 'CURRENT_PASSWORD_INCORRECT', 'Incorrect Current Password.');
define(exports.responseMessages, 'INCORRECT_PASSWORD', 'Incorrect Password.');
define(exports.responseMessages, 'ACTION_COMPLETE', 'Successful');
define(exports.responseMessages, 'ACTION_COMPLETE_2', 'Importing in progress. Notify you via email when done.');
define(exports.responseMessages, 'LOGIN_SUCCESSFULLY', 'Logged in successfully.');
define(exports.responseMessages, 'SHOW_ERROR_MESSAGE', 'Some error occurred. Please refresh the page and try again.');
define(exports.responseMessages, 'IMAGE_FILE_MISSING', 'Image file is missing.');
define(exports.responseMessages, 'ERROR_IN_EXECUTION', 'Some error occurred while executing. Please refresh the page and try again.');
define(exports.responseMessages, 'UPLOAD_ERROR', 'Error in uploading.');
define(exports.responseMessages, 'PASSWORD_CHANGED_SUCCESSFULLY', 'Password changed successfully.');
define(exports.responseMessages, 'EMAIL_ALREADY_EXISTS', 'This email ID is already registered.');
define(exports.responseMessages, 'TEAM_NAME_ALREADY_REGISTERED_WITH_YOU', 'Team name already exists.');
define(exports.responseMessages, 'INACTIVE_ACCOUNT', 'Your account is not active or blocked by admin. Please contact your admin.');
define(exports.responseMessages, 'INVALID_ACCESS', 'You are not authorized to do this.');
define(exports.responseMessages, 'INVALID_PICKUP', 'Pickup time should be greater then current time.');
define(exports.responseMessages, 'INVALID_DELIVERY', 'Delivery time should be greater then current time.');
define(exports.responseMessages, 'EMAIL_REGISTERED_ALREADY', 'Email already exists.');
define(exports.responseMessages, 'PICKUP_NOT_COMPLETED', 'Pickup not fully completed yet. Please complete pickup task first.');
define(exports.responseMessages, 'JOB_NOT_COMPLETED', 'Task is not complete yet. Please complete it first.');
define(exports.responseMessages, 'PASSWORD_ERROR', 'Password must be between 7 to 15 characters which contain at least one numeric digit and a special character.');
define(exports.responseMessages, 'APP_PASSWORD_ERROR', 'Password must be greater than ' + config.get('AppPasswordLength') + ' characters.');
define(exports.responseMessages, 'NO_PICKUP_OR_DELIVERY_ERROR', 'Please select either Pickup or Delivery with your work flow.');
define(exports.responseMessages, 'WORKFLOW_NOT_MATCHED', 'Workflow has been changed. Please logout from dashboard and login again.');
define(exports.responseMessages, 'NO_TEAMS_AVAILABLE', 'No team is available.');
define(exports.responseMessages, 'NO_DISPATCHERS_AVAILABLE', 'No manager is available.');
define(exports.responseMessages, 'EMAIL_NOT_EXISTS', 'This account is not registered with us.');
define(exports.responseMessages, 'ACTION_NOT_ALLOWED', 'This action is not permissible at this time.');
define(exports.responseMessages, 'FLEET_OFFLINE_ERROR', 'Please switch to On-Duty mode before this action.');
define(exports.responseMessages, 'JOB_COMPLETED', 'This task is already complete.');
define(exports.responseMessages, 'JOB_INTRANSIT', 'This task is already in-transit.');
define(exports.responseMessages, 'ACCOUNT_DELETED_ERROR', 'Your account has been deleted.');
define(exports.responseMessages, 'SAME_PASSWORD_ERROR', 'Old and new password should be different.');
define(exports.responseMessages, 'CARD_ADDED_SUCCESSFULLY', 'Card has been added successfully.');
define(exports.responseMessages, 'CARD_UPDATE_SUCCESSFULLY', 'Card has been updated successfully.');
define(exports.responseMessages, 'CARD_ALREADY_ADDED', 'Card is already added.');
define(exports.responseMessages, 'CARD_NOT_ADDED_ERROR', 'Please add credit card to update billing plan.');
define(exports.responseMessages, 'NO_DATA_FOUND', 'No data found.');
define(exports.responseMessages, 'UPDATE_BILLING_PLAN', 'Billing Plan has been successfully updated.');
define(exports.responseMessages, 'LOCATION_NOT_FETCHED_ERROR', 'Could not locate the address entered. Please check the address and try again.');
define(exports.responseMessages, 'EXCEED_FLEET_COUNT', 'You can use only <%FLEET%> with this plan.');
define(exports.responseMessages, 'INVALID_FORMAT', 'Please check the CSV format and try again.');
define(exports.responseMessages, 'INVALID_DATE_FORMAT', 'Incorrect date format. Please use as (MM/DD/YYYY mm:hh)');
define(exports.responseMessages, 'INVALID_EMAIL_FORMAT', 'Please check email format.');
define(exports.responseMessages, 'CSV_ROWS_ERROR', 'Rows should not be greater than 200 at a time.');
define(exports.responseMessages, 'TASKS_COUNT_ERROR', 'You have reached your monthly quota. Please upgrade your plan for more usage.');
define(exports.responseMessages, 'DELETE_TEAM_WARNING', 'If you delete a team, you will not be able to see <%FLEET%> assigned to that team on map. Please add them to a different team.');
define(exports.responseMessages, 'FLEET_ALREADY_IN_OTHER_TEAM', 'Some of the fleets are already part of any other team. Please remove them from their list first.');
define(exports.responseMessages, 'MONTHLY_BILLING_DECLINED', 'Your monthly billing has declined. Please check card details.');
define(exports.responseMessages, 'DELIVERY_NOT_FIRST', 'Delivery task should be prior than Pickup task. Please drag the tasks again.');
define(exports.responseMessages, 'SIZE_EXCEEDS', 'Size should be under 1 MB.');
define(exports.responseMessages, 'ACCOUNT_NOT_REGISTER', 'This account is not registered with ');
define(exports.responseMessages, 'UPLOAD_TASKS_PROCESS_ERROR', 'Your previous uploaded tasks are in progress. Please wait or come back in approx. in ');
define(exports.responseMessages, 'INVALID_POINT', 'Invalid points was supplied.');

//FOR FLAGS
define(exports.responseFlags, 'PARAMETER_MISSING', 100);
define(exports.responseFlags, 'INVALID_ACCESS_TOKEN', 101);
define(exports.responseFlags, 'INVALID_USERNAME', 201);
define(exports.responseFlags, 'WRONG_PASSWORD', 201);
define(exports.responseFlags, 'ACTION_COMPLETE', 200);
define(exports.responseFlags, 'LOGIN_SUCCESSFULLY', 200);
define(exports.responseFlags, 'SHOW_ERROR_MESSAGE', 201);
define(exports.responseFlags, 'IMAGE_FILE_MISSING', 102);
define(exports.responseFlags, 'ERROR_IN_EXECUTION', 404);
define(exports.responseFlags, 'UPLOAD_ERROR', 201);
define(exports.responseFlags, 'USER_NOT_FOUND', 201);
define(exports.responseFlags, 'PASSWORD_CHANGED_SUCCESSFULLY', 200);
define(exports.responseFlags, 'EXCEED_FLEET_COUNT', 202);
define(exports.responseFlags, 'ACCOUNT_EXPIRE', 401);
define(exports.responseFlags, 'SHOW_WARNING', 410);
define(exports.responseFlags, 'ACTION_COMPLETE_2', 205);

exports.notificationFlags = {};
define(exports.notificationFlags, "JOB_ASSIGN", 1);
define(exports.notificationFlags, "REASSIGN", 2);
define(exports.notificationFlags, "TASK_REMINDER", 3);
define(exports.notificationFlags, "JOB_DELETED", 4);
define(exports.notificationFlags, "BUILD_UPDATE", 5);
define(exports.notificationFlags, "TASK_UPDATE", 6);
define(exports.notificationFlags, "MARK_OFFLINE", 7);
define(exports.notificationFlags, "SILENT", 8);
define(exports.notificationFlags, "HEARTBEAT", 9);

exports.userFreeStatus = {};
define(exports.userFreeStatus, "FREE", 0);
define(exports.userFreeStatus, "BUSY", 1);

exports.userDeleteStatus = {};
define(exports.userDeleteStatus, "NO", 0);
define(exports.userDeleteStatus, "YES", 1);

exports.notificationStatus = {};
define(exports.notificationStatus, "VIEWED", 1);
define(exports.notificationStatus, "NOT_VIEWED", 0);

exports.userVerificationStatus = {};
define(exports.userVerificationStatus, "VERIFY", 1);
define(exports.userVerificationStatus, "NOT_VERIFY", 0);

exports.deviceType = {};
define(exports.deviceType, "ANDROID", 0);
define(exports.deviceType, "iOS", 1);

exports.isDispatcherStatus = {};
define(exports.isDispatcherStatus, "YES", 1);
define(exports.isDispatcherStatus, "NO", 0);

exports.hasPermissionStatus = {};
define(exports.hasPermissionStatus, "YES", 1);
define(exports.hasPermissionStatus, "NO", 0);

exports.jobStatus = {};
define(exports.jobStatus, "UPCOMING", 0);
define(exports.jobStatus, "STARTED", 1);
define(exports.jobStatus, "ENDED", 2);
define(exports.jobStatus, "FAILED", 3);
define(exports.jobStatus, "ARRIVED", 4);
define(exports.jobStatus, "PARTIAL", 5);
define(exports.jobStatus, "UNASSIGNED", 6);
define(exports.jobStatus, "ACCEPTED", 7);
define(exports.jobStatus, "DECLINE", 8);
define(exports.jobStatus, "CANCEL", 9);
define(exports.jobStatus, "DELETED", 10);
define(exports.jobStatus, "IGNORED", 11);

exports.jobStatusValue = {
    0: "Assigned",
    1: "Started",
    2: "Successful",
    3: "Failed",
    4: "In Progress",
    5: "Partial",
    6: "Unassigned",
    7: "Accepted",
    8: "Decline",
    9: "Cancel",
    10: "Deleted"
};

exports.fleetBlockStatus = {};
define(exports.fleetBlockStatus, "YES", 0);
define(exports.fleetBlockStatus, "NO", 1);

exports.jobAcknowledged = {};
define(exports.jobAcknowledged, "YES", 1);
define(exports.jobAcknowledged, "NO", 0);

exports.hasFieldInput = {};
define(exports.hasFieldInput, "YES", 1);
define(exports.hasFieldInput, "NO", 0);

exports.ratingWindowStatus = {};
define(exports.ratingWindowStatus, "YES", 1);
define(exports.ratingWindowStatus, "NO", 0);
define(exports.ratingWindowStatus, "DISABLE", 2);
define(exports.ratingWindowStatus, "CANCEL", 3);

exports.isCusotmerRated = {};
define(exports.isCusotmerRated, "YES", 1);
define(exports.isCusotmerRated, "NO", 0);

exports.completedByAdmin = {};
define(exports.completedByAdmin, "YES", 1);
define(exports.completedByAdmin, "NO", 0);

exports.jobAcknowledgementStatus = {};
define(exports.jobAcknowledgementStatus, "NO", 0);
define(exports.jobAcknowledgementStatus, "SUCCESS", 1);
define(exports.jobAcknowledgementStatus, "FAILED", 2);
define(exports.jobAcknowledgementStatus, "PARTIAL", 3);

exports.jobType = {};
define(exports.jobType, "PICKUP", 0);
define(exports.jobType, "DELIVERY", 1);
define(exports.jobType, "FOS", 2);
define(exports.jobType, "APPOINTMENT", 3);

exports.layoutType = {};
//define(exports.layoutType, "PICKUP",                0);
//define(exports.layoutType, "DELIVERY",              1);
define(exports.layoutType, "PICKUP_AND_DELIVERY", 0);
define(exports.layoutType, "APPOINTMENT", 1);
define(exports.layoutType, "FOS", 2);

exports.userActiveStatus = {};
define(exports.userActiveStatus, "ACTIVE", 1);
define(exports.userActiveStatus, "INACTIVE", 0);

exports.isFirstTimeLogin = {};
define(exports.isFirstTimeLogin, "YES", 1);
define(exports.isFirstTimeLogin, "NO", 0);
define(exports.isFirstTimeLogin, "LAYOUT", 2);

exports.previewType = {};
define(exports.previewType, "EMAIL", 1);
define(exports.previewType, "SMS", 0);

exports.hasPickup = {};
define(exports.hasPickup, "YES", 1);
define(exports.hasPickup, "NO", 0);

exports.hasDelivery = {};
define(exports.hasDelivery, "YES", 1);
define(exports.hasDelivery, "NO", 0);

exports.availableStatus = {};
define(exports.availableStatus, "AVAILABLE", 1);
define(exports.availableStatus, "NOT_AVAILABLE", 0);

exports.EmailIds = {};
define(exports.EmailIds, "SANJAY", "sanjay@click-labs.com");
define(exports.EmailIds, "ARSH", "arsh@tookanapp.com");
define(exports.EmailIds, "SUMEET", "sumeet@clicklabs.in");

exports.travellingMode = {};
define(exports.travellingMode, "DRIVING", 'driving');
define(exports.travellingMode, "CYCLING", 'Bicycling');

exports.constraintType = {};
define(exports.constraintType, "PICKUP_HARD", 1);
define(exports.constraintType, "DELIVERY_HARD", 2);
define(exports.constraintType, "PICKUP_AND_DELIVERY_HARD", 3);

exports.billingPlan = {};
define(exports.billingPlan, "TRIAL", 0);
define(exports.billingPlan, "FREE_LIMITED", 1);
define(exports.billingPlan, "FLEET_BASED", 2);
define(exports.billingPlan, "TASK_BASED", 3);

exports.billingPlanValue = {0: "TRIAL", 1: "FREE_LIMITED", 2: "AGENT_BASED", 3: "TASK_BASED"}
exports.reverseTransportType = {1: "Car", 2: "Motor Cycle", 3: "Bicycle", 4: "Scooter", 5: "Foot", 6: "Truck"}

exports.taskHistoryType = {};
define(exports.taskHistoryType, "STATE_CHANGED", "state_changed");
define(exports.taskHistoryType, "IMAGE_ADDED", "image_added");
define(exports.taskHistoryType, "TEXT_ADDED", "text_added");
define(exports.taskHistoryType, "IMAGE_AND_TEXT_ADDED", "image_and_text_added");
define(exports.taskHistoryType, "IMAGE_DELETED", "image_deleted");
define(exports.taskHistoryType, "TEXT_DELETED", "text_deleted");
define(exports.taskHistoryType, "SIGN_IMAGE_ADDED", "signature_image_added");
define(exports.taskHistoryType, "SIGN_IMAGE_UPDATED", "signature_image_updated");
define(exports.taskHistoryType, "IMAGE_UPDATE", "image_updated");
define(exports.taskHistoryType, "TEXT_UPDATE", "text_updated");
define(exports.taskHistoryType, "CUSTOM_FIELD", "custom_field_updated");
define(exports.taskHistoryType, "TASK_PUSH_NOTIFY", "task_assignment");
define(exports.taskHistoryType, "FAILED_REASON", "failed_reason");
define(exports.taskHistoryType, "CANCEL_REASON", "cancel_reason");

exports.reminderAccountExpiry = {};
define(exports.reminderAccountExpiry, "FIRST", 15);
define(exports.reminderAccountExpiry, "SECOND", 5);
define(exports.reminderAccountExpiry, "THIRD", 1);
define(exports.reminderAccountExpiry, "FORTH", 3);

exports.fleetStatusColor = {0: {0: '#999999', 1: '#63AE0C'}, 1: {0: '#999999', 1: '#2196F3'}};

exports.setUpWizardStep = {};
define(exports.setUpWizardStep, "FIRST", 1);
define(exports.setUpWizardStep, "SECOND", 2);
define(exports.setUpWizardStep, "THIRD", 3);

exports.defaultOptionalValue = {};
define(exports.defaultOptionalValue, "DEFAULT",
    [
        {
            "label": "accept",
            "value": 0
        },
        {
            "label": "notes",
            "value": 1,
            "required": 0
        },
        {
            "label": "images",
            "value": 1,
            "required": 0
        },
        {
            "label": "signature",
            "value": 1,
            "required": 0
        },
        {
            "label": "arrived",
            "value": 0
        },
        {
            "label": "slider",
            "value": 1
        },
        {
            "label": "confirm",
            "value": 0
        }
    ]
);

exports.broadcastType = {};
define(exports.broadcastType, "ONE_BY_ONE", 1);
define(exports.broadcastType, "SEND_TO_ALL", 2);

exports.autoAssign = {};
define(exports.autoAssign, "YES", 1);
define(exports.autoAssign, "NO", 0);
define(exports.autoAssign, "NO_ONE_ACCEPTED", 2);

exports.addOn = {};
define(exports.addOn, "ROUTING", 1);
define(exports.addOn, "INVOICE", 2);

exports.highest = {};
define(exports.highest, "VALUE", 999999999999999999);

//VERSION CONSTANTS
exports.refreshVersions = function () {
    myContext.fetchVersions(function (results) {
        console.log(JSON.stringify(results));
    });
}
exports.fetchVersions = function (callback) {
    var sql = "SELECT * FROM `tb_version2` order by user_id";
    connection.query(sql, function (err, version) {
        if (err) {
        } else {
            if (version && version.length) {

                exports.appVersions = {};

                // 0 for Android
                // 1 for ios.
                var versions = {}, tookanAndroidPath = {
                        path: version[0].path,
                        brand_name: version[0].brand_name,
                        gateway: version[0].gateway
                    }, tookanIosPath = {
                        path: version[1].path,
                        brand_name: version[1].brand_name,
                        gateway: version[1].gateway
                    },
                    tookanAndroidPath1 = {
                        path: version[2].path,
                        brand_name: version[2].brand_name,
                        gateway: version[2].gateway
                    }, tookanIosPath1 = {
                        path: version[3].path,
                        brand_name: version[3].brand_name,
                        gateway: version[2].gateway
                    }
                version.forEach(function (v) {
                    if (v.user_id) {
                        versions[v.device_type] = versions[v.device_type] || {};
                        versions[v.device_type] = {
                            path: v.path,
                            brand_name: v.brand_name,
                            gateway: v.gateway
                        };
                        versions[0] = tookanAndroidPath;
                        versions[1] = tookanIosPath;
                        versions[2] = tookanAndroidPath1;
                        versions[3] = tookanIosPath1;
                    }
                })

                exports.appVersions = versions;
            }
            return callback(exports.appVersions);
        }
    });
}

