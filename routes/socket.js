var commonFunc = require('./commonfunction');
var responses = require('./responses');

exports.socketInitialize = function (httpsServer) {
    var socketIO = require('socket.io').listen(httpsServer);
    process.on('emitCustomerFleetLocationUpdateResponse', function (data) {
        socketIO.to(data.socket_id).emit('fleet_location', data.response);
    })
    process.on('emitFleetTaskUpdateToCustomer', function (data) {
        socketIO.to(data.socket_id).emit('task_update_customer', data.response);
    })
    process.on('emitFleetLocationUpdateResponse', function (data) {
        socketIO.to(data.socket_id).emit('update_fleet_location', data.response);
    })
    process.on('dashboardResponse', function (data) {
        socketIO.to(data.socket_id).emit('dashboard', data.response);
    })
    process.on('notificationResponse', function (data) {
        if (data.notification.data.notification_id) {
            socketIO.to(data.socket_id).emit('check_notifications_success', {
                "socket_id": data.socket_id,
                "notification": data.notification
            });
        }
    })
    process.on('refreshPage', function (data) {
        socketIO.to(data.socket_id).emit('refresh_page', data);
    })
    process.on('refreshAPI', function (data) {
        socketIO.to(data.socket_id).emit('refresh_api', data);
    })
    socketIO.on('connection', function (socket) {
        socket.on('on_boarding', function (data) {
            commonFunc.authenticateUserAccessToken(data.access_token, function (result) {
                if (result) {
                    console.log("into on_boarding", data.access_token, result[0].email, socket.id);
                    var userSocketObject = {};
                    userSocketObject.socket_id = socket.id;
                    userSocketObject.token = data.access_token;
                    userSocketObject.email = result[0].email;
                    module.exports.get_redis_client_users(function (users) {
                        users.push(userSocketObject);
                        module.exports.insert_into_redis_users(users);
                    })
                }
            });
        });

        socket.on('customer_panel', function (data) {
            commonFunc.authenticateJobHash(data.job_id, function (result) {
                if (result && result[0].fleet_id) {
                    console.log("into customer_panel",result[0].fleet_id, socket.id);
                    var customerSocketObject = {};
                    customerSocketObject.socket_id = socket.id;
                    customerSocketObject.fleet = result[0].fleet_id;
                    module.exports.get_redis_client_customers(function (customers) {
                        customers.push(customerSocketObject);
                        module.exports.insert_into_redis_customers(customers);
                    })
                }
            });
        });

        socket.on('disconnect', function (data) {
            socket.conn.close();
            module.exports.deleting_from_redis_users(socket);
            module.exports.deleting_from_redis_customers(socket);
        });

        socket.on('disconnect_user', function (data) {
            socket.conn.close();
            module.exports.deleting_from_redis_users(socket);
            module.exports.deleting_from_redis_customers(socket);
        });
    });
}

exports.refresh_client = function (req, res) {
    module.exports.get_redis_client_users(function (userSockets) {
        userSockets.forEach(function (sockets) {
            var response = {
                "socket_id": sockets.socket_id,
                "message": "Refresh Page"
            }
            process.emit('refreshPage', response);
        });
    });
    responses.actionCompleteResponse(res);
}

exports.insert_into_redis_users = function (userSockets) {
    redis_client.set(allUserSockets, JSON.stringify(userSockets));
}
exports.insert_into_redis_customers = function (customerSockets) {
    //console.log("Total customer count on socket === ", customerSockets.length);
    redis_client.set(allCustomerSockets, JSON.stringify(customerSockets));
}

exports.deleting_from_redis_users = function (socket) {
    redis_client.get(allUserSockets, function (err, reply) {
        var userSockets = JSON.parse(reply);
        userSockets = _lodash.uniq(userSockets, 'socket_id');
        userSockets.forEach(function (sockets, index) {
            if (sockets.socket_id == socket.id) {
                console.log("DELETING " + index, userSockets[index].email);
                userSockets.splice(index, 1);
            }
        })
        module.exports.insert_into_redis_users(userSockets);
    });
}

exports.deleting_from_redis_customers = function (socket) {
    redis_client.get(allCustomerSockets, function (err, reply) {
        var customerSockets = JSON.parse(reply);
        customerSockets = _lodash.uniq(customerSockets, 'socket_id');
        customerSockets.forEach(function (sockets, index) {
            if (sockets.socket_id == socket.id) {
                console.log("DELETING Customer" + index, customerSockets[index].fleet);
                customerSockets.splice(index, 1);
            }
        })
        module.exports.insert_into_redis_customers(customerSockets);
    });
}

exports.get_redis_client_users = function (callback) {
    redis_client.get(allUserSockets, function (err, reply) {
        reply = _lodash.uniq(JSON.parse(reply), 'socket_id');
        callback(reply);
    });
}

exports.get_redis_client_customers = function (callback) {
    redis_client.get(allCustomerSockets, function (err, reply) {
        reply = _lodash.uniq(JSON.parse(reply), 'socket_id');
        callback(reply);
    });
}