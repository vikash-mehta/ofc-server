var commonFunc = require('./commonfunction');
var md5 = require('MD5');
var responses = require('./responses');
var logging = require('./logging');
var mongo = require('./mongo');
var moment = require('moment');
/*
 * --------------------------------
 * VIEW TEMPLATE TEXT FROM DATABASE
 * --------------------------------
 */

exports.view_template = function (req, res) {

    var access_token = req.body.access_token;
    var manvalues = [access_token];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var layout_type = constants.jobType.PICKUP + "," + constants.jobType.DELIVERY;
                        if (result[0].layout_type == constants.layoutType.PICKUP_AND_DELIVERY) {
                            layout_type = constants.jobType.PICKUP + "," + constants.jobType.DELIVERY;
                        } else if (result[0].layout_type == constants.layoutType.APPOINTMENT) {
                            layout_type = constants.jobType.FOS;
                        } else {
                            layout_type = constants.jobType.APPOINTMENT;
                        }
                        var dispatcher_id = null;
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {

                            commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkPerm) {
                                if (chkPerm && chkPerm.length && chkPerm[0].view_notification == constants.hasPermissionStatus.NO) {
                                    responses.invalidAccessError(res);
                                }
                                else {
                                    dispatcher_id = result[0].user_id;
                                    result[0].user_id = result[0].dispatcher_user_id;

                                    step_in();
                                }
                            })
                        } else {
                            step_in();
                        }
                        function step_in() {
                            var sql = "SELECT `template_key`,`sms_text`,`email_text`,`email_subject`,`sms_enabled`,`email_enabled`,`layout_type`,`hook_enabled`,`hook` " +
                                "FROM `tb_templates` " +
                                "WHERE `user_id`=? AND `layout_type` IN (" + layout_type + ")";
                            connection.query(sql, [result[0].user_id], function (err, result_user_sms_text) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in viewing template info : ", err, result_user_sms_text);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var result_user_sms_text_length = result_user_sms_text.length;
                                    for (var i = 0; i < result_user_sms_text_length; i++) {
                                        result_user_sms_text[i].is_enabled = result_user_sms_text[i].email_enabled;
                                    }
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": result_user_sms_text
                                    };
                                    res.send(JSON.stringify(response));
                                    return;
                                }
                            });
                        }
                    }
                });
            }
        });
    }
};


/*
 * --------------------------
 * ADD template TEXT INTO DATABASE
 * --------------------------
 */

exports.insert_template_text = function (req, res) {

    var access_token = req.body.access_token;
    var template_key = req.body.template_key;
    var email_text = req.body.email_text;
    var email_subject = req.body.email_subject;
    var sms_text = req.body.sms_text;
    var hook = req.body.hook;
    var layout_type = req.body.layout_type;
    var manvalues = [access_token, email_subject, sms_text, email_text, template_key, layout_type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var dispatcher_id = null;
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {

                            commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkPerm) {
                                if (chkPerm && chkPerm.length && chkPerm[0].update_notification == constants.hasPermissionStatus.NO) {
                                    responses.invalidAccessError(res);
                                }
                                else {
                                    dispatcher_id = result[0].user_id;
                                    result[0].user_id = result[0].dispatcher_user_id;

                                    step_in();
                                }
                            })
                        } else {
                            step_in();
                        }

                        function step_in() {
                            var sql = "SELECT `template_id` FROM `tb_templates` WHERE `user_id`=? AND `template_key`=? AND `layout_type`=? LIMIT 1";
                            connection.query(sql, [result[0].user_id, template_key, layout_type], function (err, result_user_sms_text) {
                                if (err) {
                                    logging.logDatabaseQueryError("Error in viewing sms info : ", err, result_user_sms_text);
                                    responses.sendError(res);
                                    return;
                                } else {
                                    var response;
                                    if (result_user_sms_text.length > 0) {
                                        var sql = "UPDATE `tb_templates` SET `hook`=?, `email_text`=?, `email_subject`=?, `sms_text`=? " +
                                            "WHERE `user_id`=? AND `template_key`=? AND `layout_type`=? LIMIT 1";
                                        connection.query(sql, [hook, email_text, email_subject, sms_text, result[0].user_id, template_key, layout_type], function (err, update_user_sms_text) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in update " + template_key + " info : ", err, update_user_sms_text);
                                                responses.sendError(res);
                                                return;
                                            } else {
                                                responses.actionCompleteResponse(res);
                                                return;
                                            }
                                        });

                                    } else {
                                        var sql = "INSERT INTO `tb_templates` (`layout_type`,`hook`,`email_text`,`email_subject`,`sms_text`,`template_key`,`user_id`) VALUES (?,?,?,?,?,?,?)";
                                        connection.query(sql, [layout_type, hook, email_text, email_subject, sms_text, template_key, result[0].user_id], function (err, update_user_sms_text) {
                                            if (err) {
                                                logging.logDatabaseQueryError("Error in inserting " + template_key + " info : ", err, update_user_sms_text);
                                                responses.sendError(res);
                                                return;
                                            } else {
                                                responses.actionCompleteResponse(res);
                                                return;
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
            ;
        });
    }
};

/*
 * ------------------------------------
 * VIEW SMS VARIABLE W.R.T. MESSAGE_KEY
 * ------------------------------------
 */

exports.view_template_variables = function (req, res) {

    var access_token = req.body.access_token;
    var template_key = req.body.template_key;
    var layout_type = req.body.layout_type;
    var manvalues = [access_token, template_key, layout_type];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        commonFunc.authenticateTemplateKey(template_key, result[0].user_id, layout_type, function (authenticateTemplateKeyResult) {
                            if (result == 0) {
                                responses.authenticationError(res);
                                return;
                            } else {
                                var sql = "SELECT tb_tmp_var.`variable_name` FROM `tb_template_variables` tb_tmp_var " +
                                    "WHERE tb_tmp_var.`template_id`=? ";
                                connection.query(sql, [authenticateTemplateKeyResult[0].template_id], function (err, result_template_variables) {
                                    if (err) {
                                        logging.logDatabaseQueryError("Error in viewing template info : ", err, result_template_variables);
                                        responses.sendError(res);
                                        return;
                                    } else {
                                        var message_varaible = [];
                                        var result_user_sms_text_length = result_template_variables.length;
                                        for (var i = 0; i < result_user_sms_text_length; i++) {
                                            message_varaible.push(result_template_variables[i].variable_name);
                                        }
                                        if (layout_type == 0) {
                                            layout_type = constants.layoutType.PICKUP_AND_DELIVERY;
                                        } else if (layout_type == 1) {
                                            layout_type = constants.layoutType.PICKUP_AND_DELIVERY;
                                        } else if (layout_type == 1) {
                                            layout_type = constants.layoutType.FOS;
                                        } else if (layout_type == 2) {
                                            layout_type = constants.layoutType.APPOINTMENT;
                                        }
                                        mongo.getLayoutOptionalFieldsForUser(result[0].user_id, layout_type, function (getLayoutOptionalFieldsForUserResult) {
                                            if (getLayoutOptionalFieldsForUserResult.length > 0) {
                                                if (getLayoutOptionalFieldsForUserResult[0].fields.custom_field) {
                                                    getLayoutOptionalFieldsForUserResult[0].fields.custom_field.forEach(function (field) {
                                                        Object.keys(field).forEach(function (key) {
                                                            field[key].items.forEach(function (item) {
                                                                message_varaible.push('[' + field[key].template_id + '-' + item.label + ']');
                                                            })
                                                        });
                                                    });
                                                }
                                            }
                                            sendResponse(message_varaible, res);
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
            ;
        });
    }
};

function sendResponse(variable, res) {
    var response = {
        "message": constants.responseMessages.ACTION_COMPLETE,
        "status": constants.responseFlags.ACTION_COMPLETE,
        "data": {
            "allowed_variables": variable
        }
    };
    res.send(JSON.stringify(response));
    return;
}

/*
 * ------------------
 * PREVIEW MESSAGE
 * ------------------
 */

exports.preview_message = function (req, res) {

    var access_token = req.body.access_token;
    var job_id = req.body.job_id;
    var message = req.body.message;
    var manvalues = [access_token, job_id, message];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                commonFunc.checkAccountExpiry(result[0].user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult == 1) {
                        responses.accountExpiryErrorResponse(res);
                        return;
                    } else {
                        var dispatcher_id = null;
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {

                            commonFunc.checkDispatcherPermissions(result[0].user_id, function (chkPerm) {
                                if (chkPerm && chkPerm.length && chkPerm[0].view_notification == constants.hasPermissionStatus.NO) {
                                    responses.invalidAccessError(res);
                                }
                                else {
                                    dispatcher_id = result[0].user_id;
                                    result[0].user_id = result[0].dispatcher_user_id;

                                    step_in();
                                }
                            })
                        } else {
                            step_in();
                        }

                        function step_in() {
                            commonFunc.authenticateUserIdAndJobId(result[0].user_id, job_id, function (authenticateFleetIdAndJobIdResult) {
                                if (authenticateFleetIdAndJobIdResult == 0) {
                                    responses.authenticationErrorFleetAndJobID(res);
                                    return;
                                } else {
                                    commonFunc.getCustomeDetails(authenticateFleetIdAndJobIdResult[0].customer_id, function (customerDetailsResult) {

                                        commonFunc.getFleetDetails(job_id, function (getFleetDetailsResult) {

                                            var job_date = moment(authenticateFleetIdAndJobIdResult[0].job_delivery_datetime).format('MMM Do');
                                            var job_time = moment(authenticateFleetIdAndJobIdResult[0].job_delivery_datetime).format('ha');
                                            message = message.replace(/\[CustomerName]/g, customerDetailsResult[0].customer_username);

                                            message = message.replace(/\[Dispatcher]/g, result[0].username);
                                            message = message.replace(/\[Manager]/g, result[0].username);
                                            message = message.replace(/\[DispatcherPhNo]/g, result[0].phone);
                                            message = message.replace(/\[ManagerPhone]/g, result[0].phone);
                                            message = message.replace(/\[OrgName]/g, result[0].company_name);
                                            message = message.replace(/\[CompanyName]/g, result[0].company_name);

                                            message = message.replace(/\[TrackingLink]/g, config.get('trackLink') + authenticateFleetIdAndJobIdResult[0].job_hash);
                                            if (authenticateFleetIdAndJobIdResult[0].domain)
                                                message = message.replace(/\[TrackingLink]/g, config.get('trackingPage') + authenticateFleetIdAndJobIdResult[0].job_hash);
                                            message = message.replace(/\[Date]/g, job_date);
                                            message = message.replace(/\[Time]/g, job_time);

                                            message = message.replace(/\[DriverName]/g, getFleetDetailsResult[0].username);
                                            message = message.replace(/\[DriverPhone]/g, getFleetDetailsResult[0].phone);
                                            message = message.replace(/\[AgentName]/g, getFleetDetailsResult[0].username);
                                            message = message.replace(/\[AgentPhone]/g, getFleetDetailsResult[0].phone);

                                            message = message.replace(/\[NewLine]/g, "<br>");
                                            responses.actionCompleteResponse(res);
                                            return;
                                        })
                                    })
                                }
                            })
                        }
                    }
                })
            }
        })
    }
}
