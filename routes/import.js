var commonFunc = require('./commonfunction');
var responses = require('./responses');
var logging = require('./logging');
var moment = require('moment');
var md5 = require('MD5');
var async = require('async');
var parseCsv = require('fast-csv');
var jobs = require('./jobs');
var parseFormat = require('moment-parseformat');
var json2csv = require('json2csv');
var fs = require('fs');
var socketResponse = require('./socketResponses');
/*
 * -------------------------
 * IMPORT TASKS FROM CSV
 * -------------------------
 */
exports.import_tasks_csv = function (req, res) {

    var access_token = req.body.access_token;
    var timezone = req.body.timezone;
    var file = req.files.tasks_csv;
    var team_id = req.body.team_id;
    if (typeof(team_id) === "undefined" || team_id === "") {
        team_id = 0;
    }
    var p_c_f = req.body.p_c_f;
    if (typeof(p_c_f) === "undefined" || p_c_f === "") {
        p_c_f = "";
    }
    var d_c_f = req.body.d_c_f;
    if (typeof(d_c_f) === "undefined" || d_c_f === "") {
        d_c_f = "";
    }
    var manvalues = [access_token, timezone, team_id];
    var checkblank = commonFunc.checkBlank(manvalues);
    if (checkblank == 1) {
        responses.parameterMissingResponse(res);
        return;
    } else {
        commonFunc.authenticateUserAccessToken(access_token, function (result) {
            if (result == 0) {
                responses.authenticationErrorResponse(res);
                return;
            } else {
                var request = {
                    user_id: result[0].user_id,
                    dispatcher_user_id: result[0].dispatcher_user_id,
                    team_id: team_id,
                    access_token: access_token,
                    p_c_f: p_c_f,
                    d_c_f: d_c_f,
                    timezone: timezone
                };
                commonFunc.checkAccountExpiry(request.user_id, function (checkAccountExpiryResult) {
                    if (checkAccountExpiryResult) {
                        responses.accountExpiryErrorResponse(res);
                    } else {

                        //check for existing uploaded tasks

                        checkForExistingTasks(request, function(hasTasks) {

                            if (hasTasks.status){

                                var response = {
                                    "message": constants.responseMessages.UPLOAD_TASKS_PROCESS_ERROR + hasTasks.time_left,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data" : {}
                                };
                                res.send(JSON.stringify(response));

                            }else {

                                //parse CSV format
                                parseCSV(file.path, function (data) {
                                    var dataLength = data.success.length;
                                    if (dataLength > 0) {
                                        if (result[0].layout_type == constants.layoutType.PICKUP_AND_DELIVERY) {
                                            if (dataLength > 20) {
                                                pickupAndDeliveryDB(request, data, result, req, res);
                                            } else {
                                                pickupAndDelivery(request, data, result, req, res);
                                            }
                                        } else {
                                            if (dataLength > 20) {
                                                appointmentDB(request, data, result, req, res);
                                            } else {
                                                appointment(request, data, result, req, res);
                                            }
                                        }
                                    } else {
                                        responses.noDataFoundError(res);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
};

function checkForExistingTasks(request, callback){
    var user_id = request.user_id
    if (request.dispatcher_user_id){
        user_id = request.dispatcher_user_id
    }
    db.collection(config.get('mongoCollections.importedCsvData')).find({
        user_id: user_id
    }).toArray(function (err, result) {
        if (result && result.length){
            callback({
                status: true,
                time_left: commonFunc.millisecondsToStr(result.length * 310)
            });
        }else{
            callback({
                status: false
            });
        }
    });
}


function parseCSV(path, callback) {
    var SuccessArray = [], ErrorArray = [];
    parseCsv.fromPath(path)
        .on('data', function (data) {
            SuccessArray.push(data);
        })
        .on("error", function (data) {
            ErrorArray.push(data);
        })
        .on('end', function () {
            var ResultObject = {success: SuccessArray, error: ErrorArray};
            callback(ResultObject);
        });  // This just returns something from the inner function, which has no effect.
}

function hasNoDelivery(data, i) {
    for (var j = 2; j <= 8; j++) {
        if (data.success[i][j] == "") {
            return true;
        }
    }
    return false;
}

function generateMetaData(data, i, j) {
    var data2 = [], a = false;
    do {
        if (data.success[0][j]) {
            a = true;
            if (data.success[i][j]) {
                data2.push({
                    label: data.success[0][j],
                    data: data.success[i][j]
                })
            }
            j++;
        } else {
            a = false;
            return data2;
        }
    } while (a)
}

function pickupAndDelivery(request, data, result, req, res) {
    var index = 0, csvResponse = [];
    init_pd(index);
    function init_pd(item) {
        if (data.success && data.success[0] && data.success[0][0]) {
            if ((item == 0) && (data.success[0][0].indexOf("Task Description") >= 0)) {
                index++;
                init_pd(index);
            } else if (item > data.success.length - 1) {
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": csvResponse
                };
                res.send(JSON.stringify(response));
            } else {
                commonFunc.delay(item, function (i) {
                    (function (i) {
                        var fleet_id = "", address = "";
                        if (data.success[i][9]) {
                            fleet_id = data.success[i][9];
                        }
                        for (var j = 3; j <= 6; j++) {
                            if (data.success[i][j]) {
                                address += data.success[i][j] + ",";
                            }
                        }
                        address = address.substring(0, address.length - 1);
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            request.user_id = result[0].dispatcher_user_id;
                        }

                        commonFunc.authenticateFleetIdAndUserId(fleet_id, request.user_id, function (authenticateFleetIdAndUserIdResult) {
                            if (authenticateFleetIdAndUserIdResult != 0) {
                                if (data.success[i][10] == constants.hasPickup.YES) {
                                    var pickup_datetime = new Date(data.success[i][18]);
                                    if (data.success[i][18]) {
                                        pickup_datetime = new Date(moment(data.success[i][18], parseFormat(data.success[i][18])).format('YYYY-MM-DD HH:mm'));
                                        if (pickup_datetime.getHours() == 0 && pickup_datetime.getMinutes() == 0) {
                                            pickup_datetime.setHours(20);
                                            pickup_datetime.setMinutes(0);
                                        }
                                    }
                                    var pickup_address = '';
                                    for (var j = 13; j <= 16; j++) {
                                        if (data.success[i][j]) {
                                            pickup_address += data.success[i][j] + ",";
                                        }
                                    }
                                    pickup_address = pickup_address.substring(0, pickup_address.length - 1);

                                    if (hasNoDelivery(data, i)) {
                                        if (commonFunc.validateEmail(data.success[i][11])) {

                                            req.body = {
                                                "job_delivery_datetime": "",
                                                "job_description": data.success[i][0],
                                                "job_pickup_email": data.success[i][11],
                                                "job_pickup_name": data.success[i][12],
                                                "job_pickup_address": pickup_address,
                                                "job_pickup_phone": commonFunc.formatPhoneNumber(data.success[i][17], result[0].country_phone_code.toUpperCase()),
                                                "job_pickup_datetime": moment(pickup_datetime).format('YYYY-MM-DD HH:mm'),
                                                "fleet_id": fleet_id,

                                                "timezone": request.timezone,
                                                "order_id": data.success[i][0],
                                                "has_pickup": "1",
                                                "has_delivery": "0",
                                                "layout_type": "0",
                                                "tracking_link": "1",
                                                "team_id": request.team_id,
                                                "pickup_custom_field_template": request.p_c_f,
                                                "custom_field_template": request.d_c_f,
                                                "access_token": request.access_token
                                            }
                                            var resp = {
                                                send: function (obj) {
                                                    console.log(obj);
                                                    changeIndex(JSON.parse(obj));
                                                }
                                            }
                                            jobs.create_task(req, resp);
                                        } else {
                                            var response = {
                                                "message": constants.responseMessages.INVALID_EMAIL_FORMAT,
                                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                "data": {
                                                    "customer_name": data.success[i][12],
                                                    "customer_address": pickup_address
                                                }
                                            };
                                            changeIndex(response);
                                        }
                                    } else {
                                        if ((commonFunc.validateEmail(data.success[i][11])) && (commonFunc.validateEmail(data.success[i][1]))) {
                                            var datetime = new Date(data.success[i][8]);
                                            if (data.success[i][8]) {
                                                datetime = new Date(moment(data.success[i][8], parseFormat(data.success[i][8])).format('YYYY-MM-DD HH:mm'));
                                                if (datetime.getHours() == 0 && datetime.getMinutes() == 0) {
                                                    datetime.setHours(20);
                                                    datetime.setMinutes(0);
                                                }
                                            }
                                            req.body = {
                                                "customer_email": data.success[i][1],
                                                "customer_username": data.success[i][2],
                                                "customer_phone": commonFunc.formatPhoneNumber(data.success[i][7], result[0].country_phone_code.toUpperCase()),
                                                "customer_address": address,
                                                "job_description": data.success[i][0],
                                                "job_delivery_datetime": moment(datetime).format('YYYY-MM-DD HH:mm'),
                                                "job_pickup_email": data.success[i][11],
                                                "job_pickup_name": data.success[i][12],
                                                "job_pickup_address": pickup_address,
                                                "job_pickup_phone": commonFunc.formatPhoneNumber(data.success[i][17], result[0].country_phone_code.toUpperCase()),
                                                "job_pickup_datetime": moment(pickup_datetime).format('YYYY-MM-DD HH:mm'),
                                                "fleet_id": fleet_id,

                                                "timezone": request.timezone,
                                                "order_id": data.success[i][0],
                                                "has_pickup": "1",
                                                "has_delivery": "1",
                                                "layout_type": "0",
                                                "tracking_link": "1",
                                                "team_id": request.team_id,
                                                "pickup_custom_field_template": request.p_c_f,
                                                "custom_field_template": request.d_c_f,
                                                "access_token": request.access_token
                                            }
                                            var resp = {
                                                send: function (obj) {
                                                    console.log(obj);
                                                    changeIndex(JSON.parse(obj));
                                                }
                                            }
                                            jobs.create_task(req, resp);
                                        } else {
                                            var response = {
                                                "message": constants.responseMessages.INVALID_EMAIL_FORMAT,
                                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                "data": {
                                                    "customer_name": data.success[i][2],
                                                    "customer_address": address
                                                }
                                            };
                                            changeIndex(response);
                                        }
                                    }
                                } else {
                                    if ((commonFunc.validateEmail(data.success[i][1]))) {
                                        var datetime = new Date(data.success[i][8]);
                                        if (data.success[i][8]) {
                                            datetime = new Date(moment(data.success[i][8], parseFormat(data.success[i][8])).format('YYYY-MM-DD HH:mm'));
                                            if (datetime.getHours() == 0 && datetime.getMinutes() == 0) {
                                                datetime.setHours(20);
                                                datetime.setMinutes(0);
                                            }
                                        }
                                        req.body = {
                                            "customer_email": data.success[i][1],
                                            "customer_username": data.success[i][2],
                                            "customer_phone": commonFunc.formatPhoneNumber(data.success[i][7], result[0].country_phone_code.toUpperCase()),
                                            "customer_address": address,
                                            "job_description": data.success[i][0],
                                            "job_delivery_datetime": moment(datetime).format('YYYY-MM-DD HH:mm'),
                                            "fleet_id": fleet_id,

                                            "timezone": request.timezone,
                                            "order_id": data.success[i][0],
                                            "has_pickup": "0",
                                            "has_delivery": "1",
                                            "layout_type": "0",
                                            "tracking_link": "1",
                                            "team_id": request.team_id,
                                            "pickup_custom_field_template": request.p_c_f,
                                            "custom_field_template": request.d_c_f,
                                            "access_token": request.access_token
                                        }
                                        var resp = {
                                            send: function (obj) {
                                                console.log(obj);
                                                changeIndex(JSON.parse(obj));
                                            }
                                        }
                                        jobs.create_task(req, resp);
                                    } else {
                                        var response = {
                                            "message": constants.responseMessages.INVALID_EMAIL_FORMAT,
                                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                            "data": {
                                                "customer_name": data.success[i][2],
                                                "customer_address": address
                                            }
                                        };
                                        changeIndex(response);
                                    }
                                }
                            } else {
                                var response = {
                                    "message": constants.responseMessages.INVALID_ACCESS,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {
                                        "customer_name": data.success[i][2],
                                        "customer_address": address
                                    }
                                };
                                changeIndex(response);
                            }
                        });
                    })(i);
                });
            }
        } else {
            responses.noDataFoundError(res);
        }
    }

    function changeIndex(response) {
        csvResponse.push(response);
        index++;
        init_pd(index);
    }
}

function pickupAndDeliveryDB(request, data, result, req, res) {
    var index = 0, csvResponse = [];
    init_pd(index);
    function init_pd(item) {
        if (data.success && data.success[0] && data.success[0][0]) {
            if ((item == 0) && (data.success[0][0].indexOf("Task Description") >= 0)) {
                index++;
                init_pd(index);
            } else if (item > data.success.length - 1) {
                module.exports.startImportingProcess();
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE_2,
                    "status": constants.responseFlags.ACTION_COMPLETE_2,
                    "data": csvResponse
                };
                res.send(JSON.stringify(response));
            } else {
                commonFunc.delay2(item, function (i) {
                    (function (i) {
                        var fleet_id = "", address = "";
                        if (data.success[i][9]) {
                            fleet_id = data.success[i][9];
                        }
                        for (var j = 3; j <= 6; j++) {
                            if (data.success[i][j]) {
                                address += data.success[i][j] + ",";
                            }
                        }
                        address = address.substring(0, address.length - 1);
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            request.user_id = result[0].dispatcher_user_id;
                        }
                        commonFunc.authenticateFleetIdAndUserId(fleet_id, request.user_id, function (authenticateFleetIdAndUserIdResult) {
                            if (authenticateFleetIdAndUserIdResult != 0) {
                                if (data.success[i][10] == constants.hasPickup.YES) {
                                    var pickup_datetime = new Date(data.success[i][18]);
                                    if (data.success[i][18]) {
                                        pickup_datetime = new Date(moment(data.success[i][18], parseFormat(data.success[i][18])).format('YYYY-MM-DD HH:mm'));
                                        if (pickup_datetime.getHours() == 0 && pickup_datetime.getMinutes() == 0) {
                                            pickup_datetime.setHours(20);
                                            pickup_datetime.setMinutes(0);
                                        }
                                    }
                                    var pickup_address = '';
                                    for (var j = 13; j <= 16; j++) {
                                        if (data.success[i][j]) {
                                            pickup_address += data.success[i][j] + ",";
                                        }
                                    }
                                    pickup_address = pickup_address.substring(0, pickup_address.length - 1);

                                    if (hasNoDelivery(data, i)) {
                                        if (commonFunc.validateEmail(data.success[i][11])) {

                                            req.body = {
                                                "job_delivery_datetime": "",
                                                "job_description": data.success[i][0],
                                                "job_pickup_email": data.success[i][11],
                                                "job_pickup_name": data.success[i][12],
                                                "job_pickup_address": pickup_address,
                                                "job_pickup_phone": commonFunc.formatPhoneNumber(data.success[i][17], result[0].country_phone_code.toUpperCase()),
                                                "job_pickup_datetime": moment(pickup_datetime).format('YYYY-MM-DD HH:mm'),
                                                "fleet_id": fleet_id,

                                                "timezone": request.timezone,
                                                "order_id": data.success[i][0],
                                                "has_pickup": "1",
                                                "has_delivery": "0",
                                                "layout_type": "0",
                                                "tracking_link": "1",
                                                "team_id": request.team_id,
                                                "pickup_custom_field_template": request.p_c_f,
                                                "custom_field_template": request.d_c_f,
                                                "access_token": request.access_token
                                            }
                                            var response = {
                                                "message": constants.responseMessages.ACTION_COMPLETE,
                                                "status": constants.responseFlags.ACTION_COMPLETE,
                                                "data": {
                                                    "customer_name": data.success[i][12],
                                                    "customer_address": pickup_address,
                                                    "body": req.body,
                                                    "csv": data.success[i]
                                                }
                                            };
                                            changeIndex(response);
                                        } else {
                                            var response = {
                                                "message": constants.responseMessages.INVALID_EMAIL_FORMAT,
                                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                "data": {
                                                    "customer_name": data.success[i][12],
                                                    "customer_address": pickup_address,
                                                    "csv": data.success[i]
                                                }
                                            };
                                            changeIndex(response);
                                        }
                                    } else {
                                        if ((commonFunc.validateEmail(data.success[i][11])) && (commonFunc.validateEmail(data.success[i][1]))) {
                                            var datetime = new Date(data.success[i][8]);
                                            if (data.success[i][8]) {
                                                datetime = new Date(moment(data.success[i][8], parseFormat(data.success[i][8])).format('YYYY-MM-DD HH:mm'));
                                                if (datetime.getHours() == 0 && datetime.getMinutes() == 0) {
                                                    datetime.setHours(20);
                                                    datetime.setMinutes(0);
                                                }
                                            }
                                            req.body = {
                                                "customer_email": data.success[i][1],
                                                "customer_username": data.success[i][2],
                                                "customer_phone": commonFunc.formatPhoneNumber(data.success[i][7], result[0].country_phone_code.toUpperCase()),
                                                "customer_address": address,
                                                "job_description": data.success[i][0],
                                                "job_delivery_datetime": moment(datetime).format('YYYY-MM-DD HH:mm'),
                                                "job_pickup_email": data.success[i][11],
                                                "job_pickup_name": data.success[i][12],
                                                "job_pickup_address": pickup_address,
                                                "job_pickup_phone": commonFunc.formatPhoneNumber(data.success[i][17], result[0].country_phone_code.toUpperCase()),
                                                "job_pickup_datetime": moment(pickup_datetime).format('YYYY-MM-DD HH:mm'),
                                                "fleet_id": fleet_id,

                                                "timezone": request.timezone,
                                                "order_id": data.success[i][0],
                                                "has_pickup": "1",
                                                "has_delivery": "1",
                                                "layout_type": "0",
                                                "tracking_link": "1",
                                                "team_id": request.team_id,
                                                "pickup_custom_field_template": request.p_c_f,
                                                "custom_field_template": request.d_c_f,
                                                "access_token": request.access_token
                                            }
                                            var response = {
                                                "message": constants.responseMessages.ACTION_COMPLETE,
                                                "status": constants.responseFlags.ACTION_COMPLETE,
                                                "data": {
                                                    "customer_name": data.success[i][2],
                                                    "customer_address": address,
                                                    "body": req.body,
                                                    "csv": data.success[i]
                                                }
                                            };
                                            changeIndex(response);
                                        } else {
                                            var response = {
                                                "message": constants.responseMessages.INVALID_EMAIL_FORMAT,
                                                "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                                "data": {
                                                    "customer_name": data.success[i][2],
                                                    "customer_address": address,
                                                    "csv": data.success[i]
                                                }
                                            };
                                            changeIndex(response);
                                        }
                                    }
                                } else {
                                    if ((commonFunc.validateEmail(data.success[i][1]))) {
                                        var datetime = new Date(data.success[i][8]);
                                        if (data.success[i][8]) {
                                            datetime = new Date(moment(data.success[i][8], parseFormat(data.success[i][8])).format('YYYY-MM-DD HH:mm'));
                                            if (datetime.getHours() == 0 && datetime.getMinutes() == 0) {
                                                datetime.setHours(20);
                                                datetime.setMinutes(0);
                                            }
                                        }
                                        req.body = {
                                            "customer_email": data.success[i][1],
                                            "customer_username": data.success[i][2],
                                            "customer_phone": commonFunc.formatPhoneNumber(data.success[i][7], result[0].country_phone_code.toUpperCase()),
                                            "customer_address": address,
                                            "job_description": data.success[i][0],
                                            "job_delivery_datetime": moment(datetime).format('YYYY-MM-DD HH:mm'),
                                            "fleet_id": fleet_id,

                                            "timezone": request.timezone,
                                            "order_id": data.success[i][0],
                                            "has_pickup": "0",
                                            "has_delivery": "1",
                                            "layout_type": "0",
                                            "tracking_link": "1",
                                            "team_id": request.team_id,
                                            "pickup_custom_field_template": request.p_c_f,
                                            "custom_field_template": request.d_c_f,
                                            "access_token": request.access_token
                                        }
                                        var response = {
                                            "message": constants.responseMessages.ACTION_COMPLETE,
                                            "status": constants.responseFlags.ACTION_COMPLETE,
                                            "data": {
                                                "customer_name": data.success[i][2],
                                                "customer_address": address,
                                                "body": req.body,
                                                "csv": data.success[i]
                                            }
                                        };
                                        changeIndex(response);
                                    } else {
                                        var response = {
                                            "message": constants.responseMessages.INVALID_EMAIL_FORMAT,
                                            "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                            "data": {
                                                "customer_name": data.success[i][2],
                                                "customer_address": address,
                                                "csv": data.success[i]
                                            }
                                        };
                                        changeIndex(response);
                                    }
                                }
                            } else {
                                var response = {
                                    "message": constants.responseMessages.INVALID_ACCESS,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {
                                        "customer_name": data.success[i][2],
                                        "customer_address": address,
                                        "csv": data.success[i]
                                    }
                                };
                                changeIndex(response);
                            }
                        });
                    })(i);
                });
            }
        } else {
            responses.noDataFoundError(res);
        }
    }

    function changeIndex(response) {
        module.exports.insertImportedData(request.user_id, response, function (r) {
            index++;
            init_pd(index);
        });
    }
}

function appointment(request, data, result, req, res) {
    var index = 0, csvResponse = [];
    init_apt(index);
    function init_apt(item) {
        if (data.success && data.success[0] && data.success[0][0]) {
            var meta_data = [];
            if ((item == 0) && (data.success[0][0].indexOf("Task Description") >= 0)) {
                index++;
                init_apt(index);
            } else if (item > data.success.length - 1) {
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE,
                    "status": constants.responseFlags.ACTION_COMPLETE,
                    "data": csvResponse
                };
                res.send(JSON.stringify(response));
            } else {
                commonFunc.delay(item, function (i) {
                    (function (i) {
                        var fleet_id = "", address = "";
                        if (data.success[i][11]) {
                            request.team_id = data.success[i][11]
                        }
                        if (data.success[i][12]) {
                            request.d_c_f = data.success[i][12]
                            if (data.success[i][13]) {
                                meta_data = generateMetaData(data, i, 13);
                            }
                        }
                        if (data.success[i][10]) {
                            fleet_id = data.success[i][10];
                        }
                        for (var j = 3; j <= 6; j++) {
                            if (data.success[i][j]) {
                                address += data.success[i][j] + ",";
                            }
                        }
                        address = address.substring(0, address.length - 1);
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            request.user_id = result[0].dispatcher_user_id;
                        }

                        commonFunc.authenticateFleetIdAndUserId(fleet_id, request.user_id, function (authenticateFleetIdAndUserIdResult) {
                            if (authenticateFleetIdAndUserIdResult != 0) {
                                if ((commonFunc.validateEmail(data.success[i][1]))) {
                                    var pickupdatetime = new Date(data.success[i][8]);
                                    if (data.success[i][8]) {
                                        pickupdatetime = new Date(moment(data.success[i][8], parseFormat(data.success[i][8])).format('YYYY-MM-DD HH:mm'));
                                        if (pickupdatetime.getHours() == 0 && pickupdatetime.getMinutes() == 0) {
                                            pickupdatetime.setHours(0);
                                            pickupdatetime.setMinutes(0);
                                        }
                                    }
                                    var deliverydatetime = new Date(data.success[i][9]);
                                    if (data.success[i][9]) {
                                        deliverydatetime = new Date(moment(data.success[i][9], parseFormat(data.success[i][9])).format('YYYY-MM-DD HH:mm'));
                                        if (deliverydatetime.getHours() == 0 && deliverydatetime.getMinutes() == 0) {
                                            deliverydatetime.setHours(20);
                                            deliverydatetime.setMinutes(0);
                                        }
                                    }
                                    req.body = {
                                        "customer_email": data.success[i][1],
                                        "customer_username": data.success[i][2],
                                        "customer_phone": commonFunc.formatPhoneNumber(data.success[i][7], result[0].country_phone_code.toUpperCase()),
                                        "customer_address": address,
                                        "job_description": data.success[i][0],
                                        "job_pickup_datetime": moment(pickupdatetime).format('YYYY-MM-DD HH:mm'),
                                        "job_delivery_datetime": moment(deliverydatetime).format('YYYY-MM-DD HH:mm'),
                                        "fleet_id": fleet_id,

                                        "timezone": request.timezone,
                                        "order_id": data.success[i][0],
                                        "has_pickup": "0",
                                        "has_delivery": "0",
                                        "layout_type": result[0].layout_type,
                                        "tracking_link": "1",
                                        "team_id": request.team_id,
                                        "pickup_custom_field_template": request.p_c_f,
                                        "custom_field_template": request.d_c_f,
                                        "meta_data": meta_data,
                                        "access_token": request.access_token
                                    }
                                    var resp = {
                                        send: function (obj) {
                                            console.log(obj);
                                            changeAppointmentIndex(JSON.parse(obj));
                                        }
                                    }
                                    jobs.create_task(req, resp);
                                } else {
                                    var response = {
                                        "message": constants.responseMessages.INVALID_EMAIL_FORMAT,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {
                                            "customer_name": data.success[i][2],
                                            "customer_address": address
                                        }
                                    };
                                    changeAppointmentIndex(response);
                                }
                            } else {
                                var response = {
                                    "message": constants.responseMessages.INVALID_ACCESS,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {
                                        "customer_name": data.success[i][2],
                                        "customer_address": address
                                    }
                                };
                                changeAppointmentIndex(response);
                            }
                        });
                    })(i);
                });
            }

        } else {
            responses.noDataFoundError(res);
        }
    }

    function changeAppointmentIndex(response) {
        csvResponse.push(response);
        index++;
        init_apt(index);
    }
}

function appointmentDB(request, data, result, req, res) {
    var index = 0, csvResponse = [];
    init_apt(index);
    function init_apt(item) {
        if (data.success && data.success[0] && data.success[0][0]) {
            var meta_data = [];
            if ((item == 0) && (data.success[0][0].indexOf("Task Description") >= 0)) {
                index++;
                init_apt(index);
            } else if (item > data.success.length - 1) {

                module.exports.startImportingProcess();
                var response = {
                    "message": constants.responseMessages.ACTION_COMPLETE_2,
                    "status": constants.responseFlags.ACTION_COMPLETE_2,
                    "data": csvResponse
                };
                res.send(JSON.stringify(response));
            } else {
                commonFunc.delay2(item, function (i) {
                    (function (i) {
                        var fleet_id = "", address = "";
                        if (data.success[i][11]) {
                            request.team_id = data.success[i][11]
                        }
                        if (data.success[i][12]) {
                            request.d_c_f = data.success[i][12]
                            if (data.success[i][13]) {
                                meta_data = generateMetaData(data, i, 13);
                            }
                        }
                        if (data.success[i][10]) {
                            fleet_id = data.success[i][10];
                        }
                        for (var j = 3; j <= 6; j++) {
                            if (data.success[i][j]) {
                                address += data.success[i][j] + ",";
                            }
                        }
                        address = address.substring(0, address.length - 1);
                        if (result[0].is_dispatcher == constants.isDispatcherStatus.YES) {
                            request.user_id = result[0].dispatcher_user_id;
                        }
                        commonFunc.authenticateFleetIdAndUserId(fleet_id, request.user_id, function (authenticateFleetIdAndUserIdResult) {
                            if (authenticateFleetIdAndUserIdResult != 0) {
                                if ((commonFunc.validateEmail(data.success[i][1]))) {
                                    var pickupdatetime = new Date(data.success[i][8]);
                                    if (data.success[i][8]) {
                                        pickupdatetime = new Date(moment(data.success[i][8], parseFormat(data.success[i][8])).format('YYYY-MM-DD HH:mm'));
                                        if (pickupdatetime.getHours() == 0 && pickupdatetime.getMinutes() == 0) {
                                            pickupdatetime.setHours(0);
                                            pickupdatetime.setMinutes(0);
                                        }
                                    }
                                    var deliverydatetime = new Date(data.success[i][9]);
                                    if (data.success[i][9]) {
                                        deliverydatetime = new Date(moment(data.success[i][9], parseFormat(data.success[i][9])).format('YYYY-MM-DD HH:mm'));
                                        if (deliverydatetime.getHours() == 0 && deliverydatetime.getMinutes() == 0) {
                                            deliverydatetime.setHours(20);
                                            deliverydatetime.setMinutes(0);
                                        }
                                    }
                                    req.body = {
                                        "customer_email": data.success[i][1],
                                        "customer_username": data.success[i][2],
                                        "customer_phone": commonFunc.formatPhoneNumber(data.success[i][7], result[0].country_phone_code.toUpperCase()),
                                        "customer_address": address,
                                        "job_description": data.success[i][0],
                                        "job_pickup_datetime": moment(pickupdatetime).format('YYYY-MM-DD HH:mm'),
                                        "job_delivery_datetime": moment(deliverydatetime).format('YYYY-MM-DD HH:mm'),
                                        "fleet_id": fleet_id,

                                        "timezone": request.timezone,
                                        "order_id": data.success[i][0],
                                        "has_pickup": "0",
                                        "has_delivery": "0",
                                        "layout_type": result[0].layout_type,
                                        "tracking_link": "1",
                                        "team_id": request.team_id,
                                        "pickup_custom_field_template": request.p_c_f,
                                        "custom_field_template": request.d_c_f,
                                        "meta_data": meta_data,
                                        "access_token": request.access_token
                                    }
                                    var response = {
                                        "message": constants.responseMessages.ACTION_COMPLETE,
                                        "status": constants.responseFlags.ACTION_COMPLETE,
                                        "data": {
                                            "customer_name": data.success[i][2],
                                            "customer_address": address,
                                            "body": req.body,
                                            "csv": data.success[i]
                                        }
                                    }
                                    changeAppointmentIndex(response);
                                } else {
                                    var response = {
                                        "message": constants.responseMessages.INVALID_EMAIL_FORMAT,
                                        "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                        "data": {
                                            "customer_name": data.success[i][2],
                                            "customer_address": address,
                                            "csv": data.success[i]
                                        }
                                    };
                                    changeAppointmentIndex(response);
                                }
                            }
                            else {
                                var response = {
                                    "message": constants.responseMessages.INVALID_ACCESS,
                                    "status": constants.responseFlags.SHOW_ERROR_MESSAGE,
                                    "data": {
                                        "customer_name": data.success[i][2],
                                        "customer_address": address,
                                        "csv": data.success[i]
                                    }
                                };
                                changeAppointmentIndex(response);
                            }
                        });
                    })(i);
                })
            }
        }
        else {
            responses.noDataFoundError(res);
        }
    }

    function changeAppointmentIndex(response) {
        module.exports.insertImportedData(request.user_id, response, function (r) {
            index++;
            init_apt(index);
        });
    }
}


exports.insertImportedData = function (user_id, data, callback) {
    db.collection(config.get('mongoCollections.importedCsvData')).insert({
        user_id: parseInt(user_id),
        data: data
    }, function (err, inserted) {
        callback(1);
    })
}

exports.startImportingProcess = function (){
    db.collection(config.get('mongoCollections.importedCsvData')).distinct("user_id",function(err,users){
        if (!err) {
            users.forEach(function (u) {
                module.exports.createImportTasks([], [], u);
            })
        }
    });
}

exports.createImportTasks = function (csv, error_csv, user_id) {
    db.collection(config.get('mongoCollections.importedCsvData')).findOne({
        user_id: parseInt(user_id)
    }, function (err, fetch) {
        if (fetch) {
            if (fetch.data.status == constants.responseFlags.ACTION_COMPLETE) {
                var req = {};
                fetch.data.data.body.sock = 0;
                req.body = fetch.data.data.body;
                var resp = {
                    send: function (obj) {
                        db.collection(config.get('mongoCollections.importedCsvData')).remove({"_id": fetch._id}, function (d) {
                            commonFunc.delay(1, function (i) {
                                obj = JSON.parse(obj)
                                csv.push({
                                    Customer_Name: fetch.data.data.customer_name,
                                    Customer_Address: fetch.data.data.customer_address,
                                    Status: obj.status,
                                    Message: obj.message,
                                    Task_ID: obj.data.job_id
                                })
                                if (obj.status != constants.responseFlags.ACTION_COMPLETE) {
                                    error_csv.push(fetch.data.data.csv)
                                }
                                module.exports.createImportTasks(csv, error_csv, user_id);
                            });
                        })
                    }
                }
                jobs.create_task(req, resp);
            } else {
                db.collection(config.get('mongoCollections.importedCsvData')).remove({"_id": fetch._id}, function (d) {
                    commonFunc.delay(1, function (i) {
                        csv.push({
                            Customer_Name: fetch.data.data.customer_name,
                            Customer_Address: fetch.data.data.customer_address,
                            Status: fetch.data.status,
                            Message: fetch.data.message,
                            Task_ID: '-'
                        })
                        error_csv.push(fetch.data.data.csv)
                        module.exports.createImportTasks(csv, error_csv, user_id);
                    });
                })
            }
        } else {
            createCsvAndSendMail(csv, error_csv, user_id);
        }
    })
}

function createCsvAndSendMail(data, error_csv, user_id) {
    // Get User Details from user_id
    commonFunc.getUserDetails(user_id, function (user) {
        async.parallel([
                function (callback) {
                    if (data && data.length) {
                        //This is the header of the all responses file.
                        var fields = ['Task_ID', 'Customer_Name', 'Customer_Address', 'Status', 'Message'];
                        // convert all responses from json to csv
                        json2csv({data: data, fields: fields}, function (err, csv) {
                            if (err) console.log(err);
                            // write responses file
                            fs.writeFile(config.get('invoices') + user[0].user_id + '-responses.csv', csv, function (err) {
                                if (err) {
                                    callback(err, 0);
                                } else {
                                    callback(err, config.get('invoices') + user[0].user_id + '-responses.csv');
                                }
                            })
                        })
                    } else {
                        callback(null, 0);
                    }
                },
                function (callback) {
                    if (error_csv && error_csv.length) {
                        // convert all error responses from json to csv
                        json2csv({data: error_csv}, function (err, error_csv_result) {
                            error_csv_result = error_csv_result.toString(); // stringify buffer
                            var position = error_csv_result.toString().indexOf('\n'); // find position of new line element
                            if (position != -1) { // if new line element found
                                error_csv_result = error_csv_result.substr(position + 1); // subtract string based on first line length
                                // write Error file
                                fs.writeFile(config.get('invoices') + user[0].user_id + '-errors.csv', error_csv_result, function (err1) {
                                    if (err1) {
                                        callback(err1, 0);
                                    } else {
                                        callback(err1, config.get('invoices') + user[0].user_id + '-errors.csv');
                                    }
                                })
                            } else {
                                callback(err, 0);
                            }
                        })
                    } else {
                        callback(null, 0);
                    }
                }],
            function (err, responses) {

                //Send Socket Response to users for refresh getjobandfleet details API.
                socketResponse.refresh_api(user_id);

                var brand_name = 'Tookan';
                commonFunc.getVersion2(user[0].user_id,function(versions){
                    if (versions && versions.length) {
                        versions.forEach(function (vr) {
                            brand_name = vr.brand_name;
                        })
                    }
                    var body = "Please find attached your uploaded tasks` csv results.", subject = '' + brand_name + ' Import Csv Result';
                    if (responses[0] && responses[1]) {
                        // Send both attachments of all and error responses to user email.
                        commonFunc.sendEmailWithAttachment(responses[0] + 'spaces' + responses[1], user[0].email,
                            body, subject, '', function (result) {
                                // Delete all files that has been created locally for attachments to email.
                                commonFunc.fileDelete(responses[0]);
                                commonFunc.fileDelete(responses[1]);
                            })
                    } else if (responses[0] && !responses[1]) {
                        // Send both attachments of all responses to user email.
                        commonFunc.sendEmailWithAttachment(responses[0], user[0].email,
                            body, subject, '', function (result) {
                                // Delete all files that has been created locally for attachments to email.
                                commonFunc.fileDelete(responses[0]);
                            })
                    } else if (responses[1] && !responses[0]) {
                        // Send both attachments of all error responses to user email.
                        commonFunc.sendEmailWithAttachment(responses[1], user[0].email,
                            body, subject, '', function (result) {
                                // Delete all files that has been created locally for attachments to email.
                                commonFunc.fileDelete(responses[1]);
                            })
                    }
                });
            });

    });
}