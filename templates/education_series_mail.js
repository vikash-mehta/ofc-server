/**
 * Created by sumeet on 10/09/15.
 */
var md5 = require('MD5');
var commonFunc = require('../routes/commonfunction');
var responses = require('../routes/responses');
var commonFuncMailer = require('./commonfunctionMailer');
var async = require('async');
/*
 * -------------------------
 * SEND EDUCATION SERIES
 * -------------------------
 */
exports.send_education_series = function (req, res) {
    var email = req.body.email;
    if (typeof email === "undefined" || email === '') {
        email = '';
    }
    commonFunc.getAllUserOfSystem(email, function (all_users) {
        if (all_users == 0) {
            responses.noDataFoundError(res);
        } else {
            var all_users_length = all_users.length;
            var counter = 0;
            for (var i = 0; i < all_users_length; i++) {
                (function (i) {
                    var timediff = commonFunc.timeDifferenceInDays(all_users[i].creation_datetime, new Date());
                    var template = "";
                    if (timediff == 2) {
                        template = "tip-1-of-6";
                    } else if (timediff == 5) {
                        template = "tookan-tip-2-of-6-know-your-dashboard";
                    } else if (timediff == 8) {
                        template = "tookan-tip-3-of-6-color-coded-dashboard-statuses";
                    } else if (timediff == 11) {
                        template = "tookan-tip-4-of-6-customer-notifications";
                    } else if (timediff == 14) {
                        template = "tookan-tip-5-of-6-fleet-ios-android-apps";
                    } else if (timediff == 17) {
                        template = "tookan-tip-6-of-6-analytics-and-reports";
                    }
                    if (template) {
                        if (all_users[i].has_mails_enabled)  commonFuncMailer.send_template_mail(all_users[i].access_token, all_users[i].username, all_users[i].email, template, timediff);
                    }
                    counter++;
                    sendResponse(counter, all_users_length);
                })(i);
            }
            function sendResponse(counter, all_users_length) {
                if (counter == all_users_length) {
                    responses.actionCompleteResponse(res);
                    return;
                }
            }
        }
    })
};
/*
 * -------------------------
 * SEND WEEKLY REPORT
 * -------------------------
 */
exports.send_weekly_report = function (req, res) {
    var email = req.body.email;
    if (typeof email === "undefined" || email === '') {
        email = '';
    }
    commonFunc.getAllUserOfSystem(email, function (all_users) {
        if (all_users == 0) {
            responses.noDataFoundError(res);
        } else {
            var all_users_length = all_users.length, index = 0;
            sendWeeklyMail(index);
            function changeIndex() {
                index++;
                return sendWeeklyMail(index);
            }

            function sendWeeklyMail(item) {
                if (item > all_users_length - 1) {
                    responses.actionCompleteResponse(res);
                } else {
                    commonFunc.delay2(item, function (i) {
                        (function (i) {
                            async.parallel([
                                    function (callback) {
                                        var sql = "SELECT " +
                                            " (SELECT COUNT(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " ) as total_jobs," +
                                            " (SELECT COUNT(*) FROM tb_teams WHERE user_id=" + all_users[i].user_id + " ) as total_teams," +
                                            " (SELECT COUNT(*) FROM tb_users WHERE dispatcher_user_id=" + all_users[i].user_id + " ) as total_managers," +
                                            " (SELECT COUNT(*) FROM `tb_fleets` WHERE `user_id`=" + all_users[i].user_id + " AND `registration_status`=" + constants.userActiveStatus.ACTIVE + " " +
                                            " AND `is_active`=" + constants.userActiveStatus.ACTIVE + " AND `is_deleted`= " + constants.userDeleteStatus.NO + ") as total_fleets " +
                                            " FROM `tb_users` LIMIT 1 ";
                                        connection.query(sql, function (err, result0) {
                                            callback(null, result0[0]);
                                        });
                                    },
                                    function (callback) {
                                        var sql = "SELECT " +
                                            " (SELECT COUNT(*) FROM tb_jobs WHERE user_id=" + all_users[i].user_id + " AND `creation_datetime` > (NOW()-INTERVAL 7 DAY)) as weekly_jobs," +
                                            " (SELECT COUNT(*) FROM tb_teams WHERE user_id=" + all_users[i].user_id + " AND `creation_datetime` > (NOW()-INTERVAL 7 DAY)) as weekly_teams," +
                                            " (SELECT COUNT(*) FROM tb_users WHERE dispatcher_user_id=" + all_users[i].user_id + " AND `creation_datetime` > (NOW() - INTERVAL 7 DAY)) as weekly_managers," +
                                            " (SELECT COUNT(*) FROM `tb_fleets` WHERE `user_id`=" + all_users[i].user_id + " AND `registration_status`=" + constants.userActiveStatus.ACTIVE + " " +
                                            " AND `is_active`=" + constants.userActiveStatus.ACTIVE + " AND `is_deleted`= " + constants.userDeleteStatus.NO + " AND `creation_datetime` > (NOW()-INTERVAL 7 DAY)) as weekly_fleets " +
                                            " FROM `tb_users` LIMIT 1 ";
                                        connection.query(sql, function (err, result1) {
                                            callback(null, result1[0]);
                                        });
                                    },
                                    function (callback) {
                                        var sql = "SELECT `job_id`,`job_type`,`job_status`,`job_delivery_datetime`,`arrived_datetime`,`completed_datetime`,`job_time`,`timezone` " +
                                            " FROM `tb_jobs` " +
                                            " WHERE user_id = ? AND `creation_datetime` >= (NOW() - INTERVAL 7 DAY) ";
                                        connection.query(sql, [all_users[i].user_id], function (err, result2) {
                                            var on_time = 0, delayed = 0, successful = 0, failed = 0;
                                            result2.forEach(function (data) {
                                                if (data.arrived_datetime) {
                                                    if (new Date(data.arrived_datetime).getTime() <= new Date(data.job_time).getTime()) {
                                                        on_time++;
                                                    } else if (new Date(data.arrived_datetime).getTime() > new Date(data.job_time).getTime()) {
                                                        delayed++;
                                                    }
                                                }
                                                if (data.job_status) {
                                                    if (data.job_status == constants.jobStatus.ENDED) {
                                                        successful++;
                                                    } else if (data.job_status == constants.jobStatus.FAILED) {
                                                        failed++;
                                                    }
                                                }
                                            });
                                            var response = {
                                                on_time: on_time,
                                                delayed: delayed,
                                                successful: successful,
                                                failed: failed
                                            }
                                            callback(null, response);
                                        });
                                    },
                                    function (callback) {
                                        var sql = "SELECT `job_id`,`job_type`,`job_status`,`job_delivery_datetime`,`arrived_datetime`,`completed_datetime`,`job_time`,`timezone` " +
                                            " FROM `tb_jobs` " +
                                            " WHERE user_id = ? AND `creation_datetime` >= (NOW() - INTERVAL 14 DAY) " +
                                            " AND `creation_datetime` <= (NOW() - INTERVAL 7 DAY) ";
                                        connection.query(sql, [all_users[i].user_id], function (err, result2) {
                                            var on_time = 0, delayed = 0, successful = 0, failed = 0;
                                            result2.forEach(function (data) {
                                                if (data.arrived_datetime) {
                                                    if (new Date(data.arrived_datetime).getTime() <= new Date(data.job_time).getTime()) {
                                                        on_time++;
                                                    } else if (new Date(data.arrived_datetime).getTime() > new Date(data.job_time).getTime()) {
                                                        delayed++;
                                                    }
                                                }
                                                if (data.job_status) {
                                                    if (data.job_status == constants.jobStatus.ENDED) {
                                                        successful++;
                                                    } else if (data.job_status == constants.jobStatus.FAILED) {
                                                        failed++;
                                                    }
                                                }
                                            });
                                            var response = {
                                                on_time: on_time,
                                                delayed: delayed,
                                                successful: successful,
                                                failed: failed
                                            }
                                            callback(null, response);
                                        });
                                    },
                                    function (callback) {
                                        var sql = "SELECT * " +
                                            " FROM `tb_fleet_tracking` " +
                                            " WHERE `user_id` = ? AND `updated_datetime` >= (NOW() - INTERVAL 14 DAY) " +
                                            " AND `updated_datetime` <= (NOW()-INTERVAL 7 DAY) ";
                                        connection.query(sql, [all_users[i].user_id], function (err, result3) {
                                            var enroute_distance = 0, enroute_time = 0, idle_time = 0, idle_distance = 0, time_in_sec = 0, distance_in_meters = 0;
                                            result3.forEach(function (data) {
                                                if ((data.updated_datetime)) {
                                                    if (data.enroute_time) {
                                                        enroute_time += parseInt(data.enroute_time);
                                                    }
                                                    if (data.enroute_distance) {
                                                        enroute_distance += parseInt(data.enroute_distance);
                                                    }
                                                    if (data.time_in_sec) {
                                                        time_in_sec += parseInt(data.time_in_sec);
                                                    }
                                                    if (data.distance_in_metres) {
                                                        distance_in_meters += parseInt(data.distance_in_metres);
                                                    }
                                                }
                                            });
                                            idle_time = time_in_sec - enroute_time;
                                            idle_distance = distance_in_meters - enroute_distance;
                                            var response = {
                                                enroute_distance: (enroute_distance / 3600).toFixed(2),
                                                enroute_time: (enroute_time / 3600).toFixed(2),
                                                idle_time: (idle_time / 1000).toFixed(2),
                                                idle_distance: (idle_distance / 1000).toFixed(2)
                                            }
                                            callback(null, response);
                                        });
                                    },
                                    function (callback) {
                                        var sql = "SELECT * " +
                                            " FROM `tb_fleet_tracking` " +
                                            " WHERE `user_id` = ? " +
                                            " AND `updated_datetime` >= (NOW() - INTERVAL 7 DAY) ";
                                        connection.query(sql, [all_users[i].user_id], function (err, result4) {
                                            var enroute_distance = 0, enroute_time = 0, idle_time = 0, idle_distance = 0, time_in_sec = 0, distance_in_meters = 0;
                                            result4.forEach(function (data) {
                                                if ((data.updated_datetime)) {
                                                    if (data.enroute_time) {
                                                        enroute_time += parseInt(data.enroute_time);
                                                    }
                                                    if (data.enroute_distance) {
                                                        enroute_distance += parseInt(data.enroute_distance);
                                                    }
                                                    if (data.time_in_sec) {
                                                        time_in_sec += parseInt(data.time_in_sec);
                                                    }
                                                    if (data.distance_in_metres) {
                                                        distance_in_meters += parseInt(data.distance_in_metres);
                                                    }
                                                }
                                            });
                                            idle_time = time_in_sec - enroute_time;
                                            idle_distance = distance_in_meters - enroute_distance;
                                            var response = {
                                                enroute_distance: (enroute_distance / 3600).toFixed(2),
                                                enroute_time: (enroute_time / 3600).toFixed(2),
                                                idle_time: (idle_time / 1000).toFixed(2),
                                                idle_distance: (idle_distance / 1000).toFixed(2)
                                            }
                                            callback(null, response);
                                        });
                                    }],
                                function (err, results) {
                                    var response = {
                                        "total_data": results[0],
                                        "weekly_data": results[1],
                                        "user": {
                                            username: all_users[i].username,
                                            email: all_users[i].email,
                                            acc_token: all_users[i].access_token,
                                            user_id: all_users[i].user_id
                                        },
                                        "weekly_range": commonFunc.rangeWeek(7),
                                        "previous_week_tasks": results[3],
                                        "week_tasks": results[2],
                                        "previous_week_fleet_report": results[4],
                                        "week_fleet_report": results[5],
                                        "change_rate": {
                                            successful: (results[2].successful == 0 || results[3].successful == 0) ? '-' : Math.floor(((results[2].successful - results[3].successful) / results[3].successful) * 100) + "%",
                                            failed: (results[2].failed == 0 || results[3].failed == 0) ? '-' : Math.floor(((results[2].failed - results[3].failed) / results[3].failed) * 100) + "%",
                                            on_time: (results[2].on_time == 0 || results[3].on_time == 0) ? '-' : Math.floor(((results[2].on_time - results[3].on_time) / results[3].on_time) * 100) + "%",
                                            delayed: (results[2].delayed == 0 || results[3].delayed == 0) ? '-' : Math.floor(((results[2].delayed - results[3].delayed) / results[3].delayed) * 100) + "%",
                                            enroute_distance: (results[5].enroute_distance == 0 || results[4].enroute_distance == 0) ? '-' : Math.floor(((results[5].enroute_distance - results[4].enroute_distance) / results[4].enroute_distance) * 100) + "%",
                                            enroute_time: (results[5].enroute_time == 0 || results[4].enroute_time == 0) ? '-' : Math.floor(((results[5].enroute_time - results[4].enroute_time) / results[4].enroute_time) * 100) + "%",
                                            idle_time: (results[5].idle_time == 0 || results[4].idle_time == 0) ? '-' : Math.floor(((results[5].idle_time - results[4].idle_time) / results[4].idle_time) * 100) + "%",
                                            idle_distance: (results[5].idle_distance == 0 || results[4].idle_distance == 0) ? '-' : Math.floor(((results[5].idle_distance - results[4].idle_distance) / results[4].idle_distance) * 100) + "%"
                                        }
                                    };
                                    if (all_users[i].has_mails_enabled)  commonFuncMailer.send_weekly_mail(response, "weekly-analytics-report-thin-version");
                                    changeIndex();
                                });
                        })(i);
                    });
                }
            }
        }
    })
};
/*
 * -------------------
 * unsubscribe_mails
 * -------------------
 */
exports.unsubscribe_mails = function (req, res) {
    if (req.query && req.query.user && req.query.id) {
        commonFunc.getUserDetails(req.query.id, function (result) {
            if (result == 0) {
                res.statusCode = 401;
                res.end('<html><body style="margin: 50px 0px;padding:0px;text-align:center;">Invalid Credentails.</body></html>');
                return;
            } else {
                if (md5(result[0].access_token)==req.query.user) {
                    var user_id = result[0].user_id;
                    var sql = "UPDATE `tb_users` SET `has_mails_enabled`=0 WHERE `user_id` = ? LIMIT 1";
                    connection.query(sql, [user_id], function (err, result_update) {
                        if (err) {
                            res.statusCode = 401;
                            res.end('<html><body style="margin: 50px 0px;padding:0px;text-align:center;">Something went wrong!! Please try again.</body></html>');
                            return;
                        } else {
                            res.statusCode = 200;
                            res.end('<html><body style="margin: 50px 0px;padding:0px;text-align:center;">You have been successfully unsubscribed.</body></html>');
                            return;
                        }
                    });
                } else {
                    res.statusCode = 401;
                    res.end('<html><body style="margin: 50px 0px;padding:0px;text-align:center;">Invalid Credentails.</body></html>');
                    return;
                }
            }
        });
    } else {
        res.statusCode = 401;
        res.end('<html><body style="margin: 50px 0px;padding:0px;text-align:center;">Data not available.</body></html>');
        return;
    }
};