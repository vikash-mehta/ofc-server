/**
 * Created by Sumeet on 14/09/15.
 */
var md5 = require('MD5');
var commonFunc = require('../routes/commonfunction');

exports.invoice_pdf_html = function (date, invoice, user_name, email, company_name, company_address,
                                     company_phone, plan, unit, quantity, sub_total, grand_total, callback) {
    if (user_name != '' || user_name != null) {
        user_name = user_name.split(' ')[0];
    }
    var html = '';
    html += '<!DOCTYPE html>'
    html += ' <html lang="en">'
    html += '     <head>'
    html += '     <meta charset="utf-8">'
    html += '    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">'
    html += '     <link rel="shortcut icon" type="image/x-icon" href="https://app.tookanapp.com/app/img/favicon.ico"/>'
    html += '     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">'
    html += '     <title>Tookan</title>'
    html += '    </head>'
    html += '     <body style="width:850px;margin:0 auto; font-size:16px; border:1px solid #999; font-family: Arial, Helvetica, sans-serif; padding:1px;">'
    html += '     <div style=" height: 100px; background-color: #006291"> <div style=" color: #fff;'
    html += ' padding: 25px 50px;font-size: 20px;height: 50px;font-weight: bold;">'
    html += '<img src="http://tookanapp.com/wp-content/uploads/2015/11/logo-1.png"/>'
    html += '    <span style="float: right; color: #fff;margin-top: 12px;">'
    html += 'Invoice'
    html += '</span></div>'
    html += ' </div> <div>'
    html += ' <div style="display: inline-block; width:420px; padding:40px 0px 40px 50px">'
    html += '     <p style="color:#999;line-height: 1;margin: 0 0 5px 0;"> Invoice To: </p>'
    html += ' <p style="font-size:25px; line-height: 1; margin: 0;">' + user_name + '</p>'
    html += '    <p style="color:#999;line-height: 1;margin: 5px 0">' + company_name + '</p>'
    html += '<p style="color:#999">'
    html += company_address
    html += ' </p>'
    html += '<p style="color:#999">'
    html += email
    html += '</p>'
    html += '<p style="color:#999">'
    html += company_phone
    html += '     </p></div>'
    html += '    <div style="display: inline-block; width:290px; padding:50px 0px 10px 50px; vertical-align: top">'
    html += '    <p style="line-height: 1">'
    html += '    <span style="color:#999"> Invoice No:</span>'
    html += '<span style="font-size:"14px">' + invoice + '</span>'
    html += ' </p>'
    html += ' <p style="line-height: 1">'
    html += '    <span style="color:#999"> Invoice Date:</span>'
    html += ' <span>' + date + '</span>'
    html += '</p>'
    html += ' <p style="line-height: 1">'
    html += '     <span style="color:#999">Total Due:</span>'
    html += ' <span>$' + grand_total + '</span>'
    html += ' </p>'
    html += '</div>'
    html += '</div>'
    html += ' <div style="margin:0 50px">'
    html += '     <div style="text-align:center; padding:5px 0; display: inline-block; width:170px; margin-right:15px; color:#fff; background-color:#006291; font-weight:bold ">'
    html += '     <span>Plan</span>'
    html += '     </div>'
    html += '     <div style="text-align:center; padding:5px 0; display: inline-block; width:170px; margin-right:15px; color:#fff; background-color:#006291; font-weight:bold ">'
    html += '     <span>Unit Price</span>'
    html += ' </div>'
    html += ' <div style="text-align:center; padding:5px 0; display: inline-block; width:170px; margin-right:15px; color:#fff; background-color:#006291; font-weight:bold ">'
    html += '     <span>Quantity</span>'
    html += '     </div>'
    html += '     <div style="text-align:center; padding:5px 0; display: inline-block; width:170px;; color:#fff; background-color:#006291; font-weight:bold ">'
    html += '     <span>Subtotal</span>'
    html += '     </div>'
    html += '     </div>'
    html += '     <div style="margin:0 50px">'
    html += '     <div style="text-align:center; padding:5px 0; display: inline-block; width:170px; margin-right:15px; ">'
    html += '     <span>' + constants.billingPlanValue[parseInt(plan)] + '</span>'
    html += '     </div>'
    html += '     <div style="text-align:center; padding:5px 0; display: inline-block; width:170px; margin-right:15px; ">'
    html += '     <span>' + unit + '</span>'
    html += ' </div>'
    html += ' <div style="text-align:center; padding:5px 0; display: inline-block; width:170px; margin-right:15px; ">'
    html += '   <span>' + quantity + '</span>'
    html += '   </div>'
    html += '   <div style="text-align:center; padding:5px 0; display: inline-block; width:170px; ">'
    html += '   <span>$' + sub_total + '</span>'
    html += '   </div>'
    html += '   </div>'
    html += '   <div style="margin-left:50px; width:570px; display: inline-block">'
    html += '   <p style="text-align: right">'
    html += '   <span style="color: #006291;">Subtotal : </span>'
    html += ' </p>'
    html += ' <p style="text-align: right">'
    html += '     <span style="color: #006291;">VAT(10%) : </span>'
    html += ' </p>'
    html += ' <p style="text-align: right; font-weight:bold">'
    html += '     <span style="color: #006291;">Total : </span>'
    html += ' </p>'
    html += '  </div>'
    html += ' <div style="margin-right:50px; width:170px; display: inline-block">'
    html += '     <p style="text-align: center">'
    html += '    <span style="color: #000;">$' + sub_total + '</span>'
    html += '     </p>'
    html += '     <p style="text-align: center">'
    html += '     <span style="color: #000;">$0</span>'
    html += '     </p>'
    html += '     <p style="text-align: center; font-weight:bold">'
    html += '     <span style="color: #000;">$' + grand_total + '</span>'
    html += '     </p>'
    html += '     </div>'
    html += '     <div style="margin-top:300px">'
    html += '     <p style="color:#999; text-align:center">'
    html += '    Thank you for using Tookan.'
    html += '    If you have any questions, please contact us at <a style="color:#1b72e2" href="mailto:contact@tookanapp.com">contact@tookanapp.com</a>'
    html += ' </p>'
    html += ' <p style="color:#999; text-align:center">'
    html += '     Invoice was created on a computer and is valid without the signature and seal.'
    html += ' </p>'
    html += ' </div>'
    html += ' <div style="border-top:1px solid #999; margin:30px 0 10px 0;"></div>'
    html += '     <div style=" width:600px; margin:0 auto;">'
    html += '     <div style="width:300px; display: inline-block; padding-top: 40px; vertical-align: top;">'
    html += '     <img src="http://tookanapp.com/wp-content/uploads/2015/11/logo_dark.png"/>'
    html += '     </div>'
    html += '     <div style="width:280px; color:#999; display: inline-block;">'
    html += '     <p style="line-height: 1">Socomo Inc.</p>'
    html += ' <p  style="line-height: 1">114 Sansome St, Ste 250, San Francisco</p>'
    html += ' <p  style="line-height: 1"><a style="color:#1b72e2">contact@tookanapp.com</a></p>'
    html += ' </div>'
    html += '  </div>'
    html += '  </body>'
    html += '  </html>'


    callback(html);
}

/* Email Formatting */
exports.billing_mail_text = function (invoice, user_name, email, company_name, company_address,
                                      company_phone, plan, unit, quantity, sub_total, grand_total, callback) {
    if (user_name != '' || user_name != null) {
        user_name = user_name.split(' ')[0];
    }
    var msg = '<p style="text-decoration:none;"> Hi <b>' + user_name + ',</b></p>';
    msg += 'This is a confirmation from Tookan that you successfully paid the following invoice:<br><br>' +
        'Invoice No:   <b>' + invoice + '</b><br>' +
        'Account:       <b>' + email + '</b><br>' +
        'Status:          <b>Paid</b><br>' +
        'Amount:       <b> $' + grand_total + '</b><br><br>' +
        'You can download the attached invoice in printable format. Thank you for using Tookan. If you have any queries, please don`t hesitate to contact us: contact@tookanapp.com<br><br>';

    msg += '<b>Best,</b><br><b>Team Tookan</b>';
    msg += '<br><br>--<br>Sent by Tookan. 114 Sansome St Ste 250 San Francisco CA 94104, USA';
    return callback(msg);
}

exports.send_template_mail = function (acc_token, user_name, email, template_slug, days) {
    commonFunc.authenticateUserAccessToken(acc_token, function (data) {
        if (data && data.length) {
            if (user_name) {
                user_name = user_name.split(' ')[0];
            }
            var mandrill = require('mandrill-api/mandrill'), m = new mandrill.Mandrill(config.get('emailCredentials.senderPassword'));
            var params = {
                "template_name": template_slug,
                "template_content": [
                    {
                        "name": template_slug
                    }
                ],
                "message": {
                    "global_merge_vars": [{
                        "name": "FNAME",
                        "content": user_name
                    }, {
                        "name": "EMAIL",
                        "content": email
                    }, {
                        "name": "COMPANYNAME",
                        "content": data[0].company_name || ''
                    }, {
                        "name": "DASHBOARDLINK",
                        "content": config.get('webAppLink')
                    }, {
                        "name": "DAYSLEFT",
                        "content": days
                    }, {
                        "name": "UNSUB_LINK",
                        "content": config.get('apiLink') + 'unsubscribe_mails?user=' + md5(acc_token) + '&id=' + data[0].user_id
                    }
                    ],
                    "to": [{"email": email}]
                }
            }
            m.messages.sendTemplate(params, function (res) {
                console.log(res);
            }, function (err) {
                console.log(err);
            });
        }
    })
}

exports.send_weekly_mail = function (response, template_slug) {
    if (response.user.username) {
        response.user.username = response.user.username.split(' ')[0];
    }
    var mandrill = require('mandrill-api/mandrill'), m = new mandrill.Mandrill(config.get('emailCredentials.senderPassword'));
    var params = {
        "template_name": template_slug,
        "template_content": [
            {
                "name": template_slug
            }
        ],
        "message": {
            "global_merge_vars": [{
                "name": "UNSUB_LINK",
                "content": config.get('apiLink') + 'unsubscribe_mails?user=' + md5(response.user.acc_token) + '&id=' + response.user.user_id
            }, {
                "name": "FNAME",
                "content": response.user.username
            }, {
                "name": "EMAIL",
                "content": response.user.email
            }, {
                "name": "TOTAL_TASKS",
                "content": response.total_data.total_jobs
            }, {
                "name": "TOTAL_FLEETS",
                "content": response.total_data.total_fleets
            }, {
                "name": "TOTAL_TEAMS",
                "content": response.total_data.total_teams
            }, {
                "name": "TOTAL_MANAGERS",
                "content": response.total_data.total_managers
            }, {
                "name": "WEEKLY_TASKS",
                "content": response.weekly_data.weekly_jobs
            }, {
                "name": "WEEKLY_FLEETS",
                "content": response.weekly_data.weekly_fleets
            }, {
                "name": "WEEKLY_TEAMS",
                "content": response.weekly_data.weekly_teams
            }, {
                "name": "WEEKLY_MANAGERS",
                "content": response.weekly_data.weekly_managers
            }, {
                "name": "WEEKLY_RANGE",
                "content": response.weekly_range
            }, {
                "name": "SUCCESSFUL_TASKS",
                "content": response.week_tasks.successful
            }, {
                "name": "PREVIOUS_SUCCESSFUL_TASKS",
                "content": response.previous_week_tasks.successful
            }, {
                "name": "SUCCESSFUL_TASKS_CHANGE_RATE",
                "content": response.change_rate.successful
            }, {
                "name": "FAILED_TASKS",
                "content": response.week_tasks.failed
            }, {
                "name": "PREVIOUS_FAILED_TASKS",
                "content": response.previous_week_tasks.failed
            }, {
                "name": "FAILED_TASKS_CHANGE_RATE",
                "content": response.change_rate.failed
            }, {
                "name": "ONTIME_TASKS",
                "content": response.week_tasks.on_time
            }, {
                "name": "PREVIOUS_ONTIME_TASKS",
                "content": response.previous_week_tasks.on_time
            }, {
                "name": "ONTIME_TASKS_CHANGE_RATE",
                "content": response.change_rate.on_time
            }, {
                "name": "DELAYED_TASKS",
                "content": response.week_tasks.delayed
            }, {
                "name": "PREVIOUS_DELAYED_TASKS",
                "content": response.previous_week_tasks.delayed
            }, {
                "name": "DELAYED_TASKS_CHANGE_RATE",
                "content": response.change_rate.delayed
            }, {
                "name": "ENROUTE_DISTANCE",
                "content": response.week_fleet_report.enroute_distance
            }, {
                "name": "PREVIOUS_ENROUTE_DISTANCE",
                "content": response.previous_week_fleet_report.enroute_distance
            }, {
                "name": "ENROUTE_DISTANCE_CHANGE_RATE",
                "content": response.change_rate.enroute_distance
            }, {
                "name": "IDLE_DISTANCE",
                "content": response.week_fleet_report.idle_distance
            }, {
                "name": "PREVIOUS_IDLE_DISTANCE",
                "content": response.previous_week_fleet_report.idle_distance
            }, {
                "name": "IDLE_DISTANCE_CHANGE_RATE",
                "content": response.change_rate.idle_distance
            }, {
                "name": "ENROUTE_TIME",
                "content": response.week_fleet_report.enroute_distance
            }, {
                "name": "PREVIOUS_ENROUTE_TIME",
                "content": response.previous_week_fleet_report.enroute_distance
            }, {
                "name": "ENROUTE_TIME_CHANGE_RATE",
                "content": response.change_rate.enroute_distance
            }, {
                "name": "IDLE_TIME",
                "content": response.week_fleet_report.idle_time
            }, {
                "name": "PREVIOUS_IDLE_TIME",
                "content": response.previous_week_fleet_report.idle_time
            }, {
                "name": "IDLE_TIME_CHANGE_RATE",
                "content": response.change_rate.idle_time
            }, {
                "name": "WEEKLY_RANGE",
                "content": response.weekly_range
            }],
            "to": [{"email": response.user.email}]
        }
    }

    m.messages.sendTemplate(params, function (res) {
        console.log(res);
    }, function (err) {
        console.log(err);
    });
}

/*
 Change From Email based upon template slug
 */
function change_from_email(slug) {
    var from_email = 'contact@tookanapp.com';
    if (module.exports.from_saral.indexOf(slug) >= 0) {
        from_email = 'saral@tookanapp.com'
    } else if (module.exports.from_contact.indexOf(slug) >= 0) {
        from_email = 'contact@tookanapp.com'
    } else {
        from_email = 'contact@tookanapp.com'
    }
    return from_email;
}
exports.from_saral = [
    'fname-have-60-seconds-to-share-why',
    'tookan-welcome-email',
    'tip-1-of-6',
    'tookan-tip-2-of-6-know-your-dashboard',
    'tookan-tip-3-of-6-color-coded-dashboard-statuses',
    'tookan-tip-4-of-6-customer-notifications',
    'tookan-tip-5-of-6-fleet-ios-android-apps',
    'tookan-tip-6-of-6-analytics-and-reports'
]
exports.from_contact = [
    'your-tookan-trial-has-expired',
    'your-trial-expires-in-days-1st',
    'your-tookan-trial-expires-in-days-2nd',
    'your-tookan-trial-expires-in-days-3rd'
]