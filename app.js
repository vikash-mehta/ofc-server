/**
 * Module dependencies.
 */

require('newrelic');

process.env.NODE_CONFIG_DIR = 'config/';
// Moving NODE_APP_INSTANCE aside during configuration loading
var app_instance = process.argv.NODE_APP_INSTANCE;
process.argv.NODE_APP_INSTANCE = "";
config = require('config');
process.argv.NODE_APP_INSTANCE = app_instance;

require('pmx').init();
_lodash = require('lodash');

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var errorhandler = require('errorhandler');
var logger = require('morgan');
var methodOverride = require('method-override');
var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();

var users = require('./routes/users');
var jobs = require('./routes/jobs');
var fleets = require('./routes/fleets');
var customers = require('./routes/customers');
var messenger = require('./routes/messenger');
var teams = require('./routes/teams');
var notification = require('./routes/notification');
var settings = require('./routes/settings');
var reports = require('./routes/reports');
var dispatcher = require('./routes/dispatcher');
var cronJob = require('./routes/cron');
var superadmindashboard = require('./routes/superadmindashboard');
var billing = require('./routes/billing');
var fleetnotification = require('./routes/fleetnotification');
var imports = require('./routes/import');
var exports = require('./routes/export');
var commonFunc = require('./routes/commonfunction');
var socketJS = require('./routes/socket');
var extras = require('./routes/extras');
var integrations = require('./routes/integrations');
var educationSeries = require('./templates/education_series_mail');
var route = require('./routes/route-management');
var superadmin_analytics = require('./routes/superadmin-analytics');
var autoAssign = require('./routes/auto-assignment');
var mongo = require('./routes/mongo');
var heartbeat = require('./routes/app_heartbeat');

if (process.env.NODE_ENV != 'localhost') {
    mysqlLib = require('./routes/mysqlLib');
} else {
    mysqlLib = require('./routes/mysqlLocalLib');
}
constants = require('./routes/constants');
var app = express();

// all environments
app.set('port', process.env.PORT || config.get('PORT'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(favicon(__dirname + '/views/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});
// development only
if ('development' == app.get('env')) {
    app.use(errorhandler());
}
console.log("app_environment", app.get('env'));
app.get('/test', function (req, res) {
    res.render('test');
});
app.get('/distances', function (req, res) {
    res.render('distances');
});
app.get('/analytics', function (req, res) {
    res.render('analytics');
});
app.get('/billing', function (req, res) {
    res.render('billing');
});
app.get('/admintest', function (req, res) {
    res.render('admintest');
});
app.get('/socket', function (req, res) {
    res.render('socket-1');
});
app.get('/extras', function (req, res) {
    res.render('extras');
});
app.get('/route', function (req, res) {
    res.render('route');
});
app.get('/superadmin', function (req, res) {
    res.render('superadmin');
});

//....................... IMPORT API's.............................
app.post('/import_tasks_csv', multipartMiddleware, imports.import_tasks_csv);

//....................... EXPORT API's.............................
app.post('/export_tasks', exports.export_tasks);

//....................... USER API's.............................
app.post('/register_user', users.register_user);
app.post('/register', users.register);
app.post('/user_login_via_access_token', users.user_login_via_access_token);
app.post('/verify_user_account', users.verify_user_account);
app.post('/user_login', users.user_login);
app.post('/users_change_password', users.users_change_password);
app.post('/view_all_fleets', users.view_all_fleets);
app.post('/view_all_fleets_location', users.view_all_fleets_location);
app.post('/view_all_jobs', users.view_all_jobs);
app.get('/view_all_jobs_with_pagination', users.view_all_jobs_with_pagination);
app.get('/view_jobs_with_filters_and_pagination', users.view_jobs_with_filters_and_pagination);
app.post('/view_jobs_with_filters', users.view_jobs_with_filters);
app.post('/users_forgot_password_from_email', users.users_forgot_password_from_email);
app.post('/delete_fleet_account', users.delete_fleet_account);
app.post('/block_and_unblock_fleet_account', users.block_and_unblock_fleet_account);
app.post('/view_screen', users.view_screen);
app.post('/edit_fleet_profile_information', multipartMiddleware, users.edit_fleet_profile_information);
app.post('/update_task_via_dashboard', users.update_task_via_dashboard);
app.post('/enable_or_disable_notification', users.enable_or_disable_notification);
app.post('/skip_users_change_password', users.skip_users_change_password);
app.post('/set_one_time_processes', users.set_one_time_processes);
app.post('/getJobAndFleetDetails', users.get_jobs_and_fleet_details);
app.post('/cancel_onboarding', users.cancel_onboarding);
app.post('/change_fleet_availability_via_dashboard', users.change_fleet_availability_via_dashboard);

//....................... JOB API's.............................
app.post('/create_task', jobs.create_task);
app.post('/edit_task', jobs.edit_task);
app.post('/delete_job', jobs.delete_job);
app.post('/getETA', jobs.getETA);
app.post('/check_fleet_availability', jobs.check_fleet_availability);
app.post('/find_customer_with_phone', jobs.find_customer_with_phone);
app.post('/view_notification_job_details', jobs.view_notification_job_details);
app.post('/checkFleetCurrentJobs', jobs.checkFleetCurrentJobs);
app.post('/view_task_profile', jobs.view_task_profile);
app.post('/view_all_fleet_points', jobs.view_all_fleet_points);
app.post('/view_task_details', jobs.view_task_details);
app.post('/view_task_from_order_id', jobs.view_task_from_order_id);
app.post('/assign_fleet_to_task', jobs.assign_fleet_to_task);
app.post('/reassign_fleet_to_task', jobs.reassign_fleet_to_task);
app.post('/get_task_options', jobs.get_task_options);
app.post('/upload_ref_images', multipartMiddleware, jobs.upload_ref_images);
app.post('/get_extras', jobs.get_extras);

//....................... CUSTOMER API's.............................
app.post('/view_job_details', customers.view_job_details);
app.post('/view_customer_profile', customers.view_customer_profile);
app.post('/customer_rating', customers.customer_rating);
app.post('/task_information', customers.task_information);
app.post('/view_customers', customers.view_customers);
app.post('/update_customer', customers.update_customer);
app.get('/view_customers_with_pagination', customers.view_customers_with_pagination);
app.post('/export_customers', customers.export_customers);
app.post('/export_customers_rating', customers.export_customers_rating);
app.get('/get_customer_rating', customers.get_customer_rating);
app.post('/delete_customer', customers.delete_customer);
app.post('/import_customers_csv', multipartMiddleware, customers.import_customers_csv);

//....................... DISPATCHER API's.............................
app.post('/add_dispatcher', dispatcher.add_dispatcher);
app.post('/view_all_dispatcher', dispatcher.view_all_dispatcher);
app.post('/update_dispatcher', dispatcher.update_dispatcher);
app.post('/delete_dispatcher', dispatcher.delete_dispatcher);

//....................... ANALYTICS API's.............................
app.post('/heat_map_tasks', reports.heat_map_tasks);
app.post('/heat_map_deliveries_ontime_or_delayed', reports.heat_map_deliveries_ontime_or_delayed);
app.post('/heat_map_analytics', reports.heat_map_analytics);
app.post('/efficiency_report', reports.efficiency_report);
app.post('/effectiveness_report', reports.effectiveness_report);

//....................... TEAMS API's.............................
app.post('/create_team', teams.create_team);
app.post('/view_team', teams.view_team);
app.post('/update_team', teams.update_team);
app.post('/delete_team', teams.delete_team);

//....................... MESSENGER API's.............................
app.post('/insert_template_text', messenger.insert_template_text);
app.post('/view_template', messenger.view_template);
app.post('/view_template_variables', messenger.view_template_variables);
app.post('/preview_message', messenger.preview_message);

//....................... NOTIFICATION API's.............................
app.post('/reset_notification_count', notification.reset_notification_count);
app.post('/reset_notification_status', notification.reset_notification_status);
app.post('/reset_toaster_status', notification.reset_toaster_status);

//....................... SETTINGS API's.............................
app.post('/account_settings', multipartMiddleware, settings.account_settings);
app.post('/company_geo_settings', settings.company_geo_settings);
app.post('/update_workflow', settings.update_workflow);
app.post('/update_action', settings.update_action);
app.post('/get_user_layout_fields', settings.get_user_layout_fields);

//....................... FLEETNOTIFICATION API's.............................
app.post('/view_fleet_notifications', fleetnotification.view_fleet_notifications);
app.post('/insert_fleet_notification', fleetnotification.insert_fleet_notification);
app.post('/update_fleet_notification', fleetnotification.update_fleet_notification);
app.post('/delete_fleet_notification', fleetnotification.delete_fleet_notification);

//....................... BILLING API's.............................
app.post('/enable_billing_popup', billing.enable_billing_popup);
app.post('/skip_billing_popup', billing.skip_billing_popup);
app.post('/add_credit_card', billing.add_credit_card);
app.post('/update_credit_card', billing.update_credit_card);
app.post('/deduct_monthly_payment', billing.deduct_monthly_payment);
app.post('/create_token', billing.create_token);
app.post('/change_billing_plan', billing.change_billing_plan);
app.post('/billing_history', billing.billing_history);
app.post('/deactive_fleet_account', billing.deactive_fleet_account);
app.post('/deduct_individual_payment', billing.deduct_individual_payment);

//....................... CRON JOB API's.............................
app.get('/getAllTasksForTodayAndScheduleNotification', cronJob.getAllTasksForTodayAndScheduleNotification);
app.post('/mail_to_deactivate_customers', cronJob.sendMailTemplateToDeactivatedCustomers);

//....................... SUPERADMIN API's.............................
app.get('/view_all_users', superadmindashboard.view_all_users);
app.get('/users_today_activity', superadmindashboard.users_today_activity);
app.post('/view_all_users_new', superadmindashboard.view_all_users_new);
app.get('/view_users_with_pagination', superadmindashboard.view_users_with_pagination);
app.get('/view_all_fleets_of_user', superadmindashboard.view_all_fleets_of_user);
app.post('/view_all_fleets_of_user_new', superadmindashboard.view_all_fleets_of_user_new);
app.get('/view_all_jobs_of_user', superadmindashboard.view_all_jobs_of_user);
app.post('/view_all_jobs_of_user_new', superadmindashboard.view_all_jobs_of_user_new);
app.get('/view_all_dispatchers_of_user', superadmindashboard.view_all_dispatchers_of_user);
app.post('/view_all_dispatchers_of_user_new', superadmindashboard.view_all_dispatchers_of_user_new);
app.post('/send_update_push', superadmindashboard.send_update_push);
app.post('/super_admin_login', superadmindashboard.super_admin_login);
app.post('/dashboard_report', superadmindashboard.dashboard_report);
app.get('/view_all_tasks', superadmindashboard.view_all_tasks);

//....................... FLEET API's.............................
app.post('/add_fleet', fleets.add_fleet);
app.post('/create_fleet_account', multipartMiddleware, fleets.create_fleet_account);
app.post('/fleet_login', fleets.fleet_login);
app.post('/fleet_access_token_login', fleets.fleet_access_token_login);
app.post('/view_jobs', fleets.view_jobs);
app.post('/fleet_change_password', fleets.fleet_change_password);
app.post('/change_job_status', fleets.change_job_status);
app.post('/forgot_password', fleets.forgot_password);
app.post('/fleet_forgot_password_from_email', fleets.fleet_forgot_password_from_email);
app.post('/update_fleet_location', fleets.update_fleet_location);
app.post('/edit_profile_information', multipartMiddleware, fleets.edit_profile_information);
app.post('/fleet_logout', fleets.fleet_logout);
app.post('/view_fleet_profile', fleets.view_fleet_profile);
app.post('/upload_task_image', multipartMiddleware, fleets.upload_task_image);
app.post('/delete_task_image', fleets.delete_task_image);
app.post('/upload_task_details', multipartMiddleware, fleets.upload_task_details);
app.post('/edit_task_details', multipartMiddleware, fleets.edit_task_details);
app.post('/delete_task_details', fleets.delete_task_details);
app.post('/change_fleet_status', fleets.change_fleet_status);
app.post('/add_task_details', multipartMiddleware, fleets.add_task_details);
app.post('/delete_task_detail', fleets.delete_task_detail);
app.post('/update_task_detail', multipartMiddleware, fleets.update_task_detail);
app.post('/update_custom_fields', multipartMiddleware, fleets.update_custom_fields);
app.post('/update_fleet_tags', fleets.update_fleet_tags);

//....................... NEW FLEET API's.............................
app.post('/view_tasks', fleets.view_tasks);
app.post('/view_tasks_for_date', fleets.view_tasks_for_date);
app.post('/view_task_via_id', fleets.view_task_via_id);
app.post('/set_notification_tone', fleets.set_notification_tone);

//....................... SOCKET API's.............................
app.post('/refresh_client', socketJS.refresh_client);

//....................... EXTRA API's.............................
app.post('/add_templates', extras.add_templates);
app.post('/send_template_mail', extras.send_template_mail);
app.post('/checking_hook', extras.checking_hook);
app.post('/setFleetAvgRating', extras.setFleetAvgRating);
app.post('/checkGoogleShortenUrl', extras.checkGoogleShortenUrl);
app.get('/shift_mandrill_templates', extras.shift_mandrill_templates);
app.post('/heartbeat', heartbeat.heartbeat);
app.post('/refreshVersions', constants.refreshVersions);

//....................... THIRD PARTY INTEGRATION API's.............................
app.post('/shopify', integrations.shopify);
app.get('/zapier', integrations.zapier);
app.get('/unicommerce', integrations.unicommerce);
app.post('/zapier_hook', integrations.zapier_hook);

app.post('/send_education_series', educationSeries.send_education_series);
app.post('/send_weekly_report', educationSeries.send_weekly_report);
app.get('/unsubscribe_mails', educationSeries.unsubscribe_mails);

//....................... ROUTE MANAGEMENT API's.............................
app.post('/route_management', route.route_management);
app.post('/manage_route', route.manage_route);
app.post('/manual_route', route.manual_route);
app.post('/enable_routing', route.enable_routing);

//........................Super Admin Analytics Test.....................//
app.post('/analytics_test', superadmin_analytics.superadmin_analytics_insertion);
app.post('/get_chart_data', superadmin_analytics.get_chart_data);
app.post('/insert_previous_data', superadmin_analytics.superadmin_analytics_past_insertion);

//........................MONGO .....................//
app.post('/set_default_custom_template', mongo.setDefaultCustomTemplate);
app.post('/get_template_from_template_id', mongo.getTemplateFromTemplateID);
app.post('/setDefaultTaskOptionalFields', mongo.setDefaultTaskOptionalFields);
app.post('/setDefaultAppOptionalFields', mongo.setDefaultAppOptionalFields);

var startServer;
if (process.env.NODE_ENV === 'live') {
    var http = require('http');
    startServer = http.createServer(app).listen(app.get('port'), function () {
        startInitialProcess();
    });
    socketJS.socketInitialize(startServer);
} else {
    var fs = require('fs');
    var https = require('https');
    var options = {
        key: fs.readFileSync(config.get('sslCerificate.sslCerificateKey')),
        cert: fs.readFileSync(config.get('sslCerificate.sslCerificateCert')),
        ca: [
            fs.readFileSync(config.get('sslCerificate.sslCerificateCaBundle1'), 'utf8'),
            fs.readFileSync(config.get('sslCerificate.sslCerificateCaBundle2'), 'utf8'),
            fs.readFileSync(config.get('sslCerificate.sslCerificateCaBundle3'), 'utf8')
        ]
    };
    startServer = https.createServer(options, app).listen(app.get('port'), function () {
        startInitialProcess();
    });
    socketJS.socketInitialize(startServer);
}

function startInitialProcess() {
    var redis = require('redis');
    allUserSockets = 'userSockets' + '-' + app.get('env');
    allCustomerSockets = 'customerSockets' + '-' + app.get('env');
    db = '';
    cronJob.getAllTasksForTodayAndScheduleNotificationInCaseOfServerRestart(function (response) {
        autoAssign.processAgentsOneAtATime();
        autoAssign.scheduleAllPendingDisableAutoAssign();
        constants.refreshVersions();
    });
    console.log('Express server listening on port ' + app.get('port'));
    var MongoClient = require('mongodb').MongoClient;
    MongoClient.connect(config.get('databaseSettings.mongo_db_connection'), function (err, database) {
        if (err) throw err;
        db = database;
        redis_client = redis.createClient(config.get('databaseSettings.redis.port'), config.get('databaseSettings.redis.host'));
        redis_client.on('connect', function () {
            console.log('redis-connected ===============   ');
        });
        redis_client.del(allUserSockets);
        redis_client.del(allCustomerSockets);
        imports.startImportingProcess();
    });
}

